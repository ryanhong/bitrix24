<?php
$MESS["FILEMAN_PDFVIEWER_PARAMETER_HEIGHT"] = "高度";
$MESS["FILEMAN_PDFVIEWER_PARAMETER_IFRAME"] = "在iframe中嵌入查看器";
$MESS["FILEMAN_PDFVIEWER_PARAMETER_PATH"] = "文件路徑";
$MESS["FILEMAN_PDFVIEWER_PARAMETER_PRINT"] = "顯示打印按鈕";
$MESS["FILEMAN_PDFVIEWER_PARAMETER_PRINT_URL"] = "打印頁網址";
$MESS["FILEMAN_PDFVIEWER_PARAMETER_TITLE"] = "標題";
$MESS["FILEMAN_PDFVIEWER_PARAMETER_VIEWER_ID"] = "查看器ID";
$MESS["FILEMAN_PDFVIEWER_PARAMETER_WIDTH"] = "寬度";
