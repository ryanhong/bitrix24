<?php
$MESS["F_EMPTY_FORUMS"] = "沒有可用的論壇。";
$MESS["F_FORUMS"] = "論壇";
$MESS["F_FORUM_NAME"] = "論壇標題";
$MESS["F_HAVE_NEW_MESS"] = "新帖子";
$MESS["F_LAST_POST"] = "最後一篇文章";
$MESS["F_NOT_ACTIVE_FORUM"] = "非活動論壇";
$MESS["F_NOT_APPROVED"] = "未批准";
$MESS["F_NOT_APPROVED_POSTS"] = "帖子";
$MESS["F_NOT_APPROVED_TOPICS"] = "主題";
$MESS["F_NO_NEW_MESS"] = "沒有新帖子";
$MESS["F_POSTS"] = "帖子";
$MESS["F_SET_FORUMS_READ"] = "將所有帖子標記為閱讀";
$MESS["F_SUBFORUMS"] = "亞福琴：";
$MESS["F_SUBGROUPS"] = "亞組：";
$MESS["F_SUBSCRIBE_TO_NEW_TOPICS"] = "訂閱新主題";
$MESS["F_TOPICS"] = "主題";
