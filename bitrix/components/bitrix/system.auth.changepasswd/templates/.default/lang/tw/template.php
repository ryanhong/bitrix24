<?php
$MESS["AUTH_AUTH"] = "授權";
$MESS["AUTH_CHANGE"] = "更改密碼";
$MESS["AUTH_CHANGE_PASSWORD"] = "密碼更改";
$MESS["AUTH_CHECKWORD"] = "檢查字符串：";
$MESS["AUTH_LOGIN"] = "登入:";
$MESS["AUTH_NEW_PASSWORD"] = "新密碼：";
$MESS["AUTH_NEW_PASSWORD_CONFIRM"] = "確認密碼：";
$MESS["AUTH_NEW_PASSWORD_REQ"] = "新密碼：";
$MESS["AUTH_NONSECURE_NOTE"] = "密碼將以開放的形式發送。在您的Web瀏覽器中啟用JavaScript啟用密碼加密。";
$MESS["AUTH_REQ"] = "必需的字段";
$MESS["AUTH_SECURE_NOTE"] = "密碼將在發送之前進行加密。這將防止密碼以數據傳輸通道以開放形式出現。";
$MESS["sys_auth_changr_pass_current_pass"] = "當前密碼：";
$MESS["sys_auth_chpass_code"] = "驗證碼：";
$MESS["sys_auth_chpass_phone_number"] = "電話號碼：";
$MESS["system_auth_captcha"] = "輸入您在圖片上看到的字符：";
