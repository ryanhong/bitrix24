<?php
$MESS["SALE_SLS_DISABLE_KEYBOARD_INPUT_PARAMETER"] = "使用鍵盤禁用增量搜索；當用戶選擇一個時，隱藏下一個級別。";
$MESS["SALE_SLS_INITIALIZE_BY_GLOBAL_EVENT_PARAMETER"] = "僅當指定的JavaScript事件發生在window.document上時。";
$MESS["SALE_SLS_SUPPRESS_ERRORS_PARAMETER"] = "隱藏錯誤，如果加載組件時發生任何錯誤。";
