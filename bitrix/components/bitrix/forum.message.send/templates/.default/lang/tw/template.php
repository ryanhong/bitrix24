<?php
$MESS["F_CAPTCHA_PROMT"] = "從圖像中輸入文字";
$MESS["F_CAPTCHA_TITLE"] = "請輸入您在圖像中看到的單詞";
$MESS["F_EMAIL"] = "電子郵件地址";
$MESS["F_ICQ"] = "ICQ號碼";
$MESS["F_NAME"] = "姓名";
$MESS["F_SEND"] = "發送";
$MESS["F_TEXT"] = "訊息";
$MESS["F_TITLE_ICQ"] = "ICQ消息";
$MESS["F_TITLE_MAIL"] = "電子郵件";
$MESS["F_TOPIC"] = "話題";
