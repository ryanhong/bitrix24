<?php
$MESS["CATALOG_SC_BLOG_SECTION_TITLE"] = "網站評論";
$MESS["CATALOG_SC_BLOG_TITLE"] = "標籤文字";
$MESS["CATALOG_SC_BLOG_TITLE_VALUE"] = "評論";
$MESS["CATALOG_SC_BLOG_URL"] = "博客名稱（僅拉丁語）";
$MESS["CATALOG_SC_BLOG_USE"] = "使用評論";
$MESS["CATALOG_SC_CHECK_DATES"] = "僅顯示當前活動的物品的評論";
$MESS["CATALOG_SC_COMMENTS_COUNT"] = "最大限度。可見的評論";
$MESS["CATALOG_SC_ELEMENT_CODE"] = "產品代碼";
$MESS["CATALOG_SC_ELEMENT_ID"] = "產品ID";
$MESS["CATALOG_SC_EMAIL_NOTIFY"] = "電子郵件通知";
$MESS["CATALOG_SC_FB_APP_ID"] = "Facebook應用程序ID（app_id）";
$MESS["CATALOG_SC_FB_COLORSCHEME"] = "顏色主題";
$MESS["CATALOG_SC_FB_COLORSCHEME_DARK"] = "黑暗的";
$MESS["CATALOG_SC_FB_COLORSCHEME_LIGHT"] = "光";
$MESS["CATALOG_SC_FB_ORDER_BY"] = "種類";
$MESS["CATALOG_SC_FB_ORDER_BY_REVERSE_TIME"] = "創建 - 最古老的第一個";
$MESS["CATALOG_SC_FB_ORDER_BY_SOCIAL"] = "關聯";
$MESS["CATALOG_SC_FB_ORDER_BY_TIME"] = "創建 - 最新的第一個";
$MESS["CATALOG_SC_FB_SECTION_TITLE"] = "Facebook評論";
$MESS["CATALOG_SC_FB_TITLE"] = "標籤文字";
$MESS["CATALOG_SC_FB_TITLE_VALUE"] = "Facebook";
$MESS["CATALOG_SC_FB_USE"] = "使用Facebook";
$MESS["CATALOG_SC_FB_USER_ADMIN_ID"] = "Facebook用戶ID用作評論主持人";
$MESS["CATALOG_SC_IBLOCK_ID"] = "信息塊ID";
$MESS["CATALOG_SC_IBLOCK_TYPE"] = "信息塊類型";
$MESS["CATALOG_SC_PATH_TO_SMILE"] = "笑著的道路";
$MESS["CATALOG_SC_SHOW_DEACTIVATED"] = "顯示停用產品的評論";
$MESS["CATALOG_SC_SHOW_RATING"] = "啟用評分";
$MESS["CATALOG_SC_SHOW_SPAM"] = "使管理員可用\“用戶的註釋 /垃圾郵件\”鏈接";
$MESS["CATALOG_SC_URL_TO_COMMENT"] = "產品URL";
$MESS["CATALOG_SC_VK_API_ID"] = "應用ID（API ID）";
$MESS["CATALOG_SC_VK_SECTION_TITLE"] = "#VALUE!";
$MESS["CATALOG_SC_VK_TITLE"] = "#VALUE!";
$MESS["CATALOG_SC_VK_TITLE_VALUE"] = "#VALUE!";
$MESS["CATALOG_SC_VK_USE"] = "#VALUE!";
$MESS["CATALOG_SC_WIDTH"] = "寬度";
$MESS["CP_CC_ALLOW_IMAGE_UPLOAD"] = "允許上傳圖像";
$MESS["CP_CC_ALLOW_IMAGE_UPLOAD_VALUE_A"] = "全部";
$MESS["CP_CC_ALLOW_IMAGE_UPLOAD_VALUE_N"] = "沒有人";
$MESS["CP_CC_ALLOW_IMAGE_UPLOAD_VALUE_R"] = "註冊用戶";
$MESS["RATING_TYPE"] = "評分按鈕設計";
$MESS["RATING_TYPE_CONFIG"] = "預設";
$MESS["RATING_TYPE_LIKE_GRAPHIC"] = "我喜歡它（圖片）";
$MESS["RATING_TYPE_LIKE_TEXT"] = "我喜歡它（文字）";
$MESS["RATING_TYPE_STANDART_GRAPHIC"] = "我喜歡它/我不喜歡它（圖片）";
$MESS["RATING_TYPE_STANDART_TEXT"] = "我喜歡它/我不喜歡它（文字）";
