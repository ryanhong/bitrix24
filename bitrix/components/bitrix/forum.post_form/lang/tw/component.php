<?php
$MESS["FPF_CREATE_IN_FORUM"] = "在";
$MESS["FPF_EDIT"] = "編輯";
$MESS["FPF_EDIT_FORM"] = "修改消息";
$MESS["FPF_NO_ICON"] = "沒有圖標";
$MESS["FPF_REPLY"] = "回覆";
$MESS["FPF_REPLY_FORM"] = "回复表格";
$MESS["FPF_SEND"] = "創造";
$MESS["F_ERROR_MESSAGE_NOT_FOUND"] = "沒有找到該消息。";
$MESS["F_NO_MODULE"] = "論壇模塊未安裝";
