<?php
$MESS["AJAX_TYPE_TIP"] = "使用Ajax";
$MESS["CACHE_TIME_TIP"] = "緩存壽命（秒）";
$MESS["CACHE_TYPE_TIP"] = "緩存類型";
$MESS["FID_TIP"] = "論壇ID";
$MESS["MESSAGE_TYPE_TIP"] = "編輯器表單佈局（回复；修改；新主題）";
$MESS["MID_TIP"] = "發帖ID";
$MESS["PAGE_NAME_TIP"] = "調用組件的ID";
$MESS["SHOW_VOTE_TIP"] = "顯示民意測驗";
$MESS["SMILE_TABLE_COLS_TIP"] = "每行笑臉";
$MESS["TID_TIP"] = "主題ID";
$MESS["URL_TEMPLATES_HELP_TIP"] = "論壇幫助頁面";
$MESS["URL_TEMPLATES_LIST_TIP"] = "主題頁";
$MESS["URL_TEMPLATES_MESSAGE_TIP"] = "郵政查看頁面";
$MESS["URL_TEMPLATES_RULES_TIP"] = "論壇規則頁面";
