<?php
$MESS["CHAIN_ITEM_LINK_TIP"] = "該字段指定導航鏈項目的鏈接。";
$MESS["CHAIN_ITEM_TEXT_TIP"] = "指定導航鏈項目名稱。例如：\“編輯結果\”。";
$MESS["EDIT_ADDITIONAL_TIP"] = "選擇\“是\”將顯示其他Web表單字段（如果有）。";
$MESS["EDIT_STATUS_TIP"] = "選擇\“是\”將顯示結果狀態更改表格。";
$MESS["IGNORE_CUSTOM_TEMPLATE_TIP"] = "如果已檢查，將使用默認表單模板。";
$MESS["LIST_URL_TIP"] = "在此處指定結果頁面的名稱（URL）。默認值是result_list.php（即在當前目錄中）。";
$MESS["RESULT_ID_TIP"] = "指定一個評估result_id變量值的表達式，其中包含Web表單結果ID。默認情況下，包含<nobr> <b> = {\ $ _請求[\“ result_id \”]} </b> </nobr>，通過_request通過result_id。";
$MESS["SEF_FOLDER_TIP"] = "在此處指定頁面處於活動狀態時將在地址欄中顯示的文件夾名稱。";
$MESS["SEF_MODE_TIP"] = "檢查此選項以啟用搜索引擎友好（SEF）模式。";
$MESS["SEF_URL_TEMPLATES_edit_TIP"] = "該字段的內容定義了當活性編輯頁面處於活動狀態時將顯示在地址欄中的URL。默認 - ＃result_id＃/";
$MESS["USE_EXTENDED_ERRORS_TIP"] = "如果已檢查，將顯示錯誤消息，將突出顯示錯誤的字段。";
$MESS["VIEW_URL_TIP"] = "在此處指定結果視圖頁面的名稱（URL）。默認值是result_view.php（即在當前目錄中）。";
