<?php
$MESS["SONET_FILES"] = "駕駛";
$MESS["SONET_FILES_IS_NOT_ACTIVE"] = "未啟用\“驅動器\”功能。";
$MESS["SONET_GROUP_FILES_ACCESS_DENIED"] = "拒絕訪問工作組文件。";
$MESS["SONET_GROUP_NOT_EXISTS"] = "該組不存在。";
$MESS["SONET_GROUP_PREFIX"] = "團體：";
$MESS["SONET_IBLOCK_ID_EMPTY"] = "未指定信息塊。";
$MESS["SONET_IB_MODULE_IS_NOT_INSTALLED"] = "信息塊模塊未安裝。";
$MESS["SONET_USER_FILES_ACCESS_DENIED"] = "拒絕訪問用戶文件。";
$MESS["SONET_WD_MODULE_IS_NOT_INSTALLED"] = "未安裝文檔庫模塊。";
$MESS["SONET_WEBDAV_NOT_EXISTS"] = "沒有文件。";
$MESS["WD_HOW_TO_INCREASE_QUOTA"] = "<a href= \"#href#maxfilesize \ \">如何增加配額？</a>";
