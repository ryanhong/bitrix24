<?php
$MESS["CP_BSP_NO_WORD_LOGIC"] = "禁用將通用單詞作為邏輯運算符的處理";
$MESS["F_DATE_FORMAT"] = "日期格式";
$MESS["F_DEFAULT_FID"] = "論壇ID";
$MESS["F_DISPLAY_PANEL"] = "顯示此組件的面板按鈕";
$MESS["F_INDEX_TEMPLATE"] = "論壇列表頁面";
$MESS["F_MESSAGE_TEMPLATE"] = "消息查看頁面";
$MESS["F_PAGE_NAVIGATION_TEMPLATE"] = "Pager模板的名稱";
$MESS["F_PAGE_NAVIGATION_WINDOW"] = "頁面導航中的頁數";
$MESS["F_READ_TEMPLATE"] = "主題閱讀頁面";
$MESS["F_SET_NAVIGATION"] = "顯示麵包屑導航";
$MESS["F_TOPICS_PER_PAGE"] = "每頁主題數";
$MESS["SEARCH_RESTART"] = "嘗試在沒有形態支持的情況下搜索（如果找不到搜索結果）";
