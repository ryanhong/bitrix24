<?php
$MESS["FTL_EMAIL_ACTIVE_CHECKBOX"] = "添加電子郵件發送的帖子：";
$MESS["FTL_EMAIL_CANCEL"] = "取消";
$MESS["FTL_EMAIL_CHECK_F"] = "連接錯誤。";
$MESS["FTL_EMAIL_CHECK_OK"] = "該連接已成功建立。收件箱中的消息：";
$MESS["FTL_EMAIL_FILTER"] = "濾波器工作組也：";
$MESS["FTL_EMAIL_FILTER_EMAIL"] = "由收件人的電子郵件地址：";
$MESS["FTL_EMAIL_FILTER_HINT"] = "此設置允許對多個工作組使用相同的電子郵件帳戶。如果適用任何限制，將忽略以BCC為BCC的消息將被忽略。";
$MESS["FTL_EMAIL_FILTER_SUBJECT"] = "由主題基因：";
$MESS["FTL_EMAIL_FROM"] = "當前的小組電子郵件地址：";
$MESS["FTL_EMAIL_FROM_HINT"] = "發送論壇消息時用作回复地址";
$MESS["FTL_EMAIL_GROUP"] = "一個特殊的電子郵件標頭用於單個主題中的消息。此外，消息親和力可以由消息主題定義。
在此模式下，所有訂戶將收到一個主題僅包含主題標題的消息，即無論電子郵件模板偏好如何。";
$MESS["FTL_EMAIL_GROUP_NAME"] = "通過消息主題定義主題：";
$MESS["FTL_EMAIL_INTEGRATION"] = "電子郵件討論：";
$MESS["FTL_EMAIL_MAILBOX"] = "帳戶：";
$MESS["FTL_EMAIL_MAILBOX_CHECK"] = "查看";
$MESS["FTL_EMAIL_MAILBOX_DEL"] = "收到時在服務器上刪除消息：";
$MESS["FTL_EMAIL_MAILBOX_HINT"] = "您必須選擇先前配置的POP3/SMTP帳戶或創建一個新的POP3帳戶。";
$MESS["FTL_EMAIL_MAILBOX_LINK"] = "管理";
$MESS["FTL_EMAIL_MAILBOX_LOGIN"] = "使用者名稱:";
$MESS["FTL_EMAIL_MAILBOX_NAME"] = "姓名：";
$MESS["FTL_EMAIL_MAILBOX_NEW"] = "[新POP3帳戶]";
$MESS["FTL_EMAIL_MAILBOX_PASSW"] = "密碼：";
$MESS["FTL_EMAIL_MAILBOX_SEL"] = "（選擇一個帳戶）";
$MESS["FTL_EMAIL_MAILBOX_SERVER"] = "服務器端口：";
$MESS["FTL_EMAIL_MAIL_OFF"] = "離開";
$MESS["FTL_EMAIL_MAIL_SET"] = "設定";
$MESS["FTL_EMAIL_MAIL_TITLE"] = "使用電子郵件添加帖子";
$MESS["FTL_EMAIL_PUBLIC"] = "添加從任何地址發送的消息：";
$MESS["FTL_EMAIL_PUBLIC_HINT"] = "此選項將添加無法訪問該組的用戶的消息。使用此選項來存檔消息，以獲得授權的訪問權限的授權人員的進一步閱讀。";
$MESS["FTL_EMAIL_SAVE"] = "節省";
$MESS["FTL_EMAIL_SAVE_ERR1"] = "請選擇一個電子郵件帳戶或創建一個新帳戶！";
$MESS["FTL_EMAIL_SAVE_ERR2"] = "您必須指定新POP3帳戶的所有參數！";
$MESS["FTL_EMAIL_SAVE_ERR3"] = "需要回復到電子郵件地址。";
$MESS["FTL_EMAIL_SHOW_EMAIL"] = "在“來自”中顯示作者的電子郵件地址場地：";
$MESS["FTL_EMAIL_SHOW_EMAIL_HINT"] = "在公共論壇中顯示電子郵件地址可能是不安全的。";
$MESS["FTL_EMAIL_TITLE"] = "電子郵件討論集成設置";
$MESS["FTL_EMAIL_WARNING"] = "注意：在發送論壇消息時，論壇主題標題不論電子郵件模板偏好如何。";
$MESS["F_AUTHOR"] = "作者：";
$MESS["F_CLOSED"] = "關閉";
$MESS["F_CLOSED_TOPIC"] = "主題已關閉";
$MESS["F_CREATE_NEW_TOPIC"] = "<a href= \"#href# \">創建第一個主題</a>。";
$MESS["F_HAVE_NEW_MESS"] = "新帖子";
$MESS["F_HEAD_AUTHOR"] = "作者";
$MESS["F_HEAD_LAST_POST"] = "最後一篇文章";
$MESS["F_HEAD_POSTS"] = "帖子";
$MESS["F_HEAD_TOPICS"] = "主題";
$MESS["F_HEAD_VIEWS"] = "視圖";
$MESS["F_MANAGE_CLOSE"] = "親密的話題";
$MESS["F_MANAGE_DELETE"] = "刪除主題";
$MESS["F_MANAGE_OPEN"] = "開放話題";
$MESS["F_MANAGE_PIN"] = "針主題";
$MESS["F_MANAGE_TOPICS"] = "管理主題";
$MESS["F_MANAGE_UNPIN"] = "Untin主題";
$MESS["F_MESSAGE_NOT_APPROVED"] = "未批准的消息";
$MESS["F_MOVED"] = "移動";
$MESS["F_MOVED_TOPIC"] = "主題已移動";
$MESS["F_NEW_TOPIC"] = "添加新主題";
$MESS["F_NEW_TOPIC_TITLE"] = "在這個論壇上添加新主題";
$MESS["F_NO_NEW_MESS"] = "沒有新帖子";
$MESS["F_NO_TOPICS_HERE"] = "這裡沒有話題。";
$MESS["F_PINNED"] = "黏";
$MESS["F_PINNED_CLOSED_TOPIC"] = "關閉的固定話題";
$MESS["F_PINNED_TOPIC"] = "固定";
$MESS["F_SELECT_ALL"] = "全選";
$MESS["F_SUBSCRIBE"] = "訂閱";
$MESS["F_SUBSCRIBE_TO_NEW_POSTS"] = "訂閱新帖子";
$MESS["F_TOPIC_START"] = "開始日期";
$MESS["F_UNSUBSCRIBE"] = "退訂";
$MESS["F_UNSUBSCRIBE_TO_NEW_POSTS"] = "退訂新郵政通知";
$MESS["JS_DEL_TOPICS"] = "這將不可逆轉地刪除主題。繼續？";
$MESS["JS_NO_ACTION"] = "沒有選擇在主題上執行的動作。請選擇一個動作。";
$MESS["JS_NO_TOPICS"] = "沒有選擇主題。請選擇主題。";
