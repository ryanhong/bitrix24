<?php
$MESS["FL_FORUM_CHAIN"] = "論壇";
$MESS["FORUM_SONET_MODULE_NOT_AVAIBLE"] = "該論壇不適合該用戶。";
$MESS["FORUM_SONET_NO_ACCESS"] = "您無權查看論壇。";
$MESS["F_ERR_EMPTY_ACTION"] = "沒有選擇動作。";
$MESS["F_ERR_EMPTY_TOPICS"] = "沒有選擇主題。";
$MESS["F_ERR_SESS_FINISH"] = "您的會議已經過期。請重複操作。";
$MESS["F_ERR_TOPICS_NOT_MODERATION"] = "以下主題無法調節：＃主題＃。";
$MESS["F_FID_IS_EMPTY"] = "未指定社交網絡論壇。";
$MESS["F_FID_IS_LOST"] = "找不到社交網絡論壇。";
$MESS["F_NO_MODULE"] = "論壇模塊未安裝。";
$MESS["F_TOPIC_LIST"] = "主題";
$MESS["SFTL_ERROR_NO_GROUP"] = "找不到小組。";
$MESS["SFTL_ERROR_NO_USER"] = "找不到用戶。";
$MESS["SOCNET_FORUM_TL_EMAIL_RULE"] = "將帖子添加到社交網絡論壇";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
