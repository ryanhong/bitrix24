<?php
$MESS["F_DATE_FORMAT"] = "日期格式";
$MESS["F_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["F_DEFAULT_FID"] = "論壇ID";
$MESS["F_LIST_TEMPLATE"] = "主題列表頁面";
$MESS["F_MESSAGES_PER_PAGE"] = "每頁消息的數量";
$MESS["F_MESSAGE_TEMPLATE"] = "消息查看頁面";
$MESS["F_PAGEN"] = "帕根";
$MESS["F_PAGE_NAVIGATION_TEMPLATE"] = "麵包屑導航模板的名稱";
$MESS["F_PAGE_NAVIGATION_WINDOW"] = "頁面導航中的頁數";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "個人資料頁";
$MESS["F_READ_TEMPLATE"] = "主題閱讀頁面";
$MESS["F_SOCNET_GROUP_ID"] = "組ID";
$MESS["F_TOPICS_PER_PAGE"] = "每頁主題數";
$MESS["F_TOPIC_NEW_TEMPLATE"] = "開始一個新主題頁面";
$MESS["F_URL_TEMPLATES"] = "URL處理";
$MESS["F_USER_ID"] = "用戶身份";
$MESS["F_USE_DESC_PAGE"] = "使用反向頁面導航";
