<?php
$MESS["BLOG_VAR_TIP"] = "在此處指定通過博客ID傳遞的變量的名稱。";
$MESS["ID_TIP"] = "指定將傳遞用戶ID的代碼。";
$MESS["MESSAGE_COUNT_TIP"] = "可以在頁面上顯示的最大消息數量。";
$MESS["PAGE_VAR_TIP"] = "在此處指定博客頁面通過的變量的名稱。";
$MESS["PATH_TO_BLOG_CATEGORY_TIP"] = "帶有標籤過濾器的博客頁面的路徑。示例：<Nobr> blog_filter.php？page = blog＆blog =＃blog＃<b>＆category =＃類別＃</b>。</nobr>";
$MESS["PATH_TO_BLOG_TIP"] = "主要博客頁面的路徑。示例：blog_blog.php？page = blog＆blog =＃blog＃。";
$MESS["PATH_TO_POST_EDIT_TIP"] = "博客文章編輯頁面的路徑。示例：blog_p_edit.php？page = post_edit＆blog =＃blog＃＆post_id =＃post_id＃。";
$MESS["PATH_TO_POST_TIP"] = "博客文章詳細信息頁面的路徑。示例：<Nobr> blog_post.php？page = post＆blog =＃blog＃＆post_id =＃post_id＃。</nobr>";
$MESS["PATH_TO_SMILE_TIP"] = "包含笑臉的文件夾的路徑。";
$MESS["PATH_TO_USER_TIP"] = "用戶配置文件頁面的路徑。示例：<Nobr> blog_user.php？page = user＆user_id =＃user_id＃。</nobr>";
$MESS["POST_VAR_TIP"] = "在此處指定博客文章ID的變量的名稱。";
$MESS["SET_TITLE_TIP"] = "如果選項處於活動狀態，則頁面標題將設置為<botr> <b>我的朋友</b>。</nobr>";
$MESS["USER_VAR_TIP"] = "在此處指定通過博客用戶ID傳遞的變量的名稱。";
