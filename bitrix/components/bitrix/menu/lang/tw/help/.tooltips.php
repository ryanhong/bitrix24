<?php
$MESS["CHILD_MENU_TYPE_TIP"] = "類似於根菜單類型，但為其他級別定義了菜單類型。";
$MESS["MAX_LEVEL_TIP"] = "一個從1到4的整數指定Maximun菜單嵌套級別。 1以外的值僅適用於多級菜單模板。";
$MESS["ROOT_MENU_TYPE_TIP"] = "在此處選擇現有菜單類型之一。如果選擇<b> <i>（其他）</i> </b>，則必須在旁邊的字段中指定菜單。";
$MESS["USE_EXT_TIP"] = "如果檢查了，系統將嘗試在包括菜單時在每個部分中找到一個文件<b> <i> .menu_type。</i>菜單_ext.php </b>。該文件指定了定義菜單項的一系列參數。";
