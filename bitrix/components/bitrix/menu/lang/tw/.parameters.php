<?php
$MESS["CHILD_MENU_TYPE_NAME"] = "兒童級別的菜單類型";
$MESS["CP_BM_MENU_CACHE_GET_VARS"] = "重要的查詢變量";
$MESS["CP_BM_MENU_CACHE_USE_GROUPS"] = "尊重訪問權限";
$MESS["DELAY_NAME"] = "延遲構建菜單模板";
$MESS["FILEMAN_OPTION_LEFT_MENU_NAME"] = "左菜單";
$MESS["FILEMAN_OPTION_TOP_MENU_NAME"] = "上菜單";
$MESS["MAIN_MENU_TYPE_NAME"] = "根級菜單類型";
$MESS["MAX_LEVEL_NAME"] = "菜單深度級別";
$MESS["USE_EXT_NAME"] = "使用文件.menu-type.menu_ext.php進行菜單";
$MESS["comp_menu_allow_multi_select"] = "允許將幾個菜單項突出顯示為活動";
