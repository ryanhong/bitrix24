<?php
$MESS["RATING_COMPONENT_DESC"] = "投票 -％s（％s ayes和％s noes）";
$MESS["RATING_COMPONENT_NO_VOTES"] = "沒有投票";
$MESS["RATING_TEXT_A"] = "你喜歡它？";
$MESS["RATING_TEXT_D"] = "喜歡這樣的人：";
$MESS["RATING_TEXT_LIKE_D"] = "喜歡";
$MESS["RATING_TEXT_LIKE_N"] = "與眾不同";
$MESS["RATING_TEXT_LIKE_Y"] = "喜歡";
$MESS["RATING_TEXT_N"] = "不";
$MESS["RATING_TEXT_Y"] = "是的";
