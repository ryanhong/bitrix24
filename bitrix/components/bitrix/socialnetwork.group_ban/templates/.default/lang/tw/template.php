<?php
$MESS["SONET_C7_ACT_IN_BAN"] = "禁止";
$MESS["SONET_C7_ACT_SAVE"] = "UNBAN用戶";
$MESS["SONET_C7_NO_USERS"] = "沒有成員。";
$MESS["SONET_C7_NO_USERS_DESCR"] = "顯示該小組的禁止成員。";
$MESS["SONET_C7_SUBTITLE"] = "團體禁令列表";
