<?php
$MESS["SONET_C7_ERR_SELECT"] = "沒有選擇用戶。";
$MESS["SONET_C7_NAV"] = "用戶";
$MESS["SONET_C7_NO_PERMS"] = "您無權查看此組。";
$MESS["SONET_C7_NO_PMOD"] = "您無權查看此組的禁令列表。";
$MESS["SONET_C7_TITLE"] = "團體禁令列表";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_USER_NO_GROUP"] = "找不到小組。";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶。";
