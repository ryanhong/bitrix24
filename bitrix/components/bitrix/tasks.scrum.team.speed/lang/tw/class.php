<?php
$MESS["TASKS_SCRUM_TEAM_SPEED_ACCESS_DENIED"] = "您無權查看此內容。";
$MESS["TASKS_SCRUM_TEAM_SPEED_FILTER_LAST"] = "最近的";
$MESS["TASKS_SCRUM_TEAM_SPEED_FILTER_LAST_FIELD_LABEL"] = "n最後一個項目";
$MESS["TASKS_SCRUM_TEAM_SPEED_FILTER_LAST_LABEL"] = "衝刺";
$MESS["TASKS_SCRUM_TEAM_SPEED_FILTER_PERIOD"] = "日期範圍";
$MESS["TASKS_SCRUM_TEAM_SPEED_FILTER_PERIOD_LAST_3"] = "最後3次沖刺";
$MESS["TASKS_SCRUM_TEAM_SPEED_FILTER_PERIOD_LAST_10"] = "最後10個衝刺";
$MESS["TASKS_SCRUM_TEAM_SPEED_FILTER_PERIOD_LAST_15"] = "最後15次沖刺";
$MESS["TASKS_SCRUM_TEAM_SPEED_FILTER_PERIOD_LAST_30"] = "最近30天";
$MESS["TASKS_SCRUM_TEAM_SPEED_FILTER_PRESET_LAST"] = "最近的衝刺";
$MESS["TASKS_SCRUM_TEAM_SPEED_TITLE"] = "團隊速度圖";
