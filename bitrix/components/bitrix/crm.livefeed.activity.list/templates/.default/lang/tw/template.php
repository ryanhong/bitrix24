<?php
$MESS["CRM_ENTITY_LF_ACTIVITY_CLIENT_INFO"] = "與＃客戶＃";
$MESS["CRM_ENTITY_LF_ACTIVITY_LIST_TITLE"] = "目前活動";
$MESS["CRM_ENTITY_LF_ACTIVITY_REFERENCE_INFO"] = "關於＃參考＃";
$MESS["CRM_ENTITY_LF_ACTIVITY_TYPE_CALL_INCOMING"] = "來自客戶的電話";
$MESS["CRM_ENTITY_LF_ACTIVITY_TYPE_CALL_OUTGOING"] = "打電話給客戶";
$MESS["CRM_ENTITY_LF_ACTIVITY_TYPE_EMAIL_INCOMING"] = "來自客戶的電子郵件";
$MESS["CRM_ENTITY_LF_ACTIVITY_TYPE_EMAIL_OUTGOING"] = "電子郵件給客戶";
$MESS["CRM_ENTITY_LF_ACTIVITY_TYPE_MEETING"] = "與客戶會面";
$MESS["CRM_ENTITY_LF_ACTIVITY_TYPE_TASK"] = "任務";
