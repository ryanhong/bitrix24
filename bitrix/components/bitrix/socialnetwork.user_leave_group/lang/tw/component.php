<?php
$MESS["SONET_C37_IS_AUTO_MEMBER"] = "通過其部門建立聯繫的小組成員無法離開小組。";
$MESS["SONET_C37_IS_OWNER"] = "小組所有者不能離開小組。";
$MESS["SONET_C37_NOT_MEMBER"] = "您不是這個小組的成員。";
$MESS["SONET_C37_PAGE_TITLE"] = "離開組";
$MESS["SONET_C37_PAGE_TITLE_PROJECT"] = "離開項目";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_USER_NO_GROUP"] = "找不到小組。";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶。";
