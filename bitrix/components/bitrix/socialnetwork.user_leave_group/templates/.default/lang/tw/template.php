<?php
$MESS["SONET_C37_T_CANCEL"] = "取消";
$MESS["SONET_C37_T_CANCEL_PROJECT"] = "留在項目中";
$MESS["SONET_C37_T_PROMT"] = "您確定要離開這個小組嗎？";
$MESS["SONET_C37_T_PROMT_PROJECT"] = "您確定要離開這個項目嗎？";
$MESS["SONET_C37_T_SAVE"] = "離開組";
$MESS["SONET_C37_T_SAVE_PROJECT"] = "離開項目";
$MESS["SONET_C37_T_SUCCESS"] = "你離開了小組。";
