<?php
$MESS["SALE_ACCESS_DENIED"] = "您必須授權查看訂單";
$MESS["SALE_ERROR_EDIT_PROF"] = "錯誤更新配置文件。";
$MESS["SALE_MODULE_NOT_INSTALL"] = "E商店模塊未安裝";
$MESS["SALE_NO_FIELD"] = "該字段是空的：";
$MESS["SALE_NO_NAME"] = "配置文件名稱未指定。";
$MESS["SALE_NO_PROFILE"] = "找不到配置文件";
$MESS["SPPD_PROFILE_NO"] = "個人資料編號＃ID＃";
$MESS["SPPD_TITLE"] = "修改配置文件＃";
