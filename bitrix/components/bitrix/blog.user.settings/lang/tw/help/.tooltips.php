<?php
$MESS["BLOG_URL_TIP"] = "指定通過博客ID傳遞的代碼。";
$MESS["BLOG_VAR_TIP"] = "在此處指定通過博客ID傳遞的變量的名稱。";
$MESS["PAGE_VAR_TIP"] = "在此處指定博客頁面通過的變量的名稱。";
$MESS["PATH_TO_USER_SETTINGS_EDIT_TIP"] = "用戶設置編輯頁面的路徑。示例：<Nobr> blog_u_set_edit.php？page = user_settings_edit＆blog =＃blog＃＆user_id =＃user_id＃。</nobr>";
$MESS["PATH_TO_USER_TIP"] = "用戶配置文件頁面的路徑。示例：<Nobr> blog_user.php？page = user＆user_id =＃user_id＃。</nobr>";
$MESS["SET_TITLE_TIP"] = "如果該選項處於活動狀態，則頁面標題將設置為</b> <i> \“ blog name \” </i>。</i>。</i>。</i>。";
$MESS["USER_VAR_TIP"] = "在此處指定通過博客用戶ID傳遞的變量的名稱。";
