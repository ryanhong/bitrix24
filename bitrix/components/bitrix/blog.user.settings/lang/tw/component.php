<?php
$MESS["BLOG_BLOG_ADD_F_POS_ADDED"] = "用戶“＃名稱＃”已添加到社區會員申請人名單中";
$MESS["BLOG_BLOG_ADD_F_POS_ADD_ERROR"] = "錯誤添加用戶“＃名稱＃”到社區成員申請人名單";
$MESS["BLOG_BLOG_ADD_F_POS_ALREADY_WANT"] = "用戶“＃名稱＃”已經在社區會員申請人名單中";
$MESS["BLOG_BLOG_ADD_F_POS_NOT_FOUND"] = "訪客“＃名稱＃”沒找到";
$MESS["BLOG_BLOG_SESSID_WRONG"] = "您的會議已經過期。請再試一次。";
$MESS["BLOG_MODULE_NOT_INSTALL"] = "沒有安裝博客模塊。";
$MESS["B_B_US_DELETE_OK"] = "朋友被成功刪除";
$MESS["B_B_US_NO_BLOG"] = "沒有找到博客";
$MESS["B_B_US_NO_RIGHT"] = "您無權管理此博客";
$MESS["B_B_US_TITLE"] = "用戶";
$MESS["B_B_US_TITLE_BLOG"] = "博客的用戶“＃名稱＃”";
