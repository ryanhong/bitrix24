<?php
$MESS["SONET_GCE_T_AJAX_ERROR"] = "錯誤處理形式數據。請再試一次。";
$MESS["SONET_GCE_T_STRING_FIELD_ERROR"] = "需要字段。";
$MESS["SONET_GCE_T_SUCCESS_CREATE"] = "該小組已成功創建。";
$MESS["SONET_GCE_T_SUCCESS_EDIT"] = "組參數已成功更改。";
$MESS["SONET_GCE_T_WIZARD_DESCRIPTION"] = "與其他用戶合作，並使用可用的工具，以最大程度地結果。";
$MESS["SONET_GCE_T_WIZARD_TITLE"] = "管理您的團隊";
