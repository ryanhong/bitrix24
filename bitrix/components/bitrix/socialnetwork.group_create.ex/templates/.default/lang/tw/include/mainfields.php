<?php
$MESS["SONET_GCE_T_DESCRIPTION"] = "工作組描述";
$MESS["SONET_GCE_T_DESCRIPTION_LABEL"] = "添加工作組描述";
$MESS["SONET_GCE_T_DESCRIPTION_LABEL_PROJECT"] = "為團隊成員添加項目描述";
$MESS["SONET_GCE_T_DESCRIPTION_LABEL_SCRUM"] = "告訴您的團隊成員有關Scrum團隊的信息";
$MESS["SONET_GCE_T_DESCRIPTION_PROJECT"] = "項目介紹";
$MESS["SONET_GCE_T_DESCRIPTION_SCRUM"] = "Scrum團隊描述";
$MESS["SONET_GCE_T_DESCRIPTION_SWITCHER"] = "添加描述";
$MESS["SONET_GCE_T_NAME3"] = "工作組名稱";
$MESS["SONET_GCE_T_NAME3_PROJECT"] = "項目名";
$MESS["SONET_GCE_T_NAME3_SCRUM"] = "Scrum團隊名稱";
