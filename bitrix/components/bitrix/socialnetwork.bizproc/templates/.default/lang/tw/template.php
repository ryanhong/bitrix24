<?php
$MESS["SONET_BP_ACTION"] = "行動";
$MESS["SONET_BP_DATE"] = "日期";
$MESS["SONET_BP_DESCR"] = "描述";
$MESS["SONET_BP_DOC"] = "文件";
$MESS["SONET_BP_EDIT_TASK"] = "執行";
$MESS["SONET_BP_EPMTY_TASKS"] = "沒有執行任務。";
$MESS["SONET_BP_EVENT"] = "事件";
$MESS["SONET_BP_NAME"] = "姓名";
$MESS["SONET_BP_TASKS"] = "當前任務";
$MESS["SONET_BP_TRACKING"] = "任務存檔";
$MESS["SONET_BP_VIEW_DOCUMENT"] = "查看文檔";
$MESS["SONET_BP_VIEW_TASK"] = "執行任務";
