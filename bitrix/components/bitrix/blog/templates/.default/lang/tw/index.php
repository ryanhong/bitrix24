<?php
$MESS["BC_ALL_BLOGS"] = "查看所有博客";
$MESS["BC_ALL_POSTS"] = "查看所有帖子";
$MESS["BC_COMMENTED_POSTS"] = "大多數評論";
$MESS["BC_COMMENTED_POSTS_MES"] = "大多數評論的帖子";
$MESS["BC_GROUPS"] = "博客小組";
$MESS["BC_NEW_BLOGS"] = "新的";
$MESS["BC_NEW_BLOGS_MES"] = "新博客";
$MESS["BC_NEW_COMMENTS"] = "最近的評論";
$MESS["BC_NEW_POSTS"] = "新的";
$MESS["BC_NEW_POSTS_MES"] = "新帖子";
$MESS["BC_POPULAR_BLOGS"] = "受歡迎的";
$MESS["BC_POPULAR_BLOGS_MES"] = "受歡迎的博客";
$MESS["BC_POPULAR_POSTS"] = "受歡迎的";
$MESS["BC_POPULAR_POSTS_MES"] = "受歡迎的帖子";
$MESS["BC_SEARCH_TAG"] = "標籤雲";
$MESS["BLOG_CREATE_BLOG"] = "創建您的博客";
$MESS["BLOG_TITLE"] = "部落格";
