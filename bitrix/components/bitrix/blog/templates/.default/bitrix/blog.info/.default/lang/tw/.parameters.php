<?php
$MESS["BLOG_FONT_MAX"] = "最大限度。文字大小（PX）";
$MESS["BLOG_FONT_MIN"] = "最小。文字大小（PX）";
$MESS["BLOG_WIDTH"] = "標籤雲寬度（示例：\“ 100％\”，\“ 100px \”，\“ 100pt \”或\ \“ 100in \”）";
