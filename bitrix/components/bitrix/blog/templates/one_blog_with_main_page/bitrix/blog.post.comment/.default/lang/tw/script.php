<?php
$MESS["BLOG_CATEGORY_NAME"] = "分類名稱";
$MESS["BPC_ERROR_NO_TITLE"] = "請輸入標題";
$MESS["BPC_ERROR_NO_URL"] = "請輸入地址（URL）";
$MESS["BPC_LIST_PROMPT"] = "輸入列表項。單擊“取消”或輸入空間以完成列表";
$MESS["BPC_TEXT_ENTER_IMAGE"] = "輸入完整的圖像地址（URL）";
$MESS["BPC_TEXT_ENTER_URL"] = "輸入完整地址（URL）";
$MESS["BPC_TEXT_ENTER_URL_NAME"] = "輸入站點名稱";
