<?php
$MESS["REST_HEVE_ADD_TITLE"] = "添加出站Webhook";
$MESS["REST_HEVE_EDIT_TITLE"] = "編輯出站Webhook";
$MESS["REST_HEVE_ERROR_EVENT_HANDLER"] = "不正確的處理程序地址";
$MESS["REST_HEVE_ERROR_EVENT_NAME"] = "事件未指定。";
$MESS["REST_HEVE_NOT_FOUND"] = "沒有找到活動處理程序。";
$MESS["REST_HEVE_TITLE_DEFAULT"] = "活動處理程序";
