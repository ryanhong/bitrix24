<?php
$MESS["IM_COMPONENT_DEFAULT_OG_TITLE"] = "視訊會議";
$MESS["IM_COMPONENT_MODULE_IM_NOT_INSTALLED"] = "即時通訊模塊未安裝。";
$MESS["IM_COMPONENT_OG_DESCRIPTION"] = "高清質量，親自見面的下一個最好的事情。";
$MESS["IM_COMPONENT_OG_DESCRIPTION_2"] = "加入沒有任何設備的註冊。 24個用戶免費。";
$MESS["IM_COMPONENT_OG_TITLE"] = "使用視頻電話使您的業務更加高效！";
$MESS["IM_COMPONENT_OG_TITLE_2"] = "免費Bitrix24高清視頻會議";
