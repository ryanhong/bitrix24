<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>不緩存</i>：沒有執行緩存。";
$MESS["PAGE_TIP"] = "指定訂閱編輯器路徑（默認值是：<b>＃site_dir＃about/subscr_edit.php </b>）";
$MESS["SET_TITLE_TIP"] = "檢查此選項將頁面標題設置為<b>訂閱</b>。";
$MESS["SHOW_COUNT_TIP"] = "指定在公共部分中顯示訂戶數量。";
$MESS["SHOW_HIDDEN_TIP"] = "指定在訂閱編輯器中顯示所有活動的主題。";
