<?php
$MESS["SUBSCR_ADDR"] = "電子郵件";
$MESS["SUBSCR_BUTTON"] = "訂閱";
$MESS["SUBSCR_CNT"] = "訂戶";
$MESS["SUBSCR_DESC"] = "描述";
$MESS["SUBSCR_EDIT_BUTTON"] = "進入";
$MESS["SUBSCR_EDIT_NOTE"] = "如果您已經訂閱了我們的新聞通訊，則可以修改您的設置 - 只需在需要時輸入電子郵件和密碼。";
$MESS["SUBSCR_EDIT_PASS"] = "密碼";
$MESS["SUBSCR_EDIT_PASS_ENTERED"] = "輸入密碼";
$MESS["SUBSCR_EDIT_PASS_TITLE"] = "輸入密碼（確認代碼）";
$MESS["SUBSCR_EDIT_TITLE"] = "修改設置";
$MESS["SUBSCR_EMAIL_TITLE"] = "輸入你的電子郵箱";
$MESS["SUBSCR_NAME"] = "新聞通訊類別";
$MESS["SUBSCR_NEW_NOTE"] = "在這裡，您可以從我們網站上訂閱新聞通訊。
選擇訂閱類別，輸入您的電子郵件，然後單擊<i> subscribe </i>按鈕。";
$MESS["SUBSCR_NEW_TITLE"] = "新的訂戶地址";
$MESS["SUBSCR_NOTE"] = "對於未註冊的訂戶，確認代碼將用作密碼。註冊用戶可以使用註冊時指定的密碼。";
$MESS["SUBSCR_PASS_BUTTON"] = "發送";
$MESS["SUBSCR_PASS_NOTE"] = "輸入您用於接收我們的新聞通訊的電子郵件，並將立即發送給您。";
$MESS["SUBSCR_PASS_TITLE"] = "如果您忘記了密碼";
$MESS["SUBSCR_UNSUBSCRIBE_NOTE"] = "如果您想退訂，請轉到訂閱設置，然後單擊<i> unsubscribe </i>。";
$MESS["SUBSCR_UNSUBSCRIBE_TITLE"] = "退訂";
