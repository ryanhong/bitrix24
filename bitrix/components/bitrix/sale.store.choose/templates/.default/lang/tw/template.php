<?php
$MESS["SALE_SSC_ADD_INFO"] = "其他倉庫信息";
$MESS["SALE_SSC_ADRES"] = "地址";
$MESS["SALE_SSC_CHANGE"] = "編輯";
$MESS["SALE_SSC_DESC"] = "描述";
$MESS["SALE_SSC_DIALOG_CLOSE"] = "關閉";
$MESS["SALE_SSC_EMAIL"] = "電子郵件";
$MESS["SALE_SSC_GOOGLE_MAP_INFO"] = "注意力！要使用Google Maps：<OL> <li>獲取Google API密鑰並分配適當的權限。<br>（在此處獲取更多詳細信息：＃A1＃）<li>將此鍵保存在＃A2＃站點Explorer Module Module設置上＃a3＃。</li> </ol>";
$MESS["SALE_SSC_GOOGLE_MAP_INFO_TITLE"] = "資訊";
$MESS["SALE_SSC_MAP_TYPE_CHANGE_ERROR"] = "錯誤更改地圖類型";
$MESS["SALE_SSC_PHONE"] = "電話";
$MESS["SALE_SSC_STORE"] = "倉庫";
$MESS["SALE_SSC_STORE_EXPORT"] = "接送地址";
$MESS["SALE_SSC_WORK"] = "營業時間";
