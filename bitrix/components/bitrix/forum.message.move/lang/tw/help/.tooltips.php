<?php
$MESS["CACHE_TIME_TIP"] = "緩存壽命（秒）";
$MESS["CACHE_TYPE_TIP"] = "緩存類型";
$MESS["DATE_FORMAT_TIP"] = "日期格式";
$MESS["DATE_TIME_FORMAT_TIP"] = "日期和時間格式";
$MESS["FID_TIP"] = "論壇ID";
$MESS["IMAGE_SIZE_TIP"] = "附著的圖像尺寸（PX）";
$MESS["MID_TIP"] = "發帖ID";
$MESS["SET_NAVIGATION_TIP"] = "顯示導航控件";
$MESS["SET_TITLE_TIP"] = "設置頁面標題";
$MESS["TID_TIP"] = "主題ID";
$MESS["URL_TEMPLATES_INDEX_TIP"] = "論壇頁";
$MESS["URL_TEMPLATES_LIST_TIP"] = "主題頁";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = "用戶配置文件URL模板";
$MESS["URL_TEMPLATES_READ_TIP"] = "主題查看頁面";
$MESS["URL_TEMPLATES_TOPIC_SEARCH_TIP"] = "論壇主題搜索頁面";
$MESS["WORD_LENGTH_TIP"] = "單詞長度";
