<?php
$MESS["VOTE_ACCESS_DENIED"] = "該民意調查的訪問被拒絕。";
$MESS["VOTE_EMPTY"] = "未找到民意調查。";
$MESS["VOTE_ERROR"] = "投票錯誤。";
$MESS["VOTE_MODULE_IS_NOT_INSTALLED"] = "未安裝民意調查模塊。";
$MESS["VOTE_OK"] = "感謝你的投票。";
