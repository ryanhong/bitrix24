<?php
$MESS["BLOG_VAR_TIP"] = "在此處指定通過博客ID傳遞的變量的名稱。";
$MESS["ID_TIP"] = "指定將傳遞用戶ID的代碼。";
$MESS["PAGE_VAR_TIP"] = "在此處指定博客頁面通過的變量的名稱。";
$MESS["PATH_TO_BLOG_TIP"] = "主要博客頁面的路徑。示例：blog_blog.php？page = blog＆blog =＃blog＃。";
$MESS["PATH_TO_SEARCH_TIP"] = "搜索頁面的路徑。示例：blog_search.php。";
$MESS["PATH_TO_USER_EDIT_TIP"] = "用戶配置文件編輯頁面的路徑。示例：<Nobr> blog_user_edit.php？page = user＆user_id =＃user_id＃＆mode = edit。</nobr>";
$MESS["PATH_TO_USER_TIP"] = "用戶配置文件頁面的路徑。示例：<Nobr> blog_user.php？page = user＆user_id =＃user_id＃。</nobr>";
$MESS["SET_TITLE_TIP"] = "如果該選項處於活動狀態，則頁面標題將設置為<nobr> <b> </b> <i> \“用戶名\” </i>。</i>。</nobr>";
$MESS["USER_PROPERTY_TIP"] = "在此處選擇將在用戶配置文件中顯示的其他屬性。";
$MESS["USER_VAR_TIP"] = "在此處指定通過博客用戶ID傳遞的變量的名稱。";
