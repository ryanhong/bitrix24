<?php
$MESS["BC_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["BU_BLOG_VAR"] = "博客標識符變量";
$MESS["BU_ID"] = "用戶身份";
$MESS["BU_PAGE_VAR"] = "頁面變量";
$MESS["BU_PATH_TO_BLOG"] = "博客頁面路徑模板";
$MESS["BU_PATH_TO_SEARCH"] = "搜索頁面路徑模板";
$MESS["BU_PATH_TO_USER"] = "用戶頁面路徑模板";
$MESS["BU_PATH_TO_USER_EDIT"] = "用戶編輯頁面路徑的模板";
$MESS["BU_USER_VAR"] = "用戶ID變量";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
$MESS["USER_PROPERTY"] = "顯示其他屬性";
