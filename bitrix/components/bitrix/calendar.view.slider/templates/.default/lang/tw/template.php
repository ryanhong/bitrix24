<?php
$MESS["EC_CRM_TITLE"] = "CRM項目";
$MESS["EC_VIEW_ATTENDEES_ADD"] = "邀請其他人";
$MESS["EC_VIEW_ATTENDEES_TITLE"] = "與會者";
$MESS["EC_VIEW_CREATED_BY"] = "由...製作";
$MESS["EC_VIEW_HOST"] = "組織者";
$MESS["EC_VIEW_REMINDERS"] = "提醒";
$MESS["EC_VIEW_REMINDER_ADD"] = "提醒";
$MESS["EC_VIEW_SECTION"] = "日曆";
$MESS["EC_VIEW_SLIDER_ATTENDEES"] = "與會者";
$MESS["EC_VIEW_SLIDER_CANCEL_BUTTON"] = "取消";
$MESS["EC_VIEW_SLIDER_COMMENTS"] = "評論";
$MESS["EC_VIEW_SLIDER_COPY_LINK"] = "複製事件鏈接到剪貼板";
$MESS["EC_VIEW_SLIDER_DEL"] = "刪除";
$MESS["EC_VIEW_SLIDER_EDIT"] = "編輯";
$MESS["EC_VIEW_SLIDER_EVENT_NOT_FOUND"] = "未找到或無法顯示事件。";
$MESS["EC_VIEW_SLIDER_IMPORTANT_EVENT"] = "這個事件很重要";
$MESS["EC_VIEW_SLIDER_LOCATION"] = "地點";
$MESS["EC_VIEW_SLIDER_SAVE_EVENT_BUTTON"] = "節省";
$MESS["EC_VIEW_STATUS_TITLE_I"] = "不確定";
$MESS["EC_VIEW_STATUS_TITLE_N"] = "拒絕";
$MESS["EC_VIEW_STATUS_TITLE_Q"] = "被邀請";
$MESS["EC_VIEW_STATUS_TITLE_Y"] = "參加";
