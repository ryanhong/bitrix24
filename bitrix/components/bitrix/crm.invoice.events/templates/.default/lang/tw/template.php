<?php
$MESS["CRM_ALL"] = "全部的";
$MESS["CRM_EVENT_DELETE"] = "刪除";
$MESS["CRM_EVENT_DELETE_CONFIRM"] = "你確定你要刪除嗎？";
$MESS["CRM_EVENT_DELETE_TITLE"] = "刪除事件";
$MESS["CRM_EVENT_ENTITY_TYPE_COMPANY"] = "公司";
$MESS["CRM_EVENT_ENTITY_TYPE_CONTACT"] = "接觸";
$MESS["CRM_EVENT_ENTITY_TYPE_DEAL"] = "交易";
$MESS["CRM_EVENT_ENTITY_TYPE_LEAD"] = "帶領";
$MESS["CRM_EVENT_TABLE_EMPTY"] = "- 沒有數據 -";
$MESS["CRM_EVENT_TABLE_FILES"] = "文件";
$MESS["CRM_EVENT_VIEW_ADD"] = "添加事件";
$MESS["CRM_EVENT_VIEW_ADD_SHORT"] = "事件";
$MESS["CRM_EVENT_VIEW_SHOW_FILTER"] = "顯示/隱藏過濾器";
$MESS["CRM_EVENT_VIEW_SHOW_FILTER_SHORT"] = "篩選";
$MESS["CRM_IMPORT_EVENT"] = "如果CRM記錄中指定了聯繫人的電子郵件，則可以自動將所有相關的電子郵件信件作為事件記錄保存。您需要將接收到的消息轉發到系統的電子郵件地址<b>％電子郵件％</b>，並且將作為此聯繫人的事件添加所有文本和附件的文件。";
