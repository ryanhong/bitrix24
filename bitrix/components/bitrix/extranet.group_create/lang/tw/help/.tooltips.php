<?php
$MESS["ID_TIP"] = "指定與用戶ID相對應的代碼。";
$MESS["PAGE_VAR_TIP"] = "指定社交網絡頁面將通過此處傳遞的變量的名稱。";
$MESS["PATH_TO_USER_EDIT_TIP"] = "用戶配置文件編輯器頁面的路徑。示例：sonet_user_edit.php？page = user＆user_id =＃user_id＃＆mode =編輯。";
$MESS["PATH_TO_USER_TIP"] = "用戶配置文件頁面的路徑。示例：sonet_user.php？page = user＆user_id =＃user_id＃。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將將頁面標題設置為<i> \“用戶名\” </i> <b>用戶配置文件</b>。";
$MESS["USER_PROPERTY_TIP"] = "選擇將在此處的用戶配置文件中顯示的其他屬性。";
$MESS["USER_VAR_TIP"] = "指定將在此處傳遞社交網絡用戶ID的變量的名稱。";
