<?php
$MESS["ME_MEETING_ACCESS_DENIED"] = "拒絕訪問";
$MESS["ME_MEETING_ADD"] = "創建新會議";
$MESS["ME_MEETING_COPY"] = "創建另一個會議";
$MESS["ME_MEETING_EDIT"] = "會議編號＃id＃on＃date＃＃";
$MESS["ME_MEETING_EDIT_NO_DATE"] = "會議號碼＃ID＃";
$MESS["ME_MEETING_NOT_FOUND"] = "沒有發現會議。";
$MESS["ME_MEETING_TITLE_DEFAULT"] = "（無標題）";
$MESS["ME_MEETING_TITLE_DEFAULT_1"] = "話題";
$MESS["ME_MODULE_NOT_INSTALLED"] = "\“會議和事件\”模塊未安裝。";
