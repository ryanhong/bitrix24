<?php
$MESS["SONET_C25_T_BAN"] = "禁止";
$MESS["SONET_C25_T_EDIT_MOD"] = "編輯主持人";
$MESS["SONET_C25_T_EMPTY"] = "沒有成員。<br>此列表顯示了小組成員。";
$MESS["SONET_C25_T_EXCLUDE"] = "從組中刪除";
$MESS["SONET_C25_T_INVITE"] = "邀請小組";
$MESS["SONET_C25_T_MEMBERS"] = "小組成員";
$MESS["SONET_C25_T_ONLINE"] = "在線的";
$MESS["SONET_C25_T_OWNER"] = "更改所有者";
$MESS["SONET_C25_T_SAVE"] = "設置為主持人";
$MESS["SONET_C100_MODERATOR"] = "主持人";
