<?php
$MESS["DOCGEN_TEMPLATE_ADD_ACTIVE"] = "積極的";
$MESS["DOCGEN_TEMPLATE_ADD_ADD_USER_LINK"] = "添加更多";
$MESS["DOCGEN_TEMPLATE_ADD_CANCEL"] = "取消";
$MESS["DOCGEN_TEMPLATE_ADD_COMPLETE"] = "文件已上傳";
$MESS["DOCGEN_TEMPLATE_ADD_DOWNLOAD"] = "下載模板";
$MESS["DOCGEN_TEMPLATE_ADD_DRAG_HERE"] = "在此處拖動文件或<span class = \“ docs-template-load-load-drag-line \” id = \“ upload-template-text \”>從您的計算機</span>中選擇一個";
$MESS["DOCGEN_TEMPLATE_ADD_ERROR_FILE"] = "請僅使用DOCX文件作為模板";
$MESS["DOCGEN_TEMPLATE_ADD_ERROR_NAME"] = "請輸入模板名稱";
$MESS["DOCGEN_TEMPLATE_ADD_ERROR_PROVIDER"] = "請選擇至少一個部分";
$MESS["DOCGEN_TEMPLATE_ADD_FEEDBACK"] = "回饋";
$MESS["DOCGEN_TEMPLATE_ADD_FILE"] = "上傳模板文件";
$MESS["DOCGEN_TEMPLATE_ADD_FORMATS"] = "文檔模板必須採用DOCX格式。";
$MESS["DOCGEN_TEMPLATE_ADD_INSTALL"] = "安裝";
$MESS["DOCGEN_TEMPLATE_ADD_MAIN_PROVIDER"] = "與CRM截面結合";
$MESS["DOCGEN_TEMPLATE_ADD_NAME"] = "姓名";
$MESS["DOCGEN_TEMPLATE_ADD_PRODUCTS_TABLE_VARIANT"] = "產品與服務";
$MESS["DOCGEN_TEMPLATE_ADD_PRODUCTS_TABLE_VARIANT_GOODS"] = "產品";
$MESS["DOCGEN_TEMPLATE_ADD_PRODUCTS_TABLE_VARIANT_SERVICE"] = "服務";
$MESS["DOCGEN_TEMPLATE_ADD_PRODUCTS_TABLE_VARIANT_TITLE"] = "項目列表";
$MESS["DOCGEN_TEMPLATE_ADD_PROGRESS"] = "現在上傳...";
$MESS["DOCGEN_TEMPLATE_ADD_REINSTALL"] = "還原默認模板版本";
$MESS["DOCGEN_TEMPLATE_ADD_REINSTALL_CONFIRM"] = "您確定要用默認模板替換文件嗎？";
$MESS["DOCGEN_TEMPLATE_ADD_REINSTALL_CONFIRM_TITLE"] = "還原模板";
$MESS["DOCGEN_TEMPLATE_ADD_REINSTALL_SUCCESS"] = "模板文件已更新。";
$MESS["DOCGEN_TEMPLATE_ADD_SAVE"] = "節省";
$MESS["DOCGEN_TEMPLATE_ADD_SORT"] = "種類";
$MESS["DOCGEN_TEMPLATE_ADD_SUCCESS"] = "模板已上傳";
$MESS["DOCGEN_TEMPLATE_ADD_TEMPLATE_NUMERATOR"] = "使用自動編號模板";
$MESS["DOCGEN_TEMPLATE_ADD_TEMPLATE_NUMERATOR_CREATE"] = "創造";
$MESS["DOCGEN_TEMPLATE_ADD_TEMPLATE_NUMERATOR_EDIT"] = "配置";
$MESS["DOCGEN_TEMPLATE_ADD_TEMPLATE_REGION"] = "與國家約束";
$MESS["DOCGEN_TEMPLATE_ADD_UPLOAD_NEW"] = "上傳新";
$MESS["DOCGEN_TEMPLATE_ADD_USERS"] = "模板用戶";
$MESS["DOCGEN_TEMPLATE_ADD_WITH_STAMPS"] = "帶有簽名和郵票";
