<?php
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "電子郵件地址不正確。";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_1"] = "請加入我們的Bitrix24帳戶。在這個地方，每個人都可以在任務和項目上進行溝通，協作，管理客戶並做更多的事情。";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "受邀人員的人數超出了您的許可條款。";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR"] = "具有這些電子郵件地址的用戶已經存在。";
