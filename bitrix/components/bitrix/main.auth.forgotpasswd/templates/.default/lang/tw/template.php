<?php
$MESS["MAIN_AUTH_PWD_FIELD_CAPTCHA"] = "輸入您在圖片上看到的字符";
$MESS["MAIN_AUTH_PWD_FIELD_EMAIL"] = "電子郵件";
$MESS["MAIN_AUTH_PWD_FIELD_LOGIN"] = "登入";
$MESS["MAIN_AUTH_PWD_FIELD_SUBMIT"] = "發送";
$MESS["MAIN_AUTH_PWD_HEADER"] = "發送總成本";
$MESS["MAIN_AUTH_PWD_NOTE"] = "如果忘記了密碼，請輸入您的登錄名或電子郵件。您的註冊數據和重置密碼的總結將發送到您的電子郵件。";
$MESS["MAIN_AUTH_PWD_OR"] = "或者";
$MESS["MAIN_AUTH_PWD_SUCCESS"] = "您已註冊並成功登錄。";
$MESS["MAIN_AUTH_PWD_URL_AUTH_URL"] = "登入";
$MESS["MAIN_AUTH_PWD_URL_REGISTER_URL"] = "登記";
