<?php
$MESS["DOWN_TEXT"] = "釋放刷新";
$MESS["LOAD_TEXT"] = "更新菜單...";
$MESS["MB_ASK_SYNC_CARDDAV"] = "您想將Bitrix24與手機的聯繫人同步嗎？";
$MESS["MB_ASK_SYNC_NO"] = "不";
$MESS["MB_ASK_SYNC_YES"] = "是的";
$MESS["MB_BP_MAIN_MENU_ITEM"] = "工作流程";
$MESS["MB_CALENDAR_LIST"] = "日曆";
$MESS["MB_CHAT_AND_CALLS"] = "聊天和電話";
$MESS["MB_COMPANY"] = "僱員";
$MESS["MB_CONTACTS"] = "聯繫人";
$MESS["MB_COPY_LINK_COPIED_MESSAGE"] = "鏈接已復製到剪貼板";
$MESS["MB_COPY_PUBLIC_LINK"] = "複製公共鏈接";
$MESS["MB_CRM_ACTIVITY"] = "我的活動";
$MESS["MB_CRM_COMPANY"] = "公司";
$MESS["MB_CRM_CONTACT"] = "聯繫人";
$MESS["MB_CRM_DEAL"] = "交易";
$MESS["MB_CRM_INVOICE"] = "發票";
$MESS["MB_CRM_LEAD"] = "鉛";
$MESS["MB_CRM_PRODUCT"] = "產品";
$MESS["MB_CRM_QUOTE"] = "引號";
$MESS["MB_CURRENT_USER_FILES_MAIN_MENU_ITEM"] = "我的開車";
$MESS["MB_CURRENT_USER_FILES_MAIN_MENU_ITEM_NEW"] = "我的開車";
$MESS["MB_EXIT"] = "登出";
$MESS["MB_HELP"] = "幫助";
$MESS["MB_LIVE_FEED"] = "活動流";
$MESS["MB_MARKETPLACE_GROUP_TITLE_2"] = "市場";
$MESS["MB_MESSAGES"] = "消息";
$MESS["MB_MY_BITRIX24"] = "我的bitrix24";
$MESS["MB_SEC_EXTRANET"] = "外部組";
$MESS["MB_SEC_FAVORITE"] = "我的工作區";
$MESS["MB_SEC_GROUPS"] = "組";
$MESS["MB_SHARED_FILES_MAIN_MENU_ITEM"] = "公司驅動器";
$MESS["MB_SHARED_FILES_MAIN_MENU_ITEM_NEW"] = "公司驅動器";
$MESS["MB_TASKS_MAIN_MENU_ITEM"] = "任務";
$MESS["PULL_TEXT"] = "下拉以刷新";
