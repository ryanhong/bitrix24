<?php
$MESS["CRM_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["CRM_ACTIVITY_COULD_NOT_DELETE"] = "該活動無法刪除。";
$MESS["CRM_ACTIVITY_EDIT_ACTION_DEFAULT_SUBJECT"] = "新活動（＃日期＃）";
$MESS["CRM_ACTIVITY_EDIT_EMAIL_ACTION_DEFAULT_SUBJECT"] = "新電子郵件（＃日期＃）";
$MESS["CRM_ACTIVITY_EDIT_EMAIL_EMPTY_FROM_FIELD"] = "請指定消息發送者。";
$MESS["CRM_ACTIVITY_EDIT_EMAIL_EMPTY_TO_FIELD"] = "請指定消息收件人。";
$MESS["CRM_ACTIVITY_EDIT_INCOMING_CALL_ACTION_DEFAULT_SUBJECT_EXT"] = "來電＃標題＃";
$MESS["CRM_ACTIVITY_EDIT_INVALID_EMAIL"] = "＃值＃'不是有效的電子郵件地址。";
$MESS["CRM_ACTIVITY_EDIT_MEETING_ACTION_DEFAULT_SUBJECT_EXT"] = "與＃標題＃會面";
$MESS["CRM_ACTIVITY_EDIT_OUTGOING_CALL_ACTION_DEFAULT_SUBJECT_EXT"] = "傳出電話＃標題＃";
$MESS["CRM_ACTIVITY_EDIT_TITLE_EMAIL_FROM"] = "從";
$MESS["CRM_ACTIVITY_EDIT_TITLE_EMAIL_SUBJECT"] = "主題";
$MESS["CRM_ACTIVITY_EDIT_TITLE_EMAIL_TO"] = "到";
$MESS["CRM_ACTIVITY_NOT_FOUND"] = "找不到活動## ID＃。";
$MESS["CRM_ACTIVITY_OWNER_ID_NOT_FOUND"] = "找不到所有者ID。";
$MESS["CRM_ACTIVITY_OWNER_NOT_FOUND"] = "無法確定對象。";
$MESS["CRM_ACTIVITY_OWNER_TYPE_NOT_FOUND"] = "找不到所有者類型。";
$MESS["CRM_ACTIVITY_SELECT_COMMUNICATION"] = "選擇聯繫人";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "type_not_supported：在當前上下文中不支持'＃entity_type＃'。";
