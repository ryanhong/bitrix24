<?php
$MESS["COMP_CLOCK_INIT_TIME"] = "開始時間（HH：MM）";
$MESS["COMP_CLOCK_INPUT_ID"] = "輸入字段ID";
$MESS["COMP_CLOCK_INPUT_NAME"] = "輸入字段名稱";
$MESS["COMP_CLOCK_INPUT_TITLE"] = "輸入字段工具提示";
$MESS["COMP_CLOCK_STEP"] = "最小步長以設置時間（分鐘）";
