<?php
$MESS["P_DELETE_ERROR"] = "錯誤刪除照片。";
$MESS["P_DENIED_ACCESS"] = "拒絕訪問。";
$MESS["P_SECTION_EMPTY_TO_MOVE"] = "目標專輯未指定。";
$MESS["P_SECTION_IS_NOT_IN_GALLERY"] = "該專輯不包含在指定的畫廊中。";
$MESS["P_SECTION_THIS_TO_MOVE"] = "不正確的目標專輯。";
