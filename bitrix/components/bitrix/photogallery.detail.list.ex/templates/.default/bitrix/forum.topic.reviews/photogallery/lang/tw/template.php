<?php
$MESS["ADD_COMMENT"] = "添加評論";
$MESS["ADD_COMMENT_TITLE"] = "添加評論（CTRL + Enter）";
$MESS["COMMENTS_N_FROM_M"] = "＃m＃的＃n＃";
$MESS["F_CAPTCHA_PROMT"] = "驗證圖像字符";
$MESS["F_CAPTCHA_TITLE"] = "垃圾郵件機器人保護（CAPTCHA）";
$MESS["JERROR_MAX_LEN"] = "最大消息長度為＃max_length＃符號。總符號：＃長度＃。";
$MESS["JERROR_NO_MESSAGE"] = "請提供您的消息。";
$MESS["JERROR_NO_TOPIC_NAME"] = "請提供消息標題。";
$MESS["JQOUTE_AUTHOR_WRITES"] = "寫";
$MESS["MORE_COMMENTS"] = "顯示其他評論";
$MESS["OPINIONS_EMAIL"] = "你的郵件";
$MESS["OPINIONS_NAME"] = "你的名字";
$MESS["REQUIRED_FIELD"] = "必填項目";
