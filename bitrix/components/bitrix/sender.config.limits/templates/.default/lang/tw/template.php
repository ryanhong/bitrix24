<?php
$MESS["SENDER_LIMIT"] = "限制";
$MESS["SENDER_LIMIT_AVAILABLE"] = "其餘的";
$MESS["SENDER_LIMIT_BTN_CLOSE"] = "關閉";
$MESS["SENDER_LIMIT_HELP"] = "有關限制的更多信息，請參閱此％link_start％Artist％link_end％link_end％。";
$MESS["SENDER_LIMIT_LEFT_MENU_ADDITIONAL_CONFIGURATION"] = "更多的";
$MESS["SENDER_LIMIT_LEFT_MENU_AUDIO_CALL_LIMIT"] = "傳出音頻通話";
$MESS["SENDER_LIMIT_LEFT_MENU_CALL_LIMIT"] = "呼出電話";
$MESS["SENDER_LIMIT_LEFT_MENU_MAIL_LIMIT"] = "傳出電子郵件";
$MESS["SENDER_LIMIT_LEFT_MENU_SMS_LIMIT"] = "即將出來的短信";
$MESS["SENDER_LIMIT_MAIL_DAILY_DESC"] = "存在每日發送限制以保護用戶免受垃圾郵件的影響。
<br>
如果您不發送垃圾郵件，每天的限制每天增加。
<br>
<br>
最初，您每天可以發送1000條消息。
<br>
如果您的新聞通訊的投訴數量在24小時內未達到門檻值，
<br>
限制將在第二天增加兩次。
<br>
因此，您只需遵循規則而不發送垃圾郵件即可向數千個收件人發送消息。
<br>
<br>
如果您的新聞通訊的投訴數量超過門檻值，<br>
您的1000條消息的限制將被重新建立。有效地，您必須等待幾天<br>，直到達到以前的每日發送率。
";
$MESS["SENDER_LIMIT_MAIL_DAILY_DESC_TITLE"] = "每日消息限制";
$MESS["SENDER_LIMIT_NAME"] = "限制";
$MESS["SENDER_LIMIT_NOTIFICATION_SUCCESS"] = "數據成功保存";
$MESS["SENDER_LIMIT_OF"] = "的";
$MESS["SENDER_LIMIT_SAVE"] = "節省";
$MESS["SENDER_LIMIT_SETUP"] = "增加限制";
$MESS["SENDER_LIMIT_TITLE"] = "限制";
$MESS["SENDER_MAIL_CONSENT_HELP"] = "在此處了解更多％link_start％link_end％";
$MESS["SENDER_MAIL_CONSENT_NAME"] = "同意接收新聞通訊";
$MESS["SENDER_MAIL_CONSENT_OPTION"] = "啟用同意接收新聞通訊";
$MESS["SENDER_SENDING_TIME_OPTION"] = "發送新聞通訊，短信消息並僅在指定的時間內撥打電話";
$MESS["SENDER_SENDING_TIME_TITLE"] = "時間選項";
$MESS["SENDER_TRACK_MAIL_HELP"] = "在此處了解更多％link_start％link_end％";
$MESS["SENDER_TRACK_MAIL_NAME"] = "電子郵件和鏈接跟踪";
$MESS["SENDER_TRACK_MAIL_OPTION"] = "跟踪電子郵件讀取和鏈接點擊";
