<?php
$MESS["DISK_BIZPROC_SERIAL_TEXT"] = "順序業務流程是一個簡單的業務流程，可以在文檔上執行一系列連續的操作。";
$MESS["DISK_BIZPROC_SERIAL_TITLE"] = "創建一個順序的業務流程";
$MESS["DISK_BIZPROC_STATUS_TEXT"] = "以狀態驅動的業務流程是一個連續的業務流程，具有訪問權限分發，以處理不同狀態的文檔。";
$MESS["DISK_BIZPROC_STATUS_TITLE"] = "創建一個以狀態驅動的業務流程";
