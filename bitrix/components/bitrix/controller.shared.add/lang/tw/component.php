<?php
$MESS["CSA_ERROR_ADD_SITE"] = "在控制器中註冊站點的錯誤。";
$MESS["CSA_ERROR_ADD_SITE2"] = "錯誤在控制器中註冊站點：＃錯誤＃";
$MESS["CSA_ERROR_APACHE_TEMPLATE_NOT_FOUND"] = "找不到Apache配置模板（＃文件＃）。";
$MESS["CSA_ERROR_BAD_MYSQL_PATH"] = "錯誤參數：MySQL可執行文件的路徑。";
$MESS["CSA_ERROR_CREATE_DIR"] = "錯誤創建站點目錄。";
$MESS["CSA_ERROR_DB_CONNECT"] = "連接到數據庫的錯誤：＃錯誤＃";
$MESS["CSA_ERROR_DB_CREATE"] = "錯誤創建數據庫（＃db_name＃）：＃錯誤＃";
$MESS["CSA_ERROR_DB_FILEDUMP"] = "由於缺乏讀取權限，初始數據庫轉儲文件不存在或無法讀取。";
$MESS["CSA_ERROR_DB_IMPORT"] = "錯誤導入數據庫（＃db_name＃）。";
$MESS["CSA_ERROR_DIR_PERMISSIONS"] = "分配給出租站點目錄的錯誤權限。";
$MESS["CSA_ERROR_DOMAIN_NAME"] = "域名只能包括拉丁字母，數字，點和連字符。";
$MESS["CSA_ERROR_FILE_PERMISSIONS"] = "分配給租用站點文件的錯誤權限。";
$MESS["CSA_ERROR_FILE_WRITE"] = "錯誤寫入＃文件＃。";
$MESS["CSA_ERROR_MEMORY_LIMIT"] = "分配給租用站點的不正確的內存限制。";
$MESS["CSA_ERROR_NAME_EXISTS"] = "指定的名稱已經在使用中。請選擇一個其他名稱。";
$MESS["CSA_ERROR_NGINX_TEMPLATE_NOT_FOUND"] = "找不到NGINX配置模板（＃文件＃）。";
$MESS["CSA_ERROR_NOT_FOUND_APACHE_VHOST_DIR"] = "帶有Apache配置文件的目錄不存在或無法編寫。";
$MESS["CSA_ERROR_NOT_FOUND_NGINX_VHOST_DIR"] = "帶有NGINX配置文件的目錄不存在或無法編寫。";
$MESS["CSA_ERROR_WRITE_APACHE_CONFIG"] = "錯誤寫入Apache配置文件。";
$MESS["CSA_ERROR_WRITE_NGINX_CONFIG"] = "錯誤寫入nginx配置文件。";
$MESS["CSA_LOG_ADD_CLIENT"] = "通過直接創建添加客戶";
$MESS["CSA_MODULE_NOT_INSTALLED"] = "沒有安裝控制器模塊";
$MESS["CSA_TITLE"] = "添加一個網站";
