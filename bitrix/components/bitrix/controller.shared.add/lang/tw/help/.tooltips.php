<?php
$MESS["APACHE_ROOT_TIP"] = "包含Apache虛擬站點配置文件的目錄";
$MESS["CONTROLLER_URL_TIP"] = "指定站點控制器URL";
$MESS["DIR_PERMISSIONS_TIP"] = "指定包含租用站點的文件夾的權限";
$MESS["FILE_PERMISSIONS_TIP"] = "指定出租網站文件的權限";
$MESS["MEMORY_LIMIT_TIP"] = "租用網站的內存限制";
$MESS["MYSQL_DB_PATH_TIP"] = "包含初始數據庫轉儲的文件的路徑";
$MESS["MYSQL_PASSWORD_TIP"] = "具有數據庫創建權限的MySQL用戶的密碼";
$MESS["MYSQL_PATH_TIP"] = "MySQL可執行文件的路徑";
$MESS["MYSQL_USER_TIP"] = "具有數據庫創建權限的MySQL用戶的登錄";
$MESS["NGINX_ROOT_TIP"] = "包含NGINX虛擬站點配置文件的目錄";
$MESS["PATH_PUBLIC_TIP"] = "出租網站的公共檔案";
$MESS["PATH_VHOST_TIP"] = "指定包含租用站點的文件夾的完整路徑";
$MESS["RELOAD_FILE_TIP"] = "觸發Web服務器的標誌文件重新啟動";
$MESS["SET_TITLE_TIP"] = "設置頁面標題。";
$MESS["URL_SUBDOMAIN_TIP"] = "指定頂級子域";
