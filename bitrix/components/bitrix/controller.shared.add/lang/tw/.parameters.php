<?php
$MESS["CSA_PARAM_APACHE_ROOT"] = "目錄包含虛擬網站的Apache配置文件";
$MESS["CSA_PARAM_CONTROLLER_URL"] = "控制器URL";
$MESS["CSA_PARAM_DIR_PERMISSIONS"] = "包含租用網站的文件夾的權限";
$MESS["CSA_PARAM_FILE_PERMISSIONS"] = "租用網站文件的權限";
$MESS["CSA_PARAM_MEMORY_LIMIT"] = "租用網站的存儲限制";
$MESS["CSA_PARAM_MYSQL_DB_PATH"] = "通往初始數據庫轉儲文件的路徑";
$MESS["CSA_PARAM_MYSQL_PASSWORD"] = "使用數據庫創建特權的MySQL用戶的密碼";
$MESS["CSA_PARAM_MYSQL_PATH"] = "MySQL可執行文件的路徑";
$MESS["CSA_PARAM_MYSQL_USER"] = "使用數據庫創建特權的MySQL用戶登錄";
$MESS["CSA_PARAM_NGINX_ROOT"] = "目錄包含虛擬站點的NGINX配置文件";
$MESS["CSA_PARAM_PATH_PUBLIC"] = "出租網站的公共檔案";
$MESS["CSA_PARAM_PATH_VHOST"] = "帶有租用網站的文件夾的完整路徑";
$MESS["CSA_PARAM_REGISTER_IMMEDIATE"] = "創建時註冊站點";
$MESS["CSA_PARAM_RELOAD_FILE"] = "標記文件觸發Web服務器重新啟動";
$MESS["CSA_PARAM_URL_SUBDOMAIN"] = "頂級域";
