<?php
$MESS["SONET_C27_T_ACTIONS"] = "動作";
$MESS["SONET_C27_T_ALL_MSGS"] = "所有消息";
$MESS["SONET_C27_T_ANSWER"] = "回覆";
$MESS["SONET_C27_T_BAN"] = "禁止";
$MESS["SONET_C27_T_DELETE"] = "刪除";
$MESS["SONET_C27_T_DO_DELETE"] = "刪除";
$MESS["SONET_C27_T_DO_READ"] = "標記為已讀";
$MESS["SONET_C27_T_EMPTY"] = "沒有傳入的消息。<br>此窗格顯示您的聯繫人中的消息。";
$MESS["SONET_C27_T_MARK_READ"] = "標記為已讀";
$MESS["SONET_C27_T_MESSAGE"] = "訊息";
$MESS["SONET_C27_T_SELECT_ALL"] = "檢查全部 /取消選中";
$MESS["SONET_C27_T_SENDER"] = "發件人";
