<?php
$MESS["CRM_ALL"] = "全部的";
$MESS["CRM_PRODUCT_ROW_TAX_UNIFORM_ALERT"] = "如有必要，您可以禁用此選項。確保禁用此選項不會違反您必須遵守的任何法律或法規。";
$MESS["CRM_PRODUCT_ROW_TAX_UNIFORM_TITLE"] = "貨物稅可以適用於整個列表，也就是說，對於文檔中的每個項目，也不適用。在單個文件中，不能有任何未繳稅的項目以及申請稅款的項目。";
$MESS["CRM_VAT_DELETE"] = "刪除增值稅率";
$MESS["CRM_VAT_DELETE_CONFIRM"] = "您確定要刪除“％s”？";
$MESS["CRM_VAT_DELETE_TITLE"] = "刪除此稅率";
$MESS["CRM_VAT_EDIT"] = "編輯增值稅";
$MESS["CRM_VAT_EDIT_TITLE"] = "打開此稅用於編輯";
$MESS["CRM_VAT_SHOW"] = "查看稅";
$MESS["CRM_VAT_SHOW_TITLE"] = "查看此稅";
