<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
$MESS["M_CRM_LEAD_LIST_ADD_DEAL"] = "添加活動";
$MESS["M_CRM_LEAD_LIST_CALL"] = "打個電話";
$MESS["M_CRM_LEAD_LIST_CREATE_BASE"] = "使用源創建";
$MESS["M_CRM_LEAD_LIST_CREATE_DEAL"] = "交易";
$MESS["M_CRM_LEAD_LIST_DECLINE"] = "拒絕";
$MESS["M_CRM_LEAD_LIST_DELETE"] = "刪除";
$MESS["M_CRM_LEAD_LIST_EDIT"] = "編輯";
$MESS["M_CRM_LEAD_LIST_FILTER_CUSTOM"] = "搜索結果";
$MESS["M_CRM_LEAD_LIST_FILTER_NONE"] = "所有線索";
$MESS["M_CRM_LEAD_LIST_JUNK"] = "垃圾";
$MESS["M_CRM_LEAD_LIST_MAIL"] = "發信息";
$MESS["M_CRM_LEAD_LIST_MEETING"] = "組織一個會議";
$MESS["M_CRM_LEAD_LIST_MORE"] = "更多的";
$MESS["M_CRM_LEAD_LIST_NO_FILTER"] = "所有線索";
$MESS["M_CRM_LEAD_LIST_PRESET_MY"] = "我的潛在客戶";
$MESS["M_CRM_LEAD_LIST_PRESET_NEW"] = "新線索";
$MESS["M_CRM_LEAD_LIST_PRESET_USER"] = "自定義過濾器";
