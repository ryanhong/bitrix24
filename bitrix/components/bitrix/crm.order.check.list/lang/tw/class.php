<?php
$MESS["CRM_COLUMN_ORDER_CHECK_CASHBOX_ID"] = "現金寄存器名稱";
$MESS["CRM_COLUMN_ORDER_CHECK_DATE_CREATE"] = "創建於";
$MESS["CRM_COLUMN_ORDER_CHECK_ID"] = "ID";
$MESS["CRM_COLUMN_ORDER_CHECK_LINK"] = "收據鏈接";
$MESS["CRM_COLUMN_ORDER_CHECK_ORDER_ID"] = "訂購ID";
$MESS["CRM_COLUMN_ORDER_CHECK_PAYMENT_DESCR"] = "支付";
$MESS["CRM_COLUMN_ORDER_CHECK_PAYMENT_ID"] = "付款ID";
$MESS["CRM_COLUMN_ORDER_CHECK_SHIPMENT_DESCR"] = "運輸";
$MESS["CRM_COLUMN_ORDER_CHECK_SHIPMENT_ID"] = "發貨ID";
$MESS["CRM_COLUMN_ORDER_CHECK_STATUS"] = "收據狀態";
$MESS["CRM_COLUMN_ORDER_CHECK_SUM"] = "收據金額";
$MESS["CRM_COLUMN_ORDER_CHECK_TITLE"] = "付款文件";
$MESS["CRM_COLUMN_ORDER_CHECK_TYPE"] = "收據類型";
$MESS["CRM_ERROR_WRONG_ORDER"] = "找不到訂單";
$MESS["CRM_ERROR_WRONG_ORDER_ID"] = "無效訂單ID";
$MESS["CRM_ORDER_CASHBOX_STATUS_E"] = "錯誤";
$MESS["CRM_ORDER_CASHBOX_STATUS_N"] = "未打印";
$MESS["CRM_ORDER_CASHBOX_STATUS_P"] = "印刷";
$MESS["CRM_ORDER_CASHBOX_STATUS_Y"] = "列印";
$MESS["CRM_ORDER_CHECK_LIST_TITLE"] = "## Account_number＃of＃date_bill＃（＃pay_system_name＃）的付款收據";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
