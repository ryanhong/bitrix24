<?php
$MESS["SLEM_AVATAR_SIZE"] = "消息作者的頭像尺寸（PX）";
$MESS["SLEM_AVATAR_SIZE_COMMENT"] = "評論作者的頭像尺寸（PX）";
$MESS["SLEM_COMMENT_ID"] = "評論ID";
$MESS["SLEM_EMAIL_TO"] = "將通知發送到電子郵件地址";
$MESS["SLEM_LOG_ENTRY_ID"] = "消息ID";
$MESS["SLEM_RECIPIENT_ID"] = "通知收件人ID";
$MESS["SLEM_URL"] = "消息URL";
