<?php
$MESS["IMOPENLINES_REPORT_PAGE_LIMITATION_POPUP_TEXT_1"] = "訪問高級BITRIX24功能";
$MESS["IMOPENLINES_REPORT_PAGE_LIMITATION_POPUP_TEXT_2_NEW"] = "Open Channel報告旨在幫助您改善客戶服務和客戶體驗。我們的儀表板在一個地方呈現所有關鍵信息- 哪些代理最有效，需求最高的溝通渠道，您的客戶的滿意程度，平均響應時間，一天中最繁忙的時間，一天中的最繁忙的時間等。您也可以選擇不同的報告類型- 絕對數字，圖表，圖形，直方圖等。";
$MESS["IMOPENLINES_REPORT_PAGE_META_RESTRICTION_RU"] = "*所有者Meta Platforms，Inc。在俄羅斯聯邦禁止使用。";
