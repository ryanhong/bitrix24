<?php
$MESS["CC_BLL_ACCESS_DENIED"] = "未足夠查看和編輯列表的權限。";
$MESS["CC_BLL_LISTS_FOR_SONET_GROUP_DISABLED"] = "該組禁用列表。";
$MESS["CC_BLL_MODULE_NOT_INSTALLED"] = "未安裝通用列表模塊。";
$MESS["CC_BLL_TITLE_TEXT_CLAIM"] = "工作流程";
$MESS["CC_BLL_TITLE_TEXT_LISTS"] = "列表";
$MESS["CC_BLL_UNKNOWN_ERROR"] = "未知錯誤。";
$MESS["CC_BLL_WRONG_IBLOCK"] = "指定的列表不正確。";
$MESS["CC_BLL_WRONG_IBLOCK_TYPE"] = "不正確的信息塊類型。";
