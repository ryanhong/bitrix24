<?php
$MESS["BPWC_WVCT_2LIST"] = "業務流程";
$MESS["BPWC_WVCT_APPLY"] = "申請";
$MESS["BPWC_WVCT_CANCEL"] = "取消";
$MESS["BPWC_WVCT_EMPTY"] = "這個業務過程沒有可以配置的參數。";
$MESS["BPWC_WVCT_SAVE"] = "節省";
$MESS["BPWC_WVCT_SUBTITLE"] = "新的業務流程設置";
