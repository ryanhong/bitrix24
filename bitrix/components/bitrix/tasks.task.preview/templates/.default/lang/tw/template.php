<?php
$MESS["TASKS_CLOSED_DATE"] = "完成於";
$MESS["TASKS_CREATED_DATE"] = "創建於";
$MESS["TASKS_CREATOR"] = "由...製作";
$MESS["TASKS_DATE_START"] = "開始";
$MESS["TASKS_DEADLINE"] = "最後期限";
$MESS["TASKS_END_DATE_PLAN"] = "結束日期";
$MESS["TASKS_NEW_RESPONSIBLE"] = "新員工";
$MESS["TASKS_NO_DATE"] = "不";
$MESS["TASKS_PRIORITY"] = "優先事項";
$MESS["TASKS_PRIORITY_0"] = "低的";
$MESS["TASKS_PRIORITY_1"] = "普通的";
$MESS["TASKS_PRIORITY_2"] = "高的";
$MESS["TASKS_RESPONSIBLE"] = "負責人";
$MESS["TASKS_SIDEBAR_START_DATE"] = "從";
$MESS["TASKS_START_DATE_PLAN"] = "開始日期";
$MESS["TASKS_STATUS"] = "地位";
$MESS["TASKS_STATUS_1"] = "新的";
$MESS["TASKS_STATUS_2"] = "待辦的";
$MESS["TASKS_STATUS_3"] = "進行中";
$MESS["TASKS_STATUS_4"] = "待審核";
$MESS["TASKS_STATUS_5"] = "完全的";
$MESS["TASKS_STATUS_6"] = "遞延";
$MESS["TASKS_STATUS_7"] = "拒絕";
$MESS["TASKS_STATUS_ACCEPTED"] = "待辦的";
$MESS["TASKS_STATUS_COMPLETED"] = "完全的";
$MESS["TASKS_STATUS_DECLINED"] = "拒絕";
$MESS["TASKS_STATUS_DELAYED"] = "遞延";
$MESS["TASKS_STATUS_IN_PROGRESS"] = "進行中";
$MESS["TASKS_STATUS_NEW"] = "新的";
$MESS["TASKS_STATUS_OVERDUE"] = "逾期";
$MESS["TASKS_STATUS_WAITING"] = "待審核";
$MESS["TASKS_TASK_TITLE_LABEL"] = "任務";
