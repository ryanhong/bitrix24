<?php
$MESS["CRM_ML_ENTITY_DETAIL_ERROR_NOT_ENOUGH_DEALS"] = "數據不足以訓練評分模型。您的CRM至少需要＃min_count＃交易來開始培訓。";
$MESS["CRM_ML_ENTITY_DETAIL_ERROR_NOT_ENOUGH_LEADS"] = "數據不足以訓練評分模型。您的CRM至少需要＃min_count＃線索開始培訓。";
$MESS["CRM_ML_ENTITY_DETAIL_TITLE_2"] = "AI得分";
