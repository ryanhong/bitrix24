<?php
$MESS["SALESCENTER_CONTROL_PANEL_ITEM_LABEL_RECOMMENDATION"] = "受到推崇的";
$MESS["SPP_SALESCENTER_JS_POPUP_CLOSE"] = "關閉";
$MESS["SPP_SALESCENTER_PAYSYSTEM_OTHER_SUB_TITLE"] = "其他支付系統";
$MESS["SPP_SALESCENTER_PAYSYSTEM_RECOMMENDATION_SUB_TITLE"] = "推薦的付款系統";
$MESS["SPP_SALESCENTER_PAYSYSTEM_SUB_APP_TITLE"] = "合作夥伴申請";
$MESS["SPP_SALESCENTER_PAYSYSTEM_SUB_TITLE"] = "支付系統";
$MESS["SPP_SALESCENTER_TITLE"] = "選擇支付系統";
