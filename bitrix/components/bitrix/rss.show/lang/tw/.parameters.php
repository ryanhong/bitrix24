<?php
$MESS["CP_BRS_PROCESS"] = "RSS飼料處理";
$MESS["CP_BRS_PROCESS_NONE"] = "沒有任何";
$MESS["CP_BRS_PROCESS_QUOTE"] = "屏幕HTML";
$MESS["CP_BRS_PROCESS_TEXT"] = "轉換為文字";
$MESS["CP_BRS_URL"] = "RSS進給URL";
$MESS["T_IBLOCK_DESC_RSS_NUM_NEWS"] = "顯示的新聞數（0表示全部顯示）";
$MESS["T_IBLOCK_DESC_RSS_OUT_CHANNEL"] = "新聞項目不在頻道標籤之外（通常在內部）";
