<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "未安裝業務流程模塊。";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "信息塊模塊未安裝。";
$MESS["IBLOCK_NOT_ASSIGNED"] = "沒有選擇信息塊。";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "沒有安裝社交網絡模塊。";
$MESS["WIKI_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["WIKI_MODULE_NOT_INSTALLED"] = "未安裝Wiki模塊。";
$MESS["WIKI_NOT_CHANGE_BIZPROC"] = "Wiki信息塊必須在業務流程中註冊以維護版本歷史記錄。";
$MESS["WIKI_SOCNET_INITIALIZING_FAILED"] = "社交網絡無法初始化。";
