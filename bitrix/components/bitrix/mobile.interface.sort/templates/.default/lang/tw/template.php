<?php
$MESS["M_SORT_ASC"] = "上升";
$MESS["M_SORT_BUTTON"] = "種類";
$MESS["M_SORT_CANCEL"] = "取消";
$MESS["M_SORT_DESC"] = "下降";
$MESS["M_SORT_DOWN_TEXT"] = "釋放以刷新...";
$MESS["M_SORT_FIELDS"] = "排序字段";
$MESS["M_SORT_LOAD_TEXT"] = "更新...";
$MESS["M_SORT_NO_FIELDS"] = "分類字段未指定";
$MESS["M_SORT_PULL_TEXT"] = "下拉以刷新...";
$MESS["M_SORT_TITLE"] = "排序方式";
