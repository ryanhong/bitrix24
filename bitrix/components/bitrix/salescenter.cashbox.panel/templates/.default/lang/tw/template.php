<?php
$MESS["SALESCENTER_CASHBOX_ZONE_CONFLICT"] = "注意力！一些活躍的現金寄存器支持多個國家。<br>
我們不支持混合模式。請停用支持多個國家的現金記錄。<br>
";
$MESS["SALESCENTER_CASHBOX_ZONE_CONFLICT_RU_LIST"] = "俄羅斯收銀機：<strong> #cashboxes＃</strong> <br>";
$MESS["SALESCENTER_CASHBOX_ZONE_CONFLICT_UA_LIST"] = "烏克蘭收銀機：<strong> #cashboxes＃</strong> <br>";
$MESS["SALESCENTER_CONTROL_PANEL_ITEM_LABEL_RECOMMENDATION"] = "受到推崇的";
$MESS["SCP_POPUP_BUTTON_CLOSE_MSGVER_1"] = "是的，關閉";
$MESS["SCP_POPUP_BUTTON_STAY"] = "取消";
$MESS["SCP_POPUP_CONTENT_MSGVER_1"] = "更改尚未保存。您確定要關閉設置嗎？";
$MESS["SCP_SALESCENTER_CASHBOX_SUB_TITLE"] = "收銀機";
$MESS["SCP_SALESCENTER_OFFILINE_CASHBOX_SUB_TITLE"] = "實物收銀機";
$MESS["SCP_SALESCENTER_TITLE"] = "選擇現金寄存器";
