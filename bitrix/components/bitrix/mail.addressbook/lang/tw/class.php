<?php
$MESS["MAIL_ADDRESSBOOK_ADD_ADDRESSES"] = "增加聯繫人";
$MESS["MAIL_ADDRESSBOOK_GRID_ACTION_EDIT"] = "編輯";
$MESS["MAIL_ADDRESSBOOK_GRID_ACTION_REMOVE"] = "刪除";
$MESS["MAIL_ADDRESSBOOK_LIST_COLUMN_AVATAR"] = "用戶圖像";
$MESS["MAIL_ADDRESSBOOK_LIST_COLUMN_EMAIL"] = "電子郵件";
$MESS["MAIL_ADDRESSBOOK_LIST_COLUMN_NAME"] = "姓名";
$MESS["MAIL_ADDRESSBOOK_LIST_PAGE_TITLE"] = "聯繫人";
