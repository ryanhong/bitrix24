<?php
$MESS["SMOD_ACCOUNT"] = "使用內部帳戶付款";
$MESS["SMOD_MOBILEAPP_NOT_INSTALLED"] = "未安裝MobileApp模塊。";
$MESS["SMOD_NO_PERMS2VIEW"] = "查看訂單的權限不足。";
$MESS["SMOD_PAYMENT"] = "支付";
$MESS["SMOD_PAY_BACK"] = "將資金退還給內部帳戶";
$MESS["SMOD_PAY_CANCEL"] = "取消付款";
$MESS["SMOD_PAY_CONFIRM"] = "確認付款";
$MESS["SMOD_SALE_NOT_INSTALLED"] = "沒有安裝電子商店模塊。";
$MESS["SMOD_STATUS"] = "訂單狀態";
