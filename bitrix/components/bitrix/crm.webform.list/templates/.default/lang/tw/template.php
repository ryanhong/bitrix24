<?php
$MESS["CRM_WEBFORM_LIST_ACTIONS_COPY1"] = "複製";
$MESS["CRM_WEBFORM_LIST_ACTIONS_EDIT"] = "編輯";
$MESS["CRM_WEBFORM_LIST_ACTIONS_REMOVE"] = "刪除表單";
$MESS["CRM_WEBFORM_LIST_ACTIONS_RESET_COUNTERS"] = "重置計數器";
$MESS["CRM_WEBFORM_LIST_ACTIONS_VIEW"] = "看法";
$MESS["CRM_WEBFORM_LIST_ADD_CAPTION"] = "添加新表格";
$MESS["CRM_WEBFORM_LIST_ADD_DESC1"] = "單擊此處創建表單";
$MESS["CRM_WEBFORM_LIST_ADS_FORM_DESC_HINT_FACEBOOK"] = "該表格鏈接到Facebook。<br> Facebook廣告表格現在已提交給CRM。<br>只要將其鏈接到Facebook，您就不能編輯該表格。";
$MESS["CRM_WEBFORM_LIST_ADS_FORM_DESC_HINT_VKONTAKTE"] = "該表單鏈接到VK。<br> VK AD表單現在已提交給CRM。<br>只要將其鏈接到VK，就不能編輯該表單。";
$MESS["CRM_WEBFORM_LIST_ADS_FORM_STATUS_ON_FACEBOOK"] = "鏈接到Facebook";
$MESS["CRM_WEBFORM_LIST_ADS_FORM_STATUS_ON_VKONTAKTE"] = "鏈接到VK";
$MESS["CRM_WEBFORM_LIST_APPLY1"] = "確認";
$MESS["CRM_WEBFORM_LIST_BTN_ACTIVATE"] = "使能夠";
$MESS["CRM_WEBFORM_LIST_BTN_ACTIVATE_TITLE"] = "使能夠";
$MESS["CRM_WEBFORM_LIST_BTN_CHOOSE_ACTION"] = "選擇動作";
$MESS["CRM_WEBFORM_LIST_BTN_DEACTIVATE"] = "禁用";
$MESS["CRM_WEBFORM_LIST_BTN_DEACTIVATE_TITLE"] = "禁用";
$MESS["CRM_WEBFORM_LIST_BTN_DETAILS"] = "細節";
$MESS["CRM_WEBFORM_LIST_BTN_REMOVE"] = "刪除";
$MESS["CRM_WEBFORM_LIST_BTN_REMOVE_TITLE"] = "刪除";
$MESS["CRM_WEBFORM_LIST_CANCEL"] = "取消";
$MESS["CRM_WEBFORM_LIST_CLOSE"] = "關閉";
$MESS["CRM_WEBFORM_LIST_DELETE_CONFIRM"] = "您想刪除此表格嗎？";
$MESS["CRM_WEBFORM_LIST_DELETE_CONFIRM_TITLE"] = "確認刪除";
$MESS["CRM_WEBFORM_LIST_DELETE_ERROR"] = "無法刪除項目";
$MESS["CRM_WEBFORM_LIST_DESC1"] = "使用CRM表格提高生產率。所有表單數據都將保存到CRM系統；管理人員只需要處理CRM系統中獲得的信息。";
$MESS["CRM_WEBFORM_LIST_DESC2"] = "在聊天，網站的頁面上使用表格，或將鏈接發送到公共頁面，以便您的客戶在選擇時填寫表格。";
$MESS["CRM_WEBFORM_LIST_ERROR_ACTION"] = "由於發生了錯誤，因此已取消操作。";
$MESS["CRM_WEBFORM_LIST_FILTER_SHOW"] = "顯示形式";
$MESS["CRM_WEBFORM_LIST_HIDE_DESC"] = "隱藏描述";
$MESS["CRM_WEBFORM_LIST_INFO_TITLE"] = "使用CRM表格提高生產率。";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ACTIVATED"] = "活性";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ACT_ON"] = "在";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_BTN_OFF"] = "禁用";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_BTN_ON"] = "使能夠";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_DEACTIVATED"] = "停用";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_OFF"] = "離開";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_OFF_NOW"] = "剛才停用";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_OFF_STATUS"] = "形式不活躍";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ON"] = "在";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ON_NOW"] = "活性";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ON_STATUS"] = "表格有效";
$MESS["CRM_WEBFORM_LIST_ITEM_BTN_GET_SCRIPT"] = "獲取鏈接或代碼";
$MESS["CRM_WEBFORM_LIST_ITEM_DATE_CREATE"] = "創建於";
$MESS["CRM_WEBFORM_LIST_ITEM_FILL_END"] = "成功完成";
$MESS["CRM_WEBFORM_LIST_ITEM_FILL_START"] = "總嘗試";
$MESS["CRM_WEBFORM_LIST_ITEM_GET_PAYMENT_ACTIVATED"] = "是的";
$MESS["CRM_WEBFORM_LIST_ITEM_GET_PAYMENT_DEACTIVATED"] = "不";
$MESS["CRM_WEBFORM_LIST_ITEM_PHONE_NOT_VERIFIED"] = "您的電話號碼需要驗證以使用此表格。";
$MESS["CRM_WEBFORM_LIST_ITEM_PUBLIC_LINK"] = "公共鏈接";
$MESS["CRM_WEBFORM_LIST_ITEM_PUBLIC_LINK_COPY"] = "複製到剪貼板";
$MESS["CRM_WEBFORM_LIST_ITEM_START_EDIT_BUT_STOPPED"] = "沒有完成";
$MESS["CRM_WEBFORM_LIST_ITEM_WRITE_ACCESS_DENIED"] = "您無權編輯表格。請聯繫您的BitRix24管理員。";
$MESS["CRM_WEBFORM_LIST_NOTIFY_USER_CONSENT_BTN_DETAIL"] = "細節";
$MESS["CRM_WEBFORM_LIST_NOTIFY_USER_CONSENT_BTN_OK"] = " - ";
$MESS["CRM_WEBFORM_LIST_NOTIFY_USER_CONSENT_ITEMS_1"] = " - ";
$MESS["CRM_WEBFORM_LIST_NOTIFY_USER_CONSENT_ITEMS_2"] = " - ";
$MESS["CRM_WEBFORM_LIST_NOTIFY_USER_CONSENT_ITEMS_3"] = " - ";
$MESS["CRM_WEBFORM_LIST_NOTIFY_USER_CONSENT_TEXT"] = " - ";
$MESS["CRM_WEBFORM_LIST_NOTIFY_USER_CONSENT_TEXT_BY"] = " - ";
$MESS["CRM_WEBFORM_LIST_NOTIFY_USER_CONSENT_TITLE"] = " - ";
$MESS["CRM_WEBFORM_LIST_NOT_ACTIVE"] = "不活躍";
$MESS["CRM_WEBFORM_LIST_PLUGIN_BTN_MORE"] = "更多的";
$MESS["CRM_WEBFORM_LIST_PLUGIN_DESC"] = "將小部件安裝到您的網站上，並在Bitrix24中與客戶通信。您將獲得一個即時通信工具箱：實時聊天，回調和CRM表單。安裝小部件非常簡單：只需選擇網站的平台，我們將告訴您下一步該怎麼做。";
$MESS["CRM_WEBFORM_LIST_PLUGIN_TITLE"] = "CMS插件";
$MESS["CRM_WEBFORM_LIST_SUCCESS_ACTION"] = "動作成功完成。";
$MESS["CRM_WEBFORM_LIST_SUCCESS_ACTION_CACHE_CLEANED"] = "申請同意；表單更新。";
$MESS["CRM_WEBFORM_PHONE_VERIFY_CUSTOM_DESCRIPTION_V1"] = "出於安全原因，您將必須輸入我們發送到手機的驗證代碼。每種形式只能完成一次。";
$MESS["CRM_WEBFORM_PHONE_VERIFY_CUSTOM_SLIDER_TITLE"] = "發布表格";
$MESS["CRM_WEBFORM_PHONE_VERIFY_CUSTOM_TITLE"] = "在短信中收到確認代碼";
