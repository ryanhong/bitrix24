<?php
$MESS["IMOL_ROLE_ERROR_EMPTY_NAME"] = "未指定角色名稱";
$MESS["IMOL_ROLE_ERROR_INSUFFICIENT_RIGHTS"] = "訪問權限不足";
$MESS["IMOL_ROLE_LICENSE_ERROR"] = "保存錯誤的角色。";
$MESS["IMOL_ROLE_MODULE_ERROR"] = "未安裝\“ Open Channels \”模塊";
$MESS["IMOL_ROLE_NOT_FOUND"] = "找不到角色。填寫表格以創建新角色。";
$MESS["IMOL_ROLE_SAVE_ERROR"] = "保存錯誤的角色。";
