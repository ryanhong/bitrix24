<?php
$MESS["IMOL_PERM_RESTRICTION"] = "Sie sollten auf einen der <a target=\"_blank\" href=\"https://www.bitrix24.de/prices/\">kostenpflichtigen Tarife</a> upgraden, um die Zugriffsrechte den Mitarbeitern zuweisen zu können.";
$MESS["IMOL_PERM_RESTRICTION_MSGVER_1"] = "Möchten Sie den Mitarbeitern die Zugriffsrechte gewähren, damit sie Kommunikationskanäle nutzen können, sollten Sie auf  einen der <a target=\"_blank\" href=\"https://www.bitrix24.de/prices/\">höheren Tarife</a> upgraden.";
$MESS["IMOL_ROLE_ACTION"] = "Aktion";
$MESS["IMOL_ROLE_ENTITY"] = "Einheit";
$MESS["IMOL_ROLE_LABEL"] = "Rolle";
$MESS["IMOL_ROLE_LOCK_ALT"] = "Es gibt Einschränkungen. Klicken Sie hier, um mehr zu erfahren.";
$MESS["IMOL_ROLE_PERMISSION"] = "Zugriffsrecht";
$MESS["IMOL_ROLE_SAVE"] = "Speichern";
