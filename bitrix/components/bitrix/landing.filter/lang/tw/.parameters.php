<?php
$MESS["LANDING_CMP_PAR_BUTTONS"] = "按鈕陣列";
$MESS["LANDING_CMP_PAR_DRAFT_MODE"] = "始終使用草稿模式";
$MESS["LANDING_CMP_PAR_FILTER_TYPE"] = "過濾器類型";
$MESS["LANDING_CMP_PAR_FOLDER_ID"] = "當前文件夾ID";
$MESS["LANDING_CMP_PAR_FOLDER_SITE_ID"] = "文件夾ID";
$MESS["LANDING_CMP_PAR_SETTING_LINK"] = "設置圖標鏈接";
