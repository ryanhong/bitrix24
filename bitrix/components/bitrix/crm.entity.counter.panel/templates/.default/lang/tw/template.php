<?php
$MESS["CRM_COUNTER_TYPE_IDLE"] = "<span class = \“ crm-counter-inner \”> <span class = \“ crm-counter-number \”>＃值＃</span> <span class = \'crm-counter-text \'>活動</span> </span>";
$MESS["CRM_COUNTER_TYPE_OVERDUE"] = "<span class = \“ crm-counter-inner \”> <span class = \“ crm-counter-number \”>＃值＃</span> <span class = \'crm-counter-text \> with逾期活動</span> </span>";
$MESS["CRM_COUNTER_TYPE_PENDING"] = "<span class = \“ crm-counter-inner \”> <span class = \“ crm-counter-number \”>＃值＃</span> <span class = \'crm-counter-text \> with今天的活動</span> </span>";
$MESS["NEW_CRM_COUNTER_TITLE_MY"] = "我的：";
$MESS["NEW_CRM_COUNTER_TYPE_CURRENT2"] = "計劃";
$MESS["NEW_CRM_COUNTER_TYPE_IDLE"] = "沒有活動";
$MESS["NEW_CRM_COUNTER_TYPE_INCOMINGCHANNEL"] = "入站";
$MESS["NEW_CRM_COUNTER_TYPE_OTHER"] = "其他員工";
$MESS["NEW_CRM_COUNTER_TYPE_OTHER_CURRENT"] = "員工計劃的活動";
$MESS["NEW_CRM_COUNTER_TYPE_OTHER_INCOMINGCHANNEL"] = "員工的入站活動";
$MESS["NEW_CRM_COUNTER_TYPE_OTHER_TITLE"] = "更多的";
$MESS["NEW_CRM_COUNTER_TYPE_OVERDUE"] = "逾期";
$MESS["NEW_CRM_COUNTER_TYPE_PENDING"] = "今天";
