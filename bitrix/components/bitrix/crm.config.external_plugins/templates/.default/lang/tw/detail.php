<?php
$MESS["CRM_CONFIG_PLG_DESC"] = "每當CRM中出現新訂單時，將您的網絡商店連接到Bitrix24，以創建新的活動。使您的網站商店另一個銷售信息來源。";
$MESS["CRM_CONFIG_PLG_DESC2"] = "使用Bitrix24來提高轉化率並管理客戶！";
$MESS["CRM_CONFIG_PLG_STEP1"] = "單擊＃A1＃在此處＃A2＃以在您的網站上下載並安裝應用程序。";
$MESS["CRM_CONFIG_PLG_STEP2_1"] = "創建並複制鏈接";
$MESS["CRM_CONFIG_PLG_STEP2_2"] = "創建鏈接";
$MESS["CRM_CONFIG_PLG_STEP2_3"] = "創建新鏈接";
$MESS["CRM_CONFIG_PLG_STEP2_4"] = "您將需要鏈接來配置您的網絡商店。";
$MESS["CRM_CONFIG_PLG_STEP3"] = "將鏈接粘貼到您網絡商店的應用程序配置字段中並保存更改。";
$MESS["CRM_CONFIG_PLG_STEP4"] = "您的網站現在已連接！";
$MESS["CRM_CONFIG_PLG_STEP4_2"] = "現在，每個新訂單都將在Bitrix24中註冊為一項新活動，相應的客戶記錄為CRM中的客戶。";
$MESS["CRM_CONFIG_PLG_TITLE"] = "在Bitrix24中處理您的網絡商店訂單";
$MESS["CRM_CONFIG_PLG_TITLE_1CBITRIX"] = "1c bitrix";
$MESS["CRM_CONFIG_PLG_TITLE_DRUPAL7"] = "Drupal 7";
$MESS["CRM_CONFIG_PLG_TITLE_JOOMLA"] = "JOOMLA";
$MESS["CRM_CONFIG_PLG_TITLE_MAGENTO2"] = "Magento 2";
$MESS["CRM_CONFIG_PLG_TITLE_WORDPRESS"] = "WordPress";
