<?php
$MESS["CRM_CONFIG_PLG_DESC1"] = "將網絡商店的客戶端數據庫導出到Bitrix24，並享受許多有用的客戶管理功能。";
$MESS["CRM_CONFIG_PLG_DESC2"] = "將Web Store訂單轉換為Bitrix24活動並創建客戶端配置文件。";
$MESS["CRM_CONFIG_PLG_DESC3"] = "使用BITRIX24自動化工具復甦的廢棄訂單。";
$MESS["CRM_CONFIG_PLG_DESC4"] = "客戶訪問了您的網站，但從未提交訂單？ Bitrix24將捕獲活動，創建線索並向客戶撥打自動電話。";
$MESS["CRM_CONFIG_PLG_SELECT_CMS"] = "選擇您的網絡商店的平台連接到Bitrix24";
$MESS["CRM_CONFIG_PLG_SOON"] = "即將推出！";
$MESS["CRM_CONFIG_PLG_TITLE"] = "網絡商店跟踪器";
