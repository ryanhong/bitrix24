<?php
$MESS["SUP_DESC_NO"] = "不";
$MESS["SUP_DESC_YES"] = "是的";
$MESS["SUP_LIST_DEFAULT_TEMPLATE_PARAM_1_NAME"] = "門票編輯頁面";
$MESS["SUP_LIST_TICKETS_PER_PAGE"] = "每頁門票";
$MESS["SUP_SET_PAGE_TITLE"] = "設置頁面標題";
$MESS["SUP_SHOW_USER_FIELD"] = "顯示自定義字段";
