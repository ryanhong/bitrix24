<?php
$MESS["MODULE_NOT_INSTALL"] = "Helpdesk模塊未安裝。";
$MESS["SUP_ALL"] = "（全部）";
$MESS["SUP_CLOSED"] = "關閉";
$MESS["SUP_DEFAULT_TITLE"] = "麻煩門票";
$MESS["SUP_F_CLOSE"] = "打開 /關閉";
$MESS["SUP_F_ID"] = "票證ID";
$MESS["SUP_F_LAMP"] = "狀態顏色";
$MESS["SUP_F_MESSAGE_TITLE"] = "標題 /消息文字";
$MESS["SUP_GREEN"] = "綠色的";
$MESS["SUP_GREY"] = "灰色的";
$MESS["SUP_OPENED"] = "打開";
$MESS["SUP_PAGES"] = "門票";
$MESS["SUP_RED"] = "紅色的";
