<?php
$MESS["PM_CANCEL"] = "取消";
$MESS["PM_IS_FINED"] = "成立";
$MESS["PM_NOT_FINED"] = "未找到";
$MESS["PM_SEARCH"] = "搜尋";
$MESS["PM_SEARCH_INSERT"] = "搜索字符串";
$MESS["PM_SEARCH_NOTHING"] = "未找到";
$MESS["PM_SEARCH_PATTERN"] = "在您的搜索中使用星號符號'*'。<br/>示例：'i*' - 所有以I開頭的名稱";
$MESS["PM_TITLE"] = "用戶搜索";
