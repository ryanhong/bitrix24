<?php
$MESS["BPABS_EMPTY_DOC_ID"] = "沒有指定要創建業務流程的元素ID。";
$MESS["BPABS_EMPTY_DOC_TYPE"] = "需要元素類型。";
$MESS["BPABS_EMPTY_ENTITY"] = "沒有指定要創建業務流程的實體。";
$MESS["BPABS_NO_AUTOSTART_PARAMETERS"] = "未設置自動恢復工作流程的參數。";
$MESS["BPABS_NO_PERMS"] = "您無權為此元素運行業務流程。";
$MESS["BPABS_REQUIRED_CONSTANTS"] = "工作流程常數需要配置。";
$MESS["BPABS_TITLE"] = "運行業務流程";
$MESS["BPATT_NO_MODULE_ID"] = "需要模塊ID。";
