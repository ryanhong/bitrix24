<?php
$MESS["IDEA_USER_INFO_DATE_REGISTER_TITLE"] = "註冊";
$MESS["IDEA_USER_INFO_LAST_ACTIVITY_DATE_TITLE"] = "最後登錄";
$MESS["IDEA_USER_INFO_PERSONAL_BIRTHDATE_TITLE"] = "生日";
$MESS["IDEA_USER_INFO_PERSONAL_CITY_TITLE"] = "城市";
$MESS["IDEA_USER_INFO_PERSONAL_COUNTRY_TITLE"] = "國家";
$MESS["IDEA_USER_INFO_PERSONAL_GENDER_TITLE"] = "性別";
$MESS["IDEA_USER_INFO_PERSONAL_PHONE_TITLE"] = "電話";
$MESS["IDEA_USER_INFO_PERSONAL_STATE_TITLE"] = "地區/州";
$MESS["IDEA_USER_INFO_SECOND_NAME_TITLE"] = "中間名字";
$MESS["IDEA_USER_INFO_WORK_CITY_TITLE"] = "公司城市";
$MESS["IDEA_USER_INFO_WORK_COMPANY_TITLE"] = "公司";
$MESS["IDEA_USER_INFO_WORK_COUNTRY_TITLE"] = "公司國家";
$MESS["IDEA_USER_INFO_WORK_PHONE_TITLE"] = "工作電話";
$MESS["IDEA_USER_INFO_WORK_POSITION_TITLE"] = "位置";
$MESS["IDEA_USER_INFO_WORK_PROFILE_TITLE"] = "公司簡介";
$MESS["IDEA_USER_INFO_WORK_STATE_TITLE"] = "州/地區";
$MESS["IDEA_USER_INFO_WORK_WWW_TITLE"] = "公司網站";
