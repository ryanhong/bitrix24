<?php
$MESS["CRM_ALL"] = "全部的";
$MESS["CRM_TASK_DELETE"] = "刪除任務";
$MESS["CRM_TASK_DELETE_CONFIRM"] = "你確定你要刪除嗎？";
$MESS["CRM_TASK_DELETE_TITLE"] = "刪除任務";
$MESS["CRM_TASK_EDIT"] = "編輯任務";
$MESS["CRM_TASK_EDIT_TITLE"] = "編輯任務";
$MESS["CRM_TASK_SHOW"] = "查看任務";
$MESS["CRM_TASK_SHOW_TITLE"] = "查看任務";
