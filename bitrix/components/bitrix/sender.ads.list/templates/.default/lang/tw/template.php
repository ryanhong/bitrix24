<?php
$MESS["SENDER_LETTER_LIST_ACT_COPY"] = "複製";
$MESS["SENDER_LETTER_LIST_ACT_EDIT"] = "編輯";
$MESS["SENDER_LETTER_LIST_ACT_REMOVE"] = "刪除";
$MESS["SENDER_LETTER_LIST_ACT_VIEW"] = "看法";
$MESS["SENDER_LETTER_LIST_BTN_ADD"] = "創建廣告";
$MESS["SENDER_LETTER_LIST_BUTTON_RECIPIENT"] = "收件人";
$MESS["SENDER_LETTER_LIST_DURATION"] = "如果現在啟動，則發送廣告消息所需的時間。";
$MESS["SENDER_LETTER_LIST_DUR_DATE_CREATE"] = "創建於";
$MESS["SENDER_LETTER_LIST_DUR_DATE_FINISH"] = "直到結束";
$MESS["SENDER_LETTER_LIST_RECIPIENTS_ALL"] = "總觀眾。";
$MESS["SENDER_LETTER_LIST_RECIPIENTS_SENT"] = "收到廣告的收件人數量。";
$MESS["SENDER_LETTER_LIST_ROW_FROM"] = "從";
$MESS["SENDER_LETTER_LIST_ROW_RECIPIENT"] = "收件人";
$MESS["SENDER_LETTER_LIST_SPEED_TITLE"] = "達到每日消息限制。發送明天將繼續。";
$MESS["SENDER_LETTER_LIST_STATE_IS_PAUSED"] = "停了下來";
$MESS["SENDER_LETTER_LIST_STATE_IS_SENT"] = "完全的";
$MESS["SENDER_LETTER_LIST_STATE_IS_STOPPED"] = "停了下來";
$MESS["SENDER_LETTER_LIST_STATE_PAUSE"] = "暫停";
$MESS["SENDER_LETTER_LIST_STATE_PAUSE_TITLE"] = "暫停廣告";
$MESS["SENDER_LETTER_LIST_STATE_PUBLISH"] = "發布";
$MESS["SENDER_LETTER_LIST_STATE_PUBLISH_TITLE"] = "發布廣告";
$MESS["SENDER_LETTER_LIST_STATE_RESUME"] = "恢復";
$MESS["SENDER_LETTER_LIST_STATE_RESUME_TITLE"] = "簡歷廣告";
$MESS["SENDER_LETTER_LIST_STATE_STOP"] = "取消";
$MESS["SENDER_LETTER_LIST_STATE_STOP_TITLE"] = "停止廣告並完成活動";
$MESS["SENDER_LETTER_LIST_STATE_WILL_SEND"] = "將啟動";
$MESS["SENDER_LETTER_LIST_UPDATE_INFO"] = "從提供商請求信息";
