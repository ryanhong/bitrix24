<?php
$MESS["main_app_pass_cancel"] = "取消";
$MESS["main_app_pass_comment"] = "評論";
$MESS["main_app_pass_comment_ph"] = "添加描述";
$MESS["main_app_pass_create_pass"] = "創建密碼";
$MESS["main_app_pass_create_pass_close"] = "關閉";
$MESS["main_app_pass_create_pass_text"] = "使用此密碼與所選應用程序同步。
密碼可能不會以打開形式保存；因此，僅在用戶獲得時才可用。
在復製或輸入密碼之前，不要關閉窗口。";
$MESS["main_app_pass_created"] = "創建於";
$MESS["main_app_pass_del"] = "刪除";
$MESS["main_app_pass_del_pass"] = "您確定要刪除密碼嗎？";
$MESS["main_app_pass_del_pass_text"] = "由於身份驗證錯誤，該應用程序將無法訪問數據，因此同步將被中止。";
$MESS["main_app_pass_get_pass"] = "獲取密碼";
$MESS["main_app_pass_last"] = "上次訪問";
$MESS["main_app_pass_last_ip"] = "最後一個IP";
$MESS["main_app_pass_link"] = "鏈接到";
$MESS["main_app_pass_manage"] = "管理";
$MESS["main_app_pass_other"] = "其他";
$MESS["main_app_pass_text1"] = "要將數據與外部應用程序安全地同步，請使用可以在此頁面上獲得的特殊密碼。";
$MESS["main_app_pass_text2"] = "這些工具中的每一個都支持一系列支持同步的應用程序。選擇要配置數據傳輸的應用程序。";
$MESS["main_app_pass_title"] = "應用程序密碼";
