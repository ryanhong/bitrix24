<?php
$MESS["M_CRM_CONVERT_SYNC_CONTINUE"] = "繼續";
$MESS["M_CRM_CONVERT_SYNC_DOWN_TEXT"] = "釋放以刷新...";
$MESS["M_CRM_CONVERT_SYNC_ENTITIES"] = "選擇將為缺少字段補充的文檔";
$MESS["M_CRM_CONVERT_SYNC_FIELDS"] = "這些字段將被創建";
$MESS["M_CRM_CONVERT_SYNC_LEGEND_DEAL"] = "選定的實體沒有可以存儲交易數據的字段。請選擇將創建缺少字段以適應所有可用信息的實體。";
$MESS["M_CRM_CONVERT_SYNC_LEGEND_LEAD"] = "選定的實體沒有可以存儲鉛數據的字段。請選擇將創建缺少字段以適應所有可用信息的實體。";
$MESS["M_CRM_CONVERT_SYNC_LEGEND_QUOTE"] = "選定的實體沒有可以存儲報價數據的字段。請選擇將創建缺少字段以適應所有可用信息的實體。";
$MESS["M_CRM_CONVERT_SYNC_LOAD_TEXT"] = "更新...";
$MESS["M_CRM_CONVERT_SYNC_MORE"] = "更多的";
$MESS["M_CRM_CONVERT_SYNC_PULL_TEXT"] = "下拉以刷新...";
$MESS["M_CRM_CONVERT_SYNC_TITLE"] = "使用源創建";
