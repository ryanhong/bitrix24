<?php
$MESS["FSL_AUTH"] = "您需要被授權查看此頁面。";
$MESS["FSL_NO_DELETE"] = "訂閱＃SID＃未刪除。";
$MESS["FSL_NO_DUSER"] = "用戶## uid＃找不到";
$MESS["FSL_NO_MODULE"] = "論壇模塊未安裝";
$MESS["FSL_NO_SPERMS"] = "您沒有足夠的權限來刪除此訂閱";
$MESS["FSL_SUCC_DELETE"] = "訂閱＃SID＃已成功刪除";
$MESS["FSL_TITLE"] = "訂閱列表";
$MESS["F_EMPTY_SUBSCRIBES"] = "未選擇訂閱以刪除。";
$MESS["F_ERR_SESS_FINISH"] = "您的會議已經過期。請重複操作。";
$MESS["F_SUBSCRIBE"] = "訂閱";
