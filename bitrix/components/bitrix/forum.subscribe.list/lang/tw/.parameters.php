<?php
$MESS["F_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["F_DEFAULT_UID"] = "用戶身份";
$MESS["F_DISPLAY_PANEL"] = "顯示此組件的面板按鈕";
$MESS["F_LIST_TEMPLATE"] = "主題列表頁面";
$MESS["F_MESSAGE_TEMPLATE"] = "消息查看頁面";
$MESS["F_PAGE_NAVIGATION_TEMPLATE"] = "Pager模板的名稱";
$MESS["F_PAGE_NAVIGATION_WINDOW"] = "頁面導航中的頁數";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "個人資料頁";
$MESS["F_READ_TEMPLATE"] = "主題閱讀頁面";
$MESS["F_SET_NAVIGATION"] = "顯示麵包屑導航";
$MESS["F_SUBSCRIBE_TEMPLATE"] = "訂閱頁面";
$MESS["F_TOPICS_PER_PAGE"] = "每頁主題數";
$MESS["F_URL_TEMPLATES"] = "URL處理";
