<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>不緩存</i>：沒有執行緩存。";
$MESS["CHECK_DATES_TIP"] = "指定僅搜索活動（未過期的）文檔。";
$MESS["PAGE_ELEMENTS_TIP"] = "指定雲中標籤的計數。";
$MESS["PERIOD_TIP"] = "指定選擇標籤的最近幾天的數量。";
$MESS["SEARCH_TIP"] = "搜尋";
$MESS["SORT_TIP"] = "在此處選擇標籤排序模式：按名稱或視圖。";
$MESS["TAGS_INHERIT_TIP"] = "指定縮小搜索範圍。";
$MESS["TAGS_TIP"] = "標籤";
$MESS["URL_SEARCH_TIP"] = "搜索頁面路徑（與站點root的相關）。";
$MESS["arrFILTER_TIP"] = "允許縮小搜索區域。例如，您可以指定僅搜索靜態文件。";
$MESS["arrFILTER_socialnetwork_user_TIP"] = "顯示應將其內容包含在搜索中的用戶的ID。";
