<?php
$MESS["EC_CALENDAR_OLD_VERSION"] = "事件日曆組件的當前版本已過時。 <a href= \"/bitrix/admin/module_admin.php \">安裝新版本</a>。";
$MESS["EC_CALENDAR_OLD_VERSION_INST"] = "您正在使用的\“事件日曆\”組件已過時。請<a href= \"/bitrix/admin/calendar_convert.php \">轉換您的數據</a>
使用新版本的事件日曆。";
$MESS["EC_GROUP_NOT_FOUND"] = "找不到組";
$MESS["EC_IBLOCK_ACCESS_DENIED"] = "拒絕訪問";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "未安裝\“信息塊\”模塊。";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "未安裝\“ Intranet Portal \”模塊。";
$MESS["EC_USER_NOT_FOUND"] = "找不到用戶";
