<?php
$MESS["ECL_P_ALLOW_SUPERPOSE"] = "使用喜歡的日曆";
$MESS["ECL_P_CACHE_TIME"] = "緩存壽命（秒）";
$MESS["ECL_P_CUR_USER_EVENT_LIST"] = "顯示當前用戶的事件";
$MESS["ECL_P_DETAIL_URL"] = "詳細的查看頁面URL";
$MESS["ECL_P_EVENTS_COUNT"] = "列表中的事件";
$MESS["ECL_P_EVENT_LIST_MODE"] = "僅顯示事件清單";
$MESS["ECL_P_FUTURE_MONTH_COUNT"] = "展示即將發生的事件（幾個月）";
$MESS["ECL_P_INIT_DATE"] = "初始化";
$MESS["ECL_P_LOAD_MODE"] = "加載事件";
$MESS["ECL_P_SHOW_CUR_DATE"] = "-當前日期-";
$MESS["ECL_P_USER_IBLOCK_ID"] = "用戶日曆的信息塊";
$MESS["EC_CALENDAR_SECTION"] = "日曆部分";
$MESS["EC_CALENDAR_SECTION_ALL"] = "所有部分";
$MESS["EC_TYPE"] = "日曆類型";
