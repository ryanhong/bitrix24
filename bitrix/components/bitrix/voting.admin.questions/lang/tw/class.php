<?php
$MESS["DELETE_ERROR"] = "刪除對象時發生錯誤。";
$MESS["SAVE_ERROR"] = "保存對象時發生錯誤。";
$MESS["VOTE_ACTIVE"] = "積極的";
$MESS["VOTE_DIAGRAM"] = "圖表";
$MESS["VOTE_NO"] = "不";
$MESS["VOTE_QUESTION"] = "問題";
$MESS["VOTE_REQUIRED"] = "必需的";
$MESS["VOTE_TIMESTAMP_X"] = "修改的";
$MESS["VOTE_YES"] = "是的";
