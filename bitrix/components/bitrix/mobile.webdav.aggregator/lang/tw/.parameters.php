<?php
$MESS["WD_GROUP_FILE_PATH"] = "工作組文檔頁面";
$MESS["WD_IBLOCK_GROUP_ID"] = "工作組文檔信息塊";
$MESS["WD_IBLOCK_OTHER_ID"] = "文檔信息塊";
$MESS["WD_IBLOCK_TYPE"] = "信息塊類型";
$MESS["WD_IBLOCK_USER_ID"] = "用戶文檔信息塊";
$MESS["WD_NAME_TEMPLATE"] = "名稱格式";
$MESS["WD_NAME_TEMPLATE_DEFAULT"] = "＃NOBR ##名稱＃#last_name ##/nobr＃";
$MESS["WD_USER_FILE_PATH"] = "用戶文檔頁面";
