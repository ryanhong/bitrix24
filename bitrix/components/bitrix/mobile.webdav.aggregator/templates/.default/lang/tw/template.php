<?php
$MESS["WD_AG_HELP1"] = "運行<i> Windows Explorer </i>。";
$MESS["WD_AG_HELP2"] = "Select'工具＆laquo; - >＆laquo; Map Network Drive ...＆raquo;在Explorer菜單中。";
$MESS["WD_AG_HELP3"] = "選擇要連接的文件夾的驅動器字母。";
$MESS["WD_AG_HELP4"] = "在<i>文件夾字段中提供庫路徑：";
$MESS["WD_AG_HELP5"] = "單擊<i>完成</i>。";
$MESS["WD_AG_HELP6"] = "有關使用Windows和Mac OS X中的庫的更多信息，請參閱＃StartLink＃文檔庫幫助部分＃EndLink＃。";
$MESS["WD_AG_MSGTITLE"] = "作為網絡驅動器連接庫：";
$MESS["WD_NO_LIBRARIES"] = "沒有可用的文檔庫";
