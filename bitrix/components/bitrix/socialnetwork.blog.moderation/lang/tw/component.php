<?php
$MESS["BLOG_BLOG_BLOG_FRIENDS_ONLY"] = "閱讀對話的權限不足。";
$MESS["BLOG_BLOG_BLOG_MES_DELED"] = "對話已成功刪除。";
$MESS["BLOG_BLOG_BLOG_MES_DEL_ERROR"] = "錯誤刪除對話。";
$MESS["BLOG_BLOG_BLOG_MES_DEL_NO_RIGHTS"] = "您無權刪除此對話。";
$MESS["BLOG_BLOG_BLOG_MES_PUB"] = "對話已經發布。";
$MESS["BLOG_BLOG_BLOG_MES_PUB_ERROR"] = "錯誤發布對話。";
$MESS["BLOG_BLOG_BLOG_MES_PUB_NO_RIGHTS"] = "您無權發布此對話。";
$MESS["BLOG_BLOG_SESSID_WRONG"] = "您的會議已經過期。請再試一次。";
$MESS["BLOG_MODULE_NOT_INSTALL"] = "沒有安裝博客模塊。";
$MESS["BLOG_MOD_DELETE"] = "刪除";
$MESS["BLOG_MOD_DELETE_CONFIRM"] = "您確定要刪除對話嗎？";
$MESS["BLOG_MOD_EMPTY_SOCNET_USER"] = "未指定的用戶配置文件";
$MESS["BLOG_MOD_NO_RIGHTS"] = "您無權在此組中進行中等對話。";
$MESS["BLOG_MOD_NO_SOCNET_GROUP"] = "沒有指定社交網絡組。";
$MESS["BLOG_MOD_NO_SOCNET_USER"] = "您無權查看適量提交";
$MESS["BLOG_MOD_PUB"] = "發布";
$MESS["BLOG_MOD_TITLE"] = "適度";
$MESS["BLOG_SONET_MODULE_NOT_AVAIBLE"] = "對於此用戶組，對話不可用。";
$MESS["MESSAGE_COUNT"] = "對話";
