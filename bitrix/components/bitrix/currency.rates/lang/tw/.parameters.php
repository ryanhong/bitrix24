<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：緩存在緩存設置中設置的時間; <br /> <i> cache < /i>：始終在旁邊指定的時期; <br /> <br /> <i>否CAHCE </i>：沒有執行緩存。";
$MESS["CURRENCY_BASE"] = "將所有原始貨幣轉換為貨幣";
$MESS["CURRENCY_BASE_TIP"] = "在此處選擇定義的貨幣之一。其他貨幣（以上選擇）將相對於此貨幣表示。";
$MESS["CURRENCY_FROM"] = "轉換的原始貨幣陣列";
$MESS["CURRENCY_RATE_DAY"] = "顯示貨幣匯率的日期（以\“ y-m-d \”格式）";
$MESS["RATE_DAY_TIP"] = "指定貨幣報價日期。如果此日期沒有報價，將顯示默認率。";
$MESS["SHOW_CB_TIP"] = "只有在目標貨幣為摩擦的情況下，此選項才有效。如果已檢查，將使用俄羅斯銀行提供的數據顯示摩擦率。";
$MESS["T_CURRENCY_CBRF"] = "從俄羅斯聯邦中央銀行展示貨幣匯率";
$MESS["arrCURRENCY_FROM_TIP"] = "選擇將在表中顯示的報價的貨幣。";
