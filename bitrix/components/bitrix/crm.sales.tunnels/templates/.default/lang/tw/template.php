<?php
$MESS["CRM_MENU_RIGHTS_CATEGORY_ALL_FOR_ALL"] = "授予所有用戶的訪問權限";
$MESS["CRM_MENU_RIGHTS_CATEGORY_COPY_FROM_TUNNELS2"] = "從管道複製訪問權限";
$MESS["CRM_MENU_RIGHTS_CATEGORY_CUSTOM"] = "自定義訪問設置";
$MESS["CRM_MENU_RIGHTS_CATEGORY_NONE_FOR_ALL"] = "密切訪問所有用戶";
$MESS["CRM_MENU_RIGHTS_CATEGORY_OWN_FOR_ALL"] = "僅授予我的交易";
$MESS["CRM_MENU_RIGHTS_CATEGORY_OWN_FOR_ELEMENT"] = "僅授予我物品的訪問權限";
$MESS["CRM_ST_ACTION_COPY"] = "複製";
$MESS["CRM_ST_ACTION_MOVE"] = "移動";
$MESS["CRM_ST_ADD_FUNNEL_BUTTON2"] = "添加管道";
$MESS["CRM_ST_ADD_NEW_CATEGORY_BUTTON_LABEL2"] = "添加管道";
$MESS["CRM_ST_CATEGORY_DRAG_BUTTON2"] = "拖動排序管道";
$MESS["CRM_ST_CONFIRM_STAGE_REMOVE_TEXT"] = "您確定要刪除舞台嗎？";
$MESS["CRM_ST_DOT_TITLE"] = "拖動創建隧道";
$MESS["CRM_ST_EDIT_CATEGORY_TITLE"] = "編輯名稱";
$MESS["CRM_ST_EDIT_RIGHTS_CATEGORY"] = "編輯設置";
$MESS["CRM_ST_ERROR_POPUP_CLOSE_BUTTON_LABEL"] = "關閉";
$MESS["CRM_ST_ERROR_POPUP_TITLE"] = "錯誤";
$MESS["CRM_ST_GENERATOR_HELP_BUTTON"] = "了解銷售提升";
$MESS["CRM_ST_GENERATOR_SETTINGS_LINK_LABEL"] = "銷售提升";
$MESS["CRM_ST_HELP_BUTTON"] = "幫助";
$MESS["CRM_ST_NOTIFICATION_CHANGES_SAVED"] = "更改已保存。";
$MESS["CRM_ST_REMOVE"] = "刪除";
$MESS["CRM_ST_REMOVE_CATEGORY2"] = "刪除管道";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_BUTTON_LABEL"] = "刪除";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_CANCEL_BUTTON_LABEL"] = "取消";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_POPUP_DESCRIPTION2"] = "刪除管道還將刪除所有創建的隧道。";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_POPUP_TITLE2"] = "刪除管道\“＃名稱＃\”";
$MESS["CRM_ST_REMOVE_CATEGORY_CONFIRM_REMOVE_BUTTON_LABEL2"] = "刪除管道";
$MESS["CRM_ST_ROBOTS_HELP_BUTTON"] = "閱讀有關CRM自動化的信息";
$MESS["CRM_ST_ROBOT_ACTION_COPY"] = "規則輔助副本";
$MESS["CRM_ST_ROBOT_ACTION_MOVE"] = "統治輔助行動";
$MESS["CRM_ST_ROBOT_SETTINGS_LINK_LABEL"] = "配置自動化規則";
$MESS["CRM_ST_SAVE_ERROR2"] = "保存錯誤管道。";
$MESS["CRM_ST_SETTINGS"] = "配置";
$MESS["CRM_ST_STAGES_DISABLED"] = "階段禁用";
$MESS["CRM_ST_STAGES_GROUP_FAIL"] = "失敗的";
$MESS["CRM_ST_STAGES_GROUP_IN_PROGRESS"] = "進行中";
$MESS["CRM_ST_STAGES_GROUP_SUCCESS"] = "成功";
$MESS["CRM_ST_TITLE_EDITOR_PLACEHOLDER2"] = "新管道";
$MESS["CRM_ST_TUNNEL_BUTTON_LABEL"] = "隧道";
$MESS["CRM_ST_TUNNEL_BUTTON_TITLE"] = "編輯或刪除隧道";
$MESS["CRM_ST_TUNNEL_EDIT_ACCESS_DENIED"] = "創建隧道的許可不足。";
