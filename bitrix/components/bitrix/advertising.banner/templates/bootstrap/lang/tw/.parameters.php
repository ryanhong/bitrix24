<?php
$MESS["ADV_BS_ARROW_NAV"] = "在幻燈片上顯示導航控制";
$MESS["ADV_BS_BULLET_NAV"] = "顯示Paginator控制";
$MESS["ADV_BS_CYCLING"] = "自動提前";
$MESS["ADV_BS_EFFECT"] = "幻燈片提前效果";
$MESS["ADV_BS_EFFECT_FADE"] = "褪色";
$MESS["ADV_BS_EFFECT_SLIDE"] = "滑動";
$MESS["ADV_BS_HIDE_FOR_PHONES"] = "在電話上隱藏橫幅區域";
$MESS["ADV_BS_HIDE_FOR_TABLETS"] = "在平板電腦上隱藏橫幅區域";
$MESS["ADV_BS_INTERVAL"] = "滑動提前速度（MSEC）";
$MESS["ADV_BS_KEYBOARD"] = "啟用鍵盤導航";
$MESS["ADV_BS_PAUSE"] = "懸停在懸停";
$MESS["ADV_BS_WRAP"] = "倒帶開始";
