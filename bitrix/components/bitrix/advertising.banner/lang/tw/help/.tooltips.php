<?php
$MESS["BS_EFFECT_TIP"] = "使用幻燈片效果時，帶有固定圖像的幻燈片可能會錯誤地渲染。";
$MESS["BS_HIDE_FOR_PHONES_TIP"] = "僅支持引導樣式";
$MESS["BS_HIDE_FOR_TABLETS_TIP"] = "僅支持引導樣式";
$MESS["BS_KEYBOARD_TIP"] = "使用箭頭鍵控制幻燈片";
$MESS["CACHE_TIME_TIP"] = "緩存時間（秒）";
$MESS["CACHE_TYPE_TIP"] = "緩存類型";
$MESS["DEFAULT_TEMPLATE_TIP"] = "所選的模板只能與圖像橫幅一起使用";
$MESS["JQUERY_TIP"] = "此滑塊需要jQuery。";
$MESS["KEYBOARD_TIP"] = "使用箭頭鍵控制幻燈片";
$MESS["QUANTITY_TIP"] = "最大限度。橫幅數量乘法組件模板可以顯示";
$MESS["TYPE_TIP"] = "橫幅類型";
