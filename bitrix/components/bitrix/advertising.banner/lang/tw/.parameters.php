<?php
$MESS["ADV_DEFAULT_TEMPLATE"] = "默認滑塊模板";
$MESS["ADV_NAV_SETTINGS"] = "導航設置";
$MESS["ADV_NOT_SELECTED"] = "（未選中的）";
$MESS["ADV_QUANTITY"] = "旋轉橫幅數量";
$MESS["ADV_SELECT_DEFAULT"] = "選擇橫幅類型";
$MESS["ADV_SLIDE_SETTINGS"] = "幻燈片設置";
$MESS["ADV_TYPE"] = "橫幅類型";
$MESS["adv_banner_params_noindex"] = "將NOIndex/nofollow屬性添加到鏈接";
