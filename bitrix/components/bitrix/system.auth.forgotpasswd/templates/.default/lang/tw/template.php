<?php
$MESS["AUTH_AUTH"] = "授權";
$MESS["AUTH_SEND"] = "發送";
$MESS["sys_forgot_pass_label"] = "使用以下方式恢復密碼：";
$MESS["sys_forgot_pass_login1"] = "登錄或電子郵件：";
$MESS["sys_forgot_pass_note_email"] = "您的註冊數據和重置您的密碼的檢查單詞將發送到您的電子郵件。";
$MESS["sys_forgot_pass_note_phone"] = "帶有代碼以更改密碼的SMS將發送到手機。";
$MESS["sys_forgot_pass_phone"] = "電話";
$MESS["system_auth_captcha"] = "輸入您在圖片上看到的字符：";
