<?php
$MESS["BLOG_BLOG_BLOG_CATEGORY"] = "標籤：";
$MESS["BLOG_BLOG_IN_IMAGES_TITLE"] = "圖片描述";
$MESS["BLOG_CATEGORY"] = "標籤";
$MESS["BLOG_COMMENTS"] = "評論";
$MESS["BLOG_DELETE"] = "刪除";
$MESS["BLOG_IMAGE"] = "圖像";
$MESS["BLOG_PREVIEW_TITLE"] = "預覽：";
$MESS["BLOG_P_IMAGES"] = "圖片：";
$MESS["BLOG_P_IMAGE_LINK"] = "插入圖像鏈接";
$MESS["BLOG_P_IMAGE_UPLOAD"] = "上傳圖片";
$MESS["BLOG_VIEWS"] = "視圖";
$MESS["BPC_IMAGE_SIZE_NOTICE"] = "最大圖像文件大小：<b>＃size＃mb </b>";
$MESS["BPC_VIDEO_P"] = "視頻路徑";
$MESS["BPC_VIDEO_PATH_EXAMPLE"] = "示例：<i> http：//www.youtube.com/watch?v=oacnzgr9mu </i> <br/>或<i> www.mysite.com/video/my_video.mp4 </i>";
$MESS["FPF_VIDEO"] = "插入視頻";
$MESS["IDEA_ADD_IDEA_BUTTON_TITLE"] = "建議新主意！";
$MESS["IDEA_DESCRIPTION_TITLE"] = "思想描述";
$MESS["IDEA_TITLE_TITLE"] = "標題";
