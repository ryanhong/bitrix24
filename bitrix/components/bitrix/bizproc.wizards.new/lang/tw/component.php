<?php
$MESS["BPWC_WNC_EMPTY_IBLOCK_TYPE"] = "未指定信息塊類型。";
$MESS["BPWC_WNC_EMPTY_NAME"] = "缺少業務流程名稱。";
$MESS["BPWC_WNC_ERROR"] = "錯誤";
$MESS["BPWC_WNC_ERROR_TMPL"] = "業務過程模板無效。";
$MESS["BPWC_WNC_P"] = "過程";
$MESS["BPWC_WNC_PAGE_NAV_CHAIN"] = "新的業務流程";
$MESS["BPWC_WNC_PAGE_TITLE"] = "＃名稱＃：新業務流程";
$MESS["BPWC_WNC_PNADD"] = "新要求";
$MESS["BPWC_WNC_PS"] = "過程";
$MESS["BPWC_WNC_SESSID"] = "會話安全已被妥協。請再次填寫表格。";
$MESS["BPWC_WNC_WRONG_IBLOCK_TYPE"] = "找不到組件參數中指定的信息塊類型。";
$MESS["BPWC_WNC_WRONG_TMPL"] = "丟失了業務流程模板。";
