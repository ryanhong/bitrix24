<?php
$MESS["PROFILE_URL_TIP"] = "指定用戶配置文件的路徑。";
$MESS["REGISTER_URL_TIP"] = "指定註冊表格的路徑。訪問者可以通過單擊授權表格中的<b>註冊</b>打開此表格。";
