<?php
$MESS["LANDING_CMP_DOMAIN_CREATE_DOMAIN_NAME"] = "提出一個引人入勝的網站地址";
$MESS["LANDING_CMP_DOMAIN_NEED_PAY"] = "請支付您的域名";
$MESS["LANDING_CMP_DOMAIN_NEED_PAY_SOON"] = "不要忘記續訂您的領域";
$MESS["LANDING_CMP_DOMAIN_NEED_PAY_SOON_UNTIL"] = "＃日期＃之前支付您的域名";
$MESS["LANDING_CMP_DOMAIN_WAIT_ACTIVATION"] = "等待域激活";
