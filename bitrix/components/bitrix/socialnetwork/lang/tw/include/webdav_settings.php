<?php
$MESS["IB_WDUF_ADD"] = "添加自定義字段";
$MESS["SN_BP"] = "業務流程：";
$MESS["SN_BP_LABEL"] = "使用完整版的業務流程";
$MESS["SN_BP_NOTE"] = "<b>啟用全功能的業務流程提供以下功能：< /b> <br />
<ul>
<li>業務過程創建和配置; </li>
<li>將文件上傳到未出版的＆laquo;狀態; </li>
<li>分析用戶有權在“允許狀態”中編輯所有文件;權限（用戶組權限參數）; </li>
<li>記錄文檔歷史記錄。</li>
</ul>
<b>禁用全功能的業務流程將對文檔庫功能施加某些限制。只有以下功能可用：< /b> <br />
<ul>
<li>上傳文檔已出版＆raquo;狀態; </li>
<li>記錄文檔歷史記錄。</li>
</ul>";
$MESS["SN_TITLE"] = "文檔庫";
$MESS["SN_TITLE_TITLE"] = "文檔庫參數";
$MESS["SONET_FILES"] = "文件";
$MESS["SONET_FILES_IS_NOT_ACTIVE"] = "未啟用\“ files \”功能。";
$MESS["SONET_GROUP_FILES_ACCESS_DENIED"] = "拒絕訪問工作組文件。";
$MESS["SONET_GROUP_NOT_EXISTS"] = "該組不存在。";
$MESS["SONET_GROUP_PREFIX"] = "團體：";
$MESS["SONET_IBLOCK_ID_EMPTY"] = "未指定信息塊。";
$MESS["SONET_IB_MODULE_IS_NOT_INSTALLED"] = "信息塊模塊未安裝。";
$MESS["SONET_UF_USE_BP"] = "使用業務流程";
$MESS["SONET_USER_FILES_ACCESS_DENIED"] = "拒絕訪問用戶文件。";
$MESS["SONET_WD_MODULE_IS_NOT_INSTALLED"] = "未安裝文檔庫模塊。";
$MESS["SONET_WEBDAV_NOT_EXISTS"] = "沒有文件。";
$MESS["WD_TAB15_TITLE"] = "自定義字段";
