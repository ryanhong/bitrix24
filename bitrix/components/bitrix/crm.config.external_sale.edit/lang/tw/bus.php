<?php
$MESS["BPABL_PAGE_TITLE"] = "網站連接";
$MESS["BPWC_WLC_WRONG_BP"] = "沒有找到記錄。";
$MESS["BPWC_WNC_EMPTY_LOGIN"] = "需要“登錄”字段。";
$MESS["BPWC_WNC_EMPTY_PASSWORD"] = "需要“密碼”字段。";
$MESS["BPWC_WNC_EMPTY_SESSID"] = "將CRM連接到在線商店的錯誤。可能需要更新\“ e-store \”模塊。";
$MESS["BPWC_WNC_EMPTY_URL"] = "需要“ URL”字段。";
$MESS["BPWC_WNC_MAX_SHOPS"] = "您的版本不允許添加新的網絡商店。";
$MESS["CRM_PERMISSION_DENIED"] = "您無權訪問此表格。";
