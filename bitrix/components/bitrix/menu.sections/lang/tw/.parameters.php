<?php
$MESS["CP_BMS_DEPTH_LEVEL"] = "顯示的嵌套級別數量";
$MESS["CP_BMS_DETAIL_PAGE_URL"] = "元素詳細信息頁面的URL模板";
$MESS["CP_BMS_IBLOCK_ID"] = "信息塊代碼";
$MESS["CP_BMS_IBLOCK_TYPE"] = "信息塊類型";
$MESS["CP_BMS_ID"] = "菜單的元素ID突出顯示";
$MESS["CP_BMS_IS_SEF"] = "啟用S​​EF模式兼容性";
$MESS["CP_BMS_SECTION_PAGE_URL"] = "截面頁面的URL模板";
$MESS["CP_BMS_SECTION_URL"] = "頁面的URL帶有部分內容";
$MESS["CP_BMS_SEF_BASE_URL"] = "SEF的文件夾（站點根搭配）";
