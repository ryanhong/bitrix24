<?php
$MESS["P_ADD_ALBUM"] = "新唱片";
$MESS["P_ALBUM_IS_NOT_ACTIVE"] = "專輯是隱藏的。";
$MESS["P_ALBUM_IS_NOT_ACTIVE_AND_PASSWORDED"] = "專輯被隱藏並受到密碼保護。";
$MESS["P_ALBUM_IS_PASSWORDED"] = "專輯受密碼保護。";
$MESS["P_SECTION_DELETE"] = "刪除專輯";
$MESS["P_SECTION_DELETE_ASK"] = "確定您想不可逆轉地刪除專輯嗎？";
$MESS["P_SECTION_EDIT"] = "編輯專輯";
$MESS["P_SECTION_EDIT_ICON"] = "編輯專輯封面";
$MESS["P_UPLOAD"] = "上傳照片";
