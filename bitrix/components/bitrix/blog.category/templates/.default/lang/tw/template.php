<?php
$MESS["BLOG_ADD"] = "添加";
$MESS["BLOG_CANCEL"] = "取消";
$MESS["BLOG_CONFIRM_DELETE"] = "帖子包含此標籤。您確定要刪除此標籤嗎？";
$MESS["BLOG_GROUP_ADD"] = "添加郵政類別";
$MESS["BLOG_GROUP_DELETE"] = "刪除類別";
$MESS["BLOG_GROUP_NAME"] = "標籤名稱：";
$MESS["BLOG_NAME_CHANGE"] = "更改標籤名稱";
