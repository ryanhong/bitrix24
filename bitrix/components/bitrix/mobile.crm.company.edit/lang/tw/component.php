<?php
$MESS["CRM_BUTTON_SELECT"] = "選擇";
$MESS["CRM_COMPANY_EDIT_NOT_FOUND"] = "找不到公司## ID＃。";
$MESS["CRM_COMPANY_EDIT_RESPONSIBLE_NOT_ASSIGNED"] = "[未分配]";
$MESS["CRM_FIELD_ADDRESS"] = "街道地址";
$MESS["CRM_FIELD_ADDRESS_LEGAL"] = "合法地址";
$MESS["CRM_FIELD_ASSIGNED_BY_ID"] = "負責人";
$MESS["CRM_FIELD_BANKING_DETAILS"] = "銀行資料";
$MESS["CRM_FIELD_BP_EMPTY_EVENT"] = "不要跑";
$MESS["CRM_FIELD_BP_EVENTS"] = "當前狀態";
$MESS["CRM_FIELD_BP_PARAMETERS"] = "工作流參數";
$MESS["CRM_FIELD_BP_STATE_MODIFIED"] = "當前狀態日期";
$MESS["CRM_FIELD_BP_STATE_NAME"] = "當前狀態";
$MESS["CRM_FIELD_BP_TEMPLATE_DESC"] = "描述";
$MESS["CRM_FIELD_BP_TEXT"] = "文字";
$MESS["CRM_FIELD_COMMENTS"] = "評論";
$MESS["CRM_FIELD_COMPANY_EVENT"] = "公司活動";
$MESS["CRM_FIELD_COMPANY_TYPE"] = "公司類型";
$MESS["CRM_FIELD_CONTACT_ID"] = "公司的聯繫";
$MESS["CRM_FIELD_CONTACT_ID_START_TEXT"] = "（輸入聯繫人的名稱）";
$MESS["CRM_FIELD_CURRENCY_ID"] = "貨幣";
$MESS["CRM_FIELD_EMAIL"] = "電子郵件";
$MESS["CRM_FIELD_EMAIL_PRINTABLE"] = "電子郵件（文字）";
$MESS["CRM_FIELD_EMPLOYEES"] = "僱員";
$MESS["CRM_FIELD_FIND"] = "搜尋";
$MESS["CRM_FIELD_ID"] = "ID";
$MESS["CRM_FIELD_INDUSTRY"] = "行業";
$MESS["CRM_FIELD_LEAD_ID"] = "帶領";
$MESS["CRM_FIELD_LOGO"] = "標識";
$MESS["CRM_FIELD_MESSENGER"] = "信使";
$MESS["CRM_FIELD_MESSENGER_PRINTABLE"] = "im（文字）";
$MESS["CRM_FIELD_OPENED"] = "每個人都可以使用";
$MESS["CRM_FIELD_OPENED_TITLE"] = "該公司是公開的，任何人都可以觀看。";
$MESS["CRM_FIELD_ORIGINATOR_ID"] = "電子商店";
$MESS["CRM_FIELD_ORIGIN_ID"] = "電子商店客戶";
$MESS["CRM_FIELD_PHONE"] = "電話";
$MESS["CRM_FIELD_PHONE_PRINTABLE"] = "電話（文字）";
$MESS["CRM_FIELD_REVENUE"] = "年收入";
$MESS["CRM_FIELD_TITLE"] = "公司名稱";
$MESS["CRM_FIELD_WEB"] = "網站";
$MESS["CRM_FIELD_WEB_PRINTABLE"] = "網站（文字）";
$MESS["CRM_MOBILE_MODULE_NOT_INSTALLED"] = "未安裝\“移動\”模塊。";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
