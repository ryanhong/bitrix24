<?php
$MESS["NAME_TIP"] = "指定標籤輸入表格的標題。";
$MESS["VALUE_TIP"] = "指定標籤輸入表格的初始內容。";
$MESS["arrFILTER_TIP"] = "允許縮小搜索區域。例如，您可以指定僅搜索靜態文件。";
