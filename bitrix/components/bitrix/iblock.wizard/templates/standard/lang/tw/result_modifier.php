<?php
$MESS["WZ_ACCEPTED"] = "優惠券接受了。其餘用途：";
$MESS["WZ_COUPON"] = "優惠券";
$MESS["WZ_COUPON_ERR0"] = "優惠券無效。單擊“返回”要輸入另一個優惠券或“下一步”創建一張普通票。";
$MESS["WZ_COUPON_ERR1"] = "優惠券已過期：＃date_active＃。單擊“返回”要輸入另一個優惠券或“下一步”創建一張普通票。";
$MESS["WZ_COUPON_ERR2"] = "優惠券的所有應用都已使用。單擊“返回”要輸入另一個優惠券或“下一步”創建一張普通票。";
$MESS["WZ_MESS_DETAIL"] = "如果您擁有此類優惠券，請鍵入特殊票務優惠券代碼。";
$MESS["WZ_MESS_PREVIEW"] = "僅適用於優惠券的門票";
$MESS["WZ_MESS_TITLE"] = "門票標題";
