<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
$MESS["CRM_PS_ADD"] = "添加";
$MESS["CRM_PS_ADD_TITLE"] = "添加新的付款系統";
$MESS["CRM_PS_DELETE"] = "刪除";
$MESS["CRM_PS_DELETE_DLG_BTNTITLE"] = "刪除";
$MESS["CRM_PS_DELETE_DLG_MESSAGE"] = "您確定要刪除付款系統嗎？";
$MESS["CRM_PS_DELETE_DLG_TITLE"] = "刪除付款系統";
$MESS["CRM_PS_DELETE_TITLE"] = "刪除付款系統";
$MESS["CRM_PS_LIST"] = "支付系統";
$MESS["CRM_PS_LIST_TITLE"] = "查看所有支付系統";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "沒有安裝電子商店模塊。";
