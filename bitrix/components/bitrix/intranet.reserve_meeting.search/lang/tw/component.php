<?php
$MESS["EC_IBLOCK_ID_EMPTY"] = "未選擇信息塊";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "信息塊模塊未安裝。";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "Intranet模塊未安裝。";
$MESS["INAF_F_DESCRIPTION"] = "描述";
$MESS["INAF_F_FLOOR"] = "地面";
$MESS["INAF_F_ID"] = "ID";
$MESS["INAF_F_NAME"] = "標題";
$MESS["INAF_F_PHONE"] = "電話";
$MESS["INAF_F_PLACE"] = "座位";
$MESS["INTASK_C36_PAGE_TITLE"] = "會議室搜索";
$MESS["INTS_NO_IBLOCK_PERMS"] = "您無權查看會議室信息塊。";
