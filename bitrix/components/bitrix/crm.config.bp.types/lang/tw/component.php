<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "未安裝業務流程模塊。";
$MESS["CRM_BP_COMPANY"] = "公司";
$MESS["CRM_BP_COMPANY_DESC"] = "公司工作流模板";
$MESS["CRM_BP_CONTACT"] = "接觸";
$MESS["CRM_BP_CONTACT_DESC"] = "聯繫工作流模板";
$MESS["CRM_BP_DEAL"] = "交易";
$MESS["CRM_BP_DEAL_DESC"] = "交易工作流模板";
$MESS["CRM_BP_DYNAMIC_DESC"] = "＃dynamic_type_name＃工作流模板";
$MESS["CRM_BP_ENTITY_LIST"] = "類型";
$MESS["CRM_BP_LEAD"] = "帶領";
$MESS["CRM_BP_LEAD_DESC"] = "線索工作流模板";
$MESS["CRM_BP_QUOTE_DESC_MSGVER_1"] = "估計工作流模板";
$MESS["CRM_BP_SMART_INVOICE_DESC"] = "發票工作流模板";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
