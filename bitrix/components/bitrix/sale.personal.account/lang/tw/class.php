<?php
$MESS["SALE_ACCESS_DENIED"] = "您必須授權查看帳戶";
$MESS["SALE_MODULE_NOT_INSTALL"] = "E商店模塊未安裝";
$MESS["SPA_IN_CUR"] = "在<b> #currency＃</b>  -  <b> #sum＃</b>";
$MESS["SPA_MY_ACCOUNT"] = "截至＃＃日期＃，您的用戶帳戶具有以下資金：";
$MESS["SPA_NO_ACCOUNT"] = "您沒有用戶帳戶";
$MESS["SPA_TITLE"] = "我的賬戶";
