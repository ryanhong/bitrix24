<?php
$MESS["IBLOCK_ELEMENT_SORT_FIELD"] = "排序照片的字段";
$MESS["IBLOCK_ELEMENT_SORT_ORDER"] = "排序照片";
$MESS["IBLOCK_GALLERY_URL"] = "畫廊內容";
$MESS["IBLOCK_IBLOCK"] = "信息塊";
$MESS["IBLOCK_INDEX_URL"] = "索引頁";
$MESS["IBLOCK_SECTION_CODE"] = "部分代碼";
$MESS["IBLOCK_SECTION_ID"] = "章節ID";
$MESS["IBLOCK_SECTION_URL"] = "頁面的URL帶有部分內容";
$MESS["IBLOCK_TYPE"] = "信息塊類型";
$MESS["P_ALBUM_PHOTO_THUMBS_WIDTH"] = "縮略圖寬度（PX）";
$MESS["P_ALBUM_PHOTO_WIDTH"] = "專輯照片寬度（PX）";
$MESS["P_BEHAVIOUR"] = "畫廊模式";
$MESS["P_SET_STATUS_404"] = "設置404狀態，如果找不到元素或部分";
$MESS["P_USER_ALIAS"] = "畫廊代碼";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "顯示此組件的面板按鈕";
