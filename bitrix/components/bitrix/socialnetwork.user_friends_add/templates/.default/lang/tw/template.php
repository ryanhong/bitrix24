<?php
$MESS["SONET_C34_T_MESSAGE"] = "您的留言";
$MESS["SONET_C34_T_PROMT"] = "朋友請求將發送給此用戶。一旦他們接受您的要求，他們將被列為您的朋友。";
$MESS["SONET_C34_T_SAVE"] = "發信息";
$MESS["SONET_C34_T_SUCCESS"] = "您的申請已經提交。用戶接受您的請求後，它們將被列為您的朋友。";
$MESS["SONET_C34_T_USER"] = "用戶";
