<?php
$MESS["SONET_C34_ALREADY_FRIEND"] = "您已經將此用戶列為您的朋友。";
$MESS["SONET_C34_ALREADY_SEND"] = "您已經向該用戶發送了一個朋友請求。";
$MESS["SONET_C34_IN_BLACK"] = "您在此用戶的禁令列表中。";
$MESS["SONET_C34_NO_FR_FUNC"] = "朋友功能被禁用。";
$MESS["SONET_C34_NO_TEXT"] = "未指定用戶消息。";
$MESS["SONET_C34_PAGE_TITLE"] = "加為好友";
$MESS["SONET_C34_SELF"] = "您不能將自己列為您的朋友。";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶。";
