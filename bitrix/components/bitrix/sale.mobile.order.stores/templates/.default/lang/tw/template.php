<?php
$MESS["SMOS_BACK"] = "後退";
$MESS["SMOS_CHECK_ERROR"] = "所需的項目數量和訂單項目數量不同。";
$MESS["SMOS_MUST_DEDUCT"] = "履行產品數量";
$MESS["SMOS_PRODUCT_AMOUNT"] = "可用（PC）";
$MESS["SMOS_PRODUCT_NAME"] = "產品名稱";
$MESS["SMOS_PRODUCT_QUANTITY"] = "船（PC）";
$MESS["SMOS_SAVE"] = "節省";
