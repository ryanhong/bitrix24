<?php
$MESS["SALE_DLV_SRV_SPSR_LOC_MAP"] = "地圖";
$MESS["SALE_LOCATION_MAP_ALL"] = "映射之前刪除現有映射";
$MESS["SALE_LOCATION_MAP_ALL_B"] = "映射所有位置";
$MESS["SALE_LOCATION_MAP_CANCEL"] = "取消";
$MESS["SALE_LOCATION_MAP_CLOSE"] = "關閉";
$MESS["SALE_LOCATION_MAP_LOC_COUNT"] = "註冊的位置";
$MESS["SALE_LOCATION_MAP_LOC_MAPPED"] = "位置映射";
$MESS["SALE_LOCATION_MAP_LOC_MAPPING"] = "地圖位置";
$MESS["SALE_LOCATION_MAP_NEW"] = "地圖未映射的位置";
$MESS["SALE_LOCATION_MAP_NEW_B"] = "地圖新位置";
$MESS["SALE_LOCATION_MAP_PREPARING"] = "準備";
