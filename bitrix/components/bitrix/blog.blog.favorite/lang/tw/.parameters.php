<?php
$MESS["BB_BLOG_URL"] = "博客URL";
$MESS["BB_BLOG_VAR"] = "博客標識符變量";
$MESS["BB_PAGE_VAR"] = "頁面變量";
$MESS["BB_PATH_TO_POST"] = "博客消息頁的模板";
$MESS["BB_PATH_TO_SMILE"] = "相對於站點根的路徑，帶有笑臉的路徑";
$MESS["BB_POST_VAR"] = "博客消息標識符變量";
$MESS["BC_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["BMNP_MESSAGE_COUNT"] = "消息計數";
$MESS["BMNP_MESSAGE_LENGTH"] = "最大消息顯示長度";
$MESS["BMNP_PREVIEW_HEIGHT"] = "預覽圖像高度";
$MESS["BMNP_PREVIEW_WIDTH"] = "預覽圖像寬度";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
