<?php
$MESS["SPACES_DISCUSSIONS_SMART_TRACKING_ACTIVE_HINT"] = "智能關注模式即將開始。只有您作為作者，收件人或文本中提到的帖子才會移至頂部。如果添加評論，您將訂閱所有帖子的更新。";
