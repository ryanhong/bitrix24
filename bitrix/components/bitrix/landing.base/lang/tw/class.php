<?php
$MESS["LANDING_CMP_NOT_INSTALLED"] = "目前不可用的著陸頁服務";
$MESS["LANDING_ERROR_ACCESS_DENIED"] = "拒絕訪問";
$MESS["LANDING_ERROR_PAGE_NOT_FOUND"] = "沒有找到頁面或訪問被拒絕。";
$MESS["LANDING_ERROR_SESS_EXPIRED"] = "您的會議已經過期。";
$MESS["LANDING_ERROR_SETTINGS_ACCESS_DENIED_MSGVER_1"] = "權限不足<br> <div style = \“ font-size：20px; \”>請聯繫您的主管或Bitrix24管理員。</div>";
$MESS["LANDING_GOTO_EDIT"] = "編輯設置";
