<?php
$MESS["SONET_C35_NOT_FRIEND"] = "您不會將此用戶列為您的朋友。";
$MESS["SONET_C35_NO_FR_FUNC"] = "朋友功能被禁用。";
$MESS["SONET_C35_PAGE_TITLE"] = "刪除朋友";
$MESS["SONET_C35_SELF"] = "你不能解脫自己。";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶。";
