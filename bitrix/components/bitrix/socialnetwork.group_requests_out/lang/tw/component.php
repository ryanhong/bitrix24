<?php
$MESS["SONET_C12_CANT_DELETE_INVITATION"] = "無法將邀請刪除到用戶ID =";
$MESS["SONET_C12_CANT_DELETE_INVITATION_LOG"] = "無法刪除與邀請有關的日誌條目。";
$MESS["SONET_C12_CANT_INVITE"] = "您無權邀請用戶參加此組。";
$MESS["SONET_C12_CANT_REJECT"] = "您無權刪除此組的用戶邀請。";
$MESS["SONET_C12_NAV"] = "用戶";
$MESS["SONET_C12_NOT_SELECTED"] = "沒有選擇用戶。";
$MESS["SONET_C12_NO_PERMS"] = "您無權查看此組。";
$MESS["SONET_C12_TITLE"] = "邀請小組";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_USER_NO_GROUP"] = "找不到小組。";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶。";
