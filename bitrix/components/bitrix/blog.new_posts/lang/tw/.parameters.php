<?php
$MESS["BC_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["BLG_BLOG_URL"] = "博客URL";
$MESS["BLG_GROUP_ID"] = "博客小組";
$MESS["BMNP_BLOG_VAR"] = "博客標識符變量";
$MESS["BMNP_MESSAGE_COUNT"] = "每頁消息";
$MESS["BMNP_MESSAGE_LENGTH"] = "最大消息顯示長度";
$MESS["BMNP_PAGE_VAR"] = "頁面變量";
$MESS["BMNP_PATH_TO_BLOG"] = "博客頁面路徑模板";
$MESS["BMNP_PATH_TO_GROUP_BLOG_POST"] = "小組博客文章頁面URL模板";
$MESS["BMNP_PATH_TO_POST"] = "博客消息頁的模板";
$MESS["BMNP_PATH_TO_SMILE"] = "相對於站點根的路徑，帶有笑臉的路徑";
$MESS["BMNP_PATH_TO_USER"] = "博客用戶頁面路徑的模板";
$MESS["BMNP_POST_VAR"] = "博客消息標識符變量";
$MESS["BMNP_PREVIEW_HEIGHT"] = "預覽圖像高度";
$MESS["BMNP_PREVIEW_WIDTH"] = "預覽圖像寬度";
$MESS["BMNP_USER_VAR"] = "博客用戶標識符變量";
$MESS["BMNP_USE_SOCNET"] = "在社交網絡中使用";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
