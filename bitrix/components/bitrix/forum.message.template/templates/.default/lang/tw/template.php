<?php
$MESS["F_ANCHOR"] = "鏈接到這篇文章";
$MESS["F_ATTACH_FILES"] = "附加的文件";
$MESS["F_AUTHOR_PROFILE"] = "用戶資料";
$MESS["F_DATE_REGISTER"] = "加入：";
$MESS["F_DELETE"] = "刪除";
$MESS["F_DELETE_CONFIRM"] = "該消息將被不可逆轉地刪除。繼續？";
$MESS["F_DELETE_TOPIC"] = "刪除主題";
$MESS["F_DELETE_TOPIC_CONFIRM"] = "這將不可逆轉地刪除主題。繼續？";
$MESS["F_EDIT"] = "調整";
$MESS["F_EDIT_HEAD"] = "編輯：";
$MESS["F_EDIT_TOPIC"] = "修改主題";
$MESS["F_EMAIL_TITLE"] = "發送電子郵件";
$MESS["F_GOTO"] = "去";
$MESS["F_HIDE"] = "隱藏";
$MESS["F_HIDE_TOPIC"] = "隱藏主題";
$MESS["F_INSERT_NAME"] = "答案中粘貼用戶名";
$MESS["F_NAME"] = "姓名";
$MESS["F_NO_VOTE_DO"] = "投票";
$MESS["F_NO_VOTE_UNDO"] = "取消投票";
$MESS["F_NUM_MESS"] = "文章：";
$MESS["F_POINTS"] = "點：";
$MESS["F_PRIVATE_MESSAGE"] = "訊息";
$MESS["F_PRIVATE_MESSAGE_TITLE"] = "發送私人消息";
$MESS["F_QUOTE"] = "引用";
$MESS["F_QUOTE_HINT"] = "引用回复表格中的消息選擇文本，然後單擊此處";
$MESS["F_REAL_IP"] = "/ 真實的";
$MESS["F_REPLY"] = "回覆";
$MESS["F_SHOW"] = "展示";
$MESS["F_SHOW_TOPIC"] = "展示主題";
$MESS["F_USER_ID"] = "id賓客";
$MESS["F_USER_ID_USER"] = "ID用戶";
$MESS["F_USER_PROFILE"] = "用戶資料";
