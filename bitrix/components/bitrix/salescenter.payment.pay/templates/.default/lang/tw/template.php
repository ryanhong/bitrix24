<?php
$MESS["SPP_CHECK_PRINT_TITLE"] = "收據## ID＃of＃date_create＃將很快創建";
$MESS["SPP_CHECK_TITLE"] = "查看收據## check_id＃of＃date_create＃＃";
$MESS["SPP_EMPTY_TEMPLATE_FOOTER"] = "請選擇其他付款系統以在線付款";
$MESS["SPP_EMPTY_TEMPLATE_PAY_SYSTEM_NAME_FIELD"] = "付款方式：";
$MESS["SPP_EMPTY_TEMPLATE_SUM_WITH_CURRENCY_FIELD"] = "合計訂單：";
$MESS["SPP_EMPTY_TEMPLATE_TITLE"] = "謝謝您的訂單！";
$MESS["SPP_INFO_BUTTON"] = "資訊";
$MESS["SPP_INITIATE_PAY_ERROR_TEXT"] = "不幸的是，我們遇到了一個錯誤。請選擇其他付款方式或聯繫銷售代表。";
$MESS["SPP_INITIATE_PAY_ERROR_TEXT_FOOTER"] = "請選擇其他付款方式或聯繫銷售代表。";
$MESS["SPP_INITIATE_PAY_ERROR_TEXT_HEADER"] = "不幸的是，我們遇到了一個錯誤。";
$MESS["SPP_PAID"] = "有薪酬的";
$MESS["SPP_PAID_TITLE"] = "付款## account_number＃of＃date_insert＃";
$MESS["SPP_PAY_BUTTON"] = "支付";
$MESS["SPP_PAY_RELOAD_BUTTON_NEW"] = "選擇另一種方法";
$MESS["SPP_SELECT_PAYMENT_TITLE_NEW_NEW_MSGVER_1"] = "選擇付款方式";
$MESS["SPP_SUM"] = "金額：＃sum＃";
