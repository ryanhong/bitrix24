<?php
$MESS["SONET_ITEMS_COUNT"] = "列表中的項目計數";
$MESS["SONET_PAGE_VAR"] = "頁面變量";
$MESS["SONET_PATH_TO_MESSAGES_OUTPUT"] = "傳出消息頁路徑模板";
$MESS["SONET_PATH_TO_MESSAGES_OUTPUT_USER"] = "用戶過濾傳出消息頁面的路徑模板頁面";
$MESS["SONET_PATH_TO_MESSAGE_FORM"] = "消息帖子形式路徑模板";
$MESS["SONET_PATH_TO_SMILE"] = "笑臉文件夾的路徑（相對於站點根）";
$MESS["SONET_PATH_TO_USER"] = "用戶配置文件路徑模板";
$MESS["SONET_SET_NAVCHAIN"] = "設置麵包屑";
$MESS["SONET_USER_ID"] = "用戶身份";
$MESS["SONET_USER_VAR"] = "用戶變量";
$MESS["SONET_VARIABLE_ALIASES"] = "可變別名";
