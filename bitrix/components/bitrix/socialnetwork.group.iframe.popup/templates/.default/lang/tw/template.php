<?php
$MESS["SONET_GROUP_NAME"] = "組：＃group_name＃";
$MESS["SONET_GROUP_TITLE_CREATE"] = "新小組";
$MESS["SONET_GROUP_TITLE_EDIT"] = "編輯組＃group_name＃";
$MESS["SONET_GROUP_TITLE_FEATURES"] = "配置＃group_name＃的設置";
$MESS["SONET_GROUP_TITLE_INVITE"] = "邀請到＃group_name＃";
