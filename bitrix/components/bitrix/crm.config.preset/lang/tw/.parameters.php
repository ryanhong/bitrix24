<?php
$MESS["CRM_PRESET_ENTITY_TYPE_ID"] = "類型ID";
$MESS["CRM_PRESET_ID"] = "模板ID";
$MESS["CRM_PRESET_LIST"] = "聯繫或公司詳細信息模板URL";
$MESS["CRM_PRESET_MODE"] = "模式";
$MESS["CRM_PRESET_UFIELDS"] = "未使用的自定義字段列表URL";
