<?php
$MESS["CRM_DEAL_CONVERSION_ID_NOT_DEFINED"] = "沒有找到交易ID。";
$MESS["CRM_DEAL_CONVERSION_NOT_FOUND"] = "沒有找到交易。";
$MESS["CRM_DEAL_DELETION_ERROR"] = "錯誤刪除交易。";
$MESS["CRM_DEAL_MOVE_TO_CATEGORY_ERROR"] = "無法將交易遷移到新管道。只有一旦所有活動工作流程完成執行，就只能遷移交易。您可以在業務流程選項卡中停止或完成工作流程。另外，請確保您想遷移的交易已分配給其負責人，並設定了交易階段。";
$MESS["CRM_DEAL_PRODUCT_ROWS_SAVING_ERROR"] = "保存產品時發生了錯誤。";
$MESS["CRM_DEAL_RECURRING_DATE_START_ERROR"] = "下一個創建日期比當前日期早。請更改重複的交易參數。";
