<?php
$MESS["CRM_INVOICE_CUSTOM_SAVE_BUTTON_TITLE"] = "保存並返回";
$MESS["CRM_INVOICE_PS_PROPS_CONTENT"] = "要開始使用發票工作，請提供將在發票中使用的公司信息。";
$MESS["CRM_INVOICE_PS_PROPS_GOTO"] = "添加公司詳細信息";
$MESS["CRM_INVOICE_PS_PROPS_TITLE"] = "公司詳細信息";
$MESS["CRM_INVOICE_RECUR_SHOW_TITLE"] = "重複出現的發票## Account_number＃＆mdash; ＃order_topic＃";
$MESS["CRM_INVOICE_SHOW_LEGEND"] = "發票## Account_number＃";
$MESS["CRM_INVOICE_SHOW_NEW_TITLE"] = "新發票";
$MESS["CRM_INVOICE_SHOW_TITLE"] = "發票## Account_number＃＆mdash; ＃order_topic＃";
$MESS["CRM_TAB_1"] = "發票";
$MESS["CRM_TAB_1_TITLE"] = "發票參數";
