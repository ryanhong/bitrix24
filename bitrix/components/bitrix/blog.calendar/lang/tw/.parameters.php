<?php
$MESS["BC_BLOG_URL"] = "博客URL";
$MESS["BC_BLOG_VAR"] = "博客標識符變量";
$MESS["BC_DAY"] = "白天過濾";
$MESS["BC_MONTH"] = "按月過濾";
$MESS["BC_PAGE_VAR"] = "頁面變量";
$MESS["BC_PATH_TO_BLOG"] = "博客頁面路徑模板";
$MESS["BC_YEAR"] = "每年過濾";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
