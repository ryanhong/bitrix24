<?php
$MESS["SONET_GUE_NAME_TEMPLATE_DEFAULT"] = "＃NOBR ##名稱＃#last_name ##/nobr＃";
$MESS["SONET_GUE_NO_PERMS"] = "您無權查看此組。";
$MESS["SONET_GUE_NO_PERMS_PROJECT"] = "您無權刪除此項目。";
$MESS["SONET_GUE_PAGE_TITLE"] = "小組成員";
$MESS["SONET_GUE_PAGE_TITLE_PROJECT"] = "項目成員";
$MESS["SONET_GUE_PAGE_TITLE_SCRUM"] = "Scrum團隊成員";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_USER_NO_GROUP"] = "找不到小組。";
