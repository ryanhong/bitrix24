<?php
$MESS["BPWC_WLCT_BP"] = "業務流程";
$MESS["BPWC_WLCT_F_ACT"] = "動作";
$MESS["BPWC_WLCT_F_AUTHOR"] = "作者";
$MESS["BPWC_WLCT_F_EVENTS"] = "事件";
$MESS["BPWC_WLCT_F_NAME"] = "姓名";
$MESS["BPWC_WLCT_F_STATE"] = "地位";
$MESS["BPWC_WLCT_F_TASKS"] = "任務";
$MESS["BPWC_WLCT_LIST"] = "列表";
$MESS["BPWC_WLCT_NEW"] = "新要求";
$MESS["BPWC_WLCT_SAVE"] = "節省";
$MESS["BPWC_WLCT_STOP"] = "停止";
$MESS["BPWC_WLCT_TOTAL"] = "全部的";
$MESS["BPWC_WLCT_VARS"] = "業務流程變量";
