<?php
$MESS["BPWC_WLCT_F_AUTHOR"] = "作者";
$MESS["BPWC_WLCT_F_EVENTS"] = "事件";
$MESS["BPWC_WLCT_F_NAME"] = "姓名";
$MESS["BPWC_WLCT_F_STATE"] = "地位";
$MESS["BPWC_WLCT_F_TASKS"] = "任務";
$MESS["BPWC_WLCT_SAVE"] = "發送";
$MESS["BPWC_WLC_EMPTY_IBLOCK"] = "未指定信息塊ID。";
$MESS["BPWC_WLC_EMPTY_IBLOCK_TYPE"] = "未指定信息塊類型。";
$MESS["BPWC_WLC_ERROR"] = "錯誤";
$MESS["BPWC_WLC_MISSING_BP"] = "找不到業務流程。";
$MESS["BPWC_WLC_MISSING_DOCUMENT"] = "找不到元素。";
$MESS["BPWC_WLC_NOT_DETAIL"] = "細節";
$MESS["BPWC_WLC_NOT_SET"] = "[沒有設置]";
$MESS["BPWC_WLC_WRONG_IBLOCK"] = "找不到信息塊。";
$MESS["BPWC_WLC_WRONG_IBLOCK_TYPE"] = "找不到組件參數中指定的信息塊類型。";
$MESS["INTS_TASKS_NAV"] = "業務流程";
$MESS["JHGFDC_STOP"] = "停止";
$MESS["JHGFDC_STOP_ALT"] = "您確定要停止該過程嗎？";
$MESS["JHGFDC_STOP_DELETE"] = "刪除";
$MESS["JHGFDC_STOP_DELETE_ALT"] = "您確定要刪除此過程嗎？";
