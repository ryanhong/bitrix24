<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：緩存在緩存設置中設置的時間; <br /> <i> cache < /i>：始終在旁邊指定的時期; <br /> <br /> <i>不緩存</i>：沒有執行緩存。";
$MESS["ELEMENT_NAME_TIP"] = "在此處指定將傳遞元素名稱的變量的名稱。";
$MESS["PATH_TO_SMILE_TIP"] = "笑臉夾路徑（相對於站點根）";
$MESS["PATH_TO_USER_TIP"] = "通往用戶配置文件頁面的路徑。例如：user.php？user_id =＃user_id＃＃";
$MESS["SET_TITLE_TIP"] = "檢查此選項將將頁面標題設置為基於Wiki的標題。";
