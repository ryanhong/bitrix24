<?php
$MESS["CC_BLL_ACCESS_DENIED"] = "未足夠查看和編輯列表的權限。";
$MESS["CC_BLL_COLUMN_BIZPROC"] = "業務流程";
$MESS["CC_BLL_COLUMN_SECTION"] = "部分";
$MESS["CC_BLL_COMMENTS"] = "評論";
$MESS["CC_BLL_DOWNLOAD"] = "可供下載";
$MESS["CC_BLL_ENLARGE"] = "放大";
$MESS["CC_BLL_LISTS_FOR_SONET_GROUP_DISABLED"] = "該組禁用列表。";
$MESS["CC_BLL_MODULE_NOT_INSTALLED"] = "未安裝通用列表模塊。";
$MESS["CC_BLL_UNKNOWN_ERROR"] = "未知錯誤。";
$MESS["CC_BLL_UPPER_LEVEL"] = "更高級別";
$MESS["CC_BLL_WRONG_IBLOCK"] = "指定的列表不正確。";
$MESS["CC_BLL_WRONG_IBLOCK_TYPE"] = "不正確的信息塊類型。";
