<?php
$MESS["FP_CHG_REG_INFO"] = "更改註冊信息";
$MESS["FP_ERR_INTERN"] = "內部錯誤＃HZ1257";
$MESS["FP_ERR_PROF"] = "錯誤修改配置文件";
$MESS["FP_NO_AUTHORIZE"] = "您沒有權限";
$MESS["FP_SEX_FEMALE"] = "女性";
$MESS["FP_SEX_MALE"] = "男性";
$MESS["F_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["F_ERR_SESSION_TIME_IS_UP"] = "您的會議已經過期。請重新發布您的消息。";
$MESS["F_NO_MODULE"] = "論壇模塊未安裝";
$MESS["F_TITLE"] = "編輯個人資料";
$MESS["F_TITLE_TITLE"] = "編輯個人資料";
$MESS["F_USER_NOT_FOUND"] = "找不到用戶。";
