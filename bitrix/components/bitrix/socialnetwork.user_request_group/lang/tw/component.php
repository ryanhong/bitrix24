<?php
$MESS["SONET_C39_ALREADY_JOINED"] = "您已經加入了這個小組。";
$MESS["SONET_C39_ALREADY_MEMBER"] = "您已經是這個小組的成員。";
$MESS["SONET_C39_CANT_VIEW"] = "您不能加入這個組。";
$MESS["SONET_C39_NO_TEXT"] = "消息文本為空。";
$MESS["SONET_C39_PAGE_TITLE"] = "小組會員資格請求";
$MESS["SONET_C39_PAGE_TITLE_PROJECT"] = "項目會員資格要求";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_USER_NO_GROUP"] = "找不到小組。";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶。";
$MESS["SONET_P_USER_TITLE_VIEW"] = "用戶資料";
