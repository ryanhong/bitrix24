<?php
$MESS["ADD_TO_BASKET_OK"] = "添加到您的購物車";
$MESS["CT_CPV_CATALOG_BASKET_UNKNOWN_ERROR"] = "未知錯誤將商品添加到購物車";
$MESS["CT_CPV_CATALOG_BTN_MESSAGE_BASKET_REDIRECT"] = "查看購物車";
$MESS["CT_CPV_CATALOG_BTN_MESSAGE_CLOSE"] = "關閉";
$MESS["CT_CPV_CATALOG_BTN_MESSAGE_CLOSE_POPUP"] = "繼續購物";
$MESS["CT_CPV_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT"] = "比較產品";
$MESS["CT_CPV_CATALOG_BTN_MESSAGE_LAZY_LOAD_WAITER"] = "載入中";
$MESS["CT_CPV_CATALOG_BTN_MESSAGE_SEND_PROPS"] = "選擇";
$MESS["CT_CPV_CATALOG_MESS_COMPARE_OK"] = "產品已添加到比較圖表中";
$MESS["CT_CPV_CATALOG_MESS_COMPARE_TITLE"] = "產品對比";
$MESS["CT_CPV_CATALOG_MESS_COMPARE_UNKNOWN_ERROR"] = "錯誤將產品添加到比較圖表";
$MESS["CT_CPV_CATALOG_PRICE_TOTAL_PREFIX"] = "全部的";
$MESS["CT_CPV_CATALOG_RELATIVE_QUANTITY_FEW"] = "只剩下幾個";
$MESS["CT_CPV_CATALOG_RELATIVE_QUANTITY_MANY"] = "有存貨";
$MESS["CT_CPV_CATALOG_SHOW_MAX_QUANTITY"] = "庫存";
$MESS["CT_CPV_CATALOG_TITLE_BASKET_PROPS"] = "商品屬性將傳遞給購物車";
$MESS["CT_CPV_CATALOG_TITLE_ERROR"] = "錯誤";
$MESS["CT_CPV_TPL_ELEMENT_DELETE_CONFIRM"] = "這將刪除與此記錄相關的所有信息！繼續？";
$MESS["CT_CPV_TPL_MESS_BTN_ADD_TO_BASKET"] = "添加到購物車";
$MESS["CT_CPV_TPL_MESS_BTN_BUY"] = "買";
$MESS["CT_CPV_TPL_MESS_BTN_COMPARE"] = "比較";
$MESS["CT_CPV_TPL_MESS_BTN_DETAIL"] = "細節";
$MESS["CT_CPV_TPL_MESS_BTN_SUBSCRIBE"] = "訂閱";
$MESS["CT_CPV_TPL_MESS_PRODUCT_NOT_AVAILABLE"] = "缺貨";
