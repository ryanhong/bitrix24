<?php
$MESS["SONET_C50_CANT_WRITE"] = "您無法向該用戶發送消息。";
$MESS["SONET_C50_ERR_PERMS"] = "安全錯誤";
$MESS["SONET_C50_NO_TEXT"] = "消息文本為空。";
$MESS["SONET_C50_NO_USER_ID"] = "聯繫用戶ID未指定。";
