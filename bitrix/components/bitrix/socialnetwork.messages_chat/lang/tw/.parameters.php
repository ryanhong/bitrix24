<?php
$MESS["SONET_PAGE_VAR"] = "頁面變量";
$MESS["SONET_PATH_TO_MESSAGES_USERS_MESSAGES"] = "用戶消息頁路徑模板";
$MESS["SONET_PATH_TO_SMILE"] = "笑臉文件夾的路徑（相對於站點根）";
$MESS["SONET_PATH_TO_USER"] = "用戶配置文件路徑模板";
$MESS["SONET_SET_NAVCHAIN"] = "設置麵包屑";
$MESS["SONET_USER_ID"] = "用戶身份";
$MESS["SONET_USER_VAR"] = "用戶變量";
$MESS["SONET_VARIABLE_ALIASES"] = "可變別名";
