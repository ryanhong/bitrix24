<?php
$MESS["SMPL_AMOUNT"] = "數量";
$MESS["SMPL_BALANCE"] = "平衡";
$MESS["SMPL_COUPON"] = "優惠券";
$MESS["SMPL_DISCOUNT"] = "折扣";
$MESS["SMPL_IMAGE_ABSENT"] = "沒有圖像";
$MESS["SMPL_PRICE"] = "價格";
$MESS["SMPL_PROPERTIES"] = "特性";
$MESS["SMPL_RECALCULATION"] = "重新計算";
