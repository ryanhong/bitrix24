<?php
$MESS["CRM_DEAL_CATEGORY_SELECTOR_TITLE"] = "管道";
$MESS["CRM_DEAL_FUNNEL_SHOW_FILTER"] = "顯示/隱藏過濾器";
$MESS["CRM_DEAL_FUNNEL_SHOW_FILTER_SHORT"] = "篩選";
$MESS["CRM_DEAL_FUNNEL_TYPE_SELECTOR_TITLE"] = "漏斗類型";
$MESS["DEAL_STAGES_LOSE"] = "不活躍";
$MESS["DEAL_STAGES_WON"] = "當前活動";
$MESS["FUNNEL_CHART_HIDE"] = "隱藏圖形";
$MESS["FUNNEL_CHART_NO_DATA"] = "沒有數據來創建圖形";
$MESS["FUNNEL_CHART_SHOW"] = "顯示圖形";
