<?php
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAMDIRECT_INSTRUCTION_TITLE_META_RU"] = "<span class = \“ imconnector-field-box-text-bold \”>如何連接您的Instagram*業務帳戶：</span>";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_ADDITIONAL_DESCRIPTION_META_RU"] = "您將必須創建一個公共Facebook*頁面或連接已經擁有的頁面。您的Instagram*業務帳戶必須連接到此Facebook*頁面。只有頁面管理員可以將其連接到Bitrix24。";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_SUBTITLE_META_RU"] = "將您的Instagram*頁面連接到您的Bitrix24，以收到客戶的評論。更快地回复並改善轉換。";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_INDEX_TITLE_META_RU"] = "收到您的Instagram*直接到Bitrix24的評論";
$MESS["IMCONNECTOR_COMPONENT_FBINSTAGRAM_RESTRICTIONS_META_RU_MSGVER_1"] = "*所有者Meta Platforms，Inc。在俄羅斯聯邦禁止使用。";
