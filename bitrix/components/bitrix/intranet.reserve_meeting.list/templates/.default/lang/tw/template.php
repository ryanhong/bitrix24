<?php
$MESS["INTASK_C23T_LOAD"] = "載入中...";
$MESS["INTDT_ACTIONS"] = "動作";
$MESS["INTDT_GRAPH"] = "日程";
$MESS["INTDT_NO_TASKS"] = "沒有會議室";
$MESS["INTST_CANCEL"] = "取消";
$MESS["INTST_CLOSE"] = "關閉";
$MESS["INTST_DELETE"] = "刪除";
$MESS["INTST_FOLDER_NAME"] = "文件夾名稱";
$MESS["INTST_SAVE"] = "節省";
