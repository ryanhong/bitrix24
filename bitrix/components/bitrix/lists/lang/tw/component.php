<?php
$MESS["CC_BLL_ACCESS_DENIDED"] = "拒絕訪問";
$MESS["CC_BLL_ACCESS_DENIDED_FULL"] = "任何足夠大的業務都需要一種存儲和處理大量信息的方法。數據記錄容易被人為錯誤和疏忽。手動處理幾乎可以確保延遲，因此效率低下，最終是不可接受的。
這些列表為此問題提供了整潔且無憂的解決方案。創建自己的存儲結構，並使用自定義工作流程自動處理。這些列表僅在選定的商業計劃中可用。";
$MESS["CC_BLL_ACCESS_DENIDED_MORE"] = "細節";
$MESS["CC_BLL_MODULE_NOT_INSTALLED"] = "未安裝通用列表模塊。";
