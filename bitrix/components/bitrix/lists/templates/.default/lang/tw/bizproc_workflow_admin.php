<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "未安裝業務流程模塊。";
$MESS["CT_BL_ELEMENTS_TITLE"] = "開放列表";
$MESS["CT_BL_SEQ_BIZPROC"] = "順序業務流程";
$MESS["CT_BL_SEQ_BIZPROC_TITLE"] = "順序業務流程是一個簡單的業務流程，可以在文檔上執行一系列連續的操作。";
$MESS["CT_BL_STATE_BIZPROC"] = "國家驅動的業務流程";
$MESS["CT_BL_STATE_BIZPROC_TITLE"] = "國家驅動的業務流程是一個連續的業務流程，具有訪問權限分發，以處理不同狀態的文檔。";
$MESS["CT_BL_TOOLBAR_RETURN_LIST_ELEMENT"] = "返回目錄";
