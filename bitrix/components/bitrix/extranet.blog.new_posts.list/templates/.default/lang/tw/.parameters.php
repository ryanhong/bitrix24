<?php
$MESS["EBMNP_NAME_TEMPLATE"] = "名稱格式";
$MESS["EBMNP_NAME_TEMPLATE_DEFAULT"] = "＃NOBR ##名稱＃#last_name ##/nobr＃";
$MESS["EBMNP_PATH_TO_CONPANY_DEPARTMENT"] = "部門頁面路徑模板";
$MESS["EBMNP_PATH_TO_MESSAGES_CHAT"] = "用戶使者路徑模板";
$MESS["EBMNP_PATH_TO_SONET_USER_PROFILE"] = "社交網絡用戶資料路徑模板";
$MESS["EBMNP_PATH_TO_VIDEO_CALL"] = "視頻通話頁面";
$MESS["EBMNP_SHOW_LOGIN"] = "顯示登錄名，如果沒有必需的用戶名字段";
