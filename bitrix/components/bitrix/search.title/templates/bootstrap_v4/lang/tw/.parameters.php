<?php
$MESS["TP_BST_CONTAINER_ID"] = "佈局容器ID（以寬度限制搜索結果）";
$MESS["TP_BST_CONVERT_CURRENCY"] = "僅使用一種貨幣展示價格";
$MESS["TP_BST_CURRENCY_ID"] = "將所有價格轉換為貨幣";
$MESS["TP_BST_INPUT_ID"] = "搜索查詢輸入元素ID";
$MESS["TP_BST_PREVIEW_HEIGHT"] = "圖像高度";
$MESS["TP_BST_PREVIEW_TRUNCATE_LEN"] = "最大預覽文本長度";
$MESS["TP_BST_PREVIEW_WIDTH"] = "圖像寬度";
$MESS["TP_BST_PRICE_CODE"] = "價格類型";
$MESS["TP_BST_PRICE_VAT_INCLUDE"] = "包括稅率";
$MESS["TP_BST_SHOW_INPUT"] = "顯示搜索查詢輸入表單";
$MESS["TP_BST_SHOW_PREVIEW"] = "顯示圖像";
