<?php
$MESS["CP_BST_CATEGORY_TITLE"] = "分類名稱";
$MESS["CP_BST_CHECK_DATES"] = "僅在搜索日期活動的文檔中搜索";
$MESS["CP_BST_FORM_PAGE"] = "搜索結果頁面（＃site_dir＃可用）";
$MESS["CP_BST_NUM_CATEGORIES"] = "搜索類別的數量";
$MESS["CP_BST_NUM_CATEGORY"] = "類別## num＃";
$MESS["CP_BST_ORDER"] = "排序方式";
$MESS["CP_BST_ORDER_BY_DATE"] = "日期";
$MESS["CP_BST_ORDER_BY_RANK"] = "關聯";
$MESS["CP_BST_OTHERS_CATEGORY"] = "雜項。類別";
$MESS["CP_BST_SHOW_OTHERS"] = "show \“ misc。\”類別";
$MESS["CP_BST_TOP_COUNT"] = "每個類別的結果";
$MESS["CP_BST_USE_LANGUAGE_GUESS"] = "自動檢索鍵盤佈局";
