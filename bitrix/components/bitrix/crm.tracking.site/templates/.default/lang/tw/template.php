<?php
$MESS["CRM_TRACKING_SITE_ADDITIONAL"] = "更多的";
$MESS["CRM_TRACKING_SITE_ADD_EMAIL"] = "添加電子郵件";
$MESS["CRM_TRACKING_SITE_ADD_PHONE"] = "加號碼";
$MESS["CRM_TRACKING_SITE_BTN_ADD"] = "添加";
$MESS["CRM_TRACKING_SITE_BTN_CANCEL"] = "取消";
$MESS["CRM_TRACKING_SITE_BTN_COPY"] = "複製到剪貼板（CTRL + C）";
$MESS["CRM_TRACKING_SITE_BTN_REMOVE"] = "刪除";
$MESS["CRM_TRACKING_SITE_BTN_VIEW"] = "在現場查看";
$MESS["CRM_TRACKING_SITE_CHECK"] = "測試代碼";
$MESS["CRM_TRACKING_SITE_CONNECT"] = "測試網站";
$MESS["CRM_TRACKING_SITE_DESC"] = "將代碼添加到您的網站並跟踪站點事件。在您的網站上獲取有關電話和電子郵件的報告。";
$MESS["CRM_TRACKING_SITE_DISCONNECT"] = "斷開連接站點";
$MESS["CRM_TRACKING_SITE_EMAILS"] = "電子郵件地址";
$MESS["CRM_TRACKING_SITE_ENRICH_TEXT"] = "使文本成為鏈接";
$MESS["CRM_TRACKING_SITE_ENRICH_TEXT_HINT"] = "將電話號碼文本轉換為單擊鏈接。";
$MESS["CRM_TRACKING_SITE_ENRICH_TEXT_HINT1"] = "將找到的電話號碼和電子郵件轉換為可點擊鏈接。";
$MESS["CRM_TRACKING_SITE_ENRICH_TEXT_SHORT"] = "鏈接";
$MESS["CRM_TRACKING_SITE_FOUND"] = "在現場發現";
$MESS["CRM_TRACKING_SITE_FOUND_ITEMS"] = "％phone_count％電話和％email_count％電子郵件";
$MESS["CRM_TRACKING_SITE_FOUND_ITEMS_DESC"] = "編輯列表或手動輸入數字和電子郵件。";
$MESS["CRM_TRACKING_SITE_ITEM_DEMO_EMAIL"] = "請輸入電郵地址";
$MESS["CRM_TRACKING_SITE_ITEM_DEMO_PHONE"] = "輸入號碼";
$MESS["CRM_TRACKING_SITE_NAME"] = "網站URL";
$MESS["CRM_TRACKING_SITE_NAME_INPUT"] = "輸入網站URL";
$MESS["CRM_TRACKING_SITE_OLD_BROWSER"] = "您的瀏覽器已過時。請更新您的瀏覽器以按照設計來使用此功能。";
$MESS["CRM_TRACKING_SITE_PHONES"] = "電話號碼";
$MESS["CRM_TRACKING_SITE_REPLACEMENT_DISABLE"] = "禁用交換";
$MESS["CRM_TRACKING_SITE_REPLACE_TEXT"] = "搜索並替換文字";
$MESS["CRM_TRACKING_SITE_REPLACE_TEXT_HINT"] = "交換鏈接電話號碼以及純文本電話號碼";
$MESS["CRM_TRACKING_SITE_REPLACE_TEXT_SHORT"] = "在文本交換中";
$MESS["CRM_TRACKING_SITE_RESOLVE_DUP"] = "隱藏重複";
$MESS["CRM_TRACKING_SITE_RESOLVE_DUP_HINT"] = "兩個不同的電話號碼將用於一個號碼。如果兩個數字彼此相鄰，則將刪除第二個數字。";
$MESS["CRM_TRACKING_SITE_RESOLVE_DUP_SHORT"] = "重複";
$MESS["CRM_TRACKING_SITE_SCRIPT"] = "您網站的代碼";
$MESS["CRM_TRACKING_SITE_SCRIPT_DESC"] = "在％tag_body％關閉標籤之前，將代碼添加到您的網站";
$MESS["CRM_TRACKING_SITE_SCRIPT_STATUS"] = "地位";
$MESS["CRM_TRACKING_SITE_SCRIPT_STATUS_ERROR"] = "代碼未添加";
$MESS["CRM_TRACKING_SITE_SCRIPT_STATUS_NONE"] = "代碼未測試";
$MESS["CRM_TRACKING_SITE_SCRIPT_STATUS_PROCESS"] = "測試";
$MESS["CRM_TRACKING_SITE_SCRIPT_STATUS_SUCCESS"] = "添加了代碼";
$MESS["CRM_TRACKING_SITE_SUB_DESC"] = "使用電話號碼交換和/或電子郵件交換來跟踪各種流量源的性能。輸入電子郵件和/或電話號碼，並為每個流量來源自動換掉它們。";
$MESS["CRM_TRACKING_SITE_SUB_TITLE"] = "電話號碼和電子郵件交換";
