<?php
$MESS["BM_BIRTHDAY"] = "出生日期";
$MESS["BM_DEPARTMENT"] = "部門";
$MESS["BM_DIRECTOR"] = "下屬";
$MESS["BM_DIRECTOR_OF"] = "監督";
$MESS["BM_NO_USERS"] = "沒有用戶";
$MESS["BM_PHONE"] = "電話";
$MESS["BM_PHONE_INT"] = "內部的";
$MESS["BM_PHONE_MOB"] = "移動的";
$MESS["BM_TO_USER_LIST"] = "返回目錄";
$MESS["BM_USR_CNT"] = "員工人數";
$MESS["BM_WRITE"] = "寫";
