<?php
$MESS["BCMMP_BACK"] = "後退";
$MESS["BCMMP_DOMAINS_TITLE"] = "域";
$MESS["BCMMP_JS_SAVE_ERROR"] = "由於發生錯誤而無法保存。";
$MESS["BCMMP_JS_SAVING"] = "保存...";
$MESS["BCMMP_NO_DOMAINS"] = "沒有配置域";
$MESS["BCMMP_OFF"] = "禁用";
$MESS["BCMMP_ON"] = "使能夠";
$MESS["BCMMP_PUSH_RECIEVE"] = "接收通知";
$MESS["BCMMP_SAVE"] = "節省";
$MESS["BCMMP_TITLE"] = "網站檢查員";
$MESS["BCMMP_TITLE2"] = "配置推送通知";
