<?php
$MESS["NO_OF_COUNT"] = "＃＃of＃總計＃";
$MESS["P_DROP"] = "刪除";
$MESS["P_DROP_CONFIM"] = "您確定要不可逆地刪除照片嗎？";
$MESS["P_DROP_TITLE"] = "刪除圖像";
$MESS["P_EDIT"] = "編輯";
$MESS["P_EDIT_TITLE"] = "編輯圖像屬性";
$MESS["P_GO_TO_NEXT"] = "下一張照片";
$MESS["P_GO_TO_PREV"] = "以前的照片";
$MESS["P_NEXT"] = "下一個";
$MESS["P_ORIGINAL"] = "原來的";
$MESS["P_ORIGINAL_TITLE"] = "原始圖像";
$MESS["P_PREV"] = "後退";
$MESS["P_SLIDE_SHOW"] = "幻燈片";
$MESS["P_SLIDE_SHOW_TITLE"] = "使用這張照片開始幻燈片";
$MESS["P_TAGS"] = "標籤";
$MESS["P_UNKNOWN_ERROR"] = "數據保存錯誤";
