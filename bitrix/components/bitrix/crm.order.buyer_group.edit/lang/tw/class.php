<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "沒有安裝電子商店模塊。";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_COLUMN_ACTIVE"] = "積極的";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_COLUMN_ANONYMOUS"] = "匿名的";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_COLUMN_C_SORT"] = "種類";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_COLUMN_DESCRIPTION"] = "描述";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_COLUMN_ID"] = "ID";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_COLUMN_NAME"] = "姓名";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_CREATE_TITLE"] = "創建客戶群";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_SAVE_ERROR"] = "保存客戶時發生錯誤。";
$MESS["CRM_ORDER_BUYER_GROUP_EDIT_TITLE"] = "編輯客戶組";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
