<?php
$MESS["F_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["F_DEFAULT_FID"] = "論壇ID";
$MESS["F_DEFAULT_MID"] = "消息ID";
$MESS["F_DISPLAY_PANEL"] = "顯示此組件的面板按鈕";
$MESS["F_INDEX_TEMPLATE"] = "論壇列表頁面";
$MESS["F_LIST_TEMPLATE"] = "主題列表頁面";
$MESS["F_MESSAGE_TEMPLATE"] = "消息查看頁面";
$MESS["F_MESSAGE_TYPE"] = "消息動作";
$MESS["F_NAME_TEMPLATE"] = "名稱格式";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "個人資料頁";
$MESS["F_READ_TEMPLATE"] = "主題閱讀頁面";
$MESS["F_SET_NAVIGATION"] = "顯示麵包屑導航";
$MESS["F_SHOW_VOTE"] = "顯示民意測驗";
$MESS["F_URL_TEMPLATES"] = "URL處理";
$MESS["F_VOTE_CHANNEL_ID"] = "民意測驗小組";
$MESS["F_VOTE_GROUP_ID"] = "允許創建民意調查的用戶組";
$MESS["F_VOTE_SETTINGS"] = "民意調查設置";
