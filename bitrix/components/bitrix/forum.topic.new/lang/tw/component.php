<?php
$MESS["F_ERRRO_FILE_NOT_UPLOAD"] = "禁止文件上傳。";
$MESS["F_ERR_SESS_FINISH"] = "您的會議已經過期。請重複操作。";
$MESS["F_ETITLE"] = "修改消息";
$MESS["F_FID_IS_LOST"] = "沒有找到論壇。";
$MESS["F_MID_IS_LOST"] = "找不到帖子。";
$MESS["F_NO_EPERMS"] = "您沒有足夠的權限來修改此消息";
$MESS["F_NO_MODULE"] = "論壇模塊未安裝";
$MESS["F_NO_NPERMS"] = "您沒有足夠的權限來在此論壇中創建新主題";
$MESS["F_NTITLE"] = "新話題";
