<?php
$MESS["F_ANCHOR"] = "鏈接到這篇文章";
$MESS["F_ANCHOR_TITLE"] = "將鏈接URL複製到剪貼板";
$MESS["F_ATTACH_FILES"] = "附加的文件";
$MESS["F_AUTHOR_PROFILE"] = "用戶資料";
$MESS["F_DATE_REGISTER"] = "加入：";
$MESS["F_DELETE"] = "刪除";
$MESS["F_DELETE_CONFIRM"] = "該消息將被不可逆轉地刪除。繼續？";
$MESS["F_DELETE_MESSAGES"] = "刪除消息";
$MESS["F_DELETE_MESSAGES_CONFIRM"] = "這將不可逆轉地刪除消息。繼續？";
$MESS["F_EDIT"] = "調整";
$MESS["F_EDIT_HEAD"] = "編輯：";
$MESS["F_EMAIL_TITLE"] = "發送電子郵件";
$MESS["F_EMPTY_RESULT"] = "未找到。";
$MESS["F_MANAGE_MESSAGES"] = "管理消息";
$MESS["F_NUM_MESS"] = "文章：";
$MESS["F_POINTS"] = "點：";
$MESS["F_PRIVATE_MESSAGE"] = "訊息";
$MESS["F_PRIVATE_MESSAGE_TITLE"] = "發送私人消息";
$MESS["F_REAL_IP"] = "/ 真實的";
$MESS["F_SHOW"] = "展示";
$MESS["F_SHOW_MESSAGES"] = "顯示消息";
$MESS["F_TITLE"] = "未批准的消息";
$MESS["F_USER_ID"] = "id賓客";
$MESS["F_USER_ID_USER"] = "ID用戶";
$MESS["JS_NO_ACTION"] = "沒有選擇動作。請選擇一個動作。";
$MESS["JS_NO_MESSAGES"] = "沒有選擇消息。請選擇消息。";
