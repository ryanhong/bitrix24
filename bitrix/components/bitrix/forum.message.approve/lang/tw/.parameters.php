<?php
$MESS["F_DATE_FORMAT"] = "日期格式";
$MESS["F_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["F_DEFAULT_FID"] = "論壇ID";
$MESS["F_DEFAULT_TID"] = "主題ID";
$MESS["F_DISPLAY_PANEL"] = "顯示此組件的面板按鈕";
$MESS["F_IMAGE_SIZE"] = "附件的大小（PX）";
$MESS["F_INDEX_TEMPLATE"] = "論壇列表頁面";
$MESS["F_LIST_TEMPLATE"] = "主題列表頁面";
$MESS["F_MESSAGES_PER_PAGE"] = "每頁消息的數量";
$MESS["F_MESSAGE_APPR_TEMPLATE"] = "未批准的消息頁面";
$MESS["F_MESSAGE_SEND_TEMPLATE"] = "消息發布頁面";
$MESS["F_MESSAGE_TEMPLATE"] = "消息查看頁面（帶有消息ID）";
$MESS["F_PAGE_NAVIGATION_TEMPLATE"] = "Pager模板的名稱";
$MESS["F_PAGE_NAVIGATION_WINDOW"] = "頁面導航中的頁數";
$MESS["F_PM_EDIT_TEMPLATE"] = "私人消息頁面";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "個人資料頁";
$MESS["F_READ_TEMPLATE"] = "主題閱讀頁面";
$MESS["F_SET_NAVIGATION"] = "顯示麵包屑導航";
$MESS["F_TOPIC_NEW_TEMPLATE"] = "新主題創建頁面";
$MESS["F_URL_TEMPLATES"] = "URL處理";
