<?php
$MESS["CACHE_TIME_TIP"] = "緩存壽命（秒）";
$MESS["CACHE_TYPE_TIP"] = "緩存類型";
$MESS["DATE_FORMAT_TIP"] = "日期格式";
$MESS["DATE_TIME_FORMAT_TIP"] = "日期和時間格式";
$MESS["DISPLAY_PANEL_TIP"] = "控制面板顯示此組件的按鈕";
$MESS["FID_TIP"] = "論壇ID";
$MESS["IMAGE_SIZE_TIP"] = "附著的圖像尺寸（PX）";
$MESS["MESSAGES_PER_PAGE_TIP"] = "每頁帖子";
$MESS["PAGE_NAVIGATION_TEMPLATE_TIP"] = "麵包屑導航模板的名稱";
$MESS["PAGE_NAVIGATION_WINDOW_TIP"] = "麵包屑中的頁面";
$MESS["SET_NAVIGATION_TIP"] = "顯示導航控件";
$MESS["SET_TITLE_TIP"] = "設置頁面標題";
$MESS["TID_TIP"] = "主題ID";
$MESS["URL_TEMPLATES_INDEX_TIP"] = "論壇頁";
$MESS["URL_TEMPLATES_LIST_TIP"] = "主題頁";
$MESS["URL_TEMPLATES_MESSAGE_APPR_TIP"] = "隱藏消息頁面";
$MESS["URL_TEMPLATES_MESSAGE_SEND_TIP"] = "消息創建頁面";
$MESS["URL_TEMPLATES_MESSAGE_TIP"] = "消息查看頁面（帶有消息ID）";
$MESS["URL_TEMPLATES_PM_EDIT_TIP"] = "個人消息頁面";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = "用戶配置文件URL模板";
$MESS["URL_TEMPLATES_READ_TIP"] = "主題查看頁面";
$MESS["URL_TEMPLATES_TOPIC_NEW_TIP"] = "新主題創建頁面";
$MESS["WORD_LENGTH_TIP"] = "單詞長度";
