<?php
$MESS["BPWC_WNCTS_BTN_CANCEL"] = "取消";
$MESS["BPWC_WNCTS_BTN_SYNC"] = "開始導入";
$MESS["BPWC_WNCTS_TAB1"] = "進口";
$MESS["BPWC_WNCTS_TAB1T"] = "從Web Store導入數據";
$MESS["BPWC_WNCT_2EDIT"] = "編輯";
$MESS["BPWC_WNCT_2LIST"] = "網絡商店";
$MESS["BPWC_WNCT_ACTIVE"] = "積極的";
$MESS["BPWC_WNCT_DATA_SYNC_PERIOD"] = "進口期（分鐘; 0-禁用）";
$MESS["BPWC_WNCT_DATE_CREATE"] = "創建於";
$MESS["BPWC_WNCT_DATE_UPDATE"] = "最後修改";
$MESS["BPWC_WNCT_IMPORT_PERIOD"] = "上傳期（天）";
$MESS["BPWC_WNCT_IMPORT_PREFIX"] = "交易名稱前綴";
$MESS["BPWC_WNCT_IMPORT_PROBABILITY"] = "默認交易概率，％";
$MESS["BPWC_WNCT_IMPORT_PUBLIC"] = "默認情況下交易是公開的";
$MESS["BPWC_WNCT_IMPORT_RESPONSIBLE"] = "默認負責人";
$MESS["BPWC_WNCT_LABEL"] = "修改標記";
$MESS["BPWC_WNCT_LOGIN"] = "登入";
$MESS["BPWC_WNCT_NAME"] = "姓名";
$MESS["BPWC_WNCT_NOTE_HINT"] = "在所有狀態中編輯訂單以及導入和導出的權限，指定商店用戶的登錄和密碼。";
$MESS["BPWC_WNCT_PASSWORD"] = "密碼";
$MESS["BPWC_WNCT_SIZE"] = "一步一步的命令";
$MESS["BPWC_WNCT_STATUS"] = "最後的導入狀態";
$MESS["BPWC_WNCT_STATUS_CUR"] = "導入進度";
$MESS["BPWC_WNCT_SYNC_COMPANIES"] = "公司進口";
$MESS["BPWC_WNCT_SYNC_CONTACTS"] = "聯繫人進口";
$MESS["BPWC_WNCT_SYNC_DEALS"] = "交易進口";
$MESS["BPWC_WNCT_SYNC_ERROR_SYNC"] = "數據導入錯誤";
$MESS["BPWC_WNCT_SYNC_LOAD"] = "載入中";
$MESS["BPWC_WNCT_SYNC_STOPPING"] = "停止...";
$MESS["BPWC_WNCT_SYNC_STOP_LOAD"] = "停止";
$MESS["BPWC_WNCT_SYNC_SUCCESS_SYNC"] = "進口已成功完成。";
$MESS["BPWC_WNCT_SYNC_TERMINATE"] = "導入由用戶中斷。並非所有數據都可能被導入。";
$MESS["BPWC_WNCT_TAB2"] = "設定";
$MESS["BPWC_WNCT_TAB2T"] = "其他設置";
$MESS["BPWC_WNCT_URL"] = "地址";
$MESS["CRM_EXT_SALE_CLIST_RESULT"] = "導入結果：";
$MESS["CRM_EXT_SALE_CLIST_STEP"] = "完成的步驟＃步驟＃：Create＃createDeals＃Deals，＃createContacts＃聯繫人，＃CreateCompanies＃Companies;更新＃UpdatedDeals＃Deals，＃UpdatedContacts＃Contacts，＃UpdatedCompanies＃Companies。";
$MESS["CRM_EXT_SALE_CLIST_SUCCESS"] = "數據導入已成功完成。";
