<?php
$MESS["SONET_C9_NO_PERMS"] = "您無權執行此操作。";
$MESS["SONET_C9_TITLE"] = "刪除組";
$MESS["SONET_C9_TITLE_PROJECT"] = "刪除項目";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_USER_NO_GROUP"] = "找不到小組。";
$MESS["SONET_P_USER_NO_GROUP_PROJECT"] = "找不到項目。";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶。";
