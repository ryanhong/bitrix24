<?php
$MESS["SONET_C9_DO_CANCEL"] = "不要刪除組";
$MESS["SONET_C9_DO_DEL"] = "刪除組";
$MESS["SONET_C9_DO_DEL_PROJECT"] = "刪除項目";
$MESS["SONET_C9_SUBTITLE"] = "您確定要刪除這個組嗎？";
$MESS["SONET_C9_SUBTITLE_PROJECT"] = "您確定要刪除這個項目嗎？";
$MESS["SONET_C9_SUCCESS"] = "該小組已被刪除。";
$MESS["SONET_C9_SUCCESS_PROJECT"] = "項目已刪除。";
