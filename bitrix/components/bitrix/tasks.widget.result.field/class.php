<?php

/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage tasks
 * @copyright 2001-2021 Bitrix
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}
use Bitrix\Main\Engine\CurrentUser;

class TasksWidgetResultField extends \CBitrixComponent
{
	/**
	 * @param null $component
	 */
	public function __construct($component = null)
	{
		parent::__construct($component);
		$this->userId = CurrentUser::get()->getId();
	}

	/**
	 * @param array $params
	 * @return array
	 */
	public function onPrepareComponentParams($params)
	{
		if (
			!isset($params['HIDDEN'])
			|| $params['HIDDEN'] !== 'Y'
		)
		{
			$params['HIDDEN'] = 'N';
		}

		$params['userId'] = $this->userId;
		return $params;
	}

	/**
	 * @return mixed|void|null
	 */
	public function executeComponent()
	{
		try
		{
			$this->includeComponentTemplate();
		}
		catch (\Bitrix\Main\SystemException $exception)
		{

		}
	}
}