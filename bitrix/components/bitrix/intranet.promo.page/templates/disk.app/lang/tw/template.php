<?php
$MESS["INTRANET_DISK_PROMO_DESC"] = "今天，我們幾乎可以隨時隨地訪問互聯網。但是，我們可能會遇到在線文件存儲訪問不可用的情況：例如，在飛機上。這並不意味著您必須打斷工作！ Bitrix24.Drive Desktop應用程序將使您的所有文件與計算機上的本地副本同步。脫機編輯文件 - 所有更改將在可用的Internet連接後立即自動與云同步。";
$MESS["INTRANET_DISK_PROMO_DESC_SUB"] = "將應用程序安裝到您的計算機上，並隨時繼續工作！";
$MESS["INTRANET_DISK_PROMO_HEADER"] = "使用更多文件管理功能為BitRix24供電。";
$MESS["INTRANET_DISK_PROMO_STEP1_TITLE"] = "下載並安裝＃link_start＃bitrix24＃link_end＃應用程序";
$MESS["INTRANET_DISK_PROMO_STEP2_DESC"] = "運行該應用程序以無人看管的方式同步文件。您可以專門選擇將在本地或瀏覽器中可用的文件夾。";
$MESS["INTRANET_DISK_PROMO_STEP2_TITLE"] = "連接bitrix24.drive";
$MESS["INTRANET_DISK_PROMO_TITLE_MACOS"] = "Bitrix24 MacOS的桌面應用";
$MESS["INTRANET_DISK_PROMO_TITLE_WINDOWS"] = "Windows的BitRix24桌面應用";
