<?php
$MESS["CRM_COMPANY_VAR"] = "公司ID變量名稱";
$MESS["CRM_ELEMENT_ID"] = "公司ID";
$MESS["CRM_NAME_TEMPLATE"] = "名稱格式";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "公司編輯頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "導入頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "索引頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_LIST"] = "公司頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "公司查看頁面路徑模板";
