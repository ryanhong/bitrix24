<?php
$MESS["CRM_COMPANY_MERGE_HEADER_TEMPLATE"] = "在＃date_create＃上創建";
$MESS["CRM_COMPANY_MERGE_PAGE_TITLE"] = "合併公司";
$MESS["CRM_COMPANY_MERGE_RESULT_LEGEND"] = "從列表中選擇優先公司。它將用作公司資料的基礎。您可以從其他公司資料中添加更多數據。";
