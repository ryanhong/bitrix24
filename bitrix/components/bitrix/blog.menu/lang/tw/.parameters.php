<?php
$MESS["BH_BLOG_URL"] = "博客URL";
$MESS["BH_BLOG_VAR"] = "博客標識符變量";
$MESS["BH_PAGE_VAR"] = "頁面變量";
$MESS["BH_PATH_TO_BLOG"] = "博客主頁路徑的模板";
$MESS["BH_PATH_TO_BLOG_EDIT"] = "博客設置頁面路徑的模板";
$MESS["BH_PATH_TO_BLOG_INDEX"] = "主頁路徑的模板";
$MESS["BH_PATH_TO_CATEGORY_EDIT"] = "標籤自定義頁面路徑的模板";
$MESS["BH_PATH_TO_DRAFT"] = "草稿消息文件夾路徑的模板";
$MESS["BH_PATH_TO_GROUP_EDIT"] = "用戶組自定義頁面路徑的模板";
$MESS["BH_PATH_TO_POST_EDIT"] = "消息編輯頁面的模板";
$MESS["BH_PATH_TO_USER"] = "博客用戶頁面路徑的模板";
$MESS["BH_PATH_TO_USER_FRIENDS"] = "朋友頁面路徑的模板";
$MESS["BH_PATH_TO_USER_SETTINGS"] = "訪問權限自定義頁面路徑的模板";
$MESS["BH_POST_VAR"] = "博客消息標識符變量";
$MESS["BH_SET_NAV_CHAIN"] = "將項目添加到導航鏈中";
$MESS["BH_USER_VAR"] = "用戶ID變量";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
