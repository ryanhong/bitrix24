<?php
$MESS["BLOG_URL_TIP"] = "指定通過博客ID傳遞的代碼。";
$MESS["BLOG_VAR_TIP"] = "在此處指定通過博客ID傳遞的變量的名稱。";
$MESS["PAGE_VAR_TIP"] = "在此處指定博客頁面通過的變量的名稱。";
$MESS["PATH_TO_BLOG_EDIT_TIP"] = "博客設置頁面的路徑。示例：<Nobr> blog_b_edit.php？page = blog_edit＆blog =＃blog＃。</nobr>";
$MESS["PATH_TO_BLOG_INDEX_TIP"] = "指定博客索引頁面的路徑，例如<b> blog.php </b>。";
$MESS["PATH_TO_BLOG_TIP"] = "<i> <i> this Blog的主頁的路徑。示例：blog_blog.php？page = blog＆blog =＃blog＃。";
$MESS["PATH_TO_CATEGORY_EDIT_TIP"] = "博客標籤頁面的路徑。示例：<Nobr> blog_category.php？page = category_edit＆blog =＃blog＃。</nobr>";
$MESS["PATH_TO_DRAFT_TIP"] = "包含博客草稿消息的頁面的路徑。示例：<Nobr> blog_b_draft.php？page = draft＆blog =＃blog＃。";
$MESS["PATH_TO_GROUP_EDIT_TIP"] = "博客用戶組設置頁面的路徑。示例：<Nobr> blog_user_gr.php？page = group_edit＆blog =＃blog＃。</nobr>";
$MESS["PATH_TO_POST_EDIT_TIP"] = "博客文章編輯頁面的路徑。示例：<Nobr> blog_p_edit.php？page = post_edit＆blog =＃blog＃＆post_id =＃post_id＃。</nobr>";
$MESS["PATH_TO_USER_FRIENDS_TIP"] = "包含用戶朋友消息的頁面的路徑。示例：<Nobr> blog_friends.php？page = user_friend＆user_id =＃user_id＃。</nobr>";
$MESS["PATH_TO_USER_SETTINGS_TIP"] = "用戶朋友編輯頁面的路徑。示例：<Nobr> blog_user_set.php？page = user_settings＆blog =＃blog＃。</nobr>";
$MESS["PATH_TO_USER_TIP"] = "用戶配置文件頁面的路徑。示例：<Nobr> blog_user.php？page = user＆user_id =＃user_id＃。</nobr>";
$MESS["POST_VAR_TIP"] = "在此處指定博客文章ID的變量的名稱。";
$MESS["SET_NAV_CHAIN_TIP"] = "啟用此選項將此組件生成的頁面添加到導航鏈中。";
$MESS["USER_VAR_TIP"] = "在此處指定通過博客用戶ID傳遞的變量的名稱。";
