<?php
$MESS["SALE_CANCEL_ORDER1"] = "你確定要取消";
$MESS["SALE_CANCEL_ORDER2"] = "命令";
$MESS["SALE_CANCEL_ORDER3"] = "取消訂單後，您無法還原訂單。";
$MESS["SALE_CANCEL_ORDER4"] = "請告訴我們取消訂單的原因";
$MESS["SALE_CANCEL_ORDER_BTN"] = "取消訂單";
$MESS["SALE_RECORDS_LIST"] = "訂單清單";
