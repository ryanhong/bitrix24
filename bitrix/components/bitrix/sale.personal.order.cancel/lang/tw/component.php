<?php
$MESS["SALE_ACCESS_DENIED"] = "您需要授權取消訂單";
$MESS["SALE_MODULE_NOT_INSTALL"] = "E商店模塊未安裝";
$MESS["SPOC_CANCEL_ORDER"] = "已交付和/或付款的訂單無法取消。";
$MESS["SPOC_EMPTY_ORDER_ID"] = "訂單ID不能為空。";
$MESS["SPOC_NO_ORDER"] = "訂單號。 ＃ID＃找不到。";
$MESS["SPOC_ORDER_CANCELED"] = "訂單## account_number＃已取消";
$MESS["SPOC_TITLE"] = "取消訂單＃＃ ID＃";
