<?php
$MESS["ID_TIP"] = "評估訂單ID的PHP代碼。默認值是<b> = {\ $ id} </b>。";
$MESS["PATH_TO_DETAIL_TIP"] = "如果頁面在同一文件夾中，則指定訂單詳細信息頁面的路徑名或頁面文件名。可以使用<b>訂單詳細信息</b>組件創建此頁面。";
$MESS["PATH_TO_LIST_TIP"] = "如果頁面在同一文件夾中，則指定訂單頁面的路徑名或頁面文件名。可以使用<b> orders </b>組件創建此頁面。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將頁面標題設置為<b>取消訂單#id＃</b>";
