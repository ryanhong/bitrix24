<?php
$MESS["F_DELETE"] = "刪除";
$MESS["F_EDIT"] = "編輯";
$MESS["F_FOLDER"] = "文件夾";
$MESS["F_MESSAGES"] = "消息";
$MESS["F_REMOVE"] = "清除";
$MESS["JS_DEL_FOLDERS"] = "您確定要刪除文件夾嗎？";
$MESS["JS_DEL_MESSAGES"] = "您確定要刪除文件夾中的所有消息嗎？";
$MESS["JS_NO_DATA"] = "沒有選擇文件夾。請選擇一個文件夾。";
$MESS["PM_FOLDER_ID_1"] = "收件箱";
$MESS["PM_FOLDER_ID_2"] = "剩下的";
$MESS["PM_FOLDER_ID_3"] = "輸出箱";
$MESS["PM_FOLDER_ID_4"] = "回收";
$MESS["PM_FOLDER_TITLE"] = "文件夾名稱：";
$MESS["PM_HEAD_NEW_FOLDER"] = "新建文件夾";
$MESS["PM_HEAD_NEW_MESSAGE"] = "新消息";
$MESS["PM_PM"] = "私人信息";
$MESS["PM_POST_FULLY"] = "已用空間：";
