<?php
$MESS["PM_ACT_COPY"] = "複製";
$MESS["PM_ACT_DELETE"] = "刪除";
$MESS["PM_ACT_EDIT"] = "編輯";
$MESS["PM_ACT_MOVE"] = "移動";
$MESS["PM_ACT_REPLY"] = "回覆";
$MESS["PM_DATA"] = "數據";
$MESS["PM_FROM"] = "從";
$MESS["PM_IN"] = "到";
$MESS["PM_POST_FULLY"] = "已用空間：";
$MESS["PM_REQUEST_NOTIF"] = "發件人要求讀取確認。";
$MESS["PM_SEND_NOTIF"] = "發送確認";
$MESS["PM_TO"] = "到";
$MESS["P_NEXT"] = "下一個";
$MESS["P_PREV"] = "以前的";
