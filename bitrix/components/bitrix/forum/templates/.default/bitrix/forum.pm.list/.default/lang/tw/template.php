<?php
$MESS["JS_DEL_MESSAGE"] = "這將不可逆轉地刪除消息。繼續？";
$MESS["JS_NO_MESSAGES"] = "沒有選擇消息。請選擇消息。";
$MESS["PM_ACT_DELETE"] = "刪除";
$MESS["PM_ACT_IN"] = "到";
$MESS["PM_ACT_MOVE"] = "移動";
$MESS["PM_EMPTY_FOLDER"] = "沒有消息";
$MESS["PM_HEAD_AUTHOR"] = "作者";
$MESS["PM_HEAD_DATE"] = "日期";
$MESS["PM_HEAD_RECIPIENT"] = "到";
$MESS["PM_HEAD_SENDER"] = "從";
$MESS["PM_HEAD_SUBJ"] = "主題";
$MESS["PM_POST_FULLY"] = "已用空間：";
