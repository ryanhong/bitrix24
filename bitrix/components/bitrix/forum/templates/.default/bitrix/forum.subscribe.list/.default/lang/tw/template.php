<?php
$MESS["FSL_ALL_MESSAGES"] = "所有主題";
$MESS["FSL_FORUM_NAME"] = "論壇標題";
$MESS["FSL_HERE"] = "這裡";
$MESS["FSL_LAST_SENDED_MESSAGE"] = "最後發送消息";
$MESS["FSL_NEW_TOPICS"] = "新主題";
$MESS["FSL_NOT_SUBCRIBED"] = "您沒有訂閱任何論壇。使用論壇或主題中的適當鏈接（“訂閱”）訂閱。";
$MESS["FSL_SUBSCR_DATE"] = "訂閱日期";
$MESS["FSL_SUBSCR_MANAGE"] = "訂閱";
$MESS["FSL_TOPIC_NAME"] = "主題標題";
$MESS["F_DELETE_SUBSCRIBES"] = "刪除訂閱";
$MESS["JS_DEL_SUBSCRIBE"] = "您確定要刪除訂閱嗎？";
$MESS["JS_NO_SUBSCRIBE"] = "未選擇訂閱。";
