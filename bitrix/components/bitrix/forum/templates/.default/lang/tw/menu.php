<?php
$MESS["AUTH_LOGIN_BUTTON"] = "登入";
$MESS["AUTH_LOGOUT_BUTTON"] = "登出";
$MESS["F_MESSAGES"] = "私人信息";
$MESS["F_NEW_TOPIC"] = "新主題";
$MESS["F_NEW_TOPIC_TITLE"] = "到新主題列表";
$MESS["F_PROFILE"] = "輪廓";
$MESS["F_RULES"] = "規則";
$MESS["F_SEARCH"] = "搜尋";
$MESS["F_SUBSCRIBES"] = "訂閱";
$MESS["F_USERS"] = "用戶";
