<?php
$MESS["RAPC_ACCESS"] = "該網站請求以下權限：";
$MESS["RAPC_BTN_AGREE"] = "確認";
$MESS["RAPC_BTN_DISAGREE"] = "取消";
$MESS["RAPC_DESC"] = "確認您的請求將您的Bitrix24連接到<b> #site_title＃< /b>以統一CRM內的所有客戶端通信渠道。<br /> <br />只需單擊\ \“確認\”即可連接您的站點。您可以在網站上管理連接，或者只需在此處刪除自動化密碼：<a href= \"#ap_link# \">應用程序密碼</a>。";
