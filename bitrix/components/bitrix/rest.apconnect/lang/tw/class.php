<?php
$MESS["APC_ERROR_CLIENT_TYPE_NOT_ALLOWED"] = "客戶驗證錯誤：無效類型";
$MESS["APC_ERROR_REST_AVAILABLE"] = "錯誤：當前計劃中的集成軟件包不可用。";
$MESS["APC_ERROR_WRONG_CLIENT"] = "客戶端驗證錯誤：無效ID";
$MESS["APC_NOT_AUTHORIZED"] = "拒絕訪問。";
$MESS["APC_NO_CLIENT"] = "沒有可用的客戶數據";
$MESS["APC_PASSWORD_NOT_CREATED"] = "錯誤：無法創建應用程序密碼！";
$MESS["APC_PASSWORD_NOT_REGISTERD"] = "錯誤：無法註冊應用程序密碼。從註冊服務器收到的無效響應。";
$MESS["APC_REST_MODULE_NOT_INSTALLED"] = "未安裝\“ REST \”模塊。";
$MESS["APC_SOCIALSERVICES_MODULE_NOT_INSTALLED"] = "\“社交網絡\”模塊未安裝。";
$MESS["APC_TITLE"] = "連接您的網站";
$MESS["APC_TRANSPORT_INITIALIZE_FAILED"] = "無BITRIX24.NETWORK連接數據可用";
$MESS["APC_VERIFY_REQUEST_FAILED"] = "客戶端驗證錯誤：無法連接到Bitrix24.Network";
