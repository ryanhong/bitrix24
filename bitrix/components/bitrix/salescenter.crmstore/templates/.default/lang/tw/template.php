<?php
$MESS["SC_CRM_STORE_CONTAINER_CONTENT_2_MSGVER_1"] = "CRM現在是一個功能齊全的在線銷售平台，具有現代付款方式，收據和交付。一個簡單的付款過程確保客戶完成訂單。<br>啟用CRM付款並觀察轉換增長！";
$MESS["SC_CRM_STORE_CONTAINER_GO_TO_DEAL"] = "繼續交易開始出售";
$MESS["SC_CRM_STORE_CONTAINER_LINK"] = "細節";
$MESS["SC_CRM_STORE_CONTAINER_START_SELL"] = "開始銷售";
$MESS["SC_CRM_STORE_CONTAINER_SUB_TITLE_2_MSGVER_1"] = "觀看此視頻以了解<br> CRM付款將如何幫助您銷售更多";
$MESS["SC_CRM_STORE_CONTAINER_TITLE"] = "在線銷售的新時代";
$MESS["SC_CRM_STORE_TITLE_2_MSGVER_1"] = "CRM付款";
