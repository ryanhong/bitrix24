<?php
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "信息塊未安裝";
$MESS["PHOTO_ELEMENT_NOT_FOUND"] = "沒有找到照片。";
$MESS["P_BLOG_COMMENTS_CNT"] = "評論數";
$MESS["P_BLOG_POST_ID"] = "博客的ID供評論";
$MESS["P_EMPTY_BLOG_URL"] = "沒有指定評論的博客。";
$MESS["P_MODULE_IS_NOT_INSTALLED"] = "未安裝模塊照片庫2.0。";
$MESS["P_ORIGINAL"] = "原來的";
