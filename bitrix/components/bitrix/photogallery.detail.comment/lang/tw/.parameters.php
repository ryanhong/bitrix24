<?php
$MESS["F_BLOG_URL"] = "博客以評論";
$MESS["F_COMMENTS_COUNT"] = "評論每頁";
$MESS["F_FORUM_ID"] = "論壇ID";
$MESS["F_PATH_TO_SMILE"] = "微笑文件夾的路徑（根相對）";
$MESS["F_PREORDER"] = "以前順序顯示消息";
$MESS["F_READ_TEMPLATE"] = "主題閱讀頁面";
$MESS["F_USE_CAPTCHA"] = "使用驗證碼";
$MESS["IBLOCK_DETAIL_URL"] = "詳細的查看頁面";
$MESS["IBLOCK_ELEMENT_ID"] = "元素ID";
$MESS["IBLOCK_IBLOCK"] = "信息塊";
$MESS["IBLOCK_TYPE"] = "信息塊類型";
$MESS["P_COMMENTS_TYPE"] = "註釋組件";
$MESS["P_COMMENTS_TYPE_BLOG"] = "部落格";
$MESS["P_COMMENTS_TYPE_FORUM"] = "論壇";
$MESS["P_PATH_TO_BLOG"] = "博客路徑";
$MESS["P_PATH_TO_USER"] = "通往用戶個人資料的路徑";
