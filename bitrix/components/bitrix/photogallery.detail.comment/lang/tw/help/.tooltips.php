<?php
$MESS["BLOG_URL_TIP"] = "在此處選擇將存儲評論的博客。";
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>否CAHCE </i>：沒有執行緩存。";
$MESS["COMMENTS_COUNT_TIP"] = "定義每個頁面評論的數量。其他評論將通過BreadCrumb Navigation獲得。";
$MESS["COMMENTS_TYPE_TIP"] = "在此處選擇其功能將用於評論的模塊。";
$MESS["DETAIL_URL_TIP"] = "指定完整照片查看頁面的地址。";
$MESS["ELEMENT_ID_TIP"] = "該字段包含一個評估元素（照片）ID的表達式。";
$MESS["FORUM_ID_TIP"] = "在此處選擇將存儲評論的論壇。";
$MESS["IBLOCK_ID_TIP"] = "在此處指定將存儲照片的信息塊。另外，您可以選擇<b>（其他） - > </b>並指定旁邊字段中的信息塊ID。";
$MESS["IBLOCK_TYPE_TIP"] = "在此處選擇現有的信息塊類型之一。單擊<b> <i> ok </i> </b>加載所選類型的信息塊。";
$MESS["PATH_TO_BLOG_TIP"] = "指定博客索引頁面的路徑。";
$MESS["PATH_TO_SMILE_TIP"] = "包含笑臉的文件夾的路徑。";
$MESS["PATH_TO_USER_TIP"] = "指定用戶配置文件的地址。";
$MESS["PREORDER_TIP"] = "如果已檢查，則消息將按創建日期按上升順序進行排序。";
$MESS["SECTION_ID_TIP"] = "該字段包含一個評估（專輯）ID節的表達式。";
$MESS["URL_TEMPLATES_READ_TIP"] = "論壇主題查看頁面的地址。";
$MESS["USE_CAPTCHA_TIP"] = "添加新評論時指定顯示和使用驗證碼。";
