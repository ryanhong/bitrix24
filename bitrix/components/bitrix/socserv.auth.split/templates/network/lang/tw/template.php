<?php
$MESS["SAL_N_LOGOUT"] = "在所有設備上註銷";
$MESS["SAL_N_NOTE"] = "要更改用於登錄Bitrix24的電子郵件和密碼或將社交網絡帳戶連接到您的個人資料，請打開Bitrix24.Passport並進行適當的更改。 <br> <br> bitrix24.network配置文件是一個個人頁面，僅可訪問您。通過更改此頁面上的授權數據，您可以為註冊的所有Bitrix24 Intranet以及您的BitRix24.Network帳戶更改它們。";
$MESS["SAL_N_NOTE1"] = "您的bitrix24.network帳戶：<a href= \“#personal_www# \”target= \"_blank \">＃＃名稱＃＃＃＃＃last_name＃（＃login＃）</a>";
$MESS["SAL_N_NOTE_OTHER"] = "在用戶的BitRix24.NETWork配置文件中編輯用戶身份驗證數據。該員工使用此配置文件：<a href= \"#personal_www# \“target= \"_blank \">＃＃名稱＃＃＃＃＃＃last_name＃（＃login＃）</a>";
$MESS["SAL_N_NOTE_OTHER_NOT_ACCEPTED"] = "您可以在BitRix24.NETWork用戶配置文件中管理用戶登錄憑據。員工需要接受邀請將其帳戶綁定到Bitrix24.network。";
$MESS["SAL_N_PASSPORT"] = "編輯我的bitrix24.network配置文件";
$MESS["SAL_N_PASSPORT_OTHER"] = "查看bitrix24.network配置文件";
$MESS["SOCSERV_BUTTON_CANCEL"] = "取消";
$MESS["SOCSERV_BUTTON_CONTINUE"] = "繼續";
$MESS["SOCSERV_LOGOUT_SUCCESS"] = "您已經在所有設備上成功登錄。";
$MESS["SOCSERV_LOGOUT_TEXT"] = "這將在所有瀏覽器和應用程序中都從所有Bitrix24中登錄您，除了您看到此消息的內容。";
$MESS["SOCSERV_LOGOUT_TITLE"] = "登出";
