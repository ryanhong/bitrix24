<?php
$MESS["SS_DELETE"] = "刪除";
$MESS["SS_GET_COMPONENT_INFO"] = "您可以將您的帳戶鏈接到：";
$MESS["SS_NAME"] = "姓名";
$MESS["SS_PROFILE_DELETE_CONFIRM"] = "您確定要斷開縮放配置文件嗎？";
$MESS["SS_PROFILE_DELETE_CONFIRM_OTHER"] = "您確定要刪除帳戶嗎？";
$MESS["SS_PROFILE_ZOOM_CONFERENCE_TITLE"] = "縮放視頻會議已正確配置並可用";
$MESS["SS_PROFILE_ZOOM_CONNECT"] = "連接";
$MESS["SS_PROFILE_ZOOM_CONNECTED"] = "連接的";
$MESS["SS_PROFILE_ZOOM_CONNECT_TITLE"] = "連接Zoom以安排並在您的Bitrix24中創建在線會議";
$MESS["SS_PROFILE_ZOOM_DISCONNECT"] = "斷開";
$MESS["SS_SOCNET"] = "社交網絡";
$MESS["SS_YOUR_ACCOUNTS"] = "您的鏈接帳戶：";
