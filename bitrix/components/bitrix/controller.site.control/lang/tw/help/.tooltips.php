<?php
$MESS["ACCESS_RESTRICTION_TIP"] = "限制訪問";
$MESS["ACTION_TIP"] = "行動";
$MESS["COMMAND_TIP"] = "命令";
$MESS["GROUP_PERMISSIONS_TIP"] = "指定具有管理權限的用戶組";
$MESS["SEPARATOR_TIP"] = "場分離器";
$MESS["SITE_URL_TIP"] = "網站URL";
