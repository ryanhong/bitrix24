<?php
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_OLX_LIST_ITEM_1"] = "處理直接在Bitrix24中從OLX收到的查詢";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_OLX_LIST_ITEM_2"] = "將聯繫人和通信歷史保存到CRM";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_OLX_LIST_ITEM_3"] = "指導客戶通過CRM中的銷售渠道";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_OLX_LIST_ITEM_4"] = "回复您的客戶何時何地";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_OLX_LIST_ITEM_5"] = "根據隊列規則，客戶查詢分佈在銷售代理之間";
$MESS["IMCONNECTOR_COMPONENT_OLX_AUTHORIZATION"] = "驗證";
$MESS["IMCONNECTOR_COMPONENT_OLX_AUTHORIZE"] = "登入";
$MESS["IMCONNECTOR_COMPONENT_OLX_CHANGE_ANY_TIME"] = "您可以隨時編輯或斷開連接";
$MESS["IMCONNECTOR_COMPONENT_OLX_CONNECTED"] = "OLX連接";
$MESS["IMCONNECTOR_COMPONENT_OLX_CONNECT_HELP"] = "<div class = \“ imconnector-field-button-subtitle \”>我想</div>
<div class = \“ imconnector-field-button-name \”> <span class = \“ imconnector-field-box-text-bold \”> connect </span> olx帳戶</div>";
$MESS["IMCONNECTOR_COMPONENT_OLX_CONNECT_STEP_NEW"] = "您必須＃link_start＃創建一個帳戶＃link_end＃或使用已經擁有的帳戶。如果您沒有帳戶，我們將幫助您創建並將其連接到您的Bitrix24。";
$MESS["IMCONNECTOR_COMPONENT_OLX_CONNECT_TITLE"] = "將OLX連接到開放頻道";
$MESS["IMCONNECTOR_COMPONENT_OLX_FINAL_FORM_DESCRIPTION"] = "OLX已成功連接到您的開放頻道。從現在開始，發送到您的OLX帳戶的所有消息都將轉發到您的Bitrix24。";
$MESS["IMCONNECTOR_COMPONENT_OLX_INDEX_ADDITIONAL_DESCRIPTION_NEW"] = "您必須＃link_start＃創建一個OLX帳戶＃link_end＃或使用您已經擁有的一個。如果您沒有帳戶，我們將幫助您通過幾個步驟創建它，並將其連接到BitRix24。";
$MESS["IMCONNECTOR_COMPONENT_OLX_INDEX_SUBTITLE"] = "將客戶溝通渠道合併到BITRIX24中，以更快地響應並改善轉化率。";
$MESS["IMCONNECTOR_COMPONENT_OLX_INDEX_TITLE"] = "在OLX上銷售？直接從客戶那裡收到Bitrix24的問題。";
$MESS["IMCONNECTOR_COMPONENT_OLX_INFO"] = "資訊";
$MESS["IMCONNECTOR_COMPONENT_OLX_INSTRUCTION_TITLE"] = "<span class = \“ imconnector-field-box-text-bold \”>連接指令：</span>";
$MESS["IMCONNECTOR_COMPONENT_OLX_LOG_IN_OAUTH"] = "登錄您的OLX帳戶。";
$MESS["IMCONNECTOR_COMPONENT_OLX_USER_DOMAIN"] = "領域：";
$MESS["IMCONNECTOR_COMPONENT_OLX_USER_EMAIL"] = "用戶電子郵件：";
$MESS["IMCONNECTOR_COMPONENT_OLX_USER_ID"] = "用戶身份：";
