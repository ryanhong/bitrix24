<?php
$MESS["BH_PAGE_VAR"] = "頁面變量名稱";
$MESS["BH_PATH_TO_BLOG"] = "對話頁面路徑模板";
$MESS["BH_PATH_TO_DRAFT"] = "對話草稿頁面路徑模板";
$MESS["BH_PATH_TO_POST_EDIT"] = "對話編輯頁面路徑模板";
$MESS["BH_PATH_TO_USER"] = "用戶配置文件路徑模板";
$MESS["BH_POST_VAR"] = "對話ID變量名稱";
$MESS["BH_SET_NAV_CHAIN"] = "將項目添加到麵包屑中";
$MESS["BH_USER_VAR"] = "用戶ID變量名稱";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
