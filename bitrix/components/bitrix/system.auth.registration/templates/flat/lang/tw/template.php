<?php
$MESS["AUTH_AUTH"] = "授權";
$MESS["AUTH_CONFIRM"] = "確認密碼";
$MESS["AUTH_EMAIL"] = "電子郵件";
$MESS["AUTH_EMAIL_SENT"] = "註冊確認請求已發送到指定的電子郵件地址。";
$MESS["AUTH_EMAIL_WILL_BE_SENT"] = "註冊確認請求將發送到指定的電子郵件地址。";
$MESS["AUTH_LAST_NAME"] = "姓";
$MESS["AUTH_LOGIN_MIN"] = "登錄（Min。3char。）";
$MESS["AUTH_NAME"] = "姓名";
$MESS["AUTH_PASSWORD_REQ"] = "密碼";
$MESS["AUTH_REGISTER"] = "登記";
$MESS["AUTH_REQ"] = "必需的字段。";
$MESS["AUTH_SECURE_NOTE"] = "密碼將在發送之前進行加密。這將防止密碼以數據傳輸通道以開放形式出現。";
$MESS["CAPTCHA_REGF_PROMT"] = "從圖像輸入文字";
$MESS["main_register_phone_number"] = "電話號碼";
$MESS["main_register_sms_code"] = "SMS確認代碼";
$MESS["main_register_sms_send"] = "發送";
