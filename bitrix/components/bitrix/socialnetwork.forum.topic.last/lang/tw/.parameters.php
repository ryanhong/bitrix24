<?php
$MESS["F_DEFAULT_FID"] = "論壇ID";
$MESS["F_DESC_ASC"] = "上升";
$MESS["F_DESC_DESC"] = "下降";
$MESS["F_MESSAGE_TEMPLATE"] = "消息查看頁面";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "個人資料頁";
$MESS["F_READ_TEMPLATE"] = "主題閱讀頁面";
$MESS["F_SHOW_LAST_POST_DATE"] = "最後一條消息";
$MESS["F_SHOW_POSTS"] = "答复";
$MESS["F_SHOW_TITLE"] = "標題";
$MESS["F_SHOW_USER_START_NAME"] = "作者";
$MESS["F_SHOW_VIEWS"] = "讀";
$MESS["F_SOCNET_GROUP_ID"] = "組ID";
$MESS["F_SORTING_BY"] = "排序順序";
$MESS["F_SORTING_ORD"] = "按字段排序";
$MESS["F_TOPICS_PER_PAGE"] = "每頁主題數";
$MESS["F_URL_TEMPLATES"] = "URL處理";
$MESS["F_USER_ID"] = "用戶身份";
