<?php
$MESS["CALENDAR_MODULE_IS_NOT_INSTALLED"] = "未安裝\“事件日曆\”模塊。";
$MESS["EVENTS_GROUP_LATE"] = "之後";
$MESS["EVENTS_GROUP_TODAY"] = "今天";
$MESS["EVENTS_GROUP_TOMORROW"] = "明天";
$MESS["MB_CAL_EVENTS_COUNT"] = "總事件：＃計數＃";
$MESS["MB_CAL_EVENT_ALL_DAY"] = "一整天";
$MESS["MB_CAL_EVENT_DATE_FORMAT"] = "D.，M。D，Y";
$MESS["MB_CAL_EVENT_DATE_FROM_TO"] = "從＃date_from＃到＃date_to＃";
$MESS["MB_CAL_EVENT_TIME_FORMAT"] = "G：我";
$MESS["MB_CAL_EVENT_TIME_FORMAT_AMPM"] = "H：我一個";
$MESS["MB_CAL_EVENT_TIME_FROM_TO_TIME"] = "從＃time_from＃到＃time_to＃";
$MESS["MB_CAL_NO_EVENTS"] = "沒有即將發生的事件";
