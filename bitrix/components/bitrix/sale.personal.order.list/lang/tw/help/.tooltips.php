<?php
$MESS["HISTORIC_STATUSES_TIP"] = "可用於過濾歷史記錄的狀態。如果\“ filter_history \”參數不是y，而是在這些狀態之一中的訂單，則將被拒絕並且未在搜索結果中顯示。";
$MESS["ID_TIP"] = "將用於構建訂單查看頁面等鏈接的訂單ID。";
$MESS["ORDERS_PER_PAGE_TIP"] = "指定每個頁面的訂單計數。其他訂單將通過麵包屑導航鏈接提供。";
$MESS["PATH_TO_BASKET_TIP"] = "客戶購物車頁面的路徑。";
$MESS["PATH_TO_CANCEL_TIP"] = "訂單取消頁面的路徑。";
$MESS["PATH_TO_COPY_TIP"] = "訂單重複頁面的路徑。您可以在此處指定購物車的路徑，該路徑將復制訂單並準備好結帳。";
$MESS["PATH_TO_DETAIL_TIP"] = "訂單詳細信息頁面的路徑。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將頁面標題設置為\“我的訂單\”。";
