<?php
$MESS["CACHE_FILTER_TIP"] = "激活此選項將導致所有過濾器結果被緩存。";
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>不緩存</i>：沒有執行緩存。";
$MESS["DETAIL_URL_TIP"] = "包含信息塊元素內容的頁面的URL。";
$MESS["DISPLAY_PANEL_TIP"] = "如果已檢查，則在“站點編輯”模式下的管理工具欄和組件編輯區域工具欄上顯示該工具按鈕。";
$MESS["ELEMENT_COUNT_TIP"] = "每個部分中要顯示的最大照片數量。";
$MESS["ELEMENT_SORT_FIELD_TIP"] = "要在部分中對照片進行排序的字段。";
$MESS["ELEMENT_SORT_ORDER_TIP"] = "要分類照片的字段。";
$MESS["FIELD_CODE_TIP"] = "在此處選擇要在頂部表中顯示的其他字段。如果選擇<i> <b>無</b> </i>，並且不定義下面的字段表達式，則將顯示默認字段。";
$MESS["FILTER_NAME_TIP"] = "將傳遞過濾器設置的變量的名稱。您可以將字段留為空，以使用默認名稱。";
$MESS["IBLOCK_ID_TIP"] = "選擇所選類型的信息塊。如果您選擇了其他</i>，請在下一個字段中指定信息塊ID。";
$MESS["IBLOCK_TYPE_TIP"] = "選擇列表中的現有信息塊類型之一，然後單擊<b> ok </b>。這將加載所選類型的信息塊。";
$MESS["LINE_ELEMENT_COUNT_TIP"] = "每個表行的元素數量。";
$MESS["PROPERTY_CODE_TIP"] = "在此處選擇要在頂表中顯示的屬性。如果選擇<i> <b>無</b> </i>，並且不定義下面的字段表達式，則不會顯示屬性。";
$MESS["SECTION_COUNT_TIP"] = "最大要顯示的部分。";
$MESS["SECTION_SORT_FIELD_TIP"] = "要在信息塊中對部分進行排序的字段。";
$MESS["SECTION_SORT_ORDER_TIP"] = "定義截面排序順序。";
$MESS["SECTION_URL_TIP"] = "在此處指定顯示部分內容的頁面的URL。";
