<?php
$MESS["APP_DELETE"] = "刪除";
$MESS["APP_EDIT"] = "編輯";
$MESS["APP_HEADER_CLIENT_ID"] = "應用ID（client_id）";
$MESS["APP_HEADER_NAME"] = "姓名";
$MESS["APP_HEADER_ONLY_API"] = "僅腳本";
$MESS["APP_HEADER_SECRET_ID"] = "應用程序密鑰（client_secret）";
$MESS["APP_LIST_TITLE"] = "我的申請";
$MESS["APP_NO"] = "不";
$MESS["APP_OPEN"] = "打開申請";
$MESS["APP_REINSTALL"] = "重新安裝";
$MESS["APP_RIGHTS"] = "員工訪問權限";
$MESS["APP_YES"] = "是的";
