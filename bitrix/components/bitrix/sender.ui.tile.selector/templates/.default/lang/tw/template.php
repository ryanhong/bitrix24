<?php
$MESS["SENDER_UI_STATIC_SELECTOR_SELECTED"] = "重複線索和交易生成器只能與聯繫人和公司一起使用。您選擇的細分市場無法處理。請導入您的地址列表進行。 <a href='% instruction%'>如何導入地址列表</a>";
$MESS["SENDER_UI_TILE_SELECTOR_ADD"] = "創造";
$MESS["SENDER_UI_TILE_SELECTOR_SELECT"] = "選擇";
