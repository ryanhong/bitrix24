<?php
$MESS["CRM_PERMS_BUTTONS_CANCEL"] = "取消";
$MESS["CRM_PERMS_BUTTONS_SAVE"] = "節省";
$MESS["CRM_PERMS_DLG_BTN"] = "刪除";
$MESS["CRM_PERMS_DLG_MESSAGE"] = "你確定你要刪除嗎？";
$MESS["CRM_PERMS_DLG_TITLE"] = "刪除角色";
$MESS["CRM_PERMS_PERM_ADD"] = "添加訪問權限";
$MESS["CRM_PERMS_PERM_DELETE"] = "刪除";
$MESS["CRM_PERMS_PERM_ENTITY"] = "用戶";
$MESS["CRM_PERMS_PERM_ROLE"] = "CRM角色";
$MESS["CRM_PERMS_RESTRICTION"] = "要分配您的員工不同的CRM訪問權限，請升級到<a target = \"_blank \" href= \之https://www.bitrix24.com/prices/prices/vrices/xprices/xprices/vrice/ \ \">商業計劃之一</a>。";
$MESS["CRM_PERMS_ROLE_ADD"] = "添加";
$MESS["CRM_PERMS_ROLE_DELETE"] = "刪除";
$MESS["CRM_PERMS_ROLE_EDIT"] = "編輯";
$MESS["CRM_PERMS_ROLE_LIST"] = "角色";
$MESS["CRM_PERMS_TYPE_NONE"] = "拒絕訪問。";
