<?php
$MESS["CVP_ADD_TO_BASKET_OK"] = "添加到您的購物車";
$MESS["CVP_CATALOG_BASKET_UNKNOWN_ERROR"] = "未知錯誤將商品添加到購物車";
$MESS["CVP_CATALOG_BTN_MESSAGE_BASKET_REDIRECT"] = "查看購物車";
$MESS["CVP_CATALOG_BTN_MESSAGE_CLOSE"] = "關閉";
$MESS["CVP_CATALOG_BTN_MESSAGE_SEND_PROPS"] = "選擇";
$MESS["CVP_CATALOG_SET_BUTTON_BUY"] = "查看購物車";
$MESS["CVP_CATALOG_TITLE_BASKET_PROPS"] = "商品屬性將傳遞給購物車";
$MESS["CVP_CATALOG_TITLE_ERROR"] = "錯誤";
$MESS["CVP_MSG_YOU_HAVE_NOT_YET"] = "您尚未查看任何產品。";
$MESS["CVP_TPL_ELEMENT_DELETE_CONFIRM"] = "這將刪除與此記錄相關的所有信息！繼續？";
$MESS["CVP_TPL_MESS_BTN_ADD_TO_BASKET"] = "添加到購物車";
$MESS["CVP_TPL_MESS_BTN_BUY"] = "買";
$MESS["CVP_TPL_MESS_BTN_COMPARE"] = "比較";
$MESS["CVP_TPL_MESS_BTN_DETAIL"] = "細節";
$MESS["CVP_TPL_MESS_BTN_SUBSCRIBE"] = "訂閱";
$MESS["CVP_TPL_MESS_MEASURE_SIMPLE_MODE"] = "＃值＃＃單位＃";
$MESS["CVP_TPL_MESS_PRICE_SIMPLE_MODE"] = "從＃價格＃進行＃測量＃";
$MESS["CVP_TPL_MESS_PRODUCT_NOT_AVAILABLE"] = "缺貨";
$MESS["SLB_TPL_TITLE_GIFT"] = "選擇產品並收到禮物";
