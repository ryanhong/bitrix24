<?php
$MESS["BLOG_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_PU_NO_RIGHTS"] = "未足夠編輯自己的個人資料的許可。";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶。";
$MESS["SONET_P_USER_SEX_F"] = "女性";
$MESS["SONET_P_USER_SEX_M"] = "男性";
$MESS["SONET_P_USER_TITLE"] = "編輯用戶資料";
$MESS["SONET_P_USER_TITLE_VIEW"] = "編輯個人資料";
