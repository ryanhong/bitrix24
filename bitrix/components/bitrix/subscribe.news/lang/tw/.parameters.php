<?php
$MESS["CP_BSN_ACTIVE_FROM"] = "激活日期";
$MESS["CP_BSN_IBLOCK_TYPE"] = "信息塊類型";
$MESS["CP_BSN_ID"] = "信息塊代碼";
$MESS["CP_BSN_ORDER_ASC"] = "上升";
$MESS["CP_BSN_ORDER_DESC"] = "下降";
$MESS["CP_BSN_SITE_ID"] = "地點";
$MESS["CP_BSN_SORT"] = "排序";
$MESS["CP_BSN_SORT_BY"] = "截面排序的字段";
$MESS["CP_BSN_SORT_ORDER"] = "分類的方向";
