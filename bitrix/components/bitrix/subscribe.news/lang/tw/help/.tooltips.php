<?php
$MESS["IBLOCK_TYPE_TIP"] = "在此處選擇所需的信息塊類型。";
$MESS["ID_TIP"] = "選擇新聞將在新聞通訊中使用的信息塊。如果需要所有信息塊的所有新聞，請選擇<b> all </b>。";
$MESS["SITE_ID_TIP"] = "在此處選擇其信息塊將用於創建新聞通訊的網站。";
$MESS["SORT_BY_TIP"] = "在此處選擇要分類新聞項目的字段。";
$MESS["SORT_ORDER_TIP"] = "上升或下降。";
