<?php
$MESS["INTR_GROUP_NAME"] = "Intranet門戶";
$MESS["TM_GROUP_NAME"] = "工作時間管理";
$MESS["TM_TMR_COMPONENT_DESCRIPTION"] = "向公司員工和經理展示工作時間報告。";
$MESS["TM_TMR_COMPONENT_NAME"] = "工作時間摘要";
