<?php
$MESS["CRM_ACTIVITY_COLUMN_CLIENT"] = "客戶";
$MESS["CRM_ACTIVITY_COLUMN_COMPLETED"] = "地位";
$MESS["CRM_ACTIVITY_COLUMN_CREATED"] = "創建於";
$MESS["CRM_ACTIVITY_COLUMN_DEADLINE"] = "最後期限";
$MESS["CRM_ACTIVITY_COLUMN_DESCRIPTION"] = "描述";
$MESS["CRM_ACTIVITY_COLUMN_END"] = "最後期限";
$MESS["CRM_ACTIVITY_COLUMN_END_2"] = "結尾";
$MESS["CRM_ACTIVITY_COLUMN_PRIORITY"] = "優先事項";
$MESS["CRM_ACTIVITY_COLUMN_REFERENCE"] = "交易/鉛";
$MESS["CRM_ACTIVITY_COLUMN_RESPONSIBLE"] = "負責人";
$MESS["CRM_ACTIVITY_COLUMN_START"] = "開始";
$MESS["CRM_ACTIVITY_COLUMN_SUBJECT"] = "姓名";
$MESS["CRM_ACTIVITY_COLUMN_TYPE_NAME"] = "類型";
$MESS["CRM_ACTIVITY_FILTER_COMPLETED"] = "地位";
$MESS["CRM_ACTIVITY_FILTER_CREATED"] = "創建於";
$MESS["CRM_ACTIVITY_FILTER_DEADLINE"] = "最後期限";
$MESS["CRM_ACTIVITY_FILTER_END"] = "最後期限";
$MESS["CRM_ACTIVITY_FILTER_END_2"] = "結尾";
$MESS["CRM_ACTIVITY_FILTER_ITEM_COMPLETED"] = "完全的";
$MESS["CRM_ACTIVITY_FILTER_ITEM_NOT_COMPLETED"] = "沒完成";
$MESS["CRM_ACTIVITY_FILTER_PRIORITY"] = "優先事項";
$MESS["CRM_ACTIVITY_FILTER_RESPONSIBLE"] = "負責人";
$MESS["CRM_ACTIVITY_FILTER_START"] = "開始";
$MESS["CRM_ACTIVITY_FILTER_TYPE_ID"] = "類型";
$MESS["CRM_ACTIVITY_INCOMING_CALL"] = "電話（即將到來）";
$MESS["CRM_ACTIVITY_INCOMING_EMAIL"] = "電子郵件（傳入）";
$MESS["CRM_ACTIVITY_OUTGOING_CALL"] = "電話（外向）";
$MESS["CRM_ACTIVITY_OUTGOING_EMAIL"] = "電子郵件（傳出）";
$MESS["CRM_ACTIVITY_STATUS_COMPLETED"] = "完全的";
$MESS["CRM_ACTIVITY_STATUS_NOT_COMPLETED"] = "沒完成";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_OWNER_ID_NOT_DEFINED"] = "未指定所有者ID（\“ lander_id \”）。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問。";
$MESS["CRM_PRESET_COMPLETED"] = "我完成的活動";
$MESS["CRM_PRESET_COMPLETED_ALL"] = "所有完成的活動";
$MESS["CRM_PRESET_NOT_COMPLETED"] = "我目前的活動";
$MESS["CRM_PRESET_NOT_COMPLETED_ALL"] = "所有當前活動";
$MESS["CRM_UNSUPPORTED_OWNER_TYPE"] = "不支持所有者類型：\“＃所有者_type＃\”。";
