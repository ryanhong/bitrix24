<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>不緩存</i>：沒有執行緩存。";
$MESS["DETAIL_URL_TIP"] = "在此處指定信息塊元素詳細信息頁面的路徑。";
$MESS["IBLOCKS_TIP"] = "在此處選擇現有信息塊之一。您可以通過將CTRL固定來選擇多個項目。";
$MESS["IBLOCK_TYPE_TIP"] = "在此處選擇現有的信息塊類型之一。單擊<b> <i> ok </i> </b>加載所選類型的信息塊。";
$MESS["PARENT_SECTION_TIP"] = "指定了ID OS A部分，其元素（照片）將顯示。將此字段空白以從指定信息塊的任何部分中選擇隨機照片。";
