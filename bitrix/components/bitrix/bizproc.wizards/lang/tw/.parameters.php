<?php
$MESS["BPWC_WP_ADMIN_ACCESS"] = "管理訪問";
$MESS["BPWC_WP_BP"] = "業務流程";
$MESS["BPWC_WP_IBLOCK_TYPE"] = "信息塊類型";
$MESS["BPWC_WP_LOG"] = "紀錄";
$MESS["BPWC_WP_PERMS"] = "使用權";
$MESS["BPWC_WP_SEF_EDIT"] = "編輯塊";
$MESS["BPWC_WP_SEF_INDEX"] = "塊";
$MESS["BPWC_WP_SEF_LIST"] = "運行過程";
$MESS["BPWC_WP_SEF_NEW"] = "新塊";
$MESS["BPWC_WP_SEF_START"] = "開始過程";
$MESS["BPWC_WP_SEF_VIEW"] = "查看運行過程";
$MESS["BPWC_WP_SETVAR"] = "業務流程變量";
$MESS["BPWC_WP_SET_NAV_CHAIN"] = "設置麵包屑";
$MESS["BPWC_WP_SKIP_BLOCK"] = "只有一個過程";
$MESS["BPWC_WP_TASK"] = "任務";
