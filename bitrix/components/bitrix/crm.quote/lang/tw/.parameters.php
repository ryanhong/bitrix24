<?php
$MESS["CRM_ELEMENT_ID"] = "報價ID";
$MESS["CRM_NAME_TEMPLATE"] = "名稱格式";
$MESS["CRM_QUOTE_VAR"] = "報價ID變量名稱";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "報價編輯頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "導入頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "索引頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_LIST"] = "引號頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "報價查看頁面路徑模板";
