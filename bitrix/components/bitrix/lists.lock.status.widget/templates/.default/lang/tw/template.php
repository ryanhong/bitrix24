<?php
$MESS["LISTS_LOCK_STATUS_GREEN"] = "  - 可用於編輯";
$MESS["LISTS_LOCK_STATUS_HINT_RED"] = "＃元素＃\“現在由＃User_name＃\”編輯";
$MESS["LISTS_LOCK_STATUS_HINT_YELLOW"] = "Item \“＃element＃\”被鎖定，因為您開始編輯它。
保存更改後，它將解鎖，或者您可以使用操作菜單手動解鎖它";
$MESS["LISTS_LOCK_STATUS_RED"] = "  - 鎖定者：";
$MESS["LISTS_LOCK_STATUS_YELLOW"] = "  - 被你鎖定";
