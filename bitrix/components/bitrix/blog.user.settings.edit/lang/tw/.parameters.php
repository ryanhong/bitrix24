<?php
$MESS["BUSE_BLOG_URL"] = "博客URL";
$MESS["BUSE_BLOG_VAR"] = "博客標識符變量";
$MESS["BUSE_ID"] = "用戶身份";
$MESS["BUSE_PAGE_VAR"] = "頁面變量";
$MESS["BUSE_PATH_TO_USER"] = "用戶頁面路徑模板";
$MESS["BUSE_PATH_TO_USER_SETTINGS"] = "用戶設置頁面路徑模板";
$MESS["BUSE_USER_VAR"] = "用戶ID變量";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
