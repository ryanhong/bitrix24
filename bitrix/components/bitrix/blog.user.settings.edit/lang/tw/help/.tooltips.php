<?php
$MESS["BLOG_URL_TIP"] = "指定通過博客ID傳遞的代碼。";
$MESS["BLOG_VAR_TIP"] = "在此處指定通過博客ID傳遞的變量的名稱。";
$MESS["ID_TIP"] = "指定將傳遞用戶ID的代碼。";
$MESS["PAGE_VAR_TIP"] = "在此處指定博客頁面通過的變量的名稱。";
$MESS["PATH_TO_USER_SETTINGS_TIP"] = "用戶管理頁面的路徑。示例：<Nobr> blog_user_set.php？page = user_settings＆blog =＃blog＃＆user_id =＃user_id＃。</nobr>";
$MESS["PATH_TO_USER_TIP"] = "用戶配置文件頁面的路徑。示例：<Nobr> blog_user.php？page = user＆user_id =＃user_id＃。</nobr>";
$MESS["SET_TITLE_TIP"] = "如果該選項處於活動狀態，則頁面標題將設置為<nobr> <b>編輯用戶權限</b> <i> \“博客名稱\” </i>。</i>。</ibr>";
$MESS["USER_VAR_TIP"] = "在此處指定通過博客用戶ID傳遞的變量的名稱。";
