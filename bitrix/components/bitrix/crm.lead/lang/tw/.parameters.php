<?php
$MESS["CRM_ELEMENT_ID"] = "線索ID";
$MESS["CRM_LEAD_VAR"] = "線索ID變量名稱";
$MESS["CRM_NAME_TEMPLATE"] = "名稱格式";
$MESS["CRM_SEF_PATH_TO_CONVERT"] = "線索轉換頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "主編輯器頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "領導導入頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "索引頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_LIST"] = "線索頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_SERVICE"] = "Web服務頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "線索視圖頁面路徑模板";
