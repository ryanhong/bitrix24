<?php
$MESS["DISK_DOCUMENTS_ERROR_1"] = "您不是所有者。拒絕訪問。";
$MESS["DISK_DOCUMENTS_ERROR_2"] = "請聯繫文檔所有者以獲取其他文件權限。";
$MESS["DISK_DOCUMENTS_PAGE_TITLE"] = "文件";
