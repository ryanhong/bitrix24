<?php
$MESS["SONET_C11_DO_ACT"] = "發信息";
$MESS["SONET_C11_GROUP"] = "團體";
$MESS["SONET_C11_MESSAGE"] = "您的留言";
$MESS["SONET_C11_SUBTITLE"] = "對組消息的邀請將發送給用戶。用戶確認會員資格後，將在此處列出。";
$MESS["SONET_C11_SUCCESS"] = "您的邀請已發送給用戶。用戶確認會員資格後，將在此處列出。";
$MESS["SONET_C11_USER"] = "用戶";
