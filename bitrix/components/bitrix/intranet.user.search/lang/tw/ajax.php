<?php
$MESS["INTR_EMP_CANCEL"] = "取消";
$MESS["INTR_EMP_CANCEL_TITLE"] = "取消員工選擇";
$MESS["INTR_EMP_EXTRANET"] = "外部";
$MESS["INTR_EMP_HEAD"] = "部門主管";
$MESS["INTR_EMP_LAST"] = "最近的項目";
$MESS["INTR_EMP_NOTHING_FOUND"] = "搜索返回沒有結果";
$MESS["INTR_EMP_SEARCH"] = "搜索員工";
$MESS["INTR_EMP_SUBMIT"] = "選擇";
$MESS["INTR_EMP_SUBMIT_TITLE"] = "選擇選定的員工";
$MESS["INTR_EMP_WAIT"] = "載入中...";
$MESS["INTR_EMP_WINDOW_CLOSE"] = "關閉";
$MESS["INTR_EMP_WINDOW_TITLE"] = "選擇員工";
