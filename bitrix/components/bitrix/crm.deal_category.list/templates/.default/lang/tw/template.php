<?php
$MESS["CRM_ALL"] = "全部的";
$MESS["CRM_DEAL_CATEGORY_BUTTON_CANCEL"] = "取消";
$MESS["CRM_DEAL_CATEGORY_BUTTON_SAVE"] = "節省";
$MESS["CRM_DEAL_CATEGORY_DELETE"] = "刪除";
$MESS["CRM_DEAL_CATEGORY_DELETE_CONFIRM"] = "您確定要刪除\“＃名稱＃\”？";
$MESS["CRM_DEAL_CATEGORY_DELETE_TITLE"] = "刪除管道";
$MESS["CRM_DEAL_CATEGORY_EDIT"] = "編輯";
$MESS["CRM_DEAL_CATEGORY_EDIT_TITLE"] = "編輯管道";
$MESS["CRM_DEAL_CATEGORY_ERROR_TITLE"] = "保存錯誤";
$MESS["CRM_DEAL_CATEGORY_FIELD_NAME"] = "姓名";
$MESS["CRM_DEAL_CATEGORY_FIELD_NAME_NOT_ASSIGNED_ERROR"] = "缺少\“名稱\”字段值。";
$MESS["CRM_DEAL_CATEGORY_FIELD_SORT"] = "種類";
$MESS["CRM_DEAL_CATEGORY_STATUS_EDIT"] = "編輯階段";
$MESS["CRM_DEAL_CATEGORY_STATUS_EDIT_TITLE"] = "編輯管道階段";
$MESS["CRM_DEAL_CATEGORY_TITLE_CREATE"] = "創建管道";
$MESS["CRM_DEAL_CATEGORY_TITLE_EDIT"] = "編輯管道";
