<?php
$MESS["CONFIG_ACCESS_DENIED"] = "拒絕訪問";
$MESS["CONFIG_EMAIL_ERROR"] = "網站管理員電子郵件不正確。";
$MESS["CONFIG_FORMAT_NAME_ERROR"] = "名稱格式不正確。以下宏是可能的：＃標題＃，＃名稱＃，＃last_name＃，＃second_name＃，＃name_short＃，＃last_name_short＃，＃second_name_short＃，＃";
$MESS["CONFIG_GDRP_EMAIL_ERROR"] = "指定的電子郵件地址不正確。";
$MESS["CONFIG_GDRP_EMPTY_ERROR"] = "請完成所有必需的字段。";
$MESS["CONFIG_IP_ERROR"] = "IP地址不正確。";
$MESS["CONFIG_SMTP_PASS_ERROR"] = "提供的SMTP服務器密碼不匹配。";
$MESS["DISK_VERSION_LIMIT_PER_FILE_UNLIMITED"] = "無限";
