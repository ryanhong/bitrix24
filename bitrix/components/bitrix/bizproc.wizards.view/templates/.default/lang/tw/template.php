<?php
$MESS["BPWC_WLC_MISSING_DOCUMENT"] = "找不到業務流程。";
$MESS["BPWC_WNCT_2LIST"] = "業務流程";
$MESS["BPWC_WNCT_EVENTS"] = "發送事件";
$MESS["BPWC_WNCT_NAME"] = "姓名";
$MESS["BPWC_WNCT_STATE"] = "當前狀態";
$MESS["BPWC_WNCT_TAB1"] = "一般的";
$MESS["BPWC_WNCT_TAB2"] = "更多參數";
$MESS["BPWC_WNCT_TASKS"] = "您的任務";
$MESS["BPWC_WNCT_TL_DATE"] = "日期";
$MESS["BPWC_WNCT_TL_HISTORY"] = "業務流程歷史記錄";
$MESS["BPWC_WNCT_TL_NAME"] = "姓名";
$MESS["BPWC_WNCT_TL_NOTE"] = "筆記";
$MESS["BPWC_WNCT_TL_TOTAL"] = "全部的";
