<?php
$MESS["BPABL_PAGE_TITLE"] = "業務流程";
$MESS["BPWC_WLC_EMPTY_BPID"] = "缺少業務流程ID。";
$MESS["BPWC_WLC_EMPTY_IBLOCK"] = "未指定信息塊ID。";
$MESS["BPWC_WLC_ERROR"] = "錯誤";
$MESS["BPWC_WLC_WRONG_BP"] = "找不到業務流程。";
$MESS["BPWC_WLC_WRONG_IBLOCK"] = "找不到組件設置中指定的信息塊。";
$MESS["BPWC_WNC_EMPTY_IBLOCK_TYPE"] = "未指定信息塊類型。";
$MESS["BPWC_WNC_SESSID"] = "會話安全已被妥協。請再次填寫表格。";
$MESS["BPWC_WNC_WRONG_IBLOCK_TYPE"] = "找不到組件設置中指定的信息塊類型。";
