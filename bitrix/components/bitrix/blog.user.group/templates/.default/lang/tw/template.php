<?php
$MESS["BLOG_ADD"] = "添加";
$MESS["BLOG_CANCEL"] = "取消";
$MESS["BLOG_CONFIRM_DELETE"] = "該組包含用戶。您確定要刪除小組嗎？";
$MESS["BLOG_GROUP_ADD"] = "添加用戶組";
$MESS["BLOG_GROUP_DELETE"] = "刪除用戶組";
$MESS["BLOG_GROUP_NAME"] = "團隊名字：";
$MESS["BLOG_NAME_CHANGE"] = "更改組名稱";
