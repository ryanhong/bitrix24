<?php
$MESS["CATALOG_ADD"] = "添加到購物車";
$MESS["CATALOG_BUY"] = "買";
$MESS["CATALOG_NOT_AVAILABLE"] = "（庫存不可用）";
$MESS["CATALOG_TITLE"] = "標題";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "鏈接到此記錄的所有信息將被刪除。無論如何繼續？";
