<?php
$MESS["TASKS_DOUBLE_CLICK"] = "雙擊查看";
$MESS["TASKS_MARK"] = "分數";
$MESS["TASKS_MARK_N"] = "消極的";
$MESS["TASKS_MARK_NONE"] = "沒有任何";
$MESS["TASKS_MARK_P"] = "積極的";
$MESS["TASKS_PRIORITY"] = "優先事項";
$MESS["TASKS_PRIORITY_0"] = "低的";
$MESS["TASKS_PRIORITY_1"] = "普通的";
$MESS["TASKS_PRIORITY_2"] = "高的";
