<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>否CAHCE </i>：沒有執行緩存。";
$MESS["COLS_COUNT_TIP"] = "指定將渲染組的列數。";
$MESS["GROUPS_COUNT_TIP"] = "指定要在一頁中顯示的組數。僅顯示非空置組。";
$MESS["GROUP_VAR_TIP"] = "在此處指定博客組ID的變量的名稱。";
$MESS["PAGE_VAR_TIP"] = "在此處指定博客頁面通過的變量的名稱。";
$MESS["PATH_TO_GROUP_TIP"] = "博客組頁面的路徑。示例：blog_g_blog.php？page = group＆group_id =＃group_id＃。";
$MESS["SORT_BY1_TIP"] = "指定首先對博客組進行排序的字段。";
$MESS["SORT_BY2_TIP"] = "指定博客組將在第二次通過的字段中進行分類。";
$MESS["SORT_ORDER1_TIP"] = "指定第一個分類通過的排序順序。";
$MESS["SORT_ORDER2_TIP"] = "指定第二個分選通過的排序順序。";
