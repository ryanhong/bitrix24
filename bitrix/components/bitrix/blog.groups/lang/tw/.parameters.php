<?php
$MESS["BLOG_DESCR_COLS_COUNT"] = "列";
$MESS["BLOG_DESCR_GROUP_COUNT"] = "顯示組";
$MESS["BLOG_DESCR_GROUP_NAME"] = "團隊名字";
$MESS["BLOG_DESCR_SORT_1"] = "首次分類的字段";
$MESS["BLOG_DESCR_SORT_2"] = "第二分順序的字段";
$MESS["BLOG_DESCR_SORT_ASC"] = "上升";
$MESS["BLOG_DESCR_SORT_DESC"] = "下降";
$MESS["BLOG_DESCR_SORT_ORDER"] = "第一次分類的方向";
$MESS["BMG_GROUP_VAR"] = "博客組標識符變量";
$MESS["BMG_PAGE_VAR"] = "頁面變量";
$MESS["BMG_PATH_TO_GROUP"] = "博客組頁面路徑的模板";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
