<?php
$MESS["CRM_DOCUMENT_VIEW_COPY_PUBLIC_URL_MESSAGE"] = "公共鏈接已復製到剪貼板";
$MESS["CRM_DOCUMENT_VIEW_DOWNLOAD_IN"] = "下載為：";
$MESS["CRM_DOCUMENT_VIEW_EDIT_DOCUMENT"] = "編輯文檔";
$MESS["CRM_DOCUMENT_VIEW_EDIT_TEMPLATE"] = "編輯模板";
$MESS["CRM_DOCUMENT_VIEW_FEEDBACK"] = "回饋";
$MESS["CRM_DOCUMENT_VIEW_NO_AVAILABLE_FILES"] = "無需發送可用的文件";
$MESS["CRM_DOCUMENT_VIEW_PAYMENT_BUTTON"] = "收到付款";
$MESS["CRM_DOCUMENT_VIEW_PREVIEW_GENERATION_MESSAGE"] = "生成文檔";
$MESS["CRM_DOCUMENT_VIEW_PREVIEW_MESSAGE_PREPARE"] = "正在創建PDF預覽";
$MESS["CRM_DOCUMENT_VIEW_PREVIEW_MESSAGE_READY"] = "您可以立即將文檔下載為DOCX文件，或將鏈接發送給客戶";
$MESS["CRM_DOCUMENT_VIEW_PREVIEW_READY_MESSAGE"] = "該文檔現在以DOCX格式提供。您可以獲取文檔鏈接，下載或發送給客戶。";
$MESS["CRM_DOCUMENT_VIEW_PREVIEW_TIME_MESSAGE"] = "文檔圖像和PDF版本將大約進行。 20秒";
$MESS["CRM_DOCUMENT_VIEW_PUBLIC_LINK"] = "公共鏈接";
$MESS["CRM_DOCUMENT_VIEW_PUBLIC_ON"] = "在";
$MESS["CRM_DOCUMENT_VIEW_PUBLIC_URL_VIEWED_TIME"] = "客戶點擊<span>＃#time＃</span>的公共鏈接";
$MESS["CRM_DOCUMENT_VIEW_QR"] = "付款QR碼";
$MESS["CRM_DOCUMENT_VIEW_REQUISITES_MY_COMPANY_TITLE"] = "我的公司詳細信息";
$MESS["CRM_DOCUMENT_VIEW_SEND"] = "發送";
$MESS["CRM_DOCUMENT_VIEW_SEND_EMAIL"] = "通過電子郵件";
$MESS["CRM_DOCUMENT_VIEW_SEND_SMS"] = "通過短信";
$MESS["CRM_DOCUMENT_VIEW_SHOW_FULL_DOCUMENT"] = "顯示完整的文檔";
$MESS["CRM_DOCUMENT_VIEW_SIGNED"] = "帶有簽名和郵票";
$MESS["CRM_DOCUMENT_VIEW_SIGN_AND_STAMP"] = "郵票和簽名";
$MESS["CRM_DOCUMENT_VIEW_SIGN_BUTTON"] = "符號";
$MESS["CRM_DOCUMENT_VIEW_SIGN_CLICKED"] = "您已經開始簽署文檔。";
$MESS["CRM_DOCUMENT_VIEW_SIGN_DO_USE_PREVIOUS"] = "該模板已被負責人\“％啟動器％\”用於Deal \“％title％\”。您想再次使用它嗎？";
$MESS["CRM_DOCUMENT_VIEW_SIGN_DO_USE_PREVIOUS_MSG_MSGVER_2"] = "以前與Deal \“％title％\”一起使用了帶有文本和字段的完整配置模板。負責人是％啟動者％。您可以繼續使用此模板，或者在合適的情況下對其進行編輯。";
$MESS["CRM_DOCUMENT_VIEW_SIGN_DO_USE_PREVIOUS_MSG_MSGVER_3"] = "帶有文本和配置字段的模板已與Deal \“％title％\”一起使用，負責人是\“％啟動器％\”。您可以重複使用此模板或創建一個新模板。";
$MESS["CRM_DOCUMENT_VIEW_SIGN_NEW_BUTTON_MSGVER_1"] = "使用其他模板";
$MESS["CRM_DOCUMENT_VIEW_SIGN_NEW_BUTTON_MSGVER_2"] = "查看和編輯";
$MESS["CRM_DOCUMENT_VIEW_SIGN_NEW_BUTTON_MSGVER_3"] = "創建新的";
$MESS["CRM_DOCUMENT_VIEW_SIGN_OLD_BUTTON_MSGVER_1"] = "無論如何使用此模板";
$MESS["CRM_DOCUMENT_VIEW_SIGN_OLD_BUTTON_MSGVER_2"] = "使用現有模板";
$MESS["CRM_DOCUMENT_VIEW_SIGN_POPUP_CLOSE"] = "關閉";
$MESS["CRM_DOCUMENT_VIEW_SIGN_POPUP_TITLE_MSGVER_1"] = "簽名";
$MESS["CRM_DOCUMENT_VIEW_SIGN_POPUP_TITLE_MSGVER_2"] = "找到了以前的模板";
$MESS["CRM_DOCUMENT_VIEW_SMS_PUBLIC_URL_NECESSARY"] = "發送短信需要公共鏈接";
$MESS["CRM_DOCUMENT_VIEW_TRANSFORMATION_NO_PDF_ERROR"] = "找不到文檔的PDF版本。該文件可能未成功轉換。";
$MESS["CRM_DOCUMENT_VIEW_TRANSFORMATION_PROGRESS"] = "請等到轉換完成";
$MESS["CRM_DOCUMENT_VIEW_TRANSFORM_ERROR"] = "無法為文檔生成PDF文件。";
$MESS["CRM_DOCUMENT_VIEW_TRY_AGAIN"] = "再試一次";
$MESS["CRM_DOCUMENT_VIEW_TRY_LATER"] = "請稍後再試。";
