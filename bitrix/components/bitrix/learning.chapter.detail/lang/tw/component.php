<?php
$MESS["LEARNING_CHAPTER_DENIED"] = "章節未找到或拒絕訪問。";
$MESS["LEARNING_COURSES_CHAPTER_ADD"] = "添加新章節";
$MESS["LEARNING_COURSES_CHAPTER_DELETE"] = "刪除章節";
$MESS["LEARNING_COURSES_CHAPTER_DELETE_CONF"] = "這將刪除與本章相關的所有信息！繼續？";
$MESS["LEARNING_COURSES_CHAPTER_EDIT"] = "編輯章節";
$MESS["LEARNING_COURSES_LESSON_ADD"] = "添加新課";
$MESS["LEARNING_COURSE_DENIED"] = "沒有找到或拒絕培訓課程。";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "未安裝電子學習模塊。";
