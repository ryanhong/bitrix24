<?php
$MESS["SONET_C24_T_ACTIVITY"] = "最新活動";
$MESS["SONET_C24_T_ANY"] = "任何";
$MESS["SONET_C24_T_CREATE_GROUP"] = "創建組";
$MESS["SONET_C24_T_DO_CANCEL"] = "＆nbsp;＆nbsp; cancel＆nbsp;＆nbsp; nbsp;";
$MESS["SONET_C24_T_DO_SEARCH"] = "＆nbsp;＆nbsp; search＆nbsp;＆nbsp;";
$MESS["SONET_C24_T_MEMBERS"] = "成員";
$MESS["SONET_C24_T_ORDER_DATE"] = "按日期排序";
$MESS["SONET_C24_T_ORDER_REL"] = "按相關性排序";
$MESS["SONET_C24_T_SEARCH"] = "搜尋";
$MESS["SONET_C24_T_SEARCH_TITLE"] = "分組搜索";
$MESS["SONET_C24_T_SUBJ"] = "話題";
$MESS["SONET_C24_T_SUBJECT"] = "主題";
$MESS["SONET_C39_ARCHIVE_GROUP"] = "檔案集團";
