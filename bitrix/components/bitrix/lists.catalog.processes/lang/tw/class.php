<?php
$MESS["CC_LCP_ACCESS_DENIED"] = "您無權查看/編輯此列表。";
$MESS["CC_LCP_MODULE_NOT_INSTALLED"] = "未安裝通用列表模塊。";
$MESS["CC_LCP_NOT_PROCESSES"] = "沒有工作流程可供安裝。";
$MESS["CC_LCP_PROCESS_INSTALLED"] = " （已安裝）";
$MESS["CC_LCP_SONET_GROUP_DISABLED"] = "在此工作組中禁用列表。";
$MESS["CC_LCP_TITLE"] = "工作流目錄";
$MESS["CC_LCP_UNKNOWN_ERROR"] = "未知錯誤";
$MESS["CC_LCP_WRONG_IBLOCK"] = "選擇錯誤的列表。";
$MESS["CC_LCP_WRONG_IBLOCK_TYPE"] = "錯誤的信息塊類型。";
