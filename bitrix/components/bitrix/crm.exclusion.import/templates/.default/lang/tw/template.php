<?php
$MESS["CRM_EXCLUSION_IMPORT_BUTTON_CANCEL"] = "取消";
$MESS["CRM_EXCLUSION_IMPORT_BUTTON_LOAD"] = "進口";
$MESS["CRM_EXCLUSION_IMPORT_FORMAT_DESC"] = "輸入電子郵件和/或電話號碼，每個條目在新行上。<br> <br>另外，您可以添加註釋：在電子郵件或號碼之後添加半隆並輸入評論文本。 <br>示例：<br> <br> john@example.com <br> peter@example.com; peter（送貨）<br> +0112233445566 <br> +01234567890; ann";
$MESS["CRM_EXCLUSION_IMPORT_LOADING"] = "載入中";
$MESS["CRM_EXCLUSION_IMPORT_RECIPIENTS"] = "電子郵件和/或電話號碼";
