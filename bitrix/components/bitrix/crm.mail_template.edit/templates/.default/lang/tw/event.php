<?php
$MESS["CRM_TEMPLATE_EDIT_EVENT_CANCELED"] = "動作已取消。現在，您被重定向到上一頁。如果當前頁面仍在顯示，請手動關閉它。";
$MESS["CRM_TEMPLATE_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "消息模板<a href='#url#'>＃標題＃</a>已創建。現在，您被重定向到上一頁。如果當前頁面仍在顯示，請手動關閉它。";
