<?php
$MESS["COMM_COMMENT_OK"] = "評論被成功保存。";
$MESS["F_ERR_ADD_MESSAGE"] = "錯誤創建帖子。";
$MESS["F_ERR_ADD_TOPIC"] = "錯誤創建主題。";
$MESS["F_ERR_NOT_RIGHT_FOR_ADD"] = "添加評論的權限不足。";
$MESS["F_ERR_NO_REVIEW_TEXT"] = "請輸入您的評論。";
$MESS["F_ERR_REMOVE_COMMENT"] = "錯誤試圖刪除評論。";
$MESS["F_ERR_SESSION_TIME_IS_UP"] = "您的會議已經過期。請重新發布您的消息。";
$MESS["F_FORUM_MESSAGE_CNT"] = "評論";
$MESS["F_FORUM_TOPIC_ID"] = "論壇主題";
$MESS["POSTM_CAPTCHA"] = "驗證碼代碼不正確。";
$MESS["TASKS_COMMENT_MESSAGE_ADD"] = "在\ \“＃task_title＃\”：\“＃task_comment_text＃\”上註釋";
$MESS["TASKS_COMMENT_MESSAGE_ADD_F"] = "向\ \“＃Task_title＃\”添加了註釋。註釋文本是：\“＃Task_comment_text＃\”。";
$MESS["TASKS_COMMENT_MESSAGE_ADD_M"] = "在task \“＃task_title＃\”中添加了註釋，註釋文本：\“＃task_comment_text＃\”";
$MESS["TASKS_COMMENT_MESSAGE_EDIT"] = "對\“＃task_title＃\”的修改後註釋，新文本：\“＃task_comment_text＃\”";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_F"] = "將註釋更新為\“＃Task_title＃\”。新文本為：\“＃Task_comment_text＃\”。";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_M"] = "將註釋更改為\“＃Task_title＃\”。新文本為：\“＃Task_comment_text＃\”。";
$MESS["TASKS_COMMENT_SONET_NEW_TASK_MESSAGE"] = "創建的任務";
