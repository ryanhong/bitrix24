<?php
$MESS["SALE_CSDRP_ACCESS_DENIED"] = "拒絕訪問";
$MESS["SALE_CSDRP_DELIVERY_NOT_SUPPORTED"] = "送貨服務ID \“＃delivery_id＃\”不支持運輸訂單";
$MESS["SALE_CSDRP_OBJECT_DELIVERY_ERROR"] = "無法創建交付服務ID＃velivery_id＃的實例";
$MESS["SALE_CSDRP_SALE_NOT_INCLUDED"] = "不能包括\“ e-商店\”模塊。";
