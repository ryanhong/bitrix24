<?php
$MESS["CRM_EVENT_ADD_BUTTON"] = "添加";
$MESS["CRM_EVENT_ADD_FILE"] = "附加文件";
$MESS["CRM_EVENT_ADD_ID"] = "事件類型";
$MESS["CRM_EVENT_ADD_TITLE"] = "添加新事件";
$MESS["CRM_EVENT_DATE"] = "活動日期";
$MESS["CRM_EVENT_DESC_TITLE"] = "輸入事件說明";
$MESS["CRM_EVENT_MESSAGE_OBSOLETE"] = "\“電子郵件已發送\”類型已棄用；它僅用於向後兼容。請使用\“發送消息\”命令。";
$MESS["CRM_EVENT_PHONE_OBSOLETE"] = "\“電話\”類型已棄用；它僅用於向後兼容。請使用\“新呼叫\”命令。";
$MESS["CRM_EVENT_QUOTE_STATUS_ID_MSGVER_2"] = "估計階段";
$MESS["CRM_EVENT_STAGE_ID"] = "交易階段";
$MESS["CRM_EVENT_STATUS_ID_MSGVER_1"] = "鉛階段";
$MESS["CRM_EVENT_TITLE_COMPANY"] = "公司";
$MESS["CRM_EVENT_TITLE_CONTACT"] = "接觸";
$MESS["CRM_EVENT_TITLE_DEAL"] = "交易";
$MESS["CRM_EVENT_TITLE_LEAD"] = "帶領";
$MESS["CRM_EVENT_TITLE_ORDER"] = "命令";
$MESS["CRM_EVENT_TITLE_QUOTE"] = "引用";
