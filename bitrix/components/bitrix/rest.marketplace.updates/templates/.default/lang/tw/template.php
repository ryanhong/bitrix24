<?php
$MESS["MARKETPLACE_APP_INSTALL"] = "安裝";
$MESS["MARKETPLACE_APP_INSTALL_CANCEL"] = "取消";
$MESS["MARKETPLACE_APP_POPUP_LOAD"] = "載入中";
$MESS["MARKETPLACE_APP_PORTAL_ADMIN"] = "請聯繫您的Intranet管理員安裝更新。";
$MESS["MARKETPLACE_APP_VERSION"] = "版本";
$MESS["MARKETPLACE_UPDATES"] = "更新";
$MESS["MARKETPLACE_UPDATES_EMPTY"] = "當前沒有更新。";
$MESS["MARKETPLACE_UPDATE_APP_FREE"] = "自由的";
$MESS["MARKETPLACE_UPDATE_BUTTON"] = "更新";
$MESS["MARKETPLACE_UPDATE_INSTALLED"] = "安裝";
