<?php
$MESS["MYMS_TPL_JS_ERROR"] = "錯誤";
$MESS["MYMS_TPL_JS_RESULTS"] = "成立";
$MESS["MYMS_TPL_JS_RESULTS_EMPTY"] = "您的搜索與任何位置都不匹配。";
$MESS["MYMS_TPL_JS_SEARCH"] = "搜索結果";
$MESS["MYMS_TPL_SEARCH"] = "搜索地址";
$MESS["MYMS_TPL_SUBMIT"] = "搜尋";
