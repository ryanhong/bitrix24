<?php
$MESS["CP_BCE1_ELEMENTS_PER_STEP"] = "在一個步驟中導出的元素（0-一次導出）";
$MESS["CP_BCE1_GROUP_PERMISSIONS"] = "允許出口";
$MESS["CP_BCE1_IBLOCK_ID"] = "目錄信息塊";
$MESS["CP_BCE1_INTERVAL"] = "出口階段時間，秒。 （0-一次出口一次）";
$MESS["CP_BCE1_USE_ZIP"] = "盡可能使用ZIP壓縮";
