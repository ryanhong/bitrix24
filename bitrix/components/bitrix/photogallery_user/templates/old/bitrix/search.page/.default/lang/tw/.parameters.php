<?php
$MESS["SEARCH_CELL_COUNT"] = "連續的照片數";
$MESS["SEARCH_CNT"] = "按頻率";
$MESS["SEARCH_COLOR_NEW"] = "最早的標籤顏色（例如\“ C0C0C0 \”）";
$MESS["SEARCH_COLOR_OLD"] = "最新的標籤顏色（例如\ \“ fefefe \”）";
$MESS["SEARCH_COLOR_TYPE"] = "使用梯度顏色";
$MESS["SEARCH_FONT_MAX"] = "最大的字體尺寸（PX）";
$MESS["SEARCH_FONT_MIN"] = "最小的字體尺寸（PX）";
$MESS["SEARCH_ITEM_URL"] = "替代URL";
$MESS["SEARCH_NAME"] = "按名字";
$MESS["SEARCH_PAGE_ELEMENTS"] = "標籤數";
$MESS["SEARCH_PERIOD"] = "（天）之內的搜索標籤";
$MESS["SEARCH_PERIOD_NEW_TAGS"] = "考慮在（日）期間標記新的標籤";
$MESS["SEARCH_SHOW_CHAIN"] = "顯示麵包屑導航";
$MESS["SEARCH_SORT"] = "等級標籤";
$MESS["SEARCH_TAGS_INHERIT"] = "狹窄的搜索區域";
$MESS["SEARCH_URL_SEARCH"] = "搜索頁面的路徑（相對於站點根）";
$MESS["SEARCH_WIDTH"] = "標籤雲寬度（ex。\ \“ 100％\”，\“ 100px \”，\“ 100pt \”或\ \“ 100in \”）";
