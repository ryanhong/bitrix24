<?php
$MESS["P_ERROR_CODE"] = "畫廊代碼不正確。畫廊的功能可能不正確。";
$MESS["P_GALLERY_ACTIVE"] = "預設";
$MESS["P_GALLERY_CREATE"] = "創建畫廊";
$MESS["P_GALLERY_CREATE_TITLE"] = "創建個人畫廊";
$MESS["P_GALLERY_DELETE"] = "刪除畫廊";
$MESS["P_GALLERY_DELETE_ASK"] = "您確定要刪除畫廊嗎？這個操作不能被撤銷！";
$MESS["P_GALLERY_EDIT"] = "編輯畫廊設置";
$MESS["P_GALLERY_EDIT_TITLE"] = "編輯畫廊設置";
$MESS["P_GALLERY_VIEW_TITLE"] = "查看專輯中的專輯＃畫廊＃＆raquo;";
$MESS["P_GALLEY_BY_USER"] = "作者：";
$MESS["P_UPLOAD"] = "上傳照片";
$MESS["P_UPLOAD_TITLE"] = "上傳照片";
