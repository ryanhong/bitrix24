<?php
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "信息塊模塊未安裝。";
$MESS["P_BAD_IBLOCK_ID"] = "未知信息塊ID。";
$MESS["P_CONTINUE"] = "繼續";
$MESS["P_DENIED_ACCESS"] = "拒絕訪問。";
$MESS["P_MODULE_IS_NOT_INSTALLED"] = "未安裝模塊照片庫2.0。";
$MESS["P_RECALC_1"] = "照片＃element_number＃of＃elements_cnt＃＃";
$MESS["P_RECALC_2"] = "您需要重新計算照片庫的文件大小。";
$MESS["P_RECALC_3"] = "照片庫的大小執行＃日期＃的最後重新計算是不完整的。";
$MESS["P_RECALC_4"] = "照片庫的大小執行＃日期＃的最後重新計算。";
$MESS["P_RECALC_5"] = "服務器響應未知！";
$MESS["P_RECALC_6"] = "重新計算已在＃日期＃上成功完成。";
$MESS["P_START"] = "重新計算";
$MESS["P_STOP"] = "停止";
$MESS["P_TITLE"] = "照片畫廊的尺寸重新計算";
