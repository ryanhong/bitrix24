<?php
$MESS["AUTH_AUTH"] = "授權";
$MESS["AUTH_HELP"] = "幫助";
$MESS["AUTH_LOGOUT"] = "登出";
$MESS["AUTH_NOTIFICATION"] = "通知";
$MESS["AUTH_PROFILE"] = "我的簡歷";
$MESS["AUTH_PROFILE_B24NET"] = "我的bitrix24.network頁面";
$MESS["AUTH_THEME_DIALOG"] = "主題";
$MESS["AUTH_THEME_DIALOG_HINT"] = "個性化您的Bitrix24！<br>選擇背景圖像或視頻。";
$MESS["B24_UPGRADE_LICENSE"] = "升級您的計劃";
$MESS["INTRANET_USER_PROFILE_ADMIN_PANEL"] = "控制面板";
$MESS["INTRANET_USER_PROFILE_CHANGE"] = "改變";
$MESS["INTRANET_USER_PROFILE_CONFIGURE"] = "配置";
$MESS["INTRANET_USER_PROFILE_DESKTOP_APPLE"] = "MacOS的Bitrix24";
$MESS["INTRANET_USER_PROFILE_DESKTOP_LINUX"] = "Linux的Bitrix24";
$MESS["INTRANET_USER_PROFILE_DESKTOP_WINDOWS"] = "Windows的Bitrix24";
$MESS["INTRANET_USER_PROFILE_DOWNLOAD"] = "下載";
$MESS["INTRANET_USER_PROFILE_DOWNLOAD_LINUX_DEB"] = "對於Linux Deb";
$MESS["INTRANET_USER_PROFILE_DOWNLOAD_LINUX_RPM"] = "對於Linux RPM";
$MESS["INTRANET_USER_PROFILE_GOTO"] = "看法";
$MESS["INTRANET_USER_PROFILE_HISTORY_BUTTON_LOGOUT_ALL_DEVICE"] = "在所有設備上註銷";
$MESS["INTRANET_USER_PROFILE_HISTORY_BUTTON_LOGOUT_ALL_DEVICE_ERROR"] = "錯誤記錄。請再試一次。";
$MESS["INTRANET_USER_PROFILE_HISTORY_BUTTON_LOGOUT_ALL_DEVICE_TITLE"] = "確認行動";
$MESS["INTRANET_USER_PROFILE_HISTORY_BUTTON_LOGOUT_ALL_DEVICE_WITHOUT_THIS_MESSAGE"] = "除此之外，您將在所有設備上的Bitrix24帳戶中登錄您的BitRix24帳戶。你想繼續嗎？";
$MESS["INTRANET_USER_PROFILE_HISTORY_BUTTON_LOGOUT_ALL_DEVICE_WITHOUT_THIS_RESULT"] = "除此之外，您已經成功在所有設備上登錄。";
$MESS["INTRANET_USER_PROFILE_HISTORY_BUTTON_LOGOUT_THIS_DEVICE_DESCRIPTION"] = "最後登錄到Bitrix24帳戶";
$MESS["INTRANET_USER_PROFILE_HISTORY_BUTTON_SHOW_FULL_LIST"] = "顯示整個登錄歷史記錄";
$MESS["INTRANET_USER_PROFILE_HISTORY_TITLE"] = "登錄歷史記錄";
$MESS["INTRANET_USER_PROFILE_INSTALL"] = "安裝";
$MESS["INTRANET_USER_PROFILE_INSTALLED"] = "安裝";
$MESS["INTRANET_USER_PROFILE_MASKS"] = "幀";
$MESS["INTRANET_USER_PROFILE_MOBILE_DESCRIPTION"] = "用手機掃描QR碼";
$MESS["INTRANET_USER_PROFILE_MOBILE_HOW_DOES_IT_WORK"] = "它是如何工作的？";
$MESS["INTRANET_USER_PROFILE_MOBILE_SHOW_QR"] = "顯示QR碼";
$MESS["INTRANET_USER_PROFILE_MOBILE_SHOW_QR_SMALL"] = "顯示QR碼";
$MESS["INTRANET_USER_PROFILE_MOBILE_TITLE2"] = "電話登錄";
$MESS["INTRANET_USER_PROFILE_MOBILE_TITLE2_SMALL"] = "電話<br>登錄";
$MESS["INTRANET_USER_PROFILE_OTP_DESCRIPTION"] = "它是如何工作的？";
$MESS["INTRANET_USER_PROFILE_OTP_MESSAGE"] = "兩步身份驗證";
$MESS["INTRANET_USER_PROFILE_OTP_MESSAGE_WITH_TRANSFER"] = "兩步<br>身份驗證";
$MESS["INTRANET_USER_PROFILE_OTP_TITLE"] = "OTP受保護";
$MESS["INTRANET_USER_PROFILE_PROFILE"] = "輪廓";
$MESS["INTRANET_USER_PROFILE_PULSE_MY_IS_EMPTY"] = "數據不足";
$MESS["INTRANET_USER_PROFILE_PULSE_MY_IS_EMPTY_BRIEF"] = "數據不足";
$MESS["INTRANET_USER_PROFILE_PULSE_MY_POSITION"] = "＃位置＃在＃中＃";
$MESS["INTRANET_USER_PROFILE_PULSE_MY_RATING"] = "我的評分";
$MESS["INTRANET_USER_PROFILE_PULSE_TITLE"] = "公司脈衝";
$MESS["INTRANET_USER_PROFILE_QRCODE_BODY2"] = "使用您的個人QR碼進行更快的身份驗證。
<br> <br>
<b>切勿向任何人顯示此代碼。</b>它包含您的個人登錄信息。";
$MESS["INTRANET_USER_PROFILE_QRCODE_TITLE2"] = "登錄到移動Bitrix24";
$MESS["INTRANET_USER_PROFILE_SOON"] = "很快";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_NORESULT_BUTTON"] = "我該如何衡量？";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_NORESULT_INDICATOR_TEXT"] = "壓力";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_NORESULT_TITLE"] = "測量壓力水平";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_RESULT_COME_ON"] = "測量壓力水平";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_RESULT_DESCRIPTION"] = "它是如何工作的？";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_RESULT_HINT_INVISIBLE"] = "其他用戶無法查看您的壓力水平。";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_RESULT_HINT_VISIBLE"] = "其他用戶可以查看您的壓力水平。";
$MESS["INTRANET_USER_PROFILE_STRESSLEVEL_RESULT_SHARE_LINK"] = "複製到剪貼板";
$MESS["INTRANET_USER_PROFILE_TURNED_ON"] = "連接的";
$MESS["INTRANET_USER_PROFILE_TURN_ON"] = "連接";
$MESS["INTRANET_USER_PROFILE_admin"] = "行政人員";
$MESS["INTRANET_USER_PROFILE_email"] = "電子郵件用戶";
$MESS["INTRANET_USER_PROFILE_extranet"] = "外部";
$MESS["INTRANET_USER_PROFILE_fired"] = "被解僱";
$MESS["INTRANET_USER_PROFILE_integrator"] = "Bitrix24合作夥伴";
$MESS["INTRANET_USER_PROFILE_invited"] = "被邀請";
$MESS["INTRANET_USER_PROFILE_shop"] = "在線商店用戶";
$MESS["INTRANET_USER_PROFILE_visitor"] = "遊客";
$MESS["INTRANET_USTAT_COMPANY_HELP_RATING"] = "評級基於所有在指定的時間範圍內至少一次訪問BitRix24工具的員工的個人活動水平。";
