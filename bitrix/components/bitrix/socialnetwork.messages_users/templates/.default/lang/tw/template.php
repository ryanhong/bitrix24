<?php
$MESS["SONET_C30_T_ACTIONS"] = "動作";
$MESS["SONET_C30_T_BAN"] = "禁止";
$MESS["SONET_C30_T_CHAT"] = "與用戶聊天";
$MESS["SONET_C30_T_EMPTY"] = "沒有歷史記錄。<br>此窗格向您向您發送或收到消息的用戶展示了您的信息。";
$MESS["SONET_C30_T_MESSAGES"] = "消息";
$MESS["SONET_C30_T_NEW"] = "新的";
$MESS["SONET_C30_T_ONLINE"] = "在線的";
$MESS["SONET_C30_T_TOTAL"] = "全部的";
$MESS["SONET_C30_T_USER"] = "用戶";
$MESS["SONET_C30_T_WRITE_MESSAGE"] = "發信息";
