<?php
$MESS["TASKS_INTERFACE_FILTER_NO_TASKS_FOR_EXPORT"] = "沒有導出的任務";
$MESS["TASKS_INTERFACE_FILTER_PRESETS_MOVED_TEXT_V2"] = "除了將它們用作在角色之間快速切換的預設外，您還可以自定義它們，添加新的過濾器並刪除不使用的過濾器。";
$MESS["TASKS_INTERFACE_FILTER_PRESETS_MOVED_TITLE"] = "搜索過濾器現在可以使用持續，監督，關注和協助的部分";
