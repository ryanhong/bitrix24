<?php
$MESS["CRM_1C_START_FACE_CARD_ADV_1"] = "易於連接";
$MESS["CRM_1C_START_FACE_CARD_ADV_2"] = "簡單配置";
$MESS["CRM_1C_START_FACE_CARD_ADV_3"] = "完整的客戶資料";
$MESS["CRM_1C_START_FACE_CARD_B24_BLOCK_TEXT2"] = "使用facecard設置自己的折扣計劃。面部識別僅在主要計劃中是免費的。";
$MESS["CRM_1C_START_FACE_CARD_B24_BLOCK_TITLE2"] = "Facecart可用於主要計劃。";
$MESS["CRM_1C_START_FACE_CARD_CONSENT_AGREED"] = "我接受條款";
$MESS["CRM_1C_START_FACE_CARD_CONSENT_TITLE"] = "使用條款";
$MESS["CRM_1C_START_FACE_CARD_DO_START"] = "連接";
$MESS["CRM_1C_START_FACE_CARD_INFO_TITLE"] = "客戶不再需要在他們身上攜帶折扣卡，他們的存在或照片就是所需的。";
$MESS["CRM_1C_START_FACE_CARD_INSTALL_INFO"] = "安裝面卡模塊。創建客戶忠誠度卡。增加銷量。";
$MESS["CRM_1C_START_FACE_CARD_NAME"] = "面卡";
$MESS["CRM_1C_START_FACE_CARD_WARN_TEXT"] = "請注意，面部識別功能將分別充電。 <a class = \"b24-integration-desc-link \" href= \\ \\\\\\\\\\\\\\\ \\\\\\\\\\\\\\ \"> plus </a>計劃包括無限的免費面部識別。";
