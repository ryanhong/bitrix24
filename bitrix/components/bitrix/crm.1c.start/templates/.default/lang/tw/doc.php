<?php
$MESS["CRM_1C_START_DOC_ADV_1"] = "易於連接";
$MESS["CRM_1C_START_DOC_ADV_2"] = "簡單配置";
$MESS["CRM_1C_START_DOC_ADV_3"] = "只有一個窗口";
$MESS["CRM_1C_START_DOC_ADV_TITLE"] = "1C現在是Bitrix24的一部分。";
$MESS["CRM_1C_START_DOC_DO_START"] = "連接";
$MESS["CRM_1C_START_DOC_INFO_TEXT"] = "在Bitrix24中安裝1C模塊並編輯1C文檔。";
$MESS["CRM_1C_START_DOC_NAME"] = "1C：退縮";
