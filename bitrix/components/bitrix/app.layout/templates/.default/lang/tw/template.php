<?php
$MESS["REST_ALT_NEED_REINSTALL"] = "<b>注意！</b>安全系統已更新。建議您聯繫您的Intranet管理員，以重新安裝保留當前數據的應用程序。";
$MESS["REST_ALT_NEED_REINSTALL_ADMIN"] = "<b>注意！</b>安全系統已更新。建議您重新安裝保留當前數據的應用程序。 <a href= \"#detail_url# \ \">現在重新安裝</a>";
$MESS["REST_ALT_UPDATE_AVAIL"] = "現在有一個新的應用程序版本。請聯繫您的Intranet管理員以安裝它。";
$MESS["REST_ALT_UPDATE_AVAIL_ADMIN"] = "現在可以使用新版本。 <a href= \“#detail_url# \ \">立即安裝</a>";
$MESS["REST_LOADING"] = "加載＃app_name＃";
