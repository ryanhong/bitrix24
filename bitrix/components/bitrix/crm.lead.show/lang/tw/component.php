<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "未安裝業務流程模塊。";
$MESS["CRM_ALL"] = "全部的";
$MESS["CRM_FIELD_ADDRESS"] = "地址";
$MESS["CRM_FIELD_ASSIGNED_BY_ID"] = "負責任的";
$MESS["CRM_FIELD_COMMENTS"] = "評論";
$MESS["CRM_FIELD_COMPANY_ID"] = "公司";
$MESS["CRM_FIELD_COMPANY_TITLE"] = "公司名稱";
$MESS["CRM_FIELD_COMPANY_TITLE_2"] = "公司";
$MESS["CRM_FIELD_CONTACT_ID"] = "接觸";
$MESS["CRM_FIELD_CREATED_BY_ID"] = "由...製作";
$MESS["CRM_FIELD_CURRENCY_ID"] = "貨幣";
$MESS["CRM_FIELD_DATE_CREATE"] = "創建";
$MESS["CRM_FIELD_DATE_MODIFY"] = "修改的";
$MESS["CRM_FIELD_EMAIL"] = "電子郵件";
$MESS["CRM_FIELD_ENTITY_TREE"] = "依賴性";
$MESS["CRM_FIELD_EXCH_RATE"] = "匯率";
$MESS["CRM_FIELD_FIND"] = "搜尋";
$MESS["CRM_FIELD_FULL_NAME"] = "接觸";
$MESS["CRM_FIELD_HONORIFIC"] = "致敬";
$MESS["CRM_FIELD_LAST_NAME"] = "姓";
$MESS["CRM_FIELD_LEAD_ACTIVITY"] = "任務";
$MESS["CRM_FIELD_LEAD_ACTIVITY_LIST"] = "活動";
$MESS["CRM_FIELD_LEAD_BIZPROC"] = "業務流程";
$MESS["CRM_FIELD_LEAD_CALENDAR"] = "日曆記錄";
$MESS["CRM_FIELD_LEAD_COMPANY"] = "主要公司";
$MESS["CRM_FIELD_LEAD_CONTACTS"] = "鉛觸點";
$MESS["CRM_FIELD_LEAD_DEAL"] = "領先交易";
$MESS["CRM_FIELD_LEAD_EVENT"] = "領先事件";
$MESS["CRM_FIELD_LEAD_QUOTE"] = "鉛報價";
$MESS["CRM_FIELD_LIVE_FEED"] = "溪流";
$MESS["CRM_FIELD_MESSENGER"] = "信使";
$MESS["CRM_FIELD_MODIFY_BY_ID"] = "修改";
$MESS["CRM_FIELD_NAME"] = "名";
$MESS["CRM_FIELD_OPENED"] = "每個人都可以使用";
$MESS["CRM_FIELD_OPPORTUNITY"] = "機會";
$MESS["CRM_FIELD_PHONE"] = "電話";
$MESS["CRM_FIELD_POST"] = "位置";
$MESS["CRM_FIELD_PRODUCT_ID"] = "產品";
$MESS["CRM_FIELD_PRODUCT_ROWS"] = "主要產品";
$MESS["CRM_FIELD_SECOND_NAME"] = "中間名字";
$MESS["CRM_FIELD_SOURCE_DESCRIPTION"] = "來源信息";
$MESS["CRM_FIELD_SOURCE_ID"] = "來源";
$MESS["CRM_FIELD_STATUS_DESCRIPTION_MSGVER_1"] = "有關此階段的更多信息";
$MESS["CRM_FIELD_STATUS_ID_MSGVER_1"] = "階段";
$MESS["CRM_FIELD_TITLE"] = "線索名稱";
$MESS["CRM_FIELD_UTM"] = "UTM參數";
$MESS["CRM_FIELD_WEB"] = "地點";
$MESS["CRM_LEAD_CREATE_ON_BASIS"] = "產生";
$MESS["CRM_LEAD_EVENT_INFO"] = "＃event_name＃＆rarr; ＃new_value＃";
$MESS["CRM_LEAD_NAV_TITLE_ADD"] = "添加鉛";
$MESS["CRM_LEAD_NAV_TITLE_EDIT"] = "鉛：＃名稱＃";
$MESS["CRM_LEAD_NAV_TITLE_LIST"] = "線索列表";
$MESS["CRM_LEAD_NAV_TITLE_LIST_SHORT"] = "鉛";
$MESS["CRM_LEAD_SHOW_FIELD_BIRTHDATE"] = "出生日期";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
$MESS["CRM_SECTION_ACTIVITIES"] = "活動";
$MESS["CRM_SECTION_ACTIVITY_CALENDAR"] = "電話和事件";
$MESS["CRM_SECTION_ACTIVITY_MAIN"] = "領先活動";
$MESS["CRM_SECTION_ACTIVITY_TASK"] = "任務";
$MESS["CRM_SECTION_ADDITIONAL"] = "更多的";
$MESS["CRM_SECTION_BIZPROC"] = "業務流程";
$MESS["CRM_SECTION_BIZPROC_MAIN"] = "領導業務流程";
$MESS["CRM_SECTION_CONTACT_INFO"] = "聯繫信息";
$MESS["CRM_SECTION_CONTACT_INFO_2"] = "聯繫信息";
$MESS["CRM_SECTION_DETAILS"] = "細節";
$MESS["CRM_SECTION_EVENT_MAIN"] = "線索日誌";
$MESS["CRM_SECTION_LEAD"] = "帶領";
$MESS["CRM_SECTION_LIVE_FEED"] = "溪流";
$MESS["CRM_SECTION_PRODUCT_ROWS"] = "產品";
$MESS["RESPONSIBLE_NOT_ASSIGNED"] = "[未分配]";
