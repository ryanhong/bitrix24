<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>不緩存</i>：沒有執行緩存。";
$MESS["DISPLAY_PANEL_TIP"] = "如果已檢查，則在“站點編輯”模式下的管理工具欄和組件編輯區域工具欄上顯示該工具按鈕。";
$MESS["IBLOCK_TYPE_TIP"] = "在此處選擇系統中當前存在的信息塊之一。";
$MESS["IBLOCK_URL_TIP"] = "包含信息塊部分的頁面的路徑。";
