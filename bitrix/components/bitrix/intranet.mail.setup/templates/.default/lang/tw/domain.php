<?php
$MESS["INTR_MAIL_AJAX_ERROR"] = "執行查詢的錯誤";
$MESS["INTR_MAIL_CHECK_JUST_NOW"] = "幾秒鐘前";
$MESS["INTR_MAIL_CHECK_TEXT"] = "上次檢查＃日期＃";
$MESS["INTR_MAIL_CHECK_TEXT_NA"] = "沒有域狀態的數據";
$MESS["INTR_MAIL_CHECK_TEXT_NEXT"] = "下一個郵件檢查＃日期＃";
$MESS["INTR_MAIL_DOMAINREMOVE_CONFIRM"] = "您想斷開域嗎？";
$MESS["INTR_MAIL_DOMAINREMOVE_CONFIRM_TEXT"] = "您是否要分離域？<br>附加到門戶網站的所有郵箱也將被分離！";
$MESS["INTR_MAIL_DOMAIN_BAD_NAME"] = "無效的名稱";
$MESS["INTR_MAIL_DOMAIN_BAD_NAME_HINT"] = "域名可以包括拉丁字符，數字和連字符；無法用連字符開始或結束，也不能以3和4的位置重複連字符。以<b> .ru <b>結束名稱。";
$MESS["INTR_MAIL_DOMAIN_CHECK"] = "核實";
$MESS["INTR_MAIL_DOMAIN_CHOOSE_HINT"] = "在.ru域中選擇一個名稱";
$MESS["INTR_MAIL_DOMAIN_CHOOSE_TITLE"] = "選擇域";
$MESS["INTR_MAIL_DOMAIN_EMPTY_NAME"] = "輸入名字";
$MESS["INTR_MAIL_DOMAIN_EULA_CONFIRM"] = "我接受<a href= \"http://www.bitrix24.ru/about/domain.php \" target= \“_blank \">許可協議</a>";
$MESS["INTR_MAIL_DOMAIN_HELP"] = "如果您沒有配置與Yandex託管的電子郵件一起配置的域，請立即進行。
<br/> <br/>
-  <a href= \之https://passport.yandex.com/registration/xregistration/xtart = \"_blank \">創建一個yandex託管的電子郵件帳戶</a>或使用現有郵箱（如果您有一個） 。
-  <a href= \"https://pdd.yandex.ru/domains_add/xtart target = \"_blank \">將您的域</a>附加到yandex託管的電子郵件<sup>（<a href = a href = \ http：//help.yandex.ru/pdd/add-domain/add-exist.xml \“ target = \” _ blank \“ title = \”我如何做？\ \'>？</ a> ）</sup> <br/>
 - 驗證您的域所有權<sup>（<a href = \'http：//help.yandex.ru/pdd/confirm-domain.xml \' ？\'>？</a>）</sup> < br/>
 - 配置MX Records <sup>（<a href = \“ http：//help.yandex.ru/pdd/records.xml#mx \” target = \“ _ blank \” title = \“我怎麼做？ \ '>？</a>）</sup>或將您的域委派給yandex <sup>（<a href = \ \“ http：//help.yandex.ru/pdd/pdd/hosting.xml#delegate \” target = \“ _ blank \” title = \“我如何做？\”>？</a>）</sup>
<br/> <br/>
Yandex託管電子郵件帳戶已配置後，將域將域附加到您的Bitrix24：
<br/> <br/>
 -  <a href = \'https：//pddimp.yandex.ru/api2/admin/get_token \“ target = \” _ black \“ onclick = \” 480，寬度= 720，top ='+parseint（screet. height/2-240）+'，left ='+parseint（screet.width/2-360））; return false; \ \ \'>獲取令牌</ a>（填寫表單字段，然後單擊\“獲取令牌”。
 - 將域和令牌添加到參數。";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP1"] = "步驟＆nbsp; 1.＆nbsp;＆nbsp;確認域的所有權";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP1_A"] = "將一個名為<b>＃secret_n＃.html </b>的文件上傳到您的網站root目錄。該文件必須包含文本：<b> #secret_c＃</b>";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP1_B"] = "要配置CNAME記錄，您需要在註冊商或Web託管服務上寫入對域的DNS記錄的寫入訪問權限。您將在帳戶或控制面板中找到這些設置。";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP1_B_NAME"] = "記錄名稱：";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP1_B_NAMEV"] = "<b> yamail-＃secret_n＃</b>（或<b> yamail-＃secret_n＃。＃domain＃。</b>。";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP1_B_PROMPT"] = "指定這些值：";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP1_B_TYPE"] = "記錄類型：";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP1_B_VALUE"] = "價值：";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP1_B_VALUEV"] = "<b> mail.yandex.ru。</b>（再次注意時期）";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP1_C"] = "將聯繫人的電子郵件地址設置為您的域註冊信息中的信息<b> #secret_n#@yandex.ru </b>。使用您的域註冊商控制面板設置電子郵件地址。";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP1_C_HINT"] = "確認域後，將此地址更改為您的真實電子郵件。";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP1_HINT"] = "如果您有任何疑問驗證您的域名所有權，請通過<a href= \之https://helpdesk.bitrix24.com/ target= \"_blank \"> hervesk.bitesk.bitrix24.com </a>與Helpdesk聯繫。 。";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP1_OR"] = "或者";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP1_PROMPT"] = "您需要使用以下方法之一確認擁有指定的域名：";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP2"] = "步驟＆nbsp; 2.＆nbsp; nbsp;配置MX記錄";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP2_HINT"] = "刪除與Yandex無關的所有其他MX和TXT記錄。對MX記錄的更改可能需要幾個小時到三天才能在整個互聯網上進行更新。";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP2_MXPROMPT"] = "創建一個具有以下參數的新MX記錄：";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP2_NAME"] = "記錄名稱：";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP2_NAMEV"] = "<b>@</b>（或<b> #domain＃。</b>  - 如果需要。請注意結束時期）";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP2_PRIORITY"] = "優先事項：";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP2_PROMPT"] = "確認域名後，您將必須更改網絡託管上的相應MX記錄。";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP2_TITLE"] = "配置MX記錄";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP2_TYPE"] = "記錄類型：";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP2_VALUE"] = "價值：";
$MESS["INTR_MAIL_DOMAIN_INSTR_STEP2_VALUEV"] = "<b> mx.yandex.net。</b>";
$MESS["INTR_MAIL_DOMAIN_INSTR_TITLE"] = "要將您的域連接到Bitrix24，有幾個步驟。";
$MESS["INTR_MAIL_DOMAIN_LONG_NAME"] = "最大限度。 .ru之前的63個字符";
$MESS["INTR_MAIL_DOMAIN_NAME_FREE"] = "這個名稱可用";
$MESS["INTR_MAIL_DOMAIN_NAME_OCCUPIED"] = "這個名稱不可用";
$MESS["INTR_MAIL_DOMAIN_NOCONFIRM"] = "域未得到確認";
$MESS["INTR_MAIL_DOMAIN_NOMX"] = "MX記錄未配置";
$MESS["INTR_MAIL_DOMAIN_REG_CONFIRM_TEXT"] = "連接後，您將無法更改域名<br>或獲得另一個域名，因為您可以註冊<br> bitrix24只有一個域。<br> <br>如果您找到了名稱<b>＃域＃</b>是正確的，請確認您的新域。";
$MESS["INTR_MAIL_DOMAIN_REG_CONFIRM_TITLE"] = "請檢查您是否正確輸入了域名。";
$MESS["INTR_MAIL_DOMAIN_REMOVE"] = "分離";
$MESS["INTR_MAIL_DOMAIN_SAVE"] = "節省";
$MESS["INTR_MAIL_DOMAIN_SAVE2"] = "附";
$MESS["INTR_MAIL_DOMAIN_SETUP_HINT"] = "域名可能需要1小時到幾天才能確認。";
$MESS["INTR_MAIL_DOMAIN_SHORT_NAME"] = ".ru之前至少兩個字符";
$MESS["INTR_MAIL_DOMAIN_STATUS_CONFIRM"] = "確認的";
$MESS["INTR_MAIL_DOMAIN_STATUS_NOCONFIRM"] = "還沒有確定";
$MESS["INTR_MAIL_DOMAIN_STATUS_NOMX"] = "MX記錄未配置";
$MESS["INTR_MAIL_DOMAIN_STATUS_TITLE"] = "域鏈接狀態";
$MESS["INTR_MAIL_DOMAIN_STATUS_TITLE2"] = "域證實";
$MESS["INTR_MAIL_DOMAIN_SUGGEST_MORE"] = "顯示其他選項";
$MESS["INTR_MAIL_DOMAIN_SUGGEST_TITLE"] = "請提出另一個名字或選擇一個";
$MESS["INTR_MAIL_DOMAIN_SUGGEST_WAIT"] = "搜索可能的名字...";
$MESS["INTR_MAIL_DOMAIN_TITLE"] = "如果您的域是在yandex.mail for域中配置的";
$MESS["INTR_MAIL_DOMAIN_TITLE2"] = "該域現在鏈接到您的門戶";
$MESS["INTR_MAIL_DOMAIN_TITLE3"] = "您的電子郵件域";
$MESS["INTR_MAIL_DOMAIN_WAITCONFIRM"] = "等待確認";
$MESS["INTR_MAIL_DOMAIN_WAITMX"] = "MX記錄未配置";
$MESS["INTR_MAIL_DOMAIN_WHOIS"] = "查看";
$MESS["INTR_MAIL_GET_TOKEN"] = "得到";
$MESS["INTR_MAIL_INP_CANCEL"] = "取消";
$MESS["INTR_MAIL_INP_DOMAIN"] = "域名";
$MESS["INTR_MAIL_INP_PUBLIC_DOMAIN"] = "員工可以在此域中註冊郵箱";
$MESS["INTR_MAIL_INP_TOKEN"] = "令牌";
$MESS["INTR_MAIL_MANAGE"] = "配置員工郵箱";
