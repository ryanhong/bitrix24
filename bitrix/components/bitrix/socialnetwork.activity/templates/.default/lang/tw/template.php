<?php
$MESS["SONET_ACTIVITY_T_ALL"] = "全部";
$MESS["SONET_ACTIVITY_T_BLOG"] = "博客/報告";
$MESS["SONET_ACTIVITY_T_CALENDAR"] = "日曆";
$MESS["SONET_ACTIVITY_T_FILES"] = "文件";
$MESS["SONET_ACTIVITY_T_FORUM"] = "論壇/討論";
$MESS["SONET_ACTIVITY_T_GROUP_TITLE"] = "團隊名字＃";
$MESS["SONET_ACTIVITY_T_NO_UPDATES"] = "沒有更新。";
$MESS["SONET_ACTIVITY_T_PHOTO"] = "照片";
$MESS["SONET_ACTIVITY_T_SWITCH1"] = "隱藏";
$MESS["SONET_ACTIVITY_T_SWITCH2"] = "細節";
$MESS["SONET_ACTIVITY_T_SYSTEM"] = "系統";
$MESS["SONET_ACTIVITY_T_SYSTEM_FRIENDS"] = "朋友們";
$MESS["SONET_ACTIVITY_T_SYSTEM_GROUPS"] = "組";
$MESS["SONET_ACTIVITY_T_TASKS"] = "任務";
$MESS["SONET_ACTIVITY_T_USER_TITLE1"] = "用戶";
