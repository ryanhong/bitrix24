<?php
$MESS["STPC_ACTIV"] = "積極的：";
$MESS["STPC_ANY"] = "（任何）";
$MESS["STPC_APPLY"] = "申請";
$MESS["STPC_CANCEL"] = "取消";
$MESS["STPC_CEXP"] = "截止日期：";
$MESS["STPC_CNUM"] = "信用卡號碼：";
$MESS["STPC_CURRENCY"] = "允許付款的貨幣：";
$MESS["STPC_MAX_SUM"] = "最高允許付款：";
$MESS["STPC_MIN_SUM"] = "最低允許付款：";
$MESS["STPC_PAY_SYSTEM"] = "支付系統：";
$MESS["STPC_SAVE"] = "節省";
$MESS["STPC_SORT"] = "排序索引：";
$MESS["STPC_SUM_CURR"] = "付款貨幣：";
$MESS["STPC_TIMESTAMP"] = "最後修改日期：";
$MESS["STPC_TO_LIST"] = "返回目錄";
$MESS["STPC_TYPE"] = "卡的種類：";
