<?php
$MESS["DEALS_WITH_RESERVED_PRODUCT_SLIDER_HINT_MSGVER_1"] = "這些交易具有選定的產品和/或其SKUS保留。該列表僅顯示您可以訪問的交易。";
$MESS["DEALS_WITH_RESERVED_PRODUCT_SLIDER_TITLE"] = "包含保留產品的交易\“＃product_name＃\”";
