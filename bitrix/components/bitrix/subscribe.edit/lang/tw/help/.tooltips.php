<?php
$MESS["AJAX_MODE_TIP"] = "在組件中啟用AJAX。";
$MESS["AJAX_OPTION_HISTORY_TIP"] = "允許\“ Back \”和\“ Forward \”瀏覽器按鈕用於Ajax Transitions。";
$MESS["AJAX_OPTION_JUMP_TIP"] = "當Ajax過渡完成時，指定滾動到組件。";
$MESS["AJAX_OPTION_SHADOW_TIP"] = "指定在Ajax過渡上的陰影修改區域。";
$MESS["AJAX_OPTION_STYLE_TIP"] = "指定在AJAX過渡上下載和處理CSS組件樣式。";
$MESS["ALLOW_ANONYMOUS_TIP"] = "如果已檢查，匿名訪問者可以通過提供其電子郵件地址而無需註冊來訂閱新聞通訊。";
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>不緩存</i>：沒有執行緩存。";
$MESS["SET_TITLE_TIP"] = "檢查此選項，將頁面標題設置為<b>編輯訂閱參數</b>或<b>分別在編輯或添加訂閱地址時添加訂閱</b>。";
$MESS["SHOW_AUTH_LINKS_TIP"] = "如果要向匿名訂戶顯示授權或註冊表格，請檢查此選項。";
$MESS["SHOW_HIDDEN_TIP"] = "指定在訂閱編輯器中顯示所有活動的主題。";
