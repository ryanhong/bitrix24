<?php
$MESS["SUBSCR_ALLOW_ANONYMOUS"] = "允許匿名訂閱";
$MESS["SUBSCR_SHOW_AUTH_LINKS"] = "在匿名模式下顯示授權鏈接";
$MESS["SUBSCR_SHOW_HIDDEN"] = "顯示隱藏的訂閱類別";
