<?php
$MESS["SUBSCR_MODULE_NOT_INSTALLED"] = "通訊模塊當前不可用。";
$MESS["adm_auth_err"] = "您應授權管理您的訂閱。";
$MESS["adm_auth_err_rub"] = "您需要至少選擇一個訂閱類別。";
$MESS["adm_conf_mess"] = "謝謝您，訂閱確認。";
$MESS["adm_sent_mess"] = "確認代碼已發送到訂閱地址。";
$MESS["adm_unsubscr_mess"] = "訂閱標記為無效。不會將郵件發送到此地址。";
$MESS["adm_upd_mess"] = "訂閱設置已更新。";
$MESS["subscr_active_mess"] = "訂閱激活。";
$MESS["subscr_email_not_found"] = "找不到訂戶地址。";
$MESS["subscr_pass_mess"] = "在註冊時將更改密碼的信息發送到指定的地址。";
$MESS["subscr_send_pass_mess"] = "您已請求更改密碼的信息。";
$MESS["subscr_title_add"] = "添加訂閱地址";
$MESS["subscr_title_edit"] = "編輯訂閱參數";
$MESS["subscr_wrong_rubric"] = "指定了錯誤的訂閱類別。";
