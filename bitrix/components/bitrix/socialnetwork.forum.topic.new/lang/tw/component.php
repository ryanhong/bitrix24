<?php
$MESS["FL_FORUM_CHAIN"] = "論壇";
$MESS["FORUM_SONET_MODULE_NOT_AVAIBLE"] = "該論壇不適合該用戶。";
$MESS["F_ERRRO_FILE_NOT_UPLOAD"] = "禁止文件上傳。";
$MESS["F_ERR_SESS_FINISH"] = "您的會議已經過期。請重複操作。";
$MESS["F_ETITLE"] = "修改消息";
$MESS["F_FID_IS_EMPTY"] = "未指定社交網絡論壇。";
$MESS["F_FID_IS_LOST"] = "找不到社交網絡論壇。";
$MESS["F_MID_IS_LOST"] = "找不到帖子";
$MESS["F_MID_IS_LOST_IN_FORUM"] = "該帖子在指定的論壇中找不到。";
$MESS["F_NO_EPERMS"] = "您沒有足夠的權限來修改此消息";
$MESS["F_NO_MODULE"] = "論壇模塊未安裝";
$MESS["F_NO_NPERMS"] = "您沒有足夠的權限來在此論壇中創建新主題";
$MESS["F_NTITLE"] = "新話題";
$MESS["SONET_FORUM_LOG_TEMPLATE"] = "＃fure_name＃在＃標題＃中創建了一個主題。";
$MESS["SONET_IM_NEW_TOPIC"] = "添加了一個名為\“＃標題＃\”的新論壇主題in \“＃group_name＃\”。";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
