<?php
$MESS["F_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["F_DEFAULT_FID"] = "論壇ID";
$MESS["F_DEFAULT_MID"] = "消息ID";
$MESS["F_DEFAULT_PATH_TO_ICON"] = "通往圖標文件夾的路徑（根相對）";
$MESS["F_DEFAULT_PATH_TO_SMILE"] = "微笑文件夾的路徑（根相對）";
$MESS["F_LIST_TEMPLATE"] = "主題列表頁面";
$MESS["F_MESSAGE_TEMPLATE"] = "消息查看頁面";
$MESS["F_MESSAGE_TYPE"] = "消息動作";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "個人資料頁";
$MESS["F_SOCNET_GROUP_ID"] = "組ID";
$MESS["F_URL_TEMPLATES"] = "URL處理";
$MESS["F_USER_ID"] = "用戶身份";
