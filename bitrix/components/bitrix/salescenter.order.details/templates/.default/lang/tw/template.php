<?php
$MESS["SOD_COMMON_DISCOUNT"] = "您保存";
$MESS["SOD_COMMON_SUM_NEW"] = "項目總計";
$MESS["SOD_DELIVERY"] = "送貨";
$MESS["SOD_FREE"] = "自由的";
$MESS["SOD_SUB_PAYMENT_TITLE"] = "付款## account_number＃of＃date_order_create＃";
$MESS["SOD_SUB_PAYMENT_TITLE_SHORT"] = "付款## account_number＃";
$MESS["SOD_SUMMARY"] = "全部的";
$MESS["SOD_TAX"] = "稅率";
$MESS["SOD_TOTAL_WEIGHT"] = "總重量";
$MESS["SOD_TPL_SUMOF"] = "訂單金額";
$MESS["SPOD_DOCUMENT_ACTION_DOWNLOAD"] = "下載";
$MESS["SPOD_DOCUMENT_ACTION_OPEN"] = "打開";
$MESS["SPOD_DOCUMENT_ACTION_SHARE"] = "向前";
$MESS["SPOD_DOCUMENT_TITLE"] = "您的發票";
