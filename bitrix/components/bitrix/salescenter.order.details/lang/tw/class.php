<?php
$MESS["SOD_COMMON_DISCOUNT"] = "您保存";
$MESS["SOD_COMMON_SUM_NEW"] = "項目總計";
$MESS["SOD_DELIVERY"] = "送貨";
$MESS["SOD_FREE"] = "自由的";
$MESS["SOD_SUB_PAYMENT_TITLE"] = "付款## account_number＃，＃date_order_create＃";
$MESS["SOD_SUB_PAYMENT_TITLE_SHORT"] = "付款## account_number＃";
$MESS["SOD_SUMMARY"] = "全部的";
$MESS["SPOD_ACCESS_DENIED"] = "訪問受限制";
$MESS["SPOD_CATALOG_MODULE_NOT_INSTALL"] = "請登錄以查看訂單。";
$MESS["SPOD_DOCUMENT_ACTION_DOWNLOAD"] = "下載";
$MESS["SPOD_DOCUMENT_ACTION_OPEN"] = "打開";
$MESS["SPOD_DOCUMENT_ACTION_SHARE"] = "向前";
$MESS["SPOD_DOCUMENT_TITLE"] = "您的發票";
$MESS["SPOD_NO_ORDER"] = "找不到命令。";
$MESS["SPOD_SALE_MODULE_NOT_INSTALL"] = "沒有安裝電子商店模塊。";
$MESS["SPOD_SALE_TAX_INPRICE"] = "包括";
$MESS["SPOD_TITLE"] = "我的訂單## ID＃";
