<?php
$MESS["F_IN_FORUM"] = "在論壇上＃＃";
$MESS["F_SEPARATE"] = "主題和論壇之間的分離器";
$MESS["F_SHOW_COLUMNS"] = "顯示列";
$MESS["F_SHOW_COLUMNS_LAST_POST_DATE"] = "最近更新時間";
$MESS["F_SHOW_COLUMNS_POSTS"] = "答复";
$MESS["F_SHOW_COLUMNS_USER_START_NAME"] = "作者";
$MESS["F_SHOW_COLUMNS_VIEWS"] = "觀點";
$MESS["F_SHOW_NAV"] = "顯示導航控件";
$MESS["F_SHOW_NAV_BOTTOM"] = "底部";
$MESS["F_SHOW_NAV_TOP"] = "頂部";
$MESS["F_SHOW_SORTING"] = "顯示排序控件";
