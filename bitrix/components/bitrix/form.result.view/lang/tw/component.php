<?php
$MESS["FORM_ACCESS_DENIED"] = "您沒有足夠的權限來查看表格。";
$MESS["FORM_ACCESS_DENIED_FOR_FORM"] = "您沒有足夠的權限來查看表格。";
$MESS["FORM_ACCESS_DENIED_FOR_FORM_RESULTS"] = "您沒有足夠的權限來查看此結果。";
$MESS["FORM_ACCESS_DENIED_FOR_FORM_RESULTS_EDITING"] = "您沒有足夠的權限來編輯此結果。";
$MESS["FORM_ACCESS_DENIED_FOR_FORM_WRITE"] = "您沒有足夠的權限來填寫表格。";
$MESS["FORM_EMPTY_REQUIRED_FIELDS"] = "以下所需字段留為空白：";
$MESS["FORM_INCORRECT_DATE_FORMAT"] = "Field \“＃field_name＃\”的錯誤日期格式。";
$MESS["FORM_INCORRECT_FILE_TYPE"] = "Field \“＃field_name＃\”的錯誤文件類型（請選擇另一個文件）。";
$MESS["FORM_INCORRECT_FORM_ID"] = "不正確的表單ID。";
$MESS["FORM_NOT_FOUND"] = "找不到網絡形式。";
$MESS["FORM_PUBLIC_ICON_EDIT"] = "編輯Web形式參數";
