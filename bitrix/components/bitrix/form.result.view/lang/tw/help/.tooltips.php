<?php
$MESS["CHAIN_ITEM_LINK_TIP"] = "該字段指定導航鏈項目的鏈接。";
$MESS["CHAIN_ITEM_TEXT_TIP"] = "指定導航鏈項目的標題。例如：\“結果\”。";
$MESS["EDIT_URL_TIP"] = "指定結果編輯頁面的名稱。";
$MESS["RESULT_ID_TIP"] = "指定一個評估result_id變量值的表達式，其中包含Web表單結果ID。默認情況下，包含<nobr> <b> = {\ $ _請求[\“ result_id \”]} </b> </nobr>，通過_request通過result_id。";
$MESS["SEF_FOLDER_TIP"] = "在此處指定頁面處於活動狀態時將在地址欄中顯示的文件夾名稱。";
$MESS["SEF_MODE_TIP"] = "檢查此選項啟用SEF模式。";
$MESS["SEF_URL_TEMPLATES_view_TIP"] = "指定結果查看頁面URL的<i> display </i>格式。默認值是＃result_id＃/。";
$MESS["SHOW_ADDITIONAL_TIP"] = "選擇\“是\”將顯示其他字段（如果有）。";
$MESS["SHOW_ANSWER_VALUE_TIP"] = "選擇此處\“是\”以顯示答案旁邊的Web表單問題的答案_Value的值（如果為問題指定了此參數）。";
$MESS["SHOW_STATUS_TIP"] = "選擇\“是\”將在結果視圖頁面上顯示當前結果狀態。";
