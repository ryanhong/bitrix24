<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Tasks\UI;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var CBitrixComponent $component */

Loc::loadMessages(__FILE__);
$commentAndResult = ['COMMENT', 'COMMENT_EDIT', 'COMMENT_DEL', 'RESULT', 'RESULT_EDIT', 'RESULT_REMOVE'];
$messages = Bitrix\Forum\MessageTable::query()
->whereNotNull("CHECK_ITEM_ID")
->where("TOPIC_ID", $arResult['TASK_ID'])
->setSelect(['*'])
->fetchAll();

?>

<table id="task-log-table" class="task-log-table">
	<col class="task-log-date-column" />
	<col class="task-log-author-column" />
	<col class="task-log-where-column" />
	<col class="task-log-what-column" />

	<tr>
		<th class="task-log-date-column"><?=Loc::getMessage("TASKS_LOG_WHEN")?></th>
		<th class="task-log-author-column">回報內容</th>
		<th class="task-log-where-column">回報者</th>
		<th class="task-log-what-column">回報主題</th>
	</tr>
	<?php foreach($messages as $message):?>
		<tr>
			<td class="task-log-date-column"><?=$message['POST_DATE']?></td>
			<td class="task-log-date-column"><?=$message['POST_MESSAGE']?></td>
			<td class="task-log-what-column"><?=$message['AUTHOR_NAME']?></td>
		</tr>
	<?php endforeach;?>
</table>