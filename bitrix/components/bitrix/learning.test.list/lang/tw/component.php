<?php
$MESS["LEARNING_BAD_TEST_LIST"] = "本課程不提供認證。";
$MESS["LEARNING_COURSE_DENIED"] = "當然找不到或拒絕訪問。";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "未安裝電子學習模塊。";
$MESS["LEARNING_TESTS_LIST"] = "測試列表";
$MESS["LEARNING_TESTS_NAV"] = "測試";
$MESS["LEARNING_TEST_DENIED_PREVIOUS"] = "您必須成功傳遞＃test_link＃才能成功訪問此測試。";
