<?php
$MESS["ATOM_TIP"] = "如果要顯示ATOM .3的鏈接，請檢查此選項。";
$MESS["BLOG_URL_TIP"] = "指定通過博客ID傳遞的代碼。";
$MESS["BLOG_VAR_TIP"] = "在此處指定通過博客ID傳遞的變量的名稱。";
$MESS["GROUP_ID_TIP"] = "通過博客組ID的代碼。";
$MESS["GROUP_VAR_TIP"] = "通過博客組ID的變量的名稱。";
$MESS["MODE_TIP"] = "單個博客，博客組或所有博客都可以顯示RSS鏈接。";
$MESS["PAGE_VAR_TIP"] = "在此處指定博客頁面通過的變量的名稱。";
$MESS["PATH_TO_RSS_ALL_TIP"] = "所有博客的RSS頁面的路徑。示例：<Nobr> blog_rss_all.php？page = rss_all＆blog＆blog =＃blog type＆type =＃type＃＃。</nobr>";
$MESS["PATH_TO_RSS_TIP"] = "博客RSS頁面的路徑。示例：<Nobr> blog_rss.php？page = rss＆blog =＃blog＃＆type =＃type＃＃</nobr>";
$MESS["RSS1_TIP"] = "如果要顯示指向RSS .92的鏈接，請檢查此選項。";
$MESS["RSS2_TIP"] = "如果要顯示指向RSS 2.0的鏈接，請檢查此選項。";
