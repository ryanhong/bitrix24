<?php
$MESS["SONET_GROUP_ID"] = "組ID";
$MESS["SONET_GROUP_VAR"] = "組變量";
$MESS["SONET_ITEMS_COUNT"] = "列表中的項目計數";
$MESS["SONET_PAGE_VAR"] = "頁面變量";
$MESS["SONET_PATH_TO_GROUP"] = "小組頁面路徑模板";
$MESS["SONET_PATH_TO_GROUP_REQUESTS"] = "小組成員資格請求頁的路徑模板";
$MESS["SONET_PATH_TO_GROUP_REQUEST_SEARCH"] = "用戶邀請頁面路徑模板";
$MESS["SONET_PATH_TO_USER"] = "用戶配置文件路徑模板";
$MESS["SONET_SET_NAVCHAIN"] = "設置麵包屑";
$MESS["SONET_USER_VAR"] = "用戶變量";
$MESS["SONET_VARIABLE_ALIASES"] = "可變別名";
