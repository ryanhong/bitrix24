<?php
$MESS["SONET_C12_CANT_INVITE"] = "您無權批准該組的會員請求。";
$MESS["SONET_C12_NAV"] = "用戶";
$MESS["SONET_C12_NOT_SELECTED"] = "沒有選擇用戶。";
$MESS["SONET_C12_NO_PERMS"] = "您無權查看此組。";
$MESS["SONET_C12_TITLE"] = "小組成員資格請求";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_USER_NO_GROUP"] = "找不到小組。";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶。";
