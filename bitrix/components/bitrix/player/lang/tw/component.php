<?php
$MESS["ABOUT_LINK"] = "http://www.bitrixsoft.com/products/cms/features/player.php#tab-main-link";
$MESS["ABOUT_TEXT"] = "Bitrix媒體播放器";
$MESS["INCORRECT_FILE"] = "無效文件。";
$MESS["INCORRECT_PLAYLIST"] = "未找到指定的播放列表";
$MESS["INCORRECT_PLAYLIST_FORMAT"] = "錯誤的播放列表格式";
$MESS["JWPLAYER_DEPRECATED"] = "該播放器版本不在日期。請使用基於video.js的播放器。";
$MESS["NO_SUPPORTED_FILES"] = "沒有支持的文件。";
$MESS["PLAYER_PLAYLIST_ADD"] = "創建一個播放列表";
$MESS["PLAYER_PLAYLIST_EDIT"] = "編輯播放列表";
$MESS["PLAYLIST_AUDIO_AND_VIDEO_NOT_SUPPORTED"] = "播放器不支持同時視頻和音頻播放列表條目。";
$MESS["PLAYLIST_FILE_NOT_FOUND"] = "不支持";
$MESS["PLAYLIST_STREAMING_VIDEO_NOT_SUPPORTED"] = "玩家不支持流視頻播放列表條目。";
$MESS["PLAYLIST_VIMEO_NOT_SUPPORTED"] = "播放器不支持vimeo.com播放列表條目。";
$MESS["SWF_DENIED"] = "不支持的數據類型。";
$MESS["VIDEOJS_NOT_SUPPORTED_MESSAGE"] = "找不到合適的播放模式。";
