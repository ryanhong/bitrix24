<?php
$MESS["ENABLE_JAVASCRIPT"] = "JavaScript在您的瀏覽器禁用了";
$MESS["JS_CLICKTOPLAY"] = "點擊播放";
$MESS["JS_LINK"] = "下載...";
$MESS["JS_PLAYLISTERROR"] = "播放列表負載期間發生錯誤";
$MESS["PLAYER_FLASH_CHECK"] = "Adobe Flash Player可能被禁用或未安裝";
$MESS["PLAYER_FLASH_REQUIRED"] = "玩家無法播放視頻。 Adobe Flash Player很可能被禁用或未安裝。";
$MESS["PLAYER_LOADING"] = "加載播放器";
