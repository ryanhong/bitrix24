<?php
$MESS["SEC_CODES"] = "恢復代碼";
$MESS["SEC_GENERATE"] = "生成新代碼";
$MESS["SEC_INFO"] = "複製您可能需要的恢復代碼，如果您丟失了移動設備或由於任何其他原因無法通過該應用程序獲得代碼。在您的錢包或錢包裡說，請放手。每個代碼只能使用一次。";
$MESS["SEC_NEW_CODES"] = "恢復代碼的縮寫？<br/>
創建一些新的。 <br/> <br/>
創建新的恢復代碼無效<br/>先前生成的代碼。";
$MESS["SEC_PRINT"] = "列印";
$MESS["SEC_SAVE"] = "保存到文本文件";
$MESS["SEC_SHORT_NOTICE"] = "*只能使用一次代碼。";
