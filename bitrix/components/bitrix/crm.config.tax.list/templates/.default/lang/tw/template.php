<?php
$MESS["CRM_ALL"] = "全部的";
$MESS["CRM_TAX_DELETE"] = "刪除稅";
$MESS["CRM_TAX_DELETE_CONFIRM"] = "您確定要刪除“％s”？";
$MESS["CRM_TAX_DELETE_TITLE"] = "刪除此稅";
$MESS["CRM_TAX_EDIT"] = "編輯稅";
$MESS["CRM_TAX_EDIT_TITLE"] = "打開此稅用於編輯";
$MESS["CRM_TAX_LOCATIONS"] = "位置";
$MESS["CRM_TAX_LOCATIONS_CONTENT"] = "在創建稅收之前，請創建或導入位置。";
$MESS["CRM_TAX_LOCATIONS_CREATE"] = "創造";
$MESS["CRM_TAX_LOCATIONS_IMPORT"] = "進口";
$MESS["CRM_TAX_LOCATIONS_REDIRECT"] = "去";
$MESS["CRM_TAX_SHOW"] = "查看稅";
$MESS["CRM_TAX_SHOW_TITLE"] = "查看此稅";
