<?php
$MESS["SENDER_LETTER_LIST_COMP_TITLE"] = "銷售提升";
$MESS["SENDER_LETTER_LIST_COMP_UI_COLUMN_ACTIONS"] = "動作";
$MESS["SENDER_LETTER_LIST_COMP_UI_COLUMN_CAMPAIGN_ID"] = "活動";
$MESS["SENDER_LETTER_LIST_COMP_UI_COLUMN_DATE_INSERT"] = "日期";
$MESS["SENDER_LETTER_LIST_COMP_UI_COLUMN_DATE_INSERT2"] = "創建於";
$MESS["SENDER_LETTER_LIST_COMP_UI_COLUMN_DATE_SENT"] = "發送完成";
$MESS["SENDER_LETTER_LIST_COMP_UI_COLUMN_MESSAGE_CODE"] = "類型";
$MESS["SENDER_LETTER_LIST_COMP_UI_COLUMN_REITERATE"] = "週期性新聞通訊";
$MESS["SENDER_LETTER_LIST_COMP_UI_COLUMN_REITERATE_NO"] = "不";
$MESS["SENDER_LETTER_LIST_COMP_UI_COLUMN_REITERATE_YES"] = "是的";
$MESS["SENDER_LETTER_LIST_COMP_UI_COLUMN_STATE"] = "地位";
$MESS["SENDER_LETTER_LIST_COMP_UI_COLUMN_STATUS"] = "地位";
$MESS["SENDER_LETTER_LIST_COMP_UI_COLUMN_TITLE"] = "姓名";
$MESS["SENDER_LETTER_LIST_COMP_UI_COLUMN_USER"] = "由...製作";
$MESS["SENDER_LETTER_LIST_COMP_UI_PRESET_ALL"] = "全部";
$MESS["SENDER_LETTER_LIST_COMP_UI_PRESET_FINISHED"] = "完全的";
$MESS["SENDER_LETTER_LIST_COMP_UI_PRESET_MY"] = "我的";
$MESS["SENDER_LETTER_LIST_COMP_UI_PRESET_WORKING"] = "進行中";
$MESS["SENDER_LETTER_LIST_COMP_UI_STATUS_N"] = "準備好出發";
$MESS["SENDER_LETTER_LIST_COMP_UI_STATUS_P"] = "暫停";
$MESS["SENDER_LETTER_LIST_COMP_UI_STATUS_R"] = "準備好出發";
$MESS["SENDER_LETTER_LIST_COMP_UI_STATUS_S_RC_DEAL"] = "創建交易";
$MESS["SENDER_LETTER_LIST_COMP_UI_STATUS_S_RC_LEAD"] = "創建潛在客戶";
$MESS["SENDER_LETTER_LIST_COMP_UI_STATUS_W"] = "進行中";
$MESS["SENDER_LETTER_LIST_COMP_UI_STATUS_Y"] = "完全的";
