<?php
$MESS["SONET_C241_T_ACTIVITY"] = "上次訪問";
$MESS["SONET_C241_T_ADD_FR"] = "加為好友";
$MESS["SONET_C241_T_ANY"] = "任何";
$MESS["SONET_C241_T_BIGICON"] = "大照片";
$MESS["SONET_C241_T_DO_CANCEL"] = "＆nbsp;＆nbsp; cancel＆nbsp;＆nbsp; nbsp;";
$MESS["SONET_C241_T_DO_SEARCH"] = "＆nbsp;＆nbsp; search＆nbsp;＆nbsp;";
$MESS["SONET_C241_T_FIO"] = "全名";
$MESS["SONET_C241_T_ICON"] = "小照片";
$MESS["SONET_C241_T_LIST"] = "列表";
$MESS["SONET_C241_T_MEMBERS"] = "成員";
$MESS["SONET_C241_T_NOT_FILTERED"] = "未設置搜索條件";
$MESS["SONET_C241_T_NOT_FOUND"] = "找不到用戶。";
$MESS["SONET_C241_T_ONLINE"] = "在線的";
$MESS["SONET_C241_T_ORDER_DATE"] = "按日期排序";
$MESS["SONET_C241_T_ORDER_REL"] = "按相關性排序";
$MESS["SONET_C241_T_SEARCH"] = "搜尋";
$MESS["SONET_C241_T_SEARCH_TITLE"] = "用戶搜索";
$MESS["SONET_C241_T_SHOW_LIKE"] = "顯示為";
$MESS["SONET_C241_T_SUBJECT"] = "主題";
$MESS["SONET_C241_T_TPL_FILTER_ADV"] = "高級搜索";
$MESS["SONET_C241_T_TPL_FILTER_SIMPLE"] = "搜尋";
$MESS["SONET_C241_T_WRITE"] = "發信息";
