<?php
$MESS["IBLOCK_DEFAULT"] = "默認畫廊";
$MESS["IBLOCK_GALLERY_RECALC"] = "重新計算信息";
$MESS["IBLOCK_GALLERY_SIZE"] = "上傳照片的大小";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "信息塊未安裝";
$MESS["IBSEC_A_DELERR_REFERERS"] = "可以有參考對象。";
$MESS["P_BAD_PERMISSION"] = "編輯畫廊的權限不足";
$MESS["P_BAD_PERMISSION_TO_CREATE"] = "創建畫廊的權限不足";
$MESS["P_BAD_PERMISSION_TO_CREATE_ONE"] = "編輯另一個畫廊的權限不足";
$MESS["P_DENIED_ACCESS"] = "拒絕訪問。";
$MESS["P_ERROR_CODE_BAD"] = "畫廊代碼包含無效的符號。";
$MESS["P_ERROR_CODE_EXIST"] = "指定的畫廊代碼已經存在。";
$MESS["P_GALLERY_CREATE"] = "創建畫廊";
$MESS["P_GALLERY_EDIT"] = "編輯畫廊";
$MESS["P_GALLERY_NOT_FOUND"] = "沒有找到畫廊";
$MESS["P_MODULE_IS_NOT_INSTALLED"] = "未安裝照片庫2.0模塊。";
$MESS["P_USER_UNKNOWN"] = "未知";
