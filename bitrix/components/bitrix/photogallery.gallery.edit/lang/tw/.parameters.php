<?php
$MESS["IBLOCK_IBLOCK"] = "信息塊";
$MESS["IBLOCK_TYPE"] = "信息塊類型";
$MESS["P_GALLERIES_URL"] = "用戶畫廊";
$MESS["P_GALLERY_AVATAR_SIZE"] = "畫廊頭像尺寸（PX）";
$MESS["P_GALLERY_EDIT_URL"] = "編輯畫廊";
$MESS["P_GALLERY_GROUPS"] = "成員可以創建畫廊的用戶組";
$MESS["P_GALLERY_SIZE"] = "畫廊尺寸（MB）";
$MESS["P_GALLERY_URL"] = "查看畫廊";
$MESS["P_INDEX_URL"] = "索引頁";
$MESS["P_ONLY_ONE_GALLERY"] = "註冊用戶只能創建一個畫廊";
$MESS["P_USER_ALIAS"] = "畫廊代碼";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "顯示此組件的面板按鈕";
