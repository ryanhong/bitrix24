<?php
$MESS["BPWC_WICT_CREATE"] = "創造";
$MESS["BPWC_WICT_DELETE"] = "刪除";
$MESS["BPWC_WICT_DELETE_PROMT"] = "與此業務流程相關的所有信息將被刪除！你想繼續嗎？";
$MESS["BPWC_WICT_EDIT"] = "編輯";
$MESS["BPWC_WICT_EMPTY"] = "沒有業務流程類型可用。";
$MESS["BPWC_WICT_NEW_BP"] = "新的業務流程";
