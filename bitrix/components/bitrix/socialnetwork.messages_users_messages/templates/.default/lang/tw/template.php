<?php
$MESS["SONET_C31_T_ACTION"] = "動作";
$MESS["SONET_C31_T_ACT_DEL"] = "刪除";
$MESS["SONET_C31_T_ACT_READ"] = "標記為已讀";
$MESS["SONET_C31_T_BAN"] = "禁止";
$MESS["SONET_C31_T_CHAT"] = "與用戶聊天";
$MESS["SONET_C31_T_DO_DEL"] = "刪除";
$MESS["SONET_C31_T_DO_DEL_ALL"] = "刪除所有";
$MESS["SONET_C31_T_DO_DEL_ALL_CONFIRM"] = "這將刪除所有用戶通信。繼續？";
$MESS["SONET_C31_T_DO_READ"] = "標記為已讀";
$MESS["SONET_C31_T_EMPTY"] = "沒有消息。<br>此窗格顯示了消息傳遞歷史記錄。";
$MESS["SONET_C31_T_MESSAGE"] = "訊息";
$MESS["SONET_C31_T_ME_LABEL"] = "我";
$MESS["SONET_C31_T_ONLINE"] = "在線的";
$MESS["SONET_C31_T_PROFILE"] = "用戶資料";
$MESS["SONET_C31_T_SELECT_ALL"] = "檢查全部 /取消選中";
$MESS["SONET_C31_T_WRITE_MESSAGE"] = "發信息";
