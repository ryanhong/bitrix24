<?php
$MESS["SENDER_RC_COMP_LETTER_EDIT_TITLE_ADD"] = "新任務";
$MESS["SENDER_RC_COMP_LETTER_EDIT_TITLE_EDIT"] = "編輯任務";
$MESS["SENDER_RC_COMP_LETTER_EDIT_TITLE_TEMPLATES"] = "預安裝的模板";
$MESS["SENDER_RC_SEGMENT_SELECTOR_INCLUDE_EDIT_TITLE"] = "選擇細分市場";
$MESS["SENDER_RC_SEGMENT_SELECTOR_INCLUDE_VIEW_TITLE"] = "選定的細分市場";
$MESS["SENDER_RC_SEGMENT_SELECTOR_RECIPIENT_COUNT"] = "當前選擇";
$MESS["SENDER_RC_SEGMENT_SELECTOR_RECIPIENT_COUNT_EXACT_HINT"] = "您已經指定了公司和聯繫人的數量。
<br>僅在執行後才能知道確切的潛在客戶和交易數量。
<br>一旦Lanuched，將計算積極的線索和交易的數量，
<br>因此，在完成後將知道實際的潛在客戶和交易數量。";
$MESS["SENDER_RC_SEGMENT_SELECTOR_RECIPIENT_COUNT_HINT"] = "公司和聯繫人的數量大致。
<br>僅在完成工作完成後才知道確切的潛在客戶和交易數量。
<br>因為該系統在開始作業時會檢查有效的潛在客戶和交易。";
