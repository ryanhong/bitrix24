<?php
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
$MESS["CRM_TIMELINE_DOCUMENT_DELETED"] = "刪除文檔";
$MESS["CRM_WAIT_ACTION_INVALID_BEFORE_PARAMS"] = "等待結束日期需要超過當前日期。您輸入的日期可能是過去或將來太遠。";
$MESS["CRM_WAIT_ACTION_INVALID_REQUEST_DATA"] = "請求包含不正確的參數。";
$MESS["CRM_WAIT_ACTION_ITEM_NOT_FOUND"] = "找不到項目。";
