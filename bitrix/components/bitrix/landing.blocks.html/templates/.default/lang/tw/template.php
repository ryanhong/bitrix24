<?php
$MESS["LANDING_TPL_NOT_ENABLE_LINK"] = "升級到商業計劃";
$MESS["LANDING_TPL_NOT_ENABLE_TEXT"] = "<p>編輯塊並在預覽模式下預覽更改。可以在商業計劃上獲得自定義HTML代碼。</p> <p>通過升級到主要計劃之一，享受更多網站設計師功能。</p>";
$MESS["LANDING_TPL_NOT_ENABLE_TITLE"] = "計劃限制";
$MESS["LANDING_TPL_NOT_IN_PREVIEW_MODE"] = "直到頁面發布後，HTML代碼才會渲染。";
