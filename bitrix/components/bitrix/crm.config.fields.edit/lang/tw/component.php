<?php
$MESS["CC_BLFE_BAD_FIELD_NAME"] = "需要字段名稱。";
$MESS["CC_BLFE_BAD_FIELD_NAME_LANG"] = "請在＃lang_name＃中指定字段名稱。";
$MESS["CC_BLFE_CHAIN_FIELDS"] = "字段映射";
$MESS["CC_BLFE_CHAIN_LIST_EDIT"] = "網格設置";
$MESS["CC_BLFE_ERR_IBLOCK_ELEMENT_BAD_IBLOCK_ID"] = "請選擇一個用於綁定的信息塊。";
$MESS["CC_BLFE_FIELD_NAME_DEFAULT"] = "新領域";
$MESS["CC_BLFE_TITLE_EDIT"] = "字段參數：＃名稱＃";
$MESS["CC_BLFE_TITLE_NEW"] = "新領域";
$MESS["CC_BLFE_WRONG_LINK_IBLOCK"] = "為\“ bind \”類型屬性指定了錯誤的列表。";
$MESS["CRM_FIELDS_EDIT_NAME_DEFAULT"] = "新領域";
$MESS["CRM_FIELDS_EDIT_TITLE_EDIT"] = "字段參數：＃名稱＃";
$MESS["CRM_FIELDS_EDIT_WRONG_FIELD"] = "字段ID無效。";
$MESS["CRM_FIELDS_ENTITY_LIST"] = "類型";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
