<?php
$MESS["ACTIVE"] = "積極的：";
$MESS["EMAIL"] = "電子郵件：";
$MESS["LAST_LOGIN"] = "上次授權：";
$MESS["LAST_NAME"] = "姓：";
$MESS["LAST_UPDATE"] = "最後更新：";
$MESS["LOGIN"] = "登錄（最小3個字符）：";
$MESS["MAIN_PROFILE_TITLE"] = "稱呼：";
$MESS["MAIN_RESET"] = "取消";
$MESS["NAME"] = "姓名：";
$MESS["NEW_PASSWORD"] = "新密碼（最小6個字符）：";
$MESS["NEW_PASSWORD_CONFIRM"] = "確認新密碼：";
$MESS["NEW_PASSWORD_REQ"] = "新密碼：";
$MESS["PROFILE_DATA_SAVED"] = "所有更改均保存";
$MESS["RESET"] = "重置";
$MESS["SAVE"] = "保存更改";
$MESS["SECOND_NAME"] = "中間名字：";
