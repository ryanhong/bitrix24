<?php
$MESS["CALENDAR_UPDATE_EVENT_WITH_LOCATION"] = "轉換事件";
$MESS["CALENDAR_UPDATE_STRUCTURE_TITLE"] = "優化結構";
$MESS["EC_CALENDAR_INDEX"] = "索引日曆事件";
$MESS["EC_CALENDAR_NOT_PERMISSIONS_TO_VIEW_GRID_CONTENT"] = "請聯繫您的Bitrix24管理員以獲取幫助";
$MESS["EC_CALENDAR_NOT_PERMISSIONS_TO_VIEW_GRID_TITLE"] = "訪問日曆受到您的Bitrix24管理員的限制";
$MESS["EC_CALENDAR_SPOTLIGHT_LIST"] = "為了方便起見，在各種日曆視圖之間切換。嘗試為需要所有會議和約會的列表視圖的繁忙專業人員創建的新時間表視圖。";
$MESS["EC_CALENDAR_SPOTLIGHT_SYNC"] = "將日曆與其他服務和移動設備同步。同步兩種方式都起作用。";
$MESS["EC_GROUP_ID_NOT_FOUND"] = "由於未指定組ID，因此無法顯示組日曆。";
$MESS["EC_GROUP_NOT_FOUND"] = "找不到小組。";
$MESS["EC_IBLOCK_ACCESS_DENIED"] = "拒絕訪問";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "未安裝\“信息塊\”模塊。";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "未安裝\“ Intranet Portal \”模塊。";
$MESS["EC_USER_ID_NOT_FOUND"] = "由於未指定用戶ID，因此無法顯示用戶日曆。";
$MESS["EC_USER_NOT_FOUND"] = "找不到用戶。";
