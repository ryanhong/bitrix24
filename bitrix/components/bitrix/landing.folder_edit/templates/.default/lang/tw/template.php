<?php
$MESS["LANDING_TPL_BUTTON_CANCEL"] = "取消";
$MESS["LANDING_TPL_BUTTON_SAVE"] = "節省";
$MESS["LANDING_TPL_FIELD_CODE"] = "文件夾地址";
$MESS["LANDING_TPL_FIELD_INDEX_ID"] = "索引頁";
$MESS["LANDING_TPL_FIELD_METAOG_DESCRIPTION"] = "描述";
$MESS["LANDING_TPL_FIELD_METAOG_TITLE"] = "姓名";
$MESS["LANDING_TPL_FIELD_PREVIEW"] = "社交媒體預覽";
$MESS["LANDING_TPL_FOLDER_ADD_PAGE"] = "創建頁面";
$MESS["LANDING_TPL_FOLDER_IS_EMPTY"] = "（文件夾為空）";
$MESS["LANDING_TPL_FOLDER_SELECT_PAGE"] = "選擇頁面";
$MESS["LANDING_TPL_TITLE"] = "文件夾設置";
