<?php
$MESS["SUP_DESC_NO"] = "不";
$MESS["SUP_DESC_YES"] = "是的";
$MESS["SUP_EDIT_MESSAGES_PER_PAGE"] = "每頁消息";
$MESS["SUP_LIST_TICKETS_PER_PAGE"] = "每頁門票";
$MESS["SUP_MESSAGE_MAX_LENGTH"] = "最大非斷裂弦長";
$MESS["SUP_MESSAGE_MAX_LENGTH_TIP"] = "未包裝文本字符串的最大長度不包含空格或標點器；將包裝更長的線條。";
$MESS["SUP_MESSAGE_SORT_ORDER"] = "門票消息排序訂單";
$MESS["SUP_SET_PAGE_TITLE"] = "設置頁面標題";
$MESS["SUP_SHOW_COUPON_FIELD"] = "顯示優惠券輸入字段";
$MESS["SUP_SHOW_USER_FIELD"] = "顯示自定義字段";
$MESS["SUP_SORT_ASC"] = "上升";
$MESS["SUP_SORT_DESC"] = "下降";
$MESS["SUP_SORT_FIELD_ID"] = "ID";
$MESS["SUP_SORT_FIELD_NUMBER"] = "消息號";
$MESS["SUP_TICKET_EDIT_DESC"] = "編輯機票頁面";
$MESS["SUP_TICKET_ID_DESC"] = "票證ID";
$MESS["SUP_TICKET_LIST_DESC"] = "門票清單";
