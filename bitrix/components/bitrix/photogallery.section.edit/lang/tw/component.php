<?php
$MESS["IBLOCK_DATE"] = "日期";
$MESS["IBLOCK_EDIT"] = "編輯專輯";
$MESS["IBLOCK_EDIT_TITLE"] = " （編輯）";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "信息塊模塊未安裝。";
$MESS["IBLOCK_NEW"] = "新唱片";
$MESS["IBLOCK_PASSWORD"] = "專輯密碼";
$MESS["IBLOCK_WRONG_SESSION"] = "您的會議已經過期。請再次保存您的文檔。";
$MESS["IBSEC_A_DELERR_REFERERS"] = "可以有參考對象。";
$MESS["P_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["P_DELETE_ERROR"] = "刪除照片的錯誤。";
$MESS["P_DENIED_ACCESS"] = "拒絕訪問。";
$MESS["P_GALLERY_EMPTY"] = "畫廊代碼未指定";
$MESS["P_MODULE_IS_NOT_INSTALLED"] = "未安裝模塊照片庫2.0。";
$MESS["P_SECTION_EMPTY_TO_MOVE"] = "目標專輯未指定。";
$MESS["P_SECTION_IS_NOT_IN_GALLERY"] = "專輯不在指定的畫廊中。";
$MESS["P_SECTION_THIS_TO_MOVE"] = "專輯不正確。";
