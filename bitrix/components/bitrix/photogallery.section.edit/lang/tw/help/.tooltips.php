<?php
$MESS["DATE_TIME_FORMAT_TIP"] = "在此處指定日期顯示格式。";
$MESS["DISPLAY_PANEL_TIP"] = "如果已檢查，編輯器按鈕將在“控制面板工具欄”和“組件工具箱”區域中顯示在網站編輯模式中。";
$MESS["GROUP_PERMISSIONS_TIP"] = "在此處指定其成員可以查看專輯的用戶組。";
$MESS["IBLOCK_ID_TIP"] = "在此處指定將存儲照片的信息塊。另外，您可以選擇<b>（其他） - > </b>並指定旁邊字段中的信息塊ID。";
$MESS["IBLOCK_TYPE_TIP"] = "在此處選擇現有的信息塊類型之一。單擊<b> <i> ok </i> </b>加載所選類型的信息塊。";
$MESS["INDEX_URL_TIP"] = "指定專輯頁的地址。";
$MESS["SECTION_CODE_TIP"] = "在此處指定（專輯）助記符代碼。";
$MESS["SECTION_ID_TIP"] = "該字段包含一個評估（專輯）ID節的表達式。";
$MESS["SECTION_URL_TIP"] = "指定專輯查看頁面的地址。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將頁面標題設置為專輯名稱或\“新專輯\”。";
$MESS["USE_PERMISSIONS_TIP"] = "指定限制對專輯的訪問。";
