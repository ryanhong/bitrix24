<?php
$MESS["INTR_MAIL_SUCCESS_MAIL_GO"] = "查看電子郵件";
$MESS["INTR_MAIL_SUCCESS_MAIL_HOME"] = "返回配置";
$MESS["INTR_MAIL_SUCCESS_MESSAGE_TEXT"] = "現在，您可以開始瀏覽收件箱或返回電子郵件配置頁面。";
$MESS["INTR_MAIL_SUCCESS_MESSAGE_TITLE"] = "恭喜！您的郵箱已成功附加。";
