<?php
$MESS["SET_TITLE_TIP"] = "檢查此選項將將頁面標題設置為<b>學生的個人資料</b>。";
$MESS["TRANSCRIPT_DETAIL_TEMPLATE_TIP"] = "包含考試結果的頁面的路徑。";
$MESS["USER_PROPERTY_TIP"] = "在此處選擇將在用戶配置文件中顯示的其他屬性。";
