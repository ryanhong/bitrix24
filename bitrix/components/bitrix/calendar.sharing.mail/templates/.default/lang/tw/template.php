<?php
$MESS["CALENDAR_SHARING_MAIL_ADD_TO_CALENDAR"] = "添加到日曆";
$MESS["CALENDAR_SHARING_MAIL_CANCEL_EVENT"] = "取消會議";
$MESS["CALENDAR_SHARING_MAIL_CREATE_NEW_MEETING"] = "創建新會議";
$MESS["CALENDAR_SHARING_MAIL_FOOTER_LOGO_FREE_SITES_AND_CRM"] = "由<b> Bitrix24 </b>  - 免費<＃tag＃style = \“＃style＃\” href = \“＃href＃\”>任務和CRM </＃tag＃>";
$MESS["CALENDAR_SHARING_MAIL_FOOTER_REPORT"] = "如果您收到了時間插槽的鏈接，但沒想到，請<a style = \"#style# \" href= \"#href# \ \">提交報告</a>。";
$MESS["CALENDAR_SHARING_MAIL_MEETING_HAS_MORE_USERS"] = "其他與會者：";
$MESS["CALENDAR_SHARING_MAIL_OPEN_VIDEOCONFERENCE"] = "視訊會議";
$MESS["CALENDAR_SHARING_MAIL_WHO_CANCELLED"] = "取消：＃名稱＃";
