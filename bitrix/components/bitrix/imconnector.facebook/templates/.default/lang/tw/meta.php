<?php
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_ADDITIONAL_DESCRIPTION_META_RU"] = "您將必須創建一個公共Facebook*頁面或連接已經擁有的頁面。只有頁面管理員可以將其連接到Bitrix24。";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_SUBTITLE_META_RU"] = "將您的公共Facebook*頁面連接到您的bitrix24，以接收客戶在Facebook上的查詢*。更快地回复並改善轉換。";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_TITLE_META_RU"] = "回复您的Facebook*客戶的問題";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_RESTRICTIONS_META_RU_MSGVER_1"] = "*所有者Meta Platforms，Inc。在俄羅斯聯邦禁止使用。";
