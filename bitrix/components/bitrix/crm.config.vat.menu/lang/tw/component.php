<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
$MESS["CRM_VAT_ADD"] = "添加增值稅率";
$MESS["CRM_VAT_ADD_TITLE"] = "創建一個新的增值稅率";
$MESS["CRM_VAT_DELETE"] = "刪除稅率";
$MESS["CRM_VAT_DELETE_DLG_BTNTITLE"] = "刪除增值稅率";
$MESS["CRM_VAT_DELETE_DLG_MESSAGE"] = "您確定要刪除此費率嗎？";
$MESS["CRM_VAT_DELETE_DLG_TITLE"] = "刪除增值稅率";
$MESS["CRM_VAT_DELETE_TITLE"] = "刪除增值稅率";
$MESS["CRM_VAT_EDIT"] = "編輯稅率";
$MESS["CRM_VAT_EDIT_TITLE"] = "開放式增值稅率用於編輯";
$MESS["CRM_VAT_LIST"] = "增值稅率";
$MESS["CRM_VAT_LIST_TITLE"] = "查看所有增值稅率";
$MESS["CRM_VAT_SETTINGS"] = "設定";
$MESS["CRM_VAT_SETTINGS_TITLE"] = "配置稅收系統";
$MESS["CRM_VAT_SHOW"] = "查看速率";
$MESS["CRM_VAT_SHOW_TITLE"] = "查看增值稅率";
