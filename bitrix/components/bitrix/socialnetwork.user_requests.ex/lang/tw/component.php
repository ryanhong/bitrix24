<?php
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_URE_FRIEND_CONFIRM"] = "＃用戶＃的友誼請求已得到確認。";
$MESS["SONET_URE_FRIEND_REJECT"] = "＃用戶＃的友誼請求已被取消。";
$MESS["SONET_URE_GROUP_CONFIRM"] = "已確認加入\“＃組＃\”。";
$MESS["SONET_URE_GROUP_REJECT"] = "邀請加入\“＃組＃\”已被拒絕。";
$MESS["SONET_URE_NOT_SELECTED"] = "未選擇記錄。";
$MESS["SONET_URE_NO_PERMS"] = "您無權執行此操作。";
$MESS["SONET_URE_NO_USER"] = "找不到用戶。";
$MESS["SONET_URE_PAGE_TITLE"] = "用戶請求";
