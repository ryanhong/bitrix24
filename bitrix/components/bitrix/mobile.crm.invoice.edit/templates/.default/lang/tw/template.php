<?php
$MESS["M_CRM_INVOICE_CONVERSION_NOTIFY"] = "必需的字段";
$MESS["M_CRM_INVOICE_EDIT_CANCEL_BTN"] = "取消";
$MESS["M_CRM_INVOICE_EDIT_CONTINUE_BTN"] = "繼續";
$MESS["M_CRM_INVOICE_EDIT_CONVERT_TITLE"] = "發票";
$MESS["M_CRM_INVOICE_EDIT_CREATE_TITLE"] = "創建發票";
$MESS["M_CRM_INVOICE_EDIT_EDIT_TITLE"] = "編輯";
$MESS["M_CRM_INVOICE_EDIT_SAVE_BTN"] = "節省";
$MESS["M_CRM_INVOICE_EDIT_VIEW_TITLE"] = "查看發票";
$MESS["M_CRM_INVOICE_MENU_CREATE_ON_BASE"] = "使用源創建";
$MESS["M_CRM_INVOICE_MENU_DELETE"] = "刪除";
$MESS["M_CRM_INVOICE_MENU_EDIT"] = "編輯";
$MESS["M_CRM_INVOICE_MENU_HISTORY"] = "歷史";
$MESS["M_CRM_INVOICE_MENU_SEND_EMAIL"] = "發電子郵件";
$MESS["M_CRM_INVOICE_SELECT"] = "選擇";
$MESS["M_DETAIL_DOWN_TEXT"] = "釋放以刷新...";
$MESS["M_DETAIL_LOAD_TEXT"] = "更新...";
$MESS["M_DETAIL_PULL_TEXT"] = "下拉以刷新...";
