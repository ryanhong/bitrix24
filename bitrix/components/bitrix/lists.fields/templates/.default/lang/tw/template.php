<?php
$MESS["CT_BLF_DELETE_POPUP_ACCEPT_BUTTON"] = "刪除";
$MESS["CT_BLF_DELETE_POPUP_CANCEL_BUTTON"] = "取消";
$MESS["CT_BLF_DELETE_POPUP_TITLE"] = "確認刪除";
$MESS["CT_BLF_LIST_CODE"] = "代碼";
$MESS["CT_BLF_LIST_DEFAULT_VALUE"] = "預設值";
$MESS["CT_BLF_LIST_IS_REQUIRED"] = "必需的";
$MESS["CT_BLF_LIST_MULTIPLE"] = "多種的";
$MESS["CT_BLF_LIST_NAME"] = "姓名";
$MESS["CT_BLF_LIST_SORT"] = "種類";
$MESS["CT_BLF_LIST_TYPE"] = "類型";
$MESS["CT_BLF_TOOLBAR_ADD"] = "添加字段";
$MESS["CT_BLF_TOOLBAR_ADD_TITLE"] = "添加新字段";
$MESS["CT_BLF_TOOLBAR_ELEMENTS_TITLE"] = "查看和編輯列表元素";
$MESS["CT_BLF_TOOLBAR_ELEMENT_DELETE_WARNING"] = "您確定要刪除該字段嗎？";
$MESS["CT_BLF_TOOLBAR_LIST_EDIT"] = "網格設置";
$MESS["CT_BLF_TOOLBAR_LIST_EDIT_PROCESS"] = "配置工作流程";
$MESS["CT_BLF_TOOLBAR_LIST_TITLE"] = "返回列表設置";
$MESS["CT_BLF_TOOLBAR_LIST_TITLE_PROCESS"] = "返回工作流程配置";
$MESS["CT_BLF_TOOLBAR_RETURN_LIST_ELEMENT"] = "返回目錄";
