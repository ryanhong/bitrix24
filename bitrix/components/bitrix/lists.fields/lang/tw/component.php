<?php
$MESS["CC_BLF_ACCESS_DENIED"] = "拒絕訪問列表設置。";
$MESS["CC_BLF_ACTION_MENU_DELETE"] = "刪除";
$MESS["CC_BLF_ACTION_MENU_DELETE_CONF"] = "您確定要刪除此字段嗎？";
$MESS["CC_BLF_ACTION_MENU_EDIT"] = "編輯";
$MESS["CC_BLF_CHAIN_EDIT"] = "網格設置";
$MESS["CC_BLF_CHAIN_FIELDS"] = "列表字段";
$MESS["CC_BLF_LISTS_FOR_SONET_GROUP_DISABLED"] = "該組禁用列表。";
$MESS["CC_BLF_MODULE_NOT_INSTALLED"] = "未安裝通用列表模塊。";
$MESS["CC_BLF_TITLE_EDIT"] = "＃名稱＃列表字段的設置";
$MESS["CC_BLF_TITLE_EDIT_PROCESS"] = "編輯工作流的字段：＃名稱＃";
$MESS["CC_BLF_TITLE_FIELDS"] = "字段配置：＃名稱＃";
$MESS["CC_BLF_UNKNOWN_ERROR"] = "未知錯誤。";
$MESS["CC_BLF_WRONG_IBLOCK"] = "指定的列表不正確。";
$MESS["CC_BLF_WRONG_IBLOCK_TYPE"] = "不正確的信息塊類型。";
