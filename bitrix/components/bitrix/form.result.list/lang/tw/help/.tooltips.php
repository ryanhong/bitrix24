<?php
$MESS["CHAIN_ITEM_LINK_TIP"] = "該字段指定導航鏈項目的鏈接。";
$MESS["CHAIN_ITEM_TEXT_TIP"] = "指定導航鏈項目標題。例如：\“結果\”。";
$MESS["EDIT_URL_TIP"] = "指定結果編輯頁面的路徑。如果頁面在另一個文件夾中，則該路徑相對於站點根。否則，如果該頁面存儲在當前文件夾中，則只能指定文件名（例如默認<b> result_edit.php </b>）。";
$MESS["NEW_URL_TIP"] = "指定結果創建頁面的路徑。如果頁面在另一個文件夾中，則該路徑相對於站點根。否則，如果該頁面存儲在當前文件夾中，則只能指定文件名（例如默認<b> result_edit.php </b>）。";
$MESS["NOT_SHOW_FILTER_TIP"] = "此選項允許在結果評論頁面上的過濾器中排除所需的表單字段。";
$MESS["NOT_SHOW_TABLE_TIP"] = "此選項允許在結果評論頁面上的表中排除所需的表單字段。";
$MESS["SEF_FOLDER_TIP"] = "在此處指定頁面處於活動狀態時將在地址欄中顯示的文件夾名稱。";
$MESS["SEF_MODE_TIP"] = "檢查此選項以啟用搜索引擎友好（SEF）模式。";
$MESS["SHOW_ADDITIONAL_TIP"] = "選擇\“是\”將導致Web表單結果頁面顯示其他字段（如果有）。";
$MESS["SHOW_ANSWER_VALUE_TIP"] = "如果\“是\”，則將在答案旁邊顯示Web表單問題的答案_Value參數的值（如果將此參數分配給問題）。";
$MESS["SHOW_STATUS_TIP"] = "如果是的，則結果視圖頁面將顯示當前結果狀態。";
$MESS["VIEW_URL_TIP"] = "在此處指定“結果視圖”頁面的路徑。用戶可以從“結果報告”頁面打開此頁面。默認值是<b> result_view.php </b>。";
$MESS["WEB_FORM_ID_TIP"] = "下拉列表包含當前所有exssise Web表單。另外，您可以選擇其他<i>其他</i>使用通過_request傳遞的外部表單ID。";
