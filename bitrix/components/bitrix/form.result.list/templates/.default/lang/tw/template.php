<?php
$MESS["FORM_ADD"] = "填寫新表格";
$MESS["FORM_CHANGE_TO"] = "改成";
$MESS["FORM_CONFIRM_DELETE"] = "您確定要刪除結果嗎？";
$MESS["FORM_DATE2_LESS"] = "\“ till \”日期必須大於\ \“ date（\”＃title＃\“字段）的\”日期";
$MESS["FORM_DELETE"] = "刪除";
$MESS["FORM_DELETE_ALT"] = "刪除結果";
$MESS["FORM_DELETE_CONFIRMATION"] = "您確定要刪除選定的結果嗎？";
$MESS["FORM_DELETE_SELECTED"] = "刪除所選";
$MESS["FORM_DOWNLOAD"] = "下載";
$MESS["FORM_DOWNLOAD_FILE"] = "下載文件＃file_name＃";
$MESS["FORM_EDIT"] = "編輯";
$MESS["FORM_EDIT_ALT"] = "編輯結果";
$MESS["FORM_EDIT_USER"] = "用戶設置";
$MESS["FORM_ENTERED_BY_GUEST"] = "問題和領域";
$MESS["FORM_FIELD_PARAMS"] = "現場屬性";
$MESS["FORM_FILTER"] = "顯示/隱藏過濾器";
$MESS["FORM_FILTER_OFF"] = "[未設置過濾器]";
$MESS["FORM_FILTER_ON"] = "[設置過濾器]";
$MESS["FORM_FROM_TILL_DATE_CREATE"] = "\“ till \”日期必須大於\“ date（\“創建\”字段）";
$MESS["FORM_F_AUTH"] = "用戶授權？";
$MESS["FORM_F_DATE_CREATE"] = "創建";
$MESS["FORM_F_DEL_FILTER"] = "卸下過濾器";
$MESS["FORM_F_EXISTS"] = "不是空的";
$MESS["FORM_F_GUEST"] = "訪客ID：";
$MESS["FORM_F_ID"] = "ID：";
$MESS["FORM_F_REGISTERED"] = "用戶註冊了？";
$MESS["FORM_F_SESSION"] = "會話ID：";
$MESS["FORM_F_SET_FILTER"] = "設置過濾器";
$MESS["FORM_F_STATUS"] = "地位：";
$MESS["FORM_F_STATUS_ID"] = "狀態ID：";
$MESS["FORM_F_TIMESTAMP"] = "修改的";
$MESS["FORM_F_USER"] = "用戶身份：";
$MESS["FORM_GUEST"] = "遊客";
$MESS["FORM_GUEST_ID"] = "遊客";
$MESS["FORM_INCORRECT_FORM_ID"] = "錯誤的表格ID <br>";
$MESS["FORM_INT2_LESS"] = "\“ to \”編號必須大於或等於\ \“ \”號碼（\“＃標題＃\”字段）";
$MESS["FORM_MODULE_NOT_INSTALLED"] = "未安裝Web形式模塊。";
$MESS["FORM_NOT_AUTH"] = "（未經授權）";
$MESS["FORM_NOT_REGISTERED"] = "未註冊";
$MESS["FORM_PAGES"] = "結果";
$MESS["FORM_PRINT"] = "列印";
$MESS["FORM_PRINT_ALT"] = "打開一個單獨的窗口，以打印結果的版本";
$MESS["FORM_QA_FILTER_TITLE"] = "形成問題和領域";
$MESS["FORM_RESET"] = "重置";
$MESS["FORM_SAVE"] = "節省";
$MESS["FORM_SELECT_RESULTS"] = "請選擇要刪除的結果。";
$MESS["FORM_SESSION"] = "會議";
$MESS["FORM_SESSION_ID"] = "會議";
$MESS["FORM_STATUS"] = "地位";
$MESS["FORM_TIMESTAMP"] = "修改的";
$MESS["FORM_TOTAL"] = "全部的：";
$MESS["FORM_USER"] = "由...製作";
$MESS["FORM_VIEW"] = "看法";
$MESS["FORM_VIEW_ALT"] = "查看結果";
$MESS["FORM_VIEW_FILE"] = "查看文件";
$MESS["FORM_WRONG_DATE1"] = "請輸入正確的\ \“來自\”日期（\“＃標題＃\”字段）";
$MESS["FORM_WRONG_DATE2"] = "請輸入正確的\ \“ dill \”日期（\“＃標題＃\”字段）";
$MESS["FORM_WRONG_DATE_CREATE_FROM"] = "請輸入正確的\“來自\”日期（\“創建\”字段）";
$MESS["FORM_WRONG_DATE_CREATE_TO"] = "請輸入正確的\ \“ till \”日期（\“創建\”字段）";
