<?php
$MESS["CRM_ALL"] = "全部的";
$MESS["CRM_MEASURE_DELETE"] = "刪除單元";
$MESS["CRM_MEASURE_DELETE_CONFIRM"] = "您確定要刪除'＃MEATE_TITLE＃'嗎？";
$MESS["CRM_MEASURE_DELETE_TITLE"] = "刪除這個單元的測量";
$MESS["CRM_MEASURE_EDIT"] = "編輯單元";
$MESS["CRM_MEASURE_EDIT_TITLE"] = "編輯該單元的測量參數";
$MESS["CRM_MEASURE_SHOW"] = "查看單元";
$MESS["CRM_MEASURE_SHOW_TITLE"] = "查看此單位測量的詳細信息";
