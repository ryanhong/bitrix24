<?php
$MESS["CRM_KANBAN_ED_CANCEL"] = "取消";
$MESS["CRM_KANBAN_ED_CHANGE"] = "改變";
$MESS["CRM_KANBAN_ED_CHANGE_USER"] = "改變";
$MESS["CRM_KANBAN_HAS_INACCESSIBLE_FIELDS"] = "拒絕對此功能的訪問，因為您無法使用字段。請聯繫管理員以獲取訪問並解決問題。";
$MESS["CRM_KANBAN_POPUP_COMMENT"] = "評論";
$MESS["CRM_KANBAN_POPUP_CONFIRM"] = "確認行動";
$MESS["CRM_KANBAN_POPUP_CONFIRM_DELETE"] = "您想刪除實體嗎？此操作無法撤消。";
$MESS["CRM_KANBAN_POPUP_DATE"] = "日期";
$MESS["CRM_KANBAN_POPUP_DOC_NUM"] = "發票 ＃";
$MESS["CRM_KANBAN_POPUP_INVOICE"] = "發票關閉參數：";
$MESS["CRM_KANBAN_POPUP_LEAD"] = "根據鉛創建：";
$MESS["CRM_KANBAN_POPUP_LEAD_SELECT"] = "選擇";
$MESS["CRM_KANBAN_POPUP_PARAMS_CANCEL"] = "取消";
$MESS["CRM_KANBAN_POPUP_PARAMS_DELETE"] = "刪除";
$MESS["CRM_KANBAN_POPUP_PARAMS_SAVE"] = "節省";
