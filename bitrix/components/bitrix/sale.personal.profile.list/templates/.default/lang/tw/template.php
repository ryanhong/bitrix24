<?php
$MESS["P_DATE_UPDATE"] = "更新日期";
$MESS["P_ID"] = "代碼";
$MESS["P_NAME"] = "姓名";
$MESS["P_PERSON_TYPE_ID"] = "付款人類型";
$MESS["SALE_ACTION"] = "動作";
$MESS["SALE_DELETE"] = "刪除";
$MESS["SALE_DELETE_DESCR"] = "刪除配置文件";
$MESS["SALE_DETAIL"] = "調整";
$MESS["SALE_DETAIL_DESCR"] = "編輯個人資料";
$MESS["STPPL_DELETE_CONFIRM"] = "您確定要刪除此個人資料嗎？";
$MESS["STPPL_EMPTY_PROFILE_LIST"] = "沒有個人資料";
