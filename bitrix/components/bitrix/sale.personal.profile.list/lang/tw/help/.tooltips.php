<?php
$MESS["PATH_TO_DETAIL_TIP"] = "配置文件查看頁面的路徑名。配置文件ID應作為參數傳遞。";
$MESS["PER_PAGE_TIP"] = "指定每個頁面的配置文件數量。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將頁面標題設置為<b>配置文件</b>";
