<?php
$MESS["INTRANET_OTP_CLOSE"] = "稍後提醒我";
$MESS["INTRANET_OTP_CLOSE_FOREVER"] = "不要再顯示";
$MESS["INTRANET_OTP_CODE"] = "第二道防線：驗證代碼";
$MESS["INTRANET_OTP_GOTO"] = "開始";
$MESS["INTRANET_OTP_MANDATORY_DESCR"] = "<p>今天，您的Bitrix24受到數據加密技術的保護，每個用戶都有一對登錄和密碼。但是，那裡
是惡意用戶可以使用計算機並竊取這些數據的工具。</p>
<p>您的系統管理員已啟用了額外的安全選項，現在正在詢問
您啟用兩步身份驗證。</p>
<p>兩步身份驗證意味著您必須通過兩個級別
登錄時驗證。首先，您將輸入密碼。然後，你必須
輸入發送到您的移動設備的一次性安全代碼。</p>
<p>這將使您的業務數據更加安全。</p>";
$MESS["INTRANET_OTP_MANDATORY_DESCR2"] = "<p>您有＃num＃。如果您不啟用兩步身份驗證，則無法登錄Bitrix24。</p>";
$MESS["INTRANET_OTP_MANDATORY_TITLE"] = "您的業​​務數據額外安全性";
$MESS["INTRANET_OTP_PASS"] = "第一道防線：您的密碼";
