<?php
$MESS["SN_SPACES_ERROR_GROUP_TASKS_UNAVAILABLE"] = "拒絕訪問任務。請聯繫您的主管或Bitrix24管理員。";
$MESS["SN_SPACES_ERROR_USER_TASKS_UNAVAILABLE"] = "拒絕訪問任務。請聯繫您的主管或Bitrix24管理員。";
$MESS["SN_SPACES_NEW_SPACE_EMPTY_STATE_TEXT"] = "邀請同事，討論思想和項目，安排會議和會議，並從事其他協作活動。";
$MESS["SN_SPACES_NEW_SPACE_EMPTY_STATE_TITLE"] = "新空間<b> #space_name＃</b>現在準備就緒";
