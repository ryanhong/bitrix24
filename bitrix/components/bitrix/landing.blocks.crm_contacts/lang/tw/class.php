<?php
$MESS["LNDNG_BLPHB_ALERT_REQUISITE_HELP"] = "此塊顯示了當前的詳細信息。您可以在＃link1＃您的公司信息＃link2＃頁面上編輯它們。";
$MESS["LNDNG_BLPHB_ERROR_BLOCK_NOT_ACTIVE"] = "塊是不活動的。";
$MESS["LNDNG_BLPHB_ERROR_CRM_NO_SALESCENTER_TEXT"] = "檢查\“ chat啟用sales（measoncenter）\”模塊是否存在於＃link1＃模塊＃鏈接2＃列表中。";
$MESS["LNDNG_BLPHB_ERROR_NO_REQUISITE_TEXT"] = "您必須提供＃鏈接1＃詳細信息＃鏈接2＃才能使塊正常運行。";
$MESS["LNDNG_BLPHB_ERROR_NO_REQUISITE_TITLE"] = "公司詳細信息沒有數據。";
