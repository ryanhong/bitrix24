<?php
$MESS["GRID_COLUMNS_INCORRECT"] = "列參數不正確。它必須指定列數組。";
$MESS["GRID_ID_INCORRECT"] = "grid_id不正確。它必須是一個非空字符串，例如：\“ bitrix_example_grid \”";
$MESS["interface_grid_check"] = "標記進行編輯";
$MESS["interface_grid_dblclick"] = "按兩下 -";
$MESS["interface_grid_default_view"] = "<默認視圖>";
