<?php
$MESS["ID_TIP"] = "將用於構建訂單查看頁面等鏈接的訂單ID。";
$MESS["PATH_TO_CANCEL_TIP"] = "訂單取消頁面的路徑。";
$MESS["PATH_TO_LIST_TIP"] = "訂單頁面的地址。";
$MESS["PATH_TO_PAYMENT_TIP"] = "付款系統處理器的路徑。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將頁面標題設置為\“我的訂單＃order_id＃\”。";
