<?php
$MESS["INTR_ISBN_PARAM_DETAIL_URL"] = "用戶個人資料頁面";
$MESS["INTR_ISBN_PARAM_NUM_USERS"] = "向用戶顯示";
$MESS["INTR_ISH_PARAM_DATE_FORMAT"] = "日期格式";
$MESS["INTR_ISH_PARAM_DATE_FORMAT_NO_YEAR"] = "日期格式（無年份）";
$MESS["INTR_ISH_PARAM_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["INTR_ISH_PARAM_NAME_TEMPLATE"] = "名稱格式";
$MESS["INTR_ISH_PARAM_NAME_TEMPLATE_DEFAULT"] = "＃NOBR ##名稱＃#last_name ##/nobr＃";
$MESS["INTR_ISH_PARAM_PATH_TO_CONPANY_DEPARTMENT"] = "部門頁面路徑模板";
$MESS["INTR_ISH_PARAM_PATH_TO_VIDEO_CALL"] = "視頻通話頁面";
$MESS["INTR_ISH_PARAM_PM_URL"] = "個人消息頁面";
$MESS["INTR_ISH_PARAM_SHOW_LOGIN"] = "顯示登錄名，如果沒有必需的用戶名字段";
$MESS["INTR_ISH_PARAM_SHOW_YEAR"] = "出生年份";
$MESS["INTR_ISH_PARAM_SHOW_YEAR_VALUE_M"] = "只有男性";
$MESS["INTR_ISH_PARAM_SHOW_YEAR_VALUE_N"] = "沒有人";
$MESS["INTR_ISH_PARAM_SHOW_YEAR_VALUE_Y"] = "全部";
