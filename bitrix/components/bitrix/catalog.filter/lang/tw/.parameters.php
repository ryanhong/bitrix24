<?php
$MESS["CP_BCF_CACHE_GROUPS"] = "尊重訪問權限";
$MESS["CP_BCF_OFFERS_FIELD_CODE"] = "SKU場";
$MESS["CP_BCF_OFFERS_PROPERTY_CODE"] = "SKU屬性";
$MESS["CP_BCF_PAGER_PARAMS_NAME"] = "帶有變量的陣列的名稱以構建麵包屑鏈接";
$MESS["CP_CF_PREFILTER_NAME"] = "帶有其他項目過濾器的輸入數組的名稱";
$MESS["IBLOCK_FIELD"] = "字段";
$MESS["IBLOCK_FILTER_NAME_OUT"] = "結果過濾器數組的名稱";
$MESS["IBLOCK_IBLOCK"] = "Infoblock";
$MESS["IBLOCK_LIST_HEIGHT"] = "多選擇列表字段的高度";
$MESS["IBLOCK_NUMBER_WIDTH"] = "數字間隔字段的寬度";
$MESS["IBLOCK_PRICES"] = "價格";
$MESS["IBLOCK_PRICE_CODE"] = "價格類型";
$MESS["IBLOCK_PROPERTY"] = "特性";
$MESS["IBLOCK_SAVE_IN_SESSION"] = "在用戶會話中存儲過濾器設置";
$MESS["IBLOCK_TEXT_WIDTH"] = "單行文本字段的寬度";
$MESS["IBLOCK_TYPE"] = "InfoBlock類型";
