<?php
$MESS["AJAX_MODE_TIP"] = "在組件中啟用Ajax";
$MESS["AJAX_OPTION_HISTORY_TIP"] = "允許\“ Back \”和\“ Forward \” ajax Transitions的瀏覽器按鈕";
$MESS["AJAX_OPTION_JUMP_TIP"] = "當Ajax過渡完成時，滾動到組件";
$MESS["AJAX_OPTION_SHADOW_TIP"] = "Ajax過渡上的陰影可修改區域";
$MESS["AJAX_OPTION_STYLE_TIP"] = "下載和處理AJAX過渡上的CSS組件樣式";
$MESS["SET_TITLE_TIP"] = "檢查此選項將將頁面標題設置為<b>用戶配置文件</b>。";
$MESS["USER_PROPERTY_NAME_TIP"] = "標籤的名稱將顯示哪些用戶屬性";
$MESS["USER_PROPERTY_TIP"] = "選擇要在用戶配置文件中顯示的其他屬性";
