<?php
$MESS["MESSAGES_PER_PAGE_TIP"] = "指定每個頁面的消息數。";
$MESS["SEF_FOLDER_TIP"] = "指定組件工作文件夾。該文件夾可以由真實的文件系統路徑表示，也可以是虛擬的。";
$MESS["SEF_MODE_TIP"] = "檢查此選項啟用SEF模式和URL配置字段。";
$MESS["SEF_URL_TEMPLATES_ticket_edit_TIP"] = "消息編輯器的路徑名。";
$MESS["SEF_URL_TEMPLATES_ticket_list_TIP"] = "指定門票頁面。如果門票頁面是當前部分中的索引頁面，請將此字段留為空。";
$MESS["SET_PAGE_TITLE_TIP"] = "定義組件是否設置頁面標題。";
$MESS["TICKETS_PER_PAGE_TIP"] = "指定每個頁面的門票數量。";
$MESS["VARIABLE_ALIASES_ID_TIP"] = "票證ID通過的變量，例如<b> id </b>。";
