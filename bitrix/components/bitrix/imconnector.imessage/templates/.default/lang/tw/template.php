<?php
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CHANGE_ANY_TIME"] = "您可以隨時編輯或斷開連接";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECTED"] = "Apple商業聊天已連接";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_CONNECTION_STEP"] = "要完成連接，請選擇一個打開的通道要連接。";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_CONNECTION_TITLE"] = "現在，商業聊天幾乎連接到打開頻道";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_HELP"] = "<div class = \“ imconnector-field-button-subtitle \”>我想：</div>
<div class = \“ imconnector-field-button-name \”> <span class = \“ imconnector-field-box-text-bold \”> connect </span> connect </span>商業>";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_STEP_NEW"] = "連接之前，您必須＃link1_start＃創建一個業務聊天＃link1_end＃或＃link2_start＃連接現有的一個＃link2_end＃。";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONNECT_TITLE"] = "將業務聊天連接到打開頻道";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_FINAL_FORM_DESCRIPTION"] = "商業聊天已連接到您的開放頻道。發佈到您的商務聊天中的所有消息都將重定向到您的Bitrix24。";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_ADDITIONAL_DESCRIPTION_NEW"] = "連接之前，您必須＃link1_start＃創建一個業務聊天＃link1_end＃或＃link2_start＃連接現有的一個＃link2_end＃。只能連接經過驗證的Apple業務聊天ID。手動連接或配置。";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_LIST_ITEM_1"] = "將聯繫人和通信歷史保存到CRM";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_LIST_ITEM_2"] = "指導客戶通過CRM中的銷售渠道";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_LIST_ITEM_3"] = "回复您的客戶何時何地";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_LIST_ITEM_4"] = "根據隊列規則，客戶查詢分佈在銷售代理之間";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_SUBTITLE"] = "以他們想要的方式回复您的客戶。如果客戶發現Apple消息比其他交流方式更方便，請接收到您的Bitrix24的消息並立即回复。";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INDEX_TITLE"] = "使用Apple消息與Apple設備的所有者進行通信";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INFO"] = "資訊";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INFO_NEW_CONNECT"] = "商業聊天要連接";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INFO_OLD_CONNECT"] = "連接的商務聊天信息";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_INSTRUCTION_TITLE"] = "<span class = \“ imconnector-field-box-text-bold \”>連接指令：</span>";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_I_KNOW_KEY"] = "我有業務ID";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NAME_BUSINESS_ID"] = "業務ID";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NAME_BUSINESS_NAME"] = "姓名";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NAME_CHAT_LINK"] = "實時聊天鏈接";
