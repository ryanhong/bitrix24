<?php
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONFIRMATION_RECONNECTION"] = "商業聊天已經連接到此頻道。您想重新連接嗎？";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONFIRM_BUTTON_CANCEL"] = "取消";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONFIRM_BUTTON_OK"] = "好的";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONFIRM_DESCRIPTION_NEW_2"] = "要將Apple Message連接到Bitrix24，您必須使用現有的Apple ID（或創建一個），並將Bitrix24指定為提供商服務。<br /> <br />了解更多<a onclick = \'top. bx。 helper.show（\'＃id＃\'）; \“ class = \” imConnector-field-box-box-link imConnector-box-box-box-box-box-box-link-form-imessage \ '>在“幫助”部分中BR /> <br />確保在連接之前遵循Apple指南。 <a onclick= \“top.bx.helper.show( \'# d_imit# \'); class =”imconnector-field-box-box-box-box-box-link imConnector-box-box-box-box -box-box-link-link-form-mimessage \">閱讀文檔</a>。<br /> <br />您是否想繼續訪問Apple網站以創建連接？新頁面將在此窗口中打開。";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_CONFIRM_TITLE"] = "確認進入Apple Business登記冊";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_FORM_CONNECTION_INFORMATION"] = "連接信息";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_FORM_MANUALLY"] = "手動配置";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_FORM_OR"] = " 或者";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_MODULE_NOT_INSTALLED_MSGVER_1"] = "\“外部Messenger Connectors \”模塊未安裝。";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NO_ACTIVE_CONNECTOR"] = "該連接器不活動。";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NO_CONNECT"] = "無法使用提供的參數建立測試連接";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NO_DATA_SAVE"] = "沒有數據要保存";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NO_REGISTER"] = "註冊錯誤";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_NO_SAVE"] = "保存錯誤數據。請檢查您的輸入，然後重試。";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_OK_CONNECT"] = "測試連接已成功建立";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_OK_REGISTER"] = "註冊是成功的";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_OK_SAVE"] = "信息已成功保存。";
$MESS["IMCONNECTOR_COMPONENT_IMESSAGE_SESSION_HAS_EXPIRED"] = "您的會議已經過期。請再次提交表格。";
