<?php
$MESS["INTRANET_USTAT_TELLABOUT_CRM_TEXT"] = "客戶和客戶，無論是潛在的，現有的還是長期存在的，都是業務。我們的工作是學習如何以最有效的方式與每個客戶合作。每次購買，每次互動 - 與客戶的一切都應記錄並可用。

Bitrix24中的CRM是這樣做的理想選擇，我們可以在不留下熟悉的Intranet設置的情況下使用它。並且不必記住其他服務的密碼。

[視頻寬度= 510高= 390] https://www.youtube.com/watch?v=n6k3udpfeoa [/video]

[IMG寬度= 500高= 315] https://www.bitrix24.com/images/images/ustat/en/crm1.png [/img]

[b]客戶和聯繫基礎[/b]

有關客戶（公司和人員）的所有信息，都可以記錄在CRM的接觸室中。此外，可以列出所有交互，並可以安排未來的交互。 CRM中稱為“交易”的購買或交易可以單獨保存並與每個客戶關聯。

[IMG寬度= 500高= 347] https://www.bitrix24.com/images/ustat/en/crm2.png [/img]

每個出現的新客戶都應添加到CRM中 - 別忘了這樣做！潛在客戶（潛在客戶）可以手動放入，通過電子郵件或從數據文件中導入的網站聯繫方式或反饋表自動帶入。

[img width = 500高= 278] https://www.bitrix24.com/images/ustat/en/crm3.png [/img]

[b]與客戶合作[/b]

請記住，CRM不僅是接觸基地。它是一種工具，可幫助您通過購買或決策過程來帶來和跟踪客戶。您可以直接從CRM中計劃和會議，設置任務或直接寫電子郵件。

[img width = 500高= 385] https://www.bitrix24.com/images/images/ustat/en/crm4.png [/img]

[b]將聯繫人分配給銷售夥伴[/b]

我們可以自動將引線分配給不同的銷售協會。我們需要做的就是建立一個業務流程，該流程“類別”根據所需的任何條件來領導，然後從銷售團隊中設置負責人。例如，如果潛在客戶的“機會”超過$ 9999，我們可以將其直接發送給VIP銷售經理。

一旦獲得有關潛在購買的信息，應立即創建CRM中的交易。該交易不僅跟踪了決策過程的進度，還可以為銷售渠道提供數據，並使發票創建成為一種快點。

[IMG寬度= 500高= 309] https://www.bitrix24.com/images/ustat/en/crm5.png [/img]

[b]與客戶進行的互動[/b]

CRM中收集的數據可以幫助您將來與客戶互動。您還可以計劃會議，跟進電話，並與客戶合作或單獨與客戶合作，因為您可以根據需要對客戶記錄進行分類和過濾。活動是向您展示所有即將到來的互動的部分，旁邊的計數器告訴您有多少需要關注！

[img width = 500高= 171] https://www.bitrix24.com/images/images/ustat/en/crm6.png [/img]

該系統可讓您（從電子郵件地址）發送電子郵件，並在非常方便的界面中創建發票。單擊聯繫人的電話號碼以啟動IP電話電話。當您將發票發送給客戶端時，發票將作為PDF附件包含。

[IMG寬度= 500高= 335] https://www.bitrix24.com/images/images/ustat/en/crm7.png [/img]

不要忘記，您可以隨時分析數據 -  [B]銷售渠道[/b]顯示在各個階段的交易，並讓您過濾各種標準顯示的交易。

[IMG寬度= 500高= 359] https://www.bitrix24.com/images/images/ustat/en/crm8.png [/img]

[b]移動CRM [/b]

順便說一句，對於那些在路上並與該領域客戶見面客戶的人，移動應用程序中有CRM。您可以提取任何客戶信息，在談判後編輯交易的詳細信息，或更新其他信息 - 然後立即創建發票。

[img width = 500高= 320] https://www.bitrix24.com/images/ustat/en/crm9.jpg [/img]

[b]評估您在CRM [/b]中的工作

盡可能多地使用CRM  - 有組織的銷售數據的回報是無數的。要評估您的CRM活動與您的部門或公司的整體相比，請檢查公司脈動。

[IMG寬度= 500高= 269] https://www.bitrix24.com/images/ustat/en/crm10.png [/img]

[b]有關更多信息[/b]

如果您想了解有關CRM的更多詳細信息，我建議您建議[url = https：//www.bitrixsoft.com/support/training/course/course/index.php？course_id = 55＆contercy_id = 05423]在線培訓課程[/url]。

別忘了喜歡這篇文章！ :)

[b]讓我們現在開始！[/b]

首先，嘗試將一些信息輸入有關與您合作的客戶的信息。這是提高銷售效率和提高客戶服務的關鍵工具！";
$MESS["INTRANET_USTAT_TELLABOUT_CRM_TITLE"] = "CRM：與客戶合作";
$MESS["INTRANET_USTAT_TELLABOUT_DISK_TEXT"] = "我們現在如何在文檔上合作？我們2個或更多人如何合著新聞稿或培訓計劃？我們來回發送電子郵件，並以不同的版本發送電子郵件，有時還會通過不斷變化的收件人組發送。這樣很容易被這樣弄亂 - 您可能已經註意到了。

使用本地共享驅動器，幾乎沒有辦法知道誰做了什麼變化，更重要的是，如果您不在辦公室，您將無法進入文檔，並且有人在自己的機器上可能沒有新副本尚未上傳。

社會Intranet極大地簡化了關於文檔的協作。

[視頻寬度= 400高= 300] https://www.youtube.com/watch?v=wsqopmqcu6m [/video]

[b]共享文件[/b]

共享文件沒有什麼複雜的 - 您甚至可以在Intranet中創建文件並立即共享。要與同事共享文件以進行討論，改進或僅出於他們的信息，您可以通過在活動流中啟動新帖子開始。上傳該文件，或創建一個新文件（請參見下圖中的底部）。

[img width = 500高= 579] https://www.bitrix24.com/images/ustat/en/disk1.png [/img]

如果上面顯示的複選框，此消息的所有收件人都將能夠編輯文檔。此外，當某人單擊它時，文檔將立即在預覽窗口中打開。

[img width = 500 height = 562] https://www.bitrix24.com/images/images/ustat/en/disk2.png [/img]

[b]在線編輯文檔[/b]

要編輯文檔，您不必下載，保存然後上傳。您需要從“預覽窗口”中使用的只是選擇使用Google Docs或MS Office在線編輯它。您可以從下拉中選擇。

[img width = 500高= 331] https://www.bitrix24.com/images/ustat/ustat/en/disk3.png [/img]

要以這種方式預覽或編輯文檔，您需要登錄Microsoft或Google。這些是免費服務。

現在，您可以編輯文本並保存更改。

在編輯文檔並保存後，活動流將創建一個新評論，該評論指出您已經創建了該文檔的新版本。

[IMG寬度= 500高= 428] https://www.bitrix24.com/images/images/ustat/en/disk4.png [/img]

[b]在哪裡保存文檔 - 如何設置文件交換[/b]

為了確保您的文件始終可用於同事，我們建議使用Bitrix24.Drive。這是位於Bitrix桌面應用程序中的文件同步應用程序。您的本地文件將與Bitrix24 Intranet文檔庫同步。

轉到菜單項我的工作區>文件，然後在本地計算機上安裝Bitrix24桌面應用程序。

[img width = 500高= 194] https://www.bitrix24.com/images/ustat/ustat/en/disk5.png [/img]

桌面應用程序可以在www.bitrix24.com上下載：
[列表]
[*]對於macOS [url] http://dl.bitrix24.com/b24/bitrix24_desktop.dmg [/url]
[*]對於Windows [url] http://dl.bitrix24.com/b24/bitrix24_desktop.exe [/url]
[/列表]
Bitrix24.Drive將在文件管理器中創建一個文件夾，您可以像其他任何文件夾一樣使用該文件夾。但是，該文件夾的內容將與Bitrix24中的文件庫同步。如果您要脫機工作，那麼這些文件將像往常一樣保存，並且一旦恢復與Bitrix24的連接，系統將自動同步。

[IMG寬度= 500高= 397] https://www.bitrix24.com/images/ustat/en/disk6.png [/img]

如果將文件保存或編輯在此文件夾中，則會顯示我的驅動器庫中的相同文件。同樣，如果您從門戶網站打開文件並進行編輯，則將立即對計算機上的文件進行編輯。

[img width = 500高= 358] https://www.bitrix24.com/images/ustat/ustat/en/disk7.png [/img]

[b]如何允許或拒絕訪問文件[/b]

[i]我的驅動器[/i]庫是您的個人文件存儲，您可以在整個庫上或單個文件上放置訪問限制。可以授予您的同事的任何子集，包括工作組或部門。

您可以根據需要調整每個文檔上的訪問權限，並且可以創建特殊的下載鏈接，以便門戶網站外的人們可以下載給定的文檔。這些下載鏈接可以受到時間限製或受密碼保護。

[img width = 500高= 319] https://www.bitrix24.com/images/ustat/ustat/en/disk8.png [/img]

[b]在Bitrix24.Drive和Documents [/b]中評估您的活動

為了評估您在與您的部門和公司總體上使用文件合作的積極性，請打開公司脈動並將您的排名與您的同事進行比較。

[img width = 500高= 271] https://www.bitrix24.com/images/ustat/ustat/en/disk9.png [/img]

[b]了解更多[/b]

與文檔庫和Bitrix24有關的文檔可以在此處找到：

[列表]
[*] [url = https：//www.bitrixsoft.com/support/training/course/index.php？courose_id = 55＆cystral_id = 05754]使用文件[/url]
[*] [url = https：//www.bitrixsoft.com/support/training/course/index.php？course_id = 55＆threns_id = 5760]
[/列表]
Bitrix24.Drive和在線編輯器中可在Intranet中提供，讓我們編輯文檔，並更加快速，更輕鬆地協作。讓我們立即開始使用這些工具！

[b]立即開始​​！[/b]

為了練習，我建議每個閱讀此帖子的人都會打開此帖子附加的文檔，並將您的名字添加到文件中。我認為您會發現它比您預期的要容易！

不要忘記將此帖子添加到您的收藏夾中:)";
$MESS["INTRANET_USTAT_TELLABOUT_DISK_TITLE"] = "文檔編輯... 60秒內";
$MESS["INTRANET_USTAT_TELLABOUT_IM_TEXT"] = "每個人：當您彼此討論緊急問題時，您會使用什麼工具？郵件，電話，短信，Skype，ICQ，Facebook？有選擇很棒，但是您知道2週後在哪裡可以找到信息？這是在Bitrix24中可以使用的方法。

[b]聊天[/b]

[IMG寬度= 500高= 339] https://www.bitrix24.com/images/images/ustat/en/im1.png [/img]

請注意 - 在我們的網站上有自己的聊天，其中已經包括我們所有的員工（沒有個人聯繫！）。與每個人或每個組中的聊天單獨存儲，並且有一個可以檢索的歷史記錄按鈕。聊天可以是1比1，小組聊天，甚至可以從此界面進行語音和視頻通話！

[b]聯繫人已經存在[/b]

從聊天開始很容易，因為您不必添加任何人 - 所有員工都已包含在您的聯繫人列表中。任何同事都可以簡單地通過在現場打字來找到；最近的標籤顯示了您的最新對話，因此更容易找到您與之交談的人。

[img width = 500高= 339] https://www.bitrix24.com/images/ustat/en/im2.png [/img]

請注意，如果某人在線（即目前可用），則其圖片旁邊的指示器將為綠色。如果他們離線或將自己標記為忙碌，那將是紅色。

聊天中的一切都實時發生。您可以查看對應方是否在輸入消息，並且您可以確認已閱讀消息。

[img width = 500高= 345] https://www.bitrix24.com/images/images/ustat/en/im3.png [/img]

[b]小組聊天[/b]

如果您需要一次與幾個人交談，則可以避免中斷每個人的工作日，而只是創建一個小組聊天。聊天本身將被保存，可以在以後重新開放，因此您不必再次創建同一組。

[img width = 500高= 349] https://www.bitrix24.com/images/images/ustat/en/im4.4.png [/img]

[b]消息歷史[/b]

所有聊天都保存在Intranet中，任何時候您都可以輕鬆地找到它們，在右上角打開“聊天歷史記錄”，然後搜索單詞或短語。

[img width = 500高= 345] https://www.bitrix24.com/images/ustat/en/im5.png [/img]

[b]移動聊天[/b]

聊天可通過移動應用程序訪問，以及其他Messenger功能，例如通知，接收會議邀請以及有關喜歡，任務和評論的更新。

BitRix24移動應用程序可以輕鬆安裝在智能手機和平板電腦上，來自[url = https：//itunes.apple.com/app/bitrix24/id561683423] apple appstore [/url]和[url = https：//play .google 。

[IMG寬度= 500高= 461] https://www.bitrix24.com/images/images/ustat/en/im6.jpg [/img]

[b]視頻通話[/b]

有時您需要的不僅僅是聊天 - 您需要討論和互動。同一Messenger接口中的視頻和音頻聊天完全提供了這一點。它不僅包含多達4個用戶，而且是免費的，也是團結遠方工人的好方法。

[img width = 500高= 305] https://www.bitrix24.com/images/ustat/ustat/en/im7.png [/img]

如果需要，您可以將自己的麥克風靜音。

視頻聊天在支持WEBRTC（例如Chrome）的瀏覽器中支持。如果您使用其他瀏覽器，則可以通過桌面應用程序使用視頻聊天。

[b]桌面應用[/b]

您可以使用BitRix24桌面應用程序即使沒有打開瀏覽器即使可以保持聯繫。它支持Messenger的所有功能，包括視頻呼叫，並具有針對個人文件的其他文件同步函數。它還位於工具欄上，讓您知道何時出現新消息。

[img width = 500高= 313] https://www.bitrix24.com/images/ustat/ustat/en/im8.jpg [/img]

[列表]
[*]對於macOS [url] http://dl.bitrix24.com/b24/bitrix24_desktop.dmg [/url]
[*]對於Windows [url] http://dl.bitrix24.com/b24/bitrix24_desktop.exe [/url]
[/列表]
總結到目前為止我們所說的話：我們有一種工具可以替換所有類型的使者，內部電話，甚至短信 - 所有這些工具仍然以一種或另一種形式使用。使用Bitrix24中的聊天和視頻通話，我們可以根據需要進行準確的交流 - 取決於對話的緊迫性。

[b]評估您在Messenger中的活動[/b]

您可以在Messenger中查看您的活動水平，並將其與您的部門和公司的公司進行比較。

[img width = 500高= 270] https://www.bitrix24.com/images/images/ustat/en/im9.png [/img]

[b]了解更多[/b]

如果您想要有關使用Messenger，聊天或視頻通話的更多詳細信息，請參閱以下文檔：

[列表]
[*] [url = https：//www.bitrixsoft.com/support/training/course/index.php？course_id = 55＆thress_id = 5170] chat [/url]
[*] [url = https：//www.bitrix24.com/features/mobile-and-desktop-apps.php]桌面應用程序和移動聊天[/url]
[/列表]
如果您還有疑問，可以通過視頻與我聯繫:)

將此帖子保留在您的收藏夾中，直到您熟悉Messenger及其功能為止。

讓我們開始使溝通更輕鬆，更有趣。";
$MESS["INTRANET_USTAT_TELLABOUT_IM_TITLE"] = "與樂趣合作！";
$MESS["INTRANET_USTAT_TELLABOUT_LIKES_TEXT"] = "我的朋友！讓我們停止太害羞，開始談論公司中發生的所有酷事物。當然，我們的Intranet並不是一個社交網絡，但它具有“喜歡”，並且是確保您知道發生了什麼的好方法。

查看帖子，評論，照片和文件，並“喜歡”其中一些。

[img width = 500高= 341] https://www.bitrix24.com/images/images/ustat/en/likes1.png [/img]

[b]如何表明您喜歡某些東西[/b]

如果活動流中的同事的帖子或消息，則可以使用該帖子中的“喜歡”按鈕“喜歡”它。喜歡只花一秒鐘，但他們讓寫作的人知道，貢獻對某人很有價值，並且讓其他人知道這對他們可能很有用。

[b]誰喜歡什麼[/b]

您可以通過懸停在單詞旁邊的恆星上“喜歡”帖子或評論的用戶列表。這些喜歡很重要，因為搜索功能將優先考慮與任何給定搜索相關的良好的內容。

[img width = 500高= 255] https://www.bitrix24.com/images/images/ustat/en/likes2.png [/img]

[b]為什麼有喜歡的評分系統？[/b]

社交搜索 - 意味著受讚譽影響的搜索結果，幫助用戶找到特別有用的文檔和討論。

[IMG寬度= 500高= 426] https://www.bitrix24.com/images/ustat/en/likes3.png [/img]

首頁上的“流行帖子”部分顯示了具有最多“喜歡”的消息。

[b]你還能喜歡什麼？[/b]

消息和評論並不是唯一可以喜歡的東西。您可以在完成任務時喜歡他們，以激勵同事繼續努力工作。也可以喜歡文件和照片。

[img width = 500高= 322] https://www.bitrix24.com/images/ustat/en/likes4.png [/img]

[b]評估您的喜歡活動[/b]

要查看您使用“喜歡”功能的積極性，請使用公司脈衝並將您的分數與您的部門或整個公司的分數進行比較。

[img width = 500高= 271] https://www.bitrix24.com/images/images/ustat/en/likes5.png [/img]

[b]開始喜歡它！[/b]

您現在可以在此帖子中開始使用“喜歡”按鈕。不要忘記在您認為很重要或用作參考的文件和任務上放喜歡。鼓勵您的同事，並作為一個團隊做更多的工作 - 類似按鈕使它變得簡單！
別忘了喜歡這個消息:)";
$MESS["INTRANET_USTAT_TELLABOUT_LIKES_TITLE"] = "我喜歡！我喜歡！我喜歡！";
$MESS["INTRANET_USTAT_TELLABOUT_MOBILE_TEXT"] = "大家好！我們經常必須在辦公室外工作 - 在商務旅行，客戶會議，貿易展覽會上等。不可能隨身攜帶筆記本電腦，但您確實需要與同事保持聯繫。因此，我們建議使用BitRix24移動應用程序- 從[url = https：//itunes.apple.com/app/bitrix24/id561683423] apple appe appstore [/url]或[url = https = https = https = https apple = https apple = https24/id561683423]易於安裝在智能手機或平板電腦上：//play.google.com/store/apps/details？id = com.bitrix24.android] Google Play [/url]。

[img] https://www.bitrix24.com/images/ustat/en/mob1.gif [/img]

現在，您可以獲取更新，閱讀活動流，對帖子發表評論，獲取通知並對同事做出回應，無論您身在何處。

[b]同事聯繫人總是與您同在[/b]

您擁有移動應用程序中所有公司員工的完整，始終更新的聯繫信息，因此，如果您需要快速與某人聯繫，即使是您不經常打電話的人，也可以在手機中找到他們像Intranet中一樣容易地與它們聯繫。

[img] https://www.bitrix24.com/images/images/ustat/en/mob3.png [/img] [img] https://www.bitrix24.com/images/images/images/ustat/ustat /en/en/mob4.png [/ IMG]

向您的同事發送消息，它將出現在Intranet中，如果您不在辦公室，您將通過移動設備收到有關評論的通知。

[img] https://www.bitrix24.com/images/images/ustat/en/mob5.png [/img] [img] https://www.bitrix24.com/images/images/images/ustat/ustat /en/en/mob6.png [/png [/ IMG]

[b]移動應用可以做什麼？[/b]

移動應用程序可讓您訪問Bitrix24 Intranet中的所有主要工具：活動流，註釋，喜歡，日曆，帶有即將舉行的會議，任務，搜索和文檔庫的列表。無論您是否在辦公室裡，您都將始終意識到發生了什麼並能夠實時參與討論。

[img] https://www.bitrix24.com/images/images/ustat/en/mob7.png [/img] [img] https://wwwww.bitrix24.com/images/images/images/ustat/ustat /ustat/en/en/mob8.png [/ IMG] [IMG] https://www.bitrix24.com/images/ustat/en/mob9.png [/img]

如果您離開辦公室，您仍然可以設置任務，對它們發表評論並監視其中的進度。

[img] https://www.bitrix24.com/images/images/ustat/en/mob10.png [/img] [img] https://www.bitrix24.com/images/images/images/ustat/ustat /ustat/en/en/mob11.png [/ IMG]

[b]移動CRM [/b]

最好的部分很可能是移動應用程序中的CRM工作？該應用程序功能齊全，因此，當您搬家時，您可以獲取有關客戶的信息，與他們撥打電話，檢查產品目錄，修改交易，甚至創建發票。試想一下，這是多麼方便 - 更改客戶辦公室的鉛狀態或交易！

[img width = 500高= 320] https://www.bitrix24.com/images/ustat/en/mob12.jpg [/img]

[b]在移動應用程序中評估您的工作[/b]

您可以監視您如何積極使用移動應用程序，並將其與公司或公司脈動中的公司或部門進行比較。

[IMG寬度= 500高= 269] https://www.bitrix24.com/images/images/ustat/en/mob13.png [/img]

有關移動應用程序的其他信息，請訪問[url = https：//www.bitrix24.com/features/mobile-and-desktop-apps.php] bitrix24網站[/url]。

不要忘記將此帖子添加到您的收藏夾中:)

我希望使用移動應用程序將變得更加高效，即使我們不在辦公室時，我們也能夠使一切前進。";
$MESS["INTRANET_USTAT_TELLABOUT_MOBILE_TITLE"] = "立即動員！";
$MESS["INTRANET_USTAT_TELLABOUT_SOCNET_TEXT"] = "我們都可以看到技術正在以盲目的速度發展。社交網絡由於其便利性以及跨計算機和移動設備的交換消息和媒體的簡單性而擠出了其他通信形式。因此，讓我們在工作場所使用相同的技術。

[img width = 500高= 338] https://www.bitrix24.com/images/images/ustat/en/socnet1.png [/img]

[b]為什麼我們需要一個內部社交網絡？[/b]

[列表]
[*]首先，我們需要在單個系統中合併我們的數據，文檔，知識和聯繫；
[*]這將簡化並加快常規操作（使用討論線程而不是大量電子郵件對主題進行權衡的速度要快得多）；
[*]您將不再錯過重要信息；
[*]您可以提供想法，分享意見，發布有用的信息 - 每個人（或僅在[i]中放入的人）都會看到您的消息：[/i]字段）；
[*]最後，您可以“喜歡”帖子並提高士氣：)
[/列表]

我建議從現在了解我們社會Intranet的功能開始。我會告訴你如何做。

[視頻寬度= 400高= 300] https://www.youtube.com/watch?v=fn6bsyjooty [/video]

[b]如何發送消息[/b]

在我們的Intranet中，您可以共享與工作相關的任何信息。消息可以發送給所有員工，單個組或單個用戶。收件人將在他們的常規[i]活動流中看到您的消息。

[img width = 500高= 313] https://www.bitrix24.com/images/images/ustat/en/socnet2.png [/img]

您可以將文件，圖像，添加鏈接，標籤甚至視頻附加到您的消息中，您可以提及各個同事：

[IMG寬度= 500高= 330] https://www.bitrix24.com/images/images/ustat/en/socnet3.png [/img]

[b]如何創建或輸入組[/b]

工作組是項目的方便工具。要創建自己的組並邀請成員參與其中，請使用“添加”按鈕（左上角的綠色按鈕）。要加入現有組，您可以從[i] Workgroups [/i]菜單部分打開它，然後單擊在組內加入的選項（如適用，某些組是私有的）。

[b]表示讚賞[/b]

Do not forget draw attention to the achievements of your colleagues, employees, and even executives: click 'More' in the message menu and select 'Appreciation', the select who you want to honor publicly and select the appropriate icon under the text of the訊息.

[IMG寬度= 500高= 350] https://www.bitrix24.com/images/images/ustat/en/socnet4.png [/img]

[b]將Bitrix24評估為內部工具[/b]

另外，您可以檢查團隊在Bitrix24中使用的各種工具的學會如何 - 只需使用公司脈衝功能並查看活動即可。

[img width = 500高= 271] https://www.bitrix24.com/images/ustat/en/socnet5.png [/img]

該公司Pulse可幫助您跟踪社交網絡如何整合到日常運營中。每個員工的分數可以與其他同事，他或她的部門或整個公司進行比較。

[b]如果您想了解更多[/b]

如果您想了解有關Intranet功能的更多信息，我們邀請您查看[url = http：//www.bitrixsoft.com/support/support/training/course/index.php？ ]培訓課程[/url]。

[b]現在開始！[/b]

我們現在可以從討論此消息開始 - 發表評論，給我一個“喜歡”  - 我會喜歡的；），不要忘記將此帖子添加到您的收藏夾中;）";
$MESS["INTRANET_USTAT_TELLABOUT_SOCNET_TITLE"] = "勞動力團結";
$MESS["INTRANET_USTAT_TELLABOUT_TASKS_TEXT"] = "仔細查看您的桌面：您確定任務列表旁邊沒有任何數字嗎？ ;）或日記中的任何註釋或日曆中的提醒，還是郵箱中的標誌？您如何避免忘記所有任務並完成所有任務？

今天，我邀請您嘗試一個新工具 -  Bitrix24中的任務。該工具提供了一種快速簡便的方法，可以為自己或同事組織任務。請記住，最重要的是 -  Bitrix24將向任務發送提醒，以確保它們按時完成。

[視頻寬度= 400高= 300] https://www.youtube.com/watch?v=wydi_ezzrb4 [/video]

[img width = 500高= 330] https://www.bitrix24.com/images/images/ustat/en/task1.png [/img]

如果有人仍然不熟悉任務，那麼他們的工作方式是：

[b]我如何為自己或他人創建任務？[/b]

要創建任務，請單擊“添加”>“任務”，然後在出現的窗口中放置了該任務的描述，將其分配給負責人，然後設置截止日期。如有必要，選擇其他參與者並添加文件。保存更改。如果適用，請將任務放入工作組中，以便項目團隊可以查看它。

[IMG寬度= 500高= 359] https://www.bitrix24.com/images/ustat/en/task2.png [/img]

[b]如何使用任務[/b]進行協作

與您的同事討論解決方案和任務詳細信息 - 只需在任務中添加評論，然後將其添加到活動流中 - 其他人可以立即對其進行反應。這比通過電子郵件或耗時的會議討論要容易得多，更快。此外，任務的整個歷史記錄將始終觸手可及：評論和文件存儲在任務中。

[img width = 500高= 398] https://www.bitrix24.com/images/ustat/ustat/en/task3.png [/img]

[b]如何保持任務截止日期[/b]

為了確保截止日期不會錯過，Bitrix24包括一個計數器，該計數器告訴您最需要您最直接關注的內容。它可以在您的個人資料中的“我的任務”和“任務”選項卡中使用。

[img width = 500高= 340] https://www.bitrix24.com/images/ustat/en/task4.png [/img]

計數器顯示不完整的任務：逾期任務，尚未看過的任務以及沒有設定截止日期的任務。嘗試使計數器號盡可能低 - 設置合理的截止日期，在必要時移動它們，並在完成時關閉所有任務。您對任務進行系統化的越多，櫃檯中的數字就越有意義，如果是零 - 您會知道自己做得很好:)

[b]如何跟踪在任務上花費的時間[/b]

監視在任務上花費的時間很容易 - 只需將任務添加到您的日常計劃中，並且當您開始工作時，請適當地移動任務狀態。您還可以在任務表格本身中添加花費的時間。

當您在創建任務時選擇“花費時間”選項時，將跟踪任務的持續時間。

[img width = 500高= 446] https://www.bitrix24.com/images/images/ustat/en/task5.png [/img]

現在，當您在任務上單擊“啟動執行”時，時間跟踪開始。

[img] https://www.bitrix24.com/images/ustat/en/task6.png [/img]

計算時間，直到按下暫停按鈕（或完成任務）。如果您單擊暫停，則時間跟踪停止，您可以休息一下或停止工作日。

[img width = 500高= 399] https://www.bitrix24.com/images/images/ustat/en/task7.png [/img]

[b]如果您想了解更多[/b]

如果您想了解有關此工具的更多詳細信息，我建議[url = http：//www.bitrixsoft.com/support/training/course/course/index.php？course_id = 55＆cypress_id = 04690]在線培訓課程[ /url]這是專門用於Bitrix24雲服務的用戶。

[b]通過任務評估您的工作[/b]

公司Pulse顯示了您和/或您的同事將任務整合到您的日常工作中的狀況：

[img width = 500高= 269] https://www.bitrix24.com/images/images/ustat/en/task8.png [/img]

歡迎您的想法，評論和印象 - 只需對此消息發表評論。

祝您好運 - 不要忘記喜歡此消息:)

P.S.有趣的 - 您在Bitrix24中的第一個任務是什麼？";
$MESS["INTRANET_USTAT_TELLABOUT_TASKS_TITLE"] = "任務和任務！";
