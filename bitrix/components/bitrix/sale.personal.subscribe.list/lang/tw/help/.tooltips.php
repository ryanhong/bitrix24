<?php
$MESS["PATH_TO_CANCEL_TIP"] = "訂閱取消頁面的路徑，包括訂閱ID作為參數。";
$MESS["PER_PAGE_TIP"] = "指定每個頁面訂閱的數量。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將頁面標題設置為<b>我的信用卡</b>。";
