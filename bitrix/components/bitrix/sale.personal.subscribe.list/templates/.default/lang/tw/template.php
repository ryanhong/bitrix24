<?php
$MESS["STPSL_ACTIONS"] = "動作";
$MESS["STPSL_CANCEL"] = "取消訂閱";
$MESS["STPSL_CANCEL1"] = "取消";
$MESS["STPSL_CANCELED"] = "取消";
$MESS["STPSL_DATE_LAST"] = "最後<br />付款<br />日期";
$MESS["STPSL_DATE_NEXT"] = "下一個<br />付款<br />日期";
$MESS["STPSL_LAST_SUCCESS"] = "最後<br>付款<br />成功";
$MESS["STPSL_NO"] = "不";
$MESS["STPSL_PERIOD_BETW"] = "期間<br /> <br />付款之間";
$MESS["STPSL_PERIOD_TYPE"] = "付款<br />期間<br />類型";
$MESS["STPSL_PRODUCT"] = "產品";
$MESS["STPSL_YES"] = "是的";
