<?php
$MESS["INTR_ISIA_PARAM_DETAIL_URL"] = "詳細查看頁面";
$MESS["INTR_ISIN_PARAM_DATE_FORMAT"] = "日期格式";
$MESS["INTR_ISIN_PARAM_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["INTR_ISIN_PARAM_NAME_TEMPLATE"] = "名稱格式";
$MESS["INTR_ISIN_PARAM_NAME_TEMPLATE_DEFAULT"] = "＃NOBR ##名稱＃#last_name ##/nobr＃";
$MESS["INTR_ISIN_PARAM_NUM_USERS"] = "顯示記錄";
$MESS["INTR_ISIN_PARAM_PATH_TO_CONPANY_DEPARTMENT"] = "部門頁面路徑模板";
$MESS["INTR_ISIN_PARAM_PM_URL"] = "個人消息頁面";
$MESS["INTR_ISIN_PARAM_SHOW_LOGIN"] = "顯示登錄名，如果沒有必需的用戶名字段";
$MESS["INTR_ISIN_PARAM_SHOW_YEAR"] = "出生年份";
$MESS["INTR_ISIN_PARAM_SHOW_YEAR_VALUE_M"] = "只有男性";
$MESS["INTR_ISIN_PARAM_SHOW_YEAR_VALUE_N"] = "沒有人";
$MESS["INTR_ISIN_PARAM_SHOW_YEAR_VALUE_Y"] = "全部";
$MESS["INTR_PREDEF_DEPARTMENT"] = "部門/辦公室";
