<?php
$MESS["SONET_ID"] = "用戶身份";
$MESS["SONET_ITEMS_COUNT"] = "列表中的項目計數";
$MESS["SONET_NAME_TEMPLATE"] = "名稱格式";
$MESS["SONET_NAME_TEMPLATE_DEFAULT"] = "＃NOBR ##名稱＃#last_name ##/nobr＃";
$MESS["SONET_PAGE_VAR"] = "頁面變量";
$MESS["SONET_PATH_TO_SEARCH"] = "用戶搜索頁面路徑模板";
$MESS["SONET_PATH_TO_USER"] = "用戶配置文件路徑模板";
$MESS["SONET_SET_NAVCHAIN"] = "設置麵包屑";
$MESS["SONET_SHOW_LOGIN"] = "顯示登錄名，如果沒有必需的用戶名字段";
$MESS["SONET_THUMBNAIL_LIST_SIZE"] = "用戶圖像大小（PX）";
$MESS["SONET_USER_VAR"] = "用戶變量";
$MESS["SONET_VARIABLE_ALIASES"] = "可變別名";
