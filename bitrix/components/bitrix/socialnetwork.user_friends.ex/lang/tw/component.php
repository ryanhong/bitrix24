<?php
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶。";
$MESS["SONET_UFE_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["SONET_UFE_BAN_NAV"] = "用戶";
$MESS["SONET_UFE_FRIENDS_NAV"] = "朋友們";
$MESS["SONET_UFE_NAME_TEMPLATE_DEFAULT"] = "＃NOBR ##名稱＃#last_name ##/nobr＃";
$MESS["SONET_UFE_NO_FR_FUNC"] = "朋友功能被禁用。";
$MESS["SONET_UFE_PAGE_TITLE"] = "朋友們";
