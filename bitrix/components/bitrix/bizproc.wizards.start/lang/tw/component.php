<?php
$MESS["BPCGWTL_INVALID81"] = "名字的字段是必需的。";
$MESS["BPWC_WRC_0_TMPLS"] = "找不到可用的業務流程模板。";
$MESS["BPWC_WRC_ACCESS_ERROR"] = "會議已經過期。請打開<a href='#url#'>摘要頁面</a>，然後重試。";
$MESS["BPWC_WRC_EMPTY_IBLOCK"] = "未指定信息塊ID。";
$MESS["BPWC_WRC_EMPTY_IBLOCK_TYPE"] = "未指定信息塊類型。";
$MESS["BPWC_WRC_ERROR"] = "錯誤";
$MESS["BPWC_WRC_PAGE_NAV_CHAIN"] = "新要求";
$MESS["BPWC_WRC_PAGE_TITLE"] = "＃名稱＃：新請求";
$MESS["BPWC_WRC_WRONG_IBLOCK"] = "找不到組件設置中指定的信息塊。";
$MESS["BPWC_WRC_WRONG_IBLOCK_TYPE"] = "找不到組件參數中指定的信息塊類型。";
$MESS["BPWC_WRC_Z"] = "要求";
