<?php
$MESS["BPWC_WRCT_2LIST"] = "業務流程";
$MESS["BPWC_WRCT_CANCEL"] = "取消";
$MESS["BPWC_WRCT_DESCR"] = "描述";
$MESS["BPWC_WRCT_ERROR"] = "該請求未能運行。";
$MESS["BPWC_WRCT_NAME"] = "姓名";
$MESS["BPWC_WRCT_SAVE"] = "創建請求";
$MESS["BPWC_WRCT_SUBTITLE"] = "創建新的業務流程";
$MESS["BPWC_WRCT_SUCCESS"] = "您的請求已成功創建。";
