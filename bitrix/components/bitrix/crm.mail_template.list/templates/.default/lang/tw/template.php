<?php
$MESS["CRM_ALL"] = "全部的";
$MESS["CRM_MAIL_TEMPLATE_DELETE"] = "刪除電子郵件模板";
$MESS["CRM_MAIL_TEMPLATE_DELETE_CONFIRM_MSGVER_1"] = "您確定要刪除＃標題＃嗎？";
$MESS["CRM_MAIL_TEMPLATE_DELETE_TITLE"] = "刪除此電子郵件模板";
$MESS["CRM_MAIL_TEMPLATE_EDIT"] = "編輯電子郵件模板";
$MESS["CRM_MAIL_TEMPLATE_EDIT_TITLE"] = "編輯此電子郵件模板";
$MESS["CRM_MAIL_TEMPLATE_NEED_FOR_CONVERTING"] = "系統中有傳統的電子郵件模板。您可以<a href= \"#url_execute_converting# \ \">將它們添加到您的列表</a>或<a href= \ \"#url_skip_skip_converting#\"> skip </a>。";
