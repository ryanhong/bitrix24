<?php
$MESS["DOCGEN_SETTINGS_PERMS_ADD_ROLE_TITLE"] = "創造新角色";
$MESS["DOCGEN_SETTINGS_PERMS_EDIT_ROLE_NOT_FOUND"] = "找不到角色。";
$MESS["DOCGEN_SETTINGS_PERMS_EDIT_ROLE_TITLE"] = "編輯角色＃角色＃";
$MESS["DOCGEN_SETTINGS_PERMS_FEATURE_PANEL"] = "CRM實體的員工權限可在商業計劃中獲得。";
$MESS["DOCGEN_SETTINGS_PERMS_FEATURE_TEXT"] = "<div class = \“ docgen-permissions-feature-popup \”>所有員工在您當前的計劃中具有相同的權限。考慮升級到為各種用戶分配不同角色，動作和數據的商業計劃之一。<br /> <br />要了解有關不同計劃的更多信息，請繼續進行<a target = \'_ blank \ 'href = \“/settings/liclandings/lincely_all.php \”>計劃比較頁面</a>。</div>";
$MESS["DOCGEN_SETTINGS_PERMS_FEATURE_TITLE"] = "分配訪問權限";
$MESS["DOCGEN_SETTINGS_PERMS_MODULE_DOCGEN_ERROR"] = "未安裝\“文檔生成器\”模塊。";
$MESS["DOCGEN_SETTINGS_PERMS_PERMISSIONS_ERROR"] = "您無權編輯訪問權限";
$MESS["DOCGEN_SETTINGS_PERMS_TITLE"] = "配置訪問權限";
$MESS["DOCGEN_SETTINGS_PERMS_UNKNOWN_ACCESS_CODE"] = "（未知訪問ID）";
