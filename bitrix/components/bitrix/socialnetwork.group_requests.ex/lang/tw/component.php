<?php
$MESS["SONET_GRE_CANT_DELETE_INVITATION"] = "無法將邀請刪除到用戶ID =＃RELATION_ID＃＃";
$MESS["SONET_GRE_CANT_INVITE"] = "您無權批准該組的會員請求。";
$MESS["SONET_GRE_NAME_TEMPLATE_DEFAULT"] = "＃NOBR ##名稱＃#last_name ##/nobr＃";
$MESS["SONET_GRE_NAV"] = "用戶";
$MESS["SONET_GRE_NOT_SELECTED"] = "沒有選擇用戶。";
$MESS["SONET_GRE_NO_GROUP"] = "找不到工作組。";
$MESS["SONET_GRE_NO_PERMS"] = "您無權查看此組。";
$MESS["SONET_GRE_TITLE"] = "工作組會員邀請和請求";
$MESS["SONET_GRE_TITLE_IN"] = "工作組會員要求";
$MESS["SONET_GRE_TITLE_IN_PROJECT"] = "項目會員資格請求";
$MESS["SONET_GRE_TITLE_OUT"] = "工作組會員邀請函";
$MESS["SONET_GRE_TITLE_OUT_PROJECT"] = "項目會員邀請函";
$MESS["SONET_GRE_TITLE_PROJECT"] = "項目成員邀請和請求";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶。";
