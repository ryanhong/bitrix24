<?php
$MESS["LHE_GROUP_ADDITIONAL_SETTINGS"] = "其他設置";
$MESS["LHE_GROUP_BASE_SETTINGS"] = "主要設置";
$MESS["LHE_GROUP_VIDEO_SETTINGS"] = "視頻設置";
$MESS["LHE_PAR_ADVANCED_MODE"] = "啟用高級設置";
$MESS["LHE_PAR_AUTO_RESIZE"] = "自動化（放大）編輯區以適合文本";
$MESS["LHE_PAR_CONTENT"] = "HTML文本";
$MESS["LHE_PAR_HEIGHT"] = "編輯高度";
$MESS["LHE_PAR_ID"] = "控制ID";
$MESS["LHE_PAR_INPUT_ID"] = "控制ID";
$MESS["LHE_PAR_INPUT_NAME"] = "控制名稱";
$MESS["LHE_PAR_JS_OBJ_NAME"] = "JavaScript對象名稱";
$MESS["LHE_PAR_RESIZABLE"] = "可分解的編輯區";
$MESS["LHE_PAR_USE_FILE_DIALOGS"] = "啟用文件對話框";
$MESS["LHE_PAR_VIDEO_ALLOW_VIDEO"] = "允許視頻嵌入";
$MESS["LHE_PAR_VIDEO_BUFFER"] = "播放器緩衝長度（秒）";
$MESS["LHE_PAR_VIDEO_LOGO"] = "徽標圖像文件路徑";
$MESS["LHE_PAR_VIDEO_MAX_HEIGHT"] = "最大限度。視頻高度";
$MESS["LHE_PAR_VIDEO_MAX_WIDTH"] = "最大限度。視頻寬度";
$MESS["LHE_PAR_VIDEO_ONLY_FLV"] = "（僅適用於Flash Player）";
$MESS["LHE_PAR_VIDEO_ONLY_WMV"] = "（僅適用於Silverlight Player）";
$MESS["LHE_PAR_VIDEO_SKIN"] = "視頻皮膚路徑";
$MESS["LHE_PAR_VIDEO_WINDOWLESS"] = "窗口模式（無窗）";
$MESS["LHE_PAR_VIDEO_WMODE"] = "窗口模式（WMODE）";
$MESS["LHE_PAR_WIDTH"] = "編輯寬度";
$MESS["LHE_PAR_WMODE_OPAQUE"] = "不透明";
$MESS["LHE_PAR_WMODE_TRANSPARENT"] = "透明的";
$MESS["LHE_PAR_WMODE_WINDOW"] = "普通的";
