<?php
$MESS["CRM_TYPE_COMPANY"] = "公司";
$MESS["CRM_TYPE_CONTACT"] = "接觸";
$MESS["CRM_TYPE_CREATION_SCENARIO"] = "交易創建模式";
$MESS["CRM_TYPE_CREATION_SCENARIO_DESC"] = "每個新的傳入查詢將被保存為一項新交易。聯繫或公司也將自動創建，具體取決於您的喜好。您還可以選擇要添加這些新交易的管道。";
$MESS["CRM_TYPE_DEAL_CREATE_AND"] = "創建交易和...";
$MESS["CRM_TYPE_DEAL_DIRECTION"] = "交易管道";
$MESS["CRM_TYPE_NOT_CLOSE_DEAL"] = "轉換後不要關閉活動";
$MESS["CRM_TYPE_SETTINGS"] = "設定";
