<?php
$MESS["CRM_DEAL_SETTINGS"] = "您可以配置＃link_start＃交易創建模式＃link_end＃";
$MESS["CRM_RIGTHS_INFO"] = "請聯繫您的管理員以更改CRM模式";
$MESS["CRM_TYPE_CANCEL"] = "取消";
$MESS["CRM_TYPE_CHANGE"] = "您可以在CRM設置中的任何時間更改偏好";
$MESS["CRM_TYPE_CLASSIC"] = "經典CRM";
$MESS["CRM_TYPE_CLASSIC_DESC"] = "Leads＆Rarr;交易 +聯繫人";
$MESS["CRM_TYPE_CLASSIC_DESC2"] = "推薦用於中型和大型企業。首先，所有入站查詢都成為線索。比您的銷售團隊試圖將潛在客戶轉換為交易和聯繫人。";
$MESS["CRM_TYPE_MORE"] = "了解更多";
$MESS["CRM_TYPE_POPUP_TITLE"] = "潛在客戶是任何可能成為客戶的人或任何東西。<br/>有兩種與Bitrix24 CRM合作的方法：";
$MESS["CRM_TYPE_POPUP_TITLE_LEADS_DISABLED"] = "您當前正在使用\“簡單CRM \”模式，其中<br/>新客戶請求立即成為交易。<br/>可能有兩個選項：";
$MESS["CRM_TYPE_SAVE"] = "節省";
$MESS["CRM_TYPE_SIMPLE"] = "簡單的CRM";
$MESS["CRM_TYPE_SIMPLE_DESC"] = "交易 +聯繫人（無潛在客戶）";
$MESS["CRM_TYPE_SIMPLE_DESC2"] = "建議用於小型企業的銷售部門。所有入站電子郵件，呼叫，請求和聊天都立即成為交易和/或聯繫人。";
$MESS["CRM_TYPE_TITLE"] = "選擇您想與CRM一起工作的方式";
