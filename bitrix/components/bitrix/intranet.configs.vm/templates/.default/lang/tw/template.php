<?php
$MESS["CONFIG_VM_BXENV_UPDATE"] = "虛擬設備需要更新。";
$MESS["CONFIG_VM_BXENV_UPDATE_ARTICLE_ID"] = "6147609";
$MESS["CONFIG_VM_CERTIFICATE_LETS_ENCRYPT_CONF"] = "配置讓我們加密證書";
$MESS["CONFIG_VM_CERTIFICATE_LETS_ENCRYPT_DNS"] = "此證書的有效DNS名稱";
$MESS["CONFIG_VM_CERTIFICATE_LETS_ENCRYPT_EMAIL"] = "通知電子郵件地址";
$MESS["CONFIG_VM_CERTIFICATE_PROCESS"] = "該證書現在正在配置。這可能需要一些時間。";
$MESS["CONFIG_VM_CERTIFICATE_SELF_CONF"] = "配置SSL證書";
$MESS["CONFIG_VM_CERTIFICATE_SELF_KEY_PATH"] = "私鑰路徑";
$MESS["CONFIG_VM_CERTIFICATE_SELF_PATH"] = "證書路徑";
$MESS["CONFIG_VM_CERTIFICATE_SELF_PATH_CHAIN"] = "證書鏈路徑";
$MESS["CONFIG_VM_CERTIFICATE_TITLE"] = "證書設置";
$MESS["CONFIG_VM_CERTIFICATE_TYPE"] = "證書類別";
$MESS["CONFIG_VM_HEADER_SETTINGS"] = "設定";
$MESS["CONFIG_VM_SAVE"] = "節省";
$MESS["CONFIG_VM_SAVE_SUCCESSFULLY"] = "設置已成功更新";
$MESS["CONFIG_VM_SMTP_AUTH"] = "使用身份驗證";
$MESS["CONFIG_VM_SMTP_EMAIL"] = "電子郵件地址";
$MESS["CONFIG_VM_SMTP_ERROR"] = "電子郵件配置錯誤";
$MESS["CONFIG_VM_SMTP_HOST"] = "SMTP服務器";
$MESS["CONFIG_VM_SMTP_PASSWORD"] = "用戶密碼";
$MESS["CONFIG_VM_SMTP_PORT"] = "SMTP端口";
$MESS["CONFIG_VM_SMTP_REPEAT_PASSWORD"] = "確認密碼";
$MESS["CONFIG_VM_SMTP_TITLE"] = "電子郵件配置";
$MESS["CONFIG_VM_SMTP_TLS"] = "使用TLS";
$MESS["CONFIG_VM_SMTP_USER"] = "SMTP用戶登錄";
$MESS["CONFIG_VM_START"] = "開始";
$MESS["CONFIG_VM_UPLOAD"] = "下載";
