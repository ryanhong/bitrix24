<?php
$MESS["SONET_C10_DO_SAVE"] = "統一";
$MESS["SONET_C10_DO_SET"] = "分配主持人";
$MESS["SONET_C10_NO_MODS"] = "沒有主持人。";
$MESS["SONET_C10_NO_MODS_DESCR"] = "組主持人。";
$MESS["SONET_C10_ONLINE"] = "在線的";
$MESS["SONET_C10_OWNER"] = "所有者";
$MESS["SONET_C10_SUBTITLE"] = "組主持人";
$MESS["SONET_C10_T_OWNER"] = "更改所有者";
