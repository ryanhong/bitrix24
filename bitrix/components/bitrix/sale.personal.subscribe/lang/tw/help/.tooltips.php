<?php
$MESS["PER_PAGE_TIP"] = "指定每個頁面訂閱的數量。";
$MESS["SEF_FOLDER_TIP"] = "指定組件工作文件夾。該文件夾可以由真實的文件系統路徑表示，也可以是虛擬的。";
$MESS["SEF_MODE_TIP"] = "檢查此選項啟用SEF模式和URL配置字段。";
$MESS["SEF_URL_TEMPLATES_cancel_TIP"] = "指定訂閱取消頁面URL的段。 URL應包括訂閱ID。";
$MESS["SEF_URL_TEMPLATES_list_TIP"] = "指定訂閱頁面URL的段。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將頁面標題設置為<b>重複付款</b>。";
