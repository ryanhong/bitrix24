<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>不緩存</i>：沒有執行緩存。";
$MESS["PAGE_TIP"] = "相對於站點root（＃site_dir＃），指定訂閱編輯器頁面的路徑。例如：<b>＃site_dir＃關於/subscr_edit.php </b>";
$MESS["SHOW_HIDDEN_TIP"] = "指定在訂閱編輯器中顯示所有活動的主題。隱藏的標題具有在公共訂閱中顯示的選項</b>不活動。";
$MESS["USE_PERSONALIZATION_TIP"] = "指定個性化當前用戶的訂閱。";
