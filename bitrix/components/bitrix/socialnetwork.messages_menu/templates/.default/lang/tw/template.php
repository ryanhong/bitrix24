<?php
$MESS["SONET_UM_BIZPROC"] = "業務流程";
$MESS["SONET_UM_INPUT"] = "傳入";
$MESS["SONET_UM_LOG"] = "活動流";
$MESS["SONET_UM_MUSERS"] = "消息";
$MESS["SONET_UM_OUTPUT"] = "向外";
$MESS["SONET_UM_SUBSCRIBE"] = "訂閱";
$MESS["SONET_UM_TASKS"] = "任務";
$MESS["SONET_UM_USER"] = "我的簡歷";
$MESS["SONET_UM_USER_BAN"] = "禁令列表";
