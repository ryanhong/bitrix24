<?php
$MESS["SONET_PAGE_ID"] = "頁面ID";
$MESS["SONET_PAGE_VAR"] = "頁面變量";
$MESS["SONET_PATH_TO_LOG"] = "更新日誌頁面路徑模板";
$MESS["SONET_PATH_TO_MESSAGES_INPUT"] = "傳入消息頁面路徑模板";
$MESS["SONET_PATH_TO_MESSAGES_OUTPUT"] = "傳出消息頁路徑模板";
$MESS["SONET_PATH_TO_MESSAGES_USERS"] = "消息傳遞頁面路徑模板";
$MESS["SONET_PATH_TO_SUBSCRIBE"] = "訂閱頁面路徑模板";
$MESS["SONET_PATH_TO_TASKS"] = "任務頁網址模板";
$MESS["SONET_PATH_TO_USER"] = "用戶配置文件路徑模板";
$MESS["SONET_PATH_TO_USER_BAN"] = "禁令列表頁面路徑模板";
$MESS["SONET_USER_VAR"] = "用戶變量";
