<?php
$MESS["CRM_TYPE_DETAIL_ADDITIONAL_SECTION_TITLE"] = "其他選項";
$MESS["CRM_TYPE_DETAIL_AUTOMATION_CARD_DESCRIPTION"] = "使用自動化規則和触發器（類似於運行自動化規則和交易或潛在客戶觸發器）來自動化智能過程自動化項目。";
$MESS["CRM_TYPE_DETAIL_AUTOMATION_CARD_TITLE"] = "自動化規則和触發器";
$MESS["CRM_TYPE_DETAIL_BIZ_PROC_CARD_DESCRIPTION"] = "使用功能全面的工作流設計器。在創建或修改此水療中心的項目時，配置工作流模板以運行。";
$MESS["CRM_TYPE_DETAIL_BIZ_PROC_CARD_TITLE"] = "工作流設計師";
$MESS["CRM_TYPE_DETAIL_CATEGORIES_CARD_DESCRIPTION"] = "您可以為此智能流程自動化（例如使用交易管道）創建多個管道，並使用銷售渠道在各種管道上移動項目。每個管道都有單個階段，詳細信息和訪問權限。";
$MESS["CRM_TYPE_DETAIL_CATEGORIES_CARD_TITLE"] = "管道和銷售渠道";
$MESS["CRM_TYPE_DETAIL_CATEGORIES_DISABLED_HINT"] = "有多個管道。";
$MESS["CRM_TYPE_DETAIL_CONVERSION_DESTINATION"] = "目的地類型";
$MESS["CRM_TYPE_DETAIL_CONVERSION_SOURCE"] = "源類型";
$MESS["CRM_TYPE_DETAIL_CUSTOM_SECTION_HINT_ABOUT_CUSTOM_SECTIONS_IN_CRM"] = "您目前正在瀏覽CRM區域。要在不同的自動化解決方案中創建水療中心，請使用\“ Automation \”區域。";
$MESS["CRM_TYPE_DETAIL_CUSTOM_SECTION_LABEL_MSGVER_1"] = "選擇或創建自動解決方案";
$MESS["CRM_TYPE_DETAIL_CUSTOM_SECTION_LIST_MSGVER_1"] = "自動解決方案";
$MESS["CRM_TYPE_DETAIL_CUSTOM_SECTION_SWITCHER_MSGVER_1"] = "將水療中心添加到自動解決方案";
$MESS["CRM_TYPE_DETAIL_DELETE_CONFIRM"] = "確定您想刪除此智能過程自動化嗎？";
$MESS["CRM_TYPE_DETAIL_FIELDS_SECTION_TITLE"] = "水療中心";
$MESS["CRM_TYPE_DETAIL_FIELD_CLIENT"] = "\“顧客\”";
$MESS["CRM_TYPE_DETAIL_FIELD_DATES"] = "\“開始date \” \“結束日期\”";
$MESS["CRM_TYPE_DETAIL_FIELD_MY_COMPANY"] = "\“公司詳細信息\”";
$MESS["CRM_TYPE_DETAIL_FIELD_OBSERVERS"] = "\“觀察者\”";
$MESS["CRM_TYPE_DETAIL_FIELD_SOURCE"] = "\“來源\”";
$MESS["CRM_TYPE_DETAIL_FIELD_TITLE"] = "輸入名字";
$MESS["CRM_TYPE_DETAIL_HIDE_DESCRIPTION"] = "隱藏通知";
$MESS["CRM_TYPE_DETAIL_IS_SET_OPEN_PERMISSIONS_DESCRIPTION"] = "創建新管道時，將授予所有角色訪問此新管道。";
$MESS["CRM_TYPE_DETAIL_IS_SET_OPEN_PERMISSIONS_TITLE"] = "管道的默認訪問權限";
$MESS["CRM_TYPE_DETAIL_IS_USE_IN_USERFIELD_ENABLED_TITLE"] = "CRM實體";
$MESS["CRM_TYPE_DETAIL_RECYCLE_DISABLED_HINT"] = "斷開連接之前，從回收箱中刪除此類型的項目。";
$MESS["CRM_TYPE_DETAIL_RELATION_CARD_DESCRIPTION_MSGVER_2"] = "將水療中心鏈接到任何CRM項目（鉛，交易，估計）或其他水療中心。當水療中心鏈接到交易時，交易視圖表將顯示水療項目列表。同樣，水療項目視圖表格將顯示交易。";
$MESS["CRM_TYPE_DETAIL_RELATION_CARD_TITLE"] = "與CRM項目結合";
$MESS["CRM_TYPE_DETAIL_RELATION_CHILD"] = "將此水療中心鏈接到其他CRM實體";
$MESS["CRM_TYPE_DETAIL_RELATION_CHILDREN_LIST"] = "將鏈接項目列表添加到詳細信息表格";
$MESS["CRM_TYPE_DETAIL_RELATION_CHILD_ITEMS"] = "將此水療中心鏈接到這些CRM項目：";
$MESS["CRM_TYPE_DETAIL_RELATION_PARENT"] = "與此水療中心結合";
$MESS["CRM_TYPE_DETAIL_RELATION_PARENT_ITEMS"] = "選擇要鏈接到此水療中心的CRM實體：";
$MESS["CRM_TYPE_DETAIL_STAGES_CARD_DESCRIPTION"] = "在智能過程自動化中創建自己的階段，並控制看板上這些階段的項目。每個階段都可以具有單獨的自動化規則，觸發和訪問權限。總是可以在各自的階段找到項目；禁用項目只會將其隱藏在用戶界面中。";
$MESS["CRM_TYPE_DETAIL_STAGES_CARD_TITLE"] = "階段和看板";
$MESS["CRM_TYPE_DETAIL_STAGES_DISABLED_HINT"] = "階段將在用戶界面中不可見。";
$MESS["CRM_TYPE_DETAIL_TAB_COMMON"] = "溫泉組件";
$MESS["CRM_TYPE_DETAIL_TAB_CONVERSION"] = "轉換";
$MESS["CRM_TYPE_DETAIL_TAB_CUSTOM_SECTION_CARD_DESCRIPTION_MSGVER_1"] = "您可以在自動解決方案中添加水療中心。您將在左菜單中找到自動解決方案。";
$MESS["CRM_TYPE_DETAIL_TAB_CUSTOM_SECTION_CARD_TITLE"] = "將此水療中心添加到左菜單";
$MESS["CRM_TYPE_DETAIL_TAB_CUSTOM_SECTION_MSGVER_1"] = "自動解決方案";
$MESS["CRM_TYPE_DETAIL_TAB_FIELDS_MSGVER_1"] = "水療形式佈局";
$MESS["CRM_TYPE_DETAIL_TAB_RELATIONS"] = "CRM結合";
$MESS["CRM_TYPE_DETAIL_TAB_USER_FIELDS"] = "與其他工具結合";
$MESS["CRM_TYPE_DETAIL_TAB_USER_FIELDS_CARD_DESCRIPTION"] = "您可以將此水療中心鏈接到其他CRM實體，日曆，任務或任務模板。這將允許將水療中心的項目添加到CRM交易，日曆事件，任務和任務模板中。重要的是：您還必須更改現場設置以顯示現場的水療項目。";
$MESS["CRM_TYPE_DETAIL_TAB_USER_FIELDS_CARD_TITLE"] = "其他綁定";
$MESS["CRM_TYPE_DETAIL_TAB_USER_FIELDS_TITLE"] = "與其他工具結合";
$MESS["CRM_TYPE_DETAIL_TITLE"] = "智能過程自動化";
$MESS["CRM_TYPE_DETAIL_TITLE_PRESET"] = "用例";
