<?php
$MESS["BX24_INVITE_DIALOG_ADDED"] = "<b>恭喜！</b> <br> <br>已將註冊通知發送給指定的用戶。<br> <br>查看邀請誰被邀請，請<a style = \'whiterpace：nowrap ; \“ href = \”＃site_dir＃company/？show_user = intactive \“>打開員工列表</a>。";
$MESS["BX24_INVITE_DIALOG_ERROR_EMAIL"] = "錯誤的電子郵件格式";
$MESS["BX24_INVITE_DIALOG_ERROR_EMAIL_OR_PHONE"] = "錯誤的電子郵件或電話號碼格式";
$MESS["BX24_INVITE_DIALOG_ERROR_EXTRANET_NO_SONET_GROUP_ADD"] = "請選擇一個將外部用戶添加到的組";
$MESS["BX24_INVITE_DIALOG_ERROR_EXTRANET_NO_SONET_GROUP_INVITE"] = "請選擇一個邀請外部用戶的組";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR"] = "<b>恭喜！</b> <br> <br>加入您的Bitrix24的邀請已發送到Bitrix24合作夥伴的電子郵件地址。";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_COUNT_ERROR"] = "Bitrix24合作夥伴的最大數量超過了。您可以刪除現有的＃link_start＃bitrix24合作夥伴＃link_end＃並邀請新的＃。";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_EMAIL_ERROR"] = "指定的電子郵件沒有Bitrix24合作夥伴";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_INVITE_TEXT"] = "我邀請您加入我們的Bitrix24！您將獲得完整的BitRix24合作夥伴權限，以幫助我們設置和配置Bitrix24帳戶。這些是完整的訪問權限，除非您無法添加或刪除系統管理員。";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>恭喜！</b> <br> <br> bitrix24邀請已發送到選定的電子郵件地址。<br> <br>查看邀請誰被邀請，請<a style = \'whiterap：nowrap ; \“ href = \”＃site_dir＃company/？show_user = intactive \“>打開員工列表</a>。";
$MESS["BX24_INVITE_DIALOG_INVITE_PHONE"] = "<b> <b>恭喜！</b> <br> <br> Bitrix24邀請已發送到選定的數字。<br> <br>要查看邀請誰被邀請，請<a style = \'white = \“白色空間：nowrap; \ \“ href = \”＃site_dir＃company/？show_user = intactive \“> intairive \”>打開員工列表</a>。";
$MESS["BX24_INVITE_DIALOG_SELF"] = "<b>恭喜！</b> <br> <br>已更新快速註冊設置。";
$MESS["BX24_INVITE_DIALOG_SELF_SUCCESS"] = "快速註冊設置已更新。";
$MESS["BX24_INVITE_DIALOG_WARNING_CREATE_MAILBOX_ERROR"] = "錯誤創建新郵箱：";
$MESS["BX24_INVITE_DIALOG_WARNING_MAILBOX_PASSWORD_CONFIRM"] = "您的密碼和確認密碼不匹配";
$MESS["INTRANET_INVITE_DIALOG_ERROR_LENGTH"] = "最大長度超過50個字符";
$MESS["INTRANET_INVITE_DIALOG_USER_COUNT_ERROR"] = "超過每日邀請用戶的最大最大";
