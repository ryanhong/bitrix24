<?php
$MESS["CRM_LEAD_EDIT_EVENT_CANCELED"] = "動作已取消。現在，您被重定向到上一頁。如果您仍在此頁面上，請手動關閉它。";
$MESS["CRM_LEAD_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "鉛<a href='#url#'>＃標題＃</a>已被創建。您正在重定向到初始頁面。如果此頁面仍在顯示，請手動關閉它。";
