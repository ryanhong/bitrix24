<?php
$MESS["BFDND_ACCESS_DENIED"] = "拒絕訪問";
$MESS["BFDND_DROPHERE"] = "在此處拖放一個或多個文件";
$MESS["BFDND_FILES"] = "文件：";
$MESS["BFDND_FILE_EXISTS"] = "這個名稱的文件已經存在。您仍然可以使用當前文件夾，在這種情況下，該文檔的現有版本將保存在歷史記錄中。";
$MESS["BFDND_FILE_LOADING"] = "載入中";
$MESS["BFDND_SELECT_EXIST"] = "或從計算機中選擇文件";
$MESS["BFDND_SELECT_LOCAL"] = "上傳文件";
$MESS["BFDND_UPLOAD_ERROR"] = "錯誤保存文件。";
$MESS["BFDND_UPLOAD_FILES"] = "上傳文件";
$MESS["BFDND_UPLOAD_IMAGES"] = "上傳圖像";
