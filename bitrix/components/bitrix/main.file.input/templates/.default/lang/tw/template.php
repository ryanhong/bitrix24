<?php
$MESS["MFI_CONFIRM"] = "刪除文件？";
$MESS["MFI_INPUT_CAPTION_ADD"] = "添加文件";
$MESS["MFI_INPUT_CAPTION_ADD_IMAGE"] = "添加圖片";
$MESS["MFI_INPUT_CAPTION_REPLACE"] = "替換文件";
$MESS["MFI_INPUT_CAPTION_REPLACE_IMAGE"] = "更換圖像";
$MESS["MFI_NOTICE_1"] = "選擇一個使用＃Ext＃擴展名的文件。文件大小不得超過＃大小＃。";
$MESS["MFI_NOTICE_2"] = "選擇一個使用＃Ext＃擴展名的文件。";
$MESS["MFI_NOTICE_3"] = "文件大小不得超過＃大小＃。";
$MESS["MFI_UPLOADING_ERROR"] = "文件上傳錯誤。";
