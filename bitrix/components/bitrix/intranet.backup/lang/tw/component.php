<?php
$MESS["BACKUP_ACTION_DELETE"] = "刪除";
$MESS["BACKUP_ACTION_DOWNLOAD"] = "下載";
$MESS["BACKUP_ACTION_LINK"] = "獲取備份鏈接";
$MESS["BACKUP_ACTION_RENAME"] = "改名";
$MESS["BACKUP_ACTION_RESTORE"] = "恢復";
$MESS["DUMP_DELETE_ERROR"] = "無法刪除文件＃文件＃";
$MESS["MAIN_DUMP_ERROR"] = "錯誤";
$MESS["MAIN_DUMP_ERR_COPY_FILE"] = "錯誤！無法複製文件：";
$MESS["MAIN_DUMP_ERR_FILE_RENAME"] = "文件重命名錯誤：";
$MESS["MAIN_DUMP_ERR_NAME"] = "文件名只能包括拉丁字符，數字，連字符和周期。";
$MESS["MAIN_DUMP_LOCAL"] = "本地";
$MESS["MAIN_DUMP_PARTS"] = "部分：";
$MESS["MAIN_DUMP_USE_THIS_LINK"] = "使用此鏈接使用此鏈接遷移到另一台服務器";
