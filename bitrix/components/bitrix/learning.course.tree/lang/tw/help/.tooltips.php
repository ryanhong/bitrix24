<?php
$MESS["CHAPTER_DETAIL_TEMPLATE_TIP"] = "課程章節頁面的路徑。";
$MESS["CHECK_PERMISSIONS_TIP"] = "如果要檢查該課程的用戶訪問權限，請在此處選擇\“是\”。";
$MESS["COURSE_DETAIL_TEMPLATE_TIP"] = "課程詳細信息頁面的路徑。";
$MESS["COURSE_ID_TIP"] = "在此處選擇現有課程之一。如果選擇<b> <i>（其他）</i> </b>，則必須在旁邊的字段中指定課程ID。";
$MESS["LESSON_DETAIL_TEMPLATE_TIP"] = "課程課程頁面的路徑。";
$MESS["SELF_TEST_TEMPLATE_TIP"] = "自助檢查測試頁面的路徑。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將將頁面標題設置為課程章節名稱。";
$MESS["TESTS_LIST_TEMPLATE_TIP"] = "顯示課程所有測試的頁面的路徑。";
$MESS["TEST_DETAIL_TEMPLATE_TIP"] = "通往主測試頁面的路徑。";
