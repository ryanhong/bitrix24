<?php
$MESS["FORUM_MODULE_NOT_INSTALLED"] = "未安裝論壇模塊。";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "信息塊模塊未安裝。";
$MESS["IBLOCK_NOT_ASSIGNED"] = "沒有選擇信息塊。";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_FORUM_LOG_TEMPLATE_AUTHOR"] = "

作者：[url =＃url＃]＃標題＃[/url]。";
$MESS["SONET_FORUM_LOG_TEMPLATE_GUEST"] = "

作者：＃標題＃。";
$MESS["WIKI_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["WIKI_DISCUSSION_DISABLED"] = "討論被禁用。";
$MESS["WIKI_MODULE_NOT_INSTALLED"] = "未安裝Wiki模塊。";
$MESS["WIKI_SOCNET_INITIALIZING_FAILED"] = "社交網絡無法初始化。";
$MESS["WIKI_SONET_IM_COMMENT"] = "在您的Wiki文章中添加了評論\“＃標題＃\”";
$MESS["WIKI_SONET_LOG_TITLE_TEMPLATE"] = "文章\“＃標題＃\”添加或更新";
