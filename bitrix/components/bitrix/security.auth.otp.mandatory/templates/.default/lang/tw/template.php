<?php
$MESS["SECURITY_OTP_MANDATORY_AUTH_APP_EXECUTE"] = "運行應用程序";
$MESS["SECURITY_OTP_MANDATORY_AUTH_APP_EXECUTE2"] = "然後單擊<strong>配置</strong>按鈕";
$MESS["SECURITY_OTP_MANDATORY_AUTH_APP_EXECUTE_TMP"] = "然後單擊新帳戶按鈕";
$MESS["SECURITY_OTP_MANDATORY_AUTH_BACK"] = "返回登錄屏幕";
$MESS["SECURITY_OTP_MANDATORY_AUTH_CHOOSE_TYPE"] = "選擇首選方法以接收驗證代碼";
$MESS["SECURITY_OTP_MANDATORY_AUTH_CODE_DESCR"] = "一旦代碼成功掃描或手動輸入，您的手機將顯示您必須在下面輸入的代碼。";
$MESS["SECURITY_OTP_MANDATORY_AUTH_CODE_DESCR2"] = "要完成初始化，請在移動應用程序中單擊\“獲取新代碼\”，然後輸入您在移動屏幕上看到的另一個代碼。";
$MESS["SECURITY_OTP_MANDATORY_AUTH_CODE_INFO_HOTP"] = "基於計數器";
$MESS["SECURITY_OTP_MANDATORY_AUTH_CODE_INFO_TOTP"] = "基於時間";
$MESS["SECURITY_OTP_MANDATORY_AUTH_CONNECT"] = "立即啟用兩步身份驗證";
$MESS["SECURITY_OTP_MANDATORY_AUTH_DESCR1"] = "您尚未在指定係統管理員的一段時間內啟用兩步身份驗證。如果沒有一次性代碼，您將無法登錄。
<br /> <br />
要立即啟用兩步身份驗證並獲取代碼，請按照以下步驟操作。
<br /> <br />
如果您無法安裝代碼採集應用程序或難以實現兩步身份驗證，請聯繫您的管理員。";
$MESS["SECURITY_OTP_MANDATORY_AUTH_DONE"] = "準備好";
$MESS["SECURITY_OTP_MANDATORY_AUTH_ENTER_CODE"] = "輸入驗證碼";
$MESS["SECURITY_OTP_MANDATORY_AUTH_ENTER_CODE_PL"] = "輸入代碼";
$MESS["SECURITY_OTP_MANDATORY_AUTH_ENTER_CODE_PL1"] = "第一個代碼";
$MESS["SECURITY_OTP_MANDATORY_AUTH_ENTER_CODE_PL2"] = "第二個代碼";
$MESS["SECURITY_OTP_MANDATORY_AUTH_ERROR_TITLE"] = "由於發生錯誤而無法保存。";
$MESS["SECURITY_OTP_MANDATORY_AUTH_HAND_DESCR"] = "如果您無法掃描代碼，請手動輸入它。
<br />您必須指定網站（或Bitrix24）地址，您的電子郵件，驗證字，然後選擇鍵類型。";
$MESS["SECURITY_OTP_MANDATORY_AUTH_HAND_TYPE"] = "手動輸入代碼";
$MESS["SECURITY_OTP_MANDATORY_AUTH_INPUT_METHODS_SEPARATOR"] = "或者";
$MESS["SECURITY_OTP_MANDATORY_AUTH_MOBILE"] = "下載Bitrix24 OTP應用程序";
$MESS["SECURITY_OTP_MANDATORY_AUTH_MOBILE2"] = "為您的手機或GooglePlay的手機";
$MESS["SECURITY_OTP_MANDATORY_AUTH_MOBILE2_TMP"] = "對於您的手機，AppStore，GooglePlay";
$MESS["SECURITY_OTP_MANDATORY_AUTH_MOBILE_TMP"] = "下載FreeOTP或Google Authenticator";
$MESS["SECURITY_OTP_MANDATORY_AUTH_SCAN_CODE"] = "掃描二維碼";
$MESS["SECURITY_OTP_MANDATORY_AUTH_SCAN_DESCR"] = "要掃描代碼，請將手機的相機帶到屏幕上，然後等到應用程序掃描代碼。";
$MESS["SECURITY_OTP_MANDATORY_AUTH_UNKNOWN_ERROR"] = "意外的錯誤。請稍後再試。";
$MESS["SECURITY_OTP_MANDATORY_REQUIRED"] = "如果沒有一次性密碼，您將無法登錄。";
