<?php
$MESS["SECURITY_AUTH_OTP_MANDATORY_AUTH_ERROR"] = "您已經登錄。";
$MESS["SECURITY_AUTH_OTP_MANDATORY_MODULE_ERROR"] = "沒有安裝安全模塊。";
$MESS["SECURITY_AUTH_OTP_MANDATORY_NOT_REQUIRED"] = "尚未到達啟用兩步身份驗證的截止日期。";
$MESS["SECURITY_AUTH_OTP_MANDATORY_UNKNOWN_ERROR"] = "意外的錯誤。請稍後再試。";
