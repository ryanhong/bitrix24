<?php
$MESS["HISTORIC_STATUSES_TIP"] = "可用於過濾歷史記錄的狀態。如果\“ filter_history \”參數不是y，而是在這些狀態之一中的訂單，則將被拒絕並且未在搜索結果中顯示。";
$MESS["ORDERS_PER_PAGE_TIP"] = "指定每個頁面的訂單數。其他訂單可以通過麵包屑導航獲得。";
$MESS["PATH_TO_BASKET_TIP"] = "您可以使用<b>購物車</b>組件創建此頁面。客戶通過單擊訂單列表頁面中的重複</b>打開此頁面。";
$MESS["PATH_TO_PAYMENT_TIP"] = "您可以使用<b>支付系統集成</b>組件創建此頁面。客戶通過單擊訂單詳細信息頁面中的重複付款來打開此頁面。";
$MESS["SEF_FOLDER_TIP"] = "指定組件工作文件夾。該文件夾可以由真實的文件系統路徑表示，也可以是虛擬的。";
$MESS["SEF_MODE_TIP"] = "檢查此選項以啟用SEF模式和URL配置字段。";
$MESS["SEF_URL_TEMPLATES_cancel_TIP"] = "指定訂單取消頁面的URL段。例如：<b>取消/＃ID＃/</b>";
$MESS["SEF_URL_TEMPLATES_detail_TIP"] = "指定訂單詳細信息頁面的URL段。例如：<b>詳細信息/＃id＃/</b>";
$MESS["SEF_URL_TEMPLATES_list_TIP"] = "指定客戶訂單頁面的URL段。例如：<b> list/</b>";
$MESS["SET_TITLE_TIP"] = "檢查此選項將頁面標題設置為<b>我的訂單</b>。";
