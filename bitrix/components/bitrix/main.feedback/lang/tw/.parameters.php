<?php
$MESS["MFP_ALL_REQ"] = "（全部不付款）";
$MESS["MFP_CAPTCHA"] = "使用驗證碼進行未經授權的訪客";
$MESS["MFP_EMAIL_TEMPLATES"] = "電子郵件模板";
$MESS["MFP_EMAIL_TO"] = "目的地電子郵件地址";
$MESS["MFP_MESSAGE"] = "訊息";
$MESS["MFP_NAME"] = "姓名";
$MESS["MFP_OK_MESSAGE"] = "發送時要顯示的文字";
$MESS["MFP_OK_TEXT"] = "謝謝。您的消息現在正在處理。";
$MESS["MFP_REQUIRED_FIELDS"] = "必需的字段";
