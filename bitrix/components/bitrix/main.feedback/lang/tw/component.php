<?php
$MESS["MF_CAPTCHA_WRONG"] = "您輸入的驗證碼代碼不正確。";
$MESS["MF_CAPTHCA_EMPTY"] = "需要驗證碼代碼。";
$MESS["MF_EMAIL_NOT_VALID"] = "指定的電子郵件地址無效。";
$MESS["MF_OK_MESSAGE"] = "謝謝。您的消息現在正在處理。";
$MESS["MF_REQ_EMAIL"] = "提供回復到電子郵件地址。";
$MESS["MF_REQ_MESSAGE"] = "需要消息文本。";
$MESS["MF_REQ_NAME"] = "請輸入您的名字。";
$MESS["MF_SESS_EXP"] = "您的會議已經過期。請再次發送您的消息。";
