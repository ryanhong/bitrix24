<?php
$MESS["MFT_CAPTCHA"] = "驗證碼";
$MESS["MFT_CAPTCHA_CODE"] = "輸入您在圖片上看到的字母";
$MESS["MFT_EMAIL"] = "你的郵件";
$MESS["MFT_MESSAGE"] = "訊息";
$MESS["MFT_NAME"] = "姓名";
$MESS["MFT_SUBMIT"] = "發送";
