<?php
$MESS["IBLOCK_ACTIONS"] = "動作設置";
$MESS["IBLOCK_ACTION_VARIABLE"] = "包含動作的變量";
$MESS["IBLOCK_COMPARE_NAME"] = "比較列表的唯一名稱";
$MESS["IBLOCK_COMPARE_URL"] = "比較表頁面的URL";
$MESS["IBLOCK_DETAIL_URL"] = "頁面的URL帶有細節內容";
$MESS["IBLOCK_IBLOCK"] = "Infoblock";
$MESS["IBLOCK_PRODUCT_ID_VARIABLE"] = "包含可購買產品ID的變量";
$MESS["IBLOCK_TYPE"] = "InfoBlock類型";
