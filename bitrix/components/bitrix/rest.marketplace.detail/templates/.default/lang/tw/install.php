<?php
$MESS["B24_APP_INSTALL_CANCEL"] = "關閉";
$MESS["B24_APP_INSTALL_VERSION"] = "版本";
$MESS["BX24_APP_INSTALL_ADMIN_ERROR"] = "請聯繫您的Intranet管理員安裝應用程序。";
$MESS["BX24_APP_INSTALL_BITRIX24_MODULE"] = "未安裝BitRix24模塊。";
$MESS["BX24_APP_INSTALL_ERROR"] = "錯誤！該應用程序未安裝。";
$MESS["BX24_APP_INSTALL_EULA_LINK"] = "https://www.1c-bitrix.ru/download/files/manuals/ru/oferta_fiz.html";
$MESS["BX24_APP_INSTALL_EULA_TEXT"] = "我已經閱讀並同意<a href= \"#link# \" target= \"_blank \">許可協議</a>";
$MESS["BX24_APP_INSTALL_HTTPS_WARNING"] = "<b>警告！</b> HTTPS協議才能正確運行已安裝的市場應用程序。";
$MESS["BX24_APP_INSTALL_MODULE_UNINSTALL"] = "應用程序所需的一些服務已卸載。如果沒有這些服務，該應用程序將不會運行。請安裝所需的模塊。";
$MESS["BX24_APP_INSTALL_MODULE_UNINSTALL_BITRIX24"] = "應用程序所需的一些服務已卸載。如果沒有這些服務，該應用程序將不會運行。打開<a href='#path_configs#'> Intranet設置頁面</a>以安裝它們。";
$MESS["BX24_APP_INSTALL_PRIVACY_LINK"] = "https://www.bitrix24.com/privacy/";
$MESS["BX24_APP_INSTALL_PRIVACY_TEXT"] = "我已經閱讀並同意<a href= \"#link# \" target= \"_blank \">隱私政策</a>";
$MESS["BX24_APP_INSTALL_RIGHTS"] = "該申請要求以下權限：";
$MESS["BX24_APP_INSTALL_SUCCESS"] = "該應用程序已安裝。 <a href='#link#'>打開</a>";
$MESS["BX24_APP_INSTALL_TERMS_OF_SERVICE_TEXT_2"] = "通過安裝或下載應用程序或解決方案，您同意<a href= \"#link# \" target= \"_blank \"> bitrix24.market使用條款</a>";
