<?php
$MESS["REST_MARKETPLACE_TERMS_OF_SERVICE_LINK"] = "https://www.bitrix24.com/terms/apps24_terms_of_service.pdf";
$MESS["RMP_ACCESS_DENIED"] = "拒絕訪問。聯繫您的管理員以安裝應用程序。";
$MESS["RMP_APP_TIME_LIMIT_3"] = "3個月";
$MESS["RMP_APP_TIME_LIMIT_6"] = "6個月";
$MESS["RMP_APP_TIME_LIMIT_12"] = "12個月";
