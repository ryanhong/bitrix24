<?php
$MESS["SALE_ACCESS_DENIED"] = "請登錄以查看用戶帳戶。";
$MESS["SOPC_EMPTY_PAY_SYSTEM_ID"] = "無效的支付系統ID";
$MESS["SOPC_EMPTY_PAY_SYSTEM_LIST"] = "沒有付款系統";
$MESS["SOPC_ERROR_INNER_PAY_SYSTEM_RESTRICTED"] = "付款系統錯誤。使用內部帳戶付款已受到限制。";
$MESS["SOPC_ERROR_ORDER_NOT_EXISTS"] = "找不到命令。";
$MESS["SOPC_ERROR_ORDER_PAYMENT_SYSTEM"] = "錯誤創建訂單：無法獲得付款系統。請聯繫網站管理員以獲取更多信息。";
$MESS["SOPC_ERROR_PAYMENT_NOT_EXISTS"] = "找不到付款";
$MESS["SOPC_INVALID_TOKEN"] = "無效的安全令牌";
$MESS["SOPC_LOW_BALANCE"] = "內部帳戶的資金不足";
$MESS["SOPC_MODULE_NOT_INSTALL"] = "E商店模塊未安裝";
$MESS["SOPC_TITLE"] = "充氣帳戶";
