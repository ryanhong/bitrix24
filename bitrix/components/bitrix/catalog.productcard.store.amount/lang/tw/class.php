<?php
$MESS["STORES_LIST_DROPDOWN_ACTIONS_LIST_DELETE"] = "刪除";
$MESS["STORES_LIST_DROPDOWN_ACTIONS_LIST_NONE"] = "選擇動作";
$MESS["STORE_LIST_GRID_HEADER_AMOUNT"] = "數量";
$MESS["STORE_LIST_GRID_HEADER_QUANTITY"] = "可用的庫存";
$MESS["STORE_LIST_GRID_HEADER_QUANTITY_COMMON_MSGVER_1"] = "手頭庫存";
$MESS["STORE_LIST_GRID_HEADER_QUANTITY_RESERVED"] = "預訂的";
$MESS["STORE_LIST_GRID_HEADER_TITLE"] = "倉庫";
$MESS["STORE_LIST_GRID_IM_STUB_TITLE"] = "請啟用庫存管理來管理產品庫存。";
$MESS["STORE_LIST_GRID_OPEN_DETAILS_TITLE"] = "打開";
$MESS["STORE_LIST_GRID_STORE_TITLE_WITHOUT_NAME"] = "無標題";
$MESS["STORE_LIST_GRID_STUB_TITLE"] = "該產品在所有倉庫中均無庫存。";
$MESS["STORE_LIST_INVENTORY_MANAGEMENT"] = "庫存管理";
