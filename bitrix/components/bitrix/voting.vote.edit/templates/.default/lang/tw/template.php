<?php
$MESS["VVE_ALLOW_TO_CHANGE_MIND"] = "用戶可以更改投票";
$MESS["VVE_ALLOW_TO_VIEW_RESULTS"] = "用戶可以在投票之前查看民意調查結果";
$MESS["VVE_ANS"] = "回答";
$MESS["VVE_ANS_DEL"] = "刪除答案";
$MESS["VVE_ANS_DELETE"] = "刪除答案？";
$MESS["VVE_ANS_SORT"] = "排序答案";
$MESS["VVE_DATE"] = "民意調查截止日期：";
$MESS["VVE_QUESTION"] = "問題";
$MESS["VVE_QUESTION_ADD"] = "添加問題";
$MESS["VVE_QUESTION_DEL"] = "刪除問題";
$MESS["VVE_QUESTION_DELETE"] = "刪除問題？";
$MESS["VVE_QUESTION_MULTIPLE"] = "允許多重選擇";
$MESS["VVE_QUESTION_SORT_DOWN"] = "向下移動";
$MESS["VVE_QUESTION_SORT_UP"] = "提升";
