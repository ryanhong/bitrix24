<?php
$MESS["INTRANET_USER_OTP_ACTIVATE"] = "使能夠";
$MESS["INTRANET_USER_OTP_ACTIVE"] = "啟用";
$MESS["INTRANET_USER_OTP_AUTH"] = "兩步身份驗證";
$MESS["INTRANET_USER_OTP_CHANGE_PHONE_1"] = "我的移動設備已更改";
$MESS["INTRANET_USER_OTP_CODES"] = "恢復代碼";
$MESS["INTRANET_USER_OTP_CODES_SHOW"] = "看法";
$MESS["INTRANET_USER_OTP_CONNECT"] = "連接";
$MESS["INTRANET_USER_OTP_DEACTIVATE"] = "禁用";
$MESS["INTRANET_USER_OTP_LEFT_DAYS"] = "（將在＃num＃中啟用）";
$MESS["INTRANET_USER_OTP_NOT_ACTIVE"] = "禁用";
$MESS["INTRANET_USER_OTP_NOT_EXIST"] = "未配置";
$MESS["INTRANET_USER_OTP_NO_DAYS"] = "永遠";
$MESS["INTRANET_USER_OTP_PROROGUE"] = "推遲";
$MESS["INTRANET_USER_OTP_SETUP"] = "配置";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_CLOSE"] = "關閉";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_PASSWORDS"] = "配置應用程序密碼";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT"] = "<b>恭喜！</b>
<br/> <br/>
您已成功為帳戶配置了兩步身份驗證。
<br/> <br/>
兩步身份驗證涉及兩個隨後的驗證階段。第一個需要您的主密碼。第二階段包括您必須在第二個身份驗證屏幕上輸入的一次性代碼。
<br/> <br/>
請注意，您將必須使用應用程序密碼進行安全數據交換（例如，使用外部日曆）。您將找到在用戶配置文件中管理應用程序密碼的工具。";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT2"] = "記住要確保您的恢復代碼。如果您丟失了移動設備或由於任何其他原因無法獲得代碼，則可能需要它們。";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT_NEW"] = "<b>恭喜！</b>
<br/> <br/>
您已成功為帳戶配置了兩步身份驗證。";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT_NEW2"] = "請注意，與外部服務（日曆等）的數據交換將需要使用特定於應用程序的密碼。您可以管理個人資料中的這些。";
$MESS["INTRANET_USER_OTP_SUCCESS_POPUP_TEXT_RES_CODE"] = "<b>恭喜！</b>
<br/> <br/>
您現在為您的帳戶設置了兩步身份驗證。
<br/> <br/>
請記住要保留<b>恢復代碼</b>如果丟失了移動設備或無法獲得一次性代碼，則可能需要登錄。<br/> <br/> <br/>";
