<?php
$MESS["BBE_BLOG_URL"] = "博客URL";
$MESS["BBE_BLOG_VAR"] = "博客標識符變量";
$MESS["BBE_PAGE_VAR"] = "頁面變量";
$MESS["BBE_PATH_TO_BLOG"] = "博客頁面路徑的模板";
$MESS["BBE_PATH_TO_BLOG_EDIT"] = "博客編輯頁面路徑的模板";
$MESS["BLOG_PROPERTY"] = "在博客信息中顯示其他屬性";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
