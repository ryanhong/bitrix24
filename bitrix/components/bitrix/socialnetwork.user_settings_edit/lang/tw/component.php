<?php
$MESS["SONET_C40_ALL"] = "所有訪客";
$MESS["SONET_C40_AUTHORIZED"] = "授權用戶";
$MESS["SONET_C40_FRIENDS2"] = "只有朋友和他們的朋友";
$MESS["SONET_C40_NOBODY"] = "沒有人";
$MESS["SONET_C40_NO_PERMS"] = "您無權執行此操作。";
$MESS["SONET_C40_NO_USER"] = "找不到用戶。";
$MESS["SONET_C40_NO_USER_ID"] = "未指定用戶ID。";
$MESS["SONET_C40_ONLY_FRIENDS"] = "只有朋友";
$MESS["SONET_C40_PAGE_TITLE"] = "用戶隱私";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
