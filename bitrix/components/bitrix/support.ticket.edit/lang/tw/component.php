<?php
$MESS["MODULE_NOT_INSTALL"] = "Helpdesk模塊未安裝。";
$MESS["SUP_EDIT_TICKET_TITLE"] = "票務＃＃id＃ - ＃標題＃";
$MESS["SUP_ERROR"] = "錯誤創建消息";
$MESS["SUP_FORGOT_MESSAGE"] = "請填寫\“消息\”字段";
$MESS["SUP_FORGOT_TITLE"] = "請填寫\“主題\”字段";
$MESS["SUP_MAX_FILE_SIZE_EXCEEDING"] = "錯誤！無法上傳文件\“＃file_name＃\”。可能超出了最大文件大小。";
$MESS["SUP_NEW_TICKET_TITLE"] = "新票";
$MESS["SUP_PAGES"] = "消息";
