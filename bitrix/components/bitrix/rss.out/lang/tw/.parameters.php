<?php
$MESS["CP_BRO_CACHE_FILTER"] = "緩存如果過濾器處於活動狀態";
$MESS["CP_BRO_CACHE_GROUPS"] = "尊重訪問權限";
$MESS["CP_BRO_FILTER_NAME"] = "篩選";
$MESS["CP_BRO_IBLOCK_ID"] = "信息塊";
$MESS["CP_BRO_IBLOCK_TYPE"] = "信息塊類型";
$MESS["CP_BRO_NUM_DAYS"] = "出口天數";
$MESS["CP_BRO_NUM_NEWS"] = "導出的新聞數量";
$MESS["CP_BRO_RSS"] = "RSS設置";
$MESS["CP_BRO_RSS_TTL"] = "TTL（分鐘）";
$MESS["CP_BRO_SECTION_CODE"] = "部分代碼";
$MESS["CP_BRO_SECTION_ID"] = "部分";
$MESS["CP_BRO_SORT_ACTIVE_FROM"] = "激活日期";
$MESS["CP_BRO_SORT_ASC"] = "上升";
$MESS["CP_BRO_SORT_BY1"] = "新聞首次分類通行證的字段";
$MESS["CP_BRO_SORT_BY2"] = "新聞第二分類通行證字段";
$MESS["CP_BRO_SORT_CREATED"] = "創建日期";
$MESS["CP_BRO_SORT_DESC"] = "下降";
$MESS["CP_BRO_SORT_ID"] = "ID";
$MESS["CP_BRO_SORT_NAME"] = "姓名";
$MESS["CP_BRO_SORT_ORDER1"] = "新聞的指示首次分類通行證";
$MESS["CP_BRO_SORT_ORDER2"] = "新聞第二分類通行證的方向";
$MESS["CP_BRO_SORT_SORT"] = "排序";
$MESS["CP_BRO_SORT_TIMESTAMP_X"] = "最後更改的日期";
$MESS["CP_BRO_YANDEX"] = "yandex格式";
