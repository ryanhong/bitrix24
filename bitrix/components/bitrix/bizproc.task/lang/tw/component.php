<?php
$MESS["BPAT_DELEGATE_CANCEL"] = "取消";
$MESS["BPAT_DELEGATE_LABEL"] = "代表";
$MESS["BPAT_DELEGATE_SELECT"] = "選擇";
$MESS["BPAT_EMPTY_TASK"] = "分配ID未指定。";
$MESS["BPAT_NO_ACCESS"] = "您無法查看所選任務。";
$MESS["BPAT_NO_PARAMETERS"] = "活動數據限制超出了。請更改工作流設置。";
$MESS["BPAT_NO_STATE"] = "找不到業務流程。";
$MESS["BPAT_NO_TASK"] = "找不到任務。";
$MESS["BPAT_TITLE"] = "作業";
