<?php
$MESS["SPL_CRM_MODULE_ERROR"] = "未安裝CRM模塊。";
$MESS["SPL_ORDERS_LIMITS_MESSAGE"] = "請升級到繼續處理訂單並通過銷售中心接收付款的商業計劃之一。";
$MESS["SPL_ORDERS_LIMITS_TITLE"] = "您已經達到了可以使用銷售中心創建的最大訂單數量。";
$MESS["SPL_SALESCENTER_MODULE_ERROR"] = "未安裝銷售中心模塊。";
$MESS["SPL_SALE_MODULE_ERROR"] = "未安裝在線商店模塊。";
$MESS["SPL_SESSION_ERROR"] = "會話ID未指定";
