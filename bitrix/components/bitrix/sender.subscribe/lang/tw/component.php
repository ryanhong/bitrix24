<?php
$MESS["SENDER_SUBSCR_ERR_EMAIL"] = "指定的電子郵件地址不正確。";
$MESS["SENDER_SUBSCR_ERR_SECURITY"] = "錯誤確認訂閱：無效URL。";
$MESS["SENDER_SUBSCR_MODULE_NOT_INSTALLED"] = "電子郵件營銷模塊目前不可用。";
$MESS["SENDER_SUBSCR_MODULE_NOT_INSTALLED1"] = "Marketing24模塊目前不可用。";
$MESS["SENDER_SUBSCR_NOTE_CONFIRM"] = "確認消息已發送到您的電子郵件。請單擊消息中的鏈接以確認您的訂閱。";
$MESS["SENDER_SUBSCR_NOTE_SUCCESS"] = "您現在已經訂閱了新聞通訊。";
$MESS["SENDER_SUBSCR_TITLE"] = "訂閱";
