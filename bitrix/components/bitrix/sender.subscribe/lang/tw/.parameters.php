<?php
$MESS["CP_BSF_HIDE_MAILINGS"] = "隱藏郵件列表並訂閱全部";
$MESS["CP_BSF_SET_TITLE"] = "設置頁面標題";
$MESS["CP_BSF_SHOW_HIDDEN"] = "顯示隱藏的訂閱";
$MESS["CP_BSF_USE_CONFIRMATION"] = "通過電子郵件確認訂閱";
$MESS["CP_BSF_USE_PERSONALIZATION"] = "選擇當前用戶的訂閱";
