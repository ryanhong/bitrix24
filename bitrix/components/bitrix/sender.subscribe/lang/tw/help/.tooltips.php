<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>否CAHCE </i>：沒有執行緩存。";
$MESS["CONFIRMATION_TIP"] = "包含不需要登錄的確認鏈接的消息將發送到訂戶。<br />訂閱者導航鏈接後，他們最終確定了訂閱。";
$MESS["SHOW_HIDDEN_TIP"] = "隱藏的訂閱是在公共區域選項中未選中的<i>顯示的訂閱。";
$MESS["USE_PERSONALIZATION_TIP"] = "如果已檢查，僅選擇訂閱當前用戶的新聞通訊。否則，將選擇所有訂閱。";
