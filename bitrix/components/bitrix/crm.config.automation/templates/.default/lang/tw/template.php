<?php
$MESS["CRM_CONFIG_AUTOMATION_HELP_1_1"] = "快速入門新銷售代理商。自動化規則和触發器設定了需要做什麼以及何時做的。";
$MESS["CRM_CONFIG_AUTOMATION_HELP_1_2"] = "使您的銷售代理免於他們的日常工作。";
$MESS["CRM_CONFIG_AUTOMATION_HELP_1_3"] = "控制銷售過程的每個步驟。 BITRIX24 CRM立即通知主管的代理，文檔或瓶頸的任何問題。";
$MESS["CRM_CONFIG_AUTOMATION_HELP_2_1"] = "在所有適用的CRM實體中使用自動化選項卡。";
$MESS["CRM_CONFIG_AUTOMATION_HELP_2_2"] = "設置自動化規則以幫助您的業務發展。";
$MESS["CRM_CONFIG_AUTOMATION_HELP_2_3"] = "測試並查看自動化規則和触發方式如何實時工作。";
$MESS["CRM_CONFIG_AUTOMATION_HELP_TITLE"] = "使用BitRix24 CRM自動銷售和客戶管理";
$MESS["CRM_CONFIG_AUTOMATION_HELP_TITLE_2"] = "設置規則並輕鬆觸發";
