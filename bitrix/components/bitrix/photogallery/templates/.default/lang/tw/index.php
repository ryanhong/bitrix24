<?php
$MESS["P_ADD_ALBUM"] = "新唱片";
$MESS["P_PHOTO"] = "照片庫";
$MESS["P_PHOTOGRAPHIES"] = "相片";
$MESS["P_PHOTO_SORT_COMMENTS"] = "大多數評論";
$MESS["P_PHOTO_SORT_COMMENTS_TITLE"] = "通過評論計數";
$MESS["P_PHOTO_SORT_ID"] = "新的";
$MESS["P_PHOTO_SORT_ID_TITLE"] = "按創建日期";
$MESS["P_PHOTO_SORT_RATING"] = "最好的";
$MESS["P_PHOTO_SORT_RATING_TITLE"] = "通過投票計數";
$MESS["P_PHOTO_SORT_SHOWS"] = "最受關注";
$MESS["P_PHOTO_SORT_SHOWS_TITLE"] = "通過視圖";
$MESS["P_TAGS_CLOUD"] = "標籤雲";
$MESS["P_UPLOAD"] = "上傳照片";
