<?php
$MESS["P_ALBUM_DATE"] = "日期";
$MESS["P_ALBUM_DESCRIPTION"] = "描述";
$MESS["P_ALBUM_NAME"] = "姓名";
$MESS["P_CANCEL"] = "取消";
$MESS["P_EDIT_SECTION"] = "編輯專輯屬性";
$MESS["P_PASSWORD"] = "密碼";
$MESS["P_SET_PASSWORD"] = "設置專輯密碼";
$MESS["P_SUBMIT"] = "節省";
$MESS["P_UP"] = "專輯";
$MESS["P_UP_TITLE"] = "查看所有專輯";
