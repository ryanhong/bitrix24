<?php
$MESS["P_ADD_ALBUM"] = "新唱片";
$MESS["P_ADD_ALBUM_TITLE"] = "創建新專輯";
$MESS["P_ALBUMS_CNT"] = "專輯";
$MESS["P_EDIT_ICON"] = "選擇封面";
$MESS["P_PASSWORD"] = "密碼";
$MESS["P_PASSWORD_TITLE"] = "專輯受密碼保護";
$MESS["P_PHOTOS_CNT"] = "相片";
$MESS["P_SECTION_DELETE"] = "刪除專輯";
$MESS["P_SECTION_DELETE_ASK"] = "確定您想不可逆轉地刪除專輯嗎？";
$MESS["P_SECTION_EDIT"] = "編輯專輯屬性";
$MESS["P_SLIDE_SHOW"] = "幻燈片顯示";
$MESS["P_SUPPLY_PASSWORD"] = "密碼";
$MESS["P_UP"] = "向上";
$MESS["P_UPLOAD"] = "上傳照片";
$MESS["P_UP_TITLE"] = "一個水平";
