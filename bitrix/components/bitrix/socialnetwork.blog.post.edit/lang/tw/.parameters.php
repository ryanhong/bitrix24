<?php
$MESS["BB_PATH_TO_SMILE"] = "笑臉文件夾路徑（相對於站點根）";
$MESS["BC_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["BH_PATH_TO_DRAFT"] = "對話草稿頁面路徑模板";
$MESS["BPC_ALLOW_POST_CODE"] = "將對話符號代碼作為ID";
$MESS["BPC_IMAGE_MAX_HEIGHT"] = "最大限度。圖像高度";
$MESS["BPC_IMAGE_MAX_WIDTH"] = "最大限度。圖像寬度";
$MESS["BPE_ID"] = "對話ID";
$MESS["BPE_PAGE_VAR"] = "頁面變量名稱";
$MESS["BPE_PATH_TO_BLOG"] = "對話頁面路徑模板";
$MESS["BPE_PATH_TO_POST"] = "對話頁面路徑模板";
$MESS["BPE_PATH_TO_POST_EDIT"] = "博客文章編輯頁面";
$MESS["BPE_PATH_TO_USER"] = "用戶配置文件路徑模板";
$MESS["BPE_POST_VAR"] = "對話ID變量名稱";
$MESS["BPE_USER_VAR"] = "用戶ID變量名稱";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
$MESS["POST_PROPERTY"] = "顯示博客文章屬性";
