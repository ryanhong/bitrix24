<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_NUMBER_TEMPLATE_0"] = "沒用過";
$MESS["CRM_NUMBER_TEMPLATE_1"] = "開始編號";
$MESS["CRM_NUMBER_TEMPLATE_2"] = "使用前綴";
$MESS["CRM_NUMBER_TEMPLATE_3"] = "隨機數";
$MESS["CRM_NUMBER_TEMPLATE_4"] = "用戶ID和文檔編號";
$MESS["CRM_NUMBER_TEMPLATE_5"] = "定期重新啟動編號";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問。";
