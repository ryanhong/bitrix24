<?php
$MESS["NAV_TITLE"] = "類別";
$MESS["SEARCH_CORRECT_AND_CONTINUE"] = "您的搜索沒有產生結果。更正您的請求，然後重試。";
$MESS["SEARCH_ERROR"] = "搜索請求有錯誤：";
$MESS["SEARCH_GO"] = "搜尋";
$MESS["WIKI_NO_CATEGORIES"] = "Wiki目前沒有類別。當您首先為Wiki頁面指定類別時，將根據需要創建類別。";
