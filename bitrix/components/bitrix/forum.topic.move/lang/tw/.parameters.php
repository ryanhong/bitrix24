<?php
$MESS["F_DEFAULT_FID"] = "論壇ID";
$MESS["F_DEFAULT_TID"] = "主題ID";
$MESS["F_INDEX_TEMPLATE"] = "論壇列表頁面";
$MESS["F_LIST_TEMPLATE"] = "主題列表頁面";
$MESS["F_MESSAGE_TEMPLATE"] = "消息查看頁面";
$MESS["F_MOVE_TEMPLATE"] = "主題移動頁面";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "個人資料頁";
$MESS["F_READ_TEMPLATE"] = "主題閱讀頁面";
$MESS["F_SET_NAVIGATION"] = "顯示麵包屑導航";
