<?php
$MESS["COURSE_DETAIL_TEMPLATE_TIP"] = "課程詳細信息頁面的路徑。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將將頁面標題設置為<b>測試結果</b>。";
$MESS["TEST_DETAIL_TEMPLATE_TIP"] = "通往主測試頁面的路徑。";
$MESS["TEST_ID_VARIABLE_TIP"] = "測試ID將通過的變量的名稱。";
