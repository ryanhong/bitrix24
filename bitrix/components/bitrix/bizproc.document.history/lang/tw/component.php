<?php
$MESS["BPABS_EMPTY_DOC_ID"] = "沒有指定要創建業務流程的元素ID。";
$MESS["BPABS_EMPTY_DOC_TYPE"] = "需要元素類型。";
$MESS["BPABS_EMPTY_ENTITY"] = "沒有指定要創建業務流程的實體。";
$MESS["BPADH_DELETE_DOC"] = "刪除";
$MESS["BPADH_DELETE_DOC_CONFIRM"] = "您確定要刪除此唱片嗎？";
$MESS["BPADH_DELETE_OK"] = "歷史記錄已刪除。";
$MESS["BPADH_NAV_TITLE"] = "版本";
$MESS["BPADH_NO_PERMS"] = "您無權訪問元素歷史記錄。";
$MESS["BPADH_RECOVERY_DOC"] = "恢復";
$MESS["BPADH_RECOVERY_ERROR"] = "無法從歷史記錄中恢復記錄。";
$MESS["BPADH_RECOVERY_OK"] = "歷史記錄已恢復。";
$MESS["BPADH_TITLE"] = "元素歷史";
$MESS["BPADH_VIEW_DOC"] = "查看元素";
$MESS["BPATT_NO_MODULE_ID"] = "需要模塊ID。";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "未安裝文檔庫模塊。";
