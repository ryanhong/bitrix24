<?php
$MESS["DISK_ACTION_SAVE_TO_OWN_FILES"] = "保存到bitrix24.drive";
$MESS["DISK_FOLDER_FILTER_CREATED_BY"] = "由...製作";
$MESS["DISK_FOLDER_LIST_ACT_BIZPROC_OLD_TEMPLATE"] = "（舊版本）";
$MESS["DISK_FOLDER_LIST_ACT_CONNECT"] = "連接到驅動器";
$MESS["DISK_FOLDER_LIST_ACT_COPIED_INTERNAL_LINK"] = "鏈接複製到剪貼板";
$MESS["DISK_FOLDER_LIST_ACT_COPY"] = "複製";
$MESS["DISK_FOLDER_LIST_ACT_COPY_INTERNAL_LINK"] = "複製內部鏈接";
$MESS["DISK_FOLDER_LIST_ACT_DETAILS"] = "細節";
$MESS["DISK_FOLDER_LIST_ACT_DOWNLOAD"] = "下載";
$MESS["DISK_FOLDER_LIST_ACT_EDIT"] = "編輯";
$MESS["DISK_FOLDER_LIST_ACT_GET_EXT_LINK"] = "獲取公共鏈接";
$MESS["DISK_FOLDER_LIST_ACT_LOCK"] = "鎖";
$MESS["DISK_FOLDER_LIST_ACT_MARK_DELETED"] = "刪除";
$MESS["DISK_FOLDER_LIST_ACT_MOVE"] = "移動";
$MESS["DISK_FOLDER_LIST_ACT_OPEN"] = "打開";
$MESS["DISK_FOLDER_LIST_ACT_RENAME"] = "改名";
$MESS["DISK_FOLDER_LIST_ACT_RIGHTS_SETTINGS"] = "訪問權限";
$MESS["DISK_FOLDER_LIST_ACT_SHARE_COMPLEX"] = "分享";
$MESS["DISK_FOLDER_LIST_ACT_SHOW_HISTORY"] = "修訂記錄";
$MESS["DISK_FOLDER_LIST_ACT_SHOW_SHARING_DETAIL"] = "共享參數";
$MESS["DISK_FOLDER_LIST_ACT_SHOW_SHARING_DETAIL_2"] = "分享";
$MESS["DISK_FOLDER_LIST_ACT_SHOW_SHARING_DETAIL_3"] = "與其他用戶共享";
$MESS["DISK_FOLDER_LIST_ACT_START_BIZPROC"] = "業務流程";
$MESS["DISK_FOLDER_LIST_ACT_UNLOCK"] = "開鎖";
$MESS["DISK_FOLDER_LIST_B24_APPEND_DISK_SPACE"] = "添加磁盤空間";
$MESS["DISK_FOLDER_LIST_B24_LABEL_DISK_SPACE"] = "免費：＃free_space＃of＃disk_size＃";
$MESS["DISK_FOLDER_LIST_B24_URL_DISK_SPACE"] = "http://www.bitrix24.com/prices/space.php#space100";
$MESS["DISK_FOLDER_LIST_COLUMN_BIZPROC"] = "業務流程";
$MESS["DISK_FOLDER_LIST_COLUMN_CREATE_TIME"] = "創建於";
$MESS["DISK_FOLDER_LIST_COLUMN_CREATE_USER"] = "由...製作";
$MESS["DISK_FOLDER_LIST_COLUMN_FORMATTED_SIZE"] = "尺寸";
$MESS["DISK_FOLDER_LIST_COLUMN_ID"] = "ID";
$MESS["DISK_FOLDER_LIST_COLUMN_NAME"] = "姓名";
$MESS["DISK_FOLDER_LIST_COLUMN_UPDATE_TIME"] = "修改";
$MESS["DISK_FOLDER_LIST_COLUMN_UPDATE_USER"] = "修改";
$MESS["DISK_FOLDER_LIST_DEFAULT_ACTION"] = "選擇動作";
$MESS["DISK_FOLDER_LIST_DETACH_BUTTON"] = "斷開";
$MESS["DISK_FOLDER_LIST_DETACH_FILE_CONFIRM"] = "如果您斷開該文件，則文件\“＃名稱＃\”將從磁盤中刪除。";
$MESS["DISK_FOLDER_LIST_DETACH_FILE_TITLE"] = "斷開共享文件";
$MESS["DISK_FOLDER_LIST_DETACH_FOLDER_CONFIRM"] = "文件夾\“＃name＃\”，如果您斷開連接，將從磁盤中刪除所有文件。";
$MESS["DISK_FOLDER_LIST_DETACH_FOLDER_TITLE"] = "斷開共享文件夾";
$MESS["DISK_FOLDER_LIST_DETAIL_SHARE_INFO"] = "和...分享";
$MESS["DISK_FOLDER_LIST_DETAIL_SHARE_SECTION"] = "共享";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_FIND_SHARING"] = "找不到共享。";
$MESS["DISK_FOLDER_LIST_FOLDER_LABEL_BTN_SHARE"] = "分享";
$MESS["DISK_FOLDER_LIST_GRID_BIZPROC"] = "業務流程";
$MESS["DISK_FOLDER_LIST_GRID_BIZPROC_TASKS"] = "任務";
$MESS["DISK_FOLDER_LIST_GRID_BIZPROC_TASKS_TITLE"] = "查看任務";
$MESS["DISK_FOLDER_LIST_GRID_BIZPROC_TITLE"] = "查看文檔業務流程";
$MESS["DISK_FOLDER_LIST_INF_AFTER_ACTION_COPY"] = "成功複製";
$MESS["DISK_FOLDER_LIST_INF_AFTER_ACTION_DELETE"] = "成功刪除";
$MESS["DISK_FOLDER_LIST_INF_AFTER_ACTION_MOVE"] = "成功地移動";
$MESS["DISK_FOLDER_LIST_INF_AFTER_ACTION_RENAME"] = "項目重命名為成功";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_APPROVE_RESHARING"] = "用戶可以重新分享此文件夾";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_BTN_CANCEL"] = "取消";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_BTN_DIE_ACCESS"] = "停止共享";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_BTN_DIE_SELF_ACCESS"] = "斷開共享文件夾";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_BTN_DIE_SELF_ACCESS_SIMPLE"] = "斷開";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_BTN_DIE_SELF_ACCESS_SIMPLE_CANCEL"] = "取消";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_BTN_OPEN_ACCESS"] = "分享";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_CAN_EDIT"] = "其他用戶有編輯權限";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_DESTINATION"] = "和：";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_DESTINATION_1"] = "添加員工";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_DESTINATION_2"] = "添加更多";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_OWNER_GROUP"] = "（團體）";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_OWNER_USER"] = "（所有者）";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_OWNER_USER_COMMON_SECTION"] = "（由...製作）";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_PLACEHOLDER"] = "描述";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_COMMON_SHARED_SECTION_PROCESS_DIE_ACCESS"] = "斷開共享文件夾";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_COMMON_SHARED_SECTION_PROCESS_DIE_ACCESS_SUCCESS"] = "共享文件夾已斷開連接";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_GROUP_CONNECTED_TITLE"] = "小組驅動";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_GROUP_PROCESS_DIE_ACCESS"] = "斷開組共享文件夾";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_GROUP_PROCESS_DIE_ACCESS_SUCCESS"] = "組共享文件夾斷開連接";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_HEADER_ACCESS"] = "管理共享文件夾";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_HEADER_MANAGE_ACCESS"] = "邀請";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_PROCESS_ACCESS"] = "現在打開共享訪問";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_PROCESS_ACCESS_SUCCESS"] = "現在共享文件夾'＃foldername＃'";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_PROCESS_DIE_ACCESS"] = "關閉共享訪問";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_PROCESS_DIE_ACCESS_SUCCESS"] = "文件夾不再共享";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_LOAD_MORE_COUNT_1"] = "＃計數＃用戶更多";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_LOAD_MORE_COUNT_2_4"] = "＃計數＃用戶更多";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_LOAD_MORE_COUNT_5_20"] = "＃計數＃用戶更多";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_LOAD_MORE_COUNT_21"] = "＃計數＃用戶更多";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_NOT_USAGE_COUNT_1"] = "拒絕：＃計數＃用戶";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_NOT_USAGE_COUNT_2_4"] = "拒絕：＃計數＃用戶";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_NOT_USAGE_COUNT_5_20"] = "拒絕：＃計數＃用戶";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_NOT_USAGE_COUNT_21"] = "拒絕：＃計數＃用戶";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_USAGE_CANNOT_EDIT_COUNT_1"] = "查看訪問：＃計數＃用戶";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_USAGE_CANNOT_EDIT_COUNT_2_4"] = "查看訪問：＃計數＃用戶";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_USAGE_CANNOT_EDIT_COUNT_5_20"] = "查看訪問：＃計數＃用戶";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_USAGE_CANNOT_EDIT_COUNT_21"] = "查看訪問：＃計數＃用戶";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_USAGE_CAN_EDIT_COUNT_1"] = "編輯訪問：＃計數＃用戶";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_USAGE_CAN_EDIT_COUNT_2_4"] = "編輯訪問：＃計數＃用戶";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_USAGE_CAN_EDIT_COUNT_5_20"] = "編輯訪問：＃計數＃用戶";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TAB_USERS_USAGE_CAN_EDIT_COUNT_21"] = "編輯訪問：＃計數＃用戶";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_DIE_ALL_ACCESS_DESCR"] = "關閉對該文件夾的共享訪問將刪除該文件夾的本地副本及其文件，並在當前可以訪問此文件夾的用戶的計算機上刪除。您的本地副本將保持完整。";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_DIE_ALL_ACCESS_SIMPLE"] = "停止共享";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_DIE_ONCE_ACCESS_DESCR"] = "關閉共享訪問將在用戶計算機上刪除文件夾的本地副本及其文件。";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_DIE_ONCE_ACCESS_SIMPLE"] = "停止共享";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_DIE_SELF_ACCESS_SIMPLE"] = "斷開共享文件夾";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_GROUP_DIE_SELF_ACCESS_SIMPLE"] = "斷開組共享文件夾";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_GROUP_DIE_SELF_ACCESS_SIMPLE_DESCR"] = "這將刪除組文件夾和其中的所有文件。";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_SHARING"] = "共享該文件夾";
$MESS["DISK_FOLDER_LIST_INVITE_MODAL_TITLE_USAGE_SHARING"] = "共享文件夾參數";
$MESS["DISK_FOLDER_LIST_LABEL_UPDATE_TIME"] = "更改為＃日期＃";
$MESS["DISK_FOLDER_LIST_MAKE_SHARE_SECTION"] = "分享";
$MESS["DISK_FOLDER_LIST_SORT_BY_DELETE_TIME"] = "按日期刪除";
$MESS["DISK_FOLDER_LIST_SORT_BY_FORMATTED_SIZE"] = "按大小";
$MESS["DISK_FOLDER_LIST_SORT_BY_ID"] = "由ID";
$MESS["DISK_FOLDER_LIST_SORT_BY_NAME"] = "按名字";
$MESS["DISK_FOLDER_LIST_SORT_BY_UPDATE_TIME"] = "按日期";
$MESS["DISK_FOLDER_LIST_SORT_BY_UPDATE_TIME_2"] = "按日期更改";
$MESS["DISK_FOLDER_LIST_TITLE_GRID_TOOLBAR_DEST_LABEL"] = "更高級別";
$MESS["DISK_FOLDER_LIST_TRASH_CANCEL_DELETE_BUTTON"] = "取消";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_BUTTON"] = "移動回收箱";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_DESTROY_DELETED_FILE_CONFIRM"] = "您想永久刪除\“＃名稱＃\”？";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_DESTROY_DELETED_FOLDER_CONFIRM"] = "您是否要永久刪除文件夾\“＃名稱＃\”？";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_DESTROY_FILE_CONFIRM"] = "您要將\“＃名稱＃\”移至回收箱，或不可逆地刪除它？";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_DESTROY_FOLDER_CONFIRM"] = "您是否要將文件夾\“＃name＃\”移至回收垃圾箱，或不可逆地刪除它？";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_FILE_CONFIRM"] = "您想將\“＃name＃\”移至回收垃圾箱？";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_FOLDER_CONFIRM"] = "您是否要將文件夾\“＃名稱＃\”移至回收垃圾箱？";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_GROUP_CONFIRM"] = "您想將選定的物品移動到回收垃圾箱嗎？";
$MESS["DISK_FOLDER_LIST_TRASH_DELETE_TITLE"] = "確認刪除";
$MESS["DISK_FOLDER_LIST_TRASH_DESTROY_BUTTON"] = "不可逆轉地刪除";
$MESS["DISK_FOLDER_LIST_TRASH_DESTROY_GROUP_CONFIRM"] = "您想在選定的項目中刪除所選的物品嗎？";
$MESS["DISK_FOLDER_LIST_UNSHARE_SECTION_CONFIRM"] = "這將刪除文件夾和其中的所有文件。";
$MESS["DISK_TRASHCAN_ACT_DESTROY"] = "刪除";
$MESS["DISK_TRASHCAN_ACT_RESTORE"] = "恢復";
$MESS["DISK_TRASHCAN_COLUMN_DELETE_TIME"] = "刪除";
$MESS["DISK_TRASHCAN_COLUMN_DELETE_USER"] = "被刪除";
$MESS["DISK_TRASHCAN_NAME"] = "回收箱";
$MESS["DISK_TRASHCAN_TRASH_DELETE_DESTROY_FILE_CONFIRM"] = "您確定要永久刪除\“＃名稱＃\”？";
$MESS["DISK_TRASHCAN_TRASH_DELETE_DESTROY_FOLDER_CONFIRM"] = "您確定要永久刪除文件夾\“＃name＃\”？";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_DESCR_MULTIPLE"] = "您要恢復所選物品嗎？";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_FILE_CONFIRM"] = "您要還原文檔\“＃名稱＃\”？";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_FOLDER_CONFIRM"] = "您要還原文件夾\“＃名稱＃\”？";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_SUCCESS"] = "項目已恢復";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_TITLE"] = "確認還原";
