<?php
$MESS["SAPP_ACCEPT_USER_AMOUNT"] = "允許用戶指定應付金額";
$MESS["SAPP_CALLBACK_NAME"] = "交付觸發回調功能";
$MESS["SAPP_ELIMINATED_PAY_SYSTEMS"] = "排除付款系統選擇";
$MESS["SAPP_NAME_PRICE_VALUE"] = "合計訂單";
$MESS["SAPP_PATH_TO_BASKET"] = "通往籃子的路徑";
$MESS["SAPP_PATH_TO_PAYMENT"] = "付款頁路徑";
$MESS["SAPP_REDIRECT_TO_CURRENT_PAGE"] = "將產品添加到籃子之後，重定向到當前頁面";
$MESS["SAPP_REFRESHED_COMPONENT_MODE"] = "啟用新組件版本";
$MESS["SAPP_SELL_AMOUNT"] = "金額購買";
$MESS["SAPP_SELL_CURRENCY"] = "顯示貨幣";
$MESS["SAPP_SELL_SHOW_FIXED_VALUES"] = "顯示固定付款價值";
$MESS["SAPP_SELL_SHOW_RESULT_SUM"] = "在頁面上顯示金額";
$MESS["SAPP_SELL_USER_TYPES"] = "用戶類型";
$MESS["SAPP_SELL_VALUES_FROM_VAR"] = "使用變量設置值";
$MESS["SAPP_SHOW_ALL"] = "（未選中的）";
$MESS["SAPP_VAR"] = "URL中購買ID變量的名稱";
