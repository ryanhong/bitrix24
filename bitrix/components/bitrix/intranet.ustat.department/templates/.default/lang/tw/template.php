<?php
$MESS["INTRANET_USTAT_COMPANY_ACTIVITY_TITLE"] = "活動<br>速率";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_ACTIVITY"] = "積極的";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_INVOLVEMENT"] = "功能用法";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_TITLE"] = "公司脈衝";
$MESS["INTRANET_USTAT_COMPANY_HELP_ACTIVITY"] = "活動索引：用戶在給定時間段的所有Intranet功能上的所有活動的綜合。該索引顯示了用戶如何從事各種工具的工作。";
$MESS["INTRANET_USTAT_COMPANY_HELP_GENERAL"] = "公司Pulse目前是該門戶網站中用戶活動的總體指標（前一個小時內所有用戶的複合）。";
$MESS["INTRANET_USTAT_COMPANY_HELP_INVOLVEMENT"] = "參與級別：這是一個關鍵指標，它顯示了用戶對Bitrix24的功能的熟悉程度。它顯示了公司使用至少四分之一提供的工具的百分比。";
$MESS["INTRANET_USTAT_COMPANY_HELP_RATING"] = "評級取決於所有使用在給定時間段內執行至少1個操作的用戶的個人活動指數的平均值。";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_CRM"] = "功能用法：CRM";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_DISK"] = "功能用法：驅動器";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_IM"] = "功能用法：聊天";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_LIKES"] = "功能用法：喜歡";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_MOBILE"] = "功能用法：移動應用";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_SOCNET"] = "功能用法：社交網絡";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_TASKS"] = "功能用法：任務";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_TOTAL"] = "功能用法";
$MESS["INTRANET_USTAT_COMPANY_RATING_TITLE"] = "一般評級";
$MESS["INTRANET_USTAT_COMPANY_SECTION_ACTIVITY"] = "活動";
$MESS["INTRANET_USTAT_COMPANY_SECTION_INVOLVEMENT"] = "功能用法";
$MESS["INTRANET_USTAT_COMPANY_SECTION_TELL_ABOUT"] = "告訴";
