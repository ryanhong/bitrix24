<?php
$MESS["SENDER_CONTACT_IMPORT_BUTTON_LOAD"] = "進口";
$MESS["SENDER_CONTACT_IMPORT_FORMAT_DESC"] = "輸入電子郵件和/或電話號碼，每個條目在新行上。<br> <br>另外，您可以指定名稱：在電子郵件或號碼之後添加半隆並輸入名稱。 <br>示例：<br> <br> john@example.com <br> peter@example.com; peter <br> +0112233445566 <br> +71234567890; ann";
$MESS["SENDER_CONTACT_IMPORT_LOADING"] = "載入中";
$MESS["SENDER_CONTACT_IMPORT_RECIPIENTS"] = "收件人";
$MESS["SENDER_CONTACT_IMPORT_SET"] = "添加到靜態收件人列表";
$MESS["SENDER_CONTACT_IMPORT_SET_ADD"] = "新的";
$MESS["SENDER_CONTACT_IMPORT_SET_NAME"] = "使用名稱";
