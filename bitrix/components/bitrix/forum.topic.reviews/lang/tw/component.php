<?php
$MESS["COMM_COMMENT_OK"] = "評論成功保存了";
$MESS["COMM_COMMENT_OK_AND_NOT_APPROVED"] = "該評論已成功添加。主持人批准後將顯示。";
$MESS["F_ERR_EID_EMPTY"] = "未指定信息塊元素";
$MESS["F_ERR_FID_EMPTY"] = "評論論壇尚未設置";
$MESS["F_ERR_FID_IS_NOT_EXIST"] = "評論論壇＃論壇＃不存在";
$MESS["F_EXPAND_TEXT"] = "添加新評論";
$MESS["F_MESSAGE_TEXT"] = "消息文字";
$MESS["F_MINIMIZE_TEXT"] = "隱藏評論表格";
$MESS["F_NO_MODULE"] = "論壇模塊未安裝";
$MESS["F_NO_MODULE_IBLOCK"] = "信息塊未安裝";
$MESS["NAV_OPINIONS"] = "評論";
