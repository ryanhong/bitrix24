<?php
$MESS["BLOG_BLOG_BLOG_MES_DEL_ERROR"] = "錯誤刪除消息";
$MESS["BLOG_BLOG_BLOG_MES_DEL_NO_RIGHTS"] = "您沒有足夠的權限來刪除此消息";
$MESS["BLOG_BLOG_BLOG_MES_HIDE_ERROR"] = "隱藏帖子的錯誤";
$MESS["BLOG_BLOG_BLOG_MES_HIDE_NO_RIGHTS"] = "您無權隱藏此帖子";
$MESS["BLOG_MODULE_NOT_INSTALL"] = "沒有安裝博客模塊。";
$MESS["B_B_MES_NO_BLOG"] = "沒有找到博客";
$MESS["B_B_MES_NO_MES"] = "沒有找到消息";
$MESS["B_B_MES_NO_POST"] = "未找到帖子。";
$MESS["B_B_MES_NO_RIGHTS"] = "查看消息的權限不足";
$MESS["IDEA_MODULE_NOT_INSTALL"] = "想法模塊未安裝。";
