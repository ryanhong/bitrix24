<?php
$MESS["SRP_LINE_ELEMENT_COUNT"] = "每行項目";
$MESS["SRP_TPL_TEMPLATE_THEME"] = "顏色主題";
$MESS["SRP_TPL_THEME_BLACK"] = "黑暗的";
$MESS["SRP_TPL_THEME_BLUE"] = "藍色（默認主題）";
$MESS["SRP_TPL_THEME_GREEN"] = "綠色的";
$MESS["SRP_TPL_THEME_RED"] = "紅色的";
$MESS["SRP_TPL_THEME_SITE"] = "使用站點主題（用於bitrix.shop）";
$MESS["SRP_TPL_THEME_WOOD"] = "木頭";
$MESS["SRP_TPL_THEME_YELLOW"] = "黃色的";
