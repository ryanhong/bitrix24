<?php
$MESS["CRM_CANCEL_BUTTON"] = "取消";
$MESS["CRM_CLOSE_BUTTON"] = "關閉";
$MESS["CRM_IMPORT_BUTTON"] = "進口";
$MESS["CRM_LOC_IMP_CHOOSE_FILE"] = "選擇位置定義文件";
$MESS["CRM_LOC_IMP_FILE_CNTR"] = "世界（國家）";
$MESS["CRM_LOC_IMP_FILE_FFILE"] = "上傳文件";
$MESS["CRM_LOC_IMP_FILE_NONE"] = "沒有任何";
$MESS["CRM_LOC_IMP_FILE_RS"] = "俄羅斯和前烏斯（城市）";
$MESS["CRM_LOC_IMP_FILE_USA"] = "美國（城市）";
$MESS["CRM_LOC_IMP_JS_ERROR"] = "錯誤";
$MESS["CRM_LOC_IMP_JS_FILE_PROCESS"] = "上傳文件...";
$MESS["CRM_LOC_IMP_JS_IMPORT_PROCESS"] = "導入數據...";
$MESS["CRM_LOC_IMP_JS_IMPORT_SUCESS"] = "數據導入已成功完成。";
$MESS["CRM_LOC_IMP_LOAD_ZIP"] = "上傳郵政編碼數據庫";
$MESS["CRM_LOC_IMP_STEP_CHECK"] = "步長不能為零或負數。";
$MESS["CRM_LOC_IMP_STEP_LENGTH"] = "步長（SEC）";
$MESS["CRM_LOC_IMP_STEP_LENGTH_HINT"] = "在幾秒鐘內指定所需的步長。如果您不確定即將輸入的值足夠，並且腳本不會過早結束，請不要更改此參數。";
$MESS["CRM_LOC_IMP_SYNC"] = "同步";
$MESS["CRM_LOC_IMP_SYNC_N"] = "刪除舊數據";
$MESS["CRM_LOC_IMP_SYNC_NO_ZIP"] = "只有俄羅斯郵政編碼才會同步。";
$MESS["CRM_LOC_IMP_SYNC_Y"] = "嘗試同步現有和新數據";
$MESS["CRM_LOC_IMP_TITLE"] = "進口位置";
