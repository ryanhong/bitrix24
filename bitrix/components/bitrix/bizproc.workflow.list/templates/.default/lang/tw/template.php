<?php
$MESS["BPATT_ALL"] = "全部的";
$MESS["BPATT_AUTO_EXECUTE"] = "汽車";
$MESS["BPATT_HELP1_TEXT_1"] = "以狀態驅動的業務流程是一個連續的業務流程，具有訪問權限分發以處理不同狀態中的元素。";
$MESS["BPATT_HELP2_TEXT_1"] = "順序業務流程是一個簡單的業務流程，可以執行一系列具有元素的連續活動。";
$MESS["BPATT_MODIFIED"] = "修改的";
$MESS["BPATT_NAME"] = "姓名";
$MESS["BPATT_USER"] = "修改";
$MESS["WD_EMPTY"] = "沒有業務流程模板。創建標準<a href=#href#>業務流程</a>。";
$MESS["WD_EMPTY_DEFAULT"] = "沒有業務流程模板。";
