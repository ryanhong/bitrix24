<?php
$MESS["CPD_FEEDBACK_BUTTON"] = "回饋";
$MESS["CPD_NEW_PROPERTY_ADDED"] = "屬性已添加";
$MESS["CPD_NEW_VARIATION_ADDED_MSGVER_1"] = "SKU已添加";
$MESS["CPD_PRODUCT_TYPE_SELECTOR"] = "產品類型：＃product_type_name＃";
$MESS["CPD_QUANTITY_TRACE_ACCEPT"] = "繼續";
$MESS["CPD_QUANTITY_TRACE_NOTICE"] = "一旦啟用庫存控制，它將保留在原位，您將無法將其禁用，因為庫存管理需要它。";
$MESS["CPD_QUANTITY_TRACE_NOTICE_TITLE"] = "啟用庫存控制";
$MESS["CPD_SETTING_DISABLED"] = "選項\“＃名稱＃\”被禁用";
$MESS["CPD_SETTING_ENABLED"] = "選項\“＃名稱＃\”已啟用";
$MESS["CPD_TAB_BALANCE_TITLE"] = "存貨";
$MESS["CPD_TAB_GENERAL_TITLE"] = "常見的";
