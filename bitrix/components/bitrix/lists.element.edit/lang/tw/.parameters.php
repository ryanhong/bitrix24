<?php
$MESS["CP_BLEE_BIZPROC_LOG_URL"] = "業務流程日誌URL";
$MESS["CP_BLEE_BIZPROC_TASK_URL"] = "業務流程任務URL";
$MESS["CP_BLEE_BIZPROC_WORKFLOW_START_URL"] = "業務流程啟動URL";
$MESS["CP_BLEE_ELEMENT_ID"] = "元素";
$MESS["CP_BLEE_IBLOCK_ID"] = "信息塊";
$MESS["CP_BLEE_IBLOCK_TYPE_ID"] = "信息塊類型";
$MESS["CP_BLEE_LISTS_URL"] = "列表主頁URL";
$MESS["CP_BLEE_LIST_ELEMENT_URL"] = "列表元素URL";
$MESS["CP_BLEE_LIST_FILE_URL"] = "文件URL";
$MESS["CP_BLEE_LIST_URL"] = "列表URL";
$MESS["CP_BLEE_SECTION_ID"] = "部分";
