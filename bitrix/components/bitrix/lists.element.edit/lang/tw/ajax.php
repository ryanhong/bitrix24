<?php
$MESS["LISTS_ACCESS_DENIED"] = "未足夠查看和編輯列表的權限。";
$MESS["LISTS_CONNECTION_MODULE_BIZPROC"] = "未安裝業務流程模塊。";
$MESS["LISTS_FOR_SONET_GROUP_DISABLED"] = "該組禁用列表。";
$MESS["LISTS_MESSAGE_SUCCESS"] = "成功";
$MESS["LISTS_NOTIFY_ERROR"] = "錯誤發送通知！";
$MESS["LISTS_NOTIFY_MESSAGE"] = "您能否請您配置工作流的默​​認常數</a>或將此許可委託給我？";
$MESS["LISTS_NOTIFY_SUCCESS"] = "已發送通知";
$MESS["LISTS_NOT_BIZPROC_TEMPLATE_NEW"] = "無法運行工作流程。請確保存在工作流模板並配置為自動運行。";
$MESS["LISTS_STATUS_ACTION_ERROR"] = "這是一個錯誤。";
$MESS["LISTS_STATUS_ACTION_SUCCESS"] = "成功";
$MESS["LISTS_UNKNOWN_ERROR"] = "未知錯誤。";
$MESS["LISTS_WRONG_IBLOCK"] = "指定的列表不正確。";
$MESS["LISTS_WRONG_IBLOCK_TYPE"] = "不正確的信息塊類型。";
