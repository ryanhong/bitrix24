<?php
$MESS["PATH_TO_BASKET_TIP"] = "指定籃子頁面的路徑名。您可以使用<b>購物車</b>組件創建籃子頁面。";
$MESS["PATH_TO_ORDER_TIP"] = "訂單完成頁面的路徑名。如果頁面在當前目錄中，則只能指定文件名。";
