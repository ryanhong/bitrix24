<?php
$MESS["CC_BLFE_ACCESS_DENIED"] = "未足夠查看和編輯列表的權限。";
$MESS["CC_BLFE_BAD_FIELD_ADD_READ_ONLY"] = "需要默認值。";
$MESS["CC_BLFE_BAD_FIELD_NAME"] = "名稱未指定。";
$MESS["CC_BLFE_CHAIN_FIELDS"] = "列表字段";
$MESS["CC_BLFE_CHAIN_LIST_EDIT"] = "網格設置";
$MESS["CC_BLFE_FIELD_NAME_DEFAULT"] = "新領域";
$MESS["CC_BLFE_INVALID_DEFAULT_VALUE"] = "\“默認值\”字段格式不正確";
$MESS["CC_BLFE_INVALID_LIST_VALUE"] = "這兩個列表項目不能具有相同的值";
$MESS["CC_BLFE_LISTS_FOR_SONET_GROUP_DISABLED"] = "該組禁用列表。";
$MESS["CC_BLFE_MODULE_NOT_INSTALLED"] = "未安裝通用列表模塊。";
$MESS["CC_BLFE_TITLE_EDIT"] = "字段參數：＃名稱＃";
$MESS["CC_BLFE_TITLE_NEW"] = "新領域";
$MESS["CC_BLFE_UNKNOWN_ERROR"] = "未知錯誤。";
$MESS["CC_BLFE_WRONG_IBLOCK"] = "指定的列表不正確。";
$MESS["CC_BLFE_WRONG_IBLOCK_TYPE"] = "不正確的信息塊類型。";
$MESS["CC_BLFE_WRONG_LINK_IBLOCK"] = "為\“鏈接到\” \ \ \ \“ element \”屬性指定的錯誤列表。";
