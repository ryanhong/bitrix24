<?php
$MESS["BIZPROCDESIGNER_MODULE_NOT_INSTALLED"] = "業務流程設計器模塊未安裝。";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "未安裝業務流程模塊。";
$MESS["CRM_BP_COMPANY"] = "公司";
$MESS["CRM_BP_CONTACT"] = "接觸";
$MESS["CRM_BP_DEAL"] = "交易";
$MESS["CRM_BP_ENTITY_LIST"] = "類型";
$MESS["CRM_BP_INVOICE"] = "發票";
$MESS["CRM_BP_LEAD"] = "帶領";
$MESS["CRM_BP_LIST_TITLE_EDIT"] = "模板：＃名稱＃";
$MESS["CRM_BP_ORDER"] = "命令";
$MESS["CRM_BP_WFEDIT_TITLE_ADD"] = "＃名稱＃的新工作流模板";
$MESS["CRM_BP_WFEDIT_TITLE_EDIT"] = "編輯＃名稱＃WorkFlow Template \“＃Template＃\”";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
