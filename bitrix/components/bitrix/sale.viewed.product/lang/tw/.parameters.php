<?php
$MESS["VIEWED_ACTION_VARIABLE"] = "包含動作的變量";
$MESS["VIEWED_BASKET_URL"] = "購物車URL";
$MESS["VIEWED_CANBASKET"] = "顯示\“添加到籃子\”按鈕";
$MESS["VIEWED_CANBUY"] = "顯示\“購買\”按鈕";
$MESS["VIEWED_COUNT"] = "東西的個數";
$MESS["VIEWED_CURRENCY"] = "貨幣";
$MESS["VIEWED_DEFAULT"] = "預設";
$MESS["VIEWED_IMAGE"] = "顯示圖像";
$MESS["VIEWED_NAME"] = "顯示名稱";
$MESS["VIEWED_PRICE"] = "顯示價格";
$MESS["VIEWED_PRODUCT_ID_VARIABLE"] = "包含可購買產品ID的變量";
