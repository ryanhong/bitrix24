<?php
$MESS["ALLOW_DELETE_TIP"] = "如果檢查了，將顯示指向刪除可用元素的鏈接。";
$MESS["ALLOW_EDIT_TIP"] = "如果檢查了，將顯示指編輯可用元素的鏈接。";
$MESS["EDIT_URL_TIP"] = "指定元素編輯器頁面的路徑。可以使用<i>元素編輯表單</i>組件創建此頁面。";
$MESS["ELEMENT_ASSOC_PROPERTY_TIP"] = "在此處選擇一個信息塊屬性之一，該屬性將用於區分和將元素作為用戶私人項目顯示。";
$MESS["ELEMENT_ASSOC_TIP"] = "<i>無</i>  - 未結合；所有元素均可向所有用戶使用; <br /> <i> by創建者< /i>  - 只有用戶私人元素才會向用戶顯示; <br /> <i> property < /i>  - 選擇一個信息阻止屬性將選擇元素顯示給用戶。";
$MESS["GROUPS_TIP"] = "在此處指定其成員創建和編輯元素的用戶組。";
$MESS["IBLOCK_ID_TIP"] = "在此處選擇現有信息塊之一。如果選擇<b> <i>（其他）</i> </b>，則必須在旁邊的字段中指定信息塊ID。";
$MESS["IBLOCK_TYPE_TIP"] = "在此處選擇現有的信息塊類型之一。單擊<b> <i> ok </i> </b>加載所選類型的信息塊。如果選擇<b> <i>（其他）</i> </b>，則必須在旁邊的字段中指定信息塊類型ID。";
$MESS["MAX_USER_ENTRIES_TIP"] = "用戶可以創建的最大元素數量。";
$MESS["NAV_ON_PAGE_TIP"] = "每個頁面的最大元素數量。可以通過麵包屑導航獲得其他元素。";
$MESS["SEF_FOLDER_TIP"] = "指定組件工作文件夾。該文件夾可以由真實的文件系統路徑表示，也可以是虛擬的。";
$MESS["SEF_MODE_TIP"] = "檢查此選項啟用SEF模式和URL配置字段。";
$MESS["STATUS_TIP"] = "如果工作流模塊未安裝<b> <b> <b>，您可以允許用戶編輯全部或僅活動的元素。<br /> <br />如果安裝了工作流模塊，則可以指定elements的狀態可以向用戶顯示。";
