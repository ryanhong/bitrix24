<?php
$MESS["CRM_AUTOMATION_QR_DEFAULT_DESCRIPTION"] = "歡迎，[b] #contact_name＃[/b]
很高興見到你。單擊按鈕確認您的訪問。";
$MESS["CRM_AUTOMATION_QR_NOT_FOUND"] = "QR碼缺失或已刪除";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
