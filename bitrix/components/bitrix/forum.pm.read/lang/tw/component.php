<?php
$MESS["F_ERR_SESS_FINISH"] = "您的會議已經過期。請重複操作。";
$MESS["F_NO_MODULE"] = "論壇模塊未安裝。";
$MESS["F_NO_PM"] = "PM禁用。";
$MESS["PM_AUTH"] = "您需要被授權查看此頁面。";
$MESS["PM_ERR_DELETE"] = "消息＃中＃未刪除。";
$MESS["PM_ERR_DELETE_NO_PERM"] = "沒有足夠的權限刪除消息＃中＃。";
$MESS["PM_ERR_MOVE_NO_FOLDER"] = "未指定目標文件夾。";
$MESS["PM_ERR_MOVE_NO_PERM"] = "沒有足夠的權限移動（複製）消息＃中＃。";
$MESS["PM_ERR_NO_DATA"] = "沒有數據。";
$MESS["PM_FOLDER_ID_0"] = "收件箱";
$MESS["PM_FOLDER_ID_1"] = "收件箱";
$MESS["PM_FOLDER_ID_2"] = "剩下的";
$MESS["PM_FOLDER_ID_3"] = "輸出箱";
$MESS["PM_FOLDER_ID_4"] = "回收";
$MESS["PM_NOTIFICATION_SEND"] = "閱讀確認已成功發送";
$MESS["PM_OK_DELETE"] = "消息＃中＃已成功刪除。";
$MESS["PM_OK_MOVE"] = "消息＃中＃被成功移動（複製）。";
$MESS["PM_TITLE"] = "＆laquo;＃主題＃＆raquo;";
$MESS["PM_TITLE_NAV"] = "私人信息";
$MESS["SYSTEM_POST_MESSAGE"] = "您的留言
主題：＃主題＃
已發送：＃message_date＃
消息：http：//＃server_name ## message_link＃

已收件人＃user_name＃已閱讀
http：//＃server_name ## user_link＃";
$MESS["SYSTEM_POST_SUBJ"] = "閱讀確認";
