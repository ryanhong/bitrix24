<?php
$MESS["F_DEFAULT_FID"] = "文件夾ID";
$MESS["F_DEFAULT_MID"] = "消息ID";
$MESS["F_PM_EDIT_TEMPLATE"] = "私人消息編輯（創建）頁面";
$MESS["F_PM_LIST_TEMPLATE"] = "私人消息列表頁面";
$MESS["F_PM_READ_TEMPLATE"] = "私人消息閱讀頁面";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "個人資料頁";
$MESS["F_SET_NAVIGATION"] = "顯示麵包屑導航";
$MESS["F_URL_TEMPLATES"] = "URL處理";
