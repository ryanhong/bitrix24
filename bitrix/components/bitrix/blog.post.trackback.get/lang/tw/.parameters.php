<?php
$MESS["BPTG_BLOG_URL"] = "博客URL";
$MESS["BPTG_BLOG_VAR"] = "博客標識符變量";
$MESS["BPTG_ID"] = "消息ID";
$MESS["BPTG_PAGE_VAR"] = "頁面變量";
$MESS["BPTG_PATH_TO_POST"] = "後路徑模板";
$MESS["BPTG_POST_VAR"] = "博客消息標識符變量";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
