<?php
$MESS["INTRANET_USER_LIST_ACTION_DEACTIVATE_CONFIRM"] = "員工將無法登錄Bitrix24，並且將不再在公司結構中顯示。但是，將保留所有用戶數據（文件，消息，任務等）。

您確定要撤銷用戶訪問嗎？";
$MESS["INTRANET_USER_LIST_ACTION_DEACTIVATE_INVITED_CONFIRM"] = "您無法從BITRIX24刪除受邀用戶，因為對其他實體有綁定。停用（解散）用戶關閉對此Bitrix24的訪問。

您想解僱用戶嗎？";
$MESS["INTRANET_USER_LIST_ACTION_DELETE_CONFIRM"] = "用戶邀請將不可逆轉地刪除。

您確定要刪除員工嗎？";
$MESS["INTRANET_USER_LIST_ACTION_REINVITE_SUCCESS"] = "邀請已發送";
$MESS["INTRANET_USER_LIST_ACTION_RESTORE_CONFIRM"] = "員工將能夠登錄到Bitrix24並出現在公司結構中。

您確定要讓用戶訪問Bitrix24嗎？";
$MESS["INTRANET_USER_LIST_STEPPER_TITLE"] = "重新索引用戶";
