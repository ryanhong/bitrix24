<?php
$MESS["CATALOG_QUANTITY"] = "數量";
$MESS["CT_BCE_CATALOG_ADD"] = "添加到購物車";
$MESS["CT_BCE_CATALOG_ADD_TO_BASKET_OK"] = "添加到您的購物車";
$MESS["CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR"] = "未知錯誤將商品添加到購物車";
$MESS["CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT"] = "查看購物車";
$MESS["CT_BCE_CATALOG_BTN_MESSAGE_CLOSE"] = "關閉";
$MESS["CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP"] = "繼續購物";
$MESS["CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT"] = "比較產品";
$MESS["CT_BCE_CATALOG_BTN_SEND_PROPS"] = "選擇";
$MESS["CT_BCE_CATALOG_BUY"] = "買";
$MESS["CT_BCE_CATALOG_COMMENTARY"] = "評論";
$MESS["CT_BCE_CATALOG_COMMENTS_TAB"] = "評論";
$MESS["CT_BCE_CATALOG_COMPARE"] = "比較";
$MESS["CT_BCE_CATALOG_DESCRIPTION"] = "描述";
$MESS["CT_BCE_CATALOG_DESCRIPTION_TAB"] = "描述";
$MESS["CT_BCE_CATALOG_ECONOMY_INFO2"] = "折扣＃經濟＃";
$MESS["CT_BCE_CATALOG_GIFTS_MAIN_BLOCK_TITLE_DEFAULT"] = "選擇產品並收到禮物";
$MESS["CT_BCE_CATALOG_GIFT_BLOCK_TITLE_DEFAULT"] = "為此產品選擇禮物";
$MESS["CT_BCE_CATALOG_MESS_COMPARE_OK"] = "產品已添加到比較圖表中";
$MESS["CT_BCE_CATALOG_MESS_COMPARE_TITLE"] = "產品對比";
$MESS["CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR"] = "錯誤將產品添加到比較圖表";
$MESS["CT_BCE_CATALOG_MESS_PRICE_TOTAL_PREFIX"] = "全部的";
$MESS["CT_BCE_CATALOG_NOT_AVAILABLE"] = "庫存不可用";
$MESS["CT_BCE_CATALOG_NOT_AVAILABLE_SERVICE"] = "不可用";
$MESS["CT_BCE_CATALOG_PRICE_RANGES_TITLE"] = "價格";
$MESS["CT_BCE_CATALOG_PRODUCT_GIFT_LABEL"] = "禮物";
$MESS["CT_BCE_CATALOG_PROPERTIES"] = "特性";
$MESS["CT_BCE_CATALOG_PROPERTIES_TAB"] = "特性";
$MESS["CT_BCE_CATALOG_RANGE_FROM"] = "來自＃的＃";
$MESS["CT_BCE_CATALOG_RANGE_MORE"] = "和更多";
$MESS["CT_BCE_CATALOG_RANGE_TO"] = "到＃＃";
$MESS["CT_BCE_CATALOG_RATIO_PRICE"] = "＃比率＃的價格";
$MESS["CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW"] = "只剩下幾個";
$MESS["CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY"] = "有存貨";
$MESS["CT_BCE_CATALOG_SHOW_MAX_QUANTITY"] = "庫存";
$MESS["CT_BCE_CATALOG_TITLE_BASKET_PROPS"] = "商品屬性將傳遞給購物車";
$MESS["CT_BCE_CATALOG_TITLE_ERROR"] = "錯誤";
$MESS["CT_BCE_QUANTITY"] = "數量";
