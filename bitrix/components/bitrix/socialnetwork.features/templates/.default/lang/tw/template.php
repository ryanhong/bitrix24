<?php
$MESS["SONET_C4_FUNC_TITLE"] = "啟用了功能“＃名＃”";
$MESS["SONET_C4_FUNC_TITLE_OFF"] = "功能“＃名＃”是禁用的";
$MESS["SONET_C4_FUNC_TITLE_ON"] = "啟用了功能“＃名＃”";
$MESS["SONET_C4_GR_SUCCESS"] = "組參數已成功更改。";
$MESS["SONET_C4_INVITE_OPERATION"] = "允許用戶邀請工作組";
$MESS["SONET_C4_INVITE_OPERATION_PROJECT"] = "允許用戶邀請該項目";
$MESS["SONET_C4_INVITE_TITLE"] = "工作組成員";
$MESS["SONET_C4_INVITE_TITLE_PROJECT"] = "項目成員";
$MESS["SONET_C4_NO_FEATURES"] = "此功能被禁用。您可以在“工作組參數”頁面上啟用它。";
$MESS["SONET_C4_SPAM_OPERATION"] = "許可向小組成員發送消息的權限";
$MESS["SONET_C4_SUBMIT"] = "更新參數";
$MESS["SONET_C4_TASK_FEATURE_DISABLED"] = "您當前的計劃對分配用戶訪問權限有限制。 ＃link_start＃詳細信息＃link_end＃";
$MESS["SONET_C4_T_CANCEL"] = "取消";
$MESS["SONET_C4_T_ERROR"] = "錯誤處理請求。";
$MESS["SONET_C4_US_SUCCESS"] = "用戶參數已成功更改。";
$MESS["SONET_FEATURES_NAME"] = "姓名";
