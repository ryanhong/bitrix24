<?php
$MESS["IMCONNECTOR_COMPONENT_OK_API_KEY"] = "訪問密鑰";
$MESS["IMCONNECTOR_COMPONENT_OK_CHANGE_ANY_TIME"] = "您可以隨時編輯或斷開連接";
$MESS["IMCONNECTOR_COMPONENT_OK_CONNECTED"] = "Odnoklassniki連接";
$MESS["IMCONNECTOR_COMPONENT_OK_CONNECT_HELP"] = "<div class = \“ imconnector-field-button-subtitle \”>我想</div>
<div class = \“ imconnector-field-button-name \”> <span class = \“ imconnector-field-box-text-bold \”> connect </span> odnoklassniki group </div>";
$MESS["IMCONNECTOR_COMPONENT_OK_CONNECT_STEP_NEW"] = "連接之前，您必須＃link_start＃獲取OG組的訪問密鑰＃link_end＃。我們可以幫助您創建一個組並將其連接到您的Bitrix24。";
$MESS["IMCONNECTOR_COMPONENT_OK_CONNECT_TITLE"] = "將odnoklassniki連接到打開頻道";
$MESS["IMCONNECTOR_COMPONENT_OK_FINAL_FORM_DESCRIPTION"] = "Odnoklassniki已連接到您的開放頻道。從現在開始，發送給您的工作組的所有消息都將轉發到您的Bitrix24。";
$MESS["IMCONNECTOR_COMPONENT_OK_GROUP_LINK"] = "工作組鏈接";
$MESS["IMCONNECTOR_COMPONENT_OK_GROUP_NAME"] = "工作組名稱";
$MESS["IMCONNECTOR_COMPONENT_OK_INDEX_ADDITIONAL_DESCRIPTION_NEW"] = "連接之前，您必須＃link_start＃獲取OG組的訪問密鑰＃link_end＃。我們可以幫助您創建一個組並將其連接到您的Bitrix24。";
$MESS["IMCONNECTOR_COMPONENT_OK_INDEX_LIST_ITEM_1"] = "將聯繫人和通信歷史保存到CRM";
$MESS["IMCONNECTOR_COMPONENT_OK_INDEX_LIST_ITEM_2"] = "指導客戶通過CRM中的銷售渠道";
$MESS["IMCONNECTOR_COMPONENT_OK_INDEX_LIST_ITEM_3"] = "回复您的客戶何時何地";
$MESS["IMCONNECTOR_COMPONENT_OK_INDEX_LIST_ITEM_4"] = "根據隊列規則，客戶查詢分佈在銷售代理之間";
$MESS["IMCONNECTOR_COMPONENT_OK_INDEX_SUBTITLE"] = "將您的小組連接到Bitrix24，以接收OG客戶的查詢。更快地回复並改善轉換。";
$MESS["IMCONNECTOR_COMPONENT_OK_INDEX_TITLE"] = "回复您的OG客戶的問題";
$MESS["IMCONNECTOR_COMPONENT_OK_INFO"] = "資訊";
$MESS["IMCONNECTOR_COMPONENT_OK_INSTRUCTION_TITLE"] = "<span class = \“ imconnector-field-box-text-bold \”>如何獲得</span>訪問密鑰：";
$MESS["IMCONNECTOR_COMPONENT_OK_TESTED"] = "測試連接";
