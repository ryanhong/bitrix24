<?php
$MESS["CRM_QUOTE_LIST_REBUILD_SEARCH_CONTENT_COMPLETED_SUMMARY"] = "報價搜索索引已重新創建。引號處理：＃processed_items＃。";
$MESS["CRM_QUOTE_LIST_REBUILD_SEARCH_CONTENT_NOT_REQUIRED_SUMMARY"] = "報價搜索索引不需要重新創建。";
$MESS["CRM_QUOTE_LIST_REBUILD_SEARCH_CONTENT_PROGRESS_SUMMARY"] = "引號處理：＃processed_items＃of＃total_items＃。";
$MESS["CRM_QUOTE_LIST_ROW_COUNT"] = "總計：＃row_count＃";
