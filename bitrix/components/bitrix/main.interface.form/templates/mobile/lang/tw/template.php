<?php
$MESS["MPF_CANCEL"] = "取消";
$MESS["MPF_ERROR1"] = "文件沒有上傳。";
$MESS["MPF_PHOTO_CAMERA"] = "拍照";
$MESS["MPF_PHOTO_GALLERY"] = "從畫廊中選擇";
$MESS["interface_form_add"] = "添加";
$MESS["interface_form_cancel"] = "取消";
$MESS["interface_form_change"] = "改變";
$MESS["interface_form_from"] = "從";
$MESS["interface_form_hide"] = "隱藏";
$MESS["interface_form_save"] = "節省";
$MESS["interface_form_select"] = "選擇";
$MESS["interface_form_show_more"] = "展示更多";
$MESS["interface_form_to"] = "到";
