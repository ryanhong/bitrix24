<?php
$MESS["BC_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["BPH_BLOG_URL"] = "博客URL";
$MESS["BPH_BLOG_VAR"] = "博客標識符變量";
$MESS["BPH_ID"] = "消息ID";
$MESS["BPH_PAGE_VAR"] = "頁面變量";
$MESS["BPH_PATH_TO_TRACKBACK"] = "回顧路徑模板";
$MESS["BPH_POST_VAR"] = "博客消息標識符變量";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
