<?php
$MESS["TASKS_CANNOT_ADD_DEPENDENCY"] = "無法鏈接這些任務";
$MESS["TASKS_CLOSE_PAGE_CONFIRM"] = "您所做的更改可能會丟失。";
$MESS["TASKS_DELETE_SUCCESS"] = "任務已刪除";
$MESS["TASKS_GANTT_MONTH_APR"] = "四月";
$MESS["TASKS_GANTT_MONTH_AUG"] = "八月";
$MESS["TASKS_GANTT_MONTH_DEC"] = "十二月";
$MESS["TASKS_GANTT_MONTH_FEB"] = "二月";
$MESS["TASKS_GANTT_MONTH_JAN"] = "一月";
$MESS["TASKS_GANTT_MONTH_JUL"] = "七月";
$MESS["TASKS_GANTT_MONTH_JUN"] = "六月";
$MESS["TASKS_GANTT_MONTH_MAR"] = "行進";
$MESS["TASKS_GANTT_MONTH_MAY"] = "可能";
$MESS["TASKS_GANTT_MONTH_NOV"] = "十一月";
$MESS["TASKS_GANTT_MONTH_OCT"] = "十月";
$MESS["TASKS_GANTT_MONTH_SEP"] = "九月";
$MESS["TASKS_GANTT_PRINT_SPOTLIGHT_TEXT"] = "現在，您可以打印圖表並設置每小時的時間範圍。";
$MESS["TASKS_LIST_TRIAL_EXPIRED_TEXT"] = "免費計劃僅支持五個任務依賴性。啟用擴展任務使用
無限依賴。<br>
<br>
支持四種依賴性類型：<br>
經典依賴（完成>開始）<br>
同時任務開始（開始>開始）<br>
同時任務截止日期（完成>完成）<br>
開始完成（開始>完成）<br>
<br>
態
<br>
選定的商業計劃中提供了擴展任務，擴展的CRM和擴展電話。";
$MESS["TASKS_LIST_TRIAL_EXPIRED_TITLE_V2"] = "僅在擴展任務中可用";
$MESS["TASKS_TITLE"] = "任務";
$MESS["TASKS_TITLE_GROUP_TASKS"] = "工作組任務";
$MESS["TASKS_TITLE_MY"] = "我的任務";
$MESS["TASKS_TITLE_PROJECT"] = "專案";
