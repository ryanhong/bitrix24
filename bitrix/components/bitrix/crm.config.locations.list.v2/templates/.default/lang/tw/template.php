<?php
$MESS["CRM_CLL2_ALL"] = "全部的";
$MESS["CRM_CLL2_DELETE"] = "刪除位置";
$MESS["CRM_CLL2_DELETE_CONFIRM"] = "您確定要刪除“％s”？";
$MESS["CRM_CLL2_DELETE_TITLE"] = "刪除此位置";
$MESS["CRM_CLL2_EDIT"] = "編輯位置";
$MESS["CRM_CLL2_EDIT_TITLE"] = "打開此位置進行編輯";
$MESS["CRM_CLL2_NOT_SELECTED"] = "未選中的";
$MESS["CRM_CLL2_VIEW_SUBTREE"] = "查看兒童位置";
$MESS["CRM_CLL2_VIEW_SUBTREE_TITLE"] = "查看兒童位置";
