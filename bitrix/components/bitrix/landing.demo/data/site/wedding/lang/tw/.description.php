<?php
$MESS["LANDING_DEMO_WEDDING_DESCRIPTION"] = "專業的婚禮策劃師的展示必須向您的潛在客戶表明，在他們一生中最重要的一天中，一切都會好起來的。創建您的網站並吸引更多客戶。";
$MESS["LANDING_DEMO_WEDDING_TITLE"] = "婚禮服務";
