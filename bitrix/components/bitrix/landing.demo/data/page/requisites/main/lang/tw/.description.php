<?php
$MESS["LANDING_DEMO_PAGE_REQUISITES_DESCRIPTION"] = "您想了解的有關我們公司的一切。";
$MESS["LANDING_DEMO_PAGE_REQUISITES_NAME"] = "登陸頁面";
$MESS["LANDING_DEMO_PAGE_REQUISITES_TEXT_1"] = "#RequisiteCompanyTitle是自動化業務的可靠合作夥伴。我們為任何行業的公司創建創新的解決方案，以優化工作流程並提高效率。";
