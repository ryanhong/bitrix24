<?php
$MESS["LANDING_DEMO___NEWYEAR-DESCRIPTION"] = "新年是最著名的假期之一 - 我們親吻過去的再見，並等待新的一年開始。使用美麗的新模板贈送新年禮物並吸引更多客戶！";
$MESS["LANDING_DEMO___NEWYEAR-TITLE"] = "新年";
