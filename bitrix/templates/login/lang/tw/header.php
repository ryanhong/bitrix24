<?php
$MESS["BITRIX24_COPYRIGHT"] = "＆複製; 2001-＃current_year＃bitrix24。由Bitrix24提供動力。";
$MESS["BITRIX24_COPYRIGHT_B24"] = "＆複製; 2001-＃current_year＃bitrix24";
$MESS["BITRIX24_LANG_DE"] = "德意志";
$MESS["BITRIX24_LANG_EN"] = "英語";
$MESS["BITRIX24_LANG_INTERFACE"] = "語言";
$MESS["BITRIX24_LANG_LA"] = "西班牙語";
$MESS["BITRIX24_LANG_RU"] = "俄語";
$MESS["BITRIX24_LANG_UA"] = "烏克蘭";
$MESS["BITRIX24_MOTTO"] = "社會內在網";
$MESS["BITRIX24_TITLE"] = "Bitrix24";
$MESS["BITRIX24_URL"] = "http://www.bitrix24.com";
