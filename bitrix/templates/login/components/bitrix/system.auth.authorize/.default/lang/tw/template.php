<?php
$MESS["AUTH_1_SOCSERV"] = "使用";
$MESS["AUTH_2_FORM"] = "或輸入您的登錄和密碼：";
$MESS["AUTH_AUTHORIZE"] = "登入";
$MESS["AUTH_AUTHORIZE_BY_QR"] = "使用QR代碼登錄";
$MESS["AUTH_AUTHORIZE_BY_QR_INFO_1"] = "使用QR代碼登錄";
$MESS["AUTH_AUTHORIZE_BY_QR_INFO_2"] = "使用Bitrix24移動應用程序掃描此操作，或使用您的移動相機";
$MESS["AUTH_BACK"] = "後退";
$MESS["AUTH_CAPTCHA_PROMT"] = "從圖像輸入文字";
$MESS["AUTH_FIRST_ONE"] = "如果您是網站上的首次訪問者，請填寫註冊表。";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "忘記密碼了嗎？";
$MESS["AUTH_LOGIN"] = "登入";
$MESS["AUTH_NONSECURE_NOTE"] = "密碼將以開放的形式發送。在您的Web瀏覽器中啟用JavaScript啟用密碼加密。";
$MESS["AUTH_PASSWORD"] = "密碼";
$MESS["AUTH_PLEASE_AUTH"] = "請授權：";
$MESS["AUTH_REGISTER"] = "登記";
$MESS["AUTH_REMEMBER_ME"] = "在這台電腦上記住我";
$MESS["AUTH_SECURE_NOTE"] = "密碼將在發送之前進行加密。這將防止密碼以數據傳輸通道以開放形式出現。";
$MESS["AUTH_SOCSERV"] = "使用社會服務登錄：";
$MESS["AUTH_TITLE"] = "歡迎！";
