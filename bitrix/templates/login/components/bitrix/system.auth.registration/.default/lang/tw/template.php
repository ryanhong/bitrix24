<?php
$MESS["AUTH_AUTH"] = "授權";
$MESS["AUTH_CONFIRM"] = "確認密碼：";
$MESS["AUTH_EMAIL"] = "電子郵件：";
$MESS["AUTH_EMAIL_SENT"] = "註冊確認請求已發送到指定的電子郵件地址。";
$MESS["AUTH_EMAIL_WILL_BE_SENT"] = "註冊確認請求將發送到指定的電子郵件地址。";
$MESS["AUTH_LAST_NAME"] = "姓：";
$MESS["AUTH_LOGIN_MIN"] = "登錄（Min。3char。）";
$MESS["AUTH_NAME"] = "姓名：";
$MESS["AUTH_NONSECURE_NOTE"] = "密碼將以開放的形式發送。在您的Web瀏覽器中啟用JavaScript啟用密碼加密。";
$MESS["AUTH_PASSWORD_REQ"] = "密碼：";
$MESS["AUTH_REGISTER"] = "登記";
$MESS["AUTH_REQ"] = "必需的字段";
$MESS["AUTH_SECURE_NOTE"] = "密碼將在發送之前進行加密。這將防止密碼以數據傳輸通道以開放形式出現。";
$MESS["CAPTCHA_REGF_PROMT"] = "從圖像輸入文字";
$MESS["CAPTCHA_REGF_TITLE"] = "請輸入您在圖像中看到的單詞";
