<?php
$MESS["AUTH_OTP_AUTHORIZE"] = "登入";
$MESS["AUTH_OTP_AUTH_BACK"] = "返回登錄屏幕";
$MESS["AUTH_OTP_BACK"] = "後退";
$MESS["AUTH_OTP_CAPTCHA_PROMT"] = "輸入您在圖片上看到的字符";
$MESS["AUTH_OTP_HELP_LINK"] = "需要幫忙？";
$MESS["AUTH_OTP_HELP_TEXT"] = "
為了使您的BitRix24數據更安全，您的管理員啟用了額外的安全選項：兩步身份驗證。<br/> <br/>
兩步身份驗證涉及兩個隨後的驗證階段。第一個需要您的主密碼。第二階段包括發送到您的手機（或使用硬件加密狗）的一次性代碼。<br/> <br/>
<div class = \“ login-text-img \”> <img src = \“＃路徑＃/images/en en/img1.png \”> </div>
<br/>
啟用了兩步身份驗證後，您將必須通過兩個身份驗證屏幕：<br/> <br/>
 - 輸入您的電子郵件和密碼; <br/>
 - 然後，輸入一個一次性代碼，可以使用您在啟用兩步身份驗證時已安裝的Bitrix OTP移動應用程序獲得。<br/> <br/>
在手機上運行該應用程序：<br/> <br/>
<div class = \“ login-text-img \”> <img src = \“＃路徑＃/images/en en/img2.png \”> </div> </div>
<br/>
輸入您在屏幕上看到的代碼：<br/> <br/>
<div class = \“ login-text-img \”> <img src = \“＃路徑＃/images/en en/img3.png \”> </div> </div>
<br/>
如果您使用多個Bitrix24，請在輸入之前確保代碼適用於正確的Bitrix24。<br/> <br/>
<div class = \“ login-text-img \”> <img src = \“＃路徑＃/images/en en/img4.png \”> </div>
<br/>
如果您從來沒有機會啟用和配置兩步身份驗證或丟失手機，請聯繫您的管理員以還原對Bitrix24的訪問。<br/> <br/>
如果您是管理員，請聯繫<a href= \之http://www.bitrix24.com/support/helpdesk/flow\"> bitrix helpdesk </a>以還原訪問權限。";
$MESS["AUTH_OTP_HELP_TITLE"] = "兩步身份驗證";
$MESS["AUTH_OTP_OTP"] = "輸入您的一次性驗證代碼";
$MESS["AUTH_OTP_PLEASE_AUTH"] = "登入";
$MESS["AUTH_OTP_REMEMBER_ME"] = "記住這台計算機上的一次性密碼";
