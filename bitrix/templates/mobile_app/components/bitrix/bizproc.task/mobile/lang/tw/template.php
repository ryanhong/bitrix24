<?php
$MESS["BPATL_DOC_HISTORY"] = "業務流程日誌";
$MESS["BPATL_SUCCESS"] = "作業完成。";
$MESS["BPATL_TASK_TITLE"] = "任務";
$MESS["BPATL_USER_STATUS_NO"] = "你拒絕了文件";
$MESS["BPATL_USER_STATUS_OK"] = "您閱讀了文檔";
$MESS["BPATL_USER_STATUS_YES"] = "您批准了文件";
$MESS["BPAT_COMMENT"] = "評論";
$MESS["BPAT_GOTO_DOC"] = "轉到文檔";
$MESS["MB_BP_DETAIL_ALERT"] = "錯誤";
$MESS["MB_BP_DETAIL_PULLDOWN_DOWN"] = "釋放刷新";
$MESS["MB_BP_DETAIL_PULLDOWN_LOADING"] = "更新數據...";
$MESS["MB_BP_DETAIL_PULLDOWN_PULL"] = "下拉以刷新";
$MESS["MB_BP_DETAIL_REQUEST_ERROR"] = "執行任務時發送請求的錯誤。";
$MESS["MB_BP_SKIP_MSGVER_1"] = "作業只能在Web版本中競爭。請從您的計算機登錄您的帳戶。";
