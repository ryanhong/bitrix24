<?php
$MESS["ERR_DEFAULT"] = "發生了錯誤";
$MESS["ERR_FORBIDDEN_TEXT"] = "您無權查看此頁面。<br>請聯繫消息作者解決該問題。";
$MESS["ERR_FORBIDDEN_TITLE"] = "權限不足";
$MESS["ERR_NOT_FOUND_TEXT"] = "不幸的是，找不到要求的頁面。<br>請聯繫消息作者解決問題。";
$MESS["ERR_NOT_FOUND_TITLE"] = "什麼都沒找到";
$MESS["ERR_NO_CONTENT_TEXT"] = "頁面內容沒有加載，<br>單擊鏈接<a href= \"#target# \ \">重新加載</a>。";
$MESS["ERR_NO_CONTENT_TITLE"] = "無法顯示電子郵件內容";
$MESS["ERR_SOME"] = "出了點問題。";
$MESS["ERR_UNAUTHORIZED_TEXT"] = "不幸的是，此鏈接已過期。<br>請再次使用電子郵件中的鏈接繼續工作。";
$MESS["ERR_UNAUTHORIZED_TITLE"] = "在電子郵件中使用鏈接";
$MESS["POWERED_BY"] = "供電";
