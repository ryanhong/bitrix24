<?php
$MESS["BX24_BUTTON"] = "轉移";
$MESS["BX24_CLOSE_BUTTON"] = "關閉";
$MESS["BX24_LOADING"] = "載入中";
$MESS["BX24_TITLE"] = "從外部集轉到Intranet";
$MESS["BX24_TITLE_EMAIL"] = "轉移到Intranet";
$MESS["SOCNET_CONFIRM_DELETE"] = "邀請將不可撤銷地刪除。

您確定要刪除員工嗎？";
$MESS["SOCNET_CONFIRM_FIRE"] = "員工將無法登錄到門戶，不會在公司結構中顯示。但是，他或她的所有個人數據（文件，消息，任務等）將保持完整。

您確定要關閉該員工的訪問權限嗎？";
$MESS["SOCNET_CONFIRM_RECOVER"] = "員工將能夠登錄到門戶網站，並將在公司結構中顯示。

您確定要為此員工打開訪問權限嗎？";
$MESS["SONET_ACTIONS"] = "動作";
$MESS["SONET_ACTIVITY_STATUS"] = "地位";
$MESS["SONET_ADDITIONAL_EMPTY"] = "沒有其他信息";
$MESS["SONET_ADDITIONAL_TITLE"] = "附加信息";
$MESS["SONET_C38_TP_NO_PERMS"] = "您無權查看此用戶的配置文件。";
$MESS["SONET_COMMON_EMPTY"] = "沒有一般信息";
$MESS["SONET_COMMON_TITLE"] = "一般信息";
$MESS["SONET_CONTACT_EMPTY"] = "沒有聯繫信息";
$MESS["SONET_CONTACT_TITLE"] = "聯繫信息";
$MESS["SONET_DEACTIVATE"] = "解僱";
$MESS["SONET_DELETE"] = "刪除";
$MESS["SONET_EDIT_PROFILE"] = "編輯個人資料";
$MESS["SONET_EMAIL_FORWARD_TO"] = "將消息轉發給您的Bitrix24";
$MESS["SONET_EMAIL_FORWARD_TO_BLOG_POST"] = "為活動流添加帖子";
$MESS["SONET_EMAIL_FORWARD_TO_HINT"] = "複製適當的電子郵件地址，然後將您的郵件轉發在此處，以便在活動流或新任務中創建新帖子。<br/> <br/>很重要！該帖子將與您聯繫。如果您想與其他Bitrix24用戶討論電子郵件內容，則需要在發布帖子後添加新收件人。創建新任務時，您將被分配為負責人。<br/> <br/>電子郵件正文和所有附件都會自動將其發佈到您的活動流或任務中。";
$MESS["SONET_EMAIL_FORWARD_TO_SHOW"] = "展示";
$MESS["SONET_EMAIL_FORWARD_TO_TASK"] = "創建任務";
$MESS["SONET_ERROR_DELETE"] = "錯誤！用戶未刪除。";
$MESS["SONET_EXTRANET_TO_INTRANET"] = "轉移到Intranet";
$MESS["SONET_EXTRANET_TO_INTRANET_EMAIL"] = "轉移到Intranet";
$MESS["SONET_GROUPS"] = "組";
$MESS["SONET_LAST_SEEN_F"] = "最後一個看到＃last_seen＃";
$MESS["SONET_LAST_SEEN_IDLE_F"] = "自從＃last_seen＃閒置";
$MESS["SONET_LAST_SEEN_IDLE_M"] = "自從＃last_seen＃閒置";
$MESS["SONET_LAST_SEEN_M"] = "最後一個看到＃last_seen＃";
$MESS["SONET_MANAGERS"] = "導師";
$MESS["SONET_OTP_ACTIVATE"] = "使能夠";
$MESS["SONET_OTP_ACTIVE"] = "啟用";
$MESS["SONET_OTP_AUTH"] = "兩步身份驗證";
$MESS["SONET_OTP_CHANGE_PHONE"] = "設置新電話";
$MESS["SONET_OTP_CODES"] = "恢復代碼";
$MESS["SONET_OTP_CODES_SHOW"] = "看法";
$MESS["SONET_OTP_DEACTIVATE"] = "禁用";
$MESS["SONET_OTP_LEFT_DAYS"] = "（將在＃num＃中啟用）";
$MESS["SONET_OTP_NOT_ACTIVE"] = "禁用";
$MESS["SONET_OTP_NOT_EXIST"] = "未配置";
$MESS["SONET_OTP_NO_DAYS"] = "總是";
$MESS["SONET_OTP_PROROGUE"] = "延遲";
$MESS["SONET_OTP_SETUP"] = "配置";
$MESS["SONET_OTP_SUCCESS_POPUP_CLOSE"] = "關閉";
$MESS["SONET_OTP_SUCCESS_POPUP_PASSWORDS"] = "配置應用程序密碼";
$MESS["SONET_OTP_SUCCESS_POPUP_TEXT"] = "<b>恭喜！</b>
<br/> <br/>
您已經為您的帳戶設置了兩步身份驗證。
<br/> <br/>
兩步身份驗證涉及兩個隨後的驗證階段。第一個需要您的主密碼。第二階段包括您必須在第二個身份驗證屏幕上輸入的一次性代碼。
<br/> <br/>
請注意，您將必須使用應用程序密碼進行安全數據交換（例如，使用外部日曆）。您將找到在用戶配置文件中管理應用程序密碼的工具。
<br/> <br/>";
$MESS["SONET_OTP_SUCCESS_POPUP_TEXT2"] = "記住要確保您的恢復代碼。如果您丟失了移動設備或由於任何其他原因無法獲得代碼，則可能需要它們。";
$MESS["SONET_OTP_SUCCESS_POPUP_TEXT_NEW"] = "<b>恭喜！</b>
<br/> <br/>
您現在為您的帳戶設置了兩步身份驗證。
<br/> <br/>";
$MESS["SONET_OTP_SUCCESS_POPUP_TEXT_NEW2"] = "請注意，與外部服務（日曆等）交換數據將需要使用特定的密碼。您可以管理個人資料中的這些。";
$MESS["SONET_OTP_SUCCESS_POPUP_TEXT_RES_CODE"] = "請記住要保留<b>恢復代碼</b>如果您的移動設備丟失或無法獲得一次性代碼，則可能需要登錄。<br/> <br/> <br/>";
$MESS["SONET_PASSWORDS"] = "應用程序密碼";
$MESS["SONET_PASSWORDS_SETTINGS"] = "設定";
$MESS["SONET_REINVITE"] = "再次邀請";
$MESS["SONET_REINVITE_ACCESS"] = "邀請已發送";
$MESS["SONET_RESTORE"] = "聘請";
$MESS["SONET_SECURITY"] = "安全";
$MESS["SONET_SEND_MESSAGE"] = "發信息";
$MESS["SONET_SOCSERV_CONTACTS"] = "我的聯繫方式";
$MESS["SONET_SONET_ADMIN_ON"] = "管理模式";
$MESS["SONET_SUBORDINATE"] = "下屬";
$MESS["SONET_SYNCHRONIZE"] = "同步";
$MESS["SONET_USER_ABSENCE"] = "缺席";
$MESS["SONET_USER_STATUS"] = "地位";
$MESS["SONET_USER_admin"] = "行政人員";
$MESS["SONET_USER_extranet"] = "外部";
$MESS["SONET_USER_fired"] = "不活動";
$MESS["SONET_USER_integrator"] = "Bitrix24合作夥伴";
$MESS["SONET_USER_invited"] = "被邀請";
$MESS["SONET_VIDEO_CALL"] = "視訊通話";
