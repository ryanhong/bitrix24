<?php
$MESS["interface_filter_days"] = "d。";
$MESS["interface_filter_note"] = "根據過濾標準顯示結果。";
$MESS["interface_filter_note_clear"] = "清除過濾器";
$MESS["interface_grid_additional"] = "更多過濾器";
$MESS["interface_grid_find"] = "搜尋";
$MESS["interface_grid_find_title"] = "查找匹配搜索標準的記錄";
$MESS["interface_grid_flt_cancel"] = "取消";
$MESS["interface_grid_flt_cancel_title"] = "顯示所有記錄";
$MESS["interface_grid_hide_all"] = "隱藏所有過濾器";
$MESS["interface_grid_no_no_no"] = "（不）";
$MESS["interface_grid_search"] = "搜尋";
$MESS["interface_grid_show_all"] = "顯示所有過濾器";
$MESS["main_interface_filter_save"] = "另存為...";
$MESS["main_interface_filter_save_title"] = "保存當前過濾器";
$MESS["main_interface_filter_saved"] = "保存的過濾器";
$MESS["main_interface_filter_saved_apply"] = "應用保存的過濾器";
