<?php
$MESS["CAT_1C_DEACTIVATE"] = "停用";
$MESS["CAT_1C_DELETE"] = "刪除";
$MESS["CAT_1C_GROUP_PERMISSIONS"] = "用戶組允許導入";
$MESS["CAT_1C_NONE"] = "沒有任何";
$MESS["SALE_1C_EXPORT_ALLOW_DELIVERY_ORDERS"] = "僅出口訂單";
$MESS["SALE_1C_EXPORT_FINAL_ORDERS"] = "狀態出口訂單";
$MESS["SALE_1C_EXPORT_PAYED_ORDERS"] = "僅出口訂單";
$MESS["SALE_1C_FINAL_STATUS_ON_DELIVERY"] = "從1C收購交貨後的訂單狀態";
$MESS["SALE_1C_GROUP_PERMISSIONS"] = "用戶組允許導出";
$MESS["SALE_1C_NO"] = "<未選擇>";
$MESS["SPS_JCITY"] = "城市";
$MESS["SPS_JCOUNTRY"] = "國家";
$MESS["SPS_ORDER_CURRENCY"] = "貨幣";
$MESS["SPS_ORDER_DATE"] = "訂購日期（無時間）";
$MESS["SPS_ORDER_DATETIME"] = "訂購日期";
$MESS["SPS_ORDER_DELIV"] = "送貨服務ID";
$MESS["SPS_ORDER_DESCOUNT"] = "折扣金額";
$MESS["SPS_ORDER_ID"] = "訂購ID";
$MESS["SPS_ORDER_PRICE"] = "訂單金額";
$MESS["SPS_ORDER_PRICE_DELIV"] = "運輸成本";
$MESS["SPS_ORDER_PS"] = "付款系統ID";
$MESS["SPS_ORDER_SITE"] = "地點";
$MESS["SPS_ORDER_SUM"] = "訂單價格";
$MESS["SPS_ORDER_TAX"] = "稅額";
$MESS["SPS_ORDER_USER_ID"] = "客戶ID";
$MESS["SPS_USER_ADDRESS"] = "地址";
$MESS["SPS_USER_CITY"] = "城市";
$MESS["SPS_USER_COMPANY"] = "公司";
$MESS["SPS_USER_COM_ADDRESS"] = "公司地址";
$MESS["SPS_USER_COM_CITY"] = "公司城市";
$MESS["SPS_USER_COM_COUNTRY"] = "公司國家";
$MESS["SPS_USER_COM_FAX"] = "工作傳真";
$MESS["SPS_USER_COM_PHONE"] = "工作電話";
$MESS["SPS_USER_COM_POST"] = "公司P.O.盒子";
$MESS["SPS_USER_COM_STATE"] = "公司國家";
$MESS["SPS_USER_COM_WEB"] = "公司網站";
$MESS["SPS_USER_COM_ZIP"] = "ZIP公司";
$MESS["SPS_USER_COUNTRY"] = "國家";
$MESS["SPS_USER_DEPT"] = "部門";
$MESS["SPS_USER_DOL"] = "位置";
$MESS["SPS_USER_FAX"] = "傳真";
$MESS["SPS_USER_ICQ"] = "ICQ";
$MESS["SPS_USER_ID"] = "用戶身份";
$MESS["SPS_USER_LAST_NAME"] = "姓";
$MESS["SPS_USER_LOGIN"] = "登入";
$MESS["SPS_USER_NAME"] = "姓名";
$MESS["SPS_USER_PHONE"] = "電話";
$MESS["SPS_USER_POST"] = "盒子";
$MESS["SPS_USER_PROF"] = "職業";
$MESS["SPS_USER_SEX"] = "性別";
$MESS["SPS_USER_SITE"] = "註冊地點";
$MESS["SPS_USER_STATE"] = "狀態";
$MESS["SPS_USER_WEB"] = "個人網站";
$MESS["SPS_USER_ZIP"] = "壓縮";
$MESS["SSEN_PERM_CANCEL"] = "更改\“取消\”選項";
$MESS["SSEN_PERM_DELETE"] = "刪除";
$MESS["SSEN_PERM_DELIVERY"] = "送貨標誌";
$MESS["SSEN_PERM_MARK"] = "更改\“與訂單\”選項的問題";
$MESS["SSEN_PERM_PAYMENT"] = "更改\'訂單已付\”標誌";
$MESS["SSEN_PERM_STATUS"] = "將訂單移至此狀態";
$MESS["SSEN_PERM_STATUS_FROM"] = "從此狀態移動訂單";
$MESS["SSEN_PERM_UPDATE"] = "編輯";
$MESS["SSEN_PERM_VIEW"] = "在此狀態中查看訂單";
$MESS["WW_1"] = "電子商店配置";
$MESS["WW_2"] = "您的電子商店偏好摘要：";
$MESS["WW_3"] = "用戶組允許處理訂單：";
$MESS["WW_4"] = "付款人類型";
$MESS["WW_5"] = "支付系統";
$MESS["WW_6"] = "送貨服務";
$MESS["WW_7"] = "電子商店已成功配置。";
$MESS["WW_8"] = "您可以在控制面板的E店部分中更改所有選定的設置。";
$MESS["WW_9"] = "巫師被打斷了";
$MESS["WW_10"] = "巫師被打斷了。更改沒有保存。<br /> <br />要配置電子商店，您將不得不再次運行嚮導。";
$MESS["WW_CLOSE"] = "關閉";
$MESS["WW_STEP0"] = "E商店配置嚮導";
$MESS["WW_STEP1"] = "一般設置（第8步）";
$MESS["WW_STEP1_1"] = "為您的互聯網商店指定一般設置。";
$MESS["WW_STEP1_2"] = "商店的網站";
$MESS["WW_STEP1_3"] = "電子郵件以獲取訂單信息：";
$MESS["WW_STEP1_4"] = "保存無薪籃子物品的日子：";
$MESS["WW_STEP1_5"] = "電子貨幣貨幣：";
$MESS["WW_STEP2"] = "付款人類型（第8步）";
$MESS["WW_STEP2_1"] = "您尚未選擇付款人類型";
$MESS["WW_STEP2_2"] = "選擇將在您的網站上使用的付款人類型：";
$MESS["WW_STEP3"] = "支付系統（第8步，第3步）";
$MESS["WW_STEP3_1"] = "選擇將在每種付款人類型的網站上使用的支付系統：";
$MESS["WW_STEP4"] = "支付系統設置（第4步，第4步）";
$MESS["WW_STEP4_1"] = "配置付款系統參數（表格項目，商店ID等）。這樣做需要您為數據檢索指定必要的訂單字段。";
$MESS["WW_STEP4_2"] = "參數配置";
$MESS["WW_STEP4_3"] = "隱藏";
$MESS["WW_STEP4_4"] = "在新窗口中打開";
$MESS["WW_STEP4_5"] = "訂單屬性";
$MESS["WW_STEP4_6"] = "訂購設置";
$MESS["WW_STEP4_7"] = "用戶參數";
$MESS["WW_STEP4_8"] = "其他";
$MESS["WW_STEP5"] = "許可設置（第8步，第5步）";
$MESS["WW_STEP5_1"] = "您尚未選擇用戶組";
$MESS["WW_STEP5_2"] = "選擇將管理員訪問該網站訂單的用戶組：";
$MESS["WW_STEP6"] = "狀態權限設置（第8步，第6步）";
$MESS["WW_STEP6_1"] = "指定每個狀態中訂單的用戶組訪問權限。";
$MESS["WW_STEP6_2"] = "訂購權限";
$MESS["WW_STEP7"] = "電子郵件未指定";
$MESS["WW_STEP7_1"] = "密碼確認不匹配。";
$MESS["WW_STEP7_2"] = "密碼必須至少包含6個符號。";
$MESS["WW_STEP7_3"] = "密碼必須至少包含3個符號。";
$MESS["WW_STEP7_4"] = "電子郵件地址錯誤。";
$MESS["WW_STEP7_5"] = "會計計劃集成設置（第8步，第7步）";
$MESS["WW_STEP7_6"] = "過程訂單";
$MESS["WW_STEP7_7"] = "現場";
$MESS["WW_STEP7_8"] = "在會計計劃中";
$MESS["WW_STEP7_9"] = "通過會計計劃的數據交換的許可設置";
$MESS["WW_STEP7_10"] = "您可以創建一個新的網站用戶";
$MESS["WW_STEP7_11"] = "登入";
$MESS["WW_STEP7_12"] = "最小3個符號";
$MESS["WW_STEP7_13"] = "密碼";
$MESS["WW_STEP7_14"] = "最小6個符號";
$MESS["WW_STEP7_15"] = "確認密碼";
$MESS["WW_STEP7_16"] = "允許執行數據交換操作的用戶組";
$MESS["WW_STEP8"] = "送貨服務（第8步，第8步））";
$MESS["WW_STEP8_1"] = "（自動化）";
$MESS["WW_STEP8_2"] = "商店地址";
$MESS["WW_STEP8_3"] = "選擇將在網站上使用的送貨服務。";
$MESS["WW_STEP_DESCR"] = "簡單而全面的嚮導將幫助您以8個步驟配置電子商店參數。";
