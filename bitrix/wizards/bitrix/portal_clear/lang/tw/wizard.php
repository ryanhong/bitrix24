<?php
$MESS["DATA_DELETE"] = "刪除演示數據...";
$MESS["DELETE_STEP_TITLE"] = "刪除數據";
$MESS["FINISH_STEP_CONTENT"] = "演示數據已從Bitrix24中刪除。<br /> <br />您現在可以繼續配置系統。<br /> <br />在開始之前，我們建議您服用<a a a href = \ \“ /服務/學習/\“ target = \” _空白\“>門戶管理課程</a>。<br />";
$MESS["FINISH_STEP_TITLE"] = "完成清理";
$MESS["INSTALL_SERVICE_FINISH_STATUS"] = "清理已完成。";
$MESS["INST_ERROR_NOTICE"] = "請重複當前步驟。如果錯誤持續存在，請跳過步驟。";
$MESS["INST_ERROR_OCCURED"] = "注意力！在此安裝步驟中發生了錯誤。";
$MESS["INST_JAVASCRIPT_DISABLED"] = "嚮導要求在系統上啟用JavaScript。 JavaScript被禁用或不受瀏覽器的支持。請更改瀏覽器設置，然後再試一次<a href= \ \">重試</a>。";
$MESS["INST_RETRY_BUTTON"] = "重試";
$MESS["INST_SKIP_BUTTON"] = "跳過";
$MESS["INST_TEXT_ERROR"] = "錯誤訊息";
$MESS["NEXT_BUTTON"] = "下一個";
$MESS["PREVIOUS_BUTTON"] = "後退";
$MESS["WELCOME_STEP_TITLE"] = "啟動清理";
$MESS["WELCOME_TEXT"] = "
此嚮導將幫助您從Intranet門戶中刪除演示數據。<br /> <br /> <span style ='顏色：red'>注意！<br />嚮導會刪除以下演示數據：
<ul>
<li>員工變更（缺席信息，尊敬的員工）</li>
<li>新聞</li>
<li>職位空缺</li>
<li>分類</li>
<li>任務</li>
<li>鏈接目錄</li>
<li>日曆</li>
<li>文檔庫</li>
<li> Photogalleries </li>
<li> CRM </li>
</ul>
所有門戶文件和文件夾結構都將被保留。</span>";
$MESS["WIZARD_WAIT_WINDOW_TEXT"] = "安裝數據...";
$MESS["wiz_go"] = "開放門戶";
