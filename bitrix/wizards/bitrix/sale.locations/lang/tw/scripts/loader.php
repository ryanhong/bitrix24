<?php
$MESS["WSL_LOADER_ALL_LOADED"] = "所有文件均已成功加載。";
$MESS["WSL_LOADER_ERROR_ACCESS_DENIED"] = "錯誤：訪問被拒絕！";
$MESS["WSL_LOADER_ERROR_FILES"] = "錯誤：無效的文件名！";
$MESS["WSL_LOADER_FILE_ERROR"] = "無法加載文件";
$MESS["WSL_LOADER_FILE_LOADED"] = "加載文件";
$MESS["WSL_LOADER_LOADING"] = "加載文件...";
