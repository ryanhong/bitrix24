<?php
$MESS["WSL_IMPORT_ALL_DONE"] = "完畢。";
$MESS["WSL_IMPORT_ERROR_ACCESS_DENIED"] = "錯誤：訪問被拒絕！";
$MESS["WSL_IMPORT_ERROR_FILES"] = "錯誤！不良文件名。";
$MESS["WSL_IMPORT_ERROR_NO_LANG"] = "沒有找到語言";
$MESS["WSL_IMPORT_ERROR_NO_LOC_FILE"] = "沒有加載位置文件";
$MESS["WSL_IMPORT_ERROR_NO_ZIP_FILE"] = "沒有加載zip文件";
$MESS["WSL_IMPORT_ERROR_WRONG_LOC_FILE"] = "位置文件格式不良";
$MESS["WSL_IMPORT_FILES_LOADING"] = "處理文件...";
$MESS["WSL_IMPORT_LOC_STATS"] = "位置：<ul>
<li>進口國家：#NumCountries＃</li>
<li>導入的城市：#numcities＃</li>
<li>總位置：#numlocations＃</li> </ul>";
$MESS["WSL_IMPORT_ZIP_STATS"] = "郵政編碼：<ul>
<li> zip的加載：#numzip＃</li>
<li>對於城市：#numcities＃</li> </ul>";
