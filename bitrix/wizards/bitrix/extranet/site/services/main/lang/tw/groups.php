<?php
$MESS["EXTRANET_ADMIN_GROUP_DESC"] = "管理員有完全訪問，管理和編輯Extranet站點資源的權限。";
$MESS["EXTRANET_ADMIN_GROUP_NAME"] = "Extranet站點管理員";
$MESS["EXTRANET_CREATE_WG_GROUP_DESC"] = "所有有權在Extranet站點上創建用戶組的用戶。";
$MESS["EXTRANET_CREATE_WG_GROUP_NAME"] = "允許創建Extranet用戶組";
$MESS["EXTRANET_GROUP_DESC"] = "所有可以訪問Extranet網站的用戶。";
$MESS["EXTRANET_GROUP_NAME"] = "Extranet用戶";
$MESS["EXTRANET_MENUITEM_NAME"] = "外部";
