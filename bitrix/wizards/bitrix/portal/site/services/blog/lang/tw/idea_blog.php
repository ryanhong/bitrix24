<?php
$MESS["IDEA_BLOG_DEMO_CATEGORY_1"] = "聖誕節";
$MESS["IDEA_BLOG_DEMO_CATEGORY_2"] = "2012";
$MESS["IDEA_BLOG_DEMO_CATEGORY_3"] = "國際業務";
$MESS["IDEA_BLOG_DEMO_COMMENT_BODY_1"] = "哇，這是個好主意！法國是我的夢想！";
$MESS["IDEA_BLOG_DEMO_COMMENT_BODY_2"] = "哦，明年。我們今年要去芬蘭。";
$MESS["IDEA_BLOG_DEMO_MESSAGE_BODY_1"] = "大家好，去巴黎去聖誕節呢？
我們可以在那裡度過美好的時光！";
$MESS["IDEA_BLOG_DEMO_MESSAGE_BODY_2"] = "我建議我們在加拿大開設一個辦公室（或為會員設立）。我們會大大增加羊皮靴子的銷售！";
$MESS["IDEA_BLOG_DEMO_MESSAGE_TITLE_1"] = "2012年聖誕節派對";
$MESS["IDEA_BLOG_DEMO_MESSAGE_TITLE_2"] = "擴展計劃";
$MESS["IDEA_DEMO_BLOG_GROUP_NAME"] = "公司的想法";
$MESS["IDEA_DEMO_BLOG_NAME"] = "想法";
$MESS["IDEA_UF_CATEGORY_CODE_1"] = "our_events";
$MESS["IDEA_UF_CATEGORY_CODE_2"] = "外部";
