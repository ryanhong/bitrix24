<?php
$MESS["BLG_NAME"] = "部落格";
$MESS["BLOG_DEMO_CATEGORY_1"] = "建議";
$MESS["BLOG_DEMO_CATEGORY_2"] = "尖端";
$MESS["BLOG_DEMO_COMMENT_BODY"] = "我會試試看！";
$MESS["BLOG_DEMO_COMMENT_TITLE"] = "有趣的！";
$MESS["BLOG_DEMO_GROUP_SOCNET"] = "社交網絡組";
$MESS["BLOG_DEMO_MESSAGE_BODY_1"] = "親愛的同事，Intranet門戶網站今天正式開始。

有效地，這意味著現在每個員工都允許查看公共信息，並給出了訪問其私人區域的密碼。

對門戶區域的可配置訪問為我們提供了團隊合作功能：我們現在可以編輯文檔；創建工作組；詳細說明報告 - 一起！

門戶網站的目的是使工人之​​間的通信鏈盡可能短。該門戶是快速而現代的交流手段，是避免書面工作並將業務信息轉化為電子課程的最佳方法。
 
使用Intranet門戶網站，公司員工可以獲取任何技術，參考或法學信息，包括公司標準和身份。

用戶可以發佈到論壇；向技術支持和供應服務發送請求；共享和交換文件；討論公司新聞，並從公司管理層獲取最新信息。

該門戶將成為我們公司業務和生活的重要組成部分！";
$MESS["BLOG_DEMO_MESSAGE_TITLE_1"] = "公司Intranet門戶的開業";
$MESS["BPC_SONET_COMMENT_TITLE"] = "在博客文章\“＃標題＃\”上添加了評論";
$MESS["BPC_SONET_POST_TITLE"] = "創建了博客文章\“＃標題＃\”";
