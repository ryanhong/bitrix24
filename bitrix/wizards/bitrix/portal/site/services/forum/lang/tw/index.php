<?php
$MESS["COMMENTS_GROUP_NAME"] = "論壇評論";
$MESS["DOCS_DIRECTORS_COMMENTS_NAME"] = "高層管理文件庫的評論";
$MESS["DOCS_SALES_COMMENTS_NAME"] = "銷售和營銷文件庫的評論";
$MESS["DOCS_SHARED_COMMENTS_NAME"] = "通用文件庫的評論";
$MESS["GENERAL_FORUM_DESCRIPTION"] = "公司員工的公共論壇。在這裡討論您的業務並交流意見。";
$MESS["GENERAL_FORUM_MESSAGE_BODY"] = "注意力！公司員工現在可以維護其私人區域的文件存儲。

您將找到有關文件存儲管理的詳細信息以及將存儲映射到“幫助”部分中的網絡驅動器的方法：\“我的個人資料 - 文件\”。

如果您對文件存儲配置有任何疑問，請使用支持請求表格將請求發送給TechSupport工程師。";
$MESS["GENERAL_FORUM_NAME"] = "一般論壇";
$MESS["GENERAL_FORUM_TOPIC_TITLE"] = "門戶新聞";
$MESS["GENERAL_GROUP_NAME"] = "一般論壇";
$MESS["GROUPS_AND_USERS_COMMENTS_NAME"] = "文件的評論";
$MESS["HIDDEN_GROUP_NAME"] = "隱藏的論壇";
$MESS["NEWS_COMMENTS_FORUM_NAME"] = "新聞討論";
$MESS["PHOTOGALLERY_COMMENTS_FORUM_NAME"] = "照片庫討論";
$MESS["USERS_AND_GROUPS_FORUM_DESCRIPTION"] = "個人和小組論壇";
$MESS["USERS_AND_GROUPS_FORUM_NAME"] = "用戶和組";
