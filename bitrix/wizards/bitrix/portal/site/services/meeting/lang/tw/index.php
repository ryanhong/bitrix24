<?php
$MESS["MEETING_DESCRIPTION"] = "我們將不得不舉行會議，討論Intranet門戶網站在我們公司中的部署。";
$MESS["MEETING_ITEM_TITLE_1"] = "可用解決方案的審查";
$MESS["MEETING_ITEM_TITLE_2"] = "定義實施步驟；任命負責人";
$MESS["MEETING_ITEM_TITLE_3"] = "聖誕節計劃";
$MESS["MEETING_PLACE"] = "首席執行官辦公室";
$MESS["MEETING_TITLE"] = "Intranet門戶部署";
