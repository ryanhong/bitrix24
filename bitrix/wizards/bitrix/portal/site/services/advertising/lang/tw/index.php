<?php
$MESS["PORTAL_ADV_01_absence"] = "缺勤管理系統";
$MESS["PORTAL_ADV_01_absence_url"] = "/help/absence_help.php";
$MESS["PORTAL_ADV_03_outlook"] = "Microsoft Outlook集成";
$MESS["PORTAL_ADV_03_outlook_url"] = "/help/outlook.php";
$MESS["PORTAL_ADV_05_xmpp"] = "XMPP即時消息";
$MESS["PORTAL_ADV_05_xmpp_url"] = "/help/xmpp.php";
$MESS["PORTAL_ADV_100_100_ONE"] = "初級 /第一側橫幅100x100";
$MESS["PORTAL_ADV_100_100_ONE_NAME"] = " 第一次來這裡？";
$MESS["PORTAL_ADV_100_100_TWO"] = "次級 /第二側橫幅100x100";
$MESS["PORTAL_ADV_468_60_BOTTOM"] = "底部橫幅468x60";
$MESS["PORTAL_ADV_468_60_BOTTOM_NAME"] = " 第一次來這裡？";
$MESS["PORTAL_ADV_468_60_TOP"] = "頂部橫幅468x60";
$MESS["PORTAL_ADV_INFO"] = "資訊";
$MESS["PORTAL_ADV_dashboard"] = "在幾分鐘內個性化儀表板！";
