<?php
$MESS["W_IB_ABSENCE_1_FINISH"] = "作品";
$MESS["W_IB_ABSENCE_1_NAME"] = "定期休假";
$MESS["W_IB_ABSENCE_1_PREV1"] = "定期休假";
$MESS["W_IB_ABSENCE_1_PREV2"] = " 年。";
$MESS["W_IB_ABSENCE_1_STATE"] = "休假";
$MESS["W_IB_ABSENCE_2_NAME"] = "商務旅行";
$MESS["W_IB_ABSENCE_2_PREV"] = "商務前往公司分支機構。";
$MESS["W_IB_ABSENCE_3_NAME"] = "病假";
$MESS["W_IB_ABSENCE_3_PREV"] = "帶薪病假。";
$MESS["W_IB_ABSENCE_3_STATE"] = "病假";
