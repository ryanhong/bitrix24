<?php
$MESS["STATE_FORM_1"] = "更改員工狀態表格";
$MESS["STATE_FORM_2"] = "選擇已改變狀態的員工";
$MESS["STATE_FORM_3"] = "狀態變化日期";
$MESS["STATE_FORM_4"] = "更改的類型";
$MESS["STATE_FORM_5"] = "位置";
$MESS["STATE_FORM_6"] = "部門";
$MESS["STATE_FORM_7"] = "狀態變化的簡短描述";
$MESS["STATE_FORM_8"] = "狀態";
