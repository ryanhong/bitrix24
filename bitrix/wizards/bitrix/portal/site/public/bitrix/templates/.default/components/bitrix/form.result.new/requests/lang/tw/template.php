<?php
$MESS["FORM_ACCESS_DENIED"] = "網絡形式訪問被拒絕。";
$MESS["FORM_ADD"] = "添加";
$MESS["FORM_APPLY"] = "申請";
$MESS["FORM_DATA_SAVED1"] = "謝謝。您的申請表＃";
$MESS["FORM_DATA_SAVED2"] = "收到了。";
$MESS["FORM_MODULE_NOT_INSTALLED"] = "未安裝Web形式模塊。";
$MESS["FORM_NOT_FOUND"] = "找不到網絡形式。";
$MESS["FORM_REQUIRED_FIELDS"] = "必需的字段";
