<?php
$MESS["ALL_TAGS_SUCCESSFULLY_DELETED"] = "所選標籤已從所有任務中刪除。";
$MESS["TAG_ACTION_NOT_ALLOWED"] = "權限不足。";
$MESS["TAG_CANCEL"] = "取消";
$MESS["TAG_EMPTY_NEW_NAME"] = "名稱不能為空。";
$MESS["TAG_IS_ALREADY_EXISTS"] = "此標籤已經存在。";
$MESS["TAG_IS_SUCCESSFULLY_DELETED"] = "標籤已從所有任務中刪除。";
$MESS["TAG_IS_SUCCESSFULLY_UPDATED"] = "標籤已在所有任務中進行了更新。";
$MESS["TASKS_BY_TAG_LIST"] = "任務";
