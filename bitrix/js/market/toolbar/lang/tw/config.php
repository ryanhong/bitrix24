<?php
$MESS["MARKET_TOOLBAR_JS_CATALOG_TITLE"] = "目錄";
$MESS["MARKET_TOOLBAR_JS_DETAILED"] = "了解更多";
$MESS["MARKET_TOOLBAR_JS_DIDNT_FIND_SUITABLE_SOLUTION"] = "找不到您需要的應​​用程序？";
$MESS["MARKET_TOOLBAR_JS_FAVORITES_TITLE"] = "最愛";
$MESS["MARKET_TOOLBAR_JS_LOOKING_RIGHT_APPS"] = "您的搜索結果將在這裡顯示";
$MESS["MARKET_TOOLBAR_JS_MAKE_OR_PUBLISH_YOUR_INTEGRATION"] = "創建並發布您自己的解決方案。";
$MESS["MARKET_TOOLBAR_JS_MARKET_PLUS_TITLE"] = "Bitrix24.market";
$MESS["MARKET_TOOLBAR_JS_MARKET_TITLE"] = "Bitrix24.market";
$MESS["MARKET_TOOLBAR_JS_MARKET_TOTAL_APPS"] = "＃total_apps＃+應用";
$MESS["MARKET_TOOLBAR_JS_MORE"] = "更多的";
$MESS["MARKET_TOOLBAR_JS_NO_SEARCH_RESULT"] = "您的搜索未返回任何結果。";
$MESS["MARKET_TOOLBAR_JS_SEARCH_PLACEHOLDER"] = "搜索bitrix24.market？";
