<?php
$MESS["JS_VIEWER_DOCUMENT_ITEM_OPEN_DESCR_OFFICE365"] = "要查看文件，請在新窗口中打開";
$MESS["JS_VIEWER_DOCUMENT_ITEM_OPEN_FILE_OFFICE365"] = "打開";
$MESS["JS_VIEWER_DOCUMENT_ITEM_OPEN_HELP_HINT_OFFICE365"] = "<a href= \"# \“onclick='top.bx.helper.helper.show（ Office365中的文件。";
$MESS["JS_VIEWER_DOCUMENT_ITEM_SHOW_FILE_DIALOG_OAUTH_NOTICE"] = "請登錄到您的<a id = \"bx-js-disk-run-oauth-modal \" href= \"# \">＃服務＃</a>帳戶以查看文件。";
$MESS["JS_VIEWER_DOCUMENT_ONLYOFFICE_SAVED"] = "文檔＃名稱＃更新";
$MESS["JS_VIEWER_DOCUMENT_ONLYOFFICE_SAVE_PROCESS"] = "保存文檔：＃名稱＃";
