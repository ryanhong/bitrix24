<?php
$MESS["JS_DISK_DOC_PROCESS_CREATE_DESCR_SAVE_DOC_F"] = "您目前正在其中一個窗口中編輯新文檔。完成後，單擊\“＃save_as_doc＃\”以將文檔添加到Intranet。";
$MESS["JS_DISK_DOC_PROCESS_EDIT_IN_SERVICE"] = "使用＃服務＃編輯";
$MESS["JS_DISK_DOC_PROCESS_IFRAME_DESCR_SAVE_DOC_F"] = "您目前正在其中一個窗口中編輯此文檔。完成後，單擊\“＃save_doc＃\”以將文檔上傳到Intranet。";
$MESS["JS_DISK_DOC_PROCESS_IFRAME_PROCESS_SAVE_DOC"] = "保存文檔";
$MESS["JS_DISK_DOC_PROCESS_NOW_CREATING_IN_SERVICE"] = "用＃服務＃創建文檔";
$MESS["JS_DISK_DOC_PROCESS_NOW_DOWNLOAD_FROM_SERVICE"] = "從＃服務下載文檔＃";
$MESS["JS_DISK_DOC_PROCESS_NOW_EDITING_IN_SERVICE"] = "用＃服務編輯＃";
$MESS["JS_DISK_DOC_PROCESS_SAVE"] = "節省";
$MESS["JS_DISK_DOC_PROCESS_SAVE_AS"] = "另存為";
$MESS["JS_DISK_DOC_TRY_TO_OPEN_BY_LOCAL_PROGRAM"] = "嘗試<a href='#'";
$MESS["JS_DISK_DOC_TRY_TO_OPEN_BY_LOCAL_PROGRAM_OR_DOWNLOAD"] = "或<a href= \"#download_link# \“target= \"_blank \">下載</a>。";
$MESS["JS_DISK_DOC_TRY_TO_OPEN_BY_LOCAL_PROGRAM_TITLE"] = "不能打開文件。嘗試使用bitrix24.drive <a href='#'";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP"] = "運行BitRix24應用程序";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_DESCR"] = "不幸的是，我們無法打開文件進行編輯。確保已安裝了Bitrix24應用程序。 <br> <br>如果安裝了應用程序，請等待文檔打開或稍後再試。";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_DOWNLOAD"] = "下載應用程序";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_HELP"] = "了解有關使用文檔的更多信息";
$MESS["JS_DISK_DOC_WAITING_FOR_BITRIX24_DESKTOP_TITLE"] = "編輯";
