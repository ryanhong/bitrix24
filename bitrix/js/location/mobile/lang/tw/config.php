<?php
$MESS["LOCATION_MOBILE_APP_BACK_TO_MAP"] = "返回地圖";
$MESS["LOCATION_MOBILE_APP_DETAILS_TITLE"] = "更多地址詳細信息";
$MESS["LOCATION_MOBILE_APP_DONE"] = "完畢";
$MESS["LOCATION_MOBILE_APP_EMPTY_STATE_DESCRIPTION"] = "在搜索欄中輸入您的地址，或在地圖上選擇一個位置。搜索結果還將包括您之前搜索的位置";
$MESS["LOCATION_MOBILE_APP_EMPTY_STATE_TITLE"] = "添加地址";
$MESS["LOCATION_MOBILE_APP_ENTER_ADDRESS"] = "輸入地址";
$MESS["LOCATION_MOBILE_APP_ENTER_INFORMATION_MANUALLY"] = "手動輸入地址";
$MESS["LOCATION_MOBILE_APP_ENTER_INFORMATION_MANUALLY_MSGVER_1"] = "手動輸入地址";
$MESS["LOCATION_MOBILE_APP_MAP"] = "地圖";
$MESS["LOCATION_MOBILE_APP_MATCHES_NOT_FOUND"] = "我找不到完全匹配";
$MESS["LOCATION_MOBILE_APP_MATCHES_NOT_FOUND_MSGVER_1"] = "找不到比賽";
$MESS["LOCATION_MOBILE_APP_MATCHES_NO_DESIRED_MATCHES"] = "找不到比賽？";
$MESS["LOCATION_MOBILE_APP_NEW_SEARCH"] = "新搜索";
$MESS["LOCATION_MOBILE_APP_NOT_ENTERED"] = "不提供";
$MESS["LOCATION_MOBILE_APP_RECENT_ADDRESSES"] = "最近的地址";
$MESS["LOCATION_MOBILE_APP_SAVE_DETAILS"] = "節省";
$MESS["LOCATION_MOBILE_APP_SAVE_VALUES"] = "保存值";
$MESS["LOCATION_MOBILE_APP_SEARCH"] = "搜尋";
