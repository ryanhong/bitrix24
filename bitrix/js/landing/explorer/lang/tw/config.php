<?php
$MESS["LANDING_EXT_EXPLORER_ALERT_TITLE"] = "錯誤";
$MESS["LANDING_EXT_EXPLORER_BUTTON_CANCEL"] = "取消";
$MESS["LANDING_EXT_EXPLORER_BUTTON_COPY"] = "複製";
$MESS["LANDING_EXT_EXPLORER_BUTTON_MOVE"] = "移動";
$MESS["LANDING_EXT_EXPLORER_TITLE_COPY"] = "複製\“＃標題＃\”";
$MESS["LANDING_EXT_EXPLORER_TITLE_MOVE"] = "移動\“＃標題＃\”";
