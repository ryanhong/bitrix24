<?php
$MESS["LANDING_FORM_SETTINGS_PANEL_CRM_CATEGORY_CHANGE_CONFIRM_TEXT"] = "更改管道將從默認字段值列表中刪除{fieldName}字段。";
$MESS["LANDING_FORM_SETTINGS_PANEL_CRM_CREATE_ORDER_MESSAGE_BOX_CANCEL"] = "取消";
$MESS["LANDING_FORM_SETTINGS_PANEL_CRM_CREATE_ORDER_MESSAGE_BOX_OK"] = "禁用";
$MESS["LANDING_FORM_SETTINGS_PANEL_CRM_CREATE_ORDER_MESSAGE_BOX_TITLE_1"] = "如果禁用此選項，則客戶將無法以表格付款，因為您的CRM當前處於Deals+orders模式。<br> <a href = \'javascript：void（void（0）\“ onclick =” \“ top. bx.helper.show（\'redirect = delet＆code＆code = 13632830 \'）\”>了解有關此模式的更多信息</a> <br> <br>您是否想禁用此處的訂單和付款功能形式？";
$MESS["LANDING_FORM_SETTINGS_PANEL_CRM_ITEM_TEMPLATE"] = "\"{文字}\"";
$MESS["LANDING_FORM_SETTINGS_PANEL_CRM_SCHEME_CHANGE_CONFIRM_TEXT"] = "從默認字段值列表中選擇{entityName} entity將刪除{fieldSlist}和{lastField}字段，因為它們與{entityName} entities不兼容。";
$MESS["LANDING_FORM_SETTINGS_PANEL_CRM_SCHEME_CHANGE_CONFIRM_TEXT_1"] = "選擇{entityName} entity將從默認字段值列表中刪除{fieldName}字段，因為它與{entityName} entities不兼容。";
$MESS["LANDING_FORM_SETTINGS_PANEL_CRM_SCHEME_CHANGE_CONFIRM_TITLE"] = "注意力";
$MESS["LANDING_FORM_SETTINGS_PANEL_CRM_SCHEME_CREATE_ORDER_CHANGE_CONFIRM_TITLE"] = "此表格可以收到付款";
