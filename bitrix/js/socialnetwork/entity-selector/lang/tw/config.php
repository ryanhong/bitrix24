<?php
$MESS["SOCNET_ENTITY_SELECTOR_CREATE_PROJECT"] = "創建一個工作組";
$MESS["SOCNET_ENTITY_SELECTOR_EMPLOYEE_OR_PROJECT"] = "<員工>邀請用戶</僱員> <span>或</span> <project>創建一個工作組</project>";
$MESS["SOCNET_ENTITY_SELECTOR_EMPLOYEE_OR_PROJECT_OR_GUEST"] = "<員工>邀請用戶</僱員> <span>或</span> <project>創建一個工作組</project> <span>或</span> </span> <gents>邀請訪客</guest>";
$MESS["SOCNET_ENTITY_SELECTOR_INVITED_GUEST_HINT"] = "您還可以通過電子郵件添加您的伴侶或客戶。";
$MESS["SOCNET_ENTITY_SELECTOR_INVITED_USERS_TAB_TITLE"] = "邀請人";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_EMPLOYEE"] = "邀請用戶";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_EMPLOYEE_OR_EXTRANET"] = "邀請員工或外部用戶";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_EMPLOYEE_OR_GUEST"] = "<員工>邀請用戶</employee> <span>或</span> </span> <gents>邀請訪客</guest>";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_EXTRANET"] = "邀請外部用戶";
$MESS["SOCNET_ENTITY_SELECTOR_INVITE_GUEST"] = "邀請客人";
$MESS["SOCNET_ENTITY_SELECTOR_PROJECT_OR_GUEST"] = "<project>創建一個工作組</project> <span>或</span> <guster>邀請訪客</ustauts>";
$MESS["SOCNET_ENTITY_SELECTOR_TAG_FOOTER_LABEL"] = "開始鍵入創建一個新標籤";
