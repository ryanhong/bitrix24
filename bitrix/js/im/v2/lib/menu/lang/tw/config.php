<?php
$MESS["IM_LIB_INVITE_CANCEL"] = "取消邀請";
$MESS["IM_LIB_INVITE_CANCEL_CONFIRM"] = "您確定要取消邀請嗎？";
$MESS["IM_LIB_INVITE_CANCEL_DONE"] = "邀請已被取消。";
$MESS["IM_LIB_INVITE_RESEND"] = "再次邀請";
$MESS["IM_LIB_INVITE_RESEND_DONE"] = "已發送邀請。";
$MESS["IM_LIB_MENU_CALL_2"] = "視訊通話";
$MESS["IM_LIB_MENU_FIND_CHATS_WITH_USER"] = "查找與此用戶的聊天";
$MESS["IM_LIB_MENU_HIDE"] = "隱藏聊天";
$MESS["IM_LIB_MENU_LEAVE"] = "離開聊天";
$MESS["IM_LIB_MENU_MUTE_2"] = "沉默的";
$MESS["IM_LIB_MENU_OPEN"] = "打開";
$MESS["IM_LIB_MENU_OPEN_CALENDAR"] = "查看員工日曆";
$MESS["IM_LIB_MENU_OPEN_HISTORY"] = "查看歷史";
$MESS["IM_LIB_MENU_OPEN_IN_NEW_TAB"] = "在新標籤中打開";
$MESS["IM_LIB_MENU_OPEN_PROFILE"] = "查看員工資料";
$MESS["IM_LIB_MENU_PIN"] = "引腳聊天";
$MESS["IM_LIB_MENU_READ"] = "標記為已讀";
$MESS["IM_LIB_MENU_UNMUTE_2"] = "取消靜音";
$MESS["IM_LIB_MENU_UNPIN"] = "Untin聊天";
$MESS["IM_LIB_MENU_UNREAD"] = "標記以後閱讀";
$MESS["IM_LIB_MENU_WRITE"] = "發信息";
