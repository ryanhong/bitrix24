<?php
$MESS["IM_CONTENT_COPILOT_DIALOG_STATUS_TYPING"] = "CoPilot schreibt eine Antwort?";
$MESS["IM_CONTENT_COPILOT_EMPTY_STATE_ASK_QUESTION"] = "Eine Frage stellen";
$MESS["IM_CONTENT_COPILOT_EMPTY_STATE_ERROR_CREATING_CHAT"] = "Chat kann nicht erstellt werden, versuchen Sie es bitte später erneut.";
$MESS["IM_CONTENT_COPILOT_EMPTY_STATE_MESSAGE"] = "Hallo! Ich bin CoPilot und ich bin artifizielle Intelligenz.#BR#Ich bin bereit, kreative Lösungen vorzuschlagen,#BR#einen neuen Text zu erstellen oder den von Ihnen geschriebenen zu verbessern.#BR#Ich bin auch bereit, über alle Ideen zu diskutieren, die Sie haben.";
$MESS["IM_CONTENT_COPILOT_HEADER_RENAME_ERROR"] = "Chat kann nicht umbenannt werden, versuchen Sie es bitte später erneut.";
$MESS["IM_CONTENT_COPILOT_HEADER_SUBTITLE"] = "online";
$MESS["IM_CONTENT_COPILOT_TEXTAREA_AUDIO_INPUT_ERROR"] = "Spracherkennung fehlgeschlagen. Versuchen Sie es bitte später erneut.";
$MESS["IM_CONTENT_COPILOT_TEXTAREA_PLACEHOLDER"] = "Schreiben Sie Ihre Frage";
