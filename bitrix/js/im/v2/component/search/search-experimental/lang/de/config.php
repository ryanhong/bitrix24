<?php
$MESS["IM_SEARCH_EXPERIMENTAL_ITEM_CHAT_TYPE_GROUP_V2"] = "Gruppenchat";
$MESS["IM_SEARCH_EXPERIMENTAL_ITEM_FOUND_BY_USER"] = "Unter Chatteilnehmern gefunden";
$MESS["IM_SEARCH_EXPERIMENTAL_ITEM_USER_TYPE_GROUP_V2"] = "Nutzer";
$MESS["IM_SEARCH_EXPERIMENTAL_MY_NOTES"] = "Notizen";
$MESS["IM_SEARCH_EXPERIMENTAL_RESULT_NOT_FOUND"] = "Es wurde nichts gefunden.";
$MESS["IM_SEARCH_EXPERIMENTAL_RESULT_NOT_FOUND_DESCRIPTION"] = "Versuchen Sie eine andere Suchanfrage einzugeben.";
$MESS["IM_SEARCH_EXPERIMENTAL_SECTION_RECENT"] = "Zuletzt gesucht";
$MESS["IM_SEARCH_EXPERIMENTAL_SECTION_RECENT_CHATS"] = "Zuletzt verwendete Chats";
$MESS["IM_SEARCH_EXPERIMENTAL_SECTION_USERS_AND_CHATS"] = "Chats";
