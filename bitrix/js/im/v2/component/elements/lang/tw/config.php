<?php
$MESS["IM_ELEMENTS_ATTACH_RICH_LINK_DELETE"] = "刪除Rich Link預覽";
$MESS["IM_ELEMENTS_CHAT_INFO_POPUP_HISTORY"] = "歷史";
$MESS["IM_ELEMENTS_CHAT_INFO_POPUP_NO_ACCESS"] = "拒絕訪問。";
$MESS["IM_ELEMENTS_CHAT_INFO_POPUP_OPEN_CHAT"] = "公開聊天";
$MESS["IM_ELEMENTS_CHAT_INFO_POPUP_VIDEOCALL"] = "視訊通話";
$MESS["IM_ELEMENTS_CHAT_INFO_POPUP_WRITE_A_MESSAGE"] = "發信息";
$MESS["IM_ELEMENTS_KEYBOARD_BUTTON_ACTION_COPY_SUCCESS"] = "文本已復制。";
$MESS["IM_ELEMENTS_MEDIA_IMAGE_TITLE"] = "打開完整版：＃名稱＃（＃size＃）";
