<?php
$MESS["IM_MESSENGER_MESSAGE_CONTEXT_MENU_DELETE"] = "Löschen";
$MESS["IM_MESSENGER_MESSAGE_CONTEXT_MENU_RETRY"] = "Erneut versuchen";
$MESS["IM_MESSENGER_MESSAGE_EDITED"] = "Geändert";
$MESS["IM_MESSENGER_MESSAGE_HEADER_FORWARDED_FROM"] = "Weitergeleitet von #NAME#";
$MESS["IM_MESSENGER_MESSAGE_HEADER_FORWARDED_FROM_SYSTEM"] = "Weitergeleitete Systemnachricht";
$MESS["IM_MESSENGER_MESSAGE_MENU_TITLE"] = "Klicken Sie, um das Kontextmenü zu öffnen, oder klicken Sie, indem Sie gleichzeitig auf #SHORTCUT# drücken, um die Nachricht zu zitieren";
