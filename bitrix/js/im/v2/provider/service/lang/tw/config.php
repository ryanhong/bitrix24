<?php
$MESS["IM_MESSAGE_SERVICE_ADD_MESSAGE_TO_FAVORITE_SUCCESS"] = "消息已添加到收藏夾中。您可以在聊天側欄中訪問您喜歡的消息。";
$MESS["IM_MESSAGE_SERVICE_SAVE_MESSAGE_SUCCESS"] = "消息保存到收藏夾。您可以在側邊欄上訪問收藏夾。";
