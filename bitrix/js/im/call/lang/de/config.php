<?php
$MESS["IM_M_CALL_BAD_NETWORK_HINT"] = "Aufgrund der Verbindungsschwierigkeiten haben wir die Übertragung von Video für die Anrufteilnehmer auf Ihrem Gerät unterbrochen, um so die Tonqualität zu gewährleisten.";
$MESS["IM_M_CALL_POOR_CONNECTION_WITH_USER"] = "Anrufteilnehmer hat eine langsame Internetverbindung.";
