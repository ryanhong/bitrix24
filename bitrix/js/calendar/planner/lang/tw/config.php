<?php
$MESS["EC_PLANNER_CANT_DRAG_SHARED_EVENT"] = "無法重新安排涉及外部用戶的會議。";
$MESS["EC_PLANNER_IN_VACATION"] = "休假";
$MESS["EC_PLANNER_IN_VACATION_UNTIL"] = "休假直到＃直到＃";
$MESS["EC_PLANNER_TIMEZONE_NOTICE"] = "參與者佔據不同的時區";
$MESS["EC_PLANNER_TIMEZONE_NOTICE_TEXT"] = "在不同的時區：<span class = \“＃class＃\”>＃count＃</span>";
$MESS["EC_PLANNER_TODAY"] = "今天";
