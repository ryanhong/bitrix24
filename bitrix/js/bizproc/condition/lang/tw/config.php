<?php
$MESS["BIZPROC_JS_CONDITION"] = "狀態:";
$MESS["BIZPROC_JS_CONDITION_BETWEEN"] = "在範圍內";
$MESS["BIZPROC_JS_CONDITION_CONTAIN"] = "包括";
$MESS["BIZPROC_JS_CONDITION_EMPTY"] = "空的";
$MESS["BIZPROC_JS_CONDITION_EQ"] = "等於";
$MESS["BIZPROC_JS_CONDITION_GT"] = "多於";
$MESS["BIZPROC_JS_CONDITION_GTE"] = "不小於";
$MESS["BIZPROC_JS_CONDITION_IN"] = "在";
$MESS["BIZPROC_JS_CONDITION_LT"] = "少於";
$MESS["BIZPROC_JS_CONDITION_LTE"] = "不多於";
$MESS["BIZPROC_JS_CONDITION_MODIFIED"] = "修改的";
$MESS["BIZPROC_JS_CONDITION_NE"] = "不等於";
$MESS["BIZPROC_JS_CONDITION_NOT_CONTAIN"] = "不包括";
$MESS["BIZPROC_JS_CONDITION_NOT_EMPTY"] = "不是空的";
$MESS["BIZPROC_JS_CONDITION_NOT_IN"] = "不是";
$MESS["BIZPROC_JS_CONDITION_VALUE"] = "價值：";
