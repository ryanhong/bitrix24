<?php
$MESS["CRM_TIMELINE_ENCOURAGE_BUY_PRODUCTS_ADD_PRODUCT_TO_DEAL"] = "交易，處理";
$MESS["CRM_TIMELINE_ENCOURAGE_BUY_PRODUCTS_EDIT_PRODUCTS"] = "編輯";
$MESS["CRM_TIMELINE_ENCOURAGE_BUY_PRODUCTS_ENCOURAGE_CLIENT_BUY_PRODUCTS"] = "建議客戶還要支付訂單，並提示也購買這些物品。";
$MESS["CRM_TIMELINE_ENCOURAGE_BUY_PRODUCTS_ENCOURAGE_CLIENT_BUY_PRODUCTS_2"] = "發送付款鏈接以幫助他們更快地支付訂單。";
$MESS["CRM_TIMELINE_ENCOURAGE_BUY_PRODUCTS_LOOK_AT_CLIENT_PRODUCTS"] = "客戶下了訂單。看看他們查看的項目。";
$MESS["CRM_TIMELINE_ENCOURAGE_BUY_PRODUCTS_PRODUCTS_ADDED_TO_DEAL"] = "添加的項目交易";
$MESS["CRM_TIMELINE_ENCOURAGE_BUY_PRODUCTS_PRODUCT_IN_DEAL"] = "在交易中";
$MESS["CRM_TIMELINE_ENCOURAGE_BUY_PRODUCTS_SHOW_MORE"] = "展示更多";
$MESS["CRM_TIMELINE_ENCOURAGE_BUY_PRODUCTS_VIEWED_PRODUCTS"] = "查看的項目";
$MESS["CRM_TIMELINE_ENCOURAGE_BUY_PRODUCTS_WAIT"] = "等待...";
$MESS["CRM_TIMELINE_NOTIFICATION_BITRIX24"] = "Bitrix24";
$MESS["CRM_TIMELINE_NOTIFICATION_BITRIX24_NOTIFICATIONS_CENTER"] = "Bitrix24通知中心";
$MESS["CRM_TIMELINE_NOTIFICATION_IN_MESSENGER"] = "在";
$MESS["CRM_TIMELINE_NOTIFICATION_MESSAGE"] = "訊息";
$MESS["CRM_TIMELINE_NOTIFICATION_NO_MESSAGE_TEXT"] = "使用通知中心模板的消息";
$MESS["CRM_TIMELINE_NOTIFICATION_NO_MESSAGE_TEXT_2"] = "基於模板的消息";
$MESS["CRM_TIMELINE_NOTIFICATION_VIA"] = "通過";
$MESS["CRM_TIMELINE_PRODUCT_COMPILATION_DEAL_CREATED"] = "客戶下達了訂單；創建的交易＃deal_title＃＃deal_legend＃";
$MESS["CRM_TIMELINE_PRODUCT_COMPILATION_LIST_COMPILATION_SENT"] = "該產品選擇已發送給客戶：";
$MESS["CRM_TIMELINE_PRODUCT_COMPILATION_ORDER_EXISTS_NOTICE"] = "該交易已經有訂單。如果客戶選擇從選擇中訂購產品，將創建一項新交易。";
$MESS["CRM_TIMELINE_PRODUCT_COMPILATION_TITLE"] = "選擇";
$MESS["CRM_TIMELINE_PRODUCT_COMPILATION_VIEWED"] = "查看";
$MESS["CRM_TIMELINE_PRODUCT_COMPILATION_VIEWED_DESCRIPTION"] = "客戶已經開設了建議的產品選擇";
$MESS["CRM_TIMELINE_PRODUCT_COMPILATION_VIEWED_LEGEND"] = "發送＃date_create＃";
$MESS["CRM_TIMELINE_USER_NOTE_CANCEL"] = "取消";
$MESS["CRM_TIMELINE_USER_NOTE_PLACEHOLDER"] = "輸入註釋文字";
$MESS["CRM_TIMELINE_USER_NOTE_SAVE"] = "節省";
$MESS["TIMELINE_DELIVERY_CREATE_DELIVERY_REQUEST"] = "請求交付";
$MESS["TIMELINE_DELIVERY_CREATING_REQUEST"] = "發送請求到送貨服務";
$MESS["TIMELINE_DELIVERY_SEARCHING_PERFORMER"] = "搜索表演者";
$MESS["TIMELINE_DELIVERY_TAXI_ADDRESS_FROM"] = "從";
$MESS["TIMELINE_DELIVERY_TAXI_ADDRESS_TO"] = "到";
$MESS["TIMELINE_DELIVERY_TAXI_CLIENT_DELIVERY_PRICE"] = "客戶開票價格";
$MESS["TIMELINE_DELIVERY_TAXI_DELIVERY_CANCEL_REQUEST"] = "取消訂單";
$MESS["TIMELINE_DELIVERY_TAXI_EXPECTED_DELIVERY_PRICE"] = "計算的交貨價格";
$MESS["TIMELINE_DELIVERY_TAXI_EXPECTED_PRICE_NOT_RECEIVED"] = "沒收到";
$MESS["TIMELINE_DELIVERY_TAXI_SEND_REQUEST"] = "打電話出租車";
$MESS["TIMELINE_DELIVERY_TAXI_SERVICE"] = "送貨服務";
