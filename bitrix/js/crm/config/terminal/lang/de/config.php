<?php
$MESS["CRM_CFG_TERMINAL_SETTINGS_CANCEL_BTN"] = "Abbrechen";
$MESS["CRM_CFG_TERMINAL_SETTINGS_ON_SAVE_ERROR"] = "Fehler beim Speichern der Einstellungen. Bitte aktualisieren Sie die Seite und versuchen es Sie erneut.";
$MESS["CRM_CFG_TERMINAL_SETTINGS_SAVE_BTN"] = "Speichern";
$MESS["CRM_CFG_TERMINAL_SETTINGS_SECTION_SMS_HINT_TEXT"] = "Eine SMS-Nachricht mit einem Link zu den Zahlungsinformationen wird nach erfolgreicher Zahlung an den Kunden gesendet. #LINK_START#Mehr#LINK_END#";
$MESS["CRM_CFG_TERMINAL_SETTINGS_SECTION_SMS_INFO"] = "Der Kunde wird eine SMS mit der Zahlungsbenachrichtigung erhalten, nachdem er die Zahlung abschließt:";
$MESS["CRM_CFG_TERMINAL_SETTINGS_SECTION_SMS_MESSAGE_HINT"] = "Vollständiger Link wird verfügbar sein, nachdem die Zahlung abgeschlossen wird.";
$MESS["CRM_CFG_TERMINAL_SETTINGS_SECTION_SMS_MESSAGE_TEMPLATE"] = "Zahlungsinformation anzeigen: #PAYMENT_SLIP_LINK#";
$MESS["CRM_CFG_TERMINAL_SETTINGS_SECTION_SMS_ONBOARDING_ONLY_SMS_SERVICES_TEXT"] = "Einen externen SMS-Service verbinden, um die SMS-Nachrichten an Ihre Kunden zu senden.";
$MESS["CRM_CFG_TERMINAL_SETTINGS_SECTION_SMS_ONBOARDING_TEXT"] = "Sie können die SMS- oder WhatsApp-Nachrichten an Ihre Kunden über Bitrix24 senden, oder einen beliebigen externen SMS-Service auswählen.";
$MESS["CRM_CFG_TERMINAL_SETTINGS_SECTION_SMS_SERVICE_PROVIDER_CONNECT_BTN"] = "Externen Service verbinden";
$MESS["CRM_CFG_TERMINAL_SETTINGS_SECTION_SMS_SERVICE_PROVIDER_CONNECT_MORE"] = "Anderen Service verbinden";
$MESS["CRM_CFG_TERMINAL_SETTINGS_SECTION_SMS_SERVICE_PROVIDER_UNC_CONNECTED"] = "SMS wird über Bitrix24 gesendet. #LINK_START#Wie funktioniert das?#LINK_END#";
$MESS["CRM_CFG_TERMINAL_SETTINGS_SECTION_SMS_SERVICE_SELECT"] = "SMS senden über";
$MESS["CRM_CFG_TERMINAL_SETTINGS_SECTION_SMS_SERVICE_UNC_CONNECT_BTN"] = "Über Bitrix24 senden";
$MESS["CRM_CFG_TERMINAL_SETTINGS_SECTION_SMS_TITLE"] = "Benachrichtigung über erfolgreiche Zahlung senden";
$MESS["CRM_CFG_TERMINAL_SETTINGS_SUBTITLE_NOTIFICATION"] = "Benachrichtigungen";
