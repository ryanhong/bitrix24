<?php
$MESS["CRM_ML_SCORING_BUTTON_TITLE"] = "AI24得分";
$MESS["CRM_ML_SCORING_SPOTLIGHT_TEXT_DEAL"] = "您的CRM現在擁有超過2000筆交易！<br>現在，我們有足夠的數據來培訓預測模型。單擊\“ AI預測\”以查看當前交易的概率。";
$MESS["CRM_ML_SCORING_SPOTLIGHT_TEXT_LEAD"] = "您的CRM現在擁有2000多個線索！<br>現在，我們有足夠的數據來訓練預測模型。單擊\“ AI預測\”以查看當前引線的概率。";
