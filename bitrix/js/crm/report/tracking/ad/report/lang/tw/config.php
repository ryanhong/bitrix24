<?php
$MESS["CRM_REPORT_TRACKING_AD_REPORT_BUILD"] = "創建報告";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_CLOSE"] = "關閉";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_ERROR_TITLE"] = "這是一個錯誤。";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_SETTINGS_HINT"] = "您必須配置廣告設置才能獲得正確的報告";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_STATUS_ENABLED"] = "使能夠";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_STATUS_PAUSE"] = "暫停";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_TITLE_0"] = "競選性能";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_TITLE_1"] = "廣告小組的性能";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_TITLE_100"] = "關鍵字性能";
