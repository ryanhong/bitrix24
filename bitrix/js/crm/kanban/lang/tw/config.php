<?php
$MESS["CRM_GRID_HINT_FOR_NOT_VISIBLE_DEAL_TEXT"] = "向右滾動以查看您要搜索的交易。";
$MESS["CRM_GRID_HINT_FOR_NOT_VISIBLE_DEAL_TITLE"] = "有交易";
$MESS["CRM_GRID_HINT_FOR_NOT_VISIBLE_ELEMENT_TEXT"] = "向右滾動查看您要搜索的項目。";
$MESS["CRM_GRID_HINT_FOR_NOT_VISIBLE_ELEMENT_TITLE"] = "有項目";
$MESS["CRM_GRID_HINT_FOR_NOT_VISIBLE_INVOICE_TEXT"] = "向右滾動以查看您要搜索的發票。";
$MESS["CRM_GRID_HINT_FOR_NOT_VISIBLE_INVOICE_TITLE"] = "有發票";
$MESS["CRM_GRID_HINT_FOR_NOT_VISIBLE_LEAD_TEXT"] = "向右滾動查看您要搜索的線索。";
$MESS["CRM_GRID_HINT_FOR_NOT_VISIBLE_LEAD_TITLE"] = "有鉛";
$MESS["CRM_GRID_HINT_FOR_NOT_VISIBLE_QUOTE_TEXT_MSGVER_1"] = "向右滾動查看您要搜索的估計值。";
$MESS["CRM_GRID_HINT_FOR_NOT_VISIBLE_QUOTE_TITLE_MSGVER_1"] = "有估計";
$MESS["CRM_GRID_HINT_FOR_NOT_VISIBLE_SMART_INVOICE_TEXT"] = "向右滾動以查看您要搜索的發票。";
$MESS["CRM_GRID_HINT_FOR_NOT_VISIBLE_SMART_INVOICE_TITLE"] = "有發票";
