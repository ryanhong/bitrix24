<?php
$MESS["RATING_LIKE_EMOTION_ANGRY"] = "生氣的";
$MESS["RATING_LIKE_EMOTION_CRY"] = "哭";
$MESS["RATING_LIKE_EMOTION_FACEPALM"] = "臉部";
$MESS["RATING_LIKE_EMOTION_KISS"] = "吻";
$MESS["RATING_LIKE_EMOTION_LAUGH"] = "笑";
$MESS["RATING_LIKE_EMOTION_LIKE"] = "喜歡";
$MESS["RATING_LIKE_EMOTION_WONDER"] = "想知道";
$MESS["RATING_LIKE_POPUP_ALL"] = "所有＃cnt＃";
$MESS["RATING_LIKE_RESULTS"] = "結果";
$MESS["RATING_LIKE_TOP_TEXT2_1"] = "＃Overflow_start ##用戶_1 ##溢出_end＃＃";
$MESS["RATING_LIKE_TOP_TEXT2_1_MORE"] = "＃vellflow_start ## user_1 ## vellflow_end ## more_start＃and＃user_more＃更多＃more_end＃＃";
$MESS["RATING_LIKE_TOP_TEXT2_2"] = "＃Overflow_start ## user_1＃和＃USER_2 ## Overflow_end＃＃";
$MESS["RATING_LIKE_TOP_TEXT2_2_MORE"] = "＃Overflow_start ## user_1＃，＃user_2 ## vellflow_end ## more_start＃and＃user_more＃更多＃more_end＃＃more_end＃";
$MESS["RATING_LIKE_TOP_TEXT2_YOU_0"] = "＃Overflow_start＃you＃vellflow_end＃";
$MESS["RATING_LIKE_TOP_TEXT2_YOU_1"] = "＃Overflow_start＃you和＃user_1 ## volflow_end＃＃";
$MESS["RATING_LIKE_TOP_TEXT2_YOU_1_MORE"] = "＃Overflow_start＃you，＃user_1 ## vellflow_end ## more_start＃and＃user_more＃更多＃more_end＃＃";
$MESS["RATING_LIKE_TOP_TEXT2_YOU_2"] = "＃Overflow_start＃you，＃user_1＃和＃User_2 ## Overflow_end＃＃";
$MESS["RATING_LIKE_TOP_TEXT2_YOU_2_MORE"] = "＃Overflow_start＃you，＃user_1＃，＃User_2 ## Overflow_end ## More_start＃和＃User_more＃More＃More_end＃＃";
$MESS["RATING_LIKE_TOP_TEXT3_1"] = "＃Overflow_start ## user_list_begin ## Overflow_end＃＃";
$MESS["RATING_LIKE_TOP_TEXT3_2"] = "＃Overflow_start ## user_list_begin＃和＃USER_LIST_END ## OVERFLOW_END＃＃";
$MESS["RATING_LIKE_TOP_TEXT3_USERLIST_SEPARATOR"] = "＃用戶名＃，＃用戶名＃";
$MESS["RATING_LIKE_TOP_TEXT3_YOU"] = "你";
