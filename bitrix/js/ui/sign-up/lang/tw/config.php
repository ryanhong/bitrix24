<?php
$MESS["UI_SIGN_UP_BAD_IMAGE_FORMAT_ALERT_MESSAGE"] = "此文件格式不支持。嘗試其他文件。";
$MESS["UI_SIGN_UP_CANCEL_BUTTON_LABEL"] = "取消";
$MESS["UI_SIGN_UP_SAVE_BUTTON_LABEL"] = "節省";
$MESS["UI_SIGN_UP_TAB_INITIALS_INITIALS_LABEL"] = "縮寫";
$MESS["UI_SIGN_UP_TAB_INITIALS_LAST_NAME_LABEL"] = "名字或姓氏";
$MESS["UI_SIGN_UP_TAB_INITIALS_TITLE"] = "創建名稱";
$MESS["UI_SIGN_UP_TAB_PHOTO_TITLE"] = "上傳圖片";
$MESS["UI_SIGN_UP_TAB_TOUCH_TITLE"] = "手標誌";
$MESS["UI_SIGN_UP_TAKE_SIGN_PHOTO"] = "用電話拍照";
$MESS["UI_SIGN_UP_TOUCH_CLEAR_BUTTON"] = "清除";
$MESS["UI_SIGN_UP_TOUCH_LAYOUT_LABEL"] = "使用鼠標在下面寫下您的簽名＆darr;";
$MESS["UI_SIGN_UP_TOUCH_LAYOUT_MOBILE_LABEL"] = "在下面的框中繪製簽名＆darr;";
$MESS["UI_SIGN_UP_UPLOAD_NEW"] = "上傳新圖像";
$MESS["UI_SIGN_UP_UPLOAD_SIGN_PHOTO"] = "上傳圖片";
