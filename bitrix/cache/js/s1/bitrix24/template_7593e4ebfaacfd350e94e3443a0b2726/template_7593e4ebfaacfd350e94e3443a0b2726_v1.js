
; /* Start:"a:4:{s:4:"full";s:96:"/bitrix/components/bitrix/intranet.settings.widget/templates/.main/script.min.js?170347923923708";s:6:"source";s:76:"/bitrix/components/bitrix/intranet.settings.widget/templates/.main/script.js";s:3:"min";s:80:"/bitrix/components/bitrix/intranet.settings.widget/templates/.main/script.min.js";s:3:"map";s:80:"/bitrix/components/bitrix/intranet.settings.widget/templates/.main/script.map.js";}"*/
this.BX=this.BX||{};(function(e,s,t,i,a,l){"use strict";let r=e=>e,o,n,c,d,b,p,v,g,u,h,_,P,L;var T=babelHelpers.classPrivateFieldLooseKey("widgetPopup");var H=babelHelpers.classPrivateFieldLooseKey("requisitesPopup");var B=babelHelpers.classPrivateFieldLooseKey("copyLinkPopup");var F=babelHelpers.classPrivateFieldLooseKey("target");var I=babelHelpers.classPrivateFieldLooseKey("otp");var E=babelHelpers.classPrivateFieldLooseKey("instance");var w=babelHelpers.classPrivateFieldLooseKey("marketUrl");var f=babelHelpers.classPrivateFieldLooseKey("theme");var N=babelHelpers.classPrivateFieldLooseKey("holding");var y=babelHelpers.classPrivateFieldLooseKey("holdingWidget");var m=babelHelpers.classPrivateFieldLooseKey("isBitrix24");var S=babelHelpers.classPrivateFieldLooseKey("isFreeLicense");var O=babelHelpers.classPrivateFieldLooseKey("isAdmin");var C=babelHelpers.classPrivateFieldLooseKey("requisite");var A=babelHelpers.classPrivateFieldLooseKey("settingsUrl");var R=babelHelpers.classPrivateFieldLooseKey("setOptions");var G=babelHelpers.classPrivateFieldLooseKey("setHoldingOptions");var k=babelHelpers.classPrivateFieldLooseKey("getWidget");var U=babelHelpers.classPrivateFieldLooseKey("getItemsList");var D=babelHelpers.classPrivateFieldLooseKey("getRequisites");var j=babelHelpers.classPrivateFieldLooseKey("drawItemsList");var x=babelHelpers.classPrivateFieldLooseKey("getHeader");var K=babelHelpers.classPrivateFieldLooseKey("applyTheme");var $=babelHelpers.classPrivateFieldLooseKey("getFooter");var W=babelHelpers.classPrivateFieldLooseKey("prepareElement");var M=babelHelpers.classPrivateFieldLooseKey("getRequisitesElement");var X=babelHelpers.classPrivateFieldLooseKey("createLanding");var q=babelHelpers.classPrivateFieldLooseKey("getHoldingsElement");var Q=babelHelpers.classPrivateFieldLooseKey("getHoldingWidget");var Y=babelHelpers.classPrivateFieldLooseKey("getEmptyHoldingsElement");var V=babelHelpers.classPrivateFieldLooseKey("getSecurityAndSettingsElement");var z=babelHelpers.classPrivateFieldLooseKey("getSecurityElement");var J=babelHelpers.classPrivateFieldLooseKey("getGeneralSettingsElement");var Z=babelHelpers.classPrivateFieldLooseKey("getMigrateElement");class ee extends i.EventEmitter{constructor(e){super();Object.defineProperty(this,Z,{value:Le});Object.defineProperty(this,J,{value:Pe});Object.defineProperty(this,z,{value:_e});Object.defineProperty(this,V,{value:he});Object.defineProperty(this,Y,{value:ue});Object.defineProperty(this,Q,{value:ge});Object.defineProperty(this,q,{value:ve});Object.defineProperty(this,X,{value:pe});Object.defineProperty(this,M,{value:be});Object.defineProperty(this,W,{value:de});Object.defineProperty(this,$,{value:ce});Object.defineProperty(this,K,{value:ne});Object.defineProperty(this,x,{value:oe});Object.defineProperty(this,j,{value:re});Object.defineProperty(this,D,{value:le});Object.defineProperty(this,U,{value:ae});Object.defineProperty(this,k,{value:ie});Object.defineProperty(this,G,{value:te});Object.defineProperty(this,R,{value:se});Object.defineProperty(this,T,{writable:true,value:void 0});Object.defineProperty(this,H,{writable:true,value:void 0});Object.defineProperty(this,B,{writable:true,value:void 0});Object.defineProperty(this,F,{writable:true,value:void 0});Object.defineProperty(this,I,{writable:true,value:void 0});Object.defineProperty(this,w,{writable:true,value:void 0});Object.defineProperty(this,f,{writable:true,value:void 0});Object.defineProperty(this,N,{writable:true,value:null});Object.defineProperty(this,y,{writable:true,value:void 0});Object.defineProperty(this,m,{writable:true,value:false});Object.defineProperty(this,S,{writable:true,value:false});Object.defineProperty(this,O,{writable:true,value:void 0});Object.defineProperty(this,C,{writable:true,value:void 0});Object.defineProperty(this,A,{writable:true,value:void 0});this.setEventNamespace("BX.Intranet.SettingsWidget");babelHelpers.classPrivateFieldLooseBase(this,w)[w]=e.marketUrl;babelHelpers.classPrivateFieldLooseBase(this,m)[m]=e.isBitrix24;babelHelpers.classPrivateFieldLooseBase(this,S)[S]=e.isFreeLicense;babelHelpers.classPrivateFieldLooseBase(this,O)[O]=e.isAdmin;babelHelpers.classPrivateFieldLooseBase(this,C)[C]=e.requisite;babelHelpers.classPrivateFieldLooseBase(this,A)[A]=e.settingsPath;babelHelpers.classPrivateFieldLooseBase(this,R)[R](e);top.BX.addCustomEvent("onLocalStorageSet",(e=>{var s;const t=(s=e==null?void 0:e.key)!=null?s:null;if(t==="onCrmEntityUpdate"||t==="onCrmEntityCreate"){babelHelpers.classPrivateFieldLooseBase(this,D)[D]().then((()=>{babelHelpers.classPrivateFieldLooseBase(this,j)[j]()}))}}))}setTarget(e){babelHelpers.classPrivateFieldLooseBase(this,F)[F]=e;return this}setWidgetPopup(e){babelHelpers.classPrivateFieldLooseBase(this,T)[T]=e;babelHelpers.classPrivateFieldLooseBase(this,T)[T].getPopup().subscribe("onClose",(()=>{s.Event.unbindAll(babelHelpers.classPrivateFieldLooseBase(this,k)[k]().getPopup().getPopupContainer(),"click")}));babelHelpers.classPrivateFieldLooseBase(this,U)[U]().then((()=>{babelHelpers.classPrivateFieldLooseBase(this,j)[j]()}));return this}static bindWidget(e){const s=this.getInstance();if(s){s.setWidgetPopup(e)}return s}static bindAndShow(e){const t=this.getInstance();if(t){s.Event.unbindAll(e);s.Event.bind(e,"click",t.toggle.bind(t,e));t.show(e)}return t}static init(e){if(babelHelpers.classPrivateFieldLooseBase(this,E)[E]===null){babelHelpers.classPrivateFieldLooseBase(this,E)[E]=new this(e)}return babelHelpers.classPrivateFieldLooseBase(this,E)[E]}static getInstance(){return babelHelpers.classPrivateFieldLooseBase(this,E)[E]}toggle(e){const s=babelHelpers.classPrivateFieldLooseBase(this,k)[k]().getPopup();if(s.isShown()){s.close()}else{this.show(e)}}show(e){const t=babelHelpers.classPrivateFieldLooseBase(this,k)[k]().getPopup();t.setBindElement(e);t.show();if(t.getPopupContainer().getBoundingClientRect().left<30){s.Dom.style(t.getPopupContainer(),{left:"30px"})}this.setTarget(e)}}function se(e){e.theme?babelHelpers.classPrivateFieldLooseBase(this,f)[f]=e.theme:null;e.otp?babelHelpers.classPrivateFieldLooseBase(this,I)[I]=e.otp:null;e.holding?babelHelpers.classPrivateFieldLooseBase(this,G)[G](e.holding):null}function te(e){var t,i,a,l;if(!s.Type.isPlainObject(e)){babelHelpers.classPrivateFieldLooseBase(this,N)[N]=null;return}babelHelpers.classPrivateFieldLooseBase(this,N)[N]={isHolding:(t=e.isHolding)!=null?t:false,affiliate:(i=e.affiliate)!=null?i:null,canBeHolding:(a=e.canBeHolding)!=null?a:false,canBeAffiliate:(l=e.canBeAffiliate)!=null?l:false}}function ie(){return babelHelpers.classPrivateFieldLooseBase(this,T)[T]}function ae(e=false){if(e===true||typeof babelHelpers.classPrivateFieldLooseBase(this,f)[f]==="undefined"){return new Promise(((e,t)=>{s.ajax.runComponentAction("bitrix:intranet.settings.widget","getData",{mode:"class"}).then((({data:{theme:s,otp:t,holding:i}})=>{babelHelpers.classPrivateFieldLooseBase(this,f)[f]=s;babelHelpers.classPrivateFieldLooseBase(this,I)[I]=t;babelHelpers.classPrivateFieldLooseBase(this,G)[G](i);e()}))}))}return Promise.resolve()}function le(){return new Promise(((e,t)=>{s.ajax.runComponentAction("bitrix:intranet.settings.widget","getRequisites",{mode:"class"}).then((({data:{requisite:e}})=>{babelHelpers.classPrivateFieldLooseBase(this,C)[C]=e}))}))}function re(){const e=babelHelpers.classPrivateFieldLooseBase(this,k)[k]().getPopup().getPopupContainer();s.Dom.clean(e);s.Dom.append(babelHelpers.classPrivateFieldLooseBase(this,x)[x](),e);const t=[babelHelpers.classPrivateFieldLooseBase(this,m)[m]?babelHelpers.classPrivateFieldLooseBase(this,q)[q]():null,babelHelpers.classPrivateFieldLooseBase(this,C)[C]&&babelHelpers.classPrivateFieldLooseBase(this,O)[O]?babelHelpers.classPrivateFieldLooseBase(this,M)[M]():null,babelHelpers.classPrivateFieldLooseBase(this,O)[O]?babelHelpers.classPrivateFieldLooseBase(this,V)[V]():null,babelHelpers.classPrivateFieldLooseBase(this,Z)[Z]()];t.forEach((t=>{s.Dom.append(t,e)}));s.Dom.append(babelHelpers.classPrivateFieldLooseBase(this,$)[$](),e)}function oe(){const e=e=>{if(BX.clipboard.copy(window.location.origin)){BX.UI.Notification.Center.notify({content:s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_LINK_COPIED_POPUP"),position:"top-left",autoHideDelay:3e3})}};const a=s.Tag.render(o||(o=r`
				<div class="intranet-settings-widget__header">
					<div class="intranet-settings-widget__header_inner">
						<span class="intranet-settings-widget__header-name">${0}</span>
						<span class='ui-icon-set --link-3 intranet-settings-widget__header-btn' onclick="${0}"></span>
					</div>
				</div>
			`),window.location.host,e);babelHelpers.classPrivateFieldLooseBase(this,K)[K](a,babelHelpers.classPrivateFieldLooseBase(this,f)[f]);const l=new t.PopupComponentsMakerItem({withoutBackground:true,html:a}).getContainer();s.Dom.addClass(l,"--widget-header");i.EventEmitter.subscribe("BX.Intranet.Bitrix24:ThemePicker:onThemeApply",(({data:{theme:e}})=>{babelHelpers.classPrivateFieldLooseBase(this,K)[K](a,e)}));return l}function ne(e,t){const i=`url('${s.Text.encode(t.previewImage)}')`;s.Dom.style(e,"backgroundImage",i);s.Dom.removeClass(e,"bitrix24-theme-dark bitrix24-theme-light");const a=String(t.id).indexOf("dark:")===0?"bitrix24-theme-dark":"bitrix24-theme-light";s.Dom.addClass(e,a)}function ce(){const e=()=>{babelHelpers.classPrivateFieldLooseBase(this,k)[k]().close();BX.UI.InfoHelper.show("info_implementation_request")};const t=s.Tag.render(n||(n=r`
			<span class="intranet-settings-widget__footer-item" onclick="${0}">
				${0}
			</span>
		`),e,s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_ORDER_PARTNER_LINK"));const i=()=>{if(top.BX.Helper){babelHelpers.classPrivateFieldLooseBase(this,k)[k]().close();top.BX.Helper.show("redirect=detail&code=18371844")}};const a=()=>{if(top.BX.Helper){babelHelpers.classPrivateFieldLooseBase(this,k)[k]().close();if(babelHelpers.classPrivateFieldLooseBase(this,S)[S]){BX.UI.InfoHelper.show("limit_support_bitrix")}else{BX.Helper.show("redirect=detail&code=12925062")}}};return s.Tag.render(c||(c=r`
				<div class="intranet-settings-widget__footer">
					${0}
					<span class="intranet-settings-widget__footer-item" onclick="${0}">
						${0}
					</span>
					<span class="intranet-settings-widget__footer-item" onclick="${0}">
						${0}
					</span>
				</div>
			`),babelHelpers.classPrivateFieldLooseBase(this,m)[m]?t:"",i,s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_WHERE_TO_BEGIN_LINK"),a,s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_SUPPORT_BUTTON"))}function de(e){const t=babelHelpers.classPrivateFieldLooseBase(this,k)[k]().getItem({html:e});const i=t.getContainer();s.Dom.addClass(i,"--widget-item");return i}function be(){const e=e=>{window.open(babelHelpers.classPrivateFieldLooseBase(this,C)[C].publicUrl,"_blank");babelHelpers.classPrivateFieldLooseBase(this,H)[H].close();babelHelpers.classPrivateFieldLooseBase(this,k)[k]().close()};const t=e=>{if(BX.clipboard.copy(babelHelpers.classPrivateFieldLooseBase(this,C)[C].publicUrl)){BX.UI.Notification.Center.notify({content:s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_COPIED_POPUP"),position:"top-left",autoHideDelay:3e3})}};const i=()=>{o.setWaiting(true);babelHelpers.classPrivateFieldLooseBase(this,X)[X]().then((()=>{babelHelpers.classPrivateFieldLooseBase(this,H)[H]=null;babelHelpers.classPrivateFieldLooseBase(this,j)[j]();if(!babelHelpers.classPrivateFieldLooseBase(this,C)[C].isPublic){const e=new a.Popup("public-landing-error",babelHelpers.classPrivateFieldLooseBase(this,k)[k]().getPopup().getPopupContainer().querySelector('[data-role="requisite-widget-title"]'),{autoHide:true,closeByEsc:true,angle:true,darkMode:true,content:s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_CREATE_LANDING_ERROR"),events:{onShow:()=>{setTimeout((()=>{s.Event.bindOnce(babelHelpers.classPrivateFieldLooseBase(this,k)[k]().getPopup().getPopupContainer(),"click",(()=>{e.close()}))}),0)},onClose:()=>{e.destroy()}}});e.show()}}))};const l=e=>{babelHelpers.classPrivateFieldLooseBase(this,k)[k]().close();BX.SidePanel.Instance.open("/crm/company/details/0/?mycompany=y")};const o=new BX.UI.Button({id:"requisite-btn",text:babelHelpers.classPrivateFieldLooseBase(this,C)[C].isConnected?s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_REDIRECT_TO_REQUISITE_BUTTON"):babelHelpers.classPrivateFieldLooseBase(this,C)[C].isCompanyCreated?s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_CREATE_LANDING"):s.Loc.getMessage("INTRANET_SETTINGS_CONFIGURE_REQUISITE_BUTTON"),noCaps:true,onclick:babelHelpers.classPrivateFieldLooseBase(this,C)[C].isConnected?e:babelHelpers.classPrivateFieldLooseBase(this,C)[C].isCompanyCreated?i:l,className:"ui-btn ui-btn-light-border ui-btn-round ui-btn-xs ui-btn-no-caps intranet-setting__btn-light"});const n=e=>{if(!babelHelpers.classPrivateFieldLooseBase(this,H)[H]){const i=()=>{window.open(babelHelpers.classPrivateFieldLooseBase(this,C)[C].editUrl,"_blank");babelHelpers.classPrivateFieldLooseBase(this,H)[H].close();babelHelpers.classPrivateFieldLooseBase(this,k)[k]().close()};let a=null;if(babelHelpers.classPrivateFieldLooseBase(this,C)[C].publicUrl){a={html:`\n\t\t\t\t\t\t\t<div class="intranet-settings-widget__popup-item">\n\t\t\t\t\t\t\t\t<div class="ui-icon-set --link-3"></div> \n\t\t\t\t\t\t\t\t<div class="intranet-settings-widget__popup-name">${s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_COPY_LINK_BUTTON")}</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t`,onclick:t}}let l=null;if(babelHelpers.classPrivateFieldLooseBase(this,C)[C].editUrl){l={html:`\n\t\t\t\t\t\t\t<div class="intranet-settings-widget__popup-item">\n\t\t\t\t\t\t\t\t<div class="ui-icon-set --paint-1"></div> \n\t\t\t\t\t\t\t\t<div class="intranet-settings-widget__popup-name">${s.Loc.getMessage("INTRANET_SETTINGS_CONFIGURE_CUTAWAY_SITE_BUTTON")}</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t`,onclick:i}}const r=()=>{if(babelHelpers.classPrivateFieldLooseBase(this,H)[H]){babelHelpers.classPrivateFieldLooseBase(this,H)[H].close()}babelHelpers.classPrivateFieldLooseBase(this,k)[k]().close();BX.SidePanel.Instance.open(babelHelpers.classPrivateFieldLooseBase(this,A)[A]+"?page=requisite&analyticContext=widget_settings_settings",{width:1034})};const o={html:`\n\t\t\t\t\t\t<div class="intranet-settings-widget__popup-item">\n\t\t\t\t\t\t\t<div class="ui-icon-set --pencil-40"></div>\n\t\t\t\t\t\t\t<div class="intranet-settings-widget__popup-name">${s.Loc.getMessage("INTRANET_SETTINGS_CONFIGURE_REQUISITE_BUTTON")}</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t`,onclick:r};const n=240;babelHelpers.classPrivateFieldLooseBase(this,H)[H]=BX.PopupMenu.create("requisites-settings",e.currentTarget,[a,o,l],{closeByEsc:true,autoHide:true,width:n,offsetLeft:-72,angle:{offset:n/2-15},events:{onShow:()=>{setTimeout((()=>{s.Event.bindOnce(babelHelpers.classPrivateFieldLooseBase(this,k)[k]().getPopup().getPopupContainer(),"click",(()=>{babelHelpers.classPrivateFieldLooseBase(this,H)[H].close()}))}),0)}}})}babelHelpers.classPrivateFieldLooseBase(this,H)[H].show()};const c=babelHelpers.classPrivateFieldLooseBase(this,C)[C].isCompanyCreated?s.Tag.render(d||(d=r`
				<span onclick="${0}" class="intranet-settings-widget__requisite-btn">
					<i class='ui-icon-set --more-information'></i>
				</span>
			`),n):``;const p=s.Tag.render(b||(b=r`
			<div class="intranet-settings-widget__business-card intranet-settings-widget_box">
				<div class="intranet-settings-widget__business-card_head intranet-settings-widget_inner">
					<div class="intranet-settings-widget_icon-box --gray">
						<div class="ui-icon-set --customer-card-1"></div>
					</div>
					<div class="intranet-settings-widget__title" data-role="requisite-widget-title">
						${0}
					</div>
					<i class="ui-icon-set --help" onclick="BX.Helper.show('redirect=detail&code=18213326')"></i>
				</div>

				<div class="intranet-settings-widget__business-card_footer">
					${0}
					${0}
				</div>
			</div>
		`),babelHelpers.classPrivateFieldLooseBase(this,C)[C].isConnected?s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_SECTION_REQUISITE_SITE_TITLE"):s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_SECTION_REQUISITE_TITLE"),o.getContainer(),c);return babelHelpers.classPrivateFieldLooseBase(this,W)[W](p)}function pe(){return new Promise(((e,t)=>{s.ajax.runComponentAction("bitrix:intranet.settings.widget","createRequisiteLanding",{mode:"class"}).then((({data:{isConnected:s,isPublic:t,publicUrl:i,editUrl:a}})=>{babelHelpers.classPrivateFieldLooseBase(this,C)[C].isConnected=s;babelHelpers.classPrivateFieldLooseBase(this,C)[C].isPublic=t;babelHelpers.classPrivateFieldLooseBase(this,C)[C].publicUrl=i;babelHelpers.classPrivateFieldLooseBase(this,C)[C].editUrl=a;e()}))}))}function ve(){if(babelHelpers.classPrivateFieldLooseBase(this,m)[m]!==true||babelHelpers.classPrivateFieldLooseBase(this,N)[N]===null){return null}if(!s.Type.isPlainObject(babelHelpers.classPrivateFieldLooseBase(this,N)[N].affiliate)){return babelHelpers.classPrivateFieldLooseBase(this,Y)[Y]()}const e=babelHelpers.classPrivateFieldLooseBase(this,N)[N].affiliate;const t=()=>{babelHelpers.classPrivateFieldLooseBase(this,k)[k]().close();babelHelpers.classPrivateFieldLooseBase(this,Q)[Q]().show(babelHelpers.classPrivateFieldLooseBase(this,F)[F])};const i=e.profilePhoto?`background: url('${encodeURI(s.Text.encode(e.profilePhoto))}') no-repeat center; background-size: cover;`:"";const a=s.Tag.render(p||(p=r`
		<div class="intranet-settings-widget__branch" onclick="${0}">
			<div class="intranet-settings-widget__branch-icon_box">
				<div class="ui-icon ui-icon-common-user intranet-settings-widget__branch-icon">
					<i style="${0}"></i>
				</div>
			</div>
			<div class="intranet-settings-widget__branch_content">
				<div class="intranet-settings-widget__branch-title">
					${0}
				</div>
				<div class="intranet-settings-widget__title">
					${0}
				</div>
			</div>
			<div class="intranet-settings-widget__branch-btn_box">
				<button class="ui-btn ui-btn-light-border ui-btn-round ui-btn-xs ui-btn-no-caps intranet-setting__btn-light">
					${0}
				</button>
			</div>
		</div>
		`),t,i,e.isHolding?s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_MAIN_BRANCH"):s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_SECONDARY_BRANCH"),e.name,s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_BRANCHES"));return babelHelpers.classPrivateFieldLooseBase(this,W)[W](a)}function ge(){if(!babelHelpers.classPrivateFieldLooseBase(this,y)[y]){babelHelpers.classPrivateFieldLooseBase(this,y)[y]=BX.Intranet.HoldingWidget.getInstance();const e=()=>{babelHelpers.classPrivateFieldLooseBase(this,y)[y].getWidget().close();this.show()};const t=s.Tag.render(v||(v=r`
				<div class="intranet-settings-widget__close-btn">
					<div onclick="${0}" class="ui-icon-set --arrow-left intranet-settings-widget__close-btn_icon"></div>
					<div class="intranet-settings-widget__close-btn_name">${0}</div>
				</div>
			`),e,s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_BRANCH_LIST"));babelHelpers.classPrivateFieldLooseBase(this,y)[y].getWidget().getPopup().getContentContainer().prepend(t)}return babelHelpers.classPrivateFieldLooseBase(this,y)[y]}function ue(){if(!s.Type.isPlainObject(babelHelpers.classPrivateFieldLooseBase(this,N)[N])){return null}const e=babelHelpers.classPrivateFieldLooseBase(this,O)[O]?s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_FILIAL_NETWORK"):s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_FILIAL_NETWORK_UNAVAILABLE");const t=babelHelpers.classPrivateFieldLooseBase(this,O)[O]?s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_FILIAL_SETTINGS"):s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_FILIAL_ABOUT");const i=()=>{babelHelpers.classPrivateFieldLooseBase(this,k)[k]().close();if(babelHelpers.classPrivateFieldLooseBase(this,N)[N].canBeHolding){babelHelpers.classPrivateFieldLooseBase(this,Q)[Q]().show(babelHelpers.classPrivateFieldLooseBase(this,F)[F])}else{BX.UI.InfoHelper.show("limit_office_multiple_branches")}};const a=s.Tag.render(g||(g=r`
			<div class="intranet-settings-widget__branch-lock-icon_box">
				<div class="ui-icon-set intranet-settings-widget__branch-lock-icon --lock"></div>
			</div>
		`));const l=s.Tag.render(u||(u=r`
			<div class="intranet-settings-widget__branch" onclick="${0}">
				<div class="intranet-settings-widget__branch-icon_box">
					<div class="ui-icon-set intranet-settings-widget__branch-icon --filial-network"></div>
					${0}
				</div>
				<div class="intranet-settings-widget__branch_content">
					<div class="intranet-settings-widget__title">${0}</div>
				</div>
				<div class="intranet-settings-widget__branch-btn_box">
					<button class="ui-btn ui-btn-light-border ui-btn-round ui-btn-xs ui-btn-no-caps intranet-setting__btn-light">${0}</button>
				</div>
			</div>
		`),i,!babelHelpers.classPrivateFieldLooseBase(this,N)[N].canBeHolding?a:"",e,t);return babelHelpers.classPrivateFieldLooseBase(this,W)[W](l)}function he(){return s.Tag.render(h||(h=r`
			<div class="intranet-settings-widget_inline-box">
				${0}
				${0}
			</div>
		`),babelHelpers.classPrivateFieldLooseBase(this,z)[z](),babelHelpers.classPrivateFieldLooseBase(this,J)[J]())}function _e(){const e=e=>{babelHelpers.classPrivateFieldLooseBase(this,k)[k]().close();BX.SidePanel.Instance.open(babelHelpers.classPrivateFieldLooseBase(this,A)[A]+"?page=security&analyticContext=widget_settings_settings",{width:1034})};const t=s.Tag.render(_||(_=r`
			<span onclick="${0}" class="intranet-settings-widget_box --clickable">
				<div class="intranet-settings-widget_inner">
					<div class="intranet-settings-widget_icon-box ${0}">
						<div class="ui-icon-set --shield"></div>
					</div>
					<div class="intranet-settings-widget__title">
						${0}
					</div>
				</div>
				<div class="intranet-settings-widget__arrow-btn ui-icon-set --arrow-right"></div>
			</span>
		`),e,babelHelpers.classPrivateFieldLooseBase(this,I)[I].IS_ACTIVE==="Y"?"--green":"--yellow",s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_SECTION_SECURITY_TITLE"));return babelHelpers.classPrivateFieldLooseBase(this,W)[W](t)}function Pe(){const e=e=>{babelHelpers.classPrivateFieldLooseBase(this,k)[k]().close();BX.SidePanel.Instance.open(babelHelpers.classPrivateFieldLooseBase(this,A)[A]+"?analyticContext=widget_settings_settings",{width:1034})};const t=s.Tag.render(P||(P=r`
			<span onclick="${0}" class="intranet-settings-widget_box --clickable">
				<div class="intranet-settings-widget_inner">
					<div class="intranet-settings-widget_icon-box --gray">
						<div class="ui-icon-set --settings-2"></div>
					</div>
					<div class="intranet-settings-widget__title">
						${0}
					</div>
				</div>
				<div class="intranet-settings-widget__arrow-btn ui-icon-set --arrow-right"></div>
			</span>
		`),e,s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_SECTION_SETTINGS_TITLE"));return babelHelpers.classPrivateFieldLooseBase(this,W)[W](t)}function Le(){const e=e=>{babelHelpers.classPrivateFieldLooseBase(this,k)[k]().close();BX.SidePanel.Instance.open(`${babelHelpers.classPrivateFieldLooseBase(this,w)[w]}category/migration/`)};const t=s.Tag.render(L||(L=r`
			<div onclick="${0}" class="intranet-settings-widget_box --clickable">
				<div class="intranet-settings-widget_inner">
					<div class="intranet-settings-widget_icon-box --gray">
						<div class="ui-icon-set --market-1"></div>
					</div>
					<div class="intranet-settings-widget__title">
						${0}
					</div>
				</div>
				<div class="intranet-settings-widget__arrow-btn ui-icon-set --arrow-right"></div>
			</div>
		`),e,s.Loc.getMessage("INTRANET_SETTINGS_WIDGET_SECTION_MIGRATION_TITLE"));return babelHelpers.classPrivateFieldLooseBase(this,W)[W](t)}Object.defineProperty(ee,E,{writable:true,value:null});e.SettingsWidget=ee})(this.BX.Intranet=this.BX.Intranet||{},BX,BX.UI,BX.Event,BX.Main,BX.UI.Analytics);
/* End */
;; /* /bitrix/components/bitrix/intranet.settings.widget/templates/.main/script.min.js?170347923923708*/

//# sourceMappingURL=template_7593e4ebfaacfd350e94e3443a0b2726.map.js