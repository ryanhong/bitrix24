<?php
$MESS["BPMOBILE_TASK_DETAILS_ACTION_DELEGATE"] = "Delegieren";
$MESS["BPMOBILE_TASK_DETAILS_ALERT_CANCEL"] = "Abbrechen";
$MESS["BPMOBILE_TASK_DETAILS_ALERT_DISCARD"] = "Schließen ohne zu speichern";
$MESS["BPMOBILE_TASK_DETAILS_ALERT_TEXT"] = "Möchten Sie die Änderungen speichern? Alle Änderungen gehen verloren, wenn Sie jetzt schließen.";
$MESS["BPMOBILE_TASK_DETAILS_ALERT_TITLE"] = "Änderungen speichern";
$MESS["BPMOBILE_TASK_DETAILS_COMMENTS_DEVELOPING"] = "Bald verfügbar.";
$MESS["BPMOBILE_TASK_DETAILS_COMMENTS_DEVELOPING_1"] = "Kommentare werden bald verfügbar sein";
$MESS["BPMOBILE_TASK_DETAILS_COMMENTS_LIKE"] = "Gefällt mir";
$MESS["BPMOBILE_TASK_DETAILS_COMMENTS_TITLE"] = "Kommentare";
$MESS["BPMOBILE_TASK_DETAILS_TIMELINE"] = "Protokoll";
$MESS["BPMOBILE_TASK_DETAILS_TITLE"] = "Aufgabe";
$MESS['BPMOBILE_TASK_DETAILS_RPA'] = "Aufgaben können nur in der Webversion erledigt werden";
