<?php
$MESS["M_RP_SM_HELPDESK"] = "它是如何工作的？";
$MESS["M_RP_SM_INSTRUCTION_EDITING"] = "這是您的SMS消息，因為您的客戶會看到它。如果您需要更改鏈接位置，請複制＃鏈接＃並將其粘貼到您想要的位置。";
$MESS["M_RP_SM_INSTRUCTION_READ_ONLY"] = "這是您的SMS消息，因為您的客戶會看到它。要編輯消息，請單擊鉛筆圖標。";
$MESS["M_RP_SM_LINK_FOR_PAYMENT"] = "付款鏈接";
$MESS["M_RP_SM_MESSAGE_TEMPLATE_ERROR"] = "添加＃鏈接＃顯示鏈接";
$MESS["M_RP_SM_MORE_DETAILS"] = "細節";
$MESS["M_RP_SM_PROGRESS_BAR_TITLE"] = "客戶將收到SMS到＃Contact_phone＃";
$MESS["M_RP_SM_SEND"] = "發送";
$MESS["M_RP_SM_SENDING_VIA_BITRIX24"] = "通過Bitrix24發送";
$MESS["M_RP_SM_SENDING_VIA_SMS_SERVICE"] = "使用短信使用";
$MESS["M_RP_SM_SMS_SERVICE_MENU_TITLE"] = "選擇SMS服務";
$MESS["M_RP_SM_SMS_SERVICE_SETTINGS_TITLE"] = "連接另一個服務";
$MESS["M_RP_SM_TEMPLATE_BASED_MESSAGE_WILL_BE_SENT"] = "消息將使用模板發送。";
$MESS["M_RP_SM_TITLE"] = "訊息";
