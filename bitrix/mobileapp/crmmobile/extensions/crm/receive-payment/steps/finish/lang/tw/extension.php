<?php
$MESS["M_RP_F_BACK_BUTTON_TEXT_DEAL"] = "回去交易";
$MESS["M_RP_F_MESSAGE_ERROR"] = "付款錯誤";
$MESS["M_RP_F_MESSAGE_ERROR_BOTTOM"] = "重試或聯繫您的管理員。錯誤代碼：＃error_code＃";
$MESS["M_RP_F_MESSAGE_IS_SENDING"] = "發送消息...";
$MESS["M_RP_F_MESSAGE_IS_SENT_BOTTOM"] = "觀看交易時間表中的付款狀態";
$MESS["M_RP_F_MESSAGE_IS_SENT_ORDER_PAYMENT"] = "發送給客戶的付款鏈接";
$MESS["M_RP_F_SENDING_TYPE_BITRIX24"] = "使用Bitrix24發送短信";
$MESS["M_RP_F_SENDING_TYPE_SMS"] = "使用＃名稱＃發送短信";
$MESS["M_RP_F_TITLE"] = "發信息";
