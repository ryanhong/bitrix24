<?php
$MESS["M_CRM_TL_EPC_FINISH_STEP_BACK_TO_DEAL"] = "回去交易";
$MESS["M_CRM_TL_EPC_FINISH_STEP_CREATING_PAYMENT"] = "保存...";
$MESS["M_CRM_TL_EPC_FINISH_STEP_ERROR_TEXT"] = "重試或聯繫您的BitRix24管理員";
$MESS["M_CRM_TL_EPC_FINISH_STEP_ERROR_TITLE"] = "無法創造付款";
$MESS["M_CRM_TL_EPC_FINISH_STEP_RECEIVE_PAYMENT"] = "收到付款";
$MESS["M_CRM_TL_EPC_FINISH_STEP_SENT_TO_TERMINAL"] = "完畢！";
$MESS["M_CRM_TL_EPC_FINISH_STEP_SUCCESS_BOTTOM_TEXT"] = "＃名稱＃可以通過終端接收付款";
$MESS["M_CRM_TL_EPC_FINISH_STEP_SUCCESS_BOTTOM_TEXT_CASE_SELF"] = "＃名稱＃（您）可以通過終端接收付款";
$MESS["M_CRM_TL_EPC_FINISH_STEP_TITLE"] = "創建付款";
$MESS["M_CRM_TL_EPC_PRODUCT_STEP_NEXT_STEP_BTN_TEXT"] = "創造";
$MESS["M_CRM_TL_EPC_PRODUCT_STEP_PROGRESS_BAR_TITLE"] = "訂購項目";
$MESS["M_CRM_TL_EPC_PRODUCT_WIZARD_TITLE"] = "通過終端付款";
$MESS["M_CRM_TL_EPC_RESPONSIBLE_STEP_DESCRIPTION"] = "選定的員工將在終端屏幕上的Bitrix24應用中收到付款";
$MESS["M_CRM_TL_EPC_RESPONSIBLE_STEP_PROGRESS_BAR_TITLE"] = "負責付款的人";
