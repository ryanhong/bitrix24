<?php
$MESS["M_CRM_DOCUMENT_QR_BANK_APP_NOTE"] = "QR碼必須使用銀行應用程序進行掃描";
$MESS["M_CRM_DOCUMENT_QR_DESCRIPTION"] = "您的客戶將掃描QR碼以在其銀行應用中獲取付款詳細信息";
$MESS["M_CRM_DOCUMENT_QR_ERROR"] = "錯誤創建QR代碼";
$MESS["M_CRM_DOCUMENT_QR_TITLE"] = "使用QR碼付款";
