<?php
$MESS["SE_CHAT_AUTOPLAY_TITLE_V2"] = "自動播放";
$MESS["SE_CHAT_AUTOPLAY_VIDEO_TITLE_V2"] = "影片";
$MESS["SE_CHAT_BETA_CALL_ENABLE_TITLE_V2"] = "測試電話";
$MESS["SE_CHAT_BETA_ENABLE_TITLE"] = "使能夠";
$MESS["SE_CHAT_BETA_ENABLE_TITLE_V2"] = "測試聊天";
$MESS["SE_CHAT_BETA_TITLE"] = "新聊天（beta）";
$MESS["SE_CHAT_BETA_TITLE_V2"] = "測試版";
$MESS["SE_CHAT_DESC"] = "下次您打開對話框窗口時，將應用此組設置的任何更改。";
$MESS["SE_CHAT_ENABLE_TITLE"] = "使用新聊天";
$MESS["SE_CHAT_HISTORY_SHOW_TITLE"] = "邀請時展示歷史";
$MESS["SE_CHAT_HISTORY_SHOW_TITLE_V2"] = "邀請聊天時顯示消息歷史記錄";
$MESS["SE_CHAT_HISTORY_TITLE"] = "聊天歷史";
$MESS["SE_CHAT_LOCAL_STORAGE_ENABLE_DESCRIPTION"] = "此選項允許在移動設備上緩存和保存消息。這增加了聊天性能，並保證無需可用網絡連接的消息即可快速訪問消息";
$MESS["SE_CHAT_LOCAL_STORAGE_ENABLE_TITLE"] = "緩存消息";
$MESS["SE_CHAT_TITLE"] = "聊天";
