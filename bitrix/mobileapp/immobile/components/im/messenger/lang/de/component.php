<?php
$MESS["IMMOBILE_MESSENGER_DIALOG_ACCESS_ERROR_TEXT"] = "Bitten Sie Ihre Kollegen darum, Sie zu diesem Chat hinzuzufügen";
$MESS["IMMOBILE_MESSENGER_DIALOG_ACCESS_ERROR_TITLE"] = "Das ist ein privater Chat";
$MESS["IMMOBILE_MESSENGER_HEADER"] = "Chats und Anrufe";
$MESS["IMMOBILE_MESSENGER_HEADER_CONNECTION"] = "Verbinden?";
$MESS["IMMOBILE_MESSENGER_HEADER_NETWORK_WAITING"] = "Warten auf Netzwerk?";
$MESS["IMMOBILE_MESSENGER_HEADER_SYNC"] = "Wird aktualisiert...";
$MESS["IMMOBILE_MESSENGER_REFRESH_ERROR"] = "Es war uns nicht möglich, die Verbindung zu Ihrem Bitrix24 herzustellen, evtl. wegen schlechter Internetverbindung. Wir werden es in wenigen Sekunden erneut versuchen.";
$MESS["IMMOBILE_MESSENGER_UPDATE_FOR_NEW_FEATURES_HINT"] = "Aktualisieren Sie die App jetzt und genießen Sie die neuen Bitrix24 Funktionen";
