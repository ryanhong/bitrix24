<?php
$MESS["IM_USER_API_ERROR"] = "執行請求時錯誤。請稍後再試。";
$MESS["IM_USER_CONNECTION_ERROR"] = "連接到Bitrix24的錯誤。請檢查網絡連接。";
$MESS["IM_USER_LIST_EMPTY"] = " - 用戶列表為空 - ";
$MESS["IM_USER_LIST_KICK"] = "消除";
$MESS["IM_USER_LIST_OWNER"] = "設置為所有者";
