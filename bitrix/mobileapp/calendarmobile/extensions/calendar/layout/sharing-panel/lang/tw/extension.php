<?php
$MESS["L_ML_BUTTON_SHARE"] = "共享老虎機";
$MESS["L_ML_BUTTON_VIEW_AS_GUEST"] = "我的聯繫人會看到什麼？";
$MESS["L_ML_DESCRIPTION_1"] = "發送您的聯繫人鏈接到您可用的開放插槽，讓他們選擇他們喜歡的插槽。";
$MESS["L_ML_DESCRIPTION_2"] = "查看可用的開放插槽，就像收件人所看到的那樣";
$MESS["L_ML_SHARE_LINK_MESSAGE"] = "請查看我可用的開放插槽，然後選擇適合您日程安排的插槽";
