<?php
$MESS["M_CALENDAR_SETTINGS_DESCRIPTION"] = "Verfügbare Zeitfenster";
$MESS["M_CALENDAR_SETTINGS_DURATION"] = "Dauer";
$MESS["M_CALENDAR_SETTINGS_FROM"] = "Verfügbar von";
$MESS["M_CALENDAR_SETTINGS_MEETINGS_DURATION"] = "Termindauer";
$MESS["M_CALENDAR_SETTINGS_READONLY"] = "Sie können Zeitfenster anderer Personen nicht bearbeiten.";
$MESS["M_CALENDAR_SETTINGS_SELECT_ADD"] = "Hinzufügen";
$MESS["M_CALENDAR_SETTINGS_SELECT_ADD_RANGE"] = "Weitere Zeitfenster hinzufügen";
$MESS["M_CALENDAR_SETTINGS_SELECT_WEEKDAYS"] = "Tage der Woche auswählen";
$MESS["M_CALENDAR_SETTINGS_TO"] = "Verfügbar bis";
