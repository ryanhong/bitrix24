<?php
$MESS["L_MS_CONFIRMATION_BUTTON_OK"] = "密切訪問";
$MESS["L_MS_CONFIRMATION_TEXT"] = "當您再次打開對插槽的訪問時，將創建一個新的來賓鏈接。舊鏈接將變得無效";
$MESS["L_MS_CONFIRMATION_TEXT_1"] = "當您再次打開對插槽的訪問時，將創建一個新的鏈接。舊鏈接將變得無效";
$MESS["L_MS_DESCRIPTION"] = "安排與外部參與者的會議";
$MESS["L_MS_SWITCHER"] = "打開插槽";
