<?php
$MESS["M_CALENDAR_AHA_SYNC_CALENDAR_DESC"] = "將日曆與Google，iCloud或其他日曆合併，以進一步改善您的時間管理";
$MESS["M_CALENDAR_AHA_SYNC_CALENDAR_TITLE"] = "同步日曆";
$MESS["M_CALENDAR_AHA_SYNC_ERROR_DESC"] = "密碼可能已更改。請檢查同步日曆區域中的問題";
$MESS["M_CALENDAR_AHA_SYNC_ERROR_TITLE"] = "無法同步事件";
