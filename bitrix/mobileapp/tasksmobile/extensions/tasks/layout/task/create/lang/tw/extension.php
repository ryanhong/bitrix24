<?php
$MESS["TASKSMOBILE_LAYOUT_TASK_CREATE_BUTTON_CREATE"] = "創造";
$MESS["TASKSMOBILE_LAYOUT_TASK_CREATE_CANCEL_ALERT_DESCRIPTION"] = "您確定要關閉新任務表嗎？";
$MESS["TASKSMOBILE_LAYOUT_TASK_CREATE_CANCEL_ALERT_NO"] = "取消";
$MESS["TASKSMOBILE_LAYOUT_TASK_CREATE_CANCEL_ALERT_TITLE"] = "任務沒有保存";
$MESS["TASKSMOBILE_LAYOUT_TASK_CREATE_CANCEL_ALERT_YES"] = "關閉而無需保存";
$MESS["TASKSMOBILE_LAYOUT_TASK_CREATE_SAVE_ERROR_LOADING_FILES"] = "並非所有文件都已上傳。";
$MESS["TASKSMOBILE_LAYOUT_TASK_CREATE_SECTION_MORE"] = "更多的";
$MESS["TASKSMOBILE_LAYOUT_TASK_CREATE_TITLE"] = "新任務";
