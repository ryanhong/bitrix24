<?php
$MESS["TASKSMOBILE_LAYOUT_TASK_VIEW_CANCEL_ALERT_CONTINUE"] = "Weiter bearbeiten";
$MESS["TASKSMOBILE_LAYOUT_TASK_VIEW_CANCEL_ALERT_DESCRIPTION"] = "Alle Änderungen gehen verloren, wenn Sie jetzt beenden.";
$MESS["TASKSMOBILE_LAYOUT_TASK_VIEW_CANCEL_ALERT_DISCARD"] = "Beenden ohne zu speichern";
$MESS["TASKSMOBILE_LAYOUT_TASK_VIEW_CANCEL_ALERT_SAVE"] = "Speichern und beenden";
$MESS["TASKSMOBILE_LAYOUT_TASK_VIEW_CANCEL_ALERT_TITLE"] = "Möchten Sie die Änderungen speichern?";
$MESS["TASKSMOBILE_LAYOUT_TASK_VIEW_COMMENTS"] = "Kommentare";
$MESS["TASKSMOBILE_LAYOUT_TASK_VIEW_NO_TASK_ALERT_BUTTON_OK"] = "OK";
$MESS["TASKSMOBILE_LAYOUT_TASK_VIEW_NO_TASK_ALERT_DESCRIPTION"] = "Aufgabe nicht gefunden oder Zugriff verweigert.";
$MESS["TASKSMOBILE_LAYOUT_TASK_VIEW_NO_TASK_ALERT_TITLE"] = "Zugriff verweigert";
$MESS["TASKSMOBILE_LAYOUT_TASK_VIEW_SAVE_BUTTON"] = "Speichern";
$MESS["TASKSMOBILE_LAYOUT_TASK_VIEW_SAVE_ERROR_LOADING_FILES"] = "Nicht alle Dateien wurden heruntergeladen.";
$MESS["TASKSMOBILE_LAYOUT_TASK_VIEW_SAVE_ERROR_NO_TITLE"] = "Aufgabenname ist erforderlich.";
$MESS["TASKSMOBILE_LAYOUT_TASK_VIEW_SAVING_BUTTON"] = "Wird gespeichert";
$MESS["TASKSMOBILE_LAYOUT_TASK_VIEW_SECTION_MORE"] = "Mehr";
