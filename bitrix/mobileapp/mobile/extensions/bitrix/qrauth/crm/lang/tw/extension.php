<?php
$MESS["CRM_DESKTOP_OPEN"] = "在桌面計算機上打開瀏覽器中的CRM。完成初始配置後，您可以回到這裡繼續使用CRM。";
$MESS["CRM_TITLE"] = "通過提供初始配置和連接通道開始使用CRM。";
$MESS["QR_EXTERNAL_AUTH"] = "使用QR代碼登錄";
