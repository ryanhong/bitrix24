<?php
$MESS["M_UI_KANBAN_EMPTY_LIST_TEXT"] = "創建第一個項目";
$MESS["M_UI_KANBAN_EMPTY_SEARCH_TEXT"] = "找不到條目";
$MESS["M_UI_KANBAN_ERROR_ON_SAVE"] = "由於發生錯誤而無法保存";
$MESS["M_UI_KANBAN_ERROR_ON_SAVE_INTERNAL"] = "發生了一個內部的錯誤。";
$MESS["M_UI_KANBAN_ERROR_ON_SAVE_INTERNAL_TEXT"] = "請稍後再試。 \ nif問題仍然存在，請聯繫您的Bitrix24管理員或負責CRM的人員。";
$MESS["M_UI_KANBAN_INTERNAL_ERROR_GOT_IT"] = "好的";
$MESS["M_UI_KANBAN_INTERNAL_ERROR_TEXT"] = "請稍後再試。 \ nif問題仍然存在，請聯繫您的Bitrix24管理員";
$MESS["M_UI_KANBAN_INTERNAL_ERROR_TITLE"] = "內部錯誤";
$MESS["M_UI_KANBAN_PUBLIC_ERROR_TITLE"] = "無法執行動作。稍後再試";
