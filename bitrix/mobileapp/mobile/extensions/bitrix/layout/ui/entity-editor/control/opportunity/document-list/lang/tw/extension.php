<?php
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_ADD"] = "添加記錄";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_CANCEL_DEDUCT_WARNING_REALIZATION"] = "取消銷售訂單的許可不足。";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_CANCEL_DEDUCT_WARNING_TITLE_REALIZATION"] = "無法取消銷售訂單。";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_CREATION_MENU_DELIVERY"] = "送貨";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_CREATION_MENU_PAYMENT"] = "支付";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_CREATION_MENU_REALIZATION"] = "銷售訂單";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_CREATION_MENU_TERMINAL_PAYMENT"] = "通過終端付款";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_DEAL_TITLE"] = "交易記錄";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_DEDUCT_WARNING_REALIZATION"] = "未足夠處理銷售訂單的許可。";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_DEDUCT_WARNING_TITLE_REALIZATION"] = "無法處理銷售訂單。";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_DELETE_CONFIRM_PAYMENT"] = "您確定要刪除付款嗎？";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_DELETE_CONFIRM_REALIZATION"] = "您確定要刪除銷售記錄嗎？";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_DELETE_CONFIRM_SHIPMENT"] = "您確定要刪除交貨記錄嗎？";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_DELETE_CONFIRM_TITLE"] = "刪除記錄";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_DELETE_WARNING_ACCESS_DENIED_REALIZATION"] = "刪除銷售訂單的許可不足。";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_DELETE_WARNING_PAYMENT"] = "無法刪除付費狀態的付款";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_DELETE_WARNING_REALIZATION"] = "無法刪除已處理狀態的銷售記錄";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_DELETE_WARNING_SHIPMENT"] = "無法刪除交貨狀態的交貨記錄";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_DELETE_WARNING_TITLE_PAYMENT"] = "無法刪除付款";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_DELETE_WARNING_TITLE_REALIZATION"] = "無法刪除銷售";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_DELETE_WARNING_TITLE_SHIPMENT"] = "無法刪除交貨";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_PAYMENT_AMOUNT"] = "總和＃";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_PAYMENT_DATE"] = "付款## account_number＃，＃日期＃";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_PAYMENT_MENU"] = "總和＃";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_PAYMENT_MENU_CHANGE_STATUS"] = "更改付款狀態";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_PAYMENT_MENU_DELETE"] = "刪除";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_PAYMENT_MENU_SEND_LINK"] = "再次發送鏈接";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_PAYMENT_MENU_TERMINAL_OPEN_PAYMENT_PAY"] = "通過終端獲得付款";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_PAYMENT_NOT_PAID"] = "付款等待中";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_PAYMENT_PAID"] = "有薪酬的";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_REALIZATION_DATE"] = "銷售## Account_number＃，＃日期＃";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_REALIZATION_DEDUCTED"] = "處理";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_REALIZATION_MENU_CANCEL_DEDUCT"] = "取消處理";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_REALIZATION_MENU_DEDUCT"] = "過程";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_REALIZATION_MENU_DELETE"] = "刪除";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_REALIZATION_NOT_DEDUCTED"] = "草稿";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_REALIZATION_SUM"] = "總和＃";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_REALIZATION_TITLE"] = "銷售訂單## Account_number＃";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_RESEND_LINK"] = "重發";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_SHIPMENT_DATE"] = "交付## account_number＃，＃日期＃";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_SHIPMENT_DETAIL_HINT_MSGVER_1"] = "交貨記錄只能在網絡版本中查看";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_SHIPMENT_DONE"] = "發表";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_SHIPMENT_MENU_CHANGE_STATUS"] = "更改交貨狀態";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_SHIPMENT_MENU_DELETE"] = "刪除";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_SHIPMENT_WAITING"] = "等待交貨";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_STAGE_CANCEL"] = "取消";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_STAGE_NOT_PAID"] = "未支付";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_STAGE_PAID"] = "有薪酬的";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_STAGE_REFUND"] = "退款";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_STAGE_SENT_NO_VIEWED"] = "發送";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_STAGE_VIEWED_NO_PAID"] = "查看";
$MESS["MOBILE_LAYOUT_UI_FIELDS_OPPORTUNITY_DOCUMENTS_TOTAL_SUM"] = "交易總計";
