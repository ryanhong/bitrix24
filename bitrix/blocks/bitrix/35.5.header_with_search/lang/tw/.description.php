<?php
$MESS["LANDING_BLOCK_35.5.HEADER_NAME_NEW"] = "帶有徽標，聯繫人和搜索功能的標題";
$MESS["LANDING_BLOCK_35.5.HEADER_NODES_LANDINGBLOCKNODELINK"] = "關聯";
$MESS["LANDING_BLOCK_35.5.HEADER_NODES_LANDINGBLOCKNODELOGO"] = "標識";
$MESS["LANDING_BLOCK_35.5.HEADER_NODES_LANDINGBLOCKNODETEXT"] = "文字";
$MESS["LANDING_BLOCK_35.5.HEADER_NODES_LANDINGBLOCKNODE_ICON"] = "圖示";
$MESS["LANDING_BLOCK_35.5.HEADER_NODES_LANDINGBLOCK_CARD"] = "接觸";
$MESS["LANDING_BLOCK_35.5.HEADER_NODES_LANDINGBLOCK_CARD_TITLE"] = "簽名";
$MESS["LANDING_BLOCK_35.5.HEADER_NODES_SEARCH_PAGE"] = "搜索結果頁面";
