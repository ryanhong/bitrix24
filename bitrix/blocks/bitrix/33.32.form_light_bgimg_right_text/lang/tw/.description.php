<?php
$MESS["LANDING_BLOCK_33.32.FORM_LIGHT_BGIMG_RIGHT_TEXT_NAME"] = "頁面上的形式在背景圖像上的左側";
$MESS["LANDING_BLOCK_33.32.FORM_LIGHT_BGIMG_RIGHT_TEXT_NODES_LANDINGBLOCKNODESUBTITLE"] = "字幕";
$MESS["LANDING_BLOCK_33.32.FORM_LIGHT_BGIMG_RIGHT_TEXT_NODES_LANDINGBLOCKNODETEXT"] = "文字";
$MESS["LANDING_BLOCK_33.32.FORM_LIGHT_BGIMG_RIGHT_TEXT_NODES_LANDINGBLOCKNODETITLE"] = "標題";
$MESS["LANDING_BLOCK_33.32.FORM_LIGHT_BGIMG_RIGHT_TEXT_NODES_LANDINGBLOCKNODE_BGIMG"] = "背景圖";
$MESS["LANDING_BLOCK_33.32.FORM_LIGHT_BGIMG_RIGHT_TEXT_NODES_LANDINGBLOCKNODE_BUTTON"] = "按鈕";
$MESS["LANDING_BLOCK_33.32.FORM_LIGHT_BGIMG_RIGHT_TEXT_NODES_LANDINGBLOCKNODE_FORM"] = "形式";
$MESS["LANDING_BLOCK_33_32_FORM_LIGHT_BGIMG_RIGHT_TEXT_NODES_BLOCK"] = "堵塞";
