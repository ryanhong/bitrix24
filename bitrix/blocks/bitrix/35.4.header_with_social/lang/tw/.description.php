<?php
$MESS["LANDING_BLOCK_35.4.HEADER_NAME"] = "帶有徽標，聯繫人和社交媒體鏈接的標題";
$MESS["LANDING_BLOCK_35.4.HEADER_NODES_LANDINGBLOCKNODELOGO"] = "標識";
$MESS["LANDING_BLOCK_35.4.HEADER_NODES_LANDINGBLOCKNODETEXT"] = "文字";
$MESS["LANDING_BLOCK_35.4.HEADER_NODES_LANDINGBLOCKNODE_CARD_LINK"] = "關聯";
$MESS["LANDING_BLOCK_35.4.HEADER_NODES_LANDINGBLOCKNODE_ICON"] = "圖示";
$MESS["LANDING_BLOCK_35.4.HEADER_NODES_LANDINGBLOCKNODE_SOCIAL_ICON"] = "圖示";
$MESS["LANDING_BLOCK_35.4.HEADER_NODES_LANDINGBLOCKNODE_SOCIAL_LINK"] = "關聯";
$MESS["LANDING_BLOCK_35.4.HEADER_NODES_LANDINGBLOCK_CARD"] = "接觸";
$MESS["LANDING_BLOCK_35.4.HEADER_NODES_LANDINGBLOCK_CARD_SOCIAL"] = "社交按鈕";
$MESS["LANDING_BLOCK_35.4.HEADER_NODES_LANDINGBLOCK_CARD_TITLE"] = "標題";
$MESS["LNDNGBLCK354_CARD_LABEL_1"] = "聯繫人";
$MESS["LNDNGBLCK354_CARD_LABEL_2"] = "社交按鈕";
