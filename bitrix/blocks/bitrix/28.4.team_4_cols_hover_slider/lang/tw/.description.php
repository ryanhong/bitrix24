<?php
$MESS["LANDING_BLOCK_28_4_TEAM_4_COLS--MEMBER"] = "員工";
$MESS["LANDING_BLOCK_28_4_TEAM_4_COLS--MEMBER_HOVER"] = "員工（懸停）";
$MESS["LANDING_BLOCK_28_4_TEAM_4_COLS--MEMBER_NAME"] = "姓名";
$MESS["LANDING_BLOCK_28_4_TEAM_4_COLS--MEMBER_PHOTO"] = "照片";
$MESS["LANDING_BLOCK_28_4_TEAM_4_COLS--MEMBER_POST"] = "職稱";
$MESS["LANDING_BLOCK_28_4_TEAM_4_COLS--MEMBER_TEXT"] = "文字";
$MESS["LANDING_BLOCK_28_4_TEAM_4_COLS--NAME"] = "翻轉員工的照片，其中包含有關懸停信息的信息";
$MESS["LANDING_BLOCK_28_4_TEAM_4_COLS_SLIDER"] = "滑桿";
