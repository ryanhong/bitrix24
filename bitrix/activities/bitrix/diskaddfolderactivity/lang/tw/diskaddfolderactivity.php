<?php
$MESS["BPDAF_ACCESS_DENIED"] = "除了門戶管理員以外，訪問拒絕了任何人。";
$MESS["BPDAF_ADD_FOLDER_ERROR"] = "無法創建一個新文件夾。";
$MESS["BPDAF_EMPTY_ENTITY_ID"] = "未指定存儲。";
$MESS["BPDAF_EMPTY_ENTITY_TYPE"] = "存儲類型缺少。";
$MESS["BPDAF_EMPTY_FOLDER_NAME"] = "未指定文件夾名稱。";
$MESS["BPDAF_TARGET_ERROR"] = "無法解析新文件夾位置。";
