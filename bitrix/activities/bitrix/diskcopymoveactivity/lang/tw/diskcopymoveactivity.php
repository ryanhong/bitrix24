<?php
$MESS["BPDCM_ACCESS_DENIED"] = "除了門戶管理員以外，訪問拒絕了任何人。";
$MESS["BPDCM_ADD_FOLDER_ERROR"] = "無法創建一個新文件夾。";
$MESS["BPDCM_EMPTY_ENTITY_ID"] = "未指定存儲。";
$MESS["BPDCM_EMPTY_ENTITY_TYPE"] = "存儲類型缺少。";
$MESS["BPDCM_EMPTY_SOURCE_ID"] = "沒有對像要複製或移動指定。";
$MESS["BPDCM_OPERATION_ERROR"] = "錯誤執行操作。";
$MESS["BPDCM_SOURCE_ERROR"] = "無法解析源對象。";
$MESS["BPDCM_TARGET_ERROR"] = "無法解析新文件夾位置。";
