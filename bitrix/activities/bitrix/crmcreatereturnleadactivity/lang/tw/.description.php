<?php
$MESS["CRM_CRL_DESC"] = "使用當前實體的參數創建重複引線";
$MESS["CRM_CRL_DESC_1"] = "創建標記為\“重複鉛\”的引線，並通過相同的詳細信息將其鏈接到客戶。";
$MESS["CRM_CRL_NAME"] = "創建重複引線";
$MESS["CRM_CRL_RETURN_LEAD_ID"] = "線索ID";
