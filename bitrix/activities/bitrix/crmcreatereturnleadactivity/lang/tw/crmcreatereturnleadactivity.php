<?php
$MESS["CRM_CRL_CREATED_LEAD"] = "創建的線索";
$MESS["CRM_CRL_DATA_NOT_EXISTS"] = "無法獲取項目數據";
$MESS["CRM_CRL_DEAL_NOT_EXISTS"] = "無法獲得交易數據";
$MESS["CRM_CRL_LEAD_TITLE"] = "線索名稱";
$MESS["CRM_CRL_LEAD_TITLE_DEFAULT"] = "無標題";
$MESS["CRM_CRL_NO_CLIENTS"] = "找不到與重複線索結合的聯繫";
$MESS["CRM_CRL_RESPONSIBLE"] = "負責人";
