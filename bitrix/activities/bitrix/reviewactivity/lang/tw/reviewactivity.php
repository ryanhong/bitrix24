<?php
$MESS["BPAA_ACT_APPROVERS_NONE"] = "沒有任何";
$MESS["BPAA_ACT_COMMENT_ERROR"] = "需要“＃ryment_label＃”字段。";
$MESS["BPAA_ACT_NO_ACTION"] = "選擇了無效的動作。";
$MESS["BPAR_ACT_BUTTON2"] = "完畢";
$MESS["BPAR_ACT_COMMENT"] = "評論";
$MESS["BPAR_ACT_INFO"] = "完成的＃％＃％（＃評論＃的＃總＃）";
$MESS["BPAR_ACT_PROP_EMPTY1"] = "未指定“用戶”屬性。";
$MESS["BPAR_ACT_PROP_EMPTY4"] = "缺少“名稱”屬性。";
$MESS["BPAR_ACT_REVIEWED"] = "讀取元素已完成。";
$MESS["BPAR_ACT_REVIEW_TRACK"] = "用戶＃人＃已閱讀元素＃評論＃";
$MESS["BPAR_ACT_TRACK3"] = "用戶讀取元素：＃val＃";
