<?php
$MESS["SEQWF_BEG"] = "開始";
$MESS["SEQWF_CONF_1"] = "選定的活動將從您的活動列表中刪除。繼續？";
$MESS["SEQWF_END"] = "結尾";
$MESS["SEQWF_MARKETPLACE_ADD"] = "添加更多...";
$MESS["SEQWF_SNIP_1"] = "我的活動";
$MESS["SEQWF_SNIP_DD_1"] = "放在這裡的活動以節省";
$MESS["SEQWF_SNIP_DEL_1"] = "刪除此活動";
$MESS["SEQWF_SNIP_NAME"] = "姓名：";
$MESS["SEQWF_SNIP_TITLE_1"] = "活動屬性";
