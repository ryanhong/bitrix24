<?php
$MESS["CRM_WAIT_ACTIVITY_DAY_PLURAL_0"] = "天";
$MESS["CRM_WAIT_ACTIVITY_DAY_PLURAL_1"] = "天";
$MESS["CRM_WAIT_ACTIVITY_DAY_PLURAL_2"] = "天";
$MESS["CRM_WAIT_ACTIVITY_DESCRIPTION_TYPE_AFTER"] = "等待＃持續時間＃或客戶的任何輸入";
$MESS["CRM_WAIT_ACTIVITY_DESCRIPTION_TYPE_BEFORE"] = "等待＃持續時間＃直到\“＃target_date＃\”或客戶的任何輸入";
$MESS["CRM_WAIT_ACTIVITY_ERROR_WAIT_DURATION"] = "未指定超時";
$MESS["CRM_WAIT_ACTIVITY_ERROR_WAIT_TARGET"] = "尚未指定等待日期";
$MESS["CRM_WAIT_ACTIVITY_ERROR_WAIT_TYPE"] = "未指定等待類型";
$MESS["CRM_WAIT_ACTIVITY_EXECUTE_ERROR_WAIT_BEFORE"] = "等待結束日期需要超過當前日期。您輸入的日期可能是過去或將來太遠。";
$MESS["CRM_WAIT_ACTIVITY_EXECUTE_ERROR_WAIT_TARGET_1"] = "找不到目標元素字段";
$MESS["CRM_WAIT_ACTIVITY_EXECUTE_ERROR_WAIT_TARGET_TIME_1"] = "無法從元素字段獲得日期值";
$MESS["CRM_WAIT_ACTIVITY_WAIT_AFTER"] = "等待指定的天數";
$MESS["CRM_WAIT_ACTIVITY_WAIT_BEFORE"] = "等待指定的天數直到選擇一天";
$MESS["CRM_WAIT_ACTIVITY_WAIT_DESCRIPTION"] = "評論或說明";
$MESS["CRM_WAIT_ACTIVITY_WAIT_DURATION"] = "超時間隔，天數";
$MESS["CRM_WAIT_ACTIVITY_WAIT_TARGET"] = "直到指定日期";
$MESS["CRM_WAIT_ACTIVITY_WAIT_TYPE"] = "等待類型";
$MESS["CRM_WAIT_ACTIVITY_WEEK_PLURAL_0"] = "星期";
$MESS["CRM_WAIT_ACTIVITY_WEEK_PLURAL_1"] = "幾週";
$MESS["CRM_WAIT_ACTIVITY_WEEK_PLURAL_2"] = "幾週";
