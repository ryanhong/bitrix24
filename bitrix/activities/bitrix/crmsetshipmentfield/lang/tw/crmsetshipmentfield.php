<?php
$MESS["CRM_SSF_DELIVERY_ID_NAME"] = "僅用於貨物";
$MESS["CRM_SSF_ORDER_ERROR"] = "當前實體不是\“ order \”類型實體";
$MESS["CRM_SSF_ORDER_NOT_FOUND"] = "無法獲取訂單數據";
$MESS["CRM_SSF_TARGET_AD_NAME"] = "設置\“批准\”狀態";
$MESS["CRM_SSF_TARGET_DD_NAME"] = "設置\“已發貨\”狀態";
$MESS["CRM_SSF_TARGET_STATUS_NAME"] = "設置狀態";
