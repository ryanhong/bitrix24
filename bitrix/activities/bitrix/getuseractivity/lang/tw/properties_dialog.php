<?php
$MESS["BPCRU_PD_MAX_LEVEL"] = "主管級別（越高）";
$MESS["BPCRU_PD_MAX_LEVEL_1"] = "1（直接主管）";
$MESS["BPCRU_PD_NO"] = "不";
$MESS["BPCRU_PD_SKIP_ABSENT"] = "如果缺席，升級到更高的水平";
$MESS["BPCRU_PD_SKIP_RESERVE"] = "申請備份用戶";
$MESS["BPCRU_PD_SKIP_TIMEMAN"] = "跳過檢查員工";
$MESS["BPCRU_PD_TYPE"] = "類型";
$MESS["BPCRU_PD_TYPE_BOSS"] = "導師";
$MESS["BPCRU_PD_TYPE_ORDER"] = "順序";
$MESS["BPCRU_PD_TYPE_RANDOM"] = "任何";
$MESS["BPCRU_PD_USER2"] = "備份用戶";
$MESS["BPCRU_PD_USER_BOSS"] = "用於用戶";
$MESS["BPCRU_PD_USER_RANDOM"] = "來自用戶";
$MESS["BPCRU_PD_YES"] = "是的";
