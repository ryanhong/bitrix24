<?php
$MESS["BPDD_ACCESS_DENIED"] = "除了門戶管理員以外，訪問拒絕了任何人。";
$MESS["BPDD_EMPTY_SOURCE_ID"] = "源文件未指定。";
$MESS["BPDD_SOURCE_ID_ERROR"] = "找不到源對象。";
