<?php
$MESS["CRM_CRLC_DESC"] = "建立聯繫並將鉛標記為重複";
$MESS["CRM_CRLC_DESC_1"] = "創建潛在客戶的聯繫人以供將來參考。";
$MESS["CRM_CRLC_NAME"] = "創建鉛的聯繫";
$MESS["CRM_CRLC_RETURN_CONTACT_ID"] = "聯繫人ID";
