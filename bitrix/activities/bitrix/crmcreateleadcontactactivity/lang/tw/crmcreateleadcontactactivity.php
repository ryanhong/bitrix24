<?php
$MESS["CRM_CRLC_CONTACT_NAME_DEFAULT"] = "無標題";
$MESS["CRM_CRLC_LEAD_HAS_CONTACT"] = "鉛已經屬於聯繫";
$MESS["CRM_CRLC_LEAD_NOT_EXISTS"] = "無法獲得潛在客戶數據";
$MESS["CRM_CRLC_LEAD_WRONG_STATUS_MSGVER_1"] = "當鉛處於成功階段時，只能使用轉換器自動化規則將其轉換為觸點。";
$MESS["CRM_CRLC_RESPONSIBLE"] = "負責人";
