<?php
$MESS["CRM_CDA_CHANGE_RESPONSIBLE"] = "負責人";
$MESS["CRM_CDA_CHANGE_STAGE"] = "初期";
$MESS["CRM_CDA_CYCLING_ERROR"] = "無法複製智能流程自動化，因為可能有無限的循環";
$MESS["CRM_CDA_EMPTY_PROP"] = "所需的參數為空：＃屬性＃";
$MESS["CRM_CDA_ITEM_TITLE"] = "水療名稱";
$MESS["CRM_CDA_MOVE_TO_CATEGORY"] = "移至管道";
$MESS["CRM_CDA_NEW_ITEM_TITLE"] = "＃source_title＃（複製）";
$MESS["CRM_CDA_NO_SOURCE_FIELDS"] = "無法獲得源水療中心的數據";
$MESS["CRM_CDA_STAGE_EXISTENCE_ERROR"] = "選定的階段不存在";
$MESS["CRM_CDA_STAGE_SELECTION_ERROR"] = "選定的初始階段屬於其他管道";
