<?php
$MESS["BPTA2_DESCR_CLOSEDBY"] = "任務已關閉";
$MESS["BPTA2_DESCR_CLOSEDDATE"] = "任務關閉";
$MESS["BPTA2_DESCR_DESCR"] = "添加一個新任務";
$MESS["BPTA2_DESCR_DESCR_1"] = "使用預填充字段為用戶創建並分配任務。";
$MESS["BPTA2_DESCR_IS_DELETED"] = "任務已刪除";
$MESS["BPTA2_DESCR_NAME"] = "任務";
$MESS["BPTA2_DESCR_NAME_1"] = "創建任務";
$MESS["BPTA2_DESCR_TASKID"] = "任務ID";
