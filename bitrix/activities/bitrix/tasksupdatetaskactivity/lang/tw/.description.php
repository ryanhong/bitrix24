<?php
$MESS["TASKS_UTA_CATEGORY"] = "任務";
$MESS["TASKS_UTA_DESC"] = "編輯任務";
$MESS["TASKS_UTA_DESC_1"] = "修改指定的任務或向其添加指定的數據。";
$MESS["TASKS_UTA_ERROR_MESSAGE"] = "更新失敗的錯誤文本";
$MESS["TASKS_UTA_NAME"] = "編輯任務";
$MESS["TASKS_UTA_NAME_1"] = "修改任務";
