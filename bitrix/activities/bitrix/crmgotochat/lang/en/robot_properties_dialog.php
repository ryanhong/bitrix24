<?php
$MESS["BP_CRM_GO_TO_CHAT_HELP"] = "The bot will select from the list the first available Telegram-enabled Open Channel, and start a new conversation.";
$MESS["BP_CRM_GO_TO_CHAT_HELP_LINK"] = "How to Connect Telegram to Open Channel";
