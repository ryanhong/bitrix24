<?php
$MESS["BP_CRM_GO_TO_CHAT_DESCRIPTION"] = "Laden Sie Kunden in Telegram ein, um die Konversation fortzusetzen. Der Kunde wird einen Telegram-Link zur Konversation bekommen. Die komplette Kommunikations-History wird hier gespeichert.";
$MESS["BP_CRM_GO_TO_CHAT_NAME"] = "Kunden in Messenger einladen";
