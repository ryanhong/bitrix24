<?php
$MESS["CRM_BP_OPAY_DESC"] = "執行訂單的實際付款交易";
$MESS["CRM_BP_OPAY_DESC_1"] = "將狀態更改為項目表格。";
$MESS["CRM_BP_OPAY_DESC_2"] = "根據客戶的喜好從客戶的支付卡中藉記資金。與經常付款一起使用。";
$MESS["CRM_BP_OPAY_NAME"] = "流程付款";
$MESS["CRM_BP_OPAY_NAME_1"] = "處理自動付款";
