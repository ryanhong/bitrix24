<?php
$MESS["BPCLDA_CREATED_ELEMENT_ID"] = "創建的項目";
$MESS["BPCLDA_DOC_TYPE_1"] = "實體類型";
$MESS["BPCLDA_DT_LISTS"] = "列表";
$MESS["BPCLDA_DT_LISTS_SOCNET_1"] = "工作組和項目列表";
$MESS["BPCLDA_DT_PROCESSES"] = "工作流程";
$MESS["BPCLDA_ERROR_DT_1"] = "不正確的實體類型。";
$MESS["BPCLDA_FIELD_REQUIED"] = "需要字段'＃字段＃'。";
$MESS["BPCLDA_WRONG_TYPE"] = "“＃param＃”參數類型不確定。";
