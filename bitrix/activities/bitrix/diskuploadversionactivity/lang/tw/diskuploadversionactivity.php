<?php
$MESS["BPDUV_ACCESS_DENIED"] = "除了門戶管理員以外，訪問拒絕了任何人。";
$MESS["BPDUV_EMPTY_SOURCE_FILE"] = "未選擇上傳文件。";
$MESS["BPDUV_EMPTY_SOURCE_ID"] = "源文件未指定。";
$MESS["BPDUV_SOURCE_FILE_ERROR"] = "錯誤獲取文件上傳。";
$MESS["BPDUV_SOURCE_ID_ERROR"] = "找不到源文件。";
$MESS["BPDUV_UPLOAD_ERROR"] = "錯誤上傳新版本。";
