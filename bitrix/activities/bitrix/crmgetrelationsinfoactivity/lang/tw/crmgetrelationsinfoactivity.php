<?php
$MESS["CRM_GRI_BINDING"] = "鏈接到";
$MESS["CRM_GRI_ENTITY_EXISTENCE_ERROR"] = "沒有＃element_type＃實體鏈接到此項目";
$MESS["CRM_GRI_PARENT_TYPE_ERROR"] = "選擇不正確的實體類型";
$MESS["CRM_GRI_PARENT_TYPE_EXISTENCE_ERROR"] = "您選擇的類型在工作流程中沒有視圖";
$MESS["CRM_GRI_RELATION_EXISTENCE_ERROR"] = "您選擇的鏈接不存在";
