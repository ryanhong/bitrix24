<?php
$MESS["BPMA_PD_BODY"] = "消息文字";
$MESS["BPMA_PD_CP"] = "編碼";
$MESS["BPMA_PD_DIRRECT_MAIL"] = "消息發送模式";
$MESS["BPMA_PD_DIRRECT_MAIL_N"] = "使用郵件子系統發送（參與的電子郵件事件和模板）";
$MESS["BPMA_PD_DIRRECT_MAIL_Y"] = "直接發送";
$MESS["BPMA_PD_FILE"] = "附件";
$MESS["BPMA_PD_FILE_SELECT"] = "選擇";
$MESS["BPMA_PD_FROM"] = "發件人";
$MESS["BPMA_PD_FROM_LIMITATION"] = "不允許在或連接到Bitrix24的電子郵件地址。";
$MESS["BPMA_PD_MAIL_SEPARATOR"] = "電子郵件地址分離器";
$MESS["BPMA_PD_MAIL_SITE"] = "消息模板網站";
$MESS["BPMA_PD_MAIL_SITE_OTHER"] = "其他";
$MESS["BPMA_PD_MESS_TYPE"] = "消息類型";
$MESS["BPMA_PD_SUBJECT"] = "主題";
$MESS["BPMA_PD_TEXT"] = "文字";
$MESS["BPMA_PD_TO"] = "接受者";
$MESS["BPMA_PD_TO_LIMITATION"] = "消息將最多發送到10個收件人";
