<?php
$MESS["BPMA_ATTACHMENT"] = "附件";
$MESS["BPMA_ATTACHMENT_DISK"] = "驅動文件";
$MESS["BPMA_ATTACHMENT_FILE"] = "文件";
$MESS["BPMA_ATTACHMENT_TYPE"] = "附件類型";
$MESS["BPMA_EMPTY_PROP1"] = "缺少“發件人”參數。";
$MESS["BPMA_EMPTY_PROP2"] = "缺少“收件人”參數。";
$MESS["BPMA_EMPTY_PROP3"] = "缺少“主題”參數。";
$MESS["BPMA_EMPTY_PROP4"] = "缺少“編碼”參數。";
$MESS["BPMA_EMPTY_PROP5"] = "缺少“消息類型”參數。";
$MESS["BPMA_EMPTY_PROP6"] = "“消息類型”參數不正確。";
$MESS["BPMA_EMPTY_PROP7"] = "缺少“消息文本”參數。";
$MESS["BPMA_MAIL_SUBJECT"] = "主題";
$MESS["BPMA_MAIL_TEXT"] = "消息文字";
$MESS["BPMA_MAIL_USER_FROM"] = "從";
$MESS["BPMA_MAIL_USER_TO"] = "到";
