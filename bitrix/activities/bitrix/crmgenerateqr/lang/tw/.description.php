<?php
$MESS["CRMBPGQR_DESCR_DESCR"] = "為訪問者跟踪器頁面創建QR碼";
$MESS["CRMBPGQR_DESCR_DESCR_1_MSGVER_1"] = "為任何營銷目的創建個性化的QR代碼（助手）。";
$MESS["CRMBPGQR_DESCR_NAME"] = "創建QR碼";
$MESS["CRMBPGQR_RETURN_PAGE_LINK"] = "短頁鏈接";
$MESS["CRMBPGQR_RETURN_PAGE_LINK_BB"] = "頁面鏈接（bbcode）";
$MESS["CRMBPGQR_RETURN_PAGE_LINK_HTML"] = "電子郵件的頁面鏈接（HTML）";
$MESS["CRMBPGQR_RETURN_QR_IMG"] = "添加QR碼（HTML圖像）";
$MESS["CRMBPGQR_RETURN_QR_LINK"] = "簡短的QR碼鏈接";
$MESS["CRMBPGQR_RETURN_QR_LINK_BB"] = "QR碼鏈接（BBCODE）";
$MESS["CRMBPGQR_RETURN_QR_LINK_HTML"] = "電子郵件的QR碼鏈接（HTML）";
