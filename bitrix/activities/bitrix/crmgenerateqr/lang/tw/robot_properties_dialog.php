<?php
$MESS["CRMBPGQR_ADDITIONAL"] = "其他設置";
$MESS["CRMBPGQR_HELP_1"] = "設置一個自動化規則以創建個人QR碼以跟踪活動參與者。";
$MESS["CRMBPGQR_HELP_2"] = "查看QR代碼鏈接將打開的頁面。";
$MESS["CRMBPGQR_HELP_3"] = "使用您的手機掃描QR碼，以確保鏈接有效：";
$MESS["CRMBPGQR_HELP_4"] = "如果需要，請在“自動化規則”參數中自定義頁面文本。";
