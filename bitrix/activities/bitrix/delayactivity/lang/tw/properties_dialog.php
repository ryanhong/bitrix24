<?php
$MESS["CPAD_DP_TIME"] = "暫停時間";
$MESS["CPAD_DP_TIME1"] = "日期";
$MESS["CPAD_DP_TIME_D"] = "天";
$MESS["CPAD_DP_TIME_H"] = "小時";
$MESS["CPAD_DP_TIME_LOCAL"] = "當地時間";
$MESS["CPAD_DP_TIME_M"] = "分鐘";
$MESS["CPAD_DP_TIME_S"] = "秒";
$MESS["CPAD_DP_TIME_SELECT"] = "模式";
$MESS["CPAD_DP_TIME_SELECT_DELAY"] = "時期";
$MESS["CPAD_DP_TIME_SELECT_TIME"] = "時間";
$MESS["CPAD_DP_TIME_SERVER"] = "服務器時間";
$MESS["CPAD_DP_WRITE_TO_LOG"] = "將暫停信息保存到工作流日誌";
$MESS["CPAD_PD_TIMEOUT_LIMIT"] = "最小等待時間";
