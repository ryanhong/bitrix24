<?php
$MESS["BPIMNA_PD_FROM"] = "接受者";
$MESS["BPIMNA_PD_MESSAGE"] = "通知文本";
$MESS["BPIMNA_PD_MESSAGE_BBCODE"] = "BBCode已啟用";
$MESS["BPIMNA_PD_MESSAGE_OUT"] = "電子郵件/jabber的通知文本";
$MESS["BPIMNA_PD_MESSAGE_OUT_EMPTY"] = "如果為空，則將使用\“通知文本\”。";
$MESS["BPIMNA_PD_NOTIFY_TYPE"] = "通知類型";
$MESS["BPIMNA_PD_NOTIFY_TYPE_FROM"] = "個性化通知（帶有頭像）";
$MESS["BPIMNA_PD_NOTIFY_TYPE_SYSTEM"] = "系統通知（無頭像）";
$MESS["BPIMNA_PD_TO"] = "發件人";
