<?php
$MESS["CRM_CDA_DESC"] = "創建智能過程自動化";
$MESS["CRM_CDA_DESC_1"] = "創建CRM項目";
$MESS["CRM_CDA_DESC_2"] = "創建一個CRM實體並填寫其中的指定字段。";
$MESS["CRM_CDA_NAME"] = "創建水療中心";
$MESS["CRM_CDA_NAME_1"] = "創建CRM項目";
$MESS["CRM_CDA_RETURN_ERROR_MESSAGE"] = "創建失敗的錯誤文本";
$MESS["CRM_CDA_RETURN_ITEM_ID"] = "新的水療項目ID";
$MESS["CRM_CDA_RETURN_ITEM_ID_1"] = "新的CRM項目ID";
$MESS["CRM_CDA_ROBOT_DESCRIPTION_DIGITAL_WORKPLACE"] = "創建一個新的水療項目，並填寫其中所需的字段。";
