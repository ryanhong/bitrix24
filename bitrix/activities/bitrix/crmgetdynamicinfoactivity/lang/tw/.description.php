<?php
$MESS["CRM_GDIA_DESC"] = "讀取指定的水療項目的數據";
$MESS["CRM_GDIA_DESC_1"] = "讀取指定的CRM項目的數據";
$MESS["CRM_GDIA_DESC_2_MSGVER_1"] = "將項目信息發送到其他自動化規則（助手）。";
$MESS["CRM_GDIA_NAME"] = "閱讀水療項目";
$MESS["CRM_GDIA_NAME_1"] = "閱讀CRM項目";
$MESS["CRM_GDIA_NAME_2"] = "獲取CRM項目信息";
$MESS["CRM_GDIA_ROBOT_DESCRIPTION_DIGITAL_WORKPLACE_MSGVER_1"] = "將水療項目信息發送到其他自動化規則（助手）。";
$MESS["CRM_GDIA_ROBOT_NAME_DIGITAL_WORKPLACE"] = "獲取溫泉項目信息";
