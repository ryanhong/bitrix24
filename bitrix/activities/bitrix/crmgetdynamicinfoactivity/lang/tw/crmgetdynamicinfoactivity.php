<?php
$MESS["CRM_GDIA_DYNAMIC_TYPE_ID"] = "水療項目類型";
$MESS["CRM_GDIA_ENTITY_EXISTENCE_ERROR"] = "您選擇的過濾器未產生搜索結果";
$MESS["CRM_GDIA_ENTITY_TYPE_ERROR"] = "選擇不正確的實體類型";
$MESS["CRM_GDIA_FILTERING_FIELDS_COLLAPSED_TEXT"] = "存在過濾器，單擊“展開”查看。";
$MESS["CRM_GDIA_FILTERING_FIELDS_PROPERTY"] = "現場過濾器";
$MESS["CRM_GDIA_RETURN_FIELDS_SELECTION"] = "選擇字段";
$MESS["CRM_GDIA_TYPE_ID"] = "CRM項目類型";
