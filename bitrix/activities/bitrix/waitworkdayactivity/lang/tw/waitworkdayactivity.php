<?php
$MESS["BPWWD_DEBUG_EVENT"] = "由於測試模式事件而暫停跳過";
$MESS["BPWWD_ERROR_EMPTY_USER"] = "無法找到目標員工。";
$MESS["BPWWD_PROP_ERROR_EMPTY_USER"] = "目標員工未指定。";
$MESS["BPWWD_PROP_TARGET_USER"] = "員工";
$MESS["BPWWD_SUBSCRIBED"] = "暫停直到員工時鐘或恢復工作日：＃用戶＃";
$MESS["BPWWD_SUBSCRIBE_SKIPPED"] = "暫停跳過。員工已經計時了。";
