<?php
$MESS["CRM_BP_GPI_DESC"] = "獲取訂單付款詳細信息";
$MESS["CRM_BP_GPI_DESC_1"] = "將付款狀態信息發送到其他自動化規則（助手）。";
$MESS["CRM_BP_GPI_NAME"] = "支付信息";
$MESS["CRM_BP_GPI_NAME_1"] = "獲取付款信息";
$MESS["CRM_BP_GPI_RETURN_PAYMENT_ACCOUNT_NUMBER"] = "支付 ＃";
$MESS["CRM_BP_GPI_RETURN_PAYMENT_ID"] = "付款ID";
