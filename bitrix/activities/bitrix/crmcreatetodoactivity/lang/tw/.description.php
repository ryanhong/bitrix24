<?php
$MESS["CRM_BP_CREATE_TODO_DESC"] = "創建活動：會議，電話，電子郵件或其他任何內容。主動的CRM將幫助您與客戶保持聯繫，並最終使交易取得成功。";
$MESS["CRM_BP_CREATE_TODO_ID"] = "創建的活動ID";
$MESS["CRM_BP_CREATE_TODO_NAME"] = "計劃活動";
