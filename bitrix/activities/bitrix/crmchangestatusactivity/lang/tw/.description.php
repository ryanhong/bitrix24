<?php
$MESS["CRM_CHANGE_DEAL_STAGE_NAME"] = "變更階段";
$MESS["CRM_CHANGE_STATUS_DESC_1"] = "更改元素狀態並完成工作流程。";
$MESS["CRM_CHANGE_STATUS_DESC_2"] = "將項目移至指定階段。";
$MESS["CRM_CHANGE_STATUS_NAME"] = "更改狀態";
$MESS["CRM_CHANGE_STATUS_NAME_1"] = "變更階段";
