<?php
$MESS["CRM_SODD_DESCR_DESCR"] = "船舶訂單（標記為所有貨物的訂單）";
$MESS["CRM_SODD_DESCR_DESCR_1"] = "在收到客戶的付款或其他措施後，將訂單標記為已發貨的訂單。";
$MESS["CRM_SODD_DESCR_NAME"] = "船舶訂單";
