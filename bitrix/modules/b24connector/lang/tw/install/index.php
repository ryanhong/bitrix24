<?php
$MESS["B24C_INSTALL_TITLE"] = "Bitrix24連接器模塊安裝";
$MESS["B24C_MODULE_DESCRIPTION"] = "該模塊在Bitrix站點管理器中包含BitRix24功能。";
$MESS["B24C_MODULE_NAME"] = "BitRix24連接器";
$MESS["B24C_RIGHT_DENIED"] = "拒絕訪問";
$MESS["B24C_RIGHT_FULL"] = "完全訪問";
$MESS["B24C_RIGHT_READ"] = "讀";
$MESS["B24C_UNINSTALL_TITLE"] = "BitRix24連接器模塊卸載";
