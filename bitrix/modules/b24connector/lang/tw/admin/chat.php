<?php
$MESS["B24C_CHAT_ACCESS_DENIED"] = "拒絕訪問";
$MESS["B24C_CHAT_BUTT_GET_B24"] = "獲取Bitrix24實時聊天（免費）";
$MESS["B24C_CHAT_BUTT_SETT"] = "配置實時聊天";
$MESS["B24C_CHAT_LI1"] = "實時快速回复您的客戶。";
$MESS["B24C_CHAT_LI2"] = "銷售代表看到了客戶的點擊路徑，這有助於說出客戶想要聽到的聲音。";
$MESS["B24C_CHAT_LI3"] = "在CRM中註冊您的對話並介紹您的客戶。";
$MESS["B24C_CHAT_P1"] = "通過實時聊天從客戶那裡獲得更多反饋。此工具是幫助客戶在網站上導航或選擇所需的產品或幫助他們下訂單的理想選擇。";
$MESS["B24C_CHAT_P2"] = "通過簡單地使用網站上的實時聊天，提高客戶的信心水平。";
$MESS["B24C_CHAT_TITLE"] = "在線聊天";
