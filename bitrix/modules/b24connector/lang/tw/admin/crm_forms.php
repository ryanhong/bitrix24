<?php
$MESS["B24C_CRMF_DESCR1"] = "CRM表格是一種必不可少的業務工具，它將有助於增加銷售，獲取更多客戶，更新當前的客戶資料並將研究結果保存到CRM。";
$MESS["B24C_CRMF_DESCR2"] = "<b>簡單表格</b>最常用於數據收集：註冊，反饋，簡歷和其他用戶數據提交表格。";
$MESS["B24C_CRMF_DESCR3"] = "<b>條件形式</b>是複雜的大型形式的本質。您在創建表單時指定的條件和規則將使用戶沿著取決於先前答案的問題的路徑。";
$MESS["B24C_CRMF_DESCR4"] = "<b>付款表格</b>：除了收集用戶數據外，這些表格還可以顯示用戶可以使用其選擇的付款方式選擇和購買的產品或服務的選擇。";
$MESS["B24C_CRMF_DESCR5"] = "將CRM表單放在您的網站上（將其插入網頁或添加小部件），然後將客戶信息收集到您的CRM。";
$MESS["B24C_CRMF_GET_FORMS"] = "Open Bitrix24 CRM表單";
$MESS["B24C_CRMF_TITLE"] = "CRM形式";
$MESS["B24C_CRMF_TYPES"] = "CRM形式有三種類型：";
