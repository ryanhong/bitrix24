<?php
$MESS["B24C_BL_AC_C"] = "實時聊天，回調，CRM表格 - 與客戶保持聯繫所需的任何東西。";
$MESS["B24C_BL_AC_C2"] = "小部件將改善整體網站轉換並增加銷售額。";
$MESS["B24C_BL_AC_C3"] = "只需單擊即可將小部件安裝到您的網站。";
$MESS["B24C_BL_AC_C4"] = "該網站現在有一個按鈕。";
$MESS["B24C_BL_AC_C5"] = "訪問者將單擊按鈕，然後選擇首選的通信方式：實時聊天，電話或網絡表格。";
$MESS["B24C_BL_ALL_C"] = "一個小部件中的所有溝通方式。";
$MESS["B24C_BL_OL_U"] = "這將在您的Bitrix24中統一所有可能的數字客戶通信渠道。";
$MESS["B24C_BUTT_EMPTY"] = "查看Bitrix24小部件";
$MESS["B24C_BUTT_TITLE"] = "小部件";
