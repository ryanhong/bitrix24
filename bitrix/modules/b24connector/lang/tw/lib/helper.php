<?php
$MESS["B24C_HLP_BUTTONS"] = "小部件";
$MESS["B24C_HLP_CHAT"] = "在線聊天";
$MESS["B24C_HLP_CRM_FORM"] = "CRM形式";
$MESS["B24C_HLP_GM_TEXT"] = "客戶";
$MESS["B24C_HLP_GM_TITLE"] = "將門戶連接到Bitrix24";
$MESS["B24C_HLP_INTEGRATION"] = "客戶通信";
$MESS["B24C_HLP_OL"] = "開放頻道";
$MESS["B24C_HLP_RECALL"] = "打回來";
$MESS["B24C_HLP_TELEPHONY"] = "電話";
