<?php
$MESS["BR_BLOG_URL"] = "博客URL";
$MESS["BR_BLOG_VAR"] = "博客標識符變量";
$MESS["BR_MODE"] = "導出到RSS";
$MESS["BR_MODE_COMMENT"] = "評論";
$MESS["BR_MODE_POST"] = "帖子";
$MESS["BR_NUM_POSTS"] = "發出的帖子";
$MESS["BR_PAGE_VAR"] = "頁面變量";
$MESS["BR_PATH_TO_BLOG"] = "博客頁面路徑模板";
$MESS["BR_PATH_TO_POST"] = "博客消息頁的模板";
$MESS["BR_PATH_TO_USER"] = "博客用戶頁面路徑的模板";
$MESS["BR_POST_ID"] = "博客文章ID";
$MESS["BR_POST_VAR"] = "博客消息標識符變量";
$MESS["BR_TYPE"] = "RSS格式";
$MESS["BR_USER_VAR"] = "博客用戶標識符變量";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
