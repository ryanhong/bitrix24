<?php
$MESS["BB_PATH_TO_SMILE"] = "相對於站點根的路徑，帶有笑臉的路徑";
$MESS["BC_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["BC_SEO_USE"] = "使用單獨的字段進行SEO Aware說明";
$MESS["BH_PATH_TO_DRAFT"] = "草稿消息文件夾路徑的模板";
$MESS["BPC_ALLOW_POST_CODE"] = "將消息符號代碼用作ID";
$MESS["BPC_EDITOR_CODE_DEFAULT"] = "默認為純文本編輯器模式";
$MESS["BPC_EDITOR_DEFAULT_HEIGHT"] = "Visual Editor（PIX）的默認高度";
$MESS["BPC_EDITOR_RESIZABLE"] = "可分解的視覺編輯器";
$MESS["BPC_IMAGE_MAX_HEIGHT"] = "最大限度。圖像高度";
$MESS["BPC_IMAGE_MAX_WIDTH"] = "最大限度。圖像寬度";
$MESS["BPC_SMILES_COLS"] = "笑臉列計數";
$MESS["BPC_SMILES_COUNT"] = "可見的笑臉數量";
$MESS["BPE_ALLOW_POST_MOVE"] = "在博客之間啟用移動帖子";
$MESS["BPE_BLOG_URL"] = "博客URL";
$MESS["BPE_BLOG_VAR"] = "博客標識符變量";
$MESS["BPE_ID"] = "消息ID";
$MESS["BPE_PAGE_VAR"] = "頁面變量";
$MESS["BPE_PATH_TO_BLOG"] = "博客頁面路徑模板";
$MESS["BPE_PATH_TO_BLOG_BLOG"] = "博客中的博客之路";
$MESS["BPE_PATH_TO_BLOG_DRAFT"] = "博客草稿頁面";
$MESS["BPE_PATH_TO_BLOG_POST"] = "博客文章頁面";
$MESS["BPE_PATH_TO_BLOG_POST_EDIT"] = "博客文章編輯頁面";
$MESS["BPE_PATH_TO_GROUP_BLOG"] = "社交網絡組博客頁面";
$MESS["BPE_PATH_TO_GROUP_DRAFT"] = "社交網絡組博客草稿頁面";
$MESS["BPE_PATH_TO_GROUP_POST"] = "社交網絡組博客文章頁面";
$MESS["BPE_PATH_TO_GROUP_POST_EDIT"] = "社交網絡組博客文章編輯頁面";
$MESS["BPE_PATH_TO_POST"] = "博客消息頁面路徑的模板";
$MESS["BPE_PATH_TO_POST_EDIT"] = "博客消息編輯頁面路徑的模板";
$MESS["BPE_PATH_TO_USER"] = "博客用戶頁面路徑的模板";
$MESS["BPE_PATH_TO_USER_BLOG"] = "社交網絡用戶博客頁面";
$MESS["BPE_PATH_TO_USER_DRAFT"] = "社交網絡用戶博客草稿頁面";
$MESS["BPE_PATH_TO_USER_POST"] = "社交網絡用戶博客文章頁面";
$MESS["BPE_PATH_TO_USER_POST_EDIT"] = "社交網絡用戶博客文章編輯頁面";
$MESS["BPE_POST_VAR"] = "博客消息標識符變量";
$MESS["BPE_USER_VAR"] = "博客用戶標識符變量";
$MESS["BPE_USE_GOOGLE_CODE"] = "使用外部翻譯引擎進行消息";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
$MESS["POST_PROPERTY"] = "顯示其他帖子屬性";
