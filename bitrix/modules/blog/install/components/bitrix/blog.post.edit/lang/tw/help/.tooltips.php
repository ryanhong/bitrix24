<?php
$MESS["BLOG_URL_TIP"] = "指定通過博客ID傳遞的代碼。";
$MESS["BLOG_VAR_TIP"] = "在此處指定通過博客ID傳遞的變量的名稱。";
$MESS["ID_TIP"] = "指定將傳遞用戶ID的代碼。";
$MESS["PAGE_VAR_TIP"] = "在此處指定博客頁面通過的變量的名稱。";
$MESS["PATH_TO_BLOG_TIP"] = "主要博客頁面的路徑。示例：blog_blog.php？page = blog＆blog =＃blog＃。";
$MESS["PATH_TO_DRAFT_TIP"] = "通往草稿職位頁面的路徑。";
$MESS["PATH_TO_POST_EDIT_TIP"] = "博客文章編輯頁面的路徑。示例：blog_p_edit.php？page = post_edit＆blog =＃blog＃＆post_id =＃post_id＃。";
$MESS["PATH_TO_POST_TIP"] = "博客文章詳細信息頁面的路徑。示例：<Nobr> blog_blog.php？page = post＆blog =＃blog＃＆post_id =＃post_id＃。</nobr>";
$MESS["PATH_TO_SMILE_TIP"] = "通往文件夾的路徑，包括笑臉。";
$MESS["PATH_TO_USER_TIP"] = "用戶配置文件頁面的路徑。示例：<Nobr> blog_user.php？page = user＆user_id =＃user_id＃。</nobr>";
$MESS["POST_PROPERTY_TIP"] = "在此處選擇將顯示在“郵局”頁面上的其他發布屬性。";
$MESS["POST_VAR_TIP"] = "在此處指定博客文章ID的變量的名稱。";
$MESS["SET_TITLE_TIP"] = "如果該選項處於活動狀態，則頁面標題將設置為<nobr> <b>編輯帖子</b> <i> \“博客名稱\” </i>。</i>。</nobr>";
$MESS["USER_VAR_TIP"] = "在此處指定通過博客用戶ID傳遞的變量的名稱。";
