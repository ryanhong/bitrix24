<?php
$MESS["BLG_GRP_NAME"] = "小組博客";
$MESS["BLG_NAME"] = "用戶博客";
$MESS["BLOG_EMPTY_TITLE_PLACEHOLDER"] = "圖像";
$MESS["BLOG_ERR_NO_RIGHTS"] = "錯誤：寫博客的權限不足";
$MESS["BLOG_MODULE_NOT_INSTALL"] = "沒有安裝博客模塊。";
$MESS["BLOG_NEW_MESSAGE"] = "新博客文章";
$MESS["BLOG_POST_EDIT"] = "編輯博客文章";
$MESS["BLOG_P_INSERT"] = "點擊插入圖像";
$MESS["BLOG_SONET_MODULE_NOT_AVAIBLE"] = "該用戶不可用博客。";
$MESS["BM_BLOG_CHAIN_GROUP_MICRO"] = "微博";
$MESS["BM_BLOG_CHAIN_USER_MICRO"] = "微博";
$MESS["BPE_COPY_DELETE_ERROR"] = "嘗試刪除原始帖子的錯誤。<br />";
$MESS["BPE_COPY_ERROR"] = "錯誤複製帖子。<br />";
$MESS["BPE_COPY_NO_BLOG"] = "找不到目的地博客。";
$MESS["BPE_COPY_NO_PERM"] = "在目標博客中創建新帖子以復製或移動帖子的許可不足。";
$MESS["BPE_HIDDEN_POSTED"] = "您的帖子已成功添加。此博客中的帖子已預處理；一旦博客所有者批准它，您的帖子就會變得可見。";
$MESS["BPE_SESS"] = "您的會議已經過期。請重新發布您的消息。";
$MESS["B_B_MES_NO_BLOG"] = "沒有找到博客";
$MESS["B_B_MES_NO_GROUP"] = "找不到社交網絡組。";
$MESS["B_B_MES_NO_GROUP_ACTIVE"] = "此功能對於社交網絡組不可用。";
$MESS["B_B_MES_NO_GROUP_RIGHTS"] = "在社交網絡組中創建帖子的許可不足。";
$MESS["B_B_PC_DUPLICATE_POST"] = "您已經添加了您的帖子";
