<?php
$MESS["B_B_US_1_M_F"] = "更多字段";
$MESS["B_B_US_ACTIONS"] = "動作";
$MESS["B_B_US_ADD"] = "添加";
$MESS["B_B_US_AD_NEW_FR"] = "添加新朋友";
$MESS["B_B_US_AD_NEW_FR_BY"] = "通過暱稱，代碼或博客名稱添加新朋友";
$MESS["B_B_US_DELETE"] = "刪除";
$MESS["B_B_US_EDIT"] = "編輯";
$MESS["B_B_US_EDIT_FR_LIST"] = "好友列表";
$MESS["B_B_US_FR_ACTIONS"] = "動作";
$MESS["B_B_US_FR_GROUPS"] = "組";
$MESS["B_B_US_FR_VISITOR"] = "遊客";
$MESS["B_B_US_LIST_WANTED"] = "社區申請人名單";
$MESS["B_B_US_VISIT"] = "遊客";
