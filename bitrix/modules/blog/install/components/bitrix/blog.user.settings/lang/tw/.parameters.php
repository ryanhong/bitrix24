<?php
$MESS["BUS_BLOG_URL"] = "博客URL";
$MESS["BUS_BLOG_VAR"] = "博客標識符變量";
$MESS["BUS_PAGE_VAR"] = "頁面變量";
$MESS["BUS_PATH_TO_USER"] = "用戶頁面路徑模板";
$MESS["BUS_PATH_TO_USER_SETTINGS_EDIT"] = "用戶設置編輯頁面路徑的模板";
$MESS["BUS_USER_VAR"] = "用戶ID變量";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
