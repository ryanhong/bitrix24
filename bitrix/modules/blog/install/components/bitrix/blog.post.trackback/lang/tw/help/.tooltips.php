<?php
$MESS["BLOG_URL_TIP"] = "指定通過博客ID傳遞的代碼。";
$MESS["BLOG_VAR_TIP"] = "在此處指定通過博客ID傳遞的變量的名稱。";
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>否CAHCE </i>：沒有執行緩存。";
$MESS["ID_TIP"] = "指定通過博客文章ID傳遞的代碼。";
$MESS["PAGE_VAR_TIP"] = "在此處指定博客頁面通過的變量的名稱。";
$MESS["PATH_TO_TRACKBACK_TIP"] = "通往郵遞頁面的完整路徑。示例：<Nobr>/blog/blog_p_p_track_get.php?page=trackback&blog=#blog#&post_id=#post_id＃。</nobr>";
$MESS["POST_VAR_TIP"] = "在此處指定博客文章ID的變量的名稱。";
