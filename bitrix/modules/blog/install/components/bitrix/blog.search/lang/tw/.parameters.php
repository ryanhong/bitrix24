<?php
$MESS["BB_NAV_TEMPLATE"] = "分頁模板名稱";
$MESS["BC_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["BMS_BLOG_VAR"] = "博客標識符變量";
$MESS["BMS_COUNT"] = "每頁消息";
$MESS["BMS_PAGE_VAR"] = "頁面變量";
$MESS["BMS_PATH_TO_BLOG"] = "博客頁面路徑模板";
$MESS["BMS_PATH_TO_POST"] = "博客消息頁的模板";
$MESS["BMS_PATH_TO_USER"] = "博客用戶頁面路徑的模板";
$MESS["BMS_POST_VAR"] = "博客消息標識符變量";
$MESS["BMS_SEARCH_PAGE"] = "搜索頁面";
$MESS["BMS_USER_VAR"] = "博客用戶標識符變量";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
