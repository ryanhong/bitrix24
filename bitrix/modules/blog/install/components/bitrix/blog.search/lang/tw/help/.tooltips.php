<?php
$MESS["BLOG_VAR_TIP"] = "在此處指定通過博客ID傳遞的變量的名稱。";
$MESS["PAGE_RESULT_COUNT_TIP"] = "可以在一頁上顯示的搜索結果數。";
$MESS["PAGE_VAR_TIP"] = "在此處指定博客頁面通過的變量的名稱。";
$MESS["PATH_TO_BLOG_TIP"] = "主要博客頁面的路徑。示例：blog_blog.php？page = blog＆blog =＃blog＃。";
$MESS["PATH_TO_POST_TIP"] = "博客文章詳細信息頁面的路徑。示例：<Nobr> blog_post.php？page = post＆blog =＃blog＃＆post_id =＃post_id＃。</nobr>";
$MESS["PATH_TO_USER_TIP"] = "用戶配置文件頁面的路徑。示例：<Nobr> blog_user.php？page = user＆user_id =＃user_id＃。</nobr>";
$MESS["POST_VAR_TIP"] = "在此處指定博客文章ID的變量的名稱。";
$MESS["SEARCH_PAGE_TIP"] = "搜索頁面的路徑。示例：blog_search.php。";
$MESS["SET_TITLE_TIP"] = "如果該選項處於活動狀態，則頁面標題將設置為<b> Blog Search </b>。";
$MESS["USER_VAR_TIP"] = "在此處指定通過博客用戶ID傳遞的變量的名稱。";
