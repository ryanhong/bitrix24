<?php
$MESS["BLOG_COUNT_TIP"] = "可以在頁面上顯示的最大博客數量。";
$MESS["BLOG_VAR_TIP"] = "在此處指定通過博客ID傳遞的變量的名稱。";
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>否CAHCE </i>：沒有執行緩存。";
$MESS["PAGE_VAR_TIP"] = "在此處指定博客頁面通過的變量的名稱。";
$MESS["PATH_TO_BLOG_TIP"] = "主要博客頁面的路徑。示例：blog_blog.php？page = blog＆blog =＃blog＃。";
$MESS["PATH_TO_USER_TIP"] = "用戶配置文件頁面的路徑。示例：<Nobr> blog_user.php？page = user＆user_id =＃user_id＃。</nobr>";
$MESS["SHOW_DESCRIPTION_TIP"] = "如果要顯示博客描述，請選擇<i>是</i>。";
$MESS["SORT_BY1_TIP"] = "指定首先對博客進行排序的字段。";
$MESS["SORT_BY2_TIP"] = "指定博客將在第二次通過的字段中進行分類。";
$MESS["SORT_ORDER1_TIP"] = "指定第一個分類通過的排序順序。";
$MESS["SORT_ORDER2_TIP"] = "為第二分類通過指定博客分類順序。";
$MESS["USER_VAR_TIP"] = "在此處指定通過博客用戶ID傳遞的變量的名稱。";
