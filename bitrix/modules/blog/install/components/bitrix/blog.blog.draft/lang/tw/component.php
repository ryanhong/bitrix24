<?php
$MESS["BLOG_BLOG_BLOG_MES_SHOWED"] = "該帖子已發表。";
$MESS["BLOG_BLOG_BLOG_MES_SHOW_ERROR"] = "錯誤發布帖子。";
$MESS["BLOG_BLOG_BLOG_MES_SHOW_NO_RIGHTS"] = "您無權發布帖子。";
$MESS["BLOG_BLOG_BLOG_NO_BLOG"] = "無法找到博客";
$MESS["BLOG_MODULE_NOT_INSTALL"] = "沒有安裝博客模塊。";
$MESS["BLOG_SONET_MODULE_NOT_AVAIBLE"] = "該用戶不可用博客。";
$MESS["B_B_DRAFT_M_DEL"] = "帖子已成功刪除";
$MESS["B_B_DRAFT_M_DEL_ERR"] = "錯誤刪除帖子";
$MESS["B_B_DRAFT_M_DEL_RIGHTS"] = "您沒有足夠的權限來刪除此消息";
$MESS["B_B_DRAFT_NO_R_CR"] = "您沒有足夠的權限來在此博客中創建消息";
$MESS["B_B_DRAFT_TITLE"] = "草稿職位";
$MESS["B_B_DRAFT_TITLE_BLOG"] = "草稿職位";
