<?php
$MESS["BBD_BLOG_URL"] = "博客URL";
$MESS["BBD_BLOG_VAR"] = "博客標識符變量";
$MESS["BBD_PAGE_VAR"] = "頁面變量";
$MESS["BBD_PATH_TO_BLOG_CATEGORY"] = "帶有類別過濾器的博客頁面模板";
$MESS["BBD_PATH_TO_POST_EDIT"] = "博客消息編輯頁面模板";
$MESS["BBD_PATH_TO_SMILE"] = "相對於站點根的路徑，帶有笑臉的路徑";
$MESS["BBD_POST_VAR"] = "博客消息標識符變量";
$MESS["BBD_SET_NAV_CHAIN"] = "將項目添加到導航鏈中";
$MESS["BC_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["BPC_IMAGE_MAX_HEIGHT"] = "最大限度。圖像高度";
$MESS["BPC_IMAGE_MAX_WIDTH"] = "最大限度。圖像寬度";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
