<?php
$MESS["BLOG_URL_TIP"] = "指定通過博客ID傳遞的代碼。";
$MESS["BLOG_VAR_TIP"] = "在此處指定通過博客ID傳遞的變量的名稱。";
$MESS["ID_TIP"] = "指定通過博客文章ID傳遞的代碼。";
$MESS["PAGE_VAR_TIP"] = "在此處指定博客頁面通過的變量的名稱。";
$MESS["PATH_TO_POST_TIP"] = "博客文章詳細信息頁面的路徑。示例：<Nobr> blog_post.php？page = post＆blog =＃blog＃＆post_id =＃post_id＃。</nobr>";
$MESS["POST_VAR_TIP"] = "在此處指定博客文章ID的變量的名稱。";
