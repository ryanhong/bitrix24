<?php
$MESS["NAME_TEMPLATE_TIP"] = "可能的宏：＃名稱＃ - 名字; ＃last_name＃ - 姓氏，＃second_name＃ - 中間名; ＃name_short＃，＃last_name_short＃，＃second_name_short＃ - 相應名稱的縮寫。";
$MESS["USER_PROPERTY_NAME_TIP"] = "包含額外用戶字段的標籤的名稱";
