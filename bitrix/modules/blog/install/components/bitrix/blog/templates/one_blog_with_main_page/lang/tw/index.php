<?php
$MESS["BC_COMMENTED_POSTS"] = "大多數評論";
$MESS["BC_GROUPS"] = "博客小組";
$MESS["BC_MESSAGES"] = "消息";
$MESS["BC_NEW_BLOGS"] = "新博客";
$MESS["BC_NEW_COMMENTS"] = "新評論";
$MESS["BC_NEW_POSTS"] = "新博客文章";
$MESS["BC_POPULAR_BLOGS"] = "受歡迎的博客";
$MESS["BC_POPULAR_POSTS"] = "受歡迎的";
$MESS["BC_SEARCH_TAG"] = "標籤雲";
$MESS["BLOG_BLOG_FAVORITE"] = "喜歡的消息";
$MESS["BLOG_CREATE_BLOG"] = "創建您的博客";
$MESS["BLOG_TITLE"] = "部落格";
