<?php
$MESS["BC_NAME_TEMPLATE"] = "名稱格式";
$MESS["BC_NAME_TEMPLATE_DEFAULT"] = "＃NOBR ##名稱＃#last_name ##/nobr＃";
$MESS["BC_PATH_TO_CONPANY_DEPARTMENT"] = "部門頁面路徑模板";
$MESS["BC_PATH_TO_MESSAGES_CHAT"] = "用戶使者路徑模板";
$MESS["BC_PATH_TO_SONET_USER_PROFILE"] = "社交網絡用戶資料路徑模板";
$MESS["BC_PATH_TO_VIDEO_CALL"] = "視頻通話頁面";
$MESS["BC_SHARE_HIDE"] = "默認隱藏社交網絡書籤欄";
$MESS["BC_SHARE_SHORTEN_URL_KEY"] = "bit.ly鍵";
$MESS["BC_SHARE_SHORTEN_URL_LOGIN"] = "bit.ly登錄";
$MESS["BC_SHARE_SYSTEM"] = "使用社交網絡和書籤";
$MESS["BC_SHARE_TEMPLATE"] = "社交網絡書籤模板";
$MESS["BC_SHOW_LOGIN"] = "顯示登錄名，如果沒有必需的用戶名字段";
$MESS["BC_USE_SHARE"] = "顯示社交網絡書籤欄";
$MESS["ONE_BLOG_BLOG_URL"] = "符號博客名";
$MESS["USER_PROPERTY_NAME"] = "附加屬性選項卡的名稱";
