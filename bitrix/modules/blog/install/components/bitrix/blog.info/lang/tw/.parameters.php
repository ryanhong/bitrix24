<?php
$MESS["BI_BLOG_URL"] = "博客URL";
$MESS["BI_BLOG_VAR"] = "博客標識符變量";
$MESS["BI_CATEGORY_ID"] = "通過標籤ID過濾";
$MESS["BI_PAGE_VAR"] = "頁面變量";
$MESS["BI_PATH_TO_BLOG"] = "博客頁面路徑模板";
$MESS["BI_PATH_TO_BLOG_CATEGORY"] = "帶有標籤過濾器的博客頁面路徑模板";
$MESS["BI_PATH_TO_USER"] = "博客用戶頁面路徑的模板";
$MESS["BI_USER_VAR"] = "博客用戶標識符變量";
$MESS["BLOG_PROPERTY_LIST"] = "在博客信息中顯示其他博客屬性";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
