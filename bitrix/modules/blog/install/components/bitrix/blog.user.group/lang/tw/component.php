<?php
$MESS["BLOG_ERR_NO_BLOG"] = "錯誤！沒有找到博客";
$MESS["BLOG_ERR_NO_RIGHTS"] = "錯誤！在博客中寫信的權限不足";
$MESS["BLOG_GROUP_EXIST_1"] = "團體";
$MESS["BLOG_GROUP_EXIST_2"] = "已經存在。";
$MESS["BLOG_MODULE_NOT_INSTALL"] = "沒有安裝博客模塊。";
$MESS["BLOG_USER_GROUP_TITLE"] = "博客用戶組";
