<?php
$MESS["BRL_ATOM"] = "顯示<nobr> atom .3 </nobr>鏈接";
$MESS["BRL_BLOG_URL"] = "博客URL";
$MESS["BRL_BLOG_VAR"] = "博客標識符變量";
$MESS["BRL_GROUP_ID"] = "博客組ID";
$MESS["BRL_GROUP_VAR"] = "組ID變量別名";
$MESS["BRL_MODE"] = "顯示RSS的鏈接";
$MESS["BRL_PAGE_VAR"] = "頁面變量";
$MESS["BRL_PATH_TO_RSS"] = "RSS頁面路徑模板";
$MESS["BRL_PATH_TO_RSS_ALL"] = "所有博客的RSS的頁面路徑模板";
$MESS["BRL_POST_ID"] = "博客文章ID";
$MESS["BRL_P_B"] = "用於博客";
$MESS["BRL_P_C"] = "評論要發布";
$MESS["BRL_P_G"] = "對於小組中的所有博客";
$MESS["BRL_P_S"] = "對於所有網站博客";
$MESS["BRL_RSS1"] = "顯示<nobr> rss .92 </nobr>鏈接";
$MESS["BRL_RSS2"] = "顯示<nobr> RSS 2.0 </nobr>鏈接";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
