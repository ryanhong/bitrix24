<?php
$MESS["BLOG_MODULE_NOT_INSTALL"] = "沒有安裝博客模塊。";
$MESS["BRL_B"] = "以格式顯示博客文章";
$MESS["BRL_C"] = "以格式向當前帖子顯示評論";
$MESS["BRL_G"] = "以當前組的格式顯示所有博客的帖子";
$MESS["BRL_S"] = "以格式顯示所有博客的帖子";
