<?php
$MESS["BLOG_URL_TIP"] = "指定通過博客ID傳遞的代碼。";
$MESS["BLOG_VAR_TIP"] = "在此處指定通過博客ID傳遞的變量的名稱。";
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>否CAHCE </i>：沒有執行緩存。";
$MESS["ID_TIP"] = "指定通過博客文章ID傳遞的代碼。";
$MESS["PAGE_VAR_TIP"] = "在此處指定博客頁面通過的變量的名稱。";
$MESS["PATH_TO_BLOG_CATEGORY_TIP"] = "帶有標籤過濾器的博客頁面的路徑。示例：<Nobr> blog_filter.php？page = blog＆blog =＃blog＃<b>＆category =＃類別＃</b>。</nobr>";
$MESS["PATH_TO_BLOG_TIP"] = "主要博客頁面的路徑。示例：blog_blog.php？page = blog＆blog =＃blog＃。";
$MESS["PATH_TO_POST_EDIT_TIP"] = "博客文章編輯頁面的路徑。示例：blog_p_edit.php？page = post_edit＆blog =＃blog＃＆post_id =＃post_id＃。";
$MESS["PATH_TO_SMILE_TIP"] = "通往文件夾的路徑，包括笑臉。";
$MESS["PATH_TO_USER_TIP"] = "用戶配置文件頁面的路徑。示例：<Nobr> blog_user.php？page = user＆user_id =＃user_id＃。</nobr>";
$MESS["POST_PROPERTY_TIP"] = "在此處選擇將顯示在“郵局”頁面上的其他發布屬性。";
$MESS["POST_VAR_TIP"] = "在此處指定博客文章ID的變量的名稱。";
$MESS["SET_NAV_CHAIN_TIP"] = "如果已檢查，則將添加到導航鏈中的父博客的名稱。";
$MESS["SET_TITLE_TIP"] = "如果該選項處於活動狀態，則頁面標題將設置為帖子標題。";
$MESS["USER_VAR_TIP"] = "在此處指定通過博客用戶ID傳遞的變量的名稱。";
