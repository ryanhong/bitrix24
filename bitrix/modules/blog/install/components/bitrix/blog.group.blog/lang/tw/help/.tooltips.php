<?php
$MESS["BLOG_COUNT_TIP"] = "可以在頁面上顯示的最大博客數量。其他博客將通過BreadCrumb導航鏈接提供。";
$MESS["BLOG_VAR_TIP"] = "在此處指定通過博客ID傳遞的變量的名稱。";
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>否CAHCE </i>：沒有執行緩存。";
$MESS["ID_TIP"] = "指定通過博客組ID的代碼。";
$MESS["PAGE_VAR_TIP"] = "在此處指定博客頁面通過的變量的名稱。";
$MESS["PATH_TO_BLOG_TIP"] = "主要博客頁面的路徑。示例：blog_blog.php？page = blog＆blog =＃blog＃。";
$MESS["PATH_TO_POST_TIP"] = "博客文章詳細信息頁面的路徑。示例：<Nobr> blog_post.php？page = post＆blog =＃blog＃＆post_id =＃post_id＃。</nobr>";
$MESS["PATH_TO_USER_TIP"] = "用戶配置文件頁面的路徑。示例：<Nobr> blog_user.php？page = user＆user_id =＃user_id＃。</nobr>";
$MESS["POST_VAR_TIP"] = "在此處指定博客文章ID的變量的名稱。";
$MESS["SET_TITLE_TIP"] = "如果該選項處於活動狀態，則頁面標題將設置為博客組名稱。";
$MESS["SHOW_BLOG_WITHOUT_POSTS_TIP"] = "如果該選項處於活動狀態，則將顯示空的博客（無消息）。";
$MESS["USER_VAR_TIP"] = "在此處指定通過博客用戶ID傳遞的變量的名稱。";
