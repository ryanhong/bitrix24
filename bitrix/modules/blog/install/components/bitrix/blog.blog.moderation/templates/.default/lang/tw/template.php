<?php
$MESS["BLOG_BLOG_BLOG_CATEGORY"] = "標籤：";
$MESS["BLOG_BLOG_BLOG_COMMENTS"] = "評論：";
$MESS["BLOG_BLOG_BLOG_VIEWS"] = "視圖：";
$MESS["BLOG_MES_DELETE"] = "刪除";
$MESS["BLOG_MES_DELETE_POST_CONFIRM"] = "您確定要刪除這篇文章嗎？";
$MESS["BLOG_MES_EDIT"] = "編輯";
$MESS["BLOG_MES_SHOW"] = "發布";
$MESS["BLOG_MES_SHOW_POST_CONFIRM"] = "您確定要解開這篇文章嗎？";
$MESS["BLOG_PHOTO"] = "相片）：";
$MESS["B_B_MODERATION_NO_MES"] = "此博客不包含需要審核的帖子。";
