<?php
$MESS["BLG_SONET_TITLE"] = "在博客中添加了帖子\“＃標題＃\”";
$MESS["BLOG_BLOG_BLOG_MES_SHOWED"] = "該帖子已發表。";
$MESS["BLOG_BLOG_BLOG_MES_SHOW_ERROR"] = "錯誤發布帖子。";
$MESS["BLOG_BLOG_BLOG_MES_SHOW_NO_RIGHTS"] = "您無權發布帖子。";
$MESS["BLOG_BLOG_BLOG_NO_BLOG"] = "無法找到博客";
$MESS["BLOG_MODULE_NOT_INSTALL"] = "沒有安裝博客模塊。";
$MESS["BLOG_SONET_MODULE_NOT_AVAIBLE"] = "該用戶不可用博客。";
$MESS["B_B_HIDE_M_DEL"] = "該帖子已成功刪除。";
$MESS["B_B_HIDE_M_DEL_ERR"] = "錯誤刪除帖子。";
$MESS["B_B_HIDE_M_DEL_RIGHTS"] = "您無權刪除此帖子。";
$MESS["B_B_HIDE_NO_R_CR"] = "您無權此博客中的帖子。";
$MESS["B_B_HIDE_TITLE"] = "適度";
$MESS["B_B_HIDE_TITLE_BLOG"] = "適度";
