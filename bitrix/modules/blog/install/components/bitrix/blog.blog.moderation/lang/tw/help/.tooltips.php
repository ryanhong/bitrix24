<?php
$MESS["BLOG_URL_TIP"] = "指定通過博客ID傳遞的代碼。";
$MESS["BLOG_VAR_TIP"] = "在此處指定通過博客ID傳遞的變量的名稱。";
$MESS["PAGE_VAR_TIP"] = "在此處指定博客頁面通過的變量的名稱。";
$MESS["PATH_TO_BLOG_CATEGORY_TIP"] = "帶有標籤過濾器的博客頁面的路徑。示例：blog_filter.php？page = blog＆blog =＃blog＃<b>＆category =＃類別＃</b>。";
$MESS["PATH_TO_POST_EDIT_TIP"] = "博客文章編輯頁面的路徑。示例：<Nobr> blog_p_edit.php？page = post_edit＆blog =＃blog＃＆post_id =＃post_id＃。</nobr>";
$MESS["PATH_TO_SMILE_TIP"] = "包含笑臉的文件夾的路徑。";
$MESS["POST_VAR_TIP"] = "在此處指定博客文章ID的變量的名稱。";
$MESS["SET_NAV_CHAIN_TIP"] = "啟用此選項將此組件生成的頁面添加到導航鏈中。";
$MESS["SET_TITLE_TIP"] = "如果該選項處於活動狀態，則頁面標題將設置為<nobr> <b> </b> <i> \“博客名稱\” </i>。</i>。";
