<?php
$MESS["BLOG_PROPERTY_TIP"] = "在此處選擇將在博客中顯示的其他屬性。";
$MESS["BLOG_URL_TIP"] = "指定通過博客ID傳遞的代碼。";
$MESS["BLOG_VAR_TIP"] = "在此處指定通過博客ID傳遞的變量的名稱。";
$MESS["PAGE_VAR_TIP"] = "在此處指定博客頁面通過的變量的名稱。";
$MESS["PATH_TO_BLOG_EDIT_TIP"] = "博客編輯頁面的路徑。示例：<Nobr> blog_b_edit.php？page = blog_edit＆blog =＃blog＃</nobr>。";
$MESS["PATH_TO_BLOG_TIP"] = "主要博客頁面的路徑。示例：<Nobr> blog_blog.php？page = blog＆blog =＃blog＃</nobr>。";
$MESS["SET_TITLE_TIP"] = "如果該選項處於活動狀態，則頁面標題將設置為<nobr> <b>編輯博客</b> <i> \“博客名稱\” </i>。";
