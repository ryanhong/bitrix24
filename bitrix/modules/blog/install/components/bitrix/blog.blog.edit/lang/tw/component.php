<?php
$MESS["BLOG_ERR_NO_RIGHTS"] = "錯誤：寫博客的權限不足";
$MESS["BLOG_ERR_SAVE"] = "保存博客參數錯誤";
$MESS["BLOG_FRIENDS"] = "朋友們";
$MESS["BLOG_GROUP_EXIST"] = "具有名稱＃group_name＃已經存在的用戶組已經存在";
$MESS["BLOG_MODULE_NOT_INSTALL"] = "沒有安裝博客模塊。";
$MESS["BLOG_NEW_BLOG"] = "新博客";
$MESS["BLOG_NOT_RIGHTS_TO_CREATE"] = "您沒有足夠的權限來創建博客。";
$MESS["BLOG_TOP_TITLE"] = "博客“＃博客＃”";
