<?php
$MESS["BLOG_MODULE_NOT_INSTALL"] = "沒有安裝博客模塊。";
$MESS["B_B_PU_NO_RIGHTS"] = "沒有足夠的權限來修改個人設置";
$MESS["B_B_USER_NO_USER"] = "找不到用戶。";
$MESS["B_B_USER_SEX_F"] = "女性";
$MESS["B_B_USER_SEX_M"] = "男性";
$MESS["B_B_USER_TITLE"] = "用戶資料";
$MESS["B_B_USER_TITLE_VIEW"] = "用戶資料";
