<?php
$MESS["BLOG_BLOG_TO_YOU_DESC"] = "＃blog_id＃ - 博客ID
＃blog_name＃ - 博客名稱
＃blog_url＃ - 博客名稱，僅拉丁字母
＃blog_adr＃ - 博客地址
＃USER_ID＃ - 用戶ID
＃用戶＃-用戶
＃USER_URL＃ - 用戶URL
＃email_from＃ - 發送者電子郵件
＃email_to＃ - 收件人電子郵件";
$MESS["BLOG_BLOG_TO_YOU_MESSAGE"] = "來自＃site_name＃的消息
-------------------------------------------------- --------

博客\“＃blog_name＃\”已添加到您的朋友中。

博客地址：＃blog_adr＃

您的個人資料：＃USER_URL＃

此消息已自動生成。
";
$MESS["BLOG_BLOG_TO_YOU_NAME"] = "博客已添加到您的朋友";
$MESS["BLOG_BLOG_TO_YOU_SUBJECT"] = "＃site_name＃：[b]博客\“＃blog_name＃\”已添加到您的朋友中。";
$MESS["BLOG_POST_BROADCAST_DESC"] = "
＃message_title＃ - 發布主題
＃message_text＃ - 發短信
＃Message_path＃ - 發布URL
＃作者＃ - 發表作者
＃email_to＃ - 電子郵件收件人";
$MESS["BLOG_POST_BROADCAST_MESSAGE"] = "＃site_name＃上的新活動
-------------------------------------------------- --------

自您上次訪問以來，已經添加了新帖子。

主題：
＃Message_title＃

作者：＃作者＃

帖子文字：
＃message_text＃

郵政地址：
＃Message_path＃

此消息已自動生成。";
$MESS["BLOG_POST_BROADCAST_NAME"] = "添加了新帖子";
$MESS["BLOG_POST_BROADCAST_SUBJECT"] = "＃site_name＃：＃message_title＃";
$MESS["BLOG_SONET_NEW_COMMENT_DESC"] = "＃email_to＃ - 收件人電子郵件
＃comment_id＃ - 評論ID
＃POST_ID＃ - 發布ID
＃url_id＃ - 發布URL";
$MESS["BLOG_SONET_NEW_COMMENT_NAME"] = "評論添加了";
$MESS["BLOG_SONET_NEW_POST_DESC"] = "＃email_to＃ - 收件人電子郵件
＃POST_ID＃ - 發布ID
＃url_id＃ - 發布URL";
$MESS["BLOG_SONET_NEW_POST_NAME"] = "添加了新帖子";
$MESS["BLOG_SONET_POST_SHARE_DESC"] = "＃email_to＃ - 收件人電子郵件
＃POST_ID＃ - 發布ID
＃url_id＃ - 發布URL";
$MESS["BLOG_SONET_POST_SHARE_NAME"] = "添加了新收件人";
$MESS["BLOG_USER_TO_YOUR_BLOG_DESC"] = "＃blog_id＃ - 博客ID
＃blog_name＃ - 博客名稱
＃blog_url＃ - 博客名稱，僅拉丁字母
＃blog_adr＃ - 博客地址
＃USER_ID＃ - 用戶ID
＃用戶＃-用戶
＃USER_URL＃ - 用戶URL
＃email_from＃ - 發送者電子郵件
＃email_to＃ - 收件人電子郵件";
$MESS["BLOG_USER_TO_YOUR_BLOG_MESSAGE"] = "來自＃site_name＃的消息
-------------------------------------------------- --------

朋友＃用戶＃已添加到您的博客\“＃Blog_name＃\”中。

用戶資料：＃USER_URL＃

您的博客頁面：＃Blog_adr＃

此消息已自動生成。
";
$MESS["BLOG_USER_TO_YOUR_BLOG_NAME"] = "朋友已添加到您的博客中";
$MESS["BLOG_USER_TO_YOUR_BLOG_SUBJECT"] = "＃site_name＃：[b]一個朋友＃用戶＃已添加到您的博客\“＃blog_name＃\”。";
$MESS["BLOG_YOUR_BLOG_TO_USER_DESC"] = "＃blog_id＃ - 博客ID
＃blog_name＃ - 博客名稱
＃blog_url＃ - 博客名稱，僅拉丁字母
＃blog_adr＃ - 博客地址
＃USER_ID＃ - 用戶ID
＃用戶＃-用戶
＃USER_URL＃ - 用戶URL
＃email_from＃ - 發送者電子郵件
＃email_to＃ - 收件人電子郵件";
$MESS["BLOG_YOUR_BLOG_TO_USER_MESSAGE"] = "來自＃site_name＃的消息
-------------------------------------------------- --------

您的博客\“＃blog_name＃\”已添加為＃用戶＃的朋友。

用戶資料：＃USER_URL＃

您的博客網址：＃Blog_adr＃

此消息已自動生成。
";
$MESS["BLOG_YOUR_BLOG_TO_USER_NAME"] = "您的博客已添加到朋友";
$MESS["BLOG_YOUR_BLOG_TO_USER_SUBJECT"] = "＃site_name＃：[b]您的博客\“＃blog_name＃\”已添加為＃用戶＃的朋友。";
$MESS["BLOG_YOU_TO_BLOG_DESC"] = "＃blog_id＃ - 博客ID
＃blog_name＃ - 博客名稱
＃blog_url＃ - 博客名稱，僅拉丁字母
＃blog_adr＃ - 博客地址
＃USER_ID＃ - 用戶ID
＃用戶＃-用戶
＃USER_URL＃ - 用戶URL
＃email_from＃ - 發送者電子郵件
＃email_to＃ - 收件人電子郵件";
$MESS["BLOG_YOU_TO_BLOG_MESSAGE"] = "來自＃site_name＃的消息
-------------------------------------------------- --------

您已被添加為朋友\ \“＃blog_name＃\”。
博客URL：＃Blog_adr＃

您的個人資料：＃USER_URL＃

此消息已自動生成。
";
$MESS["BLOG_YOU_TO_BLOG_NAME"] = "您已被添加到博客朋友";
$MESS["BLOG_YOU_TO_BLOG_SUBJECT"] = "＃site_name＃：[b]作為朋友，您已被添加到\“＃blog_name＃\”中。";
$MESS["NEW_BLOG_COMMENT2COMMENT_DESC"] = "＃blog_id＃ - 博客ID
＃blog_name＃ - 博客標題
＃blog_url＃ - 博客URL
＃message_title＃ - 消息標題
＃ryment_title＃ - 評論標題
＃ryment_text＃ - 評論文本
＃comment_date＃ - 評論日期
＃comment_path＃ - 註釋URL
＃作者＃ - 評論作者
＃email_from＃ - 發送者電子郵件
＃email_to＃ - 收件人電子郵件";
$MESS["NEW_BLOG_COMMENT2COMMENT_MESSAGE"] = "來自網站的信息消息＃site_name＃
-------------------------------------------------- --------

您在博客中評論的新評論\“＃blog_name＃\” to post \“＃message_title＃\”。

主題：
＃comment_title＃

作者：＃作者＃
日期：＃comment_date＃＃

評論：
＃ryment_text＃

URL地址：
＃comment_path＃

自動生成的消息。";
$MESS["NEW_BLOG_COMMENT2COMMENT_NAME"] = "您在博客中的新評論";
$MESS["NEW_BLOG_COMMENT2COMMENT_SUBJECT"] = "＃site_name＃：[b]＃message_title＃：＃comment_title＃＃";
$MESS["NEW_BLOG_COMMENT2COMMENT_WITHOUT_TITLE_DESC"] = "＃blog_id＃ - 博客ID
＃blog_name＃ - 博客標題
＃blog_url＃ - 博客URL
＃message_title＃ - 消息標題
＃ryment_text＃ - 評論文本
＃comment_date＃ - 評論日期
＃comment_path＃ - 註釋URL
＃作者＃ - 評論作者
＃email_from＃ - 發送者電子郵件
＃email_to＃ - 收件人電子郵件";
$MESS["NEW_BLOG_COMMENT2COMMENT_WITHOUT_TITLE_MESSAGE"] = "來自網站的信息消息＃site_name＃
-------------------------------------------------- --------

您在博客中評論的新評論\“＃blog_name＃\” to post \“＃message_title＃\”。

作者：＃作者＃
日期：＃comment_date＃＃

評論：
＃ryment_text＃

URL地址：
＃comment_path＃

自動生成的消息。";
$MESS["NEW_BLOG_COMMENT2COMMENT_WITHOUT_TITLE_NAME"] = "您在博客中的新評論（無主題）";
$MESS["NEW_BLOG_COMMENT2COMMENT_WITHOUT_TITLE_SUBJECT"] = "＃site_name＃：[b]＃message_title＃";
$MESS["NEW_BLOG_COMMENT_DESC"] = "＃blog_id＃ - 博客ID
＃blog_name＃ - 博客標題
＃blog_url＃ - 博客URL
＃message_title＃ - 消息標題
＃ryment_title＃ - 評論標題
＃ryment_text＃ - 評論文本
＃comment_date＃ - 評論日期
＃comment_path＃ - 註釋URL
＃作者＃ - 評論作者
＃email_from＃ - 發送者電子郵件
＃email_to＃ - 收件人電子郵件";
$MESS["NEW_BLOG_COMMENT_MESSAGE"] = "來自網站的信息消息＃site_name＃
-------------------------------------------------- --------

博客中的新註釋\“＃blog_name＃\” to post \“＃message_title＃\”。

主題：
＃comment_title＃

作者：＃作者＃
日期：＃comment_date＃＃

評論：
＃ryment_text＃

URL地址：
＃comment_path＃

自動生成的消息。";
$MESS["NEW_BLOG_COMMENT_NAME"] = "博客中的新評論";
$MESS["NEW_BLOG_COMMENT_SUBJECT"] = "＃site_name＃：[b]＃message_title＃：＃comment_title＃＃";
$MESS["NEW_BLOG_COMMENT_WITHOUT_TITLE_DESC"] = "＃blog_id＃ - 博客ID
＃blog_name＃ - 博客標題
＃blog_url＃ - 博客URL
＃message_title＃ - 消息標題
＃ryment_text＃ - 評論文本
＃comment_date＃ - 評論日期
＃comment_path＃ - 註釋URL
＃作者＃ - 評論作者
＃email_from＃ - 發送者電子郵件
＃email_to＃ - 收件人電子郵件";
$MESS["NEW_BLOG_COMMENT_WITHOUT_TITLE_MESSAGE"] = "來自網站的信息消息＃site_name＃
-------------------------------------------------- --------

博客中的新註釋\“＃blog_name＃\” to post \“＃message_title＃\”。

作者：＃作者＃
日期：＃comment_date＃＃

評論：
＃ryment_text＃

URL地址：
＃comment_path＃

自動生成的消息。";
$MESS["NEW_BLOG_COMMENT_WITHOUT_TITLE_NAME"] = "博客中的新評論（無主題）";
$MESS["NEW_BLOG_COMMENT_WITHOUT_TITLE_SUBJECT"] = "＃site_name＃：[b]＃message_title＃";
$MESS["NEW_BLOG_MESSAGE_DESC"] = "＃blog_id＃ - 博客ID
＃blog_name＃ - 博客標題
＃blog_url＃ - 博客URL
＃message_title＃ - 消息標題
＃Message_Text＃ - 消息文本
＃message_date＃ - 消息日期
＃Message_path＃ -URL到消息
＃作者＃ - 消息作者
＃email_from＃ - 發送者電子郵件
＃email_to＃ - 收件人電子郵件";
$MESS["NEW_BLOG_MESSAGE_MESSAGE"] = "來自網站的信息消息＃site_name＃
-------------------------------------------------- --------

博客中的新消息\“＃blog_name＃\”。

主題：
＃Message_title＃

作者：＃作者＃
日期：＃message_date＃
消息文字：

＃message_text＃

消息URL：
＃Message_path＃

自動生成的消息。";
$MESS["NEW_BLOG_MESSAGE_NAME"] = "新的博客消息";
$MESS["NEW_BLOG_MESSAGE_SUBJECT"] = "＃site_name＃：[b]＃blog_name＃：＃message_title＃";
