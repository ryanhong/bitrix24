<?php
$MESS["BLI_COPY_FOLDER"] = "要復製文件的文件夾（相對於站點根）";
$MESS["BLI_COPY_PUBLIC_FILES"] = "安裝公共部分";
$MESS["BLI_DELETE_EMAIL"] = "刪除電子郵件模板";
$MESS["BLI_INSTALL_404"] = "安裝使用SEF模式的公共文件";
$MESS["BLI_INSTALL_EMAIL"] = "創建電子郵件模板";
$MESS["BLI_INSTALL_SMILES"] = "安裝微笑";
$MESS["BLI_PERM_D"] = "閱讀博客";
$MESS["BLI_PERM_K"] = "查看公共頁面";
$MESS["BLI_PERM_N"] = "創建博客";
$MESS["BLI_PERM_R"] = "查看控制面板頁面";
$MESS["BLI_PERM_W"] = "完全訪問";
$MESS["BLOG_INSTALL_DESCRIPTION"] = "使用此模塊，您可以在網站上設置，主機並保留Web日誌。";
$MESS["BLOG_INSTALL_NAME"] = "部落格";
$MESS["BLOG_INSTALL_PUBLIC_REW"] = "覆蓋現有文件";
$MESS["BLOG_INSTALL_TITLE"] = "博客模塊安裝";
