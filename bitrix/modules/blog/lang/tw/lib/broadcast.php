<?php
$MESS["BLOG_BROADCAST_PUSH_POST"] = "＃作者＃發布了一個新消息\“＃標題＃\”";
$MESS["BLOG_BROADCAST_PUSH_POSTA"] = "＃作者＃發布了一條新消息";
$MESS["BLOG_BROADCAST_PUSH_POSTA_F"] = "＃作者＃發布了一條新消息";
$MESS["BLOG_BROADCAST_PUSH_POSTA_M"] = "＃作者＃發布了一條新消息";
$MESS["BLOG_BROADCAST_PUSH_POST_F"] = "＃作者＃發布了一個新消息\“＃標題＃\”";
$MESS["BLOG_BROADCAST_PUSH_POST_M"] = "＃作者＃發布了一個新消息\“＃標題＃\”";
$MESS["BLOG_BROADCAST_REQUEST_IM_BUTTON_OFF_N"] = "不，謝謝";
$MESS["BLOG_BROADCAST_REQUEST_IM_BUTTON_OFF_Y"] = "禁用通知";
$MESS["BLOG_BROADCAST_REQUEST_IM_BUTTON_ON_N"] = "不，謝謝";
$MESS["BLOG_BROADCAST_REQUEST_IM_BUTTON_ON_Y"] = "啟用通知";
$MESS["BLOG_BROADCAST_REQUEST_IM_MESSAGE_OFF"] = "我們已經分析了您的活動流內容，並且似乎向所有用戶發送了太多消息。
建議您禁用給所有用戶的消息通知，並對這些消息進行評論。";
$MESS["BLOG_BROADCAST_REQUEST_IM_MESSAGE_ON"] = "我們已經分析了您的活動流內容。每個用戶收到有關所有用戶發送的消息的通知，以及這些消息留下的每個評論。";
