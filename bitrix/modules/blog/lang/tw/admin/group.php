<?php
$MESS["BLG_ADD_NEW"] = "新小組";
$MESS["BLG_ADD_NEW_ALT"] = "點擊添加新組";
$MESS["BLG_DELETE_ALT"] = "刪除組";
$MESS["BLG_DELETE_CONF"] = "您確定要刪除這個組嗎？";
$MESS["BLG_DELETE_ERROR"] = "錯誤刪除組";
$MESS["BLG_ERROR_UPDATE"] = "錯誤更新組參數";
$MESS["BLG_FILTER_SITE_ID"] = "地點";
$MESS["BLG_GROUP_NAME"] = "姓名";
$MESS["BLG_GROUP_NAV"] = "組";
$MESS["BLG_GROUP_SITE_ID"] = "地點";
$MESS["BLG_SPT_ALL"] = "[全部]";
$MESS["BLG_TITLE"] = "博客小組";
$MESS["BLG_UPDATE_ALT"] = "編輯組參數";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "檢查：";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "刪除";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "選擇：";
