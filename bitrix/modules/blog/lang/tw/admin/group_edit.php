<?php
$MESS["BLGE_2FLIST"] = "小組列表";
$MESS["BLGE_ADDING"] = "創建新組";
$MESS["BLGE_DELETE_GROUP"] = "刪除組";
$MESS["BLGE_DELETE_GROUP_CONFIRM"] = "您確定要刪除小組嗎？如果小組包含博客，則不會刪除。";
$MESS["BLGE_ERROR_SAVING"] = "錯誤保存組";
$MESS["BLGE_NAME"] = "團隊名字";
$MESS["BLGE_NEW_GROUP"] = "新小組";
$MESS["BLGE_NO_PERMS2ADD"] = "創建組的權限不足";
$MESS["BLGE_SITE"] = "小組網站";
$MESS["BLGE_TAB_GROUP"] = "博客小組";
$MESS["BLGE_TAB_GROUP_DESCR"] = "博客組參數";
$MESS["BLGE_UPDATING"] = "編輯組參數";
