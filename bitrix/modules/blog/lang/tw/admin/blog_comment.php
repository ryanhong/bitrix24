<?php
$MESS["BLB_AUTHOR_ANONYM"] = "不安";
$MESS["BLB_AUTHOR_EMAIL"] = "作者電子郵件";
$MESS["BLB_AUTHOR_ID"] = "作者";
$MESS["BLB_AUTHOR_IP"] = "作者IP";
$MESS["BLB_AUTHOR_IP1"] = "作者IP1";
$MESS["BLB_AUTHOR_NAME"] = "作者名稱";
$MESS["BLB_BLOG_ACTIVE"] = "博客活躍";
$MESS["BLB_BLOG_GROUP_ID"] = "博客小組";
$MESS["BLB_BLOG_GROUP_SITE_ID"] = "地點";
$MESS["BLB_BLOG_ID"] = "部落格";
$MESS["BLB_BLOG_OWNER_ID"] = "博客所有者";
$MESS["BLB_BLOG_SOCNET_GROUP_ID"] = "博客小組所有者";
$MESS["BLB_COMMENT"] = "評論";
$MESS["BLB_DATE_CREATE"] = "創建於";
$MESS["BLB_DELETE_ALT"] = "刪除";
$MESS["BLB_DELETE_CONF"] = "你確定要刪除此評論嗎？";
$MESS["BLB_DELETE_ERROR"] = "錯誤刪除評論。";
$MESS["BLB_F_ALL"] = "（全部）";
$MESS["BLB_GROUP_NAV"] = "評論";
$MESS["BLB_HIDE_ALT"] = "隱藏";
$MESS["BLB_HIDE_ERROR"] = "錯誤隱藏評論。";
$MESS["BLB_NO"] = "不";
$MESS["BLB_POST_ID"] = "訊息";
$MESS["BLB_POST_TEXT"] = "評論文字";
$MESS["BLB_PUBLISH_STATUS"] = "出版";
$MESS["BLB_SHOW_ALT"] = "展示";
$MESS["BLB_SHOW_ERROR"] = "錯誤顯示評論。";
$MESS["BLB_STOP_LIST"] = "停止列表";
$MESS["BLB_TITLE"] = "評論";
$MESS["BLB_VIEW_ALT"] = "在網站上查看評論";
$MESS["BLB_YES"] = "是的";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "檢查：";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "刪除";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "選擇：";
