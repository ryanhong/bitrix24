<?php
$MESS["BLG_AM_BLOGS"] = "部落格";
$MESS["BLG_AM_BLOGS1"] = "部落格";
$MESS["BLG_AM_BLOGS1_ALT"] = "博客列表";
$MESS["BLG_AM_BLOGS_ALT"] = "博客管理";
$MESS["BLG_AM_COMMENT"] = "評論";
$MESS["BLG_AM_COMMENT_ALT"] = "管理博客評論";
$MESS["BLG_AM_GROUPS"] = "博客小組";
$MESS["BLG_AM_GROUPS_ALT"] = "博客小組";
$MESS["BLG_AM_POST"] = "帖子";
$MESS["BLG_AM_POST_ALT"] = "管理博客文章";
$MESS["BLOG_MENU_SMILES"] = "笑臉";
$MESS["BLOG_MENU_SMILES_ALT"] = "編輯笑臉和圖標";
