<?php
$MESS["BLG_NS"] = "對話（活動流中的帖子）";
$MESS["BLG_NS_BROADCAST_COMMENT"] = "向所有用戶添加評論以獲取消息";
$MESS["BLG_NS_BROADCAST_POST"] = "向所有用戶發送新消息";
$MESS["BLG_NS_COMMENT"] = "您的帖子中添加了評論";
$MESS["BLG_NS_GRAT"] = "欣賞通知";
$MESS["BLG_NS_IM_ANSWER_ERROR"] = "錯誤發布評論";
$MESS["BLG_NS_IM_ANSWER_SUCCESS"] = "評論已發表";
$MESS["BLG_NS_MENTION"] = "您在帖子中被提及";
$MESS["BLG_NS_MENTION_COMMENT"] = "您在評論中提到";
$MESS["BLG_NS_MODERATE_COMMENT"] = "新的未修改評論";
$MESS["BLG_NS_MODERATE_POST"] = "新的未修改帖子";
$MESS["BLG_NS_POST"] = "您被指定為郵政收件人";
$MESS["BLG_NS_POST_MAIL"] = "您的消息已發佈到活動流。";
$MESS["BLG_NS_PUBLISHED_COMMENT"] = "您的評論由主持人發表";
$MESS["BLG_NS_PUBLISHED_POST"] = "您的帖子由主持人發布";
$MESS["BLG_NS_SHARE"] = "您的帖子與更多用戶共享";
$MESS["BLG_NS_SHARE2USERS"] = "帖子與您分享";
