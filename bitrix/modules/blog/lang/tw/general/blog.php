<?php
$MESS["BLG_GB_BAD_URL"] = "偽名稱只能包含拉丁字母，數字，破折號和下劃線。";
$MESS["BLG_GB_DUBLICATE_NAME"] = "這個名字的博客已經存在";
$MESS["BLG_GB_DUBLICATE_URL"] = "這個偽名稱的博客已經存在";
$MESS["BLG_GB_EMPTY_DATE_CREATE"] = "博客創建日期不正確";
$MESS["BLG_GB_EMPTY_DATE_UPDATE"] = "博客更新日期不正確";
$MESS["BLG_GB_EMPTY_GROUP_ID"] = "未指定包含博客的組";
$MESS["BLG_GB_EMPTY_LAST_POST_DATE"] = "最後一個博客消息的創建日期不正確";
$MESS["BLG_GB_EMPTY_NAME"] = "未指定博客的名稱";
$MESS["BLG_GB_EMPTY_OWNER_ID"] = "未指定博客所有者";
$MESS["BLG_GB_EMPTY_URL"] = "未指定博客的偽名稱（拉丁字母）";
$MESS["BLG_GB_ERROR_NO_GROUP_ID"] = "包含博客的小組不正確";
$MESS["BLG_GB_ERROR_NO_OWNER_ID"] = "博客所有者ID不正確";
$MESS["BLG_GB_RESERVED_NAME"] = "博客不能具有偽名稱＆quot＃name＃＆quot;";
$MESS["BLG_GB_RSS_DETAIL"] = "更多的...";
$MESS["BLG_RSS_ALL_GROUP_TITLE"] = "小組博客的帖子";
$MESS["BLG_RSS_ALL_TITLE"] = "網站博客的帖子";
$MESS["BLG_RSS_NAME_SONET"] = "＃fure_name＃：對話";
$MESS["BLG_RSS_NAME_SONET_GROUP"] = "WorkGroup \“＃group_name＃\”：對話";
$MESS["BLG_TRY_AGAIN"] = "錯誤創建博客。請再試一次。";
