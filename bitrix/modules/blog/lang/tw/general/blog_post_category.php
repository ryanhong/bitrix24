<?php
$MESS["BLG_GCT_EMPTY_BLOG_ID"] = "標籤的博客未指定";
$MESS["BLG_GCT_EMPTY_CATEGORY_ID"] = "標籤未指定";
$MESS["BLG_GCT_EMPTY_POST_ID"] = "標籤的帖子未指定";
$MESS["BLG_GCT_ERROR_NO_BLOG"] = "帶有ID'＃id＃'的博客找不到";
$MESS["BLG_GCT_ERROR_NO_CATEGORY"] = "帶有ID'＃ID＃'的標籤";
$MESS["BLG_GCT_ERROR_NO_POST"] = "帶有ID'＃id＃'的帖子找不到";
