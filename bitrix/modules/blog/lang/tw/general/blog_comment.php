<?php
$MESS["BLG_GCM_COMMENT_TITLE"] = "評論\ \“＃post_title＃\”來自＃comment_author＃";
$MESS["BLG_GCM_EMPTY_AUTHOR_ID"] = "評論作者未指定";
$MESS["BLG_GCM_EMPTY_AUTHOR_NAME"] = "評論作者的名稱未指定";
$MESS["BLG_GCM_EMPTY_BLOG_ID"] = "評論博客未指定";
$MESS["BLG_GCM_EMPTY_POST_ID"] = "未指定要註釋的消息";
$MESS["BLG_GCM_EMPTY_POST_TEXT"] = "評論文字是空的";
$MESS["BLG_GCM_EMPTY_TITLE"] = "評論標題為空";
$MESS["BLG_GCM_ERROR_AUTHOR_EMAIL"] = "評論作者的電子郵件地址不正確";
$MESS["BLG_GCM_ERROR_DATE_CREATE"] = "評論的創建日期不正確";
$MESS["BLG_GCM_ERROR_NO_AUTHOR_ID"] = "評論作者ID不正確";
$MESS["BLG_GCM_ERROR_NO_BLOG"] = "帶有ID'＃id＃'的博客找不到";
$MESS["BLG_GCM_ERROR_NO_COMMENT"] = "評論ID'＃id＃'找不到";
$MESS["BLG_GCM_ERROR_NO_POST"] = "帶有ID'＃id＃'的消息找不到";
$MESS["BLG_GCM_RSS_TITLE"] = "\“＃blog_name＃\” on \“ post_title＃\”中的註釋";
$MESS["BLG_GCM_RSS_TITLE_SOCNET"] = "＃fure_name＃：對話的評論\“＃post_title＃\”";
$MESS["BLG_SONET_COMMENT_TITLE"] = "在博客中向帖子中添加了評論\“＃標題＃\”";
