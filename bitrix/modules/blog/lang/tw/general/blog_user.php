<?php
$MESS["BLG_GU_EMPTY_USER_ID"] = "未指定用戶ID";
$MESS["BLG_GU_ERROR_AUTHOR"] = "刪除用戶帖子時出錯。";
$MESS["BLG_GU_ERROR_DATE_REG"] = "註冊日期不正確";
$MESS["BLG_GU_ERROR_DUPL_ALIAS"] = "這個暱稱已經存在";
$MESS["BLG_GU_ERROR_LAST_VISIT"] = "上次訪問日期不正確";
$MESS["BLG_GU_ERROR_NO_BLOG"] = "帶有ID＃ID＃的博客找不到";
$MESS["BLG_GU_ERROR_NO_USER_ID"] = "用戶ID不正確";
$MESS["BLG_GU_ERROR_OWNER"] = "用戶是博客所有者，因此無法刪除";
$MESS["BLG_GU_NAME_TEMPLATE_DEFAULT"] = "＃名稱＃＃last_name＃";
