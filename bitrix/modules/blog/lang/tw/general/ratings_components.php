<?php
$MESS["BLOG_MODULE_NAME"] = "部落格";
$MESS["BLOG_RATING_USER_RATING_ACTIVITY_DESC"] = "估計使用了當今創建的帖子的計數；在過去的7天和過去30天中。";
$MESS["BLOG_RATING_USER_RATING_ACTIVITY_FIELDS_ALL_COMMENT_COEF"] = "評論以上的評論因素：";
$MESS["BLOG_RATING_USER_RATING_ACTIVITY_FIELDS_ALL_POST_COEF"] = "帖子以上的帖子因素：";
$MESS["BLOG_RATING_USER_RATING_ACTIVITY_FIELDS_MONTH_COMMENT_COEF"] = "評級乘數在最近一個月的評論：";
$MESS["BLOG_RATING_USER_RATING_ACTIVITY_FIELDS_MONTH_POST_COEF"] = "最近一個月的評級乘數：";
$MESS["BLOG_RATING_USER_RATING_ACTIVITY_FIELDS_TODAY_COMMENT_COEF"] = "評級乘數今天的評論：";
$MESS["BLOG_RATING_USER_RATING_ACTIVITY_FIELDS_TODAY_POST_COEF"] = "評級今天的帖子：";
$MESS["BLOG_RATING_USER_RATING_ACTIVITY_FIELDS_WEEK_COMMENT_COEF"] = "評級乘數最近一周評論：";
$MESS["BLOG_RATING_USER_RATING_ACTIVITY_FIELDS_WEEK_POST_COEF"] = "最近一周的評級乘數：";
$MESS["BLOG_RATING_USER_RATING_ACTIVITY_FORMULA_DESC"] = "p <sub> 1 </sub>，p <sub> 7 </sub>，p <ub> 30 </sub>，p <sub>所有</sub>  - 創建的帖子數：今天；在上週；在上個月和永恆的； <br>
k <sub> p1 </sub>，k <sub> p7 </sub>，k <ub> p30 </sub>，k <ub> pall </sub>  - 創建的帖子的用戶定義因素：今天；在上週；在上個月和永恆的。<br>
C <un> 1 </sub>，C <ub> 7 </sub>，C <ub> 30 </sub>，c <un>所有</sub>  - 添加的註釋數：今天；在上週；在上個月和永恆的； <br>
k <un> c1 </sub>，k <sub> c7 </sub>，k <sub> c30 </sub>，k <ub>呼叫</sub>  - 用戶定義的評論因素：今天；在上週；在過去的一個月和永恆。";
$MESS["BLOG_RATING_USER_RATING_ACTIVITY_NAME"] = "博客活動";
$MESS["BLOG_RATING_USER_VOTE_COMMENT_DESC"] = "使用指定天數的投票結果計算評級。<br>將天數設置為\“ 0 \”以使用所有數據進行計算。";
$MESS["BLOG_RATING_USER_VOTE_COMMENT_LIMIT_NAME"] = "白天跨度：";
$MESS["BLOG_RATING_USER_VOTE_COMMENT_NAME"] = "在博客中投票給用戶的評論";
$MESS["BLOG_RATING_USER_VOTE_POST_DESC"] = "使用指定天數的投票結果計算評級。<br>將天數設置為\“ 0 \”以使用所有數據進行計算。";
$MESS["BLOG_RATING_USER_VOTE_POST_LIMIT_NAME"] = "白天跨度：";
$MESS["BLOG_RATING_USER_VOTE_POST_NAME"] = "在博客中投票給用戶的帖子";
