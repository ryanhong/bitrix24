<?php
$MESS["IMOPENLINES_ACCOUNT_DEBUG"] = "調試模式";
$MESS["IMOPENLINES_ACCOUNT_ERROR_PUBLIC"] = "未指定正確的公共地址。";
$MESS["IMOPENLINES_ACCOUNT_EXEC_DESCRIPTION"] = "可以設置活動以在代理商或cron上運行。<br>
1. <b>代理</b>：可以在每個新的命中上運行，也可以分配給Cron（即計劃）。不建議在命中奔跑。<br>
2. <b> cron </b>是推薦模式。您必須在切換此選項之前確保正確配置服務器。將/bitrix/tools/imopenlines/agents.php設置為每分鐘運行。<br>
<a style = \"cursor：pointer \" class = \"bx-helpdesk-link \" onclick= \"top.bx.helper.helper.show('redirect=detail＆code = 9283081 :) \ \">請參考文檔有關更多詳細信息</a>。";
$MESS["IMOPENLINES_ACCOUNT_EXEC_MODE"] = "執行為";
$MESS["IMOPENLINES_ACCOUNT_EXEC_MODE_AGENT"] = "代理人";
$MESS["IMOPENLINES_ACCOUNT_EXEC_MODE_CRON"] = "克朗";
$MESS["IMOPENLINES_ACCOUNT_URL"] = "網站公共地址";
$MESS["IMOPENLINES_TAB_SETTINGS"] = "設定";
$MESS["IMOPENLINES_TAB_TITLE_SETTINGS_2"] = "模塊設置";
