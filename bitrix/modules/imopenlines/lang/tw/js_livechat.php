<?php
$MESS["DEFAULT_TITLE"] = "開放通道";
$MESS["ERROR_3RD_PARTY_COOKIE_DESC"] = "設置您的Web瀏覽器以接受第三方Cookie或使用其他通信渠道。";
$MESS["ERROR_INTRANET_USER_DESC"] = "要使用此聊天，您需要從當前的Bitrix24帳戶中登錄：＃url＃";
$MESS["ERROR_INTRANET_USER_DESC_2"] = "您無法寫入此聊天，因為您已經在此瀏覽器中作為員工登錄了此Bitrix24。但是，您可以從＃url_start＃發佈到您的Portal＃url_end＃或使用以下通信渠道之一：";
$MESS["ERROR_TITLE"] = "不幸的是，我們無法加載實時聊天。";
$MESS["ERROR_UNKNOWN"] = "請使用其他交流方式。";
$MESS["LOADING_MESSAGE"] = "給我們留言，我們會盡快回复。";
$MESS["POWERED_BY"] = "供電";
$MESS["READY_TO_RESPOND"] = "我們的熟練專業人士團隊在這裡提供幫助！";
$MESS["RESPOND_LATER"] = "我們的一位代表將盡快與您聯繫。";
$MESS["SONET_ICONS"] = "對其他使者感到更舒服？";
$MESS["SONET_ICONS_CLICK"] = "單擊圖標使用您選擇的即時信使";
$MESS["TEXTAREA_FILE"] = "發送文件";
$MESS["TEXTAREA_HOTKEY"] = "點擊更改鍵盤快捷鍵";
$MESS["TEXTAREA_PLACEHOLDER"] = "輸入消息";
$MESS["TEXTAREA_SEND"] = "發信息";
$MESS["TEXTAREA_SMILE"] = "選擇表情符號";
