<?php
$MESS["IMOL_QUEUE_OPERATOR_DISMISSAL"] = "由於代理人已被駁回，因此從代理商那裡刪除了對話";
$MESS["IMOL_QUEUE_OPERATOR_NONWORKING"] = "由於代理商已經計時或尚未計時，因此從代理商中刪除了對話。";
$MESS["IMOL_QUEUE_OPERATOR_NOT_AVAILABLE"] = "由於代理不可用，因此從代理商中刪除了對話。";
$MESS["IMOL_QUEUE_OPERATOR_OFFLINE"] = "對話已從代理商中刪除，因為此代理人離線";
$MESS["IMOL_QUEUE_OPERATOR_REMOVING"] = "由於代理人已從開放渠道刪除，因此從代理商中刪除了對話";
$MESS["IMOL_QUEUE_OPERATOR_VACATION"] = "由於特工要休假，因此從代理商那裡刪除了對話";
$MESS["QUEUE_OPERATOR_DEFAULT_NAME"] = "代理人";
