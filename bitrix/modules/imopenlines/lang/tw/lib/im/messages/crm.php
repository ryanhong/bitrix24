<?php
$MESS["IMOL_MESSAGE_CRM_CARD_COMPANY_TITLE"] = "公司";
$MESS["IMOL_MESSAGE_CRM_CARD_EMAIL"] = "電子郵件";
$MESS["IMOL_MESSAGE_CRM_CARD_FULL_NAME"] = "姓名";
$MESS["IMOL_MESSAGE_CRM_CARD_PHONE"] = "電話";
$MESS["IMOL_MESSAGE_CRM_CARD_POST"] = "位置";
$MESS["IMOL_MESSAGE_CRM_COMPANY_ADD_NEW"] = "創建了新公司";
$MESS["IMOL_MESSAGE_CRM_COMPANY_EXTEND"] = "保存給公司的聯繫信息";
$MESS["IMOL_MESSAGE_CRM_COMPANY_UPDATE"] = "對話的公司";
$MESS["IMOL_MESSAGE_CRM_CONTACT_ADD_NEW"] = "創建了新的聯繫人";
$MESS["IMOL_MESSAGE_CRM_CONTACT_EXTEND"] = "保存到聯繫人的聯繫信息";
$MESS["IMOL_MESSAGE_CRM_CONTACT_UPDATE"] = "對話附帶的聯繫人";
$MESS["IMOL_MESSAGE_CRM_DEAL_ADD_NEW"] = "創建了新交易";
$MESS["IMOL_MESSAGE_CRM_DEAL_EXTEND"] = "保存到交易的聯繫方式";
$MESS["IMOL_MESSAGE_CRM_DEAL_UPDATE"] = "交談的交易";
$MESS["IMOL_MESSAGE_CRM_LEAD_ADD_NEW"] = "創建了一個新的線索";
$MESS["IMOL_MESSAGE_CRM_LEAD_EXTEND"] = "聯繫信息保存到領先";
$MESS["IMOL_MESSAGE_CRM_LEAD_UPDATE"] = "對話的鉛";
$MESS["IMOL_MESSAGE_CRM_ORDER_ADD_NEW"] = "創建了新訂單";
$MESS["IMOL_MESSAGE_CRM_ORDER_EXTEND"] = "保存到訂單的聯繫信息";
$MESS["IMOL_MESSAGE_CRM_ORDER_UPDATE"] = "聊天附帶的訂單";
$MESS["IMOL_MESSAGE_CRM_OTHER_ADD_NEW"] = "創建的新CRM實體";
$MESS["IMOL_MESSAGE_CRM_OTHER_EXTEND"] = "保存給CRM實體的聯繫信息";
$MESS["IMOL_MESSAGE_CRM_OTHER_UPDATE"] = "聊天附帶的CRM實體";
