<?php
$MESS["IMOL_QUEUE_NOTIFICATION_EMPTY_QUEUE"] = "您缺少客戶通過聯繫中心頻道發布的消息。當前沒有僱員分配給相應的消息隊列。 [url =＃url＃]配置消息隊列[/url]";
$MESS["IMOL_QUEUE_SESSION_SKIP_ALONE"] = "您不能拒絕對話，因為您是隊列中唯一可用的人。";
$MESS["IMOL_QUEUE_SESSION_TRANSFER_OPERATOR_OFFLINE"] = "會話重新提交到隊列，因為代理是離線的";
