<?php
$MESS["IMOL_SECURITY_ACTION_MODIFY"] = "編輯";
$MESS["IMOL_SECURITY_ACTION_PERFORM"] = "執行";
$MESS["IMOL_SECURITY_ACTION_VIEW"] = "看法";
$MESS["IMOL_SECURITY_ENTITY_CONNECTORS"] = "連接通信渠道";
$MESS["IMOL_SECURITY_ENTITY_HISTORY"] = "溝通歷史";
$MESS["IMOL_SECURITY_ENTITY_JOIN"] = "加入對話";
$MESS["IMOL_SECURITY_ENTITY_LINES"] = "開放頻道";
$MESS["IMOL_SECURITY_ENTITY_QUICK_ANSWERS"] = "罐頭響應";
$MESS["IMOL_SECURITY_ENTITY_SESSION"] = "通信統計";
$MESS["IMOL_SECURITY_ENTITY_SETTINGS"] = "常見參數";
$MESS["IMOL_SECURITY_ENTITY_SOFT_PAUSE_LIST"] = "非活性劑";
$MESS["IMOL_SECURITY_ENTITY_VOTE_HEAD"] = "對話評分";
$MESS["IMOL_SECURITY_PERMISSION_ALLOW"] = "授予訪問權限";
$MESS["IMOL_SECURITY_PERMISSION_ANY"] = "任何";
$MESS["IMOL_SECURITY_PERMISSION_DEPARTMENT"] = "個人和部門";
$MESS["IMOL_SECURITY_PERMISSION_NONE"] = "拒絕訪問。";
$MESS["IMOL_SECURITY_PERMISSION_SELF"] = "個人的";
