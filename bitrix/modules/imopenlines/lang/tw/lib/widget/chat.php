<?php
$MESS["IMOL_WIDGET_CHAT_ERROR_CONFIG_NOT_FOUND"] = "指定的開放通道不存在。";
$MESS["IMOL_WIDGET_CHAT_ERROR_CREATE"] = "錯誤創建聊天。";
$MESS["IMOL_WIDGET_CHAT_ERROR_USER_NOT_FOUND"] = "指定的用戶不存在。";
$MESS["IMOL_WIDGET_CHAT_NAME"] = "＃user_name＃ - ＃line_name＃";
