<?php
$MESS["IMOL_QA_IBLOCK_COMMON_SECTION"] = "常見的";
$MESS["IMOL_QA_IBLOCK_DELIVERY_SECTION"] = "送貨";
$MESS["IMOL_QA_IBLOCK_DESCRIPTION"] = "包含罐頭響應，用於與開放頻道一起使用";
$MESS["IMOL_QA_IBLOCK_ELEMENTS_NAME"] = "回答";
$MESS["IMOL_QA_IBLOCK_ELEMENT_ADD"] = "添加響應";
$MESS["IMOL_QA_IBLOCK_ELEMENT_DELETE"] = "刪除響應";
$MESS["IMOL_QA_IBLOCK_ELEMENT_EDIT"] = "編輯響應";
$MESS["IMOL_QA_IBLOCK_ELEMENT_NAME"] = "回覆";
$MESS["IMOL_QA_IBLOCK_GREETING_SECTION"] = "歡迎消息";
$MESS["IMOL_QA_IBLOCK_NAME"] = "罐頭響應";
$MESS["IMOL_QA_IBLOCK_NAME_FIELD"] = "姓名";
$MESS["IMOL_QA_IBLOCK_PAYMENT_SECTION"] = "支付";
$MESS["IMOL_QA_IBLOCK_RATING_FIELD"] = "評分";
$MESS["IMOL_QA_IBLOCK_SECTIONS_NAME"] = "部分";
$MESS["IMOL_QA_IBLOCK_SECTION_ADD"] = "添加部分";
$MESS["IMOL_QA_IBLOCK_SECTION_DELETE"] = "刪除部分";
$MESS["IMOL_QA_IBLOCK_SECTION_EDIT"] = "編輯部分";
$MESS["IMOL_QA_IBLOCK_SECTION_NAME"] = "部分";
$MESS["IMOL_QA_IBLOCK_TEXT_FIELD"] = "罐頭響應文字";
