<?php
$MESS["IMOL_TRACKER_BUTTON_CANCEL"] = "取消";
$MESS["IMOL_TRACKER_BUTTON_CHANGE"] = "改變";
$MESS["IMOL_TRACKER_COMPANY_EXTEND"] = "保存給公司的聯繫信息";
$MESS["IMOL_TRACKER_CONTACT_EXTEND"] = "保存到聯繫人的聯繫信息";
$MESS["IMOL_TRACKER_ERROR_NO_REQUIRED_PARAMETERS"] = "未提交所需的參數或具有無效格式";
$MESS["IMOL_TRACKER_LEAD_ADD"] = "使用聯繫信息創建了一個新的潛在客戶";
$MESS["IMOL_TRACKER_LEAD_EXTEND"] = "聯繫信息保存到領先";
$MESS["IMOL_TRACKER_LIMIT_1"] = "聯繫信息沒有保存到CRM，因為超出了您當前計劃規定的識別限制。 ＃link_start＃升級現在＃link_end＃＃";
$MESS["IMOL_TRACKER_LIMIT_2"] = "沒有執行CRM搜索，因為超出了您當前計劃規定的每月識別限制。 ＃link_start＃升級現在＃link_end＃＃";
$MESS["IMOL_TRACKER_LIMIT_BUTTON"] = "升級";
$MESS["IMOL_TRACKER_SESSION_COMPANY"] = "公司";
$MESS["IMOL_TRACKER_SESSION_CONTACT"] = "接觸";
$MESS["IMOL_TRACKER_SESSION_DEAL"] = "交易";
