<?php
$MESS["IMOL_LCC_ERROR_ACCESS_DENIED"] = "您沒有足夠的權限訪問此對話。";
$MESS["IMOL_LCC_ERROR_CHAT_ID"] = "聊天ID無效。";
$MESS["IMOL_LCC_ERROR_CHAT_TYPE"] = "這個聊天不是打開的頻道";
$MESS["IMOL_LCC_ERROR_FORM_ID"] = "表格ID未提交";
$MESS["IMOL_LCC_ERROR_IM_LOAD"] = "錯誤初始化Instant Messenger模塊。";
$MESS["IMOL_LCC_ERROR_PULL_LOAD"] = "錯誤初始化推送和拉模塊。";
$MESS["IMOL_LCC_ERROR_USER_ID"] = "用戶ID無效。";
$MESS["IMOL_LCC_FORM_EMAIL"] = "電子郵件";
$MESS["IMOL_LCC_FORM_HISTORY"] = "客戶要求的對話日誌##鏈接＃";
$MESS["IMOL_LCC_FORM_HISTORY_2"] = "對話的歷史##鏈接＃已根據他們的要求發送給客戶";
$MESS["IMOL_LCC_FORM_NAME"] = "姓名";
$MESS["IMOL_LCC_FORM_NONE"] = "沒有任何";
$MESS["IMOL_LCC_FORM_PHONE"] = "電話";
$MESS["IMOL_LCC_FORM_SUBMIT"] = "提交的表格";
$MESS["IMOL_LCC_GUEST_NAME"] = "客人";
