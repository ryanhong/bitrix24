<?php
$MESS["IMOPENLINES_CHECK_CONNECTOR"] = "未安裝\“外部IM連接器\”模塊。";
$MESS["IMOPENLINES_CHECK_CONNECTOR_MSGVER_1"] = "未安裝外部Messenger連接器模塊。";
$MESS["IMOPENLINES_CHECK_IM"] = "未安裝\“ Instant Messenger \”模塊。";
$MESS["IMOPENLINES_CHECK_IM_VERSION"] = "請將\“ Instant Messenger \”模塊更新為版本16.5.0";
$MESS["IMOPENLINES_CHECK_PUBLIC_PATH"] = "未指定正確的公共地址。";
$MESS["IMOPENLINES_CHECK_PULL"] = "未安裝\“ push and pull \”模塊，或者未配置隊列服務器。";
$MESS["IMOPENLINES_INSTALL_TITLE"] = "\“ Open Channels \”模塊安裝";
$MESS["IMOPENLINES_MODULE_DESCRIPTION"] = "開放通道模塊。";
$MESS["IMOPENLINES_MODULE_NAME"] = "開放頻道";
$MESS["IMOPENLINES_UNINSTALL_QUESTION"] = "您確定要刪除模塊嗎？";
$MESS["IMOPENLINES_UNINSTALL_TITLE"] = "\“ Open Channels \”模塊卸載";
