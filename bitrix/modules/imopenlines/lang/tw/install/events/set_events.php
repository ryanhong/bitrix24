<?php
$MESS["IMOL_HISTORY_LOG_NAME"] = "打開頻道對話日誌";
$MESS["IMOL_MAIL_PARAMS_DESC_NEW"] = "＃email_to＃ - 收件人的電子郵件
＃標題＃ - 消息主題
＃Template_session_id＃ -會話ID
＃template_action_title＃ - 動作標題
＃template_action_desc＃ - 操作說明
＃template_widget_domain ＃-小部件的站點域
＃Template_widget_url＃-Widget的頁面URL
＃template_line_name＃ - 打開頻道名稱";
$MESS["IMOL_OPERATOR_ANSWER_NAME_NEW"] = "客戶的消息和代理在開放頻道會話中的答复";
