<?php
$MESS["IMOL_QUICK_ANSWERS_EDIT_ALL"] = "全部";
$MESS["IMOL_QUICK_ANSWERS_EDIT_CANCEL"] = "取消";
$MESS["IMOL_QUICK_ANSWERS_EDIT_CREATE"] = "創造";
$MESS["IMOL_QUICK_ANSWERS_EDIT_ERROR"] = "無法保存罐頭響應。請稍後再試。";
$MESS["IMOL_QUICK_ANSWERS_EDIT_ERROR_EMPTY_TEXT"] = "文字不應該為空";
$MESS["IMOL_QUICK_ANSWERS_EDIT_SECTION_TITLE"] = "在本節中創建罐頭響應";
$MESS["IMOL_QUICK_ANSWERS_EDIT_SUCCESS"] = "您的罐頭響應已保存！";
$MESS["IMOL_QUICK_ANSWERS_EDIT_TEXT_PLACEHOLDER"] = "罐頭響應文字";
$MESS["IMOL_QUICK_ANSWERS_EDIT_UPDATE"] = "更新";
$MESS["IMOL_QUICK_ANSWERS_INFO_LIST_1"] = "單擊按鈕<span class = \“ imopenlines-iframe-quick-check-icon \”> </span>在消息旁";
$MESS["IMOL_QUICK_ANSWERS_INFO_LIST_2"] = "從頭開始創建一個新的";
$MESS["IMOL_QUICK_ANSWERS_INFO_LIST_3"] = "或打開<a id = \"quick-info-all-url \“class = \"imopenlines-iframe-quick-link \">罐頭響應形式</a>";
$MESS["IMOL_QUICK_ANSWERS_INFO_TITLE_NEW"] = "使用罐頭響應來快速回答常見問題";
$MESS["IMOL_QUICK_ANSWERS_NOT_FOUND"] = "抱歉，我們什麼都找不到";
$MESS["IMOL_QUICK_ANSWERS_SEARCH_PROGRESS"] = "搜索正在進行...";
