<?php
$MESS["IMOL_PERM_ADD"] = "添加";
$MESS["IMOL_PERM_ADD_ACCESS_CODE"] = "添加訪問權限";
$MESS["IMOL_PERM_DELETE"] = "刪除";
$MESS["IMOL_PERM_EDIT"] = "編輯";
$MESS["IMOL_PERM_ERROR"] = "錯誤";
$MESS["IMOL_PERM_RESTRICTION"] = "請升級到<a target = \"_blank \" href= \之https://www.bitrix24.com/prices/x/frices/frices/frices/frices/ver \ \">商業計劃</a>將訪問權限分配給員工。";
$MESS["IMOL_PERM_RESTRICTION_MSGVER_1"] = "請升級到<a target = \"_blank \" href= \"https://www.bitrix24.com/prices/x/frices/x/frices/frices/xhttps://www.bitrix24>主要計劃</ a>分配員工訪問權限以使用開放式渠道。";
$MESS["IMOL_PERM_ROLE"] = "角色";
$MESS["IMOL_PERM_ROLE_CANCEL"] = "取消";
$MESS["IMOL_PERM_ROLE_DELETE"] = "刪除角色";
$MESS["IMOL_PERM_ROLE_DELETE_CONFIRM"] = "您確定要刪除角色嗎？";
$MESS["IMOL_PERM_ROLE_DELETE_ERROR"] = "錯誤刪除角色。";
$MESS["IMOL_PERM_ROLE_LIST"] = "角色";
$MESS["IMOL_PERM_ROLE_OK"] = "好的";
$MESS["IMOL_PERM_SAVE"] = "節省";
