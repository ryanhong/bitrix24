<?php
$MESS["IMOL_PERM_ACCESS_DENIED"] = "訪問權限不足";
$MESS["IMOL_PERM_LICENSING_ERROR"] = "您的計劃不允許管理開放渠道的訪問權限。";
$MESS["IMOL_PERM_MODULE_ERROR"] = "未安裝\“ Open Channels \”模塊";
$MESS["IMOL_PERM_UNKNOWN_ACCESS_CODE"] = "（未知訪問ID）";
$MESS["IMOL_PERM_UNKNOWN_SAVE_ERROR"] = "保存錯誤數據";
