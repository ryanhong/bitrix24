<?php
$MESS["IMOL_MA_ATTACHMENT"] = "附件";
$MESS["IMOL_MA_ATTACHMENT_DISK"] = "駕駛";
$MESS["IMOL_MA_ATTACHMENT_FILE_1"] = "元素文件";
$MESS["IMOL_MA_ATTACHMENT_TYPE"] = "附件類型";
$MESS["IMOL_MA_EMPTY_MESSAGE"] = "缺少“消息文本”參數。";
$MESS["IMOL_MA_IS_SYSTEM"] = "隱藏消息（耳語模式）";
$MESS["IMOL_MA_IS_SYSTEM_DESCRIPTION"] = "發布的消息將無法看到外部聯繫人（耳語模式）";
$MESS["IMOL_MA_MESSAGE"] = "消息文字";
$MESS["IMOL_MA_NO_CHAT"] = "未找到客戶聊天";
$MESS["IMOL_MA_NO_SESSION_CODE"] = "沒有找到連接開放渠道的客戶";
$MESS["IMOL_MA_TIMELINE_ERROR"] = "消息未發送。 ＃error_text＃";
$MESS["IMOL_MA_UNSUPPORTED_DOCUMENT"] = "當前元素不支持此活動類型";
