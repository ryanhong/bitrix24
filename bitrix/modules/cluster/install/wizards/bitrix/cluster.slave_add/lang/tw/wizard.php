<?php
$MESS["CLUWIZ_CANCELSTEP_BUTTONTITLE"] = "關閉";
$MESS["CLUWIZ_CANCELSTEP_CONTENT"] = "巫師已被取消。";
$MESS["CLUWIZ_CANCELSTEP_TITLE"] = "巫師已被取消。";
$MESS["CLUWIZ_CHEKED"] = "檢查。";
$MESS["CLUWIZ_DATABASE_NOT_SUPPORTED"] = "嚮導不支持指定的數據庫類型。";
$MESS["CLUWIZ_FINALSTEP_BUTTONTITLE"] = "結束";
$MESS["CLUWIZ_FINALSTEP_NAME"] = "連接名稱";
$MESS["CLUWIZ_FINALSTEP_TITLE"] = "完成巫師";
$MESS["CLUWIZ_NO_GROUP_ERROR"] = "服務器組未定義。";
$MESS["CLUWIZ_NO_MASTER_ERROR"] = "該服務器組沒有工作主數據庫。";
$MESS["CLUWIZ_NO_MODULE_ERROR"] = "未安裝Web群集模塊。巫師現在將中止。";
$MESS["CLUWIZ_STEP1_CONTENT"] = "嚮導將連接一個新的從數據庫，並驗證主要復制必不可少的主要數據庫的主要參數。<br />";
$MESS["CLUWIZ_STEP1_TITLE"] = "新的從屬數據庫嚮導";
$MESS["CLUWIZ_STEP2_DB_HOST"] = "數據庫連接字符串";
$MESS["CLUWIZ_STEP2_DB_LOGIN"] = "用戶";
$MESS["CLUWIZ_STEP2_DB_NAME"] = "資料庫";
$MESS["CLUWIZ_STEP2_DB_NAME_HINT"] = "（與主數據庫相同的名稱）";
$MESS["CLUWIZ_STEP2_DB_PASSWORD"] = "密碼";
$MESS["CLUWIZ_STEP2_MASTER_CONN"] = "主數據庫連接參數：";
$MESS["CLUWIZ_STEP2_MASTER_HOST"] = "服務器IP或名稱";
$MESS["CLUWIZ_STEP2_MASTER_PORT"] = "港口";
$MESS["CLUWIZ_STEP2_TITLE"] = "從屬數據庫連接參數";
$MESS["CLUWIZ_STEP4_CONN_ERROR"] = "無法建立數據庫連接。請返回上一步並修改連接參數。";
$MESS["CLUWIZ_STEP4_SLAVE_IS_RUNNING"] = "主數據庫已經被複製到指定的數據庫。";
$MESS["CLUWIZ_STEP4_TITLE"] = "數據庫參數驗證";
