<?php
$MESS["CLUWIZ_ALL_DONE1"] = "該模塊已成功傳輸。該數據庫現已在線。";
$MESS["CLUWIZ_ALL_DONE2"] = "該模塊已成功傳輸。數據庫現在準備就緒。";
$MESS["CLUWIZ_CONNECTION_ERROR"] = "連接到數據庫的錯誤。";
$MESS["CLUWIZ_ERROR_ACCESS_DENIED"] = "錯誤！拒絕訪問。";
$MESS["CLUWIZ_INIT"] = "初始化...";
$MESS["CLUWIZ_NOMODULE_ERROR"] = "獲得刪除表的錯誤。";
$MESS["CLUWIZ_QUERY_ERROR"] = "錯誤查詢數據庫。";
$MESS["CLUWIZ_TABLE_PROGRESS"] = "表＃table_name＃：移動行：＃記錄＃。";
