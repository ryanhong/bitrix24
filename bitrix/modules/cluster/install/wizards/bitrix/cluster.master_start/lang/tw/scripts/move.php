<?php
$MESS["CLUWIZ_ALL_DONE"] = "表已成功地轉移。現在正在進行複制（在線）。";
$MESS["CLUWIZ_CONNECTION_ERROR"] = "連接到數據庫的錯誤。";
$MESS["CLUWIZ_ERROR_ACCESS_DENIED"] = "錯誤！拒絕訪問。";
$MESS["CLUWIZ_INIT"] = "初始化...";
$MESS["CLUWIZ_QUERY_ERROR"] = "錯誤查詢數據庫。";
$MESS["CLUWIZ_SITE_OPEN"] = "單擊\“完成\”以使網站的公共部分可供訪問者使用。";
$MESS["CLUWIZ_TABLE_PROGRESS"] = "表：＃table_name＃<br​​ />移動行：＃記錄＃<br />剩下的表：＃表＃";
