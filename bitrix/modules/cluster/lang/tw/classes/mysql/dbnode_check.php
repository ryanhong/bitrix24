<?php
$MESS["CLU_AFTER_CONNECT_D7_MSG"] = "必須配置主數據庫和系統環境，以使得沒有文件php_interface/after_connect.php。";
$MESS["CLU_AFTER_CONNECT_MSG"] = "必須配置主數據庫和系統環境，以使得沒有文件php_interface/after_connect_d7.php。";
$MESS["CLU_AFTER_CONNECT_WIZREC"] = "正確配置系統並確保網站正確運行。然後，刪除文件並再次運行嚮導。";
$MESS["CLU_AUTO_INCREMENT_INCREMENT_ERR_MSG"] = "具有ID＃＃node_id＃的服務器具有auto_increment_increment的無效值。它應將其設置為＃value＃（當前值：auto_increment_increment：＃current＃）。";
$MESS["CLU_AUTO_INCREMENT_INCREMENT_NODE_ERR_MSG"] = "新服務器具有auto_increment_increment的無效值。它應將其設置為＃value＃（當前值：auto_increment_increment：＃current＃）。";
$MESS["CLU_AUTO_INCREMENT_INCREMENT_OK_MSG"] = "應將auto_increment_increment的值設置為群集服務器的＃值＃。";
$MESS["CLU_AUTO_INCREMENT_INCREMENT_WIZREC"] = "將auto_increment_increment設置為my.cnf中的＃值＃。然後，重新啟動mysql，然後單擊\“ next \”。";
$MESS["CLU_AUTO_INCREMENT_OFFSET_ERR_MSG"] = "具有ID＃＃node_id＃的服務器具有auto_increment_offset的無效值。不得將其設置為＃當前＃。";
$MESS["CLU_AUTO_INCREMENT_OFFSET_NODE_ERR_MSG"] = "新服務器具有auto_increment_offset的無效值。不得將其設置為＃當前＃。";
$MESS["CLU_AUTO_INCREMENT_OFFSET_OK_MSG"] = "群集服務器的\“ auto_increment_offset \”值不得帶來碰撞。";
$MESS["CLU_AUTO_INCREMENT_OFFSET_WIZREC"] = "將my.cnf中的auto_increment_offset設置為與其他服務器不同的值。重新啟動mysql，然後單擊\“ next \”。";
$MESS["CLU_CHARSET_MSG"] = "服務器，數據庫，連接和客戶端必須設置以使用相同的字符集。";
$MESS["CLU_CHARSET_WIZREC"] = "重新配置MySQL參數：<br /> pronage_set_server（當前：＃farne_set_server＃），<br /> prinne_set_set_database（當前：＃farne_set_set_set_set_database＃），<br /> <br /> <br /> tarne_set_client（當前：＃farne_set_client＃ ）。<br />確保系統運行正確並再次運行嚮導。";
$MESS["CLU_COLLATION_MSG"] = "服務器，數據庫和連接必須設置以使用相同的排序規則。";
$MESS["CLU_COLLATION_WIZREC"] = "重新配置MySQL參數：<br /> colation_server（當前：＃collat​​ion_server＃），<br /> collat​​ion_database（當前：＃collat​​ion_database＃），<br /> <br /> <br />確保系統運行正確並再次運行嚮導。";
$MESS["CLU_FLUSH_ON_COMMIT_MSG"] = "為了更好地複制，使用InnoDB，應將參數Innodb_flush_log_at_trx_commit設置為1（當前：＃Innodb_flush_log_at_t_trx_trx_commit＃）。";
$MESS["CLU_LOG_BIN_MSG"] = "必須為主服務器啟用日誌記錄（當前log-bin：＃log-bin＃）。";
$MESS["CLU_LOG_BIN_NODE_MSG"] = "新服務器應啟用日誌記錄（當前值：log-bin：＃log-bin＃）。";
$MESS["CLU_LOG_BIN_WIZREC"] = "將參數log-bin = mysql-bin添加到my.cnf。然後，重新啟動mysql，然後單擊\“ next \”。";
$MESS["CLU_LOG_SLAVE_UPDATES_MSG"] = "服務器ID＃＃node_id＃未從主數據庫記錄請求。這可能需要這項服務器將已連接到它的從數據庫。 Log-Slave-Updates的當前值是：＃log-slave-updates＃。";
$MESS["CLU_LOG_SLAVE_UPDATES_OK_MSG"] = "集群的主服務器必須從另一個主數據庫啟用記錄事件。";
$MESS["CLU_LOG_SLAVE_UPDATES_WIZREC"] = "將\“ log-slave-updates \”設置為my.cnf中的＃value＃。然後，重新啟動mysql，然後單擊\“ next \”。";
$MESS["CLU_MASTER_CHARSET_MSG"] = "必須設置主服務器和新連接以使用相同的Charset和排序規則。";
$MESS["CLU_MASTER_CHARSET_WIZREC"] = "重新配置MySQL參數：<br /> pronage_set_server（當前：＃farne_set_server＃），<br /> collat​​ion_server（當前：＃collat​​ion_server＃）。<br />確保系統正確並運行wizardard wizardard 。";
$MESS["CLU_MASTER_CONNECT_ERROR"] = "主數據庫連接錯誤：";
$MESS["CLU_MASTER_STATUS_MSG"] = "檢查復制狀態的特權不足。";
$MESS["CLU_MASTER_STATUS_WIZREC"] = "執行查詢：＃SQL＃。";
$MESS["CLU_MAX_ALLOWED_PACKET_MSG"] = "從屬數據庫的\“ max_allowed_pa​​cket \”值不得低於主數據庫的值。";
$MESS["CLU_MAX_ALLOWED_PACKET_WIZREC"] = "在my.cnf中設置\“ max_allowed_pa​​cket \”值，然後重新啟動mysql。";
$MESS["CLU_NOT_MASTER"] = "實際上，指定為主要的數據庫並非如此。";
$MESS["CLU_RELAY_LOG_ERR_MSG"] = "在服務器上使用ID＃＃node_id＃（當前值：realay-log：＃relay-log＃）禁用中繼log。";
$MESS["CLU_RELAY_LOG_OK_MSG"] = "必須為群集服務器（\“ realay-log \”參數）啟用繼電器-log。";
$MESS["CLU_RELAY_LOG_WIZREC"] = "在文件my.cnf（示例：mySQLD-relay-bin）中設置參數\“ realay-log \”，然後重新啟動mySQL。";
$MESS["CLU_RUNNING_SLAVE"] = "指定的數據庫已經開始復制。連接無法建立。";
$MESS["CLU_SAME_DATABASE"] = "該數據庫似乎與主要數據庫相同。連接無法建立。";
$MESS["CLU_SERVER_ID_MSG"] = "每個群集節點必須具有唯一的ID（當前服務器ID：＃Server-ID＃）。";
$MESS["CLU_SERVER_ID_WIZREC"] = "添加並在my.cnf中設置參數server-id。然後，重新啟動mysql，然後單擊\“ next \”。";
$MESS["CLU_SERVER_ID_WIZREC1"] = "缺少參數服務器ID。";
$MESS["CLU_SERVER_ID_WIZREC2"] = "具有該服務器ID的數據庫服務器已經在模塊中註冊。";
$MESS["CLU_SKIP_NETWORKING_MSG"] = "必須通過網絡訪問主服務器（當前Skip-NetWorking：＃Skip-Networking＃）。";
$MESS["CLU_SKIP_NETWORKING_NODE_MSG"] = "應為新服務器啟用網絡（當前值：Skip-NetWorking：＃Skip-Networking＃）。";
$MESS["CLU_SKIP_NETWORKING_WIZREC"] = "刪除或評論my.cnf中的參數跳過網絡。然後，重新啟動mysql，然後單擊\“ next \”。";
$MESS["CLU_SLAVE_PRIVILEGE_MSG"] = "用戶特權複製。";
$MESS["CLU_SLAVE_RELAY_LOG_MSG"] = "未指定參數\“ Relay-Log \”。如果服務器主機名更改，則復制將中止。";
$MESS["CLU_SLAVE_VERSION_MSG"] = "從屬數據庫的MySQL版本（＃從version＃）至少必須是＃必要交換＃。";
$MESS["CLU_SQL_MSG"] = "用戶必須有權創建和刪除表；插入，選擇，修改和刪除數據。";
$MESS["CLU_SQL_WIZREC"] = "權限不足。無法執行以下SQL查詢：＃sql_erorrs_list＃";
$MESS["CLU_SYNC_BINLOGDODB_MSG"] = "只能將一個數據庫設置為複制。";
$MESS["CLU_SYNC_BINLOGDODB_WIZREC"] = "將參數binlog-do-db =＃數據庫＃添加到my.cnf。然後，重新啟動mysql，然後單擊\“ next \”。";
$MESS["CLU_SYNC_BINLOG_MSG"] = "為了更好地使用InnoDB，請使用InnoDB時，應將參數Sync_binlog設置為1（當前：＃Sync_binlog＃）。";
$MESS["CLU_VERSION_MSG"] = "從屬數據庫的MySQL版本（＃從version＃）不得低於主數據庫版本（＃master-version＃）。";
$MESS["CLU_VERSION_WIZREC"] = "更新您的MySQL，然後再次啟動嚮導。";
