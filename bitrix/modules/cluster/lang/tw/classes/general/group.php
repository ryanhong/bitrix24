<?php
$MESS["CLU_GROUP_EMPTY_NAME"] = "服務器組名稱未指定。";
$MESS["CLU_GROUP_HAS_CACHESERVER"] = "服務器組具有緩存服務器。";
$MESS["CLU_GROUP_HAS_DBNODE"] = "服務器組中有數據庫。";
$MESS["CLU_GROUP_HAS_WEBNODE"] = "服務器組中有Web服務器。";
