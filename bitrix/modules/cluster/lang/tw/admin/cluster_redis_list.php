<?php
$MESS["CLU_REDIS_CLUSTER_TITLE"] = "REDIS群集";
$MESS["CLU_REDIS_DISABLED"] = "REDIS被禁用。";
$MESS["CLU_REDIS_LIST_ADD"] = "添加";
$MESS["CLU_REDIS_LIST_ADD_TITLE"] = "添加redis連接";
$MESS["CLU_REDIS_LIST_DELETE"] = "刪除";
$MESS["CLU_REDIS_LIST_DELETE_CONF"] = "刪除連接？";
$MESS["CLU_REDIS_LIST_EDIT"] = "編輯";
$MESS["CLU_REDIS_LIST_FLAG"] = "地位";
$MESS["CLU_REDIS_LIST_HOST"] = "伺服器";
$MESS["CLU_REDIS_LIST_ID"] = "ID";
$MESS["CLU_REDIS_LIST_NOTE"] = "<p>您必須配置服務器文件系統同步才能啟動多個Web服務器。</p>
<p>建議使用REDIS同步文件時減少服務器加載。 REDIS將緩存保持在內存而不是磁盤上。</p>
";
$MESS["CLU_REDIS_LIST_REFRESH"] = "重新整理";
$MESS["CLU_REDIS_LIST_START_USING"] = "從事";
$MESS["CLU_REDIS_LIST_STATUS"] = "細節";
$MESS["CLU_REDIS_LIST_STOP_USING"] = "脫離接觸";
$MESS["CLU_REDIS_LIST_TITLE"] = "REDIS連接";
$MESS["CLU_REDIS_LIST_WARNING_NO_CACHE"] = "注意力！緩存被禁用。通過在.settings.php文件中與Redis Connections之一或編輯參數進行啟用。";
$MESS["CLU_REDIS_LIST_WEIGHT"] = "用法 （％）";
$MESS["CLU_REDIS_NOCONNECTION"] = "無連接";
$MESS["CLU_REDIS_NO_EXTENTION"] = "未安裝REDIS PHP擴展名。";
$MESS["CLU_REDIS_UPTIME"] = "正常運行時間";
