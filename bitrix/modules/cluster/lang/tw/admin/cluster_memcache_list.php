<?php
$MESS["CLU_MEMCACHE_DISABLED"] = "memcache被禁用。";
$MESS["CLU_MEMCACHE_LIST_ADD"] = "添加";
$MESS["CLU_MEMCACHE_LIST_ADD_TITLE"] = "添加一個備用連接";
$MESS["CLU_MEMCACHE_LIST_DELETE"] = "刪除";
$MESS["CLU_MEMCACHE_LIST_DELETE_CONF"] = "刪除連接？";
$MESS["CLU_MEMCACHE_LIST_EDIT"] = "編輯";
$MESS["CLU_MEMCACHE_LIST_FLAG"] = "旗幟";
$MESS["CLU_MEMCACHE_LIST_HOST"] = "伺服器";
$MESS["CLU_MEMCACHE_LIST_ID"] = "ID";
$MESS["CLU_MEMCACHE_LIST_NOTE"] = "<p>需要同步多個服務器的文件系統才能正確函數。</p>
強烈建議使用memcached來減少文件同步壓力和服務器加載，因為它將緩存文件存儲在內存而不是物理文件中。</p>";
$MESS["CLU_MEMCACHE_LIST_REFRESH"] = "重新整理";
$MESS["CLU_MEMCACHE_LIST_START_USING"] = "使用Memcached";
$MESS["CLU_MEMCACHE_LIST_STATUS"] = "地位";
$MESS["CLU_MEMCACHE_LIST_STOP_USING"] = "停止使用Memcached";
$MESS["CLU_MEMCACHE_LIST_TITLE"] = "備忘錄的連接";
$MESS["CLU_MEMCACHE_LIST_WARNING_NO_CACHE"] = "注意力！緩存關閉。要啟用，請在.settings.php中使用Memcached Connection或編輯參數。";
$MESS["CLU_MEMCACHE_LIST_WEIGHT"] = "重量 （％）";
$MESS["CLU_MEMCACHE_NOCONNECTION"] = "斷開連接";
$MESS["CLU_MEMCACHE_NO_EXTENTION"] = "未安裝php擴展名\“ memcached \”。";
$MESS["CLU_MEMCACHE_UPTIME"] = "正常運行時間";
