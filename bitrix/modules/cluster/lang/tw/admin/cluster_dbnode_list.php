<?php
$MESS["CLU_DBNODE_LIST_ACTIVE"] = "積極的";
$MESS["CLU_DBNODE_LIST_ADD"] = "添加新數據庫";
$MESS["CLU_DBNODE_LIST_ADD_TITLE1"] = "運行數據庫連接嚮導";
$MESS["CLU_DBNODE_LIST_ADD_TITLE2"] = "添加新數據庫連接";
$MESS["CLU_DBNODE_LIST_DB_HOST"] = "伺服器";
$MESS["CLU_DBNODE_LIST_DB_LOGIN"] = "用戶";
$MESS["CLU_DBNODE_LIST_DB_NAME"] = "資料庫";
$MESS["CLU_DBNODE_LIST_DELETE"] = "刪除";
$MESS["CLU_DBNODE_LIST_DELETE_CONF"] = "刪除連接？";
$MESS["CLU_DBNODE_LIST_DESCRIPTION"] = "描述";
$MESS["CLU_DBNODE_LIST_EDIT"] = "編輯";
$MESS["CLU_DBNODE_LIST_FLAG"] = "狀態";
$MESS["CLU_DBNODE_LIST_ID"] = "ID";
$MESS["CLU_DBNODE_LIST_MODULES"] = "模塊";
$MESS["CLU_DBNODE_LIST_NAME"] = "姓名";
$MESS["CLU_DBNODE_LIST_NOTE1"] = "某些模塊的數據表可以移至單獨的數據庫，從而在幾個服務器之間分配數據庫負載。要移動表，請先單擊\“添加新數據庫\”。添加數據庫後，您必須使用以下方法之一傳輸模塊數據。</p>
<p>第一種方法僅適用於mySQL。在列表中找到所需的數據庫，然後在菜單操作中選擇命令\“使用數據庫\”。然後，按照嚮導說明。</p>
<p>另外，您可以卸載模塊並重新安裝。在嚮導的第一步，選擇模塊將使用的數據庫。當前現有的數據庫表將不會傳輸。</p>
以下模塊支持聚類：";
$MESS["CLU_DBNODE_LIST_NOTE2"] = "<p> <i>垂直碎片涉及將選定模塊的查詢委派分配在兩個或更多數據庫之間。每個選定的模塊都有指定的數據庫連接。</i> </p>
<p> <i>水平碎片意味著在單獨的數據庫中分發統一數據（例如用戶配置文件）。</i> </p>";
$MESS["CLU_DBNODE_LIST_REFRESH"] = "重新整理";
$MESS["CLU_DBNODE_LIST_START_USING_DB"] = "使用數據庫";
$MESS["CLU_DBNODE_LIST_STATUS"] = "地位";
$MESS["CLU_DBNODE_LIST_STOP_USING_DB"] = "停止使用此數據庫";
$MESS["CLU_DBNODE_LIST_TITLE"] = "垂直分片：模塊數據庫";
$MESS["CLU_DBNODE_NOCONNECTION"] = "斷開連接";
$MESS["CLU_DBNODE_UPTIME"] = "正常運行時間";
$MESS["CLU_DBNODE_UPTIME_UNKNOWN"] = "未知";
