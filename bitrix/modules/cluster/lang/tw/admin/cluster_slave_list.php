<?php
$MESS["CLU_MAIN_LOAD"] = "最小負載";
$MESS["CLU_SLAVE_BACKUP"] = "備份";
$MESS["CLU_SLAVE_LIST_ADD"] = "添加從數據庫";
$MESS["CLU_SLAVE_LIST_ADD_TITLE"] = "運行新的從屬數據庫嚮導";
$MESS["CLU_SLAVE_LIST_BEHIND"] = "潛伏期（SEC）";
$MESS["CLU_SLAVE_LIST_DB_HOST"] = "伺服器";
$MESS["CLU_SLAVE_LIST_DB_LOGIN"] = "用戶";
$MESS["CLU_SLAVE_LIST_DB_NAME"] = "資料庫";
$MESS["CLU_SLAVE_LIST_DELETE"] = "刪除";
$MESS["CLU_SLAVE_LIST_DELETE_CONF"] = "刪除連接？";
$MESS["CLU_SLAVE_LIST_DESCRIPTION"] = "描述";
$MESS["CLU_SLAVE_LIST_EDIT"] = "編輯";
$MESS["CLU_SLAVE_LIST_FLAG"] = "狀態";
$MESS["CLU_SLAVE_LIST_ID"] = "ID";
$MESS["CLU_SLAVE_LIST_MASTER_ADD"] = "添加主/從數據庫";
$MESS["CLU_SLAVE_LIST_MASTER_ADD_TITLE"] = "運行新的主/從數據庫嚮導";
$MESS["CLU_SLAVE_LIST_NAME"] = "姓名";
$MESS["CLU_SLAVE_LIST_NOTE"] = "<p>數據庫複製是同一數據庫的多個副本的創建和維護，該數據庫提供了兩個主要功能：</p>
<p>
1）在主數據庫和一個或多個從屬數據庫之間分配負載； <br>
2）將奴隸用作熱待機。<br>
</p>
<p>很重要！<br>
 - 僅使用具有最快連接性的獨立服務器進行複制。<br>
 - 複製過程首先複製數據庫內容。在這段時間內，網站的公共部分將不可用，但是控制面板仍可訪問。複製過程中發生的任何數據修改都可能影響網站的適當操作。<br>
</p>
<p>複製指南<br>
步驟1：通過單擊\“添加從數據庫\”來啟動嚮導。嚮導將檢查服務器配置並建議您添加從屬數據庫。<br>
步驟2：在列表中查找所需的數據庫，然後在“操作”菜單中選擇命令\“使用數據庫\”。<br>
步驟3：按照嚮導說明。<br>
</p>
";
$MESS["CLU_SLAVE_LIST_PAUSE"] = "暫停";
$MESS["CLU_SLAVE_LIST_REFRESH"] = "重新整理";
$MESS["CLU_SLAVE_LIST_RESUME"] = "恢復";
$MESS["CLU_SLAVE_LIST_SKIP_SQL_ERROR"] = "忽略錯誤";
$MESS["CLU_SLAVE_LIST_SKIP_SQL_ERROR_ALT"] = "忽略單個SQL錯誤並繼續複製";
$MESS["CLU_SLAVE_LIST_START_USING_DB"] = "使用數據庫";
$MESS["CLU_SLAVE_LIST_STATUS"] = "地位";
$MESS["CLU_SLAVE_LIST_STOP"] = "廢棄數據庫";
$MESS["CLU_SLAVE_LIST_TITLE"] = "從數據庫";
$MESS["CLU_SLAVE_LIST_WEIGHT"] = "重量 （％）";
$MESS["CLU_SLAVE_NOCONNECTION"] = "斷開連接";
$MESS["CLU_SLAVE_UPTIME"] = "正常運行時間";
