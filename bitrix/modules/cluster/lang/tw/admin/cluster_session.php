<?php
$MESS["CLU_SESSION_DB_BUTTON_OFF"] = "不要將會話數據存儲在安全模塊數據庫中";
$MESS["CLU_SESSION_DB_BUTTON_ON"] = "將會話數據存儲在安全模塊數據庫中";
$MESS["CLU_SESSION_DB_OFF"] = "會話數據當前未存儲在安全模塊數據庫中。";
$MESS["CLU_SESSION_DB_ON"] = "會話數據存儲在安全模塊數據庫中。";
$MESS["CLU_SESSION_DB_WARNING"] = "注意力！打開或關閉會話模式將導致當前授權的用戶丟失授權（會話數據將被銷毀）。";
$MESS["CLU_SESSION_NOTE"] = "<p> Web服務器聚類要求您正確配置會話支持。</p>
<p>最常用的負載分配策略是：</p>
<p> 1）將訪問者會話分配給Web服務器以處理所有其他請求。</p>
<p> 2）允許通過不同的Web服務器處理Web會話的不同命中。<br>
策略（2）的強制性先決條件是必須配置安全模塊以存儲會話數據。</p>";
$MESS["CLU_SESSION_NO_SECURITY"] = "需要\“主動保護\”模塊。";
$MESS["CLU_SESSION_SAVEDB_TAB"] = "數據庫中的會話";
$MESS["CLU_SESSION_SAVEDB_TAB_TITLE"] = "在數據庫中配置會話數據的存儲";
$MESS["CLU_SESSION_SESSID_WARNING"] = "會話ID與主動保護模塊不兼容。帶有session_id（）函數返回的標識符不得超過32個字符，並且僅包含拉丁字母或數字。";
$MESS["CLU_SESSION_TITLE"] = "將會話存儲在數據庫中";
