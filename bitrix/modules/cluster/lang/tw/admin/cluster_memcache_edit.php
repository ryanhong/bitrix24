<?php
$MESS["CLU_MEMCACHE_DISABLED"] = "memcache被禁用。";
$MESS["CLU_MEMCACHE_EDIT_EDIT_TITLE"] = "編輯備忘錄連接";
$MESS["CLU_MEMCACHE_EDIT_HOST"] = "伺服器";
$MESS["CLU_MEMCACHE_EDIT_ID"] = "ID";
$MESS["CLU_MEMCACHE_EDIT_MENU_LIST"] = "列表";
$MESS["CLU_MEMCACHE_EDIT_MENU_LIST_TITLE"] = "備忘錄的連接";
$MESS["CLU_MEMCACHE_EDIT_NEW_TITLE"] = "新的memcached連接";
$MESS["CLU_MEMCACHE_EDIT_PORT"] = "港口";
$MESS["CLU_MEMCACHE_EDIT_SAVE_ERROR"] = "錯誤保存連接參數。";
$MESS["CLU_MEMCACHE_EDIT_TAB"] = "參數";
$MESS["CLU_MEMCACHE_EDIT_TAB_TITLE"] = "後備服務器連接參數";
$MESS["CLU_MEMCACHE_EDIT_WARNING"] = "注意力！已經存在一個備用的服務器連接。某些版本的memcached Extension具有錯誤，如果存在多個連接，則導致性能顯著降低。使用<a href= \“#link# \">性能監視器</a>在添加另一個連接之前和之後評估系統的性能。如果發生重大差異，請嘗試安裝不同版本的Memcached。";
$MESS["CLU_MEMCACHE_EDIT_WEIGHT"] = "負載，％（0..100）";
$MESS["CLU_MEMCACHE_NO_EXTENTION"] = "未安裝php擴展名\“ memcached \”。";
