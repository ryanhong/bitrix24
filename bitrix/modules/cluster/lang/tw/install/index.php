<?php
$MESS["CLU_GROUP_NO_ONE"] = "小組＃1";
$MESS["CLU_INSTALL_TITLE"] = "安裝Web群集支持模塊";
$MESS["CLU_MAIN_DATABASE"] = "主數據庫";
$MESS["CLU_MODULE_DESCRIPTION"] = "Web群集支持模塊。";
$MESS["CLU_MODULE_NAME"] = "網絡集群";
$MESS["CLU_UNINSTALL_TITLE"] = "卸載Web群集支持模塊";
