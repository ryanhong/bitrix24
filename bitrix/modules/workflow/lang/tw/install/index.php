<?php
$MESS["FLOW_DENIED"] = "否認";
$MESS["FLOW_INSTALL_DRAFT"] = "草稿";
$MESS["FLOW_INSTALL_PUBLISHED"] = "出版";
$MESS["FLOW_INSTALL_READY"] = "需要批准";
$MESS["FLOW_INSTALL_TITLE"] = "工作流模塊安裝";
$MESS["FLOW_MODIFY"] = "在工作流程中編輯文檔";
$MESS["FLOW_MODULE_DESCRIPTION"] = "組織和處理站點文檔的模塊。";
$MESS["FLOW_MODULE_NAME"] = "工作流程";
$MESS["FLOW_READ"] = "只讀";
$MESS["FLOW_WRITE"] = "完全訪問";
