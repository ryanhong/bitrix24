<?php
$MESS["FLOW_ADMIN"] = "工作流管理員組：";
$MESS["FLOW_CLEAR"] = "立即刪除";
$MESS["FLOW_DAYS_AFTER_PUBLISHING"] = "保留最後一個文件版本的天數：<br>（使用負值以保持永恆）：";
$MESS["FLOW_HISTORY_COPIES"] = "最大文件版本：";
$MESS["FLOW_HISTORY_DAYS"] = "保留先前文件版本的天數：<br>（使用負值以保持永恆）：";
$MESS["FLOW_HISTORY_SIMPLE_EDITING"] = "保留直接編輯的文件的版本歷史記錄（工作流模塊之外）";
$MESS["FLOW_MAX_LOCK"] = "鎖定文件進行編輯（分鐘）：";
$MESS["FLOW_RESET"] = "重置";
$MESS["FLOW_SAVE"] = "節省";
$MESS["FLOW_USE_HTML_EDIT"] = "使用Visual HTML編輯器";
$MESS["MAIN_RESTORE_DEFAULTS"] = "默認設置";
