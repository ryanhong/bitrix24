<?php
$MESS["FLOW_ACCESS_DENIED_FOLDER"] = "您無權寫入＃文件名＃。";
$MESS["FLOW_ACCESS_DENIED_FOR_FILE_WRITE"] = "您無權創建文件\“＃fileName＃\”（您應該具有\“ Write \”訪問此文件或目錄的訪問）。";
$MESS["FLOW_ACCESS_DENIED_PHP_VIEW"] = "您無權查看可執行文件。";
$MESS["FLOW_CAN_NOT_WRITE_FILE"] = "無法保存文件\“＃文件名＃\”。";
$MESS["FLOW_CURRENT_STATUS"] = "當前狀態：";
$MESS["FLOW_DOCUMENT_NOT_PUBLISHED"] = "該文檔無法發布。";
$MESS["FLOW_ERROR"] = "錯誤！";
$MESS["FLOW_PANEL_CREATE_ALT"] = "通過工作流在當前部分中創建一個新頁面";
$MESS["FLOW_PANEL_CREATE_WITH_WF"] = "通過工作流程";
$MESS["FLOW_PANEL_EDIT_ALT"] = "通過工作流程編輯當前頁面";
$MESS["FLOW_PANEL_EDIT_WITH_WF"] = "通過工作流程";
$MESS["FLOW_PANEL_HISTORY"] = "歷史";
$MESS["FLOW_PANEL_HISTORY_ALT"] = "當前頁面的變化歷史記錄";
