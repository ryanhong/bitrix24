<?php
$MESS["FLOW_COMPARE"] = "比較";
$MESS["FLOW_COMPARE_ALERT"] = "選擇兩個文檔進行比較。";
$MESS["FLOW_DATE_MODIFY"] = "修改的";
$MESS["FLOW_DELETE"] = "刪除";
$MESS["FLOW_DELETE_CONFIRM"] = "您確定要從文檔歷史記錄中刪除記錄嗎？";
$MESS["FLOW_DOCUMENT"] = "文檔ID";
$MESS["FLOW_FILENAME"] = "文件名";
$MESS["FLOW_F_BODY"] = "內容";
$MESS["FLOW_F_DATE_MODIFY"] = "修改的";
$MESS["FLOW_F_DOCUMENT"] = "文檔ID";
$MESS["FLOW_F_FILENAME"] = "文件名";
$MESS["FLOW_F_LOGIC"] = "字段之間的邏輯";
$MESS["FLOW_F_MODIFIED_BY"] = "由（ID）修改";
$MESS["FLOW_F_STATUS"] = "文件狀態";
$MESS["FLOW_F_TITLE"] = "標題";
$MESS["FLOW_MODIFIED_BY"] = "修改";
$MESS["FLOW_PAGES"] = "變化";
$MESS["FLOW_PAGE_TITLE"] = "改變歷史";
$MESS["FLOW_RECORDS_LIST"] = "文件列表";
$MESS["FLOW_SITE_ID"] = "網站";
$MESS["FLOW_STATUS"] = "地位";
$MESS["FLOW_STATUS_ALT"] = "查看狀態信息";
$MESS["FLOW_TITLE"] = "標題";
$MESS["FLOW_USER_ALT"] = "查看用戶信息";
$MESS["FLOW_VIEW"] = "看法";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "檢查：";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "刪除";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "選擇：";
$MESS["MAIN_ALL"] = "（全部）";
$MESS["MAIN_FIND"] = "尋找";
$MESS["MAIN_FIND_TITLE"] = "輸入文本以搜索";
