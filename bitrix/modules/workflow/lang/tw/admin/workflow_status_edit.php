<?php
$MESS["FLOW_ACTIVE"] = "積極的：";
$MESS["FLOW_DELETE_STATUS"] = "刪除當前狀態";
$MESS["FLOW_DELETE_STATUS_CONFIRM"] = "您確定要刪除此狀態嗎？";
$MESS["FLOW_DESCRIPTION"] = "描述：";
$MESS["FLOW_DOCUMENTS"] = "此狀態中的文檔：";
$MESS["FLOW_DOCUMENTS_ALT"] = "查看此狀態中的文檔";
$MESS["FLOW_EDIT_RECORD"] = "狀態的偏好";
$MESS["FLOW_EDIT_RIGHTS"] = "允許編輯和保存<br>此狀態中的文檔：";
$MESS["FLOW_ERROR"] = "錯誤！";
$MESS["FLOW_MOVE_RIGHTS"] = "允許將此狀態設置為文檔：";
$MESS["FLOW_NEW_RECORD"] = "新狀態";
$MESS["FLOW_NEW_STATUS"] = "創建新狀態";
$MESS["FLOW_NOTIFY"] = "通知所有允許的用戶<br>編輯或查看文檔進入此狀態時";
$MESS["FLOW_RECORDS_LIST"] = "狀態列表";
$MESS["FLOW_SORTING"] = "排序索引：";
$MESS["FLOW_TIMESTAMP"] = "修改的：";
$MESS["FLOW_TITLE"] = "標題：";
$MESS["FLOW_VIEW"] = "允許查看此狀態中的文檔";
