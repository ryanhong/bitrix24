<?php
$MESS["FLOW_MENU_DOCUMENTS"] = "文件";
$MESS["FLOW_MENU_DOCUMENTS_ALT"] = "工作流處理中的文檔";
$MESS["FLOW_MENU_HISTORY"] = "歷史";
$MESS["FLOW_MENU_HISTORY_ALT"] = "文檔更改歷史記錄";
$MESS["FLOW_MENU_MAIN"] = "工作流程";
$MESS["FLOW_MENU_MAIN_TITLE"] = "文件管理";
$MESS["FLOW_MENU_STAGE"] = "狀態";
$MESS["FLOW_MENU_STAGE_ALT"] = "文檔的可能狀態";
