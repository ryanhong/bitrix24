<?php
$MESS["DW_NO_WEBRTC"] = "您的瀏覽器不支持視頻通話。";
$MESS["DW_PUT_USER_ID"] = "輸入user_id";
$MESS["DW_VIDEO_ANSWER"] = "回答";
$MESS["DW_VIDEO_CALL"] = "視訊通話";
$MESS["DW_VIDEO_DECLINE"] = "衰退";
$MESS["DW_VIDEO_WAIT"] = "等待答案";
$MESS["DW_WINDOW_RELOAD"] = "如果您離開此頁面，通話將結束！";
