<?php
$MESS["NM_DOWNTEXT"] = "釋放刷新";
$MESS["NM_EMPTY"] = "沒有新的通知";
$MESS["NM_FOLD"] = "坍塌";
$MESS["NM_FORMAT_DATE"] = "G：i A，D f y";
$MESS["NM_FORMAT_TIME"] = "G：我";
$MESS["NM_LOADTEXT"] = "重新加載...";
$MESS["NM_MORE"] = "細節";
$MESS["NM_MORE_USERS"] = "和＃計數＃更多用戶（S）";
$MESS["NM_PULLTEXT"] = "拉刷新";
$MESS["NM_SYSTEM_USER"] = "系統消息";
$MESS["NM_TITLE"] = "通知";
$MESS["NM_TITLE_2"] = "更新...";
$MESS["NM_UNFOLD"] = "擴張";
