<?php
$MESS["IM_CHAT_TYPE_CHAT_NEW"] = "私人聊天";
$MESS["IM_CHAT_TYPE_OPEN_NEW"] = "公眾聊天";
$MESS["IM_CREATE_API_ERROR"] = "錯誤創建聊天。請稍後再試。";
$MESS["IM_CREATE_CONNECTION_ERROR"] = "連接到Bitrix24的錯誤。請檢查網絡連接。";
$MESS["IM_DEPARTMENT_START"] = "輸入部門名稱以啟動搜索。";
