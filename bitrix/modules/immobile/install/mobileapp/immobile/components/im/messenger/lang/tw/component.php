<?php
$MESS["IMMOBILE_MESSENGER_DIALOG_ACCESS_ERROR_TEXT"] = "要求您的同事將您添加到此聊天中";
$MESS["IMMOBILE_MESSENGER_DIALOG_ACCESS_ERROR_TITLE"] = "這個聊天是私人的";
$MESS["IMMOBILE_MESSENGER_HEADER"] = "聊天和電話";
$MESS["IMMOBILE_MESSENGER_HEADER_CONNECTION"] = "連接...";
$MESS["IMMOBILE_MESSENGER_HEADER_NETWORK_WAITING"] = "等待網絡...";
$MESS["IMMOBILE_MESSENGER_HEADER_SYNC"] = "更新...";
$MESS["IMMOBILE_MESSENGER_REFRESH_ERROR"] = "由於可能的Internet連接問題，我們無法連接到您的Bitrix24。我們將在短短幾秒鐘內再次嘗試。";
$MESS["IMMOBILE_MESSENGER_UPDATE_FOR_NEW_FEATURES_HINT"] = "立即更新該應用並享受新的Bitrix24功能";
