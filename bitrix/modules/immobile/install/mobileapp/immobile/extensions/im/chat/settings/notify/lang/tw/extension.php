<?php
$MESS["SE_NOTIFY_CATEGORY_IM_TITLE"] = "聊天和通知";
$MESS["SE_NOTIFY_DEFAULT_TITLE"] = "設定";
$MESS["SE_NOTIFY_DESC"] = "推動通知和計數器";
$MESS["SE_NOTIFY_EMPTY"] = "無法配置此類別。";
$MESS["SE_NOTIFY_LOADING"] = "載入中...";
$MESS["SE_NOTIFY_MAIN_COUNTER_DETAIL_DESC"] = "選擇您希望在應用程序圖標的摘要計數器中看到其計數器的工具";
$MESS["SE_NOTIFY_MAIN_COUNTER_TITLE"] = "應用計數器";
$MESS["SE_NOTIFY_NOTIFY_TYPES_TITLE"] = "通知";
$MESS["SE_NOTIFY_PUSH_TITLE"] = "允許通知";
$MESS["SE_NOTIFY_SMART_FILTER_DESC"] = "智能過濾器將使通知減少侵入性。瀏覽Internet或使用桌面應用程序時，您將不會收到通知，而無需暫停。";
$MESS["SE_NOTIFY_SMART_FILTER_TITLE"] = "使用智能過濾器";
$MESS["SE_NOTIFY_TITLE"] = "通知";
