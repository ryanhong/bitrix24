<?php
$MESS["SEARCH"] = "搜尋...";
$MESS["SEARCH_BACK"] = "返回目錄";
$MESS["SEARCH_CATEGORY_CHAT"] = "聊天";
$MESS["SEARCH_CATEGORY_DEPARTMENT_USER"] = "僱員";
$MESS["SEARCH_CATEGORY_LINE"] = "開放頻道";
$MESS["SEARCH_CATEGORY_USER"] = "僱員";
$MESS["SEARCH_EMPTY"] = "\ \“＃text＃\”沒有結果";
$MESS["SEARCH_MORE"] = "裝載更多...";
$MESS["SEARCH_MORE_READY"] = "顯示找到的項目。裝載更多...";
