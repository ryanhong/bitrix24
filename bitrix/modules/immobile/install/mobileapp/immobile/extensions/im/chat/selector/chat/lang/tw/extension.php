<?php
$MESS["MOBILE_EXT_CHAT_SELECTOR_BUTTON_MORE"] = "更多的";
$MESS["MOBILE_EXT_CHAT_SELECTOR_BUTTON_SEARCH_NETWORK_MSGVER_1"] = "搜索Bitrix24網絡";
$MESS["MOBILE_EXT_CHAT_SELECTOR_SECTION_COMMON_CHAT_USER_TITLE"] = "用戶聊天";
$MESS["MOBILE_EXT_CHAT_SELECTOR_SECTION_COMMON_TITLE"] = "實時聊天";
$MESS["MOBILE_EXT_CHAT_SELECTOR_SECTION_CUSTOM_TITLE"] = "僱員";
$MESS["MOBILE_EXT_CHAT_SELECTOR_SECTION_NETWORK_TITLE"] = "外部開放通道";
$MESS["MOBILE_EXT_CHAT_SELECTOR_SECTION_RECENT_TITLE"] = "最近的搜索";
$MESS["MOBILE_EXT_CHAT_SELECTOR_TITLE"] = "查找聊天";
$MESS["MOBILE_EXT_CHAT_SELECTOR_TITLE_BUTTON_LESS"] = "顯示較少";
$MESS["MOBILE_EXT_CHAT_SELECTOR_TITLE_BUTTON_MORE"] = "展示更多";
