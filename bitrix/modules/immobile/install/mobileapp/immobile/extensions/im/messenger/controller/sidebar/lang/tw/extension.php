<?php
$MESS["IMMOBILE_DIALOG_SIDEBAR_BTN_CALL"] = "稱呼";
$MESS["IMMOBILE_DIALOG_SIDEBAR_BTN_MORE"] = "更多的...";
$MESS["IMMOBILE_DIALOG_SIDEBAR_BTN_MUTE"] = "沉默的";
$MESS["IMMOBILE_DIALOG_SIDEBAR_BTN_SEARCH"] = "搜尋";
$MESS["IMMOBILE_DIALOG_SIDEBAR_BTN_VIDEO"] = "影片";
$MESS["IMMOBILE_DIALOG_SIDEBAR_IS_YOU"] = "（你）";
$MESS["IMMOBILE_DIALOG_SIDEBAR_LEAVE_CHAT_CONFIRM_NO"] = "取消";
$MESS["IMMOBILE_DIALOG_SIDEBAR_LEAVE_CHAT_CONFIRM_TITLE"] = "您確定要離開聊天嗎？";
$MESS["IMMOBILE_DIALOG_SIDEBAR_LEAVE_CHAT_CONFIRM_YES"] = "是的";
$MESS["IMMOBILE_DIALOG_SIDEBAR_NOTICE_CALL_ERROR_LIMIT"] = "呼叫參與者限制超過";
$MESS["IMMOBILE_DIALOG_SIDEBAR_NOTICE_CALL_ERROR_LIVE"] = "用戶無法通話";
$MESS["IMMOBILE_DIALOG_SIDEBAR_NOTICE_CALL_ERROR_MOREONE"] = "現在您是唯一的聊天參與者";
$MESS["IMMOBILE_DIALOG_SIDEBAR_NOTICE_COMING_SOON"] = "即將推出！";
$MESS["IMMOBILE_DIALOG_SIDEBAR_PARTICIPANTS_ADD_NAME_BTN"] = "完畢";
$MESS["IMMOBILE_DIALOG_SIDEBAR_PARTICIPANTS_ADD_ROW"] = "邀請";
$MESS["IMMOBILE_DIALOG_SIDEBAR_PARTICIPANTS_ADD_ROW_GROUP"] = "創建組聊天";
$MESS["IMMOBILE_DIALOG_SIDEBAR_PARTICIPANTS_ADD_TITLE"] = "邀請聊天";
$MESS["IMMOBILE_DIALOG_SIDEBAR_PARTICIPANTS_ITEM_ACTION_REMOVE"] = "消除";
$MESS["IMMOBILE_DIALOG_SIDEBAR_PARTICIPANTS_ITEM_ACTION_REMOVE_ERROR_MSGVER_1"] = "無法刪除聊天成員";
$MESS["IMMOBILE_DIALOG_SIDEBAR_PROFILE_TITLE_NOTES"] = "我的筆記";
$MESS["IMMOBILE_DIALOG_SIDEBAR_REMOVE_PARTICIPANT_CONFIRM_NO"] = "取消";
$MESS["IMMOBILE_DIALOG_SIDEBAR_REMOVE_PARTICIPANT_CONFIRM_TITLE"] = "您確定要刪除此用戶嗎？";
$MESS["IMMOBILE_DIALOG_SIDEBAR_REMOVE_PARTICIPANT_CONFIRM_YES"] = "消除";
$MESS["IMMOBILE_DIALOG_SIDEBAR_TAB_AUDIO"] = "聲音的";
$MESS["IMMOBILE_DIALOG_SIDEBAR_TAB_BRIEFS"] = "內褲";
$MESS["IMMOBILE_DIALOG_SIDEBAR_TAB_FILES"] = "文件";
$MESS["IMMOBILE_DIALOG_SIDEBAR_TAB_LINKS"] = "鏈接";
$MESS["IMMOBILE_DIALOG_SIDEBAR_TAB_MEDIA"] = "媒體";
$MESS["IMMOBILE_DIALOG_SIDEBAR_TAB_MEETINGS"] = "會議";
$MESS["IMMOBILE_DIALOG_SIDEBAR_TAB_PARTICIPANTS"] = "成員";
$MESS["IMMOBILE_DIALOG_SIDEBAR_TAB_SAVE"] = "最愛";
$MESS["IMMOBILE_DIALOG_SIDEBAR_TAB_SIGN"] = "符號";
$MESS["IMMOBILE_DIALOG_SIDEBAR_TAB_TASKS"] = "任務";
$MESS["IMMOBILE_DIALOG_SIDEBAR_USER_COUNTER_PLURAL_0"] = "＃計數＃成員";
$MESS["IMMOBILE_DIALOG_SIDEBAR_USER_COUNTER_PLURAL_1"] = "＃計數＃成員";
$MESS["IMMOBILE_DIALOG_SIDEBAR_USER_COUNTER_PLURAL_2"] = "＃計數＃成員";
$MESS["IMMOBILE_DIALOG_SIDEBAR_WIDGET_TITLE"] = "關於這個聊天";
