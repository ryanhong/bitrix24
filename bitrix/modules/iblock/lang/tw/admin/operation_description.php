<?php
$MESS["OP_DESC_ELEMENT_BIZPROC_START_MSGVER_1"] = "用戶可以在此信息塊元素上運行業務流程";
$MESS["OP_DESC_ELEMENT_DELETE"] = "用戶可以刪除信息塊元素";
$MESS["OP_DESC_ELEMENT_EDIT"] = "用戶可以編輯信息塊元素，除了訪問權限和父母綁定外";
$MESS["OP_DESC_ELEMENT_EDIT_ANY_WF_STATUS"] = "用戶可以在任何工作流狀態中編輯信息塊元素，除了訪問權限和父母綁定";
$MESS["OP_DESC_ELEMENT_EDIT_PRICE"] = "用戶可以編輯與此元素相關的價格（需要元素_edit和catalog_price_edit操作）";
$MESS["OP_DESC_ELEMENT_READ"] = "用戶可以讀取信息塊元素";
$MESS["OP_DESC_ELEMENT_RIGHTS_EDIT"] = "用戶可以編輯信息塊元素訪問權限（需要元素_edit操作）";
$MESS["OP_DESC_IBLOCK_ADMIN_DISPLAY"] = "用戶可以在控制面板中訪問信息塊";
$MESS["OP_DESC_IBLOCK_DELETE"] = "用戶可以刪除信息塊";
$MESS["OP_DESC_IBLOCK_EDIT"] = "用戶可以編輯所有信息塊參數，除了訪問權限";
$MESS["OP_DESC_IBLOCK_EXPORT"] = "預訂的";
$MESS["OP_DESC_IBLOCK_RIGHTS_EDIT"] = "用戶可以編輯信息塊訪問權限（需要Iblock_edit操作）";
$MESS["OP_DESC_SECTION_DELETE"] = "用戶可以刪除信息塊部分";
$MESS["OP_DESC_SECTION_EDIT"] = "用戶可以編輯信息塊部分的所有參數，而不是父母部分";
$MESS["OP_DESC_SECTION_ELEMENT_BIND"] = "用戶可以創建信息塊元素";
$MESS["OP_DESC_SECTION_READ"] = "用戶可以讀取信息塊部分參數（字段和屬性）";
$MESS["OP_DESC_SECTION_RIGHTS_EDIT"] = "用戶可以編輯信息塊部分的訪問權限（要求section_edit操作）";
$MESS["OP_DESC_SECTION_SECTION_BIND"] = "用戶可以創建信息塊部分";
$MESS["OP_NAME_ELEMENT_BIZPROC_START"] = "在元素上運行業務流程";
$MESS["OP_NAME_ELEMENT_DELETE"] = "刪除元素";
$MESS["OP_NAME_ELEMENT_EDIT"] = "編輯元素";
$MESS["OP_NAME_ELEMENT_EDIT_ANY_WF_STATUS"] = "在任何工作流狀態中編輯元素";
$MESS["OP_NAME_ELEMENT_EDIT_PRICE"] = "編輯元素相關價格";
$MESS["OP_NAME_ELEMENT_READ"] = "讀取元素";
$MESS["OP_NAME_ELEMENT_RIGHTS_EDIT"] = "編輯元素訪問權限";
$MESS["OP_NAME_IBLOCK_ADMIN_DISPLAY"] = "在控制面板中顯示信息塊";
$MESS["OP_NAME_IBLOCK_DELETE"] = "刪除信息塊";
$MESS["OP_NAME_IBLOCK_EDIT"] = "編輯信息塊參數";
$MESS["OP_NAME_IBLOCK_EXPORT"] = "導出信息塊";
$MESS["OP_NAME_IBLOCK_RIGHTS_EDIT"] = "編輯信息塊訪問權限";
$MESS["OP_NAME_SECTION_DELETE"] = "刪除部分";
$MESS["OP_NAME_SECTION_EDIT"] = "編輯部分";
$MESS["OP_NAME_SECTION_ELEMENT_BIND"] = "向部分添加新元素";
$MESS["OP_NAME_SECTION_READ"] = "讀取部分參數";
$MESS["OP_NAME_SECTION_RIGHTS_EDIT"] = "編輯部分訪問權限";
$MESS["OP_NAME_SECTION_SECTION_BIND"] = "創建子小節";
