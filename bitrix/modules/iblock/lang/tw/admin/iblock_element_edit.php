<?php
$MESS["IBEL_BIZPROC_DATE"] = "當前狀態日期：";
$MESS["IBEL_BIZPROC_DESC"] = "業務流程說明：";
$MESS["IBEL_BIZPROC_NA"] = "沒有業務流程運行。";
$MESS["IBEL_BIZPROC_NAME"] = "業務流程名稱：";
$MESS["IBEL_BIZPROC_NEW"] = "新的業務流程";
$MESS["IBEL_BIZPROC_RUN_CMD"] = "運行動作：";
$MESS["IBEL_BIZPROC_RUN_CMD_NO"] = "沒有任何";
$MESS["IBEL_BIZPROC_START"] = "開展新業務流程";
$MESS["IBEL_BIZPROC_STATE"] = "當前狀態：";
$MESS["IBEL_BIZPROC_STOP"] = "停止";
$MESS["IBEL_BIZPROC_TASKS"] = "業務流程任務：";
$MESS["IBEL_E_ACTIONS"] = "動作";
$MESS["IBEL_E_ACTIONS_TITLE"] = "在信息塊元素上執行操作";
$MESS["IBEL_E_COPY_ELEMENT"] = "複製";
$MESS["IBEL_E_COPY_ELEMENT_TITLE"] = "複製元素";
$MESS["IBEL_E_IBLOCK_ELEMENT"] = "元素";
$MESS["IBEL_E_IBLOCK_MANAGE_HINT"] = "您可以編輯信息塊訪問權限和屬性";
$MESS["IBEL_E_IBLOCK_MANAGE_HINT_HREF"] = "信息塊設置頁面。";
$MESS["IBEL_E_IBLOCK_SECTIONS"] = "部分";
$MESS["IBEL_E_LINK_TIP"] = "從名稱生成代碼";
$MESS["IBEL_E_MAIN_IBLOCK_SECTION_ID"] = "主要部分";
$MESS["IBEL_E_PUBLISHED"] = "發表的文件";
$MESS["IBEL_E_RIGHTS_FIELD"] = "元素訪問權限";
$MESS["IBEL_E_RIGHTS_SECTION_TITLE"] = "元素訪問權限";
$MESS["IBEL_E_SEO_CLEAR_VALUES"] = "清除緩存";
$MESS["IBEL_E_SEO_ELEMENT_TITLE"] = "元素標題";
$MESS["IBEL_E_SEO_FILE_ALT"] = "Alt模板";
$MESS["IBEL_E_SEO_FILE_NAME"] = "文件名模板";
$MESS["IBEL_E_SEO_FILE_TITLE"] = "標題模板";
$MESS["IBEL_E_SEO_FOR_ELEMENTS"] = "元素參數";
$MESS["IBEL_E_SEO_FOR_ELEMENTS_DETAIL_PICTURE"] = "元素完整圖像參數";
$MESS["IBEL_E_SEO_FOR_ELEMENTS_PREVIEW_PICTURE"] = "元素縮略圖參數";
$MESS["IBEL_E_SEO_MANAGEMENT"] = "管理";
$MESS["IBEL_E_SEO_META_DESCRIPTION"] = "元描述模板";
$MESS["IBEL_E_SEO_META_KEYWORDS"] = "元關鍵字模板";
$MESS["IBEL_E_SEO_META_TITLE"] = "元標題模板";
$MESS["IBEL_E_SEO_OVERWRITE"] = "應用於此元素。";
$MESS["IBEL_E_SETTINGS"] = "設定";
$MESS["IBEL_E_SETTINGS_TITLE"] = "編輯表單外觀設置";
$MESS["IBEL_E_TAB14"] = "SEO";
$MESS["IBEL_E_TAB14_TITLE"] = "編輯SEO數據";
$MESS["IBEL_E_TAB_BIZPROC"] = "業務流程";
$MESS["IBEL_E_TAB_DET"] = "細節";
$MESS["IBEL_E_TAB_DET_TITLE"] = "詳細視圖的信息";
$MESS["IBEL_E_TAB_ELEMENT"] = "參數";
$MESS["IBEL_E_TAB_ELEMENT_TITLE"] = "一般參數";
$MESS["IBEL_E_TAB_PREV"] = "預覽";
$MESS["IBEL_E_TAB_PREV_TITLE"] = "預覽的信息";
$MESS["IBEL_E_TAB_RIGHTS"] = "使用權";
$MESS["IBEL_E_TAB_RIGHTS_TITLE"] = "訪問權限";
$MESS["IBEL_E_TRANSLATION_SERVICE_NOT_CONFIGURED"] = "不存在外部翻譯引擎。在<a href= \"settings.php?mid=main \">內核模塊設置中添加翻譯引擎</a>。";
$MESS["IBEL_E_WARNING"] = "警告。";
$MESS["IBEL_HIST"] = "更新歷史記錄";
$MESS["IBLOCK_ACCESS_DENIED_SECTION"] = "您無權將元素鏈接到指定部分。";
$MESS["IBLOCK_ACCESS_DENIED_STATUS"] = "您沒有足夠的權限來編輯此記錄的當前狀態";
$MESS["IBLOCK_APPLY"] = "申請";
$MESS["IBLOCK_BAD_ELEMENT"] = "找不到或拒絕訪問元素。";
$MESS["IBLOCK_COMMENTS"] = "評論";
$MESS["IBLOCK_CREATED"] = "創建：";
$MESS["IBLOCK_DATE_LOCK"] = "鎖定：";
$MESS["IBLOCK_DOCUMENT_LOCKED"] = "該記錄由用戶＃＃id＃（＃date＃）暫時鎖定";
$MESS["IBLOCK_EDIT_TITLE"] = "編輯元素";
$MESS["IBLOCK_ELEMENT_DEL_CONF"] = "鏈接到此記錄的所有信息將被刪除。無論如何繼續？";
$MESS["IBLOCK_ELEMENT_EDIT_ELEMENTS"] = "元素";
$MESS["IBLOCK_ELEMENT_EDIT_LINKED"] = "鏈接元素：";
$MESS["IBLOCK_ELEMENT_EDIT_PROP_ADD"] = "添加";
$MESS["IBLOCK_ELEMENT_EDIT_TAGS_TIP"] = "（輸入逗號分隔的單詞或短語）";
$MESS["IBLOCK_ELEMENT_ERR_BUILDER_ADSENT"] = "找不到鏈接構建器。";
$MESS["IBLOCK_ELEMENT_FORM_TITLE_EDIT"] = "＃iblock_name＃：＃element_type＃：＃名稱＃ - 編輯";
$MESS["IBLOCK_ELEMENT_FORM_TITLE_NEW"] = "＃iblock_name＃：＃element_type＃：添加";
$MESS["IBLOCK_ELEMENT_FORM_TITLE_VIEW"] = "＃iblock_name＃：＃element_type＃：＃名稱＃ - 查看";
$MESS["IBLOCK_ELEMENT_OFFERS_IS_ABSENT"] = "沒有創建SKU";
$MESS["IBLOCK_ELEMENT_PROP_VALUE"] = "屬性值：";
$MESS["IBLOCK_EL_CANC"] = "取消";
$MESS["IBLOCK_EL_EDIT_IN_PANEL"] = "控制面板";
$MESS["IBLOCK_EL_OFFERS_ACCESS_DENIED"] = "您無權編輯SKU。";
$MESS["IBLOCK_EL_SAVE"] = "節省";
$MESS["IBLOCK_EL_SAVE_AND_ADD"] = "保存並添加";
$MESS["IBLOCK_EL_TAB_MO"] = "更多的";
$MESS["IBLOCK_EL_TAB_OFFERS"] = "sku";
$MESS["IBLOCK_EL_TAB_OFFERS_TITLE"] = "Sku的";
$MESS["IBLOCK_EL_TAB_PRODUCT_GROUP"] = "捆綁項目";
$MESS["IBLOCK_EL_TAB_PRODUCT_GROUP_TITLE"] = "與此產品捆綁在一起的物品";
$MESS["IBLOCK_EL_TAB_PRODUCT_SET"] = "設定項目";
$MESS["IBLOCK_EL_TAB_PRODUCT_SET_TITLE"] = "該產品的設置項目";
$MESS["IBLOCK_EL_TAB_WF"] = "工作流程";
$MESS["IBLOCK_EL_TAB_WF_TITLE"] = "工作流設置";
$MESS["IBLOCK_FIELD_ACTIVE"] = "積極的";
$MESS["IBLOCK_FIELD_ACTIVE_PERIOD_FROM"] = "活動開始";
$MESS["IBLOCK_FIELD_ACTIVE_PERIOD_TO"] = "活動結束";
$MESS["IBLOCK_FIELD_CODE"] = "瘋子代碼";
$MESS["IBLOCK_FIELD_CREATED"] = "創建於";
$MESS["IBLOCK_FIELD_DETAIL_PICTURE"] = "詳細的圖像";
$MESS["IBLOCK_FIELD_DETAIL_TEXT"] = "詳細說明";
$MESS["IBLOCK_FIELD_HINT_XML_ID"] = "現有元素所需；在創建新元素時，可以將空外ID設置為元素ID時空白。";
$MESS["IBLOCK_FIELD_LAST_UPDATED"] = "修改";
$MESS["IBLOCK_FIELD_NAME"] = "姓名";
$MESS["IBLOCK_FIELD_PREVIEW_PICTURE"] = "預覽圖像";
$MESS["IBLOCK_FIELD_PREVIEW_TEXT"] = "預覽文本";
$MESS["IBLOCK_FIELD_SORT"] = "排序";
$MESS["IBLOCK_FIELD_STATUS"] = "地位";
$MESS["IBLOCK_FIELD_TAGS"] = "標籤";
$MESS["IBLOCK_IMPORT_FROM"] = "從";
$MESS["IBLOCK_LAST_UPDATE"] = "最後更新：";
$MESS["IBLOCK_LINKED_ELEMENT_TITLE"] = "打開控制面板";
$MESS["IBLOCK_LINK_TO_MEDIA"] = "媒體鏈接";
$MESS["IBLOCK_SECTION"] = "鏈接（多個）：";
$MESS["IBLOCK_TCATALOG"] = "商業目錄";
$MESS["IBLOCK_UPPER_LEVEL"] = "更高級別";
$MESS["IBLOCK_WRONG_SESSION"] = "您的會議已經過期。請再次保存您的文檔。";
$MESS["IBLOCK_WRONG_WF_STATUS"] = "無效狀態";
