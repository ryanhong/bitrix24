<?php
$MESS["IBLOCK_ADM_HISTORY_BAD_ELEMENT"] = "找不到或拒絕訪問元素。";
$MESS["IBLOCK_ADM_HISTORY_BAD_IBLOCK"] = "未找到或訪問的部分。";
$MESS["IBLOCK_ADM_HISTORY_COMPARE"] = "比較";
$MESS["IBLOCK_ADM_HISTORY_COMPARE_ALERT"] = "選擇兩個文檔進行比較。";
$MESS["IBLOCK_ADM_HISTORY_CONFIRM_DEL"] = "您確定要刪除此唱片嗎？";
$MESS["IBLOCK_ADM_HISTORY_DELETE"] = "刪除";
$MESS["IBLOCK_ADM_HISTORY_DELETE_ALT"] = "刪除記錄";
$MESS["IBLOCK_ADM_HISTORY_DELETE_ERROR"] = "錯誤刪除記錄＃";
$MESS["IBLOCK_ADM_HISTORY_ORIGINAL"] = "當前版本";
$MESS["IBLOCK_ADM_HISTORY_ORIGINAL_TITLE"] = "單擊以查看元素當前版本";
$MESS["IBLOCK_ADM_HISTORY_PAGER"] = "記錄";
$MESS["IBLOCK_ADM_HISTORY_RESTORE"] = "恢復";
$MESS["IBLOCK_ADM_HISTORY_RESTORE_ALT"] = "還原歷史記錄";
$MESS["IBLOCK_ADM_HISTORY_RESTORE_CONFIRM"] = "您確定要從歷史記錄中還原這張唱片嗎？";
$MESS["IBLOCK_ADM_HISTORY_RESTORE_ERROR"] = "錯誤還原記錄＃";
$MESS["IBLOCK_ADM_HISTORY_TITLE"] = "記錄的修改歷史＃＃id＃";
$MESS["IBLOCK_ADM_HISTORY_UPDERR3"] = "錯誤更新記錄：訪問被拒絕";
$MESS["IBLOCK_ADM_HISTORY_VIEW"] = "看法";
$MESS["IBLOCK_ADM_HISTORY_VIEW_ALT"] = "查看記錄";
