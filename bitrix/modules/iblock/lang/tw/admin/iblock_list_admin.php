<?php
$MESS["IBLIST_A_ACCESS_DENIED_STATUS"] = "您沒有足夠的權限來編輯此記錄的當前狀態";
$MESS["IBLIST_A_ACTIVATE"] = "啟用";
$MESS["IBLIST_A_ACTIVE"] = "積極的";
$MESS["IBLIST_A_ADD_TO_SECTION"] = "將鏈接添加到部分";
$MESS["IBLIST_A_ADMIN_VIEW"] = "預覽";
$MESS["IBLIST_A_BAD_BLOCK_TYPE_ID"] = "錯誤的塊類型。";
$MESS["IBLIST_A_BAD_IBLOCK"] = "未找到或訪問的部分。";
$MESS["IBLIST_A_BP_H"] = "業務流程";
$MESS["IBLIST_A_BP_TASK"] = "任務";
$MESS["IBLIST_A_CATALOG_AVAILABLE"] = "可用性";
$MESS["IBLIST_A_CATALOG_AVAILABLE_TITLE_EXT"] = "產品可用性（與可購買的不相同）";
$MESS["IBLIST_A_CATALOG_BAR_CODE"] = "條碼";
$MESS["IBLIST_A_CATALOG_BAR_CODE_MULTI"] = "多種的";
$MESS["IBLIST_A_CATALOG_BUNDLE"] = "捆";
$MESS["IBLIST_A_CATALOG_CAN_BUY_ZERO"] = "製作庫存的物品可供購買";
$MESS["IBLIST_A_CATALOG_CAN_BUY_ZERO_TITLE"] = "製作庫存的物品可供購買，包括那些負數的物品";
$MESS["IBLIST_A_CATALOG_DEFAULT"] = "(預設)";
$MESS["IBLIST_A_CATALOG_EMPTY_VALUE"] = "未選中的";
$MESS["IBLIST_A_CATALOG_EXTRA_DESCRIPTION"] = "＃價值＃的附加費至基本價格";
$MESS["IBLIST_A_CATALOG_HEIGHT"] = "高度";
$MESS["IBLIST_A_CATALOG_LENGTH"] = "長度";
$MESS["IBLIST_A_CATALOG_MEASURE"] = "測量單位";
$MESS["IBLIST_A_CATALOG_MEASURE_RATIO"] = "因素";
$MESS["IBLIST_A_CATALOG_MEASURE_RATIO_TITLE"] = "單位比率";
$MESS["IBLIST_A_CATALOG_MEASURE_TITLE"] = "測量單位";
$MESS["IBLIST_A_CATALOG_PRODUCT"] = "產品";
$MESS["IBLIST_A_CATALOG_PRODUCT_MAPPING_SAVE_NOTIFICATION"] = "產品可見性已更改";
$MESS["IBLIST_A_CATALOG_PRODUCT_TITLE"] = "產品";
$MESS["IBLIST_A_CATALOG_PURCHASING_PRICE"] = "購買價格";
$MESS["IBLIST_A_CATALOG_QUANTITY_EXT"] = "庫存數量";
$MESS["IBLIST_A_CATALOG_QUANTITY_NEW_CARD"] = "可用的庫存";
$MESS["IBLIST_A_CATALOG_QUANTITY_RESERVED"] = "預訂的";
$MESS["IBLIST_A_CATALOG_QUANTITY_RESERVED_NEW_CARD"] = "預訂的";
$MESS["IBLIST_A_CATALOG_QUANTITY_TRACE"] = "減少訂單數量";
$MESS["IBLIST_A_CATALOG_QUANTITY_TRACE_EXT"] = "啟用庫存控制";
$MESS["IBLIST_A_CATALOG_SKU_PRICE"] = "從＃價格＃";
$MESS["IBLIST_A_CATALOG_TYPE"] = "產品類別";
$MESS["IBLIST_A_CATALOG_TYPE_MESS_GROUP_MSGVER_1"] = "放";
$MESS["IBLIST_A_CATALOG_TYPE_TITLE"] = "產品類別";
$MESS["IBLIST_A_CATALOG_VAT_ID"] = "稅率";
$MESS["IBLIST_A_CATALOG_VAT_INCLUDED"] = "含稅";
$MESS["IBLIST_A_CATALOG_WEIGHT"] = "重量（克）";
$MESS["IBLIST_A_CATALOG_WIDTH"] = "寬度";
$MESS["IBLIST_A_CLEAR_COUNTER"] = "重置視圖計數器";
$MESS["IBLIST_A_CLEAR_COUNTER_CONFIRM"] = "您確定要重置計數器和第一次查看日期嗎？";
$MESS["IBLIST_A_CLEAR_COUNTER_TITLE"] = "重置視圖計數器和第一個查看日期";
$MESS["IBLIST_A_CODE"] = "瘋子代碼";
$MESS["IBLIST_A_CODE_TRANSLIT_CONFIRM_MULTI"] = "您確定要更改所選項目和部分的符號代碼嗎？這可能會影響他們各自的URL。";
$MESS["IBLIST_A_CODE_TRANSLIT_ELEMENT_CONFIRM"] = "您確定要更改項目的符號代碼嗎？這可能會更改指向項目的鏈接。";
$MESS["IBLIST_A_CODE_TRANSLIT_ELEMENT_TITLE"] = "使用項目名稱創建符號代碼";
$MESS["IBLIST_A_CODE_TRANSLIT_MSGVER_1"] = "更改符號代碼";
$MESS["IBLIST_A_CODE_TRANSLIT_SECTION_CONFIRM"] = "您確定要更改該部分的符號代碼嗎？這可能會更改指向部分和部分項目的鏈接。";
$MESS["IBLIST_A_CODE_TRANSLIT_SECTION_TITLE"] = "使用截面名稱創建符號代碼";
$MESS["IBLIST_A_CONVERT_PRODUCT_TO_SERVICE_CONFIRM_WITHOUT_INVENTORY"] = "您確定要將此產品轉換為服務嗎？當前的庫存信息將無法用於此產品。";
$MESS["IBLIST_A_CONVERT_PRODUCT_TO_SERVICE_CONFIRM_WITH_INVENTORY"] = "您確定要將此產品轉換為服務嗎？所有相關信息將從所有倉庫中刪除；當前的庫存信息將不可用。";
$MESS["IBLIST_A_CONVERT_PRODUCT_TO_SERVICE_TITLE"] = "轉換為服務";
$MESS["IBLIST_A_CONVERT_SERVICE_TO_PRODUCT_CONFIRM_MESSAGE"] = "您確定要將此服務轉換為產品嗎？";
$MESS["IBLIST_A_CONVERT_SERVICE_TO_PRODUCT_TITLE"] = "轉換為產品";
$MESS["IBLIST_A_COPY_ELEMENT"] = "複製";
$MESS["IBLIST_A_CREATED_USER_NAME"] = "由...製作";
$MESS["IBLIST_A_DATE_ACTIVE_FROM"] = "活躍";
$MESS["IBLIST_A_DATE_ACTIVE_TO"] = "活躍直到";
$MESS["IBLIST_A_DATE_CREATE"] = "創建";
$MESS["IBLIST_A_DEACTIVATE"] = "停用";
$MESS["IBLIST_A_DETAIL_PICTURE"] = "詳細的圖像";
$MESS["IBLIST_A_DETAIL_TEXT"] = "詳細說明";
$MESS["IBLIST_A_ED2_TITLE"] = "直接編輯原始文檔";
$MESS["IBLIST_A_EDIT"] = "編輯元素";
$MESS["IBLIST_A_ED_TITLE"] = "通過工作流程間接編輯文檔";
$MESS["IBLIST_A_ELEMENTS"] = "元素";
$MESS["IBLIST_A_ELEMENT_DELETE_ERROR"] = "錯誤刪除元素## ID＃。";
$MESS["IBLIST_A_ELS"] = "元素";
$MESS["IBLIST_A_EL_EDIT"] = "開放元素編輯頁面";
$MESS["IBLIST_A_ERR_BUILDER_ADSENT"] = "找不到鏈接構建器。";
$MESS["IBLIST_A_EXTCODE_MSGVER_1"] = "外部ID";
$MESS["IBLIST_A_F_CREATED_BY"] = "由...製作";
$MESS["IBLIST_A_F_CREATED_WHEN"] = "創建日期";
$MESS["IBLIST_A_F_DESC"] = "描述";
$MESS["IBLIST_A_F_FROM_TO_ID"] = "ID（第一個和最後一個值）：";
$MESS["IBLIST_A_F_MODIFIED_BY"] = "修改";
$MESS["IBLIST_A_F_SECTION"] = "部分：";
$MESS["IBLIST_A_F_STATUS"] = "工作流狀態";
$MESS["IBLIST_A_GREEN_ALT"] = "可修改";
$MESS["IBLIST_A_HIST"] = "更新歷史記錄";
$MESS["IBLIST_A_HISTORY_ALT"] = "修改歷史";
$MESS["IBLIST_A_IBLOCK_MANAGE_HINT"] = "您可以編輯信息塊訪問權限和屬性";
$MESS["IBLIST_A_IBLOCK_MANAGE_HINT_HREF"] = "信息塊設置頁面。";
$MESS["IBLIST_A_ID"] = "ID";
$MESS["IBLIST_A_LIST"] = "查看小節列表";
$MESS["IBLIST_A_LIST_TITLE"] = "＃iblock_name＃：產品";
$MESS["IBLIST_A_LIST_TITLE_2"] = "產品目錄";
$MESS["IBLIST_A_LOCKED_USER_NAME"] = "鎖定";
$MESS["IBLIST_A_LOCK_ACTION"] = "鎖";
$MESS["IBLIST_A_LOCK_STATUS"] = "鎖定";
$MESS["IBLIST_A_MODIFIED_BY"] = "經過";
$MESS["IBLIST_A_MOVE_TO_SECTION"] = "移至部分";
$MESS["IBLIST_A_NAME"] = "姓名";
$MESS["IBLIST_A_NO_BASE_PRICE"] = "未針對元素## ID＃指定基本價格。";
$MESS["IBLIST_A_ORIG_ED"] = "編輯原件";
$MESS["IBLIST_A_ORIG_ED_TITLE"] = "編輯發布的版本";
$MESS["IBLIST_A_PARENT"] = "父部分";
$MESS["IBLIST_A_PREVIEW_PICTURE"] = "預覽圖像";
$MESS["IBLIST_A_PREVIEW_TEXT"] = "預覽說明";
$MESS["IBLIST_A_PROP_ADD"] = "添加";
$MESS["IBLIST_A_PROP_DESC"] = "描述：";
$MESS["IBLIST_A_PROP_DESC_TITLE"] = "屬性價值說明";
$MESS["IBLIST_A_PROP_NOT_SET"] = "（沒有設置）";
$MESS["IBLIST_A_RED_ALT"] = "暫時鎖定（目前正在編輯）";
$MESS["IBLIST_A_SAVE_ERROR"] = "錯誤保存記錄## ID＃：＃error_message＃";
$MESS["IBLIST_A_SECS"] = "小節";
$MESS["IBLIST_A_SECTIONS"] = "部分";
$MESS["IBLIST_A_SECTION_DELETE_ERROR"] = "錯誤刪除部分## ID＃。";
$MESS["IBLIST_A_SEC_EDIT"] = "開放部分編輯頁面";
$MESS["IBLIST_A_SHOW_COUNTER"] = "顯示";
$MESS["IBLIST_A_SHOW_COUNTER_START"] = "第一場演出的日期";
$MESS["IBLIST_A_SORT"] = "種類";
$MESS["IBLIST_A_STATUS"] = "地位";
$MESS["IBLIST_A_TAGS"] = "標籤";
$MESS["IBLIST_A_TIMESTAMP"] = "修改。";
$MESS["IBLIST_A_TS"] = "修改的";
$MESS["IBLIST_A_UNLOCK"] = "開鎖";
$MESS["IBLIST_A_UNLOCK_ACTION"] = "開鎖";
$MESS["IBLIST_A_UNLOCK_ALT"] = "解鎖記錄";
$MESS["IBLIST_A_UNLOCK_CONFIRM"] = "您確定要解鎖記錄嗎？";
$MESS["IBLIST_A_UP"] = "一個水平";
$MESS["IBLIST_A_UPDERR_ACCESS"] = "錯誤更新記錄## ID＃：訪問被拒絕。";
$MESS["IBLIST_A_UPDERR_LOCKED"] = "無法更新記錄## ID＃，因為它已鎖定。";
$MESS["IBLIST_A_UPDERR_STATUS"] = "錯誤更新記錄## ID＃：轉移到此狀態的權限不足。";
$MESS["IBLIST_A_UP_TITLE"] = "點擊瀏覽更高級別";
$MESS["IBLIST_A_USERINFO"] = "查看用戶信息";
$MESS["IBLIST_A_VIEW_WF"] = "工作流預覽";
$MESS["IBLIST_A_VIEW_WF_ALT"] = "查看已發表的元素";
$MESS["IBLIST_A_WF_COMMENTS"] = "管理員評論";
$MESS["IBLIST_A_WF_DATE_LOCK"] = "日期鎖定";
$MESS["IBLIST_A_WF_NEW"] = "未出版";
$MESS["IBLIST_A_WF_STATUS_CHANGE"] = "更改狀態";
$MESS["IBLIST_A_YELLOW_ALT"] = "您已經鎖定了它（請記住解鎖）";
$MESS["IBLIST_BP"] = "業務流程";
$MESS["IBLIST_BP_START"] = "運行業務流程";
$MESS["IBLIST_BTN_BP"] = "業務流程模板";
$MESS["IBLIST_CHPRICE_ERROR_BASE_FROM_EXTRA_SIMPLE_ELEMENTS"] = "錯誤更新[## ID＃] \“＃名稱＃\”：無法使用附加費和指定價格計算基本價格。";
$MESS["IBLIST_CHPRICE_ERROR_BASE_FROM_EXTRA_SKU_ELEMENTS"] = "錯誤更新SKU [## ID＃] \“＃名稱＃\”：無法使用附加費和指定價格計算基本價格。";
$MESS["IBLIST_CHPRICE_ERROR_PRICE_WITH_EXTRA_SIMPLE_ELEMENTS"] = "錯誤更新[## ID＃] \“＃名稱＃\”：無法更改定義為基本價格的附加費。";
$MESS["IBLIST_CHPRICE_ERROR_PRICE_WITH_EXTRA_SKU_ELEMENTS"] = "錯誤更新SKU [## ID＃] \“＃名稱＃\”：無法將價格定義為基本價格的附加費。";
$MESS["IBLIST_CHPRICE_ERROR_WRONG_CURRENCY"] = "更新錯誤：未指定貨幣。";
$MESS["IBLIST_CHPRICE_ERROR_WRONG_INPUT_VALUE"] = "更新錯誤：提供的不正確值。";
$MESS["IBLIST_CHPRICE_ERROR_WRONG_VALUE_SIMPLE_ELEMENTS"] = "更新錯誤：產品的新值[## ID＃] \“＃名稱＃\”小於或等於零。";
$MESS["IBLIST_CHPRICE_ERROR_WRONG_VALUE_SKU_ELEMENTS"] = "更新錯誤：SKU [## ID＃] \“＃名稱＃\”的新值小於或等於零。";
$MESS["IBLIST_DEFAULT_VALUE"] = "預設";
$MESS["IBLIST_NOT_SET"] = "（沒有設置）";
$MESS["IBLIST_NO_VALUE"] = "不";
$MESS["IBLIST_PRODUCTS_INSTAGRAM"] = "Instagram產品";
$MESS["IBLIST_YES_VALUE"] = "是的";
$MESS["IBLOCK_ALL"] = "（任何）";
$MESS["IBLOCK_CHANGE"] = "調整";
$MESS["IBLOCK_CHANGING_PRICE"] = "更新價格";
$MESS["IBLOCK_CONFIRM_DEL_MESSAGE"] = "與此記錄有關的所有信息將被刪除！繼續？";
$MESS["IBLOCK_DELETE_ALT"] = "刪除記錄";
$MESS["IBLOCK_FIELD_TIMESTAMP_X"] = "修改日期";
$MESS["IBLOCK_NO"] = "不";
$MESS["IBLOCK_UPPER_LEVEL"] = "更高級別";
$MESS["IBLOCK_VALUE_ANY"] = "（任何）";
$MESS["IBLOCK_YES"] = "是的";
$MESS["MAIN_ADMIN_LIST_ACTIVATE"] = "啟用";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "檢查：";
$MESS["MAIN_ADMIN_LIST_DEACTIVATE"] = "停用";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "刪除";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "選擇：";
$MESS["MAIN_DELETE"] = "刪除";
