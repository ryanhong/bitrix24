<?php
$MESS["IBLOCK_ACTIVE"] = "記錄活動：";
$MESS["IBLOCK_CODE"] = "助記符代碼：";
$MESS["IBLOCK_CONTENT"] = "更高級別";
$MESS["IBLOCK_DELETE"] = "刪除";
$MESS["IBLOCK_EXTERNAL_CODE"] = "外部數據庫中的代碼：";
$MESS["IBLOCK_NAME"] = "姓名：";
$MESS["IBLOCK_SORT"] = "排序索引：";
$MESS["IBLOCK_TAGS"] = "標籤：";
$MESS["IBLOCK_WF_STATUS"] = "地位：";
