<?php
$MESS["IBLOCK_ADM_ACTIVE"] = "積極的";
$MESS["IBLOCK_ADM_CONFIRM_DEL_MESSAGE"] = "與此記錄有關的所有信息將被刪除！繼續？";
$MESS["IBLOCK_ADM_DELETE_ERROR"] = "刪除時錯誤。可能存在引用對象。";
$MESS["IBLOCK_ADM_FILT_ACT"] = "積極的";
$MESS["IBLOCK_ADM_FILT_ID"] = "ID（第一和最後）：";
$MESS["IBLOCK_ADM_FILT_NAME"] = "姓名：";
$MESS["IBLOCK_ADM_FILT_SITE"] = "地點";
$MESS["IBLOCK_ADM_HEADER_BIZPROC"] = "業務流程模板";
$MESS["IBLOCK_ADM_HEADER_CANONICAL_PAGE_URL"] = "元素規範URL";
$MESS["IBLOCK_ADM_HEADER_DETAIL_URL"] = "元素頁網址";
$MESS["IBLOCK_ADM_HEADER_EL"] = "元素";
$MESS["IBLOCK_ADM_HEADER_LIST_URL"] = "信息塊頁網址";
$MESS["IBLOCK_ADM_HEADER_SECT"] = "部分";
$MESS["IBLOCK_ADM_HEADER_TOINDEX"] = "將元素添加到索引";
$MESS["IBLOCK_ADM_HEADER_WORKFLOW"] = "工作流程";
$MESS["IBLOCK_ADM_LANG"] = "地點";
$MESS["IBLOCK_ADM_MANAGE_HINT"] = "您可以在此處在上表中創建，編輯和管理信息塊";
$MESS["IBLOCK_ADM_MANAGE_HINT_HREF"] = "信息塊 - 信息塊類型";
$MESS["IBLOCK_ADM_MENU_BIZPROC"] = "業務流程";
$MESS["IBLOCK_ADM_MENU_PROPERTIES"] = "特性";
$MESS["IBLOCK_ADM_NAME"] = "姓名";
$MESS["IBLOCK_ADM_SAVE_ERROR"] = "錯誤保存記錄## ID＃：＃error_text＃";
$MESS["IBLOCK_ADM_SORT"] = "種類。";
$MESS["IBLOCK_ADM_TIMESTAMP"] = "修改。";
$MESS["IBLOCK_ADM_TITLE"] = "＃iblock_type＃：信息塊";
$MESS["IBLOCK_ADM_TO_ADDIBLOCK"] = "添加信息塊";
$MESS["IBLOCK_ADM_TO_ADDIBLOCK_TITLE"] = "添加一個新的信息塊";
$MESS["IBLOCK_ADM_TO_EDIT"] = "編輯信息塊";
$MESS["IBLOCK_ADM_TO_ELLIST"] = "單擊查看元素";
$MESS["IBLOCK_ADM_TO_EL_LIST"] = "單擊查看元素";
$MESS["IBLOCK_ADM_TO_SECTLIST"] = "單擊查看部分";
$MESS["IBLOCK_ADM_UPD_ERROR"] = "錯誤更新記錄：";
$MESS["IBLOCK_ALL"] = "（任何）";
$MESS["IBLOCK_FIELD_CODE"] = "瘋子代碼";
$MESS["IBLOCK_NO"] = "不";
$MESS["IBLOCK_YES"] = "是的";
$MESS["MAIN_ADMIN_LIST_ACTIVATE"] = "啟用";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "檢查：";
$MESS["MAIN_ADMIN_LIST_DEACTIVATE"] = "停用";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "刪除";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "選擇：";
$MESS["MAIN_ADMIN_MENU_DELETE"] = "刪除";
$MESS["MAIN_ADMIN_MENU_EDIT"] = "編輯";
