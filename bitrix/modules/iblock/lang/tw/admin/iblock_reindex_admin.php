<?php
$MESS["IBLOCK_RADM_ACTIVE"] = "積極的";
$MESS["IBLOCK_RADM_ACTIVE_NO"] = "不";
$MESS["IBLOCK_RADM_ACTIVE_YES"] = "是的";
$MESS["IBLOCK_RADM_IBLOCKS"] = "信息塊";
$MESS["IBLOCK_RADM_ID"] = "ID";
$MESS["IBLOCK_RADM_INDEX_OK"] = "進行中";
$MESS["IBLOCK_RADM_NAME"] = "姓名";
$MESS["IBLOCK_RADM_PROPERTY_INDEX"] = "地位";
$MESS["IBLOCK_RADM_REINDEX"] = "創造";
$MESS["IBLOCK_RADM_REINDEX_ALL"] = "創建全部";
$MESS["IBLOCK_RADM_REINDEX_DISABLE"] = "禁用";
$MESS["IBLOCK_RADM_REINDEX_DISABLE_CONFIRM"] = "您確定要禁用此信息塊的刻面索引嗎？";
$MESS["IBLOCK_RADM_TITLE"] = "信息塊索引";
