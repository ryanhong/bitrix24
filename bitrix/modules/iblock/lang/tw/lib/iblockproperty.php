<?php
$MESS["IBLOCK_PROPERTY_ENTITY_ACTIVE_FIELD"] = "積極的";
$MESS["IBLOCK_PROPERTY_ENTITY_CODE_FIELD"] = "符號代碼";
$MESS["IBLOCK_PROPERTY_ENTITY_COL_COUNT_FIELD"] = "列計數";
$MESS["IBLOCK_PROPERTY_ENTITY_DEFAULT_VALUE_FIELD"] = "預設值";
$MESS["IBLOCK_PROPERTY_ENTITY_FILE_TYPE_FIELD"] = "允許的文件擴展名";
$MESS["IBLOCK_PROPERTY_ENTITY_FILTRABLE_FIELD"] = "在“元素”頁面上顯示此屬性的過濾器控件";
$MESS["IBLOCK_PROPERTY_ENTITY_HINT_FIELD"] = "暗示";
$MESS["IBLOCK_PROPERTY_ENTITY_IBLOCK_ID_FIELD"] = "信息塊ID";
$MESS["IBLOCK_PROPERTY_ENTITY_ID_FIELD"] = "屬性ID";
$MESS["IBLOCK_PROPERTY_ENTITY_IS_REQUIRED_FIELD"] = "必需的";
$MESS["IBLOCK_PROPERTY_ENTITY_LINK_IBLOCK_ID_FIELD"] = "鏈接的信息塊ID";
$MESS["IBLOCK_PROPERTY_ENTITY_LIST_TYPE_FIELD"] = "列表顯示";
$MESS["IBLOCK_PROPERTY_ENTITY_MULTIPLE_CNT_FIELD"] = "多個值字段的數量";
$MESS["IBLOCK_PROPERTY_ENTITY_MULTIPLE_FIELD"] = "多種的";
$MESS["IBLOCK_PROPERTY_ENTITY_NAME_FIELD"] = "姓名";
$MESS["IBLOCK_PROPERTY_ENTITY_PROPERTY_TYPE_FIELD"] = "財產種類";
$MESS["IBLOCK_PROPERTY_ENTITY_ROW_COUNT_FIELD"] = "行計";
$MESS["IBLOCK_PROPERTY_ENTITY_SEARCHABLE_FIELD"] = "將屬性值添加到搜索索引";
$MESS["IBLOCK_PROPERTY_ENTITY_SORT_FIELD"] = "種類";
$MESS["IBLOCK_PROPERTY_ENTITY_TIMESTAMP_X_FIELD"] = "修改";
$MESS["IBLOCK_PROPERTY_ENTITY_TMP_ID_FIELD"] = "臨時ID";
$MESS["IBLOCK_PROPERTY_ENTITY_USER_TYPE_FIELD"] = "自定義類型";
$MESS["IBLOCK_PROPERTY_ENTITY_USER_TYPE_SETTINGS_FIELD"] = "自定義類型參數";
$MESS["IBLOCK_PROPERTY_ENTITY_VERSION_FIELD"] = "屬性價值存儲模式";
$MESS["IBLOCK_PROPERTY_ENTITY_WITH_DESCRIPTION_FIELD"] = "有描述";
$MESS["IBLOCK_PROPERTY_ENTITY_XML_ID_FIELD"] = "外部ID";
