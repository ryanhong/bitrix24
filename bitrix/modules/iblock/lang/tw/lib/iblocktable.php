<?php
$MESS["IBLOCK_ENTITY_ACTIVE_FIELD"] = "積極的";
$MESS["IBLOCK_ENTITY_BIZPROC_FIELD"] = "在業務流程中使用";
$MESS["IBLOCK_ENTITY_CANONICAL_PAGE_URL_FIELD"] = "元素規範URL";
$MESS["IBLOCK_ENTITY_CODE_FIELD"] = "符號代碼";
$MESS["IBLOCK_ENTITY_DESCRIPTION_FIELD"] = "描述";
$MESS["IBLOCK_ENTITY_DESCRIPTION_TYPE_FIELD"] = "說明類型";
$MESS["IBLOCK_ENTITY_DETAIL_PAGE_URL_FIELD"] = "詳細信息頁網址";
$MESS["IBLOCK_ENTITY_EDIT_FILE_AFTER_FIELD"] = "包含元素編輯表單的文件";
$MESS["IBLOCK_ENTITY_EDIT_FILE_BEFORE_FIELD"] = "元素編輯文件允許在保存之前更新字段";
$MESS["IBLOCK_ENTITY_IBLOCK_TYPE_ID_FIELD"] = "信息塊類型";
$MESS["IBLOCK_ENTITY_ID_FIELD"] = "ID";
$MESS["IBLOCK_ENTITY_INDEX_ELEMENT_FIELD"] = "將項目添加到搜索索引";
$MESS["IBLOCK_ENTITY_INDEX_SECTION_FIELD"] = "將部分添加到搜索索引";
$MESS["IBLOCK_ENTITY_LAST_CONV_ELEMENT_FIELD"] = "最後一個轉換的元素";
$MESS["IBLOCK_ENTITY_LIST_MODE_FIELD"] = "部分和元素視圖模式";
$MESS["IBLOCK_ENTITY_LIST_PAGE_URL_FIELD"] = "信息塊頁網址";
$MESS["IBLOCK_ENTITY_NAME_FIELD"] = "姓名";
$MESS["IBLOCK_ENTITY_PICTURE_FIELD"] = "圖像";
$MESS["IBLOCK_ENTITY_PROPERTY_INDEX_FIELD"] = "使用刻面索引";
$MESS["IBLOCK_ENTITY_RIGHTS_MODE_FIELD"] = "訪問權限模式";
$MESS["IBLOCK_ENTITY_SECTION_CHOOSER_FIELD"] = "元素到截面結合控制";
$MESS["IBLOCK_ENTITY_SECTION_PAGE_URL_FIELD"] = "部分頁面URL";
$MESS["IBLOCK_ENTITY_SECTION_PROPERTY_FIELD"] = "具有私人特性";
$MESS["IBLOCK_ENTITY_SOCNET_GROUP_ID_FIELD"] = "社交網絡組";
$MESS["IBLOCK_ENTITY_SORT_FIELD"] = "種類";
$MESS["IBLOCK_ENTITY_TIMESTAMP_X_FIELD"] = "修改";
$MESS["IBLOCK_ENTITY_TMP_ID_FIELD"] = "臨時ID";
$MESS["IBLOCK_ENTITY_VERSION_FIELD"] = "屬性存儲設置";
$MESS["IBLOCK_ENTITY_WORKFLOW_FIELD"] = "啟用文檔工作流程";
$MESS["IBLOCK_ENTITY_XML_ID_FIELD"] = "外部ID";
