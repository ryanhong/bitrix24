<?php
$MESS["IBLOCK_TYPE_ENTITY_EDIT_FILE_AFTER_FIELD"] = "包含元素編輯表單的文件";
$MESS["IBLOCK_TYPE_ENTITY_EDIT_FILE_BEFORE_FIELD"] = "元素編輯文件允許在保存之前更新字段";
$MESS["IBLOCK_TYPE_ENTITY_ID_FIELD"] = "ID";
$MESS["IBLOCK_TYPE_ENTITY_IN_RSS_FIELD"] = "啟用RSS出口";
$MESS["IBLOCK_TYPE_ENTITY_SECTIONS_FIELD"] = "使用分層存儲進行項目（類似樹狀的結構允許嵌套小節）";
$MESS["IBLOCK_TYPE_ENTITY_SORT_FIELD"] = "種類";
