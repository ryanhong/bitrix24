<?php
$MESS["PROPERTY_FEATURE_ERR_BAD_FEATURE_LIST"] = "無效的屬性參數集";
$MESS["PROPERTY_FEATURE_ERR_BAD_PROPERTY_ID"] = "無效的屬性ID";
$MESS["PROPERTY_FEATURE_ERR_EMPTY_FEATURE_LIST"] = "屬性參數集為空";
$MESS["PROPERTY_FEATURE_NAME_DETAIL_PAGE_SHOW"] = "在項目詳細信息頁面上顯示";
$MESS["PROPERTY_FEATURE_NAME_LIST_PAGE_SHOW"] = "在項目頁面上顯示";
