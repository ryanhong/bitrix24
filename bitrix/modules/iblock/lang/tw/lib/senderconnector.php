<?php
$MESS["sender_connector_iblock_empty"] = "（沒有可用的信息塊）";
$MESS["sender_connector_iblock_field_email"] = "電子郵件字段：";
$MESS["sender_connector_iblock_field_iblock"] = "信息塊：";
$MESS["sender_connector_iblock_field_name"] = "名稱字段：";
$MESS["sender_connector_iblock_field_select"] = "（選擇一個字段）";
$MESS["sender_connector_iblock_name"] = "信息塊";
$MESS["sender_connector_iblock_prop_empty"] = "（沒有可用字段）";
$MESS["sender_connector_iblock_required_settings"] = "此功能需要配置";
$MESS["sender_connector_iblock_select"] = "（選擇信息塊）";
