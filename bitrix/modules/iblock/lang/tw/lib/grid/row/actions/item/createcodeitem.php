<?php
$MESS["IBLOCK_GRID_ROW_ACTIONS_CREATE_CODE_BACK_BUTTON"] = "取消";
$MESS["IBLOCK_GRID_ROW_ACTIONS_CREATE_CODE_CONFIRM_BUTTON"] = "是的";
$MESS["IBLOCK_GRID_ROW_ACTIONS_CREATE_CODE_ELEMENT_CONFIRM_CONTENT"] = "這可能會影響網站上項目頁面的鏈接。";
$MESS["IBLOCK_GRID_ROW_ACTIONS_CREATE_CODE_ELEMENT_CONFIRM_TITLE"] = "您確定要更改項目符號代碼嗎？";
$MESS["IBLOCK_GRID_ROW_ACTIONS_CREATE_CODE_ERROR_ACCESS_DENIED_ELEMENT"] = "編輯項目## ID＃的權限不足。";
$MESS["IBLOCK_GRID_ROW_ACTIONS_CREATE_CODE_ERROR_ACCESS_DENIED_SECTION"] = "編輯部分## ID＃的權限不足。";
$MESS["IBLOCK_GRID_ROW_ACTIONS_CREATE_CODE_NAME_MSGVER_1"] = "更改符號代碼";
$MESS["IBLOCK_GRID_ROW_ACTIONS_CREATE_CODE_SECTION_CONFIRM_CONTENT"] = "這可能會影響網站上“部分和部分”頁面的鏈接。";
$MESS["IBLOCK_GRID_ROW_ACTIONS_CREATE_CODE_SECTION_CONFIRM_TITLE"] = "您確定要更改部分符號代碼嗎？";
