<?php
$MESS["IBLOCK_TYPE_BAD_ID"] = "信息塊類型ID未指定。";
$MESS["IBLOCK_TYPE_BAD_NAME"] = "語言名稱未指定";
$MESS["IBLOCK_TYPE_DUBL_ID"] = "信息塊類型ID已重複。";
$MESS["IBLOCK_TYPE_EMPTY_NAMES"] = "語言依賴性名稱未指定。";
$MESS["IBLOCK_TYPE_ID_HAS_WRONG_CHARS"] = "信息塊類型ID只能包含拉丁字母，數字和下劃線符號。";
