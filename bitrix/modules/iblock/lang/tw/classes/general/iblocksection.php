<?php
$MESS["IBLOCK_BAD_BLOCK_SECTION_ID_PARENT"] = "部分塊代碼與父部分的塊不匹配！";
$MESS["IBLOCK_BAD_BLOCK_SECTION_PARENT"] = "父母部分不正確！";
$MESS["IBLOCK_BAD_BLOCK_SECTION_RECURSE"] = "無法將部分移動到自身內部。";
$MESS["IBLOCK_BAD_SECTION"] = "未指定截面名稱。";
$MESS["IBLOCK_BAD_SECTION_FIELD"] = "需要字段\“＃field_name＃\”。";
$MESS["IBLOCK_BAD_SECTION_ID"] = "沒有此ID（＃id＃）的部分。";
$MESS["IBLOCK_DUP_SECTION_CODE"] = "具有此符號代碼的部分已經存在。";
