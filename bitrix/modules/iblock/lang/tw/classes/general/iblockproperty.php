<?php
$MESS["IBLOCK_PROPERTY_ADD_ERROR"] = "錯誤（＃代碼＃）添加屬性＃id＃。";
$MESS["IBLOCK_PROPERTY_BAD_NAME"] = "名稱未指定。";
$MESS["IBLOCK_PROPERTY_CHANGE_ERROR"] = "錯誤（＃代碼＃）修改屬性＃id＃。";
$MESS["IBLOCK_PROPERTY_CODE_FIRST_LETTER"] = "屬性代碼不能以數字開頭。";
$MESS["IBLOCK_PROPERTY_NOT_FOUND"] = "屬性＃ID＃找不到。";
$MESS["IBLOCK_PROPERTY_WRONG_CODE"] = "屬性代碼只能包含拉丁字母，數字和下劃線符號。";
