<?php
$MESS["SP_DISPLAY_TYPE_A"] = "滑塊的數字範圍";
$MESS["SP_DISPLAY_TYPE_B"] = "數字範圍";
$MESS["SP_DISPLAY_TYPE_F"] = "複選框";
$MESS["SP_DISPLAY_TYPE_G"] = "帶圖像的複選框";
$MESS["SP_DISPLAY_TYPE_H"] = "帶文本和圖像的複選框";
$MESS["SP_DISPLAY_TYPE_K"] = "無線電按鈕";
$MESS["SP_DISPLAY_TYPE_P"] = "下拉列表";
$MESS["SP_DISPLAY_TYPE_R"] = "帶有文本和圖像的下拉列表";
$MESS["SP_DISPLAY_TYPE_U"] = "日曆";
