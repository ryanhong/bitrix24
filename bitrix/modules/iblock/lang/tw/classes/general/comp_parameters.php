<?php
$MESS["IB_COMPLIB_PARAMETER_FILE_404"] = "顯示此頁面（默認情況下為/404.php）";
$MESS["IB_COMPLIB_PARAMETER_GROUP_404_SETTINGS"] = "404錯誤設置";
$MESS["IB_COMPLIB_PARAMETER_MESSAGE_404"] = "顯示此消息（默認情況下提供的組件提供的消息）";
$MESS["IB_COMPLIB_PARAMETER_SET_STATUS_404"] = "設置狀態404";
$MESS["IB_COMPLIB_PARAMETER_SHOW_404"] = "顯示頁面";
$MESS["IB_COMPLIB_POPUP_CATALOG"] = "目錄字段";
$MESS["IB_COMPLIB_POPUP_CATALOG_MEASURE"] = "測量單位";
$MESS["IB_COMPLIB_POPUP_CATALOG_STORE_LIST"] = "倉庫";
$MESS["IB_COMPLIB_POPUP_CATALOG_WEIGHT"] = "重量";
$MESS["IB_COMPLIB_POPUP_ELEMENT"] = "元素字段";
$MESS["IB_COMPLIB_POPUP_ELEMENT_CODE"] = "元素瘋子代碼";
$MESS["IB_COMPLIB_POPUP_ELEMENT_DETAIL_TEXT"] = "當前元素詳細說明";
$MESS["IB_COMPLIB_POPUP_ELEMENT_EXTERNAL_ID_MSGVER_1"] = "元素外部ID";
$MESS["IB_COMPLIB_POPUP_ELEMENT_ID"] = "元素ID";
$MESS["IB_COMPLIB_POPUP_ELEMENT_LOWER_NAME"] = "當前元素名稱，小寫";
$MESS["IB_COMPLIB_POPUP_ELEMENT_NAME"] = "當前元素名稱";
$MESS["IB_COMPLIB_POPUP_ELEMENT_PREVIEW_TEXT"] = "當前元素預覽文本";
$MESS["IB_COMPLIB_POPUP_IBLOCK"] = "信息塊字段";
$MESS["IB_COMPLIB_POPUP_IBLOCK_CODE"] = "inf。塊磁符代碼";
$MESS["IB_COMPLIB_POPUP_IBLOCK_EXTERNAL_ID_MSGVER_1"] = "inf。阻止外部ID";
$MESS["IB_COMPLIB_POPUP_IBLOCK_ID"] = "inf。塊ID";
$MESS["IB_COMPLIB_POPUP_IBLOCK_NAME"] = "信息塊名稱";
$MESS["IB_COMPLIB_POPUP_IBLOCK_TEXT"] = "信息塊描述";
$MESS["IB_COMPLIB_POPUP_IBLOCK_TYPE_ID"] = "inf。塊類型";
$MESS["IB_COMPLIB_POPUP_MAX_PRICE"] = "最大限度：";
$MESS["IB_COMPLIB_POPUP_MIN_PRICE"] = "最低限度：";
$MESS["IB_COMPLIB_POPUP_MISC"] = "更多的";
$MESS["IB_COMPLIB_POPUP_PARENT"] = "父字段";
$MESS["IB_COMPLIB_POPUP_PARENT_CODE"] = "父符號代碼";
$MESS["IB_COMPLIB_POPUP_PARENT_NAME"] = "父母名字";
$MESS["IB_COMPLIB_POPUP_PARENT_TEXT"] = "父母描述";
$MESS["IB_COMPLIB_POPUP_PRICE"] = "價格";
$MESS["IB_COMPLIB_POPUP_PROPERTIES"] = "特性";
$MESS["IB_COMPLIB_POPUP_SECTION"] = "部分字段";
$MESS["IB_COMPLIB_POPUP_SECTIONS_PATH"] = "當前元素路徑";
$MESS["IB_COMPLIB_POPUP_SECTION_CODE"] = "節磁有代碼";
$MESS["IB_COMPLIB_POPUP_SECTION_CODE_PATH"] = "嵌套部分的助記符代碼";
$MESS["IB_COMPLIB_POPUP_SECTION_EXTERNAL_ID_MSGVER_1"] = "部分外部ID";
$MESS["IB_COMPLIB_POPUP_SECTION_ID"] = "章節ID";
$MESS["IB_COMPLIB_POPUP_SECTION_LOWER_NAME"] = "當前部分名稱，小寫";
$MESS["IB_COMPLIB_POPUP_SECTION_NAME"] = "當前部分名稱";
$MESS["IB_COMPLIB_POPUP_SECTION_PREVIEW_TEXT"] = "當前部分說明";
$MESS["IB_COMPLIB_POPUP_SERVER_NAME"] = "網站URL";
$MESS["IB_COMPLIB_POPUP_SITE_DIR"] = "站點根文件夾";
$MESS["IB_COMPLIB_POPUP_SKU_PRICE"] = "SKU價格";
$MESS["IB_COMPLIB_POPUP_SKU_PROPERTIES"] = "SKU屬性";
$MESS["IB_COMPLIB_POPUP_STORE"] = "倉庫";
$MESS["IB_COMPLIB_POPUP_STORE_LIST"] = "倉庫";
$MESS["T_IBLOCK_DESC_BASE_LINK"] = "鏈接的基本URL（默認為自動）";
$MESS["T_IBLOCK_DESC_BASE_LINK_ENABLE"] = "啟用鏈接處理";
$MESS["T_IBLOCK_DESC_BOTTOM_PAGER"] = "顯示在列表的底部";
$MESS["T_IBLOCK_DESC_PAGER_DESC_NUMBERING"] = "使用反向頁面導航";
$MESS["T_IBLOCK_DESC_PAGER_DESC_NUMBERING_CACHE_TIME"] = "帶有反向頁面導航的頁面的緩存時間";
$MESS["T_IBLOCK_DESC_PAGER_SETTINGS"] = "Pager設置";
$MESS["T_IBLOCK_DESC_PAGER_SHOW_ALWAYS"] = "始終顯示尋呼機";
$MESS["T_IBLOCK_DESC_PAGER_TEMPLATE"] = "Pager模板的名稱";
$MESS["T_IBLOCK_DESC_PAGER_TEMPLATE_DEFAULT"] = "預設";
$MESS["T_IBLOCK_DESC_PAGER_TEMPLATE_EXT"] = "麵包屑導航模板";
$MESS["T_IBLOCK_DESC_PAGER_TEMPLATE_SITE_DEFAULT"] = "通用模板";
$MESS["T_IBLOCK_DESC_PAGER_TEMPLATE_SYSTEM"] = "內置模板";
$MESS["T_IBLOCK_DESC_PAGER_TITLE"] = "分類名稱";
$MESS["T_IBLOCK_DESC_PARAMS_NAME"] = "帶有變量構建鏈接的數組的名稱";
$MESS["T_IBLOCK_DESC_SHOW_ALL"] = "顯示全鏈接";
$MESS["T_IBLOCK_DESC_TOP_PAGER"] = "顯示在列表的頂部";
