<?php
$MESS["IBLOCK_BAD_ACTIVE_FROM"] = "不正確的啟動日期格式。";
$MESS["IBLOCK_BAD_ACTIVE_TO"] = "不正確到期日期格式。";
$MESS["IBLOCK_BAD_ELEMENT_NAME"] = "名稱未指定。";
$MESS["IBLOCK_BAD_FIELD"] = "未指定所需字段\“＃field_name＃\”。";
$MESS["IBLOCK_BAD_PROPERTY"] = "所需屬性\“＃屬性＃\”未指定。";
$MESS["IBLOCK_DUP_ELEMENT_CODE"] = "具有此符號代碼的元素已經存在。";
$MESS["IBLOCK_ERR_DETAIL_PICTURE"] = "錯誤保存詳細信息圖像。";
$MESS["IBLOCK_ERR_FILE_PROPERTY"] = "在屬性中保存錯誤文件。";
$MESS["IBLOCK_ERR_PREVIEW_PICTURE"] = "錯誤保存預覽圖像。";
