<?php
$MESS["ACTIVE_DATE_FORMAT_TIP"] = "在此處選擇所需的日期格式。如果選擇<i> <b>其他</b> </i>，則可以使用<i> <b> date </b> </i> php函數創建自己的格式。";
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>不緩存</i>：沒有執行緩存。";
$MESS["DETAIL_URL_TIP"] = "包含信息塊元素內容的頁面的URL。 URL應包括一個參數以傳遞元素ID，例如：<b> news_detail.php？id =＃element_id＃</b>";
$MESS["FIELD_CODE_TIP"] = "在此處選擇要在所有新聞</b>頁面中顯示的字段。如果選擇<i> <b>無</b> </i>，並且不定義下面的字段表達式，則將顯示默認字段。";
$MESS["FILTER_NAME_TIP"] = "將傳遞過濾器設置的變量的名稱。默認值是<b> arrfilter </b>。";
$MESS["IBLOCKS_TIP"] = "在此處選擇現有信息塊之一。您可以通過將CTRL固定來選擇多個項目。";
$MESS["IBLOCK_SORT_BY_TIP"] = "指定要分類信息塊的字段。";
$MESS["IBLOCK_SORT_ORDER_TIP"] = "將分類信息塊的方向。";
$MESS["IBLOCK_TYPE_TIP"] = "選擇列表中的現有信息塊類型之一，然後單擊<b> ok </b>。這將加載所選類型的信息塊。";
$MESS["IBLOCK_URL_TIP"] = "包含信息塊元素的頁面的URL。 URL應該包括一個參數以傳遞信息塊ID，例如：<b> news.php？id =＃iblock_id＃</b>";
$MESS["NEWS_COUNT_TIP"] = "指定從信息塊中提取的元素數量。";
$MESS["PROPERTY_CODE_TIP"] = "選擇要在過濾器中顯示的信息塊屬性。選擇<b>（無）</b>並且不指定屬性代碼將導致不顯示屬性。";
$MESS["SORT_BY1_TIP"] = "指定首先對新聞項目進行排序的字段。否則，您可以選擇<b> <i>其他</i> </b>並在旁邊的字段中定義ID。";
$MESS["SORT_BY2_TIP"] = "指定博客組將在第二次通過的字段中進行分類。";
$MESS["SORT_ORDER1_TIP"] = "指定第一個分類通過的排序順序。";
$MESS["SORT_ORDER2_TIP"] = "指定第一個分類通過的排序順序。";
