<?php
$MESS["AJAX_MODE_TIP"] = "在組件中啟用AJAX。";
$MESS["AJAX_OPTION_HISTORY_TIP"] = "允許\“ Back \”和\“ Forward \”瀏覽器按鈕用於Ajax Transitions。";
$MESS["AJAX_OPTION_JUMP_TIP"] = "當Ajax過渡完成時，指定滾動到組件。";
$MESS["AJAX_OPTION_SHADOW_TIP"] = "指定在Ajax過渡上的陰影修改區域。";
$MESS["AJAX_OPTION_STYLE_TIP"] = "指定在AJAX過渡上下載和處理CSS組件樣式。";
$MESS["COMPARE_URL_TIP"] = "在此處指定當前信息塊元素比較頁面的路徑。";
$MESS["DETAIL_URL_TIP"] = "在此處指定信息塊元素詳細信息頁面的路徑。";
$MESS["IBLOCK_ID_TIP"] = "選擇所選類型的信息塊之一。如果選擇<b>（其他） - ＆gt; </b>，請在旁邊的字段中指定信息塊ID。";
$MESS["IBLOCK_TYPE_TIP"] = "選擇列表中的現有信息塊類型之一，然後單擊<b> ok </b>。這將加載所選類型的信息塊。";
$MESS["NAME_TIP"] = "變量的名稱傳遞要比較的元素列表。默認值是<b> catalog_compare_list </b>。";
