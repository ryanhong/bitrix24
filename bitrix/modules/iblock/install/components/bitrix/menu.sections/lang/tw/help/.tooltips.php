<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>不緩存</i>：沒有執行緩存。";
$MESS["DEPTH_LEVEL_TIP"] = "指定要顯示的嵌套級別的數量。";
$MESS["IBLOCK_ID_TIP"] = "在此處選擇所需的信息塊。";
$MESS["IBLOCK_TYPE_TIP"] = "包含系統中存在的所有信息塊類型。";
$MESS["ID_TIP"] = "評估創建菜單項的部分或元素的ID的表達式。默認值是<b> = {\ $ _請求[\“ id \”]} </b>";
$MESS["SECTION_URL_TIP"] = "部分索引頁的URL。";
