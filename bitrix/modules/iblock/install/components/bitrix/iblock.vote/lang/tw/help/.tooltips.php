<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>不緩存</i>：沒有執行緩存。";
$MESS["ELEMENT_ID_TIP"] = "評估元素ID的代碼。您可以明確指定ID，或從請求數組中獲取它。默認值是<b> = {\ $ _請求[\“ element_id \”]} </b>";
$MESS["IBLOCK_ID_TIP"] = "在此處選擇現有信息塊之一。如果選擇<b> <i>（其他）</i> </b>，則必須在旁邊的字段中指定信息塊ID。";
$MESS["IBLOCK_TYPE_TIP"] = "在此處選擇現有的信息塊類型之一。單擊<b> <i> ok </i> </b>加載所選類型的信息塊。";
$MESS["MAX_VOTE_TIP"] = "指定最大得分（票數）。";
$MESS["VOTE_NAMES_TIP"] = "在這裡，您可以提供將使用的得分標題而不是數字分數。例如：非常適合5，適合4等。";
