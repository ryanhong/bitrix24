<?php
$MESS["CPT_BCSL_HIDE_SECTION_NAME"] = "隱藏小節名稱";
$MESS["CPT_BCSL_SHOW_PARENT_NAME"] = "顯示部分名稱";
$MESS["CPT_BCSL_VIEW_MODE"] = "小節查看模式";
$MESS["CPT_BCSL_VIEW_MODE_LINE"] = "列表";
$MESS["CPT_BCSL_VIEW_MODE_LIST"] = "多級列表";
$MESS["CPT_BCSL_VIEW_MODE_TEXT"] = "文字";
$MESS["CPT_BCSL_VIEW_MODE_TILE"] = "瓦";
$MESS["HIDE_SECTION_NAME_TIP"] = "如果選擇，\“ tile \”視圖模式將僅顯示部分圖像。";
$MESS["SHOW_PARENT_NAME_TIP"] = "指定顯示當前部分的名稱（根部除外）。";
$MESS["VIEW_MODE_TIP"] = "指定小節在頁面上佈局的方式。注意力！只有\“多級列表\”選項可以指定分量的任意計數。對於所有其他視圖模式，小節嵌套深度必須為1。";
