<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>不緩存</i>：沒有執行緩存。";
$MESS["FIELD_CODE_TIP"] = "在此處選擇其他字段以用於過濾器中。";
$MESS["FILTER_NAME_TIP"] = "將傳遞過濾器設置的變量的名稱。您可以將字段留為空，以使用默認名稱。";
$MESS["IBLOCK_ID_TIP"] = "選擇所選類型的信息塊之一。如果選擇<b>（其他） - ＆gt; </b>，請在旁邊的字段中指定信息塊ID。";
$MESS["IBLOCK_TYPE_TIP"] = "在此處選擇現有信息塊類型之一。單擊<b> OK </b>將加載所選類型的信息塊。";
$MESS["LIST_HEIGHT_TIP"] = "此值定義了過濾器中列錶框的高度。";
$MESS["NUMBER_WIDTH_TIP"] = "此值定義了過濾器中文本輸入字段的寬度。";
$MESS["PRICE_CODE_TIP"] = "指定要在目錄中顯示的價格類型。如果沒有選擇任何類型，則價格和<b>購買</b>和</b>添加到籃子</b>按鈕將無法顯示。";
$MESS["PROPERTY_CODE_TIP"] = "選擇要在過濾器中顯示的信息塊屬性。選擇<b>（無）</b>並且不指定屬性代碼將導致不顯示屬性。";
$MESS["SAVE_IN_SESSION_TIP"] = "如果已檢查，則將保存在用戶會話數據中。";
$MESS["TEXT_WIDTH_TIP"] = "此值定義了過濾器中文本輸入字段的寬度。";
