<?php
$MESS["ACTION_VARIABLE_TIP"] = "將傳遞所需操作的變量的名稱：add_to_compare_list，add2basket等。默認值是<i> action </i>。";
$MESS["AJAX_MODE_TIP"] = "在組件中啟用AJAX。";
$MESS["AJAX_OPTION_HISTORY_TIP"] = "允許\“ Back \”和\“ Forward \”瀏覽器按鈕用於Ajax Transitions。";
$MESS["AJAX_OPTION_JUMP_TIP"] = "當Ajax過渡完成時，指定滾動到組件。";
$MESS["AJAX_OPTION_SHADOW_TIP"] = "指定在Ajax過渡上的陰影修改區域。";
$MESS["AJAX_OPTION_STYLE_TIP"] = "指定在AJAX過渡上下載和處理CSS組件樣式。";
$MESS["BASKET_URL_TIP"] = "在此處指定客戶購物車頁面的路徑。";
$MESS["CACHE_FILTER_TIP"] = "激活此選項將導致所有過濾器結果被緩存。";
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>不緩存</i>：沒有執行緩存。";
$MESS["DETAIL_URL_TIP"] = "在此處指定信息塊元素詳細信息頁面的路徑。";
$MESS["DISPLAY_BOTTOM_PAGER_TIP"] = "如果已檢查，則麵包屑導航鏈接將顯示在頁面底部。";
$MESS["DISPLAY_PANEL_TIP"] = "如果已檢查，則在“站點編輯”模式下的管理工具欄和組件編輯區域工具欄上顯示該工具按鈕。";
$MESS["DISPLAY_TOP_PAGER_TIP"] = "如果已檢查，則麵包屑導航鏈接將顯示在頁面頂部。";
$MESS["ELEMENT_ID_TIP"] = "該字段包含一個評估元素ID的表達式，該元素要顯示綁定元素的列表。";
$MESS["ELEMENT_SORT_FIELD_TIP"] = "將分類項目的字段。";
$MESS["ELEMENT_SORT_ORDER_TIP"] = "分類元素的方向。";
$MESS["FILTER_NAME_TIP"] = "將傳遞過濾器設置的變量的名稱。您可以將字段留為空，以使用默認名稱。";
$MESS["IBLOCK_ID_TIP"] = "選擇所選類型的信息塊之一。如果選擇<b>（其他） - ＆gt; </b>，請在旁邊的字段中指定信息塊ID。";
$MESS["IBLOCK_TYPE_TIP"] = "選擇列表中的現有信息塊類型之一，然後單擊<b> ok </b>。這將加載所選類型的信息塊。";
$MESS["LINK_PROPERTY_SID_TIP"] = "在此處選擇“元素綁定屬性”，或將其指定為下一個字段中的代碼。";
$MESS["PAGER_DESC_NUMBERING_CACHE_TIME_TIP"] = "是時候以幾秒鐘的時間來緩存第一頁以進行向後導航。";
$MESS["PAGER_DESC_NUMBERING_TIP"] = "如果您希望將新元素放在頂部，請使用此選項。因此，僅修改了麵包屑導航結構中的最後一頁。所有其他頁面都可以緩存很長時間。";
$MESS["PAGER_SHOW_ALWAYS_TIP"] = "如果檢查了，即使所有元素都適合頁面，也將顯示麵包屑導航控件。";
$MESS["PAGER_TEMPLATE_TIP"] = "麵包屑導航模板的名稱。如果為空，將使用默認模板（<b> .default </b>）。另外，您可以使用<b>橙色</b>模板。";
$MESS["PAGER_TITLE_TIP"] = "麵包屑導航將瀏覽的類別名稱。";
$MESS["PAGE_ELEMENT_COUNT_TIP"] = "指定每個頁面元素的數量。";
$MESS["PRICE_CODE_TIP"] = "指定目錄中要使用的價格類型。如果您省略了價格類型，價格和按鈕</b>和<b>添加到籃子</b>將不會顯示。";
$MESS["PRICE_VAT_INCLUDE_TIP"] = "檢查此選項指定在顯示價格中包括稅款。";
$MESS["PRODUCT_ID_VARIABLE_TIP"] = "將通過產品ID的變量的名稱。";
$MESS["PROPERTY_CODE_TIP"] = "選擇要在元素列表中顯示的信息塊屬性。選擇<i> nonne </i>，並且不指定下面的屬性代碼將導致屬性未顯示。";
$MESS["SECTION_ID_VARIABLE_TIP"] = "信息塊部分ID的變量名稱。";
$MESS["SECTION_URL_TIP"] = "在此處指定信息塊部分描述的路徑。";
$MESS["SET_TITLE_TIP"] = "如果已檢查，則頁面標題將設置為綁定信息塊的名稱。";
$MESS["SHOW_PRICE_COUNT_TIP"] = "如果未選中“使用價格範圍”選項（這意味著僅顯示一個價格），但是有基於數量的定價產品，請指定數量以選擇正確的價格。此選項對固定價格的產品沒有影響。";
$MESS["USE_PRICE_COUNT_TIP"] = "如果已檢查，將選擇所有現有的價格範圍。";
