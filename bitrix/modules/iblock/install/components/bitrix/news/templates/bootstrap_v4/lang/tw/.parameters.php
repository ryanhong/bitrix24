<?php
$MESS["TP_BN_LIST_USE_SHARE"] = "在列表中顯示共享工具欄";
$MESS["TP_BN_MEDIA_PROPERTY"] = "媒體屬性";
$MESS["TP_BN_SLIDER_PROPERTY"] = "包含滑塊圖像的屬性";
$MESS["TP_BN_TEMPLATE_THEME"] = "顏色主題";
$MESS["TP_BN_THEME_BLUE"] = "藍色（默認主題）";
$MESS["TP_BN_THEME_GREEN"] = "綠色的";
$MESS["TP_BN_THEME_RED"] = "紅色的";
$MESS["TP_BN_THEME_SITE"] = "使用站點主題（用於bitrix.shop）";
$MESS["TP_BN_THEME_YELLOW"] = "黃色的";
$MESS["T_IBLOCK_DESC_NEWS_DATE"] = "顯示元素日期";
$MESS["T_IBLOCK_DESC_NEWS_PICTURE"] = "顯示元素預覽圖片";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SHORTEN_URL_KEY"] = "bit.ly鍵";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SHORTEN_URL_LOGIN"] = "bit.ly登錄";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SYSTEM"] = "使用社交網絡和書籤";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_TEMPLATE"] = "社交網絡書籤模板";
$MESS["T_IBLOCK_DESC_NEWS_TEXT"] = "顯示元素預覽文本";
$MESS["T_IBLOCK_DESC_NEWS_USE_SHARE"] = "顯示社交網絡書籤欄";
