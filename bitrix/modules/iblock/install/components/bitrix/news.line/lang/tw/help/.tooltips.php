<?php
$MESS["ACTIVE_DATE_FORMAT_TIP"] = "在此處選擇所需的日期格式。如果選擇<i> <b>其他</b> </i>，則可以使用<i> <b> date </b> </i> php函數創建自己的格式。";
$MESS["CACHE_TIME_TIP"] = "在幾秒鐘內指定緩存時間。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>不緩存</i>：沒有執行緩存。";
$MESS["DETAIL_URL_TIP"] = "包含信息塊元素內容的頁面的URL。 URL應包括一個參數以傳遞元素ID，例如：<b> news_detail.php？id =＃element_id＃</b>";
$MESS["IBLOCKS_TIP"] = "在此處選擇現有信息塊之一。您可以通過持有<b> ctrl </b>向下選擇多個項目。";
$MESS["IBLOCK_TYPE_TIP"] = "在此處選擇現有的信息塊類型之一。單擊<b> <i> ok </i> </b>加載所選類型的信息塊。";
$MESS["NEWS_COUNT_TIP"] = "指定每個頁面元素的數量。";
$MESS["SORT_BY1_TIP"] = "指定首先對新聞項目進行排序的字段。否則，您可以選擇<b> <i>其他</i> </b>並在旁邊的字段中定義ID。";
$MESS["SORT_BY2_TIP"] = "指定博客組將在第二次通過的字段中進行分類。";
$MESS["SORT_ORDER1_TIP"] = "指定第一個分類通過的排序順序。";
$MESS["SORT_ORDER2_TIP"] = "指定第一個分類通過的排序順序。";
