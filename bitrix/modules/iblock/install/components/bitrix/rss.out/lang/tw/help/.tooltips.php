<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>不緩存</i>：沒有執行緩存。";
$MESS["IBLOCK_ID_TIP"] = "選擇所選類型的信息塊之一。如果選擇<b>（其他） - ＆gt; </b>，請在旁邊的字段中指定信息塊ID。";
$MESS["IBLOCK_TYPE_TIP"] = "在此處選擇現有的信息塊類型之一。單擊<b> <i> ok </i> </b>加載所選類型的信息塊。";
$MESS["NUM_DAYS_TIP"] = "指定將項目導出到RSS的最近幾天的數量。例如：30天。";
$MESS["NUM_NEWS_TIP"] = "指定RSS導出的新聞項目計數。";
$MESS["RSS_TTL_TIP"] = "在計劃更新之前，在幾分鐘內指定RSS緩存壽命的周期。";
$MESS["SECTION_ID_TIP"] = "指定其中內容將導出到RSS的部分的ID或符號代碼。";
$MESS["SORT_BY1_TIP"] = "指定首先對新聞項目進行排序的字段。否則，您可以選擇<b> <i>其他</i> </b>並在旁邊的字段中定義ID。";
$MESS["SORT_BY2_TIP"] = "指定博客組將在第二次通過的字段中進行分類。";
$MESS["SORT_ORDER1_TIP"] = "指定第一個分類通過的排序順序。";
$MESS["SORT_ORDER2_TIP"] = "指定第一個分類通過的排序順序。";
$MESS["YANDEX_TIP"] = "啟用導出到<i> yandex.news </i>。指定最近兩天出口新聞項目。";
