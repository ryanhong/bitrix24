<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>不緩存</i>：沒有執行緩存。";
$MESS["NUM_NEWS_TIP"] = "指定要導出的新聞項目數量的最大數量。";
$MESS["OUT_CHANNEL_TIP"] = "如果您的RSS文件在<b>＆lt; channel＆gt; </b>之外，請檢查此選項。";
$MESS["PATH_TIP"] = "網站上RSS生成器腳本的路徑。例如：<b>/bitrix/rss.php </b>";
$MESS["PORT_TIP"] = "用於傳輸RSS Feed的TCP端口。通常80。";
$MESS["QUERY_STR_TIP"] = "示例：<Nobr> <b> id = news_sm＆lang = ru＆type = news＆limit = 5 </nobr> </b>。您的請求必須符合HTTP。";
$MESS["SITE_TIP"] = "指定將要出口新聞項目的網站的URL。默認值是www.bitrixsoft.com。";
