<?php
$MESS["CATALOG_ADD"] = "添加到購物車";
$MESS["CATALOG_BUY"] = "買";
$MESS["CATALOG_COMPARE"] = "比較";
$MESS["CATALOG_NOT_AVAILABLE"] = "（庫存不可用）";
$MESS["CATALOG_QUANTITY"] = "數量";
$MESS["CATALOG_QUANTITY_FROM"] = "＃來自＃以及更多";
$MESS["CATALOG_QUANTITY_FROM_TO"] = "從＃從＃到＃到＃";
$MESS["CATALOG_QUANTITY_TO"] = "到＃＃";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "鏈接到此記錄的所有信息將被刪除。無論如何繼續？";
$MESS["CT_BCS_QUANTITY"] = "數量";
$MESS["PUB_DATE"] = "發布日期：";
