<?php
$MESS["ADD_PICT_PROP_TIP"] = "產品的其他圖像屬性";
$MESS["ADD_TO_BASKET_ACTION_ADD"] = "添加到購物車";
$MESS["ADD_TO_BASKET_ACTION_BUY"] = "立即購買";
$MESS["CPT_BCS_SECTIONS_OFFSET_MODE"] = "部分塊的初始滾動位置";
$MESS["CPT_BCS_SECTIONS_OFFSET_MODE_DYNAMIC"] = "在請求中指定";
$MESS["CPT_BCS_SECTIONS_OFFSET_MODE_FIXED"] = "靜態值";
$MESS["CPT_BCS_SECTIONS_OFFSET_MODE_NO"] = "沒有任何";
$MESS["CPT_BCS_SECTIONS_OFFSET_VARIABLE"] = "請求參數指定滾動位置";
$MESS["CP_BCS_TPL_ADD_PICT_PROP"] = "主要項目的其他圖像";
$MESS["CP_BCS_TPL_ADD_TO_BASKET_ACTION"] = "顯示\“添加到購物車\”或\“立即購買\”按鈕";
$MESS["CP_BCS_TPL_BESTSELLERS"] = "賣出";
$MESS["CP_BCS_TPL_BRAND_PROPERTY"] = "品牌財產";
$MESS["CP_BCS_TPL_COMPARE_NAME"] = "比較圖表的唯一名稱";
$MESS["CP_BCS_TPL_CYCLIC_LOADING"] = "無限滾動";
$MESS["CP_BCS_TPL_CYCLIC_LOADING_COUNTER_NAME"] = "請求參數指定計數器值";
$MESS["CP_BCS_TPL_DATA_LAYER_NAME"] = "數據容器名稱";
$MESS["CP_BCS_TPL_DEFERRED_LOAD"] = "延遲負載";
$MESS["CP_BCS_TPL_DML_EXT"] = "擴展";
$MESS["CP_BCS_TPL_DML_SIMPLE"] = "簡單模式";
$MESS["CP_BCS_TPL_LABEL_PROP"] = "產品標籤屬性";
$MESS["CP_BCS_TPL_LABEL_PROP_MOBILE"] = "移動設備的產品標籤屬性";
$MESS["CP_BCS_TPL_LABEL_PROP_POSITION"] = "產品標籤位置";
$MESS["CP_BCS_TPL_LAZY_LOAD"] = "顯示懶負載按鈕";
$MESS["CP_BCS_TPL_LOAD_ON_SCROLL"] = "在滾動上加載更多產品";
$MESS["CP_BCS_TPL_MESS_BTN_ADD_TO_BASKET"] = "\“添加到購物車\”按鈕文本";
$MESS["CP_BCS_TPL_MESS_BTN_ADD_TO_BASKET_DEFAULT"] = "添加到購物車";
$MESS["CP_BCS_TPL_MESS_BTN_BUY"] = "\“購買\”按鈕文字";
$MESS["CP_BCS_TPL_MESS_BTN_BUY_DEFAULT"] = "買";
$MESS["CP_BCS_TPL_MESS_BTN_COMPARE"] = "\“比較\”按鈕文本";
$MESS["CP_BCS_TPL_MESS_BTN_COMPARE_DEFAULT"] = "比較";
$MESS["CP_BCS_TPL_MESS_BTN_DETAIL"] = "\“詳細信息\”按鈕文字";
$MESS["CP_BCS_TPL_MESS_BTN_DETAIL_DEFAULT"] = "細節";
$MESS["CP_BCS_TPL_MESS_BTN_LAZY_LOAD"] = "\“顯示更多\”按鈕文本";
$MESS["CP_BCS_TPL_MESS_BTN_LAZY_LOAD_DEFAULT"] = "展示更多";
$MESS["CP_BCS_TPL_MESS_BTN_SUBSCRIBE"] = "\“返回庫存\”按鈕文本";
$MESS["CP_BCS_TPL_MESS_BTN_SUBSCRIBE_DEFAULT"] = "通知何時庫存";
$MESS["CP_BCS_TPL_MESS_NOT_AVAILABLE"] = "項目不可用的消息";
$MESS["CP_BCS_TPL_MESS_NOT_AVAILABLE_DEFAULT"] = "項目不可用";
$MESS["CP_BCS_TPL_MESS_NOT_AVAILABLE_SERVICE"] = "服務不可用的消息";
$MESS["CP_BCS_TPL_MESS_NOT_AVAILABLE_SERVICE_DEFAULT"] = "不可用";
$MESS["CP_BCS_TPL_MESS_RELATIVE_QUANTITY_FEW"] = "值的文本小於計算";
$MESS["CP_BCS_TPL_MESS_RELATIVE_QUANTITY_FEW_DEFAULT"] = "只剩下幾個";
$MESS["CP_BCS_TPL_MESS_RELATIVE_QUANTITY_MANY"] = "值大於計算的值";
$MESS["CP_BCS_TPL_MESS_RELATIVE_QUANTITY_MANY_DEFAULT"] = "有存貨";
$MESS["CP_BCS_TPL_MESS_SHOW_MAX_QUANTITY"] = "餘額文字";
$MESS["CP_BCS_TPL_MESS_SHOW_MAX_QUANTITY_DEFAULT"] = "庫存";
$MESS["CP_BCS_TPL_OFFER_ADD_PICT_PROP"] = "SKU的其他圖像屬性";
$MESS["CP_BCS_TPL_OFFER_TREE_PROPS"] = "Skus分組的屬性";
$MESS["CP_BCS_TPL_PERSONAL"] = "個人建議";
$MESS["CP_BCS_TPL_PERSONAL_WBEST"] = "賣出/個人";
$MESS["CP_BCS_TPL_PRODUCT_DISPLAY_MODE"] = "查看模式";
$MESS["CP_BCS_TPL_PRODUCT_ID_PARAM"] = "產品ID參數（建議）";
$MESS["CP_BCS_TPL_PRODUCT_SUBSCRIPTION"] = "啟用後庫存通知";
$MESS["CP_BCS_TPL_PROPERTY_CODE_MOBILE"] = "移動設備的產品屬性";
$MESS["CP_BCS_TPL_PROP_EMPTY"] = "未選中的";
$MESS["CP_BCS_TPL_RAND"] = "任何建議";
$MESS["CP_BCS_TPL_RELATIVE_QUANTITY_FACTOR"] = "如果超過股票價值";
$MESS["CP_BCS_TPL_SECTIONS_SECTION_CODE"] = "截面代碼顯示小節";
$MESS["CP_BCS_TPL_SECTIONS_SECTION_ID"] = "顯示小節的部分ID";
$MESS["CP_BCS_TPL_SECTIONS_TOP_DEPTH"] = "最大可見兒童段";
$MESS["CP_BCS_TPL_SHOW_CLOSE_POPUP"] = "在彈出窗口中顯示\“繼續購物”按鈕";
$MESS["CP_BCS_TPL_SHOW_FROM_SECTION"] = "在節中顯示產品";
$MESS["CP_BCS_TPL_SHOW_MAX_QUANTITY"] = "顯示產品餘額";
$MESS["CP_BCS_TPL_SHOW_MAX_QUANTITY_M"] = "用文字替換平衡";
$MESS["CP_BCS_TPL_SHOW_MAX_QUANTITY_N"] = "不要顯示";
$MESS["CP_BCS_TPL_SHOW_MAX_QUANTITY_Y"] = "顯示真正的平衡";
$MESS["CP_BCS_TPL_SHOW_OLD_PRICE"] = "顯示以前的價格";
$MESS["CP_BCS_TPL_SHOW_SECTIONS"] = "顯示部分";
$MESS["CP_BCS_TPL_SIMILAR"] = "類似的產品";
$MESS["CP_BCS_TPL_SIMILAR_ANY"] = "也購買/查看/類似產品";
$MESS["CP_BCS_TPL_SOLD_WITH"] = "也買了";
$MESS["CP_BCS_TPL_TEMPLATE_THEME"] = "顏色主題";
$MESS["CP_BCS_TPL_THEME_BLUE"] = "藍色（默認主題）";
$MESS["CP_BCS_TPL_THEME_GREEN"] = "綠色的";
$MESS["CP_BCS_TPL_THEME_RED"] = "紅色的";
$MESS["CP_BCS_TPL_THEME_SITE"] = "使用站點主題（用於bitrix.shop）";
$MESS["CP_BCS_TPL_THEME_YELLOW"] = "黃色的";
$MESS["CP_BCS_TPL_TYPE_TITLE"] = "建議類型";
$MESS["CP_BCS_TPL_USE_ENHANCED_ECOMMERCE"] = "將電子商務數據提交給Google";
$MESS["CP_BCS_TPL_USE_OFFER_NAME"] = "使用SKU名稱";
$MESS["CP_BCS_TPL_VIEWED_WITH"] = "也觀看了";
$MESS["LABEL_PROP_TIP"] = "產品標籤屬性";
$MESS["MESS_BTN_ADD_TO_BASKET_TIP"] = "在按鈕上顯示要顯示的文字";
$MESS["MESS_BTN_BUY_TIP"] = "在按鈕上顯示要顯示的文字";
$MESS["MESS_BTN_COMPARE_TIP"] = "在按鈕上顯示要顯示的文字";
$MESS["MESS_BTN_DETAIL_TIP"] = "在按鈕上顯示要顯示的文字";
$MESS["MESS_BTN_SUBSCRIBE_TIP"] = "在按鈕上顯示要顯示的文字";
$MESS["MESS_NOT_AVAILABLE_TIP"] = "項目不可用的消息";
$MESS["OFFER_ADD_PICT_PROP_TIP"] = "屬性包含SKU的額外圖像（如果存在）";
$MESS["OFFER_TREE_PROPS_TIP"] = "將將SKU分組和顯示的屬性列表";
$MESS["PRODUCT_DISPLAY_MODE_TIP"] = "項目顯示模式（有或沒有SKU等）";
$MESS["PRODUCT_SUBSCRIPTION_TIP"] = "使系統能夠將他或她特別感興趣的先前庫存物品通知客戶。";
$MESS["RELATIVE_QUANTITY_FACTOR_TIP"] = "將此值與\“股票數量\” / \“單位比率\”的結果進行比較。";
$MESS["SHOW_CLOSE_POPUP_TIP"] = "如果檢查了，彈出窗口將顯示\“繼續購物\”按鈕";
$MESS["SHOW_OLD_PRICE_TIP"] = "如果折扣處於活動狀態，請顯示以前的價格";
$MESS["TEMPLATE_THEME_TIP"] = "定義用於渲染網站文本和圖形的顏色。藍色主題是默認選項。";
$MESS["USE_ENHANCED_ECOMMERCE_TIP"] = "這需要Google Analytics（分析）增強的電子商務選項";
