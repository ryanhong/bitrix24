<?php
$MESS["P_ALBUM_IS_NOT_ACTIVE"] = "專輯是隱藏的。";
$MESS["P_ALBUM_IS_NOT_ACTIVE_AND_PASSWORDED"] = "專輯被隱藏並受到密碼保護。";
$MESS["P_ALBUM_IS_PASSWORDED"] = "密碼保護。";
$MESS["P_ALBUM_IS_PASSWORDED_SHORT"] = "私人專輯";
$MESS["P_EDIT_ICON"] = "選擇封面";
$MESS["P_EDIT_ICON_TITLE"] = "選擇專輯封面";
$MESS["P_EMPTY_DATA"] = "還沒有添加專輯";
$MESS["P_NO_PHOTOS"] = "沒有照片";
$MESS["P_OTHER_PHOTOS"] = "所有專輯照片";
$MESS["P_OTHER_PHOTOS_TITLE"] = "查看其他照片";
$MESS["P_SECTION_DELETE"] = "刪除";
$MESS["P_SECTION_DELETE_ASK"] = "確定您想不可逆轉地刪除專輯嗎？";
$MESS["P_SECTION_DELETE_TITLE"] = "刪除專輯";
$MESS["P_SECTION_EDIT"] = "編輯";
$MESS["P_SECTION_EDIT_TITLE"] = "編輯專輯";
$MESS["P_SECT_PHOTOS"] = "照片";
