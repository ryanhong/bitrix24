<?php
$MESS["P_CLOSE_TITLE"] = "關閉幻燈片";
$MESS["P_COMMENT"] = "評論";
$MESS["P_COMMENTS"] = "評論";
$MESS["P_COMMENTS_2"] = "評論";
$MESS["P_DETAIL_INFO"] = "查看詳情";
$MESS["P_OF"] = "的";
$MESS["P_PAUSE_TITLE"] = "暫停";
$MESS["P_PHOTOS"] = "照片";
$MESS["P_PLAY_TITLE"] = "播放幻燈片";
$MESS["P_SEK"] = "s。";
$MESS["P_SHOW"] = "看法";
$MESS["P_SHOWS"] = "觀點";
$MESS["P_SHOWS_2"] = "觀點";
$MESS["P_SPEED"] = "間隔";
$MESS["P_SPEED_TITLE"] = "幻燈片顯示速度";
$MESS["P_TO_COMMENT"] = "評論";
