<?php
$MESS["IBLOCK_DETAIL_SLIDE_SHOW_URL"] = "幻燈片顯示";
$MESS["IBLOCK_GALLERY_URL"] = "畫廊內容";
$MESS["IBLOCK_IBLOCK"] = "信息塊";
$MESS["IBLOCK_INDEX_URL"] = "部分列表";
$MESS["IBLOCK_SECTION_CODE"] = "部分代碼";
$MESS["IBLOCK_SECTION_EDIT_ICON_URL"] = "專輯（封面編輯）";
$MESS["IBLOCK_SECTION_EDIT_URL"] = "專輯（編輯）";
$MESS["IBLOCK_SECTION_ID"] = "章節ID";
$MESS["IBLOCK_SECTION_URL"] = "頁面的URL帶有部分內容";
$MESS["IBLOCK_TYPE"] = "信息塊類型";
$MESS["IBLOCK_UPLOAD_URL"] = "上傳圖片";
$MESS["P_ALBUM_PHOTO_THUMBS_WIDTH"] = "縮略圖寬度（PX）";
$MESS["P_ALBUM_PHOTO_WIDTH"] = "專輯照片寬度（PX）";
$MESS["P_BEHAVIOUR"] = "畫廊模式";
$MESS["P_GALLERY_SIZE"] = "畫廊的大小";
$MESS["P_RETURN_SECTION_INFO"] = "返回專輯數據陣列";
$MESS["P_SET_STATUS_404"] = "設置404狀態，如果找不到元素或部分";
$MESS["P_USER_ALIAS"] = "畫廊代碼";
$MESS["T_DATE_TIME_FORMAT"] = "日期格式";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "顯示此組件的面板按鈕";
