<?php
$MESS["P_BEST_PHOTO"] = "最好的";
$MESS["P_BEST_PHOTOS"] = "吸引人的";
$MESS["P_GALLERIES"] = "畫廊";
$MESS["P_NOT_APPROVED"] = "未批准的照片";
$MESS["P_NOT_APPROVED_TITLE"] = "索引頁面上所有訪問者都可以看到的照片，但仍未批准。";
$MESS["P_NOT_MODERATED"] = "未批准";
$MESS["P_NOT_MODERATED_TITLE"] = "適度的照片正在等待。";
$MESS["P_PHOTO_COMMENT"] = "大多數評論";
$MESS["P_PHOTO_NEW"] = "新照片";
$MESS["P_PHOTO_NEW_ALL"] = "新照片";
$MESS["P_PHOTO_POPULAR"] = "受歡迎的";
$MESS["P_TAGS_ALL"] = "所有標籤";
$MESS["P_TAGS_POPULAR"] = "流行標籤";
$MESS["P_TITLE"] = "相片";
$MESS["P_VIEW_ALL_GALLERIES"] = "查看所有畫廊";
