<?php
$MESS["P_DELETE"] = "刪除";
$MESS["P_FILTER_RESET"] = "重置";
$MESS["P_FILTER_SHOW"] = "展示";
$MESS["P_GROUP_BY_DATE_CREATE"] = "創建日期的集體照片";
$MESS["P_NOT_ACTIVE_PHOTO"] = "未批准的照片";
$MESS["P_NOT_ACTIVE_PHOTO_2"] = "隱藏的照片等待批准";
$MESS["P_NOT_PUBLIC"] = "不要發布";
$MESS["P_NOT_PULIC_PHOTO"] = "未發表的照片";
$MESS["P_NOT_PULIC_PHOTO_2"] = "等待在主頁上出版的照片";
$MESS["P_PHOTO_ORDER_BY_COMMENTS"] = "大多數評論的照片";
$MESS["P_PHOTO_ORDER_BY_DATE_CREATE"] = "新照片";
$MESS["P_PHOTO_ORDER_BY_RATING"] = "最好的照片";
$MESS["P_PHOTO_ORDER_BY_SHOWS"] = "流行照片";
$MESS["P_PHOTO_SORT_COMMENTS"] = "大多數評論";
$MESS["P_PHOTO_SORT_COMMENTS_TITLE"] = "通過計數投票";
$MESS["P_PHOTO_SORT_ID"] = "新的";
$MESS["P_PHOTO_SORT_ID_TITLE"] = "按創建日期";
$MESS["P_PHOTO_SORT_RATING"] = "最好的";
$MESS["P_PHOTO_SORT_RATING_TITLE"] = "通過投票計數";
$MESS["P_PHOTO_SORT_SHOWS"] = "最受關注";
$MESS["P_PHOTO_SORT_SHOWS_TITLE"] = "通過查看計數";
$MESS["P_PUBLIC"] = "發布";
$MESS["P_SELECT_ALL"] = "全選";
$MESS["P_SELECT_PHOTO_FROM_PERIOD"] = "顯示在期間添加的照片";
$MESS["P_SET_ACTIVE"] = "批准";
