<?php
$MESS["P_COLOR_00FFFF"] = "青色";
$MESS["P_COLOR_000000"] = "黑色的";
$MESS["P_COLOR_008000"] = "綠色的";
$MESS["P_COLOR_800080"] = "紫色的";
$MESS["P_COLOR_FF0000"] = "紅色的";
$MESS["P_COLOR_FFA500"] = "橘子";
$MESS["P_COLOR_FFFF00"] = "黃色的";
$MESS["P_COLOR_FFFFFF"] = "白色的";
$MESS["P_INDEX_PAGE_TOP_ELEMENTS_COUNT"] = "[索引頁]最大。頂部照片";
$MESS["P_MODERATE"] = "[索引頁]啟用照片";
$MESS["P_PUBLIC_BY_DEFAULT"] = "[上傳照片頁]檢查\\'發布\\'選項";
$MESS["P_SHOW_CONTROLS_BUTTONS"] = "顯示控件";
$MESS["P_SHOW_ONLY_PUBLIC"] = "[索引頁]僅顯示已發布的照片";
$MESS["P_SHOW_WATERMARK"] = "[上傳照片頁]顯示水印模板";
$MESS["P_SLIDER_COUNT_CELL"] = "[詳細頁面]滑塊中的照片數";
$MESS["P_USE_LIGHT_TEMPLATE"] = "使用垂直模板進行大規模上傳";
$MESS["P_WATERMARK_COLORS"] = "[上傳照片頁]水印顏色";
