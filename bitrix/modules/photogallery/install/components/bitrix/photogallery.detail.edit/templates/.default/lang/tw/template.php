<?php
$MESS["P_ACTIVE_ELEMENT"] = "批准照片（如果沒有選中，只有所有者才能看到照片）";
$MESS["P_ALBUMS"] = "專輯";
$MESS["P_APPROVE_ELEMENT"] = "批准照片";
$MESS["P_CANCEL"] = "取消";
$MESS["P_DATE"] = "日期";
$MESS["P_DESCRIPTION"] = "描述";
$MESS["P_EDIT_ELEMENT"] = "編輯照片屬性";
$MESS["P_PUBLIC_ELEMENT"] = "發布照片";
$MESS["P_SUBMIT"] = "節省";
$MESS["P_TAGS"] = "標籤";
$MESS["P_TITLE"] = "姓名";
