<?php
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "信息塊未安裝";
$MESS["IBLOCK_WRONG_SESSION"] = "您的會議已經過期。請再次保存您的文檔。";
$MESS["PHOTO_ELEMENT_NOT_FOUND"] = "沒有找到照片。";
$MESS["P_BAD_SECTION"] = "專輯不屬於這個畫廊。";
$MESS["P_DENIED_ACCESS"] = "拒絕訪問。";
$MESS["P_GALLERY_EMPTY"] = "畫廊代碼未指定。";
$MESS["P_MODULE_IS_NOT_INSTALLED"] = "未安裝模塊照片庫2.0。";
