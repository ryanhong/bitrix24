<?php
$MESS["IBLOCK_BEHAVIOUR"] = "畫廊模式";
$MESS["IBLOCK_BEHAVIOUR_SIMPLE"] = "簡單的";
$MESS["IBLOCK_BEHAVIOUR_USER"] = "多用戶";
$MESS["IBLOCK_DETAIL_URL"] = "頁面的URL帶有細節內容";
$MESS["IBLOCK_ELEMENT_ID"] = "元素ID";
$MESS["IBLOCK_GALLERY_URL"] = "畫廊內容";
$MESS["IBLOCK_IBLOCK"] = "信息塊";
$MESS["IBLOCK_SECTION_ID"] = "章節ID";
$MESS["IBLOCK_SECTION_URL"] = "頁面的URL帶有部分內容";
$MESS["IBLOCK_TYPE"] = "信息塊類型";
$MESS["IBLOCK_USER_ALIAS"] = "畫廊代碼";
$MESS["P_ACTION"] = "行動";
$MESS["P_GALLERY_SIZE"] = "畫廊的大小";
$MESS["P_SET_STATUS_404"] = "設置404狀態，如果找不到元素或部分";
$MESS["T_DATE_TIME_FORMAT"] = "日期格式";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "顯示此組件的面板按鈕";
