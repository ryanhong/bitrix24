<?php
$MESS["NO_OF_COUNT"] = "＃＃of＃總計＃";
$MESS["P_DROP"] = "刪除";
$MESS["P_DROP_CONFIM"] = "您確定要不可逆地刪除照片嗎？";
$MESS["P_DROP_TITLE"] = "刪除圖像";
$MESS["P_EDIT"] = "編輯";
$MESS["P_EDIT_TITLE"] = "編輯圖像屬性";
$MESS["P_GO_TO_NEXT"] = "下一張照片";
$MESS["P_GO_TO_PREV"] = "以前的照片";
$MESS["P_GO_TO_SECTION"] = "回到專輯";
$MESS["P_ORIGINAL"] = "原來的";
$MESS["P_ORIGINAL_TITLE"] = "圖片原件";
$MESS["P_SLIDE_SHOW"] = "幻燈片顯示";
$MESS["P_SLIDE_SHOW_TITLE"] = "從此圖像開始幻燈片顯示";
$MESS["P_UP"] = "回到專輯";
$MESS["P_UPLOAD"] = "上傳照片";
$MESS["P_UPLOAD_TITLE"] = "上傳照片";
