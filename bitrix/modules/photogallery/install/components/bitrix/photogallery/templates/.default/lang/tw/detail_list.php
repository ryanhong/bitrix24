<?php
$MESS["P_CHOSE_PHOTO_FROM_PERIOD"] = "顯示在期間添加的照片";
$MESS["P_FILTER_RESET"] = "重置";
$MESS["P_FILTER_SHOW"] = "展示";
$MESS["P_PHOTO_SORT_COMMENTS"] = "大多數評論";
$MESS["P_PHOTO_SORT_COMMENTS_TITLE"] = "通過計數投票";
$MESS["P_PHOTO_SORT_ID"] = "新的";
$MESS["P_PHOTO_SORT_ID_TITLE"] = "按創建日期";
$MESS["P_PHOTO_SORT_RATING"] = "最好的";
$MESS["P_PHOTO_SORT_RATING_TITLE"] = "通過投票計數";
$MESS["P_PHOTO_SORT_SHOWS"] = "最受關注";
$MESS["P_PHOTO_SORT_SHOWS_TITLE"] = "通過查看計數";
$MESS["P_TITLE_COMMENTS"] = "大多數評論的照片";
$MESS["P_TITLE_ID"] = "新照片";
$MESS["P_TITLE_RATING"] = "流行照片";
$MESS["P_TITLE_SHOWS"] = "大多數查看的照片";
