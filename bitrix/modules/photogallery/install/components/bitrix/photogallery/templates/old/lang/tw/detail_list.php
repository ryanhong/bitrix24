<?php
$MESS["P_FILTER_SHOW"] = "展示";
$MESS["P_PHOTO_SORT_COMMENTS"] = "大多數評論";
$MESS["P_PHOTO_SORT_COMMENTS_TITLE"] = "通過計數投票";
$MESS["P_PHOTO_SORT_ID"] = "新的";
$MESS["P_PHOTO_SORT_ID_TITLE"] = "按創建日期";
$MESS["P_PHOTO_SORT_RATING"] = "最好的";
$MESS["P_PHOTO_SORT_RATING_TITLE"] = "通過投票計數";
$MESS["P_PHOTO_SORT_SHOWS"] = "最受關注";
$MESS["P_PHOTO_SORT_SHOWS_TITLE"] = "通過查看計數";
$MESS["P_SET_FILTER"] = "設置過濾器";
$MESS["P_UP"] = "向上";
$MESS["P_UP_TITLE"] = "一個水平";
