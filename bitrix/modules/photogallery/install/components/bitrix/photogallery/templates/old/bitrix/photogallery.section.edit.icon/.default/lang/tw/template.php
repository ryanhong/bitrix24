<?php
$MESS["P_BACK_UP"] = "後退";
$MESS["P_BACK_UP_TITLE"] = "後退";
$MESS["P_CANCEL"] = "取消";
$MESS["P_EMPTY_PHOTO"] = "專輯是空的";
$MESS["P_SELECT_PHOTO"] = "選擇封面照片";
$MESS["P_SUBMIT"] = "節省";
$MESS["P_UP"] = "向上";
$MESS["P_UP_TITLE"] = "一個水平";
