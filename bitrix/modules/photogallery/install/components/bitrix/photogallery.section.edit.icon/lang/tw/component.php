<?php
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "信息塊未安裝";
$MESS["IBLOCK_WRONG_SESSION"] = "您的會議已經過期。請再次保存您的文檔。";
$MESS["P_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["P_ALBUM_EMPTY"] = "專輯代碼未指定";
$MESS["P_GALLERY_EMPTY"] = "畫廊代碼未指定";
$MESS["P_MODULE_IS_NOT_INSTALLED"] = "未安裝模塊照片庫2.0。";
$MESS["P_NAV_TITLE"] = "選擇封面";
$MESS["P_NO_PHOTO"] = "沒有為專輯封面指定的照片。";
$MESS["P_PHOTO"] = "照片";
$MESS["P_TITLE"] = "（選擇封面）";
