<?php
$MESS["P_CANCEL"] = "取消";
$MESS["P_EDIT_ALBUM_ICON_TITLE"] = "選擇專輯封面";
$MESS["P_EMPTY_PHOTO"] = "專輯是空的。";
$MESS["P_PHOTO_MORE"] = "更多照片";
$MESS["P_SELECT_PHOTO"] = "選擇封面照片。";
$MESS["P_SUBMIT"] = "節省";
$MESS["P_UNKNOWN_ERROR"] = "數據保存錯誤";
