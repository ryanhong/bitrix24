<?php
$MESS["P_CANCEL"] = "取消";
$MESS["P_CREATE"] = "創造";
$MESS["P_GALLERY_ACTIVE"] = "默認畫廊";
$MESS["P_GALLERY_ACTIVE_NOTIFY"] = "如果已檢查，請指定該圖庫默認。";
$MESS["P_GALLERY_AVATAR"] = "畫廊圖像";
$MESS["P_GALLERY_AVATAR_NOTIFY"] = "如果<span>畫廊映像</span>將大小超過＃height＃pixels＃width＃的限制。";
$MESS["P_GALLERY_CODE"] = "畫廊代碼";
$MESS["P_GALLERY_CODE_NOTIFY"] = "<span>畫廊代碼</span>必須僅包含<b>拉丁字母，數字和下劃線（'_''）</b>。該代碼將用於構建畫廊的URL：http：//site.com/photo/ [Photo Gallery代碼]。";
$MESS["P_GALLERY_CREATE"] = "創建畫廊";
$MESS["P_GALLERY_DESCRIPTION"] = "畫廊描述";
$MESS["P_GALLERY_EDIT"] = "編輯畫廊";
$MESS["P_GALLERY_NAME"] = "畫廊標題";
$MESS["P_SAVE"] = "節省";
