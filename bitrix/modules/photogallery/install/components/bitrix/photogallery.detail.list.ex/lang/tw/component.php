<?php
$MESS["ALBUM_NOT_FOUND_ERROR"] = "您無權查看此專輯。";
$MESS["DEL_ITEM_ERROR"] = "刪除照片的錯誤。";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "信息塊模塊未安裝。";
$MESS["IBLOCK_WRONG_SESSION"] = "您的會議已經過期。請再次保存數據。";
$MESS["PHOTOS_NOT_FOUND_ERROR"] = "找不到照片。";
$MESS["P_LIST_PHOTO"] = "相片";
$MESS["P_MODULE_IS_NOT_INSTALLED"] = "未安裝模塊照片庫2.0。";
$MESS["P_PHOTOS"] = "相片";
$MESS["ROTATE_ERROR"] = "錯誤保存照片。";
$MESS["SAVE_DESC_ERROR"] = "保存描述時發生錯誤。";
$MESS["UNKNOWN_AUTHOR"] = "<untitled>";
