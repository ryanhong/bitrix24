<?php
$MESS["ADDITIONAL_SIGHTS_TIP"] = "在此處選擇要在照片頁面上的縮略圖選擇列表中顯示的縮略圖類型。";
$MESS["ALBUM_PHOTO_THUMBS_WIDTH_TIP"] = "指定專輯縮略圖圖像的大小。";
$MESS["ALBUM_PHOTO_WIDTH_TIP"] = "指定專輯封面的大小。";
$MESS["DISPLAY_PANEL_TIP"] = "如果已檢查，編輯器按鈕將在“控制面板工具欄”和“組件工具箱”區域中顯示在網站編輯模式中。";
$MESS["IBLOCK_ID_TIP"] = "在此處指定將存儲照片的信息塊。另外，您可以選擇<b>（其他） - > </b>並指定旁邊字段中的信息塊ID。";
$MESS["IBLOCK_TYPE_TIP"] = "在此處選擇現有的信息塊類型之一。單擊<b> <i> ok </i> </b>加載所選類型的信息塊。";
$MESS["INDEX_URL_TIP"] = "指定專輯頁的地址。";
$MESS["JPEG_QUALITY1_TIP"] = "指定縮略圖質量。";
$MESS["JPEG_QUALITY2_TIP"] = "指定完整的圖像質量。";
$MESS["JPEG_QUALITY_TIP"] = "指定原始圖像質量。";
$MESS["PREVIEW_SIZE_TIP"] = "指定完整圖像的大小。";
$MESS["SECTION_CODE_TIP"] = "在此處指定（專輯）助記符代碼。";
$MESS["SECTION_ID_TIP"] = "該字段包含一個評估（專輯）ID節的表達式。";
$MESS["SECTION_URL_TIP"] = "指定專輯查看頁面的地址。";
$MESS["SET_TITLE_TIP"] = "如果已檢查，則頁面標題將設置為當前專輯名稱。";
$MESS["THUMBNAIL_SIZE_TIP"] = "當在等邊方塊中顯示時縮略圖大小。實際尺寸將比指定值大1.8倍。";
$MESS["THUMBS_SIZE_TIP"] = "定義縮略圖圖像的大小。";
$MESS["UPLOAD_MAX_FILE_SIZE_TIP"] = "指定允許上傳的圖像的最大大小。";
$MESS["UPLOAD_MAX_FILE_TIP"] = "定義在一個郵政請求中可以傳輸的文件數量。";
$MESS["WATERMARK_MIN_PICTURE_SIZE_TIP"] = "在這裡指定可以應用水印的最小圖像尺寸。水印不會出現在較小的圖像上。";
