<?php
$MESS["P_ALBUM_IS_BLOCKED"] = "專輯被鎖定。";
$MESS["P_ALBUM_IS_PASSWORDED"] = "專輯受密碼保護。";
$MESS["P_ALBUM_IS_PASSWORDED_TITLE"] = "要查看照片，請鍵入一個密碼的“＃名稱＃＆raquo”。";
$MESS["P_DENIED_ACCESS"] = "拒絕訪問。";
$MESS["P_ENTER"] = "進入";
$MESS["P_GALLERY_IS_BLOCKED"] = "畫廊被鎖定。";
$MESS["P_GALLERY_NOT_FOUND"] = "沒有找到畫廊。";
$MESS["P_IBLOCK_ID_EMPTY"] = "信息塊ID丟失。";
$MESS["P_PARENT_ALBUM_IS_PASSWORDED"] = "父專輯受密碼保護。";
$MESS["P_PASSWORD"] = "密碼";
$MESS["P_SECTION_IS_EMPTY"] = "專輯中沒有照片。";
$MESS["P_SECTION_IS_NOT_APPROVED"] = "專輯是隱藏的。";
$MESS["P_SECTION_NOT_FOUND"] = "尚未找到部分。";
$MESS["P_UPLOADER_TYPE_NOTIFY"] = "照片庫組件中指定的\“上傳器類型\”參數的值已過時。使用推薦的值：\“ HTML5 Uploader \”。不再建議使用Java和Flash上​​傳器。";
