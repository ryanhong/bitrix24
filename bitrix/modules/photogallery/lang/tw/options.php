<?php
$MESS["P_CODE"] = "代碼";
$MESS["P_DROP"] = "刪除";
$MESS["P_FONT"] = "上傳字體";
$MESS["P_FONTS"] = "照片庫字體";
$MESS["P_FONTS_NOTE"] = "使用<a target = \“_self \”href='#fileman_admin#'>網站Explorer </a>來管理您的文件。要上傳新文件，請使用<a target = \"_self \" href='#fileman_file_upload#'>文件上傳表單</a>。";
$MESS["P_FONT_IS_NOT_EXISTS"] = "沒有字體文件";
$MESS["P_NEW"] = "新視圖";
$MESS["P_PICTURES"] = "自定義縮略圖大小";
$MESS["P_PICTURES_CODE_HELP"] = "縮略圖圖像代碼可能不會以數字開頭。";
$MESS["P_PICTURES_HELP"] = "使用這些字段來定義您自己的縮略圖。如果設置了照片庫組件以創建自定義縮略圖，則每當上傳新圖像時，它們都會根據指定的參數創建它們。";
$MESS["P_QUALITY"] = "品質 (%)";
$MESS["P_SIZE"] = "尺寸（PX）";
$MESS["P_TITLE"] = "姓名";
$MESS["P_UPLOAD"] = "上傳字體";
