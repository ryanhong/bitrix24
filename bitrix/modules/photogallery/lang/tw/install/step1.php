<?php
$MESS["P_CREATE"] = "創造";
$MESS["P_CREATE_NEW_BLOG"] = "創建博客";
$MESS["P_CREATE_NEW_IBLOCK"] = "新信息塊";
$MESS["P_CREATE_NEW_IBLOCK_NAME"] = "信息塊名稱";
$MESS["P_CREATE_NEW_IBLOCK_TYPE"] = "信息塊類型";
$MESS["P_DESCRIPTION"] = "描述";
$MESS["P_GROUP_BLOG"] = "博客小組";
$MESS["P_ID"] = "ID";
$MESS["P_INSTALL"] = "安裝";
$MESS["P_NAME"] = "姓名";
$MESS["P_NAME_LATIN"] = "名稱（僅拉丁字母）";
$MESS["P_SELECT"] = "選擇";
