<?php
$MESS["AD_BANNER_DAYS"] = "保留橫幅統計的天數：";
$MESS["AD_BANNER_DIAGRAM_DIAMETER"] = "餅圖直徑：";
$MESS["AD_BANNER_GRAPH_HEIGHT"] = "圖表高度：";
$MESS["AD_BANNER_GRAPH_WEIGHT"] = "圖表寬度：";
$MESS["AD_CLEAR"] = "刪除保存";
$MESS["AD_COOKIE_DAYS"] = "記住給訪客的橫幅印象的天數：";
$MESS["AD_DELETE_ALL"] = "刪除所有";
$MESS["AD_DONT_USE_CONTRACT"] = "顯示橫幅時忽略合同條件";
$MESS["AD_OPT_DONT_FIX_BANNER_SHOWS"] = "不要在任何地方跟踪橫幅印象";
$MESS["AD_RECORDS"] = "當前存儲的記錄：";
$MESS["AD_SHOW_COMPONENT_PREVIEW"] = "帶有選定模板的預覽橫幅";
$MESS["AD_UPLOAD_SUBDIR"] = "將橫幅上傳到子文件夾：";
$MESS["AD_USE_HTML_EDIT"] = "使用Visual HTML編輯器編輯橫幅代碼";
$MESS["MAIN_RESTORE_DEFAULTS"] = "設置為默認";
