<?php
$MESS["ADV_BANNER_STATUS_CHANGE_DESC"] = "＃email_to＃ - 郵件接收器的電子郵件（＃所有者_Email＃）
＃admin_email＃ - 用\“ Banners Manager \”和\“管理員\”角色的用戶電子郵件
＃add_email＃ - 橫幅經理的電子郵件
＃stat_email＃ - 具有權限的用戶電子郵件查看橫幅統計信息的電子郵件
＃edit_email＃ - 具有修改某些合同字段的用戶電子郵件的電子郵件
＃所有者_Email＃ - 具有合同權限的用戶電子郵件
＃bcc＃ - 複製（＃admin_email＃）
＃id＃ - 橫幅ID
＃Contract_ID＃ - 合同ID
＃contract_name＃ - 合同標題
＃type_sid＃ - 輸入ID
＃type_name＃ - 類型標題
＃狀態＃-狀態
＃status_comments＃ - 狀態評論
＃名稱＃-橫幅標題
＃group_sid＃ -Banner Group
＃活動＃-橫幅活動標誌[y | n]
＃指示器＃-網站上顯示橫幅嗎？
＃site_id＃ - 顯示橫幅網站的語言部分顯示
＃重量＃ - 重量（優先級）
＃MAX_SHOW_COUNT＃ -最大橫幅數字顯示
＃show_count＃ - 橫幅數量顯示
＃MAX_CLICK_COUNT＃ - 橫幅上的最大點擊次數
＃click_count＃ - 橫幅上的點擊數
＃date_last_show＃ - 最後一個橫幅的日期顯示
＃date_last_click＃ - 上次橫幅的日期點擊
＃date_show_from＃ - 橫幅的開始日期顯示期間
＃date_show_to＃ - 橫幅的結束日期顯示期間
＃image_link＃ - 圖像鏈接
＃image_alt＃ - 圖像工具提示文本
＃URL＃ - 圖像上的URL
＃url_target＃ - 在哪裡打開URL
＃代碼＃橫幅代碼
＃code_type＃ - 橫幅代碼類型（文本| html）
＃評論＃-橫幅評論
＃date_create＃ - 橫幅創建日期
＃create_by＃ -Banner Creator
＃date_modify＃ - 橫幅修改日期
＃modified_by＃ - 誰修改了橫幅
";
$MESS["ADV_BANNER_STATUS_CHANGE_MESSAGE"] = "標語＃＃id＃的狀態已更改為[＃狀態＃]。

>================================================= ======================================== =====

橫幅 -  [＃id＃]＃名稱＃
合同 -  [＃Contract_id＃]＃Contract_name＃
類型 -  [＃type_sid＃]＃type_name＃
組 - ＃group_sid＃

-------------------------------------------------- --------------------------- ----------------------- ------

活動：＃指示器＃

顯示期間 -  [＃date_show_from＃ - ＃date_show_to＃]
顯示 - ＃show_count＃ /＃max_show_count＃[＃date_last_show＃]
單擊 - ＃click_count＃ /＃max_click_count＃[＃date_last_click＃]
行為。標誌 -  [＃活動＃]
狀態 -  [＃狀態＃]
評論：
＃status_comments＃
-------------------------------------------------- --------------------------- ----------------------- ------

圖像 -  [＃image_alt＃]＃image_link＃
url- [＃url_target＃]＃url＃

代碼：[＃code_type＃]
＃代碼＃

>================================================= =========== ===================

創建 - ＃create_by＃[＃date_create＃]
修改 - ＃modified_by＃[＃date_modify＃]

要查看橫幅設置，請訪問鏈接：
http：//#server_name#/bitrix/admin/admin/adv_banner_edit.php？id =＃id＃＆Contract_id =＃contract_id＃＆lang =＃lang =＃langagenage_id＃＃

自動生成的消息。
";
$MESS["ADV_BANNER_STATUS_CHANGE_NAME"] = "橫幅狀態已更改";
$MESS["ADV_BANNER_STATUS_CHANGE_SUBJECT"] = "[bid ## id＃]＃site_name＃：橫幅狀態已更改 -  [＃狀態＃]";
$MESS["ADV_CONTRACT_INFO_DESC"] = "＃ID＃ - 合同ID
＃消息＃ - 消息
＃email_to＃ - 消息接收器的電子郵件
＃admin_email＃ - 用\“ Banners Manager \”和\“管理員\”角色的用戶電子郵件
＃add_email＃ - 橫幅經理的電子郵件
＃stat_email＃ - 具有權限的用戶電子郵件查看橫幅統計信息的電子郵件
＃edit_email＃ - 具有修改某些合同字段的用戶電子郵件的電子郵件
＃所有者_Email＃ - 具有合同權限的用戶電子郵件
＃bcc＃ - 複製
＃指示器＃-網站上是否顯示合同橫幅？
＃活動＃-合同活動標誌[y | n]
＃名稱＃-合同標題
＃描述＃ - 合同描述
＃MAX_SHOW_COUNT＃ -所有合同橫幅顯示的最大號碼顯示
＃show_count＃ - 所有合同標語的數量顯示
＃MAX_CLICK_COUNT＃ -所有合同標語單擊的最大號碼
＃click_count＃ - 所有合同標語單擊的號碼
＃標語＃ - 合同橫幅數量
＃date_show_from＃ - 橫幅的開始日期顯示期間
＃date_show_to＃ - 橫幅的結束日期顯示期間
＃date_create＃ - 合同創建日期
＃create_by＃ - 合同創建者
＃date_modify＃ - 合同修改日期
＃modified_by＃ - 誰修改了合同
";
$MESS["ADV_CONTRACT_INFO_MESSAGE"] = "#訊息#
合同：[＃id＃]＃名稱＃
＃描述＃
>================================================= ========================================= ==

活動：＃指示器＃

期間 -  [＃date_show_from＃ - ＃date_show_to＃]
顯示 - ＃show_count＃ /＃max_show_count＃
單擊 - ＃click_count＃ /＃max_click_count＃
行為。標誌 -  [＃活動＃]

橫幅 - ＃標語＃
>================================================= =========== ===================

創建 - ＃create_by＃[＃date_create＃]
更改 - ＃modified_by＃[＃date_modify＃]

要查看合同設置，請訪問鏈接：
http：//#server_name#/bitrix/admin/adv_contract_edit.php？id =＃id＃＆lang =＃langagancy_id＃＃

自動生成的消息。
";
$MESS["ADV_CONTRACT_INFO_NAME"] = "廣告合同設置";
$MESS["ADV_CONTRACT_INFO_SUBJECT"] = "[CID ## ID＃]＃site_name＃：廣告合同設置";
