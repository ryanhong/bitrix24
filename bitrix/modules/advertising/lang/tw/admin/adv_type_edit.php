<?php
$MESS["AD_ACTIVE"] = "積極的：";
$MESS["AD_ADD_NEW_TYPE"] = "添加新類型";
$MESS["AD_ADD_NEW_TYPE_TITLE"] = "添加新類型的橫幅";
$MESS["AD_BACK_TO_TYPE_LIST"] = "類型列表";
$MESS["AD_CREATED"] = "創建：";
$MESS["AD_DELETE_TYPE"] = "刪除當前類型";
$MESS["AD_DELETE_TYPE_CONFIRM"] = "您確定要刪除類型及其所有橫幅嗎？";
$MESS["AD_DESCRIPTION"] = "描述：";
$MESS["AD_EDIT_TYPE"] = "類型\“＃SID＃\”";
$MESS["AD_MODIFIED"] = "修改的：";
$MESS["AD_NAME"] = "姓名：";
$MESS["AD_NEW_TYPE"] = "新型";
$MESS["AD_SID"] = "符號標識符：";
$MESS["AD_SID_ALT"] = "（只有拉丁字母，數字和下劃線符號\“ _ \”允許）";
$MESS["AD_SORT"] = "排序：";
$MESS["AD_STATISTICS"] = "該橫幅類型的CTR圖";
$MESS["AD_STATISTICS_TITILE"] = "查看此類型橫幅的統計數據";
$MESS["AD_TYPE"] = "橫幅類型";
$MESS["AD_TYPE_EDIT"] = "編輯橫幅類型";
$MESS["AD_TYPE_EDIT_TITLE"] = "編輯橫幅類型";
$MESS["AD_TYPE_VIEW_SETTINGS"] = "查看類型";
$MESS["AD_TYPE_VIEW_SETTINGS_TITLE"] = "查看橫幅類型參數";
$MESS["AD_USER_ALT"] = "查看用戶信息";
