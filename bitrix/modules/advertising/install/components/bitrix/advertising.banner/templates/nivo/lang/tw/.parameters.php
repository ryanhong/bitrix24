<?php
$MESS["ADV_NIVO_CONTROL_NAV"] = "顯示Paginator控制";
$MESS["ADV_NIVO_CYCLING"] = "自動提前";
$MESS["ADV_NIVO_DIRECTION_NAV"] = "在幻燈片上顯示導航控制";
$MESS["ADV_NIVO_EFFECT"] = "幻燈片變化效果";
$MESS["ADV_NIVO_EFFECT_RANDOM"] = "隨機的";
$MESS["ADV_NIVO_INTERVAL"] = "滑動提前速度（MSEC）";
$MESS["ADV_NIVO_JQUERY"] = "自動包括jQuery";
$MESS["ADV_NIVO_PAUSE"] = "懸停在懸停";
$MESS["ADV_NIVO_SPEED"] = "動畫長度（MSEC。）";
