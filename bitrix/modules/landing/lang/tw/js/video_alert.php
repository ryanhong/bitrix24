<?php
$MESS["LANDING_VIDEO_ALERT_WRONG_SOURCE"] = "視頻鏈接缺少";
$MESS["LANDING_VIDEO_ALERT_WRONG_SOURCE_TEXT_2"] = "檢查您的視頻鏈接是否正確。僅支持指向YouTube，Facebook和Vimeo的完整或縮短鏈接。";
