<?php
$MESS["LANDING_INSTALL_TITLE"] = "\“著陸頁設計師\”模塊安裝";
$MESS["LANDING_MODULE_DESCRIPTION"] = "使用著陸頁設計師創建著陸頁。";
$MESS["LANDING_MODULE_NAME"] = "著陸";
$MESS["LANDING_RIGHT_D"] = "拒絕訪問";
$MESS["LANDING_RIGHT_W"] = "完全訪問";
$MESS["LANDING_UNINSTALL_TITLE"] = "\“著陸頁設計師\”模塊卸載";
