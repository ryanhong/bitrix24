<?php
$MESS["LANDING_TABLE_FIELD_ACTIVE"] = "積極的";
$MESS["LANDING_TABLE_FIELD_APP_CODE"] = "應用ID";
$MESS["LANDING_TABLE_FIELD_CONTENT"] = "塊內容";
$MESS["LANDING_TABLE_FIELD_CREATED_BY_ID"] = "由用戶ID創建";
$MESS["LANDING_TABLE_FIELD_DATE_CREATE"] = "創建於";
$MESS["LANDING_TABLE_FIELD_DATE_MODIFY"] = "修改";
$MESS["LANDING_TABLE_FIELD_DESCRIPTION"] = "描述";
$MESS["LANDING_TABLE_FIELD_MANIFEST"] = "顯現";
$MESS["LANDING_TABLE_FIELD_MODIFIED_BY_ID"] = "通過用戶ID修改";
$MESS["LANDING_TABLE_FIELD_NAME"] = "姓名";
$MESS["LANDING_TABLE_FIELD_PREVIEW"] = "預覽圖像";
$MESS["LANDING_TABLE_FIELD_SECTIONS"] = "類別";
$MESS["LANDING_TABLE_FIELD_SITE_TEMPLATE_ID"] = "內核模塊模板";
$MESS["LANDING_TABLE_FIELD_XML_ID"] = "外部ID";
