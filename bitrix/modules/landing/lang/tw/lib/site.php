<?php
$MESS["LANDING_CLB_ERROR_DELETE_SMN"] = "在刪除站點之前，請在網站24區域中刪除站點的頁面。";
$MESS["LANDING_COPY_ERROR_FOLDER_NOT_FOUND"] = "未找到或拒絕訪問文件夾。";
$MESS["LANDING_COPY_ERROR_MOVE_RESTRICTION"] = "無法移動文件夾。";
$MESS["LANDING_COPY_ERROR_SITE_NOT_FOUND"] = "未發現網站或訪問被拒絕";
$MESS["LANDING_DELETE_FOLDER_ERROR_CONTAINS_AREAS"] = "無法刪除此文件夾，因為它包含包含區域。";
$MESS["LANDING_EXPORT_ERROR"] = "\“代碼\”參數只能包括拉丁字符和數字。";
$MESS["LANDING_TYPE_GROUP"] = "工作組";
$MESS["LANDING_TYPE_KNOWLEDGE"] = "知識庫";
$MESS["LANDING_TYPE_PAGE"] = "降落";
$MESS["LANDING_TYPE_SMN"] = "專案";
$MESS["LANDING_TYPE_STORE"] = "網上商城";
