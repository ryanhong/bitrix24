<?php
$MESS["LANDING_TRANSFER_EXPORT_ACTION_DESCRIPTION_KNOWLEDGE"] = "單擊按鈕將知識庫導出到存檔文件。";
$MESS["LANDING_TRANSFER_EXPORT_ACTION_DESCRIPTION_PAGE"] = "單擊按鈕將站點導出到存檔文件。";
$MESS["LANDING_TRANSFER_EXPORT_ACTION_DESCRIPTION_STORE"] = "單擊按鈕將商店導出到存檔文件。";
$MESS["LANDING_TRANSFER_EXPORT_ACTION_TITLE_BLOCK_KNOWLEDGE"] = "出口知識庫";
$MESS["LANDING_TRANSFER_EXPORT_ACTION_TITLE_BLOCK_PAGE"] = "出口站點";
$MESS["LANDING_TRANSFER_EXPORT_ACTION_TITLE_BLOCK_STORE"] = "導出在線商店";
$MESS["LANDING_TRANSFER_GROUP_TITLE_KNOWLEDGE"] = "知識庫";
$MESS["LANDING_TRANSFER_GROUP_TITLE_PAGE"] = "站點";
$MESS["LANDING_TRANSFER_GROUP_TITLE_STORE"] = "在線商店";
$MESS["LANDING_TRANSFER_IMPORT_ACTION_TITLE_BLOCK_CREATE_PAGE"] = "創建站點";
$MESS["LANDING_TRANSFER_IMPORT_ACTION_TITLE_BLOCK_KNOWLEDGE"] = "進口知識庫";
$MESS["LANDING_TRANSFER_IMPORT_ACTION_TITLE_BLOCK_PAGE"] = "導入站點";
$MESS["LANDING_TRANSFER_IMPORT_ACTION_TITLE_BLOCK_STORE"] = "導入在線商店";
$MESS["LANDING_TRANSFER_IMPORT_DESCRIPTION_UPLOAD_KNOWLEDGE"] = "單擊按鈕以從存檔文件導入知識庫。";
$MESS["LANDING_TRANSFER_IMPORT_DESCRIPTION_UPLOAD_PAGE"] = "單擊按鈕從存檔文件導入站點。";
$MESS["LANDING_TRANSFER_IMPORT_DESCRIPTION_UPLOAD_STORE"] = "單擊按鈕從存檔文件導入商店。";
