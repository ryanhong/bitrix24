<?php
$MESS["LANDING_ACCESS_DENIED"] = "目前僅向管理員使用著陸頁服務。";
$MESS["LANDING_ACCESS_DENIED2"] = "未足夠的許可。";
$MESS["LANDING_METHOD_NOT_FOUND"] = "找不到方法";
$MESS["LANDING_MISSING_PARAMS"] = "一些呼叫參數缺少：＃缺少＃";
$MESS["LANDING_REST_DELETE_EXIST_BLOCKS"] = "在Bitrix24站點中為此應用程序創建了一些塊。請先刪除這些塊。";
$MESS["LANDING_REST_DELETE_EXIST_PAGES"] = "為此應用程序創建了頁面和/或站點。請先刪除它們。";
$MESS["LANDING_SESSION_EXPIRED"] = "您的會議已經過期。";
