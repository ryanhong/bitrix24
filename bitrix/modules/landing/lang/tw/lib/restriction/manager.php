<?php
$MESS["LANDING_BLOCK_SUBSCRIBE_EXPIRED_2"] = "該塊是由您的Bitrix24上安裝的應用程序添加的。您必須訂閱Bitrix24.Market才能在您的網站上啟用此塊。";
$MESS["LANDING_LANDING_PAYMENT_FAILED_2"] = "該頁面是由您的Bitrix24上安裝的應用程序添加的。您必須訂閱Bitrix24.Market才能發布此頁面。";
$MESS["LANDING_LANDING_PAYMENT_FAILED_BLOCK"] = "該塊是由您的Bitrix24上安裝的應用程序添加的。您必須訂閱Bitrix24.Market才能發布此頁面。";
$MESS["LANDING_LIMIT_FREE_DOMEN"] = "您的計劃上沒有免費域。請升級到主要計劃之一，或者不要為此網站使用自由域。";
$MESS["LANDING_LIMIT_KNOWLEDGE_BASE_NUMBER_PAGE"] = "您無法在當前計劃上創建更多的知識庫。請升級以創建一個新的知識庫。";
$MESS["LANDING_LIMIT_SITES_ACCESS_PERMISSIONS"] = "您當前的計劃無法獲得權限。請升級到管理和使用權限的主要計劃之一。";
$MESS["LANDING_LIMIT_SITES_DYNAMIC_BLOCKS"] = "您可以在當前計劃中發布多達兩個動態塊。請刪除先前創建的塊或升級到主要計劃之一。";
$MESS["LANDING_LIMIT_SITES_HTML_JS"] = "您無法在當前計劃中使用自定義HTML。請升級到在網站上添加您自己的HTML代碼的主要計劃之一。";
$MESS["LANDING_LIMIT_SITES_NUMBER"] = "您可以在當前計劃中創建或發布最多兩個站點。請升級到管理更多網站的主要計劃之一。";
$MESS["LANDING_LIMIT_SITES_NUMBER_FREE"] = "網站發布僅限於付費計劃。";
$MESS["LANDING_LIMIT_SITES_NUMBER_PAGE"] = "您不能在當前計劃上發布更多頁面。請升級到發布新頁面的主要計劃之一。";
