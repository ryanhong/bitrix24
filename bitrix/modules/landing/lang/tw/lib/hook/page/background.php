<?php
$MESS["LANDING_HOOK_BG_COLOR"] = "背景顏色";
$MESS["LANDING_HOOK_BG_DESCRIPTION"] = "更改當前頁面的背景。如果未選擇背景，則使用站點偏好。";
$MESS["LANDING_HOOK_BG_NAME"] = "頁面背景";
$MESS["LANDING_HOOK_BG_PICTURE"] = "背景圖";
$MESS["LANDING_HOOK_BG_POSITION"] = "位置";
$MESS["LANDING_HOOK_BG_POSITION_CENTER_2"] = "充滿";
$MESS["LANDING_HOOK_BG_POSITION_CENTER_NO_REPEAT"] = "沒有重複";
$MESS["LANDING_HOOK_BG_POSITION_CENTER_REPEAT_Y"] = "適合寬度";
$MESS["LANDING_HOOK_BG_POSITION_HELP_3"] = "背景圖像選項：
<ul>
<li>填充：縮放圖像，以使其完全填充屏幕而沒有滾動條； </li>
<li>瓷磚：沿著兩個軸的原始大小重複圖像以填充屏幕; </li>
<li>擬合寬度：水平伸展圖像以適合屏幕寬度並垂直重複到屏幕的底部。</li>
</ul>";
$MESS["LANDING_HOOK_BG_POSITION_REPEAT_2"] = "瓦";
$MESS["LANDING_HOOK_BG_USE"] = "自定義頁面背景";
