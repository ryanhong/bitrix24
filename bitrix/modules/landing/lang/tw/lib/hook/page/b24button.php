<?php
$MESS["LANDING_HOOK_B24BUTTONCODE"] = "網站小部件";
$MESS["LANDING_HOOK_B24BUTTONCODE_NO"] = "沒有任何";
$MESS["LANDING_HOOK_B24BUTTONCODE_PAGE_TITLE"] = "站點頁小部件";
$MESS["LANDING_HOOK_B24BUTTONCODE_USE"] = "使用自定義小部件設置";
$MESS["LANDING_HOOK_B24BUTTONCOLOR"] = "小部件顏色";
$MESS["LANDING_HOOK_B24BUTTONCOLOR_BUTTON"] = "使用小部件顏色";
$MESS["LANDING_HOOK_B24BUTTONCOLOR_CUSTOM"] = "使用自定義顏色";
$MESS["LANDING_HOOK_B24BUTTONCOLOR_SITE"] = "使用網站顏色";
$MESS["LANDING_HOOK_B24BUTTONCOLOR_VALUE"] = "自定義顏色";
