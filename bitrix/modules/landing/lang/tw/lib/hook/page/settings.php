<?php
$MESS["LANDING_HOOK_SETTINGS_CART_POSITION"] = "購物車對齊";
$MESS["LANDING_HOOK_SETTINGS_CART_POSITION_BC"] = "底部中心";
$MESS["LANDING_HOOK_SETTINGS_CART_POSITION_BL"] = "左下";
$MESS["LANDING_HOOK_SETTINGS_CART_POSITION_BR"] = "右下";
$MESS["LANDING_HOOK_SETTINGS_CART_POSITION_CL"] = "左中間";
$MESS["LANDING_HOOK_SETTINGS_CART_POSITION_CR"] = "右中";
$MESS["LANDING_HOOK_SETTINGS_CART_POSITION_TC"] = "頂部中心";
$MESS["LANDING_HOOK_SETTINGS_CART_POSITION_TL"] = "左上";
$MESS["LANDING_HOOK_SETTINGS_CART_POSITION_TR"] = "右頂";
$MESS["LANDING_HOOK_SETTINGS_IBLOCK_ID"] = "信息塊";
