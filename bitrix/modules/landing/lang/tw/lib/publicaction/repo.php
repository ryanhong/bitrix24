<?php
$MESS["LANDING_APP_CONTENT_IS_BAD"] = "塊的內容被標記為不安全。使用Landing.Repo.CheckContent方法來標記不安全的區域。";
$MESS["LANDING_APP_MANIFEST_INTERSECT_IMG"] = "清單項目\“＃Selector＃\”您的編輯不能為\“ Image \”樣式類型。請更改樣式類型。";
$MESS["LANDING_APP_NOT_FOUND"] = "找不到申請。";
$MESS["LANDING_APP_PLACEMENT_EXIST"] = "這個嵌入區域已經存在";
$MESS["LANDING_APP_PLACEMENT_NO_EXIST"] = "這個嵌入區域不存在";
$MESS["LANDING_APP_PRESET_CONTENT_IS_BAD"] = "\“＃預設＃\” Preset（\“＃card＃\”）的內容被標記為不安全。使用Landing.Repo.CheckContent方法來標記不安全的區域。";
