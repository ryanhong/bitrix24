<?php
$MESS["LANDING_EMAIL_NOT_CONFIRMED"] = "需要確認電子郵件以發佈內容";
$MESS["LANDING_LICENSE_EXPIRED"] = "您的許可已過期。";
$MESS["LANDING_PHONE_NOT_CONFIRMED"] = "需要確認電話號碼以發佈內容。";
$MESS["LANDING_URLCHECKER_FAIL"] = "頁面上檢測到的惡意內容";
