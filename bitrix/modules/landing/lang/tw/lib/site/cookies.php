<?php
$MESS["LANDING_COOKIES_MAIN_AGREEMENT_LABEL"] = "我們使用Cookie來個性化內容和廣告，提供社交媒體功能並分析我們的流量。";
$MESS["LANDING_COOKIES_MAIN_AGREEMENT_TEXT"] = "<p> cookie是網站發送到瀏覽器的小文本文件，並存儲在計算機的瀏覽器目錄上。
當您使用瀏覽器訪問使用cookie來跟踪網站中的動作，幫助您恢復關閉的地方，記住您的註冊登錄，主題選擇，偏好和其他自定義功能時，會創建cookie。 P>
<p>第一次訪問此網站時，您可以同意使用cookie。您可以隨時通過在網站底部單擊“ cookie \”圖標，從我們網站上的cookie聲明中更改或撤回同意。如果您以後不同意使用cookie或撤回同意，則該網站的某些功能和/或內容可能會受到限制。</p>";
$MESS["LANDING_COOKIES_MAIN_AGREEMENT_TITLE"] = "餅乾同意";
$MESS["LANDING_COOKIES_SYS_FBP_TEXT"] = "該網站使用Facebook像素，該像素使用Cookie來個性化廣告和內容，提供社交媒體功能並分析我們的流量。我們還與可信賴的社交媒體，廣告和分析合作夥伴共享有關您使用我們網站的信息。
[url = https：//www.facebook.com/policies/cookies/]了解有關Facebook Cookies策略的更多信息[/url]";
$MESS["LANDING_COOKIES_SYS_FBP_TITLE"] = "Facebook像素";
$MESS["LANDING_COOKIES_SYS_GA_TEXT"] = "Google Analytics（分析）使我們能夠收集和分析有關您瀏覽體驗的信息。此信息無法識別您。
[url = https：//policies.google.com/technologies/types]了解有關Google [/url]使用的文件的更多信息。
[url = https：//developers.google.com/analytics/devguides/collection/collection/analyticsjs/cookie-usage]有關Google Analytics（分析）cookie和隱私政策的更多信息[/url]";
$MESS["LANDING_COOKIES_SYS_GA_TITLE"] = "谷歌分析";
$MESS["LANDING_COOKIES_SYS_GMAP_TEXT"] = "我們的網站使用Google Maps提供的功能和內容[url = https：//maps.google.com/help/terms_maps/] policies.google.com/privacy] Google隱私政策[/url]";
$MESS["LANDING_COOKIES_SYS_GMAP_TITLE"] = "谷歌地圖";
$MESS["LANDING_COOKIES_SYS_GTM_TEXT"] = "Google標籤管理器是標籤管理系統。我們使用標籤來跟踪和分析我們網站上訪問者的瀏覽路徑。使用Google標籤管理器收集的信息無法識別您；它的存在是使我們的網站更容易訪問。
[url = https：//www.google.com/analytics/terms/tag-manager/]詳細介紹Google Tag Manager使用條款[/url]";
$MESS["LANDING_COOKIES_SYS_GTM_TITLE"] = "Google標籤管理器";
$MESS["LANDING_COOKIES_SYS_VKP_TEXT"] = "#VALUE!";
$MESS["LANDING_COOKIES_SYS_VKP_TITLE"] = "#VALUE!";
$MESS["LANDING_COOKIES_SYS_YM_TEXT"] = "#VALUE!";
$MESS["LANDING_COOKIES_SYS_YM_TITLE"] = "#VALUE!";
$MESS["LANDING_COOKIES_SYS_YT_TEXT"] = "YouTube播放時使用Cookie來獲取嵌入式視頻的設置。
[url = https：//www.youtube.com/t/terms]了解有關YouTube策略的更多信息[/url]
[url = https：//policies.google.com/technologies/types] Google使用的文件[/url]";
$MESS["LANDING_COOKIES_SYS_YT_TITLE"] = "Youtube";
