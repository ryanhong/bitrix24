<?php
$MESS["LANDING_BLOCK_EMPTY_GMAP_DESC"] = "請在網站首選項中提供有效的Google Maps密鑰。";
$MESS["LANDING_BLOCK_EMPTY_GMAP_SETTINGS"] = "打開設置";
$MESS["LANDING_BLOCK_EMPTY_GMAP_TITLE"] = "Google Maps密鑰未指定";
$MESS["LANDING_GOOGLE_MAP--STYLE_LANDMARKS_TITLE"] = "地標";
$MESS["LANDING_GOOGLE_MAP--STYLE_OFF"] = "隱藏";
$MESS["LANDING_GOOGLE_MAP--STYLE_ON"] = "展示";
$MESS["LANDING_GOOGLE_MAP--STYLE_ROADS_TITLE"] = "道路";
$MESS["LANDING_GOOGLE_MAP--STYLE_THEME_AUBERGINE"] = "Aubergine";
$MESS["LANDING_GOOGLE_MAP--STYLE_THEME_DARK"] = "黑暗的";
$MESS["LANDING_GOOGLE_MAP--STYLE_THEME_DEFAULT"] = "預設";
$MESS["LANDING_GOOGLE_MAP--STYLE_THEME_NIGHT"] = "夜晚";
$MESS["LANDING_GOOGLE_MAP--STYLE_THEME_RETRO"] = "復古的";
$MESS["LANDING_GOOGLE_MAP--STYLE_THEME_SILVER"] = "黑與白";
$MESS["LANDING_GOOGLE_MAP--STYLE_THEME_TITLE"] = "顏色主題";
$MESS["LANDING_GOOGLE_MAP--STYLE_TITLE"] = "地圖";
