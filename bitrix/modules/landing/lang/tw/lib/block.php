<?php
$MESS["LANDING_BLOCK_ACCESS_DENIED"] = "拒絕訪問";
$MESS["LANDING_BLOCK_BAD_ANCHOR"] = "錨應以字母順序的字符（A-Z）開始，並且可以包含字符\“ A-Z \”，\“ 0-9 \”，\“  -  \”，\“ _ \” _ \“，\”，\ \ “。\”。 “ 僅有的。";
$MESS["LANDING_BLOCK_BR1"] = "目錄";
$MESS["LANDING_BLOCK_BR2"] = "產品頁面";
$MESS["LANDING_BLOCK_CARD_NOT_FOUND"] = "找不到塊卡";
$MESS["LANDING_BLOCK_INCORRECT_AFFECTED"] = "保存之前和之後的數據不匹配";
$MESS["LANDING_BLOCK_LANDING_NOT_EXIST"] = "著陸頁不存在";
$MESS["LANDING_BLOCK_MESSAGE_ERROR_DYNAMIC_LIMIT_TITLE"] = "超出限制";
$MESS["LANDING_BLOCK_MESSAGE_ERROR_EVAL"] = "運行塊的錯誤。請嘗試再次添加。";
$MESS["LANDING_BLOCK_MESSAGE_ERROR_LIMIT_BUTTON"] = "現在升級";
$MESS["LANDING_BLOCK_NOT_FOUND"] = "找不到塊或塊內容物";
$MESS["LANDING_BLOCK_SEPARATOR_PARTNER_2"] = "Bitrix24.market";
$MESS["LANDING_BLOCK_TEXT_FULL"] = "塊尺寸不合時宜";
$MESS["LANDING_BLOCK_TITLE"] = "頁面標題";
$MESS["LANDING_BLOCK_WRONG_VERSION"] = "塊版與模塊版本不匹配";
$MESS["LD_BLOCK_SECTION_LAST"] = "最近的項目";
