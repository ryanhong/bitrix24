<?php
$MESS["LANDING_BLOCK_36.2.CONCERTES_DATES_WITH_BUTTON_CARDS_LANDINGBLOCKNODECARD"] = "細繩";
$MESS["LANDING_BLOCK_36.2.CONCERTES_DATES_WITH_BUTTON_NAME"] = "活動日期與圖像，描述和按鈕";
$MESS["LANDING_BLOCK_36.2.CONCERTES_DATES_WITH_BUTTON_NODES_LANDINGBLOCKNODECARDBUTTON"] = "按鈕";
$MESS["LANDING_BLOCK_36.2.CONCERTES_DATES_WITH_BUTTON_NODES_LANDINGBLOCKNODECARDDATETEXT"] = "日期：標題";
$MESS["LANDING_BLOCK_36.2.CONCERTES_DATES_WITH_BUTTON_NODES_LANDINGBLOCKNODECARDDATEVALUE"] = "日期";
$MESS["LANDING_BLOCK_36.2.CONCERTES_DATES_WITH_BUTTON_NODES_LANDINGBLOCKNODECARDIMG"] = "圖像";
$MESS["LANDING_BLOCK_36.2.CONCERTES_DATES_WITH_BUTTON_NODES_LANDINGBLOCKNODECARDPRICE"] = "價格";
$MESS["LANDING_BLOCK_36.2.CONCERTES_DATES_WITH_BUTTON_NODES_LANDINGBLOCKNODECARDPRICETEXT"] = "價格：標題";
$MESS["LANDING_BLOCK_36.2.CONCERTES_DATES_WITH_BUTTON_NODES_LANDINGBLOCKNODECARDTEXT"] = "文字";
$MESS["LANDING_BLOCK_36.2.CONCERTES_DATES_WITH_BUTTON_NODES_LANDINGBLOCKNODECARDTITLE"] = "標題";
