<?php
$MESS["LANDING_BLOCK_TEAM002._CARDS_LANDINGBLOCKCARDEMPLOYEE"] = "員工";
$MESS["LANDING_BLOCK_TEAM002._CARDS_LANDINGBLOCKNODEINNER"] = "堵塞";
$MESS["LANDING_BLOCK_TEAM002._NAME"] = "員工照片帶標題";
$MESS["LANDING_BLOCK_TEAM002._NODES_LANDINGBLOCKNODEEMPLOYEENAME"] = "姓名";
$MESS["LANDING_BLOCK_TEAM002._NODES_LANDINGBLOCKNODEEMPLOYEEPHOTO"] = "照片";
$MESS["LANDING_BLOCK_TEAM002._NODES_LANDINGBLOCKNODEEMPLOYEEPOST"] = "位置";
$MESS["LANDING_BLOCK_TEAM002._NODES_LANDINGBLOCKNODEEMPLOYEEQUOTE"] = "引用";
$MESS["LANDING_BLOCK_TEAM002._NODES_LANDINGBLOCKNODEEMPLOYEESUBTITLE"] = "字幕";
$MESS["LANDING_BLOCK_TEAM002._STYLE_LANDINGBLOCKNODEEMPLOYEENAME"] = "姓名";
$MESS["LANDING_BLOCK_TEAM002._STYLE_LANDINGBLOCKNODEEMPLOYEEPOST"] = "位置";
$MESS["LANDING_BLOCK_TEAM002._STYLE_LANDINGBLOCKNODEEMPLOYEEQUOTE"] = "引用";
$MESS["LANDING_BLOCK_TEAM002._STYLE_LANDINGBLOCKNODEEMPLOYEESUBTITLE"] = "字幕";
