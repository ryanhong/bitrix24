<?php
$MESS["LANDING_BLOCK_31_5-CARD"] = "塊元素";
$MESS["LANDING_BLOCK_31_5-COLUMN"] = "柱子";
$MESS["LANDING_BLOCK_31_5-IMAGE"] = "圖像";
$MESS["LANDING_BLOCK_31_5-LINK"] = "關聯";
$MESS["LANDING_BLOCK_31_5-NAME"] = "左側圖像的頁面列表，右側有鏈接";
$MESS["LANDING_BLOCK_31_5-SUBTITLE"] = "字幕";
$MESS["LANDING_BLOCK_31_5-TEXT"] = "文字";
$MESS["LANDING_BLOCK_31_5-TITLE"] = "標題";
