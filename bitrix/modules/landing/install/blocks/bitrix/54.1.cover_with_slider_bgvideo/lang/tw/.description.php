<?php
$MESS["LANDING_BLOCK_54_COVER_SLIDER_BGVIDEO--LANDINGBLOCKNODE--CARD"] = "滑動";
$MESS["LANDING_BLOCK_54_COVER_SLIDER_BGVIDEO--LANDINGBLOCKNODE--VIDEO"] = "背景視頻";
$MESS["LANDING_BLOCK_54_COVER_SLIDER_BGVIDEO--LANDINGBLOCKNODECARD--BTN"] = "按鈕";
$MESS["LANDING_BLOCK_54_COVER_SLIDER_BGVIDEO--LANDINGBLOCKNODECARD--TEXT"] = "文字";
$MESS["LANDING_BLOCK_54_COVER_SLIDER_BGVIDEO--LANDINGBLOCKNODECARD--TITLE"] = "標題";
$MESS["LANDING_BLOCK_54_COVER_SLIDER_BGVIDEO--NAME"] = "在背景視頻上蓋上滑塊";
$MESS["LANDING_BLOCK_54_COVER_SLIDER_BGVIDEO_SLIDER"] = "滑桿";
