<?php
$MESS["LANDING_BLOCK_44.4.SLIDER_5_COLS_WITH_PRICES_CARDS_LANDINGBLOCKNODECARD"] = "柱子";
$MESS["LANDING_BLOCK_44.4.SLIDER_5_COLS_WITH_PRICES_NAME"] = "圓柱旋轉木馬帶有文字和黑暗背景上的圖像";
$MESS["LANDING_BLOCK_44.4.SLIDER_5_COLS_WITH_PRICES_NODES_LANDINGBLOCKNODECARDIMG"] = "照片";
$MESS["LANDING_BLOCK_44.4.SLIDER_5_COLS_WITH_PRICES_NODES_LANDINGBLOCKNODECARDPRICE"] = "價格";
$MESS["LANDING_BLOCK_44.4.SLIDER_5_COLS_WITH_PRICES_NODES_LANDINGBLOCKNODECARDTEXT"] = "文字";
$MESS["LANDING_BLOCK_44.4.SLIDER_5_COLS_WITH_PRICES_NODES_LANDINGBLOCKNODECARDTITLE"] = "標題";
$MESS["LANDING_BLOCK_44_4_SLIDER_5_COLS_WITH_PRICES_NODES_LANDINGBLOCKNODE_SLIDER"] = "滑桿";
