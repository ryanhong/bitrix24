<?php
$MESS["LANDING_BLOCK_2_TWO_COLS_BIG_WITH_TEXT_AND_TITLES_NAME"] = "兩列：顏色背景上的頁面上的文字";
$MESS["LANDING_BLOCK_2_TWO_COLS_BIG_WITH_TEXT_AND_TITLES_NODES_LANDINGBLOCKNODESUBTITLE"] = "字幕";
$MESS["LANDING_BLOCK_2_TWO_COLS_BIG_WITH_TEXT_AND_TITLES_NODES_LANDINGBLOCKNODETEXT"] = "文字";
$MESS["LANDING_BLOCK_2_TWO_COLS_BIG_WITH_TEXT_AND_TITLES_NODES_LANDINGBLOCKNODETITLE"] = "標題";
$MESS["LANDING_BLOCK_2_TWO_COLS_BIG_WITH_TEXT_AND_TITLES_NODES_LANDINGBLOCK_CARD"] = "柱子";
$MESS["LANDING_BLOCK_2_TWO_COLS_BIG_WITH_TEXT_AND_TITLES_NODES_LANDINGBLOCK_COLS"] = "列";
$MESS["LANDING_BLOCK_2_TWO_COLS_BIG_WITH_TEXT_AND_TITLES_STYLE_LANDINGBLOCKNODECARDHEADER"] = "標題";
$MESS["LANDING_BLOCK_2_TWO_COLS_BIG_WITH_TEXT_AND_TITLES_STYLE_LANDINGBLOCKNODECONTAINER"] = "塊背景";
$MESS["LANDING_BLOCK_2_TWO_COLS_BIG_WITH_TEXT_AND_TITLES_STYLE_LANDINGBLOCKNODESUBTITLE"] = "字幕";
$MESS["LANDING_BLOCK_2_TWO_COLS_BIG_WITH_TEXT_AND_TITLES_STYLE_LANDINGBLOCKNODETEXT"] = "文字";
$MESS["LANDING_BLOCK_2_TWO_COLS_BIG_WITH_TEXT_AND_TITLES_STYLE_LANDINGBLOCKNODETITLE"] = "標題";
