<?php
$MESS["LNDNGBLCK_CALLBACK_ERR_DESC"] = "確保在您的Bitrix24上激活<a target= \“href= \ \"#link1# \ \">電話</a>。另外，請檢查您在<a target= \"_blank \" href= \“#link2# \ \"> forms </a>頁面上是否有一個主動回調表格</a>頁面。";
$MESS["LNDNGBLCK_CALLBACK_ERR_DESC_CONNECTOR"] = "您的Bitrix24沒有任何活動的回調表格。確保在您的Bitrix24上激活<a target= \“href= \ \"#link1# \ \">電話</a>。另外，請檢查您在<a target= \"_blank \" href= \“#link2# \ \"> forms </a>頁面上是否有一個主動回調表格</a>頁面。";
$MESS["LNDNGBLCK_CALLBACK_ERR_DESC_NO_CONNECTOR"] = "您沒有安裝BitRix24連接器模塊（B24 Connector）。確保它存在於<a target = \"_blank \" href= \"#link1# \"> settings＆gt;系統設置＆GT;模塊</a>頁面。";
$MESS["LNDNGBLCK_CALLBACK_ERR_TITLE"] = "找不到回調表格";
$MESS["LNDNGBLCK_CALLBACK_SETTINGS"] = "設定";
