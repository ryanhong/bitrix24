<?php
$MESS["AUTH_AUTHORIZE"] = "授權";
$MESS["AUTH_CAPTCHA_PROMT"] = "從圖像輸入文字";
$MESS["AUTH_FIRST_ONE"] = "如果您是網站上的首次訪問者，請填寫註冊表。";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "忘記密碼了嗎？";
$MESS["AUTH_LOGIN"] = "登入:";
$MESS["AUTH_NONSECURE_NOTE"] = "密碼將以開放的形式發送。在您的Web瀏覽器中啟用JavaScript啟用密碼加密。";
$MESS["AUTH_PASSWORD"] = "密碼：";
$MESS["AUTH_PLEASE_AUTH"] = "請授權：";
$MESS["AUTH_REGISTER"] = "登記";
$MESS["AUTH_REMEMBER_ME"] = "在這台電腦上記住我";
$MESS["AUTH_SECURE_NOTE"] = "密碼將在發送之前進行加密。這將防止密碼以數據傳輸通道以開放形式出現。";
$MESS["AUTH_TITLE"] = "登入";
