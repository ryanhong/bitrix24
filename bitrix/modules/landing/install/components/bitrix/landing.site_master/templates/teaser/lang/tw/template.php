<?php
$MESS["LANDING_TPL_ALERT_DISABLE"] = "您的許可證可能已經過期，或者許可證密鑰無效。";
$MESS["LANDING_TPL_HOT_HEADER_NEW"] = "＃span_hot＃新＃span_end＃crm+在線商店";
$MESS["LANDING_TPL_HOT_SUBHEADER"] = "立即開始銷售";
$MESS["LANDING_TPL_RANDOM_LOAD_TEXT_1"] = "您正在創建您的在線商店";
$MESS["LANDING_TPL_RANDOM_LOAD_TEXT_2"] = "非常適合使者和社交媒體";
$MESS["LANDING_TPL_RANDOM_LOAD_TEXT_3"] = "當代移動設計";
$MESS["LANDING_TPL_RANDOM_LOAD_TEXT_4"] = "只有幾秒鐘！";
$MESS["LANDING_TPL_SUBMIT"] = "單擊幾下創建您的在線商店";
