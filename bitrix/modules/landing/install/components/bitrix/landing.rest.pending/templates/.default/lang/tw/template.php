<?php
$MESS["LANDING_TPL_APP_NOT_FOUND"] = "該塊不可用，因為為此塊服務的應用程序無法安裝在您的BitRix24 [＃App_Code＃]上。";
$MESS["LANDING_TPL_BLOCK_NOT_FOUND"] = "無法安裝應用程序<b> #app_name＃</b>對此塊進行維修。請聯繫應用程序開發人員以解決問題。";
