<?php
$MESS["LANDING_CMP_ACCESS_DENIED"] = "BITRIX24站點服務當前僅適用於管理員。";
$MESS["LANDING_CMP_ACCESS_DENIED2"] = "未足夠的許可。";
$MESS["LANDING_CMP_AGREEMENT_LABEL"] = "我接受使用條款";
$MESS["LANDING_CMP_AGREEMENT_NAME"] = "Bitrix24站點使用條款";
$MESS["LANDING_CMP_AGREEMENT_TEXT4"] = "
<div class ='landing-grerement-popup-content-1'>
<p>我們的規則很簡單。您必須擁有您發布的內容，或者擁有書面許可才能使用第三方在版權中擁有和保留的內容。您發布的內容不得以任何形式包含法律禁止的任何信息或圖像。</p>
</div>";
$MESS["LANDING_CMP_AGREEMENT_TEXT4_1"] = "
<div class ='landing-grerement-popup-content-1'>
<p>我們的規則很簡單。您必須擁有您發布的內容，或者擁有書面許可才能使用第三方在版權中擁有和保留的內容。您發布的內容不得以任何形式包含法律禁止的任何信息或圖像。</p>
</div>";
$MESS["LANDING_CMP_MODULE_NOT_INSTALLED"] = "Bitrix24站點服務當前不可用。";
$MESS["LANDING_CMP_MODULE_NOT_INSTALLED_CRM"] = "無法準備CRM數據。";
$MESS["LANDING_CMP_TITLE"] = "站點";
$MESS["LANDING_CMP_TYPE_IS_NOT_ENABLED"] = "不支持類型";
