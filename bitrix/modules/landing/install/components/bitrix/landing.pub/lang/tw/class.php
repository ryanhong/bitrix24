<?php
$MESS["LANDING_CMP_ASK_ACCESS_KNOWLEDGE"] = "如果您可以授予我訪問知識基礎\“＃名稱＃\”，我將不勝感激。 ＃link1＃配置訪問＃鏈接2＃。";
$MESS["LANDING_CMP_SITE_NOT_ALLOWED"] = "拒絕訪問";
$MESS["LANDING_CMP_SITE_NOT_ALLOWED_2_GROUP"] = "這個知識庫是私人的。";
$MESS["LANDING_CMP_SITE_NOT_ALLOWED_KNOWLEDGE"] = "公眾對此知識庫的訪問已關閉。請提交請求以獲取訪問權限。";
$MESS["LANDING_CMP_SITE_NOT_FOUND2"] = "此鏈接似乎沒有打開任何頁面。";
$MESS["LANDING_CMP_SITE_NOT_FOUND2_GROUP"] = "該鏈接沒有知識庫。請檢查您的鏈接是正確的。";
$MESS["LANDING_CMP_SITE_NOT_FOUND2_KNOWLEDGE"] = "該鏈接沒有知識庫。請檢查您的鏈接是正確的。";
