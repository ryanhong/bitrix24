<?php
$MESS["LANDING_TPL_ACCESS_ASK_DESCRIPTION_KNOWLEDGE"] = "向其中一個用戶發送請求，以閱讀對此知識庫的訪問。";
$MESS["LANDING_TPL_ACCESS_ASK_HEADER_KNOWLEDGE"] = "這個知識庫是私人的";
$MESS["LANDING_TPL_ACCESS_ASK_SEND_KNOWLEDGE"] = "申請進入";
$MESS["LANDING_TPL_ACCESS_ASK_TITLE_KNOWLEDGE"] = "訪問請求";
$MESS["LANDING_TPL_COPY_FULL"] = "由＃徽標＃＆mdash;免費網站和CRM。";
$MESS["LANDING_TPL_COPY_FULL2"] = "由<linklogo> #logo＃</linklogo>＆mdash; <linksite>免費網站</linksite>和<Linkcrm> crm </linkcrm>。 <LinkCreate>創建您的網站</linkCreate>";
$MESS["LANDING_TPL_COPY_LINK"] = "創建您的網站";
$MESS["LANDING_TPL_COPY_NAME"] = "Bitrix24";
$MESS["LANDING_TPL_COPY_NAME_0"] = "供電";
$MESS["LANDING_TPL_COPY_NAME_SMN_0"] = "供電";
$MESS["LANDING_TPL_COPY_NAME_SMN_1"] = "Bitrix24";
$MESS["LANDING_TPL_COPY_REVIEW"] = "免費網站和CRM。";
$MESS["LANDING_TPL_EDIT_PAGE"] = "編輯頁面";
$MESS["LANDING_TPL_EDIT_PAGE_GROUP"] = "編輯文章";
$MESS["LANDING_TPL_EDIT_PAGE_KNOWLEDGE"] = "編輯文章";
$MESS["LANDING_TPL_ERROR_NOT_ALLOWED_ASK"] = "發送請求";
$MESS["LANDING_TPL_ERROR_NOT_ALLOWED_GROUP"] = "查看工作組";
$MESS["LANDING_TPL_ERROR_NOT_ALLOWED_NOTE_2_GROUP"] = "聯繫＃link1＃WorkGroup的＃link2＃所有者獲取視圖訪問知識庫。";
$MESS["LANDING_TPL_ERROR_NOT_ALLOWED_NOTE_KNOWLEDGE"] = "向其中一個用戶發送請求以訪問此知識庫。";
$MESS["LANDING_TPL_ERROR_NOT_FOUND_NOTE"] = "檢查鏈接地址或返回＃link1＃索引頁＃鏈接2＃。";
$MESS["LANDING_TPL_ERROR_NOT_FOUND_NOTE_KNOWLEDGE"] = "嘗試在＃link1中找到您需要的知識庫＃列表＃link2＃";
$MESS["LANDING_TPL_PUB_COPIED_LINK"] = "頁面鏈接已復製到剪貼板";
$MESS["LANDING_TPL_SETTINGS_BUTTON_TITLE"] = "設定";
$MESS["LANDING_TPL_SITES"] = "我的網站";
$MESS["LANDING_TPL_SITES_GROUP"] = "知識庫";
$MESS["LANDING_TPL_SITES_KNOWLEDGE"] = "知識庫";
$MESS["LANDING_TPL_SITES_STORE"] = "我的商店";
$MESS["LANDING_TPL_TITLE"] = "網站";
$MESS["LANDING_TPL_VIEWS"] = "視圖";
