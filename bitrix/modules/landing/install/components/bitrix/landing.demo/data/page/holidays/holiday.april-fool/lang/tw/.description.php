<?php
$MESS["LANDING_DEMO_APRIL_FOOL_DESCRIPTION"] = "愚人節是一年來的唯一一天，您可以玩實用的笑話或散佈騙局。如果您出售愚人節的禮物，請使用此模板創建特殊優惠。";
$MESS["LANDING_DEMO_APRIL_FOOL_TITLE"] = "愚人節的日子";
