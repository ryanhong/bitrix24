<?php
$MESS["LANDING_DEMO_THANKSGIVING-DESCRIPTION"] = "這是值得感謝的一天。現在是時候在頁面的調色板上添加更多顏色了。
通過特殊的Bitrix24感恩節模板發送感恩節祝福。您的客戶會喜歡它！";
$MESS["LANDING_DEMO_THANKSGIVING-TITLE"] = "感恩節";
