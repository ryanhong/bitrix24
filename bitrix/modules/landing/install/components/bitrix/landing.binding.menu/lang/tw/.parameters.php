<?php
$MESS["LANDING_CMP_PAR_LANDING_MODE"] = "模式";
$MESS["LANDING_CMP_PAR_LANDING_MODE_CREATE"] = "創造";
$MESS["LANDING_CMP_PAR_LANDING_MODE_LIST"] = "列表";
$MESS["LANDING_CMP_PAR_LANDING_TYPE"] = "類型";
$MESS["LANDING_CMP_PAR_MENU_ID"] = "菜單ID";
$MESS["LANDING_CMP_PAR_PATH_AFTER_CREATE"] = "創建工作組知識庫後，重定向到此頁面";
$MESS["LANDING_CMP_PAR_SITE_ID"] = "站點ID";
