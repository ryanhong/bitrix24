<?php
$MESS["LANDING_TPL_GIFT_HEADER_CHANGE"] = "有一個使用禮品域的網站";
$MESS["LANDING_TPL_GIFT_HEADER_DELETE"] = "使用禮品域的網站無法刪除";
$MESS["LANDING_TPL_GIFT_TEXT"] = "請選擇要移動域的網站";
$MESS["LANDING_TPL_NO_SITES"] = "找不到合適的地點。您現在可以創建一個。";
$MESS["LANDING_TPL_SELECT"] = "選擇";
$MESS["LANDING_TPL_SWITCH_TITLE"] = "將域移至另一個站點";
