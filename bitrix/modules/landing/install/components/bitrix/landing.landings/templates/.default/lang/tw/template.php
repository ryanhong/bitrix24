<?php
$MESS["LANDING_TPL_ACTIONS"] = "動作";
$MESS["LANDING_TPL_ACTION_ADD"] = "新一頁";
$MESS["LANDING_TPL_ACTION_ADD_PAGE"] = "創建頁面";
$MESS["LANDING_TPL_ACTION_COPY"] = "複製頁面";
$MESS["LANDING_TPL_ACTION_COPYLINK"] = "複製URL";
$MESS["LANDING_TPL_ACTION_DELETE"] = "刪除頁面";
$MESS["LANDING_TPL_ACTION_DELETE_CONFIRM"] = "您要刪除頁面嗎？";
$MESS["LANDING_TPL_ACTION_DELETE_FOLDER"] = "刪除文件夾";
$MESS["LANDING_TPL_ACTION_EDIT_2"] = "頁面設置";
$MESS["LANDING_TPL_ACTION_EDIT_DESIGN_2"] = "頁面設計";
$MESS["LANDING_TPL_ACTION_EDIT_FOLDER_MSGVER_1"] = "配置文件夾";
$MESS["LANDING_TPL_ACTION_FOLDER_UP"] = "向上";
$MESS["LANDING_TPL_ACTION_GOTO"] = "打開頁面";
$MESS["LANDING_TPL_ACTION_MOVE"] = "移動頁面";
$MESS["LANDING_TPL_ACTION_MOVE_FOLDER"] = "移動文件夾";
$MESS["LANDING_TPL_ACTION_PUBLIC"] = "發布";
$MESS["LANDING_TPL_ACTION_PUBLIC_CHANGED"] = "發布更改";
$MESS["LANDING_TPL_ACTION_PUBLIC_FOLDER"] = "禁用文件夾";
$MESS["LANDING_TPL_ACTION_REC_CONFIRM"] = "移至回收箱？";
$MESS["LANDING_TPL_ACTION_UNDELETE"] = "還原頁面";
$MESS["LANDING_TPL_ACTION_UNDELETE_FOLDER"] = "還原文件夾";
$MESS["LANDING_TPL_ACTION_UNPUBLIC"] = "未出版";
$MESS["LANDING_TPL_ACTION_UNPUBLIC_FOLDER"] = "啟用文件夾";
$MESS["LANDING_TPL_ACTION_VIEW"] = "編輯";
$MESS["LANDING_TPL_AREA_HEADER_FOOTER_1"] = "標題";
$MESS["LANDING_TPL_AREA_HEADER_FOOTER_2"] = "頁尾";
$MESS["LANDING_TPL_AREA_HEADER_ONLY_1"] = "標題";
$MESS["LANDING_TPL_AREA_MAIN_PAGE"] = "主頁";
$MESS["LANDING_TPL_AREA_SIDEBAR_LEFT_1"] = "左側欄";
$MESS["LANDING_TPL_AREA_SIDEBAR_RIGHT_1"] = "右側欄";
$MESS["LANDING_TPL_AREA_WITHOUT_LEFT_1"] = "標題";
$MESS["LANDING_TPL_AREA_WITHOUT_LEFT_2"] = "右側欄";
$MESS["LANDING_TPL_AREA_WITHOUT_LEFT_3"] = "頁尾";
$MESS["LANDING_TPL_AREA_WITHOUT_RIGHT_1"] = "標題";
$MESS["LANDING_TPL_AREA_WITHOUT_RIGHT_2"] = "左側欄";
$MESS["LANDING_TPL_AREA_WITHOUT_RIGHT_3"] = "頁尾";
$MESS["LANDING_TPL_DELETED"] = "刪除";
$MESS["LANDING_TPL_MODIF"] = "修改的";
$MESS["LANDING_TPL_PUBLIC"] = "出版";
$MESS["LANDING_TPL_TITLE"] = "網站";
$MESS["LANDING_TPL_TITLE_PAGE"] = "網站";
$MESS["LANDING_TPL_TITLE_STORE"] = "網上商城";
$MESS["LANDING_TPL_TTL_DELETE"] = "距離永久刪除的天數";
$MESS["LANDING_TPL_TTL_DELETE_D"] = "天";
$MESS["LANDING_TPL_UNPUBLIC"] = "未出版";
