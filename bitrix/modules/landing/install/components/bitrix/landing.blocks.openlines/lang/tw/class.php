<?php
$MESS["LANDING_CMP_OL_BUTTON_NO_CHOOSE_BUTTON"] = "打開設置";
$MESS["LANDING_CMP_OL_BUTTON_NO_CHOOSE_PAGE_1"] = "選擇頁面小部件";
$MESS["LANDING_CMP_OL_BUTTON_NO_CHOOSE_PAGE_TEXT_1"] = "選擇一個可以顯示可用社交圖標的小部件。您可以在頁面設置中選擇或創建小部件。";
$MESS["LANDING_CMP_OL_BUTTON_NO_CHOOSE_SITE_1"] = "選擇站點小部件";
$MESS["LANDING_CMP_OL_BUTTON_NO_CHOOSE_SITE_TEXT_1"] = "選擇一個可以顯示可用社交圖標的小部件。您可以在網站設置中選擇或創建小部件。";
$MESS["LANDING_CMP_OL_BUTTON_REST_ERROR"] = "該塊暫時不可用";
$MESS["LANDING_CMP_OL_BUTTON_REST_ERROR_DESC"] = "需要更新Bitrix24才能繼續進行。請稍等。";
$MESS["LANDING_CMP_OL_NO_BUTTON"] = "您的網站上沒有CRM小部件。";
$MESS["LANDING_CMP_OL_NO_BUTTON_CP"] = "請在此處檢查小部件是否可用並有效：\“＃link1＃crm -CRM表單＃link2＃\”。";
$MESS["LANDING_CMP_OL_NO_BUTTON_ID"] = "未選擇CRM小部件";
$MESS["LANDING_CMP_OL_NO_BUTTON_SM"] = "請檢查模塊\“ BitRix24 Connector \”是否已安裝在此處：\“＃link1＃設置 - 系統設置 - 模塊＃link2＃\”。還要檢查BitRix24已連接在\“＃link3＃客戶端通信＃link4＃\”頁面上。";
