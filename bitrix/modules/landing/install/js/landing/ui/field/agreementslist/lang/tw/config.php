<?php
$MESS["LANDING_AGREEMENT_CONSENTS_BUTTON_LABEL"] = "查看用戶同意";
$MESS["LANDING_AGREEMENT_EDIT_BUTTON_LABEL"] = "協議參數";
$MESS["LANDING_AGREEMENT_FORM_TITLE"] = "協議行為";
$MESS["LANDING_AGREEMENT_FORM_TYPE_FIELD_ITEM_1"] = "需要同意。<br>自動接受";
$MESS["LANDING_AGREEMENT_FORM_TYPE_FIELD_ITEM_2"] = "需要同意。<br>協議文本必須閱讀";
$MESS["LANDING_AGREEMENT_FORM_TYPE_FIELD_ITEM_3"] = "不需要同意。<br>自動接受";
$MESS["LANDING_AGREEMENT_FORM_TYPE_FIELD_ITEM_4"] = "不需要同意。<br>不要自動接受";
$MESS["LANDING_AGREEMENT_LIST_CREATE_BUTTON_LABEL"] = "創建用戶協議";
$MESS["LANDING_AGREEMENT_LIST_SELECT_BUTTON_LABEL"] = "選擇用戶協議";
