<?php
$MESS["VOTE_ACCESS_DENIED"] = "該民意調查的訪問被拒絕。";
$MESS["VOTE_ALREADY_VOTE"] = "您不能兩次參加此民意調查。";
$MESS["VOTE_EMPTY"] = "未找到民意調查。";
$MESS["VOTE_MODULE_IS_NOT_INSTALLED"] = "未安裝民意調查模塊。";
$MESS["VOTE_NOT_FOUND"] = "未找到民意調查。";
$MESS["VOTE_OK"] = "感謝你的投票。";
$MESS["VOTE_RED_LAMP"] = "民意調查不活躍。";
