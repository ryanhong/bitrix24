<?php
$MESS["VOTE_GROUP_TOTAL"] = "全部的";
$MESS["VOTE_IS_ACTIVE"] = "民意調查是活躍的。";
$MESS["VOTE_IS_ACTIVE_SMALL"] = "積極的";
$MESS["VOTE_IS_NOT_ACTIVE"] = "民意調查不活躍。";
$MESS["VOTE_IS_NOT_ACTIVE_SMALL"] = "不活躍";
$MESS["VOTE_VOTES"] = "投票";
