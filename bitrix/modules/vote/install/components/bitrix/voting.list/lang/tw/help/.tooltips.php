<?php
$MESS["CHANNEL_SID_TIP"] = "選擇此處的民意測驗組將在此頁面上顯示。您可以選擇<b>所有組</b>以顯示所有活動的民意調查。";
$MESS["VOTE_FORM_TEMPLATE_TIP"] = "指定民意調查表的路徑名。 URL應包含輪詢ID。默認值是<b> fote_new.php？dote_id =＃vote_id＃</b>。";
$MESS["VOTE_RESULT_TEMPLATE_TIP"] = "指定民意調查結果圖的路徑名。 URL應包含輪詢ID。默認值是<b> fote_result.php？dote_id =＃vote_id＃</b>。";
