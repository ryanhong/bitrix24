<?php
$MESS["VOTE_ACTIVATE"] = "啟用";
$MESS["VOTE_ACTIVE"] = "積極的";
$MESS["VOTE_ADD_QUESTION"] = "添加一個問題";
$MESS["VOTE_CONFIRM_DEL_QUESTION"] = "您確定要刪除問題嗎？";
$MESS["VOTE_C_SORT"] = "種類。";
$MESS["VOTE_DEACTIVATE"] = "停用";
$MESS["VOTE_DIAGRAM"] = "圖表";
$MESS["VOTE_IMAGE_ID"] = "圖像";
$MESS["VOTE_QUESTION"] = "問題";
$MESS["VOTE_QUESTION_TYPE"] = "文本類型（文本，html）";
$MESS["VOTE_REQUIRED"] = "必需的";
$MESS["VOTE_TIMESTAMP_X"] = "修改的";
