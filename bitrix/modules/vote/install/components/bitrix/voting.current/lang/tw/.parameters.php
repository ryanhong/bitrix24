<?php
$MESS["F_VOTE_DAYS"] = "天";
$MESS["F_VOTE_HOURS"] = "小時";
$MESS["F_VOTE_MINUTES"] = "分鐘";
$MESS["F_VOTE_SECONDS"] = "秒";
$MESS["F_VOTE_UNIQUE"] = "否認一再投票";
$MESS["F_VOTE_UNIQUE_COOKIE_ONLY"] = "與相同的餅乾";
$MESS["F_VOTE_UNIQUE_IP_DELAY"] = "拒絕從同一IP中重複投票";
$MESS["F_VOTE_UNIQUE_IP_ONLY"] = "按IP基礎";
$MESS["F_VOTE_UNIQUE_SESSION"] = "在同一會話中";
$MESS["F_VOTE_UNIQUE_USER_ID_ONLY"] = "按用戶ID為基礎";
$MESS["VOTE_ALL_RESULTS"] = "顯示\“ text \”和\“ textarea \”字段的響應選項";
$MESS["VOTE_CHANNEL_SID"] = "民意測驗渠道";
$MESS["VOTE_SELECT_DEFAULT"] = "選擇民意測驗通道";
$MESS["VOTE_VOTE_ID"] = "投票ID";
