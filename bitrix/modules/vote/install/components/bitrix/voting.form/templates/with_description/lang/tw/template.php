<?php
$MESS["F_CAPTCHA_PROMT"] = "驗證圖像字符";
$MESS["F_CAPTCHA_TITLE"] = "垃圾郵件機器人保護（CAPTCHA）";
$MESS["VOTE_DROPDOWN_SET"] = "選擇一個值";
$MESS["VOTE_END_DATE"] = "結尾";
$MESS["VOTE_IS_ACTIVE"] = "民意調查是活躍的。";
$MESS["VOTE_IS_NOT_ACTIVE"] = "民意調查不活躍。";
$MESS["VOTE_RESET"] = "重置";
$MESS["VOTE_START_DATE"] = "開始";
$MESS["VOTE_SUBMIT_BUTTON"] = "投票";
$MESS["VOTE_VOTES"] = "投票";
