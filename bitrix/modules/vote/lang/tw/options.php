<?php
$MESS["MAIN_RESTORE_DEFAULTS"] = "恢復默認值";
$MESS["VOTE_COMPATIBLE"] = "支持舊模板（default.php，bluebar.php）";
$MESS["VOTE_PUBLIC_DIR"] = "民意調查文件夾（無網站文件夾）：";
$MESS["VOTE_RESET"] = "重置";
$MESS["VOTE_SAVE"] = "節省";
$MESS["VOTE_TEMPLATE_RESULTS_QUESTION"] = "問題的路徑結果查看模板：";
$MESS["VOTE_TEMPLATE_RESULTS_QUESTION_NEW"] = "相對問題結果視圖視圖模板：";
$MESS["VOTE_TEMPLATE_RESULTS_VOTE"] = "投票結果的路徑查看模板：";
$MESS["VOTE_TEMPLATE_VOTES"] = "通往民意調查形式模板的路徑：";
$MESS["VOTE_USE_HTML_EDIT"] = "使用HTML編輯器";
