<?php
$MESS["VOTE_MENU_ADDITIONAL"] = "更多的";
$MESS["VOTE_MENU_CHANNEL"] = "民意測驗渠道";
$MESS["VOTE_MENU_CHANNEL_ALT"] = "管理民意調查渠道";
$MESS["VOTE_MENU_MAIN"] = "民意調查";
$MESS["VOTE_MENU_MAIN_TITLE"] = "民意調查管理";
$MESS["VOTE_MENU_POLL_DESCRIPTION"] = "民意調查結果";
$MESS["VOTE_MENU_USER"] = "訪客";
$MESS["VOTE_MENU_USER_ALT"] = "參加民意調查的訪客名單";
$MESS["VOTE_MENU_VOTE"] = "民意調查清單";
$MESS["VOTE_MENU_VOTE_ALT"] = "管理現場訪問者民意測驗";
