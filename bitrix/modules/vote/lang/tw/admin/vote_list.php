<?php
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "檢查：";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "選擇：";
$MESS["MAIN_ADMIN_MENU_DELETE"] = "刪除";
$MESS["MAIN_ADMIN_MENU_EDIT"] = "編輯";
$MESS["VOTE_ACTIVATE"] = "啟用";
$MESS["VOTE_ACTIVE"] = "積極的";
$MESS["VOTE_ACTIVE_RED_LAMP"] = "民意調查不可見。";
$MESS["VOTE_ACTIVE_RED_LAMP_EXPIRED"] = "民意調查已過期。";
$MESS["VOTE_ACTIVE_RED_LAMP_UPCOMING"] = "民意調查仍在進行。";
$MESS["VOTE_ADD_LIST"] = "添加民意調查";
$MESS["VOTE_ALL"] = "（全部）";
$MESS["VOTE_AUTHOR_ID"] = "由...製作";
$MESS["VOTE_CHANNEL"] = "渠道";
$MESS["VOTE_CONFIRM_DEL_VOTE"] = "您確定要刪除民意調查嗎？";
$MESS["VOTE_CONFIRM_RESET_VOTE"] = "您確定要刪除民意調查的所有結果嗎？";
$MESS["VOTE_COPY"] = "複製";
$MESS["VOTE_COUNTER"] = "投票";
$MESS["VOTE_CREATE"] = "創造";
$MESS["VOTE_C_SORT"] = "種類。";
$MESS["VOTE_DATE_END"] = "結尾";
$MESS["VOTE_DATE_START"] = "開始";
$MESS["VOTE_DEACTIVATE"] = "停用";
$MESS["VOTE_DELETE"] = "刪除";
$MESS["VOTE_EDIT_TITLE"] = "修改民意調查設置";
$MESS["VOTE_EXACT_MATCH"] = "使用精確匹配";
$MESS["VOTE_FL_ACTIVE"] = "活動";
$MESS["VOTE_FL_CHANNEL"] = "渠道";
$MESS["VOTE_FL_COUNTER"] = "票數";
$MESS["VOTE_FL_DATE_END"] = "結束日期";
$MESS["VOTE_FL_DATE_START"] = "開始日期";
$MESS["VOTE_FL_ID"] = "投票ID";
$MESS["VOTE_FL_LAMP"] = "指標";
$MESS["VOTE_F_ACTIVE"] = "主動標誌已打開：";
$MESS["VOTE_F_CHANNEL"] = "民意調查渠道：";
$MESS["VOTE_F_CHANNEL_ID"] = "民意測驗頻道[ID]：";
$MESS["VOTE_F_COUNTER"] = "選票數：";
$MESS["VOTE_F_DATE_END"] = "結束日期";
$MESS["VOTE_F_DATE_START"] = "開始日期";
$MESS["VOTE_F_LAMP"] = "指標：";
$MESS["VOTE_F_TITLE"] = "標題：";
$MESS["VOTE_GREEN"] = "綠色的";
$MESS["VOTE_LAMP"] = "印第安";
$MESS["VOTE_LAMP_ACTIVE"] = "民意調查是活躍的，顯示期未過期";
$MESS["VOTE_NO"] = "不";
$MESS["VOTE_NOT_ACTIVE"] = "民意調查是不活動的。";
$MESS["VOTE_PAGES"] = "民意調查";
$MESS["VOTE_PAGE_TITLE"] = "民意調查清單";
$MESS["VOTE_PREVIEW"] = "形式";
$MESS["VOTE_PREVIEW_TITLE"] = "選擇民意調查顯示模板";
$MESS["VOTE_QUESTIONS"] = "問題";
$MESS["VOTE_RED"] = "紅色的";
$MESS["VOTE_RESET_NULL"] = "擦除";
$MESS["VOTE_RESULTS"] = "結果圖";
$MESS["VOTE_SAVE_ERROR"] = "錯誤寫入數據庫";
$MESS["VOTE_TILL"] = "直到";
$MESS["VOTE_TITLE"] = "標題";
$MESS["VOTE_VOTES_TITLE"] = "結果表";
$MESS["VOTE_WRONG_END_DATE_FROM"] = "請在\“結束日期\”的過濾器中輸入正確的\“ from \”日期";
$MESS["VOTE_WRONG_END_DATE_TILL"] = "請在\“結束日期\”的過濾器中輸入正確的\“ till \”日期";
$MESS["VOTE_WRONG_END_FROM_TILL"] = "\“ till \”日期必須大於\“ \” \“ \”最終日期\“ \”日期的\ \“日期";
$MESS["VOTE_WRONG_START_DATE_FROM"] = "請在\“啟動日期\”的過濾器中輸入正確的\“ from \”日期";
$MESS["VOTE_WRONG_START_DATE_TILL"] = "請在\“啟動日期\”的過濾器中輸入正確的\“ till \”日期";
$MESS["VOTE_WRONG_START_FROM_TILL"] = "\“ till \”日期必須大於\“ \” \“啟動日期\”的\“日期\”日期。";
$MESS["VOTE_YES"] = "是的";
