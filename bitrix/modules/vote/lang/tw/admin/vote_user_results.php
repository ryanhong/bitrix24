<?php
$MESS["VOTE_CHANNEL"] = "渠道：";
$MESS["VOTE_DATE"] = "日期：";
$MESS["VOTE_EDIT_USER"] = "用戶資料";
$MESS["VOTE_END_DATE"] = "結束日期：";
$MESS["VOTE_ENLARGE"] = "放大";
$MESS["VOTE_F_IP"] = "IP地址：";
$MESS["VOTE_GUEST"] = "遊客：";
$MESS["VOTE_NOT_AUTHORIZED"] = "未經授權";
$MESS["VOTE_NOT_FOUND"] = "未找到民意調查";
$MESS["VOTE_PAGE_TITLE"] = "投票＃＃ID＃";
$MESS["VOTE_PARAMS"] = "參數";
$MESS["VOTE_PARAMS_TITE"] = "參數設置";
$MESS["VOTE_RESULTS_LIST"] = "民意調查結果";
$MESS["VOTE_RESULT_NOT_FOUND"] = "未找到投票";
$MESS["VOTE_SESSION"] = "會議：";
$MESS["VOTE_START_DATE"] = "開始日期：";
$MESS["VOTE_USER_LIST"] = "訪客列表";
$MESS["VOTE_VALID"] = "有效的：";
$MESS["VOTE_VOTE"] = "輪詢：";
$MESS["VOTE_VOTES"] = "總投票：";
$MESS["VOTE_VOTE_IS_ACTIVE"] = "民意調查是<b>活動</b>";
$MESS["VOTE_VOTE_IS_NOT_ACTIVE"] = "民意調查不活躍";
