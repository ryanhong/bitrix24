<?php
$MESS["VOTE_ANSWERS_EMPTY"] = "問題\“＃問題＃\”沒有答案。";
$MESS["VOTE_CHANNEL_ID_ERR"] = "不正確的民意測驗小組。";
$MESS["VOTE_DENIED"] = "關閉";
$MESS["VOTE_EDIT"] = "添加民意調查";
$MESS["VOTE_EDIT_MY_OWN"] = "編輯自己的民意調查";
$MESS["VOTE_QUESTION_EMPTY"] = "問題文本未指定。";
$MESS["VOTE_READ"] = "查看結果";
$MESS["VOTE_VOTE_NOT_FOUND"] = "未找到民意調查＃id＃。";
$MESS["VOTE_WRITE"] = "投票";
