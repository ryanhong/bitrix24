<?php
$MESS["VOTE_DIAGRAM_TYPE_CIRCLE"] = "餅形圖";
$MESS["VOTE_DIAGRAM_TYPE_HISTOGRAM"] = "條形圖";
$MESS["VOTE_FORGOT_SITE"] = "該網站是必需的。";
$MESS["VOTE_FORGOT_SYMBOLIC_NAME"] = "需要符號ID。";
$MESS["VOTE_FORGOT_TITLE"] = "需要投票標題。";
$MESS["VOTE_INCORRECT_SYMBOLIC_NAME"] = "無效的民意測驗組符號ID（僅拉丁字母，數字和下劃線是可能的）。";
$MESS["VOTE_SYMBOLIC_NAME_ALREADY_IN_USE"] = "指定的符號ID已由民意測驗組## ID＃使用。";
