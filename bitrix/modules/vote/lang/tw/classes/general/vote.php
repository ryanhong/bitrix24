<?php
$MESS["USER_VOTE_EMPTY"] = "請選擇一個選項。";
$MESS["VOTE_ACCESS_DENIED"] = "該民意調查的訪問被拒絕。";
$MESS["VOTE_ALREADY_VOTE"] = "您不能兩次參加此民意調查。";
$MESS["VOTE_BAD_CAPTCHA"] = "您輸入的代碼不正確。";
$MESS["VOTE_EMPTY_CHANNEL_ID"] = "未指定民意測驗組。";
$MESS["VOTE_NOT_FOUND"] = "未找到民意調查。";
$MESS["VOTE_RED_LAMP"] = "民意調查不活躍。";
$MESS["VOTE_REQUIRED_MISSING"] = "您沒有回答強制性問題。";
$MESS["VOTE_WRONG_CHANNEL_ID"] = "無效的民意測驗小組。";
$MESS["VOTE_WRONG_DATE_END"] = "請輸入正確的民意調查結束日期";
$MESS["VOTE_WRONG_DATE_START"] = "請輸入正確的民意調查開始日期";
$MESS["VOTE_WRONG_DATE_TILL"] = "民意調查結束日期應大於民意調查的開始日期";
$MESS["VOTE_WRONG_INTERVAL"] = "進行此民意調查的時間間隔與同一組的民意調查＃＃ID＃相交";
