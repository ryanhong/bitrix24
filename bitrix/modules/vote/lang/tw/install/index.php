<?php
$MESS["VOTE_INSTALL_TITLE"] = "民意調查和調查模塊安裝";
$MESS["VOTE_MODULE_DESCRIPTION"] = "該模塊將訪問者的民意調查和調查功能添加到站點。";
$MESS["VOTE_MODULE_NAME"] = "民意調查";
$MESS["VOTE_UNINSTALL_TITLE"] = "民意調查和調查模塊卸載";
