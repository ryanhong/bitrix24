<?php
$MESS["VOTE_FOR_DESC"] = "＃id＃ - 投票結果ID
＃時間＃ - 投票時間
＃投票_Title＃ - 民意調查名稱
＃投票_DESCRIPTION＃ -ROUL描述
＃投票_ID＃ - 投票ID
＃頻道＃ - 民意調查組名稱
＃channel_id＃ - 投票組ID
＃poter_id＃ - 選民的用戶ID
＃user_name＃ - 用戶全名
＃登錄＃-登錄
＃USER_ID＃ - 用戶ID
＃stat_guest_id＃ -  Web分析模塊中的訪問者ID
＃session_id＃ -  Web分析模塊中的會話ID
＃IP＃ -  IP地址
＃投票_statistic＃ - 此民意調查類型的摘要統計（ - 問題 - 答案）
＃URL＃ - 民意調查URL";
$MESS["VOTE_FOR_MESSAGE"] = "＃user_name＃在民意調查中投票\“＃dote_title＃\”：
＃投票_statistic＃

http：//＃server_name ## url＃
自動生成的消息。";
$MESS["VOTE_FOR_NAME"] = "新投票";
$MESS["VOTE_FOR_SUBJECT"] = "＃site_name＃：[v]＃投票_title＃";
