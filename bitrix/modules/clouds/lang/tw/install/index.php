<?php
$MESS["CLO_INSTALL_TITLE"] = "雲存儲模塊安裝";
$MESS["CLO_MODULE_DESCRIPTION"] = "支持雲文件存儲。";
$MESS["CLO_MODULE_NAME"] = "雲存儲";
$MESS["CLO_UNINSTALL_TITLE"] = "雲存儲模塊卸載";
