<?php
$MESS["CLO_STORAGE_ESTIMATE_DUPLICATES"] = "估計大小和重複的數量";
$MESS["CLO_STORAGE_LIST_ACTIVATE"] = "啟用";
$MESS["CLO_STORAGE_LIST_ACTIVE"] = "積極的";
$MESS["CLO_STORAGE_LIST_ADD"] = "添加";
$MESS["CLO_STORAGE_LIST_ADD_TITLE"] = "添加了新的雲存儲連接";
$MESS["CLO_STORAGE_LIST_BUCKET"] = "桶";
$MESS["CLO_STORAGE_LIST_CANNOT_DELETE"] = "無法刪除連接：＃error_msg＃。";
$MESS["CLO_STORAGE_LIST_CONT_MOVE_FILES"] = "繼續將文件移至雲存儲";
$MESS["CLO_STORAGE_LIST_COPY"] = "將數據複製到內核模塊。";
$MESS["CLO_STORAGE_LIST_DEACTIVATE"] = "停用";
$MESS["CLO_STORAGE_LIST_DEACTIVATE_CONF"] = "停用雲存儲連接？";
$MESS["CLO_STORAGE_LIST_DELETE"] = "刪除";
$MESS["CLO_STORAGE_LIST_DELETE_CONF"] = "刪除雲存儲連接？";
$MESS["CLO_STORAGE_LIST_DOWNLOAD_DONE"] = "文件已從存儲中下載。";
$MESS["CLO_STORAGE_LIST_DOWNLOAD_IN_PROGRESS"] = "現在正在從存儲中下載文件。";
$MESS["CLO_STORAGE_LIST_DOWNLOAD_PROGRESS"] = "
<b>＃保留＃（#bytes＃）</b>保留。
";
$MESS["CLO_STORAGE_LIST_DUPLICATES_INFO"] = "
重複數：<b> #count＃</b> <br>
總尺寸：<b> #size＃</b> <br>
<a href= \"#list_link# \ \">查看文件</a> <br>
<b>注意！</b> <br>
1.並非所有重複項都可以安全刪除。只有在數據庫中註冊並由系統模塊管理的重複項可以安全刪除。<br>
將不會刪除臨時或服務文件夾中的重複項（resize_cache，bizproc等）。<br>
2.要搜索重複項，系統比較文件大小和校驗和。沒有執行文件內容分析。<br>
可能存在兩個具有相同尺寸和校驗和校驗和校驗值的內容不同的文件的情況。
";
$MESS["CLO_STORAGE_LIST_DUPLICATES_RESULT"] = "重複的搜索結果";
$MESS["CLO_STORAGE_LIST_EDIT"] = "編輯";
$MESS["CLO_STORAGE_LIST_FILE_COUNT"] = "文件";
$MESS["CLO_STORAGE_LIST_FILE_SIZE"] = "尺寸";
$MESS["CLO_STORAGE_LIST_ID"] = "ID";
$MESS["CLO_STORAGE_LIST_LISTING"] = "獲取雲存儲文件列表";
$MESS["CLO_STORAGE_LIST_MODE"] = "模式";
$MESS["CLO_STORAGE_LIST_MOVE_DONE"] = "文件已移至存儲。";
$MESS["CLO_STORAGE_LIST_MOVE_FILE_ERROR"] = "錯誤將文件移至雲存儲。";
$MESS["CLO_STORAGE_LIST_MOVE_IN_PROGRESS"] = "文件現在正在轉移到存儲空間。";
$MESS["CLO_STORAGE_LIST_MOVE_LOCAL"] = "從雲存儲中下載文件";
$MESS["CLO_STORAGE_LIST_MOVE_LOCAL_CONF"] = "您確定要將文件從雲移回服務器嗎？";
$MESS["CLO_STORAGE_LIST_MOVE_PROGRESS"] = "
處理的總文件：<b> #total＃</b>。<br>
移動文件：<b>＃移動＃（#bytes＃）</b>，跳過文件：<b> #skiped＃</b>。
";
$MESS["CLO_STORAGE_LIST_NOT_EMPTY"] = "存儲中當前有文件";
$MESS["CLO_STORAGE_LIST_READ_ONLY"] = "只讀";
$MESS["CLO_STORAGE_LIST_READ_WRITE"] = "讀/寫";
$MESS["CLO_STORAGE_LIST_SERVICE"] = "服務";
$MESS["CLO_STORAGE_LIST_SORT"] = "排序";
$MESS["CLO_STORAGE_LIST_START_MOVE_FILES"] = "將文件上傳到雲存儲";
$MESS["CLO_STORAGE_LIST_STOP"] = "停止";
$MESS["CLO_STORAGE_LIST_TITLE"] = "雲存儲";
