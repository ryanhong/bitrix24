<?php
$MESS["CLO_404_ON_MOVED_FILE"] = "移動文件時發生錯誤404。";
$MESS["CLO_STORAGE_MENU"] = "雲儲存";
$MESS["CLO_STORAGE_TITLE"] = "在雲存儲中查看和管理文件";
$MESS["CLO_STORAGE_UPLOAD_CONF"] = "您確定要將文件上傳到雲存儲嗎？";
$MESS["CLO_STORAGE_UPLOAD_MENU"] = "移至雲存儲";
