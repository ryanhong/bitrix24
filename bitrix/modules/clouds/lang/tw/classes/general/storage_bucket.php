<?php
$MESS["CLO_STORAGE_BAD_BUCKET_NAME"] = "存儲桶ID包含無效的字符（僅使用小寫的拉丁字母，數字和破折號\“  -  \”）。";
$MESS["CLO_STORAGE_BAD_BUCKET_NAME2"] = "水桶ID必須以字母或數字開始和結尾。";
$MESS["CLO_STORAGE_BAD_BUCKET_NAME3"] = "僅允許數字和字母符號在點旁邊（之前和之後）。";
$MESS["CLO_STORAGE_BUCKET_ALREADY_EXISTS"] = "雲存儲中已經存在一個帶有此ID的水庫。 ID必須在所有云存儲用戶中都是唯一的。";
$MESS["CLO_STORAGE_CLOUD_ADD_ERROR"] = "錯誤創建一個存儲桶：＃error_msg＃。";
$MESS["CLO_STORAGE_CLOUD_BUCKET_NOT_EMPTY"] = "錯誤刪除存儲桶：容器仍然具有文件。";
$MESS["CLO_STORAGE_CLOUD_DELETE_ERROR"] = "錯誤刪除存儲桶：＃error_msg＃。";
$MESS["CLO_STORAGE_DB_DELETE_ERROR"] = "雲存儲桶已被刪除，但是從數據庫中刪除了存儲桶描述。";
$MESS["CLO_STORAGE_EMPTY_BUCKET"] = "丟失了存儲桶ID。";
$MESS["CLO_STORAGE_UNKNOWN_SERVICE"] = "未知服務";
$MESS["CLO_STORAGE_WRONG_BUCKET_NAME_LENGTH"] = "有效的存儲桶ID必須包含2到64個字符。";
$MESS["CLO_STORAGE_WRONG_BUCKET_NAME_LENGTH2"] = "存儲桶ID的每個部分（點之間的字符串）的長度必須包含2到64個字符。";
$MESS["CLO_STORAGE_WRONG_SERVICE"] = "錯誤的存儲桶服務。";
