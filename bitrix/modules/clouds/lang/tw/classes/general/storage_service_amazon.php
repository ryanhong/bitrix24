<?php
$MESS["CLO_STORAGE_AMAZON_EDIT_ACCESS_KEY"] = "訪問密鑰";
$MESS["CLO_STORAGE_AMAZON_EDIT_HELP"] = "在開始使用此服務之前，您必須在<a href= \"http://aws.amazon.com/s3/x3/x3/drover上註冊，並激活S3 Access。可以通過<a href= \"https://aws-portal.amazon.com/gp/aws/developer/account/index.html?ie=utf8＆action = access-key \">安全憑證</a>。";
$MESS["CLO_STORAGE_AMAZON_EDIT_SECRET_KEY"] = "密鑰";
$MESS["CLO_STORAGE_AMAZON_EMPTY_ACCESS_KEY"] = "未指定訪問密鑰。";
$MESS["CLO_STORAGE_AMAZON_EMPTY_SECRET_KEY"] = "秘密密鑰未指定。";
