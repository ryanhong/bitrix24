<?php
$MESS["CLO_STORAGE_GOOGLE_EDIT_ACCESS_KEY"] = "訪問密鑰";
$MESS["CLO_STORAGE_GOOGLE_EDIT_HELP"] = "在開始使用此服務之前，您必須激活<a href= \"https://cloud.google.com/storage/storage/docs/signup \"> Google for Developers for Developers </a>。然後，使用<a href= \"https://code.google.com/apis/console#:storage \ \"> api Google Console </a>激活傳統訪問並啟用帳單。";
$MESS["CLO_STORAGE_GOOGLE_EDIT_PROJECT_ID"] = "項目ID";
$MESS["CLO_STORAGE_GOOGLE_EDIT_SECRET_KEY"] = "密鑰";
$MESS["CLO_STORAGE_GOOGLE_EMPTY_ACCESS_KEY"] = "未指定訪問密鑰。";
$MESS["CLO_STORAGE_GOOGLE_EMPTY_PROJECT_ID"] = "未指定項目ID。";
$MESS["CLO_STORAGE_GOOGLE_EMPTY_SECRET_KEY"] = "秘密密鑰未指定。";
$MESS["CLO_STORAGE_GOOGLE_XML_ERROR"] = "服務錯誤：＃errmsg＃";
$MESS["CLO_STORAGE_GOOGLE_XML_PARSE_ERROR"] = "無法識別的服務響應（錯誤## errno＃）";
