<?php
$MESS["MB_TASKS_CONTROLS_TITLE"] = "任務類別";
$MESS["MB_TASKS_PANEL_TAB_ALL"] = "全部";
$MESS["MB_TASKS_PANEL_TAB_PROJECTS"] = "專案";
$MESS["MB_TASKS_ROLES_TASK_ADD"] = "新任務";
$MESS["MB_TASKS_ROLES_VIEW_TASK_CATEGORY_EXPIRED"] = "逾期";
$MESS["MB_TASKS_ROLES_VIEW_TASK_CATEGORY_EXPIRED_CANDIDATES"] = "幾乎逾期了";
$MESS["MB_TASKS_ROLES_VIEW_TASK_CATEGORY_NEW"] = "未讀";
$MESS["MB_TASKS_ROLES_VIEW_TASK_CATEGORY_WAIT_CTRL"] = "待審核";
$MESS["MB_TASKS_ROLES_VIEW_TASK_CATEGORY_WO_DEADLINE"] = "沒有截止日期<br>（我分配）";
$MESS["MB_TASKS_ROLES_VIEW_TASK_CATEGORY_WO_DEADLINE_FOR_ME"] = "沒有截止日期<br>（分配給我）";
