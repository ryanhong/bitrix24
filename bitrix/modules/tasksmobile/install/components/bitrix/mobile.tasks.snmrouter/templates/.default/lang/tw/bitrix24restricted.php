<?php
$MESS["TASK_RESTRICTED_ADMIN1_MSGVER_1"] = "<h1>此功能在您的計劃上不可用</h1> <p>業務工具僅適用於當前計劃中的24個用戶。</p> <p>選擇可以訪問業務工具的用戶，您必須必須在瀏覽器中打開BitRix24的Web版本。</p>";
$MESS["TASK_RESTRICTED_ADMIN2_MSGVER_1"] = "打開網絡版本";
$MESS["TASK_RESTRICTED_USER1"] = "<h1>您的計劃限制了您的計劃</h1> <p>您的計劃中的業務工具可用於多達24個用戶。</p> <p>如果您需要訪問業務工具，請聯繫您的BitRix24管理員。</p>";
$MESS["TASK_RESTRICTED_USER2"] = "發送請求";
$MESS["TASK_RESTRICTED_USER3"] = "請求已發送";
