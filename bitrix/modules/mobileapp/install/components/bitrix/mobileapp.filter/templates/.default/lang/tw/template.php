<?php
$MESS["MOBILE_APP_FILTER_APPLY"] = "應用過濾器";
$MESS["MOBILE_APP_FILTER_BACK"] = "後退";
$MESS["MOBILE_APP_FILTER_FIELDS_LIST"] = "字段";
$MESS["MOBILE_APP_FILTER_FIELDS_RESET"] = "重新設置領域";
$MESS["MOBILE_APP_FILTER_SAVE"] = "節省";
$MESS["MOBILE_APP_FILTER_SAVING"] = "保存...";
$MESS["MOBILE_APP_FILTER_TITLE"] = "濾波器參數";
$MESS["MOBILE_APP_FILTER_VISIBLE_FIELDS"] = "可見的字段";
$MESS["MOBILE_APP_OFFLINE_MESSAGE"] = "無網絡連接";
$MESS["MOBILE_APP_OFFLINE_TITLE"] = "無法完成操作";
