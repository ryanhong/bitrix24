<?php
$MESS["DEMO_MENU_ALERT"] = "警報和束縛";
$MESS["DEMO_MENU_API"] = "檢查API版本";
$MESS["DEMO_MENU_BUTTONS"] = "鈕扣";
$MESS["DEMO_MENU_CHOSE_PHOTO"] = "照片選擇器";
$MESS["DEMO_MENU_CONTEXT_MENU"] = "內容選單";
$MESS["DEMO_MENU_DOCS"] = "文件";
$MESS["DEMO_MENU_LISTS"] = "列表";
$MESS["DEMO_MENU_LOADING"] = "裝載機";
$MESS["DEMO_MENU_LOCAL_NOTIF"] = "本地通知";
$MESS["DEMO_MENU_MAIN"] = "主要的";
$MESS["DEMO_MENU_NAV_BAR"] = "導航欄";
$MESS["DEMO_MENU_PHOTOGALLERY"] = "照片庫";
$MESS["DEMO_MENU_PICKERS"] = "採摘者";
$MESS["DEMO_MENU_QR"] = "QR掃描儀";
$MESS["DEMO_MENU_REFRESH"] = "拉力到倒數";
$MESS["DEMO_MENU_SECTION_MAIN"] = "主菜單";
$MESS["DEMO_MENU_SLIDING_PANEL"] = "滑桿";
$MESS["DEMO_MENU_TEXT_PANEL"] = "文字欄";
