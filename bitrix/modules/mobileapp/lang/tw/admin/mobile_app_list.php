<?php
$MESS["MOBILEAPP_ADD_APP"] = "添加應用程序";
$MESS["MOBILEAPP_EDIT_APP"] = "編輯";
$MESS["MOBILEAPP_REMOVE_APP"] = "刪除";
$MESS["MOBILEAPP_REMOVE_APP_CONFIRM"] = "您確定要刪除應用程序嗎？應用程序創建的所有相關首選項和文件也將被刪除。";
