<?php
$MESS["MOBILEAPP_CALL_BUSY"] = "無法撥打呼叫，因為其他用戶目前在其他呼叫上。";
$MESS["MOBILEAPP_CALL_DECLINE"] = "用戶取消了通話";
$MESS["MOBILEAPP_CALL_NO_ACCESS"] = "呼叫斷開連接：也許其他參與者沒有網絡攝像頭或麥克風。";
