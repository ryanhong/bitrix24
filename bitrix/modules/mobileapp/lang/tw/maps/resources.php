<?php
$MESS["RES_GP_PROMO_IMAGE"] = "Google Play中的“推薦\”區域的圖像";
$MESS["RES_ICON"] = "圖標（＃size＃）";
$MESS["RES_LAUNCH_SCREEN_LAND"] = "啟動屏幕，景觀（＃size＃）";
$MESS["RES_LAUNCH_SCREEN_PORT"] = "啟動屏幕，肖像（＃size＃）";
$MESS["RES_LSCREEN_IPAD_NON_RETINA_LAND"] = "iPad，景觀（＃size＃）";
$MESS["RES_LSCREEN_IPAD_NON_RETINA_PORT"] = "iPad，肖像（＃size＃）";
$MESS["RES_LSCREEN_IPAD_RETINA_LAND"] = "iPad Retina，景觀（＃size＃）";
$MESS["RES_LSCREEN_IPAD_RETINA_PORT"] = "iPad Retina，肖像（＃size＃）";
$MESS["RES_LSCREEN_IPHONE_INCH_3_5"] = "iPhone Retina 3.5'（4/4S，＃size＃）";
$MESS["RES_LSCREEN_IPHONE_INCH_4"] = "iPhone Retina 4'（5/5C/5S，＃size＃）";
$MESS["RES_LSCREEN_IPHONE_INCH_4_7_PORT"] = "iPhone Retina 4.7'，肖像（＃size＃）";
$MESS["RES_LSCREEN_IPHONE_INCH_5_5_LAND"] = "iPhone Retina 5.5'，景觀（＃size＃）";
$MESS["RES_LSCREEN_IPHONE_INCH_5_5_PORT"] = "iPhone Retina 5.5'，肖像（＃size＃）";
