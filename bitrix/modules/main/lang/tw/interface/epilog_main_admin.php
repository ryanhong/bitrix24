<?php
$MESS["ADMIN_FAV_ADD"] = "添加到<b>最愛</b>";
$MESS["ADMIN_FAV_ADD_ERROR"] = "錯誤將鏈接添加到收藏夾。";
$MESS["ADMIN_FAV_ADD_SUCCESS"] = "添加到<b>最愛</b>";
$MESS["ADMIN_FAV_DEL"] = "從<b>收藏夾刪除</b>";
$MESS["ADMIN_FAV_DEL_ERROR"] = "從收藏夾中刪除錯誤。";
$MESS["ADMIN_FAV_DEL_SUCCESS"] = "從<b>收藏夾中刪除</b>";
$MESS["ADMIN_FAV_GOTO"] = "查看最愛";
$MESS["ADMIN_FAV_HINT"] = "在這裡拖放項目";
$MESS["EPILOG_ADMIN_POWER"] = "供電";
$MESS["epilog_admin_copyrights"] = "版權所有者";
$MESS["epilog_support_link"] = "支持";
