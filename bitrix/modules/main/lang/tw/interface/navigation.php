<?php
$MESS["navigation_first"] = "第一頁";
$MESS["navigation_last"] = "最後一頁";
$MESS["navigation_next"] = "下一頁";
$MESS["navigation_prev"] = "上一頁";
$MESS["navigation_records"] = "記錄：";
$MESS["navigation_records_all"] = "全部";
$MESS["navigation_records_of"] = "的";
