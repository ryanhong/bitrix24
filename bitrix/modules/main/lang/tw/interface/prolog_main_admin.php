<?php
$MESS["DEVSERVER_ADMIN_MESSAGE"] = "此安裝作為Bitrix網站經理開發工作台存在。不得將其用作公開可用的網站。";
$MESS["MAIN_PROLOG_ADMIN_LOGOUT"] = "註銷";
$MESS["MAIN_PROLOG_ADMIN_TITLE"] = "行政部門";
$MESS["MAIN_PR_ADMIN_CUR_LINK"] = "鏈接到當前頁面";
$MESS["MAIN_PR_ADMIN_FAV"] = "最愛";
$MESS["MAIN_PR_ADMIN_FAV_ADD"] = "添加到收藏夾";
$MESS["MAIN_PR_ADMIN_FAV_DEL"] = "從收藏夾中刪除";
$MESS["TRIAL_ATTENTION"] = "注意力！使用<a href= \"/bitrix/admin/sysupdate.php \"> siteupdate </a>技術以獲取最新產品版本。<br>";
$MESS["TRIAL_ATTENTION_TEXT1_1c_bitrix"] = "這是Bitrix網站管理器產品的試用版。";
$MESS["TRIAL_ATTENTION_TEXT1_1c_bitrix_eduportal"] = "這是1C-Bitrix教育門戶產品的試用版。";
$MESS["TRIAL_ATTENTION_TEXT1_1c_bitrix_gosportal"] = "這是1C-Bitrix政府門戶產品的試用版。";
$MESS["TRIAL_ATTENTION_TEXT1_1c_bitrix_gossite"] = "這是1C-Bitrix政府網站產品的試用版。";
$MESS["TRIAL_ATTENTION_TEXT1_1c_bitrix_portal"] = "這是1C-Bitrix Intranet產品的試用版。";
$MESS["TRIAL_ATTENTION_TEXT1_bitrix"] = "這是Bitrix網站管理器產品的試用版。";
$MESS["TRIAL_ATTENTION_TEXT1_bitrix_portal"] = "這是Bitrix24產品的試用版。";
$MESS["TRIAL_ATTENTION_TEXT1_ua_bitrix_portal"] = "這是Bitrix Intranet產品的試用版。";
$MESS["TRIAL_ATTENTION_TEXT2"] = "評估期到期";
$MESS["TRIAL_ATTENTION_TEXT3"] = "天";
$MESS["TRIAL_ATTENTION_TEXT4_1c_bitrix"] = "Bitrix網站經理產品的試用期已經過期。該站點將在兩週內完全停止工作。";
$MESS["TRIAL_ATTENTION_TEXT4_1c_bitrix_eduportal"] = "1C-Bitrix教育門戶產品的試驗期已經過期。該站點將在兩週內完全停止工作。";
$MESS["TRIAL_ATTENTION_TEXT4_1c_bitrix_gosportal"] = "1C-Bitrix政府門戶產品的試用期已過期。該站點將在兩週內完全停止工作。";
$MESS["TRIAL_ATTENTION_TEXT4_1c_bitrix_gossite"] = "1C-Bitrix政府網站產品的試用期已經過期。該站點將在兩週內完全停止工作。";
$MESS["TRIAL_ATTENTION_TEXT4_1c_bitrix_portal"] = "Bitrix24產品的試驗期已經過期。該站點將在兩週內完全停止工作。";
$MESS["TRIAL_ATTENTION_TEXT4_bitrix"] = "Bitrix網站經理產品的試用期已經過期。該站點將在兩週內完全停止工作。";
$MESS["TRIAL_ATTENTION_TEXT4_bitrix_portal"] = "Bitrix24產品的試驗期已經過期。該站點將在兩週內完全停止工作。";
$MESS["TRIAL_ATTENTION_TEXT4_ua_bitrix_portal"] = "Bitrix24產品的試驗期已經過期。該站點將在兩週內完全停止工作。";
$MESS["TRIAL_ATTENTION_TEXT5_1c_bitrix"] = "您可以從<a href= \"http://www.bitrixsoft.com/buy/buy/ \ \"> http://www.bitrixsoft.com/buy/ </a>頁面。";
$MESS["TRIAL_ATTENTION_TEXT5_1c_bitrix_eduportal"] = "您可以從<a href= \"http://www.bitrixsoft.com/buy/buy/xhtp://wwwww.bitrixsoft.com/buy/buy//< >頁面。";
$MESS["TRIAL_ATTENTION_TEXT5_1c_bitrix_gosportal"] = "您可以從<a href= \"http://www.bitrixsoft.com/buy/buy/xhorptp://wwwww.bitrixsoft.com/buy/buy//< >頁面。";
$MESS["TRIAL_ATTENTION_TEXT5_1c_bitrix_gossite"] = "您可以從<a href= \之http://www.bitrixsoft.com/buy/buy/xprol購買1C-Bitrix政府網站的完整版本>頁面。";
$MESS["TRIAL_ATTENTION_TEXT5_1c_bitrix_portal"] = "您可以從<a href= \"http://www.bitrixsoft.com/buy/buy/ \ \"> http://www.bitrixsoft.com/buy/ </a>購買完整版本的完整版本。頁。";
$MESS["TRIAL_ATTENTION_TEXT5_bitrix"] = "您可以從<a href= \"http://www.bitrixsoft.com/buy/buy/ \ \"> http://www.bitrixsoft.com/buy/ </a>頁面。";
$MESS["TRIAL_ATTENTION_TEXT5_bitrix_portal"] = "You can purchase the full version of Bitrix24 from the <a href=\"https://www.bitrix24.com/prices/self-hosted.php\">https://www.bitrix24.com/prices/self- hosted.php </a>頁面。";
$MESS["TRIAL_ATTENTION_TEXT5_ua_bitrix_portal"] = "您可以從<a href= \"https://www.bitrix.ua/buy/buy/intranet.php \"> https://www.bitrix.ua/</a/a/A >頁面。";
$MESS["admin_panel_browser"] = "控制面板不支持Internet Explorer 7或以下。請安裝現代瀏覽器：<a href= \"http://www.firefox.com \"> firefox </a>，<a href = \' > chrome </a>，<a href= \"http ://www.opera.com \"> opera </a>或<a href = \ \'http://www.microsoft.com/windows/windows/internet -Explorer/\“> Microsoft Edge </a >。";
$MESS["main_prolog_help"] = "幫助";
$MESS["prolog_admin_headers_sent"] = "注意力！以下系統文件中已經檢測到無效的字符：＃文件＃，行＃行＃。";
$MESS["prolog_main_hide_menu"] = "隱藏菜單";
$MESS["prolog_main_less_buttons"] = "在菜單中使用小按鈕";
$MESS["prolog_main_m_e_n_u"] = "m <br> e <br> n <br> u <br>";
$MESS["prolog_main_more_buttons"] = "放大按鈕";
$MESS["prolog_main_show_menu"] = "顯示菜單";
$MESS["prolog_main_support1"] = "<span class = \“必需\”>注意！</span>您的TechSupport和Update subscription <b>將在＃finish_date＃，＃days_ago＃上到期</b>。您可以購買<a href= \"http://www.bitrixsoft.com/support/key_info.php?license_key_key = #plicense_key.key.key.key.trognit= \"_blank \">一個月的早期訂閱< /a>到期日期後。";
$MESS["prolog_main_support2"] = "<span class = \“必需\”>注意！</span>您的TechSupport和Update subscription <b>已在＃finish_date＃，<b>＃days_ago＃＆nbsp; days </b> ogo上過期</b >。您可以購買<a href= \\http://www.bitrixsoft.com/support/key_info.php?license_key_key = #plicense_key.key.key.key.trognit= \“target= \"_blank \">一個月的早期訂閱</a>到期日期後。";
$MESS["prolog_main_support3"] = "<span class = \“必需\”>注意！</span>您的TechSupport和Update subscription <b>已在＃finish_date＃上過期</b>。您可以購買<a href= \之http://www.bitrixsoft.com/support/key_info.php?license_key_key = #plicense_key.key.key.key. target= \"_blank \ \"> late subscription renewal </a >。";
$MESS["prolog_main_support11"] = "<span class = \“必需\”>注意！</span>您的TechSupport和Update subscription <b>將在＃finish_date＃，#days_ago＃。";
$MESS["prolog_main_support21"] = "<span class = \“必需\”>注意！</span>您的TechSupport和Update sublcription已在＃finish_date＃，<b>＃days_ago＃＆nbsp; </b>幾天前到期。what_is_it＃<br />您的早期更新寬限期將以＃sup_finish_date＃結束。";
$MESS["prolog_main_support31"] = "<span class = \“必需\”> <span class = \“必需\”>注意！< /span>您的techsupport和update sublcription已在#finish_date＃＃。＃what_is_it＃<br />您現在可以更新您的訂閱。";
$MESS["prolog_main_support_almost_expire"] = "<span class = \“必需\”>注意！</span>您的許可證將在＃finish_date＃上到期</b>。 （<span onclick = \“ bx.toggle（bx（'supdescr'））\” style ='border-bottom：1px虛線＃1c91e7; color：＃1c91e7; cursor：pointer：pointer：pointer;'> <br / >如果您未能續訂許可證，則將失去對更新和技術支持的訪問。";
$MESS["prolog_main_support_button_no_prolong"] = "不，謝謝";
$MESS["prolog_main_support_button_no_prolong2"] = "稍後提醒我";
$MESS["prolog_main_support_button_prolong"] = "續訂訂閱";
$MESS["prolog_main_support_days"] = "在<b>＃n_days_ago＃＆nbsp; day </b>";
$MESS["prolog_main_support_expired"] = "<span class = \“必需\”>注意！</span>您的許可證<b>在＃finish_date＃上已過期</b>。 （<span onclick = \“ bx.toggle（bx（'supdescr'））\” style ='border-bottom：1px虛線＃1c91e7; color：＃1c91e7; cursor：pointer：pointer：pointer;'> <br / >您無法訪問更新或技術支持。";
$MESS["prolog_main_support_menu1"] = "在：";
$MESS["prolog_main_support_menu2"] = "星期";
$MESS["prolog_main_support_menu3"] = "兩個星期";
$MESS["prolog_main_support_menu4"] = "<span style = \“顏色：紅色; \”>月</span>";
$MESS["prolog_main_support_wit_descr1"] = "什麼是訂閱到期？";
$MESS["prolog_main_support_wit_descr2"] = "一旦您的技術支持和更新訂閱過期，您將不再擁有訪問市場。有效地
這意味著您將無法安裝平台更新，購買，安裝或
更新市場解決方案。您的技術支持優先級將降級為
對於常見用戶，響應時間長達24小時。你仍然可以
只要您需要，繼續使用產品。要訪問系統更新和市場，您必須續訂訂閱。<br/> <br/>
 在到期後，您將獲得一個月（30天）的寬限期
續訂成本僅是您產品版本價格的22％（早期續訂）。<br/> <br/>
 在此期間，續訂成本是您產品價格的60％
版本（晚更新）。";
$MESS["prolog_main_support_wit_descr2_cp"] = "訂閱技術支持和產品更新後，您的產品副本無法安裝平台更新；您將無法獲得新產品版本，安裝或更新市場解決方案，使用Web電話服務或免費\“ Cloud Backup \”功能。此外，您提交給我們的Helpdesk的支持票將以較低的優先級處理（您可能需要等待48小時）。<br /> <br />
儘管續訂技術支持和產品更新訂閱不是強制性的，但我們鼓勵您再續訂一年。產品更新包括關鍵的錯誤修復，補丁和新功能。借助Active訂閱，您可以安裝新的模塊，功能和站點模板，這些模板可與每個新產品發行版一起使用，並將產品副本升級到最新版本。
<br /> <br />
您可以以您的產品版本當前價格的25％（產品選項）續簽維護訂閱一年。
<br /> <br />
對於較舊的產品版本（\“ Infopace \”，\“ Teampace \”，\“ bizpace \”，\“ bizpace Enterprise \”），您可以以當前產品價格的22％的22％的價格將維護訂閱續訂一年（早期續訂選項）在您的維護訂閱到期日期後的30天期內，或此後任何時候以當前產品價格的60％（後期續簽期權）。請注意，對於較舊的產品版本，必須另外續訂任何數量的其他Intranet用戶（超過默認的25用戶包的用戶）。
<br /> <br />
有關更多詳細信息，請隨時參考<a href= \"https://store.bitrix24.com/help/licensing-policy.php \">許可策略</a>頁面。
";
$MESS["prolog_main_support_wit_description_bus"] = "許可證到期後，您將無法訪問更新，技術支持和某些系統功能。<br /> <br />
以下功能將不可用：<br /> <br />
 - 安全掃描儀更新; <br />
-bitrix24.Sites Designer; <br />
-bitrix24.market解決方案。<br /> <br />
您可以找到不可用的功能的完整列表<a href= \"https://www.bitrix24.com/eula/limitations.php \" target = \"_blank \">在這裡</a>。<br /> <br />
有關更多詳細信息，請參閱<a href= \"#link# \" target= \"_blank \">許可協議</a>。";
$MESS["prolog_main_support_wit_description_cp"] = "許可證到期後，您將無法訪問更新，技術支持和某些系統功能。<br /> <br />
以下功能將不可用：<br /> <br />
 - 電話; <br />
 - 開放頻道； <br />
-bitrix24.market解決方案。<br /> <br />
您可以找到不可用的功能的完整列表<a href= \"https://www.bitrix24.com/eula/limitations.php \" target = \"_blank \">在這裡</a>。<br /> <br />
有關更多詳細信息，請參閱<a href= \"#link# \" target= \"_blank \">許可協議</a>。";
$MESS["prolog_main_timelimit_almost_expire"] = "<span class = \“必需\”>注意！</span>您的許可證將在＃finish_date＃上到期</b>。如果您不續訂許可證，則無法使用Bitrix24。態";
$MESS["prolog_main_timelimit_expired"] = "<span class = \“必需\”>注意！</span>您的許可證<b>在＃finish_date＃上已過期</b>。如果您不通過＃block_date＃續訂許可證，則無法使用Bitrix24。態";
$MESS["prolog_main_today"] = "<b>今天</b>";
