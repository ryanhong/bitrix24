<?php
$MESS["main_epilog_before_menu_edit"] = "編輯中的編輯頁面";
$MESS["main_epilog_before_menu_edit_html"] = "編輯頁面為html";
$MESS["main_epilog_before_menu_edit_html_title"] = "編輯頁面html代碼";
$MESS["main_epilog_before_menu_edit_title"] = "視覺編輯器中的編輯頁面";
$MESS["main_epilog_before_menu_prop"] = "編輯頁面屬性";
$MESS["main_epilog_before_menu_prop_title"] = "編輯頁面標題和其他頁面屬性";
$MESS["main_epilog_before_menu_title"] = "編輯當前頁面";
$MESS["main_epilog_before_remove_panel"] = "隱藏這個酒吧";
$MESS["main_epilog_before_remove_panel_confirm"] = "您想隱藏頁面編輯欄嗎？您可以隨時將其帶回用戶界面設置表單。";
$MESS["main_epilog_before_remove_panel_title"] = "不要顯示頁面編輯欄";
