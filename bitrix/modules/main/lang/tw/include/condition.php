<?php
$MESS["MAIN_PATH"] = "文件或文件夾的路徑";
$MESS["MAIN_PERIOD_FROM"] = "開始日期";
$MESS["MAIN_PERIOD_NOTE"] = "如果沒有比期限設置的周期範圍是無限的。";
$MESS["MAIN_PERIOD_TO"] = "結束日期";
$MESS["MAIN_URL_FIELD"] = "範圍";
$MESS["MAIN_URL_VALUE"] = "價值";
$MESS["MAIN_USERGROUPS"] = "用戶組";
$MESS["TYPES_EMPTY"] = "[沒有條件]";
$MESS["TYPES_EMPTY_COND"] = "＆lt;沒有條件＆gt;";
$MESS["TYPES_FALSE"] = "[離開]";
$MESS["TYPES_FALSE_COND"] = "＆lt; hide＆gt;";
$MESS["TYPES_FOLDER"] = "用於文件或文件夾";
$MESS["TYPES_NO_ACCESS"] = "用戶訪問被拒絕";
$MESS["TYPES_PERIOD"] = "時間段";
$MESS["TYPES_PHP"] = "PHP表達";
$MESS["TYPES_UGROUPS"] = "用於用戶組";
$MESS["TYPES_URL"] = "URL參數";
