<?php
$MESS["CANVAS_CANCEL"] = "取消";
$MESS["CANVAS_CROP"] = "莊稼";
$MESS["CANVAS_DELETE"] = "刪除";
$MESS["CANVAS_FLIP_H"] = "水平翻轉";
$MESS["CANVAS_FLIP_V"] = "垂直翻轉";
$MESS["CANVAS_GRAYSCALE"] = "灰度";
$MESS["CANVAS_OK"] = "申請";
$MESS["CANVAS_POSTER_SIGN"] = "簽名：";
$MESS["CANVAS_SIGN"] = "符號";
$MESS["CANVAS_TURN_L"] = "逆時針旋轉";
$MESS["CANVAS_TURN_R"] = "順時針旋轉";
