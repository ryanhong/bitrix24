<?php
$MESS["BX_CLOCK_CLOSE"] = "關閉";
$MESS["BX_CLOCK_DOWN"] = "減少（向下）";
$MESS["BX_CLOCK_HOURS"] = "小時";
$MESS["BX_CLOCK_INSERT"] = "設置時間";
$MESS["BX_CLOCK_MINUTES"] = "分鐘";
$MESS["BX_CLOCK_TITLE"] = "設置時間";
$MESS["BX_CLOCK_UP"] = "增加（向上）";
