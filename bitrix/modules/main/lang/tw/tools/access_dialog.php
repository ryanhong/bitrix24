<?php
$MESS["acc_dialog_access_denied"] = "拒絕訪問。您需要獲得授權。";
$MESS["acc_dialog_not_found"] = "您的搜索未返回任何結果。";
$MESS["acc_dialog_sel"] = "選定";
$MESS["acc_dialog_wait"] = "請稍等...";
