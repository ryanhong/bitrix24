<?php
$MESS["HELP_ACCESS_DENIED"] = "拒絕訪問";
$MESS["HELP_BACK"] = "後退";
$MESS["HELP_CLOSE"] = "關閉";
$MESS["HELP_COLLAPSE_ALL"] = "全部收縮";
$MESS["HELP_CONFIRM_DELETE"] = "您確定要刪除文件和/或文件夾嗎？";
$MESS["HELP_CREATE"] = "創造";
$MESS["HELP_CREATE_FOLDER_ERROR"] = "錯誤創建文件夾";
$MESS["HELP_CURRENT_FOLDER"] = "當前文件夾：";
$MESS["HELP_DELETE"] = "刪除";
$MESS["HELP_DELETE_ERROR"] = "錯誤刪除文件";
$MESS["HELP_EDIT"] = "編輯";
$MESS["HELP_ERROR_DELETE_FOLDER"] = "錯誤刪除文件夾＃文件夾＃";
$MESS["HELP_EXPAND_ALL"] = "展開全部";
$MESS["HELP_FILENAME"] = "文件名：";
$MESS["HELP_FILE_EXISTS"] = "帶有指定名稱的文件或文件夾已經存在";
$MESS["HELP_FILE_NOT_EXIST"] = "文件＃文件＃不存在";
$MESS["HELP_FILE_NOT_FOUND"] = "找不到幫助文件";
$MESS["HELP_FILE_SAVE_ERROR"] = "錯誤保存文件";
$MESS["HELP_FOLDER_NOT_EMPTY"] = "錯誤刪除文件夾＃文件夾＃（不是空）";
$MESS["HELP_FOLDER_NOT_EXIST"] = "文件夾＃文件夾＃不存在";
$MESS["HELP_FORGOT_FILENAME"] = "請提供文件名（\“文件名\”）";
$MESS["HELP_HEADER"] = "標頭：";
$MESS["HELP_HIDE"] = "隱藏";
$MESS["HELP_IS_FOLDER"] = "創建文件夾：";
$MESS["HELP_KERNEL"] = "核心";
$MESS["HELP_MOVE_DOWN"] = "向下移動";
$MESS["HELP_MOVE_UP"] = "提升";
$MESS["HELP_RESET"] = "重置";
$MESS["HELP_SEARCH"] = "搜尋";
$MESS["HELP_SORT"] = "排序：";
$MESS["HELP_TITLE"] = "幫助";
$MESS["HELP_TREE"] = "與主題列表同步";
