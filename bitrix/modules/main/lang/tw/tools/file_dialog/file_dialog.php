<?php
$MESS["MAIN_BFSD_ALL_FILES"] = "全部文件";
$MESS["MAIN_BFSD_APPLY"] = "申請";
$MESS["MAIN_BFSD_BITRIX_PERMS_D"] = "否定";
$MESS["MAIN_BFSD_BITRIX_PERMS_R"] = "讀";
$MESS["MAIN_BFSD_BITRIX_PERMS_U"] = "工作流程";
$MESS["MAIN_BFSD_BITRIX_PERMS_W"] = "寫";
$MESS["MAIN_BFSD_BITRIX_PERMS_X"] = "完全訪問";
$MESS["MAIN_BFSD_CLOSE"] = "取消";
$MESS["MAIN_BFSD_DO_LOAD"] = "載入";
$MESS["MAIN_BFSD_DTIT_DATE"] = "日期更新";
$MESS["MAIN_BFSD_DTIT_NAME"] = "姓名";
$MESS["MAIN_BFSD_DTIT_PERM"] = "使用權";
$MESS["MAIN_BFSD_DTIT_SIZE"] = "尺寸";
$MESS["MAIN_BFSD_DTIT_TYPE"] = "類型";
$MESS["MAIN_BFSD_FILENAME"] = "文件名";
$MESS["MAIN_BFSD_FILETYPE"] = "文件類型";
$MESS["MAIN_BFSD_FLTR_DAT"] = "數據文件";
$MESS["MAIN_BFSD_FLTR_PIC"] = "圖片";
$MESS["MAIN_BFSD_FTYPE_ARC"] = "檔案";
$MESS["MAIN_BFSD_FTYPE_CSS"] = "CSS樣式";
$MESS["MAIN_BFSD_FTYPE_CSV"] = "CSV文件";
$MESS["MAIN_BFSD_FTYPE_FOLDER"] = "文件夾";
$MESS["MAIN_BFSD_FTYPE_GIF"] = "GIF圖像";
$MESS["MAIN_BFSD_FTYPE_HTML"] = "HTML文檔";
$MESS["MAIN_BFSD_FTYPE_JPG"] = "JPEG圖像";
$MESS["MAIN_BFSD_FTYPE_NA"] = "文件";
$MESS["MAIN_BFSD_FTYPE_PHP"] = "PHP腳本";
$MESS["MAIN_BFSD_FTYPE_PNG"] = "PNG圖像";
$MESS["MAIN_BFSD_FTYPE_SWF"] = "閃存文件";
$MESS["MAIN_BFSD_FTYPE_SYS"] = "系統";
$MESS["MAIN_BFSD_FTYPE_TXT"] = "文本文檔";
$MESS["MAIN_BFSD_FTYPE_XML"] = "XML文檔";
$MESS["MAIN_BFSD_LOAD_DENY_ALERT"] = "禁止寫對文件的訪問。";
$MESS["MAIN_BFSD_LOAD_ENTERNAME_ALERT"] = "請輸入文件名。";
$MESS["MAIN_BFSD_LOAD_EXIST_ALERT"] = "帶有此名稱的文件已經存在！如果您需要覆蓋現有文件，請設置\“ Overprite \”選項。";
$MESS["MAIN_BFSD_LOAD_FILE"] = "加載文件";
$MESS["MAIN_BFSD_NO_PERMS"] = "您沒有足夠的權限使用打開的文件對話框";
$MESS["MAIN_BFSD_OPEN"] = "打開";
$MESS["MAIN_BFSD_PRT_OPEN"] = "打開";
$MESS["MAIN_BFSD_REWRITE"] = "覆蓋";
$MESS["MAIN_BFSD_SITE"] = "地點";
$MESS["MAIN_BFSD_TITLE"] = "打開文件";
$MESS["MAIN_BFSD_URL"] = "URL";
$MESS["MAIN_BFSD_VIEW"] = "看法";
$MESS["MAIN_BFSD_VIEW_DET"] = "細節";
$MESS["MAIN_BFSD_VIEW_LIST"] = "列表";
$MESS["MAIN_BFSD_VIEW_PREW"] = "縮略圖";
$MESS["file_dialog_warn"] = "請指定文件名。";
