<?php
$MESS["MAIN_FD_ADD_2_MENU"] = "添加到菜單：";
$MESS["MAIN_FD_ADD_2_MENU_ADD_NEW"] = "添加新項目";
$MESS["MAIN_FD_ADD_2_MENU_ITEM"] = "菜單項：";
$MESS["MAIN_FD_ADD_2_MENU_NA"] = "（沒有數據）";
$MESS["MAIN_FD_ADD_2_MENU_NEW_NAME"] = "新項目的名稱：";
$MESS["MAIN_FD_ADD_2_MENU_NOT"] = "（不要添加）";
$MESS["MAIN_FD_ADD_2_MENU_REUSE"] = "鏈接到現有項目";
$MESS["MAIN_FD_ADD_2_MENU_REUSE_ITEM"] = "鏈接到項目：";
$MESS["MAIN_FD_ADD_2_MENU_REUSE_NAME"] = "在項目之前添加：";
$MESS["MAIN_FD_LAST_MENU_ITEM"] = "（最後一個菜單項）";
$MESS["MAIN_FD_SAVE"] = "節省";
$MESS["MAIN_FD_TITLE"] = "頁面標題：";
