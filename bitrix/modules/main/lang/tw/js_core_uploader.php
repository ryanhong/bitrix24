<?php
$MESS["FILE_BAD_SIZE"] = "超出上傳文件的最大允許大小";
$MESS["FILE_SIZE_Gb"] = "GB";
$MESS["FILE_SIZE_Kb"] = "KB";
$MESS["FILE_SIZE_Mb"] = "MB";
$MESS["FILE_SIZE_Tb"] = "TB";
$MESS["FILE_SIZE_b"] = "b";
$MESS["UPLOADER_ACTION_URL_NOT_DEFINED"] = "未指定上傳路徑。";
$MESS["UPLOADER_INPUT_IS_NOT_DEFINED"] = "未指定輸入字段。";
$MESS["UPLOADER_UPLOADING_ERROR"] = "上傳錯誤。";
$MESS["UPLOADER_UPLOADING_ERROR1"] = "不正確的服務器響應。";
$MESS["UPLOADER_UPLOADING_ERROR2"] = "無法上傳文件，因為有太多的文件以指定的表單上傳。";
$MESS["UPLOADER_UPLOADING_ERROR3"] = "該文件不能在服務器上合併。";
$MESS["UPLOADER_UPLOADING_ERROR4"] = "文件大小超過配額。";
$MESS["UPLOADER_UPLOADING_ERROR5"] = "由於未收到服務器響應，因此無法上傳該文件。";
$MESS["UPLOADER_UPLOADING_ERROR6"] = "服務器磁盤空間不足。";
$MESS["UPLOADER_UPLOADING_ONBEFOREUNLOAD"] = "文件仍在上傳。您想離開此頁面嗎？";
