<?php
$MESS["MAIN_SIDEPANEL_CLOSE"] = "關閉";
$MESS["MAIN_SIDEPANEL_COPY_LINK"] = "複製鏈接";
$MESS["MAIN_SIDEPANEL_MINIMIZE"] = "最小化";
$MESS["MAIN_SIDEPANEL_NEW_WINDOW"] = "在新窗口中打開";
$MESS["MAIN_SIDEPANEL_PRINT"] = "列印";
$MESS["MAIN_SIDEPANEL_REMOVE_ALL"] = "空頁面欄";
