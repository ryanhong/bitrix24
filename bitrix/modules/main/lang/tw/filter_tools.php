<?php
$MESS["AND"] = "和";
$MESS["ASC_ORDER"] = "上升";
$MESS["DESC_ORDER"] = "下降";
$MESS["FILTER_ERROR_LOGIC"] = "過濾器和數據庫設置的組合不允許\ \“或\”邏輯與過濾器字段（自動替換為\“ and \”）";
$MESS["FILTER_LOGIC"] = "過濾場之間邏輯：";
$MESS["FILTER_LOGIC_HELP"] = "幫助過濾器查詢語言";
$MESS["MAIN_ADD_TO_FAVORITES"] = "添加到收藏夾";
$MESS["MAIN_EXACT_MATCH"] = "使用精確匹配";
$MESS["OR"] = "或者";
$MESS["admin_filter_filter_not_set"] = "過濾器不活躍";
$MESS["admin_filter_filter_set"] = "過濾器活動";
$MESS["admin_filter_hide"] = "隱藏過濾器";
$MESS["admin_filter_hide2"] = "隱藏";
$MESS["admin_filter_show"] = "顯示過濾器";
$MESS["admin_filter_show2"] = "展示";
