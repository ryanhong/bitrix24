<?php
$MESS["JSADM_AI_ALL_NOTIF"] = "所有通知";
$MESS["JSADM_AI_HIDE_EXTRA"] = "隱藏額外的物品";
$MESS["JSADM_AUTH_REQ"] = "需要身份驗證！";
$MESS["JS_CORE_H"] = "H";
$MESS["JS_CORE_IMAGE_FULL"] = "全尺寸";
$MESS["JS_CORE_LOADING"] = "載入中...";
$MESS["JS_CORE_M"] = "m";
$MESS["JS_CORE_NO_DATA"] = "- 沒有數據 -";
$MESS["JS_CORE_S"] = "s";
$MESS["JS_CORE_WINDOW_AUTH"] = "登入";
$MESS["JS_CORE_WINDOW_CANCEL"] = "取消";
$MESS["JS_CORE_WINDOW_CLOSE"] = "關閉";
$MESS["JS_CORE_WINDOW_CONTINUE"] = "繼續";
$MESS["JS_CORE_WINDOW_EXPAND"] = "擴張";
$MESS["JS_CORE_WINDOW_NARROW"] = "恢復";
$MESS["JS_CORE_WINDOW_SAVE"] = "節省";
