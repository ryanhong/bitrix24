<?php
$MESS["BAD_LANG_DUP"] = "站點ID重複。";
$MESS["BAD_LANG_LID"] = "站點ID必須為2個字符的長度。";
$MESS["BAD_LANG_NAME"] = "請提供網站的名稱。";
$MESS["BAD_LANG_SORT"] = "請選擇排序。";
$MESS["lang_check_culture_incorrect"] = "區域選擇不正確。";
$MESS["lang_check_culture_not_set"] = "沒有提供區域選擇。";
