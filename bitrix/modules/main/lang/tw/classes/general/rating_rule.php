<?php
$MESS["RR_GENERAL_ERR_ACTION_NAME"] = "動作錯誤。";
$MESS["RR_GENERAL_ERR_ACTIVATE"] = "錯誤指定“應用程序”選項。";
$MESS["RR_GENERAL_ERR_ACTIVE"] = "錯誤指定“ Active”的錯誤選項。";
$MESS["RR_GENERAL_ERR_CONDITION_NAME"] = "條件錯誤。";
$MESS["RR_GENERAL_ERR_DEACTIVATE"] = "錯誤指定“取消訴訟”的錯誤選項。";
$MESS["RR_GENERAL_ERR_ENTITY_TYPE_ID"] = "指定的對像不存在。";
$MESS["RR_GENERAL_ERR_NAME"] = "“名稱”字段不能為空。";
