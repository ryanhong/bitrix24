<?php
$MESS["MAIN_YES"] = "是的";
$MESS["USER_TYPE_BOOL_CHECKBOX"] = "複選框";
$MESS["USER_TYPE_BOOL_DEFAULT_VALUE"] = "預設值";
$MESS["USER_TYPE_BOOL_DESCRIPTION"] = "真假";
$MESS["USER_TYPE_BOOL_DISPLAY"] = "控制";
$MESS["USER_TYPE_BOOL_DROPDOWN"] = "下拉列表";
$MESS["USER_TYPE_BOOL_LABELS"] = "價值標籤";
$MESS["USER_TYPE_BOOL_LABEL_CHECKBOX"] = "複選框標籤";
$MESS["USER_TYPE_BOOL_RADIO"] = "單選按鈕";
