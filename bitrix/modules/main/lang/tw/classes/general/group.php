<?php
$MESS["BAD_GROUP_NAME"] = "請輸入組名稱。";
$MESS["MAIN_ERROR_STRING_ID"] = "具有此類字符串ID的組已經存在";
$MESS["WRONG_USER_DATE_ACTIVE_FROM"] = "用戶＃＃user_id＃的活動期間的最早日期不正確";
$MESS["WRONG_USER_DATE_ACTIVE_TO"] = "用戶＃＃user_id＃的活動期間的最新日期不正確";
