<?php
$MESS["USER_TYPE_DT_DEFAULT_VALUE"] = "預設值";
$MESS["USER_TYPE_DT_DESCRIPTION"] = "約會時間";
$MESS["USER_TYPE_DT_ERROR"] = "\“＃field_name＃\”的值不是有效的日期或時間。";
$MESS["USER_TYPE_DT_NONE"] = "不";
$MESS["USER_TYPE_DT_NOW"] = "當前時間";
$MESS["USER_TYPE_DT_USE_SECOND"] = "使用秒";
