<?php
$MESS["CSV_IMPORT_DELIMETER_NOT_FOUND"] = "缺少CSV標頭，或指定了不正確的場分離器。";
$MESS["CSV_IMPORT_HEADER_NOT_FOUND"] = "文件不存在，或者缺少CSV標頭。";
$MESS["CSV_IMPORT_LAST_NAME_NOT_FOUND"] = "CSV文件標頭缺少last_name字段（用戶姓氏）。";
$MESS["CSV_IMPORT_MUST_BE_UTF"] = "CSV文件應具有UTF-8編碼。";
$MESS["CSV_IMPORT_NAME_NOT_FOUND"] = "CSV文件標頭缺少名稱字段（用戶名）。";
$MESS["CSV_IMPORT_NO_LASTNAME"] = "用戶姓氏未指定。";
$MESS["CSV_IMPORT_NO_NAME"] = "未指定用戶名。";
