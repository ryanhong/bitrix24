<?php
$MESS["MAIN_AI_ALL_NOTIF"] = "顯示所有";
$MESS["MAIN_AI_NEW_NOTIF"] = "新通知";
$MESS["top_panel_ai_composite_desc"] = "獨特的網站建築技術可以提高網頁加載速度。";
$MESS["top_panel_ai_composite_switch_on"] = "啟用自動複合模式";
$MESS["top_panel_ai_composite_title"] = "自動複雜材料站點";
$MESS["top_panel_ai_in_all"] = "全部的：";
$MESS["top_panel_ai_in_aviable"] = "可用的：";
$MESS["top_panel_ai_in_no"] = "沒有任何";
$MESS["top_panel_ai_in_recomend"] = "推薦：";
$MESS["top_panel_ai_marketplace"] = "利率市場解決方案";
$MESS["top_panel_ai_marketplace_add"] = "留下反饋";
$MESS["top_panel_ai_marketplace_descr"] = "您正在使用<b> #name＃< /b>（＃id＃）。<br />請分享您的意見。<br />
<br />您想看到什麼改進？<br />您的意見很重要！";
$MESS["top_panel_ai_marketplace_hide"] = "隱藏";
$MESS["top_panel_ai_sys_ver"] = "當前版本";
$MESS["top_panel_ai_title_err"] = "錯誤";
$MESS["top_panel_ai_upd_aviable"] = "更新<br>可用：";
$MESS["top_panel_ai_upd_chk"] = "檢查更新";
$MESS["top_panel_ai_upd_installed"] = "更新已安裝。";
$MESS["top_panel_ai_upd_instl"] = "安裝更新";
$MESS["top_panel_ai_upd_last"] = "上次更新：";
$MESS["top_panel_ai_upd_never"] = "沒有安裝更新。";
$MESS["top_panel_ai_upd_stp"] = "配置更新";
$MESS["top_panel_ai_updates"] = "更新";
$MESS["top_panel_ai_used_space"] = "使用的空間";
