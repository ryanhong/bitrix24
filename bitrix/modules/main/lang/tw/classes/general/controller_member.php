<?php
$MESS["MAIN_CMEMBER_AUTH_TYPE"] = "控制器授權";
$MESS["MAIN_CMEMBER_ERR1"] = "控制器授權錯誤";
$MESS["MAIN_CMEMBER_ERR2"] = "錯誤將消息發送給控制器";
$MESS["MAIN_CMEMBER_ERR3"] = "連接到控制器的錯誤";
$MESS["MAIN_CMEMBER_ERR4"] = "錯誤與控制器斷開連接";
$MESS["MAIN_CMEMBER_ERR5"] = "連接到服務器的錯誤";
$MESS["MAIN_CMEMBER_ERR6"] = "控制器返回無效結果";
$MESS["MAIN_CMEMBER_ERR7"] = "控制器的答案無效：";
