<?php
$MESS["MN_SU_DELETE_ERROR"] = "錯誤刪除記錄。";
$MESS["MN_SU_HTTP_STATUS_301"] = "301（永久移動）";
$MESS["MN_SU_HTTP_STATUS_302"] = "302（發現）";
$MESS["MN_SU_NO_ID"] = "未指定URL ID。";
$MESS["MN_SU_NO_SHORT_URI"] = "未指定備用URL。";
$MESS["MN_SU_NO_STATUS"] = "未指定HTTP狀態代碼。";
$MESS["MN_SU_NO_URI"] = "目的地URL未指定。";
$MESS["MN_SU_WRONG_SHORT_URI"] = "目的地URL不正確。";
$MESS["MN_SU_WRONG_STATUS"] = "不支持指定的HTTP狀態代碼。";
