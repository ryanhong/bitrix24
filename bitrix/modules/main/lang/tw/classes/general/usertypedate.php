<?php
$MESS["USER_TYPE_D_DEFAULT_VALUE"] = "預設值";
$MESS["USER_TYPE_D_DESCRIPTION"] = "日期";
$MESS["USER_TYPE_D_ERROR"] = "\“＃field_name＃\”的值不是有效的日期。";
$MESS["USER_TYPE_D_NONE"] = "不";
$MESS["USER_TYPE_D_NOW"] = "當前日期";
