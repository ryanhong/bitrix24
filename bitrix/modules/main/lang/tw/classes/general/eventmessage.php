<?php
$MESS["ACTION_EMPTY"] = "沒有指定動作";
$MESS["BAD_EMAIL_FROM"] = "請輸入發件人的電子郵件。";
$MESS["BAD_EMAIL_TO"] = "請輸入收件人的電子郵件。";
$MESS["BAD_EVENT_TYPE"] = "事件類型不正確。";
$MESS["EVENT_ID_EMPTY"] = "事件未指定";
$MESS["EVENT_NAME_EMPTY"] = "事件類型名稱未指定";
$MESS["EVENT_NAME_EXIST"] = "事件\“＃event_name＃\”已經存在＃site_id＃語言";
$MESS["EVENT_TYPE_EMPTY"] = "未指定事件類型";
$MESS["LID_EMPTY"] = "事件類型語言未指定";
$MESS["MAIN_BAD_EVENT_NAME_NA"] = "請提供一種類型。";
$MESS["MAIN_BAD_SITE_NA"] = "請建立一個網站協會。";
$MESS["MAIN_EVENT_BAD_SITE"] = "糟糕的網站！";
