<?php
$MESS["authprov_all"] = "所有訪客";
$MESS["authprov_all_desc"] = "包括未經授權的訪問者在內的所有用戶";
$MESS["authprov_all_groups"] = "所有小組";
$MESS["authprov_author"] = "由...製作";
$MESS["authprov_author_desc"] = "內容作者";
$MESS["authprov_authorized"] = "所有授權用戶";
$MESS["authprov_authorized_desc"] = "所有授權用戶";
$MESS["authprov_group_name"] = "輸入組名稱。";
$MESS["authprov_group_prov"] = "團體";
$MESS["authprov_last"] = "最後的";
$MESS["authprov_other"] = "其他角色";
$MESS["authprov_search"] = "搜尋";
$MESS["authprov_user"] = "輸入用戶登錄或名字或姓氏。";
$MESS["authprov_user1"] = "用戶";
$MESS["authprov_user_curr"] = "當前用戶";
