<?php
$MESS["MAIN_EDIT"] = "調整";
$MESS["USER_TYPE_EDIT_TAB"] = "自定義字段";
$MESS["USER_TYPE_EDIT_TAB_HREF"] = "定制";
$MESS["USER_TYPE_EDIT_TAB_TITLE"] = "自定義字段";
$MESS["USER_TYPE_FIELD_VALUE_IS_MISSING"] = "所需的字段＃field_name＃缺少。";
$MESS["USER_TYPE_FIELD_VALUE_IS_MULTIPLE"] = "字段\“＃field_name＃\”不能是多個字段。";
$MESS["USER_TYPE_PROP_ADD"] = "添加";
