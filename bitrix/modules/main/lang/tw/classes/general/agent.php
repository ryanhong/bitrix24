<?php
$MESS["MAIN_AGENT_ERROR_DATE_CHECK"] = "指定了不正確的檢查日期！";
$MESS["MAIN_AGENT_ERROR_EXIST"] = "該用戶已經使用具有相同功能的代理！";
$MESS["MAIN_AGENT_ERROR_EXIST_EXT"] = "具有功能＃代理＃的代理已經存在！";
$MESS["MAIN_AGENT_ERROR_EXIST_FOR_USER"] = "具有函數＃代理＃用戶## user_id＃已經存在的代理！";
$MESS["MAIN_AGENT_ERROR_LAST_EXEC"] = "指定了最後一個代理運行的不正確日期！";
$MESS["MAIN_AGENT_ERROR_NAME"] = "指定不正確的代理功能！";
$MESS["MAIN_AGENT_ERROR_NEXT_EXEC"] = "指定了下一個代理運行的不正確日期！";
