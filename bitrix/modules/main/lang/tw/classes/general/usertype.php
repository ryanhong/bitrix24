<?php
$MESS["USER_TYPE_ADD_ALREADY_ERROR"] = "forn＃field_name＃for entity＃entity_id＃已經存在。";
$MESS["USER_TYPE_ADD_ERROR"] = "嘗試將＃field_name＃＃添加到ENTITY＃ENTITY_ID＃時發生錯誤。";
$MESS["USER_TYPE_DELETE_ERROR"] = "錯誤刪除＃field_name＃for＃entity_id＃object。";
$MESS["USER_TYPE_ENTITY_ID_INVALID"] = "實體標識符包含無效的符號。有效符號為：A-Z，0-9和_。";
$MESS["USER_TYPE_ENTITY_ID_MISSING"] = "實體未定義。";
$MESS["USER_TYPE_ENTITY_ID_TOO_LONG1"] = "實體標識符太長（超過50個符號）。";
$MESS["USER_TYPE_FIELD_NAME_INVALID"] = "字段名稱包含無效的符號。有效符號為：A-Z，0-9和_。";
$MESS["USER_TYPE_FIELD_NAME_MISSING"] = "字段名稱未定義。";
$MESS["USER_TYPE_FIELD_NAME_NOT_UF"] = "字段名稱前綴不是UF_";
$MESS["USER_TYPE_FIELD_NAME_TOO_LONG1"] = "字段名稱太長（超過50個符號）。";
$MESS["USER_TYPE_FIELD_NAME_TOO_SHORT"] = "字段名稱太短（少於4個符號）。";
$MESS["USER_TYPE_TABLE_CREATION_ERROR"] = "為＃entity_id＃創建實體屬性表時發生了錯誤。";
$MESS["USER_TYPE_UPDATE_ERROR"] = "錯誤編輯＃field_name＃for＃entity_id＃object。";
$MESS["USER_TYPE_USER_TYPE_ID_INVALID"] = "自定義類型無效。";
$MESS["USER_TYPE_USER_TYPE_ID_MISSING"] = "自定義類型缺少。";
