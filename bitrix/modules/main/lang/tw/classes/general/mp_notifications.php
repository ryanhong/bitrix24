<?php
$MESS["TOP_PANEL_AI_MODULE_END_UPDATE"] = "市場解決方案更新訂閱即將到期";
$MESS["TOP_PANEL_AI_MODULE_END_UPDATE_DESC"] = "<b> #name＃</b>的更新訂閱即將到期。";
$MESS["TOP_PANEL_AI_MODULE_UPDATE"] = "市場解決方案更新了";
$MESS["TOP_PANEL_AI_MODULE_UPDATE_BUTTON_HIDE"] = "隱藏";
$MESS["TOP_PANEL_AI_MODULE_UPDATE_BUTTON_VIEW"] = "市場解決方案";
$MESS["TOP_PANEL_AI_MODULE_UPDATE_DESC"] = "<b> #name＃</b>的更新準備安裝。";
$MESS["TOP_PANEL_AI_NEW_MODULE_DESC"] = "剛剛發布的新解決方案<b> #partner＃</b>：<br>";
$MESS["TOP_PANEL_AI_NEW_MODULE_TITLE"] = "可用的新解決方案";
