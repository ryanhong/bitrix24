<?php
$MESS["USER_TYPE_ENUM_CAPTION_NO_VALUE"] = "當價值為空時使用字幕";
$MESS["USER_TYPE_ENUM_CHECKBOX"] = "複選框";
$MESS["USER_TYPE_ENUM_DESCRIPTION"] = "列表";
$MESS["USER_TYPE_ENUM_DIALOG"] = "實體選擇對話框";
$MESS["USER_TYPE_ENUM_DISPLAY"] = "控制";
$MESS["USER_TYPE_ENUM_LIST"] = "列表";
$MESS["USER_TYPE_ENUM_LIST_HEIGHT"] = "列表控制高度";
$MESS["USER_TYPE_ENUM_NO_VALUE"] = "未選中的";
$MESS["USER_TYPE_ENUM_SHOW_NO_VALUE"] = "在必需字段中顯示空值";
$MESS["USER_TYPE_ENUM_UI"] = "多選擇列表";
