<?php
$MESS["RATING_ALLOW_VOTE_ACCESS"] = "您無權投票。";
$MESS["RATING_ALLOW_VOTE_COUNT_VOTE"] = "您已經超出了每日投票配額。";
$MESS["RATING_ALLOW_VOTE_GUEST"] = "僅允許授權用戶投票。";
$MESS["RATING_ALLOW_VOTE_LOW_WEIGHT"] = "您的權威不足以投票。";
$MESS["RATING_ALLOW_VOTE_SELF"] = "你不能為自己投票。";
$MESS["RATING_GENERAL_ERR_ACTIVE"] = "錯誤指定“ Active”的錯誤選項。";
$MESS["RATING_GENERAL_ERR_CAL_METHOD"] = "指定的方法不存在。";
$MESS["RATING_GENERAL_ERR_CUR_NAME"] = "當前價值的自定義字段“自定義字段”需要字段。";
$MESS["RATING_GENERAL_ERR_ENTITY_ID"] = "指定的對像不存在。";
$MESS["RATING_GENERAL_ERR_NAME"] = "“名稱”字段不能為空。";
$MESS["RATING_GENERAL_ERR_PREV_NAME"] = "先前價值的自定義字段“自定義字段”需要字段。";
$MESS["RATING_NO_POSITION"] = "不";
