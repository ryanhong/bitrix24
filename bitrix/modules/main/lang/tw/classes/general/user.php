<?php
$MESS["ACCOUNT_INFO_SENT"] = "用於重置密碼和您的註冊信息的代碼剛剛發送到您的電子郵件地址。請查看你的郵箱。請注意，重置代碼將在每個請求上重新生成。";
$MESS["CHANGE_PASS_SUCC"] = "您的密碼成功更改了。";
$MESS["CHECKWORD_EXPIRE"] = "檢查單詞有效期已過期。您需要再次要求。";
$MESS["CHECKWORD_INCORRECT1"] = "不正確的檢查字。";
$MESS["DATA_NOT_FOUND1"] = "找不到用戶配置文件。";
$MESS["FORGOT_NAME"] = "所需的名稱和姓氏";
$MESS["FORMATNAME_NONAME"] = "無標題";
$MESS["INFO_REQ"] = "您已要求您的註冊信息。";
$MESS["LIMIT_USERS_COUNT"] = "注意力！用戶數量超過許可配額。您必須獲得更多許可。請聯繫技術支持部門。";
$MESS["LOGIN_BLOCK"] = "您的登錄被阻止";
$MESS["LOGIN_NOT_FOUND1"] = "找不到用戶";
$MESS["LOGIN_WHITESPACE"] = "登錄無法啟動和結束一個空間。";
$MESS["MAIN_FUNCTION_REGISTER_CAPTCHA"] = "錯誤輸入了防止自動註冊的詞。";
$MESS["MAIN_FUNCTION_REGISTER_NA_INADMIN"] = "您不能使用函數cuser :: indravion_section註冊！";
$MESS["MAIN_FUNCTION_REGISTER_PASSWORD_DIGITS"] = "密碼必須包含數字（0-9）。";
$MESS["MAIN_FUNCTION_REGISTER_PASSWORD_LENGTH"] = "密碼必須至少包含＃長度＃字符。";
$MESS["MAIN_FUNCTION_REGISTER_PASSWORD_LOWERCASE"] = "密碼必須包含拉丁小寫字母（A-Z）。";
$MESS["MAIN_FUNCTION_REGISTER_PASSWORD_PUNCTUATION"] = "密碼必須包含標點符號（＃special_chars＃）。";
$MESS["MAIN_FUNCTION_REGISTER_PASSWORD_UNIQUE"] = "您的密碼不得與最近使用的密碼相同。";
$MESS["MAIN_FUNCTION_REGISTER_PASSWORD_UPPERCASE"] = "密碼必須包含拉丁大寫字母（A-Z）。";
$MESS["MAIN_FUNCTION_SIMPLEREGISTER_NA_INADMIN"] = "您無法在Admin_section中使用函數cuser :: SimpleRegister！";
$MESS["MAIN_GP_PASSWORD_DIGITS"] = "包含數字（0-9）";
$MESS["MAIN_GP_PASSWORD_LENGTH"] = "密碼必須至少包含＃長度＃字符";
$MESS["MAIN_GP_PASSWORD_LOWERCASE"] = "包含拉丁小寫字母（A-Z）";
$MESS["MAIN_GP_PASSWORD_PUNCTUATION"] = "包含標點符號（＃special_chars＃）";
$MESS["MAIN_GP_PASSWORD_UPPERCASE"] = "包含拉丁大寫字母（A-Z）";
$MESS["MAIN_LOGIN_CHANGE_PASSWORD"] = "您的密碼已過期，您必須更改它。";
$MESS["MAIN_LOGIN_EMAIL_CONFIRM"] = "您尚未確認您的註冊（確認消息已發送到＃電子郵件＃）。如果需要，請使用密碼檢索表格再次發送確認消息。";
$MESS["MAIN_SEND_PASS_CONFIRM"] = "您尚未確認您的註冊。另一條確認消息已發送到您的電子郵件地址。";
$MESS["MIN_LOGIN"] = "登錄必須包含3個或更多字符";
$MESS["MIN_PASSWORD1"] = "密碼必須包含6個或更多字符";
$MESS["PASSWORD_CHANGE_OK"] = "密碼成功更改了。<br>新的註冊信息剛剛發送到您的電子郵件地址。";
$MESS["PROFILE_ACCESS_DENIED"] = "您沒有訪問權利來編輯個人資料";
$MESS["REGISTRATION_OK"] = "註冊成功。<br>註冊信息剛剛發送到您的電子郵件地址。";
$MESS["STATUS_ACTIVE"] = "積極的";
$MESS["STATUS_BLOCKED"] = "阻止";
$MESS["TASK_NAME_FM_FOLDER_ACCESS_READ"] = "讀";
$MESS["TASK_NAME_MAIN_DENIED"] = "拒絕訪問";
$MESS["USER_AUTH_DIGEST_ERR"] = "HTTP Digest授權錯誤。";
$MESS["USER_EXIST"] = "登錄\“＃登錄＃\”的用戶已經存在";
$MESS["USER_LAST_SEEN_MORE_YEAR"] = "一年多以前";
$MESS["USER_LAST_SEEN_NOW"] = "現在";
$MESS["USER_LAST_SEEN_TODAY"] = "今天在＃時間＃";
$MESS["USER_LAST_SEEN_TOMORROW"] = "明天在＃時間＃";
$MESS["USER_LAST_SEEN_YESTERDAY"] = "昨天在＃時間＃";
$MESS["USER_LOGIN_OTP_ERROR"] = "一次性密碼被禁用。";
$MESS["USER_LOGIN_OTP_INCORRECT"] = "一次性密碼不正確。";
$MESS["USER_REGISTERED_SIMPLE"] = "您的註冊已成功完成。";
$MESS["USER_REGISTER_OK"] = "您已經成功註冊了。";
$MESS["USER_STATUS_OFFLINE"] = "離線";
$MESS["USER_STATUS_ONLINE"] = "在線的";
$MESS["USER_WITH_EMAIL_EXIST"] = "具有此電子郵件地址（＃電子郵件＃）的用戶已經存在。";
$MESS["WRONG_CONFIRMATION"] = "密碼確認不正確";
$MESS["WRONG_DATE_ACTIVE_FROM"] = "組＃＃group_id＃的活動期間的最早日期不正確";
$MESS["WRONG_DATE_ACTIVE_TO"] = "組＃＃group_id＃的活動期的最新日期不正確";
$MESS["WRONG_EMAIL"] = "不正確的電子郵件";
$MESS["WRONG_LOGIN"] = "寫錯了登錄名或密碼";
$MESS["WRONG_PERSONAL_BIRTHDAY"] = "出生日期不正確。";
$MESS["main_change_pass_changed"] = "密碼已成功更改。";
$MESS["main_change_pass_code_error"] = "錯誤或過期的確認代碼。您可以請求另一個代碼。";
$MESS["main_change_pass_empty_checkword"] = "未指定驗證代碼或當前密碼。";
$MESS["main_change_pass_error"] = "錯誤更改密碼。";
$MESS["main_change_pass_incorrect_pass"] = "您提供的當前密碼不正確。";
$MESS["main_check_password_weak"] = "密碼太弱（在弱密碼列表中找到）。";
$MESS["main_login_need_phone_confirmation"] = "您尚未確認您的註冊（您的電話號碼：＃電話＃）。請使用密碼恢復表格請求另一個確認SMS。";
$MESS["main_register_no_user"] = "找不到用戶。";
$MESS["main_register_sms_sent"] = "更改密碼的代碼已發送到您的手機。";
$MESS["main_register_timeout"] = "超時尚未到期。";
$MESS["main_send_password_email_code"] = "授權代碼已發送到您用於註冊的電子郵件地址。您可以在一分鐘內發送另一個代碼。";
$MESS["main_user_captcha_error"] = "驗證碼代碼不正確。";
$MESS["main_user_check_no_phone"] = "註冊電話號碼未指定。";
$MESS["main_user_pass_request_sent"] = "確認代碼已發送到指定的電話號碼。";
$MESS["user_email_not_set"] = "未指定用戶電子郵件。";
$MESS["user_login_not_set"] = "未指定用戶登錄。";
$MESS["user_pass_not_set"] = "未指定用戶密碼。";
