<?php
$MESS["fav_general_err_lang"] = "指定的接口語言不存在。";
$MESS["fav_general_err_lang1"] = "請指定接口語言。";
$MESS["fav_general_err_name"] = "“名稱”字段不能為空。";
$MESS["fav_general_err_url"] = "“鏈接”字段不能為空。";
$MESS["fav_general_err_user"] = "指定的用戶不存在。";
$MESS["fav_general_err_user1"] = "請指定擁有書籤的用戶。";
$MESS["fav_main_menu_add_dd"] = "另一種方法是<br>將菜單項拖放到右側的目標區域。";
$MESS["fav_main_menu_add_icon"] = "將頁面添加到收藏夾<br>通過單擊星號<br>在章節標題旁邊。";
$MESS["fav_main_menu_alt"] = "最喜歡的鏈接";
$MESS["fav_main_menu_close_hint"] = "關閉暗示";
$MESS["fav_main_menu_header"] = "最愛";
$MESS["fav_main_menu_nothing"] = "您的最愛列表<br>當前是空的。";
$MESS["fav_main_menu_open_hint"] = "打開提示";
