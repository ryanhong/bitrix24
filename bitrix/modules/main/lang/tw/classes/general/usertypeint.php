<?php
$MESS["USER_TYPE_INTEGER_DEFAULT_VALUE"] = "預設值";
$MESS["USER_TYPE_INTEGER_DESCRIPTION"] = "整數";
$MESS["USER_TYPE_INTEGER_MAX_VALUE"] = "最大值（0-無驗證）";
$MESS["USER_TYPE_INTEGER_MAX_VALUE_ERROR"] = "\“＃field_name＃\”的值不得超過＃max_value＃。";
$MESS["USER_TYPE_INTEGER_MIN_VALUE"] = "最小值（0-無驗證）";
$MESS["USER_TYPE_INTEGER_MIN_VALUE_ERROR"] = "\“＃field_name＃\”的值不得小於＃min_value＃。";
$MESS["USER_TYPE_INTEGER_SIZE"] = "輸入字段大小";
