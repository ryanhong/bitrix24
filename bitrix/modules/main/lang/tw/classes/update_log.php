<?php
$MESS["HINT_WIND_EXEC"] = "更新說明";
$MESS["HINT_WIND_EXEC_ALT"] = "更新說明";
$MESS["HINT_WIND_TITLE"] = "更新說明";
$MESS["SUP_HIST_DATE"] = "日期";
$MESS["SUP_HIST_DESCR"] = "描述";
$MESS["SUP_HIST_EMPTY_LOG"] = "更新歷史記錄為空";
$MESS["SUP_HIST_ERROR"] = "錯誤";
$MESS["SUP_HIST_NOTES"] = "筆記";
$MESS["SUP_HIST_PNOTES1"] = "筆記。";
$MESS["SUP_HIST_PNOTES2"] = "此頁面顯示最後20個已安裝的更新。";
$MESS["SUP_HIST_PROMT"] = "該表顯示已安裝的更新。";
$MESS["SUP_HIST_STATUS"] = "地位";
$MESS["SUP_HIST_SUCCESS"] = "成功";
$MESS["update_log_index"] = "首頁";
$MESS["update_log_index_title"] = "轉到SiteUpdate系統的開始頁面";
$MESS["update_log_nav"] = "記錄";
$MESS["update_log_title"] = "查看更新歷史記錄";
