<?php
$MESS["AUTOSAVE"] = "更改的表單字段沒有保存。";
$MESS["AUTOSAVE_L"] = "上一次自動保存：＃日期＃";
$MESS["AUTOSAVE_R"] = "還原為自動保存值？";
$MESS["AUTOSAVE_T"] = "自動保存";
