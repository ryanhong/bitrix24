<?php
$MESS["MAIN_USER_CONSENT_INTL_DESCRIPTION"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_DESCRIPTION_BY"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_DESCRIPTION_KZ"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_FIELDS_HINT"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_COMPANY_ADDRESS"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_COMPANY_ADDRESS_HINT"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_COMPANY_NAME"] = "公司名稱";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_COMPANY_NAME_HINT"] = "示例：Smith LLC";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_EMAIL"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_PURPOSES"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_PURPOSES_DEFAULT"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_PURPOSES_DEFAULT_BY"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_PURPOSES_HINT"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_THIRD_PARTIES"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_THIRD_PARTIES_HINT"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_THIRD_PARTIES_HINT_BY"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_THIRD_PARTIES_HINT_KZ"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_HINT_FIELD_DEFAULT"] = "默認文本將包含：";
$MESS["MAIN_USER_CONSENT_INTL_LABEL"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_LABEL_BY"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_LABEL_KZ"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_LANG_NAME_BY"] = "白俄羅斯";
$MESS["MAIN_USER_CONSENT_INTL_LANG_NAME_KZ"] = "哈薩克";
$MESS["MAIN_USER_CONSENT_INTL_NAME"] = "標準個人數據處理策略�％language_name％�";
$MESS["MAIN_USER_CONSENT_INTL_NOTIFY_TEXT"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_NOTIFY_TEXT_BTN"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_PHRASE_COMPANY_NAME"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_PHRASE_COMPANY_NAME_BY"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_PHRASE_COMPANY_NAME_KZ"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_PHRASE_IP_NAME"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_PHRASE_IP_NAME_BY"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_PHRASE_IP_NAME_KZ"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_TEXT"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_TEXT_BY"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_TEXT_KZ"] = " - ";
$MESS["MAIN_USER_CONSENT_INTL_TYPE_N"] = "風俗";
$MESS["MAIN_USER_CONSENT_INTL_TYPE_S"] = "標準";
