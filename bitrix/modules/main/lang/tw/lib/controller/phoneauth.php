<?php
$MESS["main_err_confirm"] = "不正確的代碼。請檢查SMS並再次輸入代碼。";
$MESS["main_err_confirm_code_format"] = "錯誤的驗證代碼";
$MESS["main_register_incorrect_request"] = "無效的請求";
$MESS["main_register_no_user"] = "找不到用戶";
