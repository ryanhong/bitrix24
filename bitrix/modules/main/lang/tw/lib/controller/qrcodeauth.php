<?php
$MESS["qrcodeauth_error_auth"] = "授權錯誤。";
$MESS["qrcodeauth_error_option"] = "QR代碼身份驗證被禁用。";
$MESS["qrcodeauth_error_pull"] = "未安裝推和拉模塊。";
$MESS["qrcodeauth_error_request"] = "無效的請求。";
$MESS["qrcodeauth_error_unique_id"] = "選擇用於身份驗證的不正確配置文件。";
