<?php
$MESS["MAIN_EXPORT_ACTION_CANCEL"] = "出口已取消。";
$MESS["MAIN_EXPORT_ACTION_EXPORT"] = "已處理的記錄：＃processed_items＃of＃total_items＃。文件大小：＃file_size_format＃。";
$MESS["MAIN_EXPORT_ACTION_UPLOAD"] = "將導出文件上傳到雲。上傳：＃uploaded_size_format＃。";
$MESS["MAIN_EXPORT_CLEAR"] = "刪除導出文件";
$MESS["MAIN_EXPORT_COMPLETED"] = "已經創建了導出文件。";
$MESS["MAIN_EXPORT_DOWNLOAD"] = "下載導出文件";
$MESS["MAIN_EXPORT_ERROR_NO_CLOUD_BUCKET"] = "雲未連接到門戶。";
$MESS["MAIN_EXPORT_EXPECTED_DURATION"] = "剩下的估計時間：";
$MESS["MAIN_EXPORT_EXPECTED_DURATION_HOURS"] = "＃小時＃H＃分鐘＃最低。";
$MESS["MAIN_EXPORT_EXPECTED_DURATION_MINUTES"] = "＃分鐘＃最低。";
$MESS["MAIN_EXPORT_FILE_DROPPED"] = "導出文件已刪除";
$MESS["MAIN_EXPORT_PURGE"] = "先前的導出嘗試創建的臨時文件已刪除。";
$MESS["MAIN_EXPORT_VOID"] = "無數據導出";
