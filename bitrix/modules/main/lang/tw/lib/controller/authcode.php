<?php
$MESS["main_authcode_incorrect_code"] = "錯誤的驗證代碼。";
$MESS["main_authcode_incorrect_code_input"] = "您輸入的代碼不正確。請檢查代碼，然後重試。";
$MESS["main_authcode_incorrect_request"] = "無效的請求。";
$MESS["main_authcode_otp_captcha_required"] = "需要兩因素身份驗證驗證碼。";
$MESS["main_authcode_otp_required"] = "需要兩因素身份驗證代碼。";
$MESS["main_authcode_retry_count"] = "超出嘗試的最大嘗試數。";
