<?php
$MESS["sms_template_active_title"] = "積極的";
$MESS["sms_template_event_name_title"] = "事件名稱";
$MESS["sms_template_id_title"] = "模板ID";
$MESS["sms_template_language_title"] = "消息語言";
$MESS["sms_template_message_title"] = "消息模板";
$MESS["sms_template_receiver_title"] = "收件人電話號碼";
$MESS["sms_template_sender_title"] = "發件人電話號碼";
