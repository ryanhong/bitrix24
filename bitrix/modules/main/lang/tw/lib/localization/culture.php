<?php
$MESS["culture_entity_charset"] = "charset";
$MESS["culture_entity_date_format"] = "日期格式";
$MESS["culture_entity_datetime_format"] = "日期和時間格式";
$MESS["culture_entity_name"] = "姓名";
$MESS["culture_entity_name_format"] = "名稱格式";
$MESS["culture_err_del_lang"] = "無法刪除區域選項，因為它們是由語言＃蓋子＃使用的。";
$MESS["culture_err_del_site"] = "無法刪除區域選項，因為它們是由網站＃蓋子＃使用的。";
