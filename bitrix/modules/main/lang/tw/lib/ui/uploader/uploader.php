<?php
$MESS["BXU_AccessDenied"] = "上傳文件夾的權限不足。請聯繫您的管理員。";
$MESS["BXU_EmptyData"] = "空數據";
$MESS["BXU_FileIsBlockedByOtherProcess"] = "該文件被另一個過程鎖定。";
$MESS["BXU_FileIsFailed"] = "文件驗證失敗。";
$MESS["BXU_FileIsFailedToRead"] = "錯誤讀取文件。";
$MESS["BXU_FileIsLost"] = "文件未找到。";
$MESS["BXU_FileIsNotFullyUploaded"] = "該文件僅部分上傳。";
$MESS["BXU_FileIsNotRestored"] = "該文件未恢復。";
$MESS["BXU_FileIsNotUploaded"] = "文件沒有上傳。";
$MESS["BXU_FilePartCanNotBeOpened"] = "無法打開文件塊。";
$MESS["BXU_FilePartCanNotBeRead"] = "無法讀取文件塊。";
$MESS["BXU_FileTransferIntoTheCloudIsFailed"] = "錯誤將文件上傳到雲。";
$MESS["BXU_FilesIsNotGlued"] = "合併錯誤。";
$MESS["BXU_RequiredParamCIDIsNotEntered"] = "需要CID。";
$MESS["BXU_RequiredParamPackageIndexIsNotEntered"] = "需要包裝INDEX。";
$MESS["BXU_SessionIsExpired"] = "您的會議已經過期。請再試一次。";
$MESS["BXU_TemporaryDirectoryIsNotCreated"] = "未創建臨時上傳目錄。請聯繫您的管理員。";
$MESS["BXU_TemporaryFileIsNotCreated"] = "未創建臨時上傳文件。";
$MESS["BXU_UPLOAD_ERR_CANT_WRITE"] = "無法將文件寫入磁盤。";
$MESS["BXU_UPLOAD_ERR_EXTENSION"] = "PHP擴展程序已停止上傳文件。";
$MESS["BXU_UPLOAD_ERR_FORM_SIZE"] = "上傳的文件大小超過了以HTML表單指定的max_file_size的限制。";
$MESS["BXU_UPLOAD_ERR_INI_SIZE"] = "上傳的文件大小超過了限制。";
$MESS["BXU_UPLOAD_ERR_NO_FILE"] = "該文件沒有上傳。";
$MESS["BXU_UPLOAD_ERR_NO_TMP_DIR"] = "缺少臨時目錄。";
$MESS["BXU_UPLOAD_ERR_PARTIAL"] = "該文件僅部分上傳。";
$MESS["BXU_UserHandlerError"] = "用戶處理程序錯誤。";
