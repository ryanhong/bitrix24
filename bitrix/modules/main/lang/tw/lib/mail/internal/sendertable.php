<?php
$MESS["main_mail_sender_email_title"] = "電子郵件";
$MESS["main_mail_sender_id_title"] = "ID";
$MESS["main_mail_sender_is_confirmed_title"] = "確認的";
$MESS["main_mail_sender_is_public_title"] = "向所有人提供";
$MESS["main_mail_sender_name_title"] = "發件人名稱";
$MESS["main_mail_sender_options_title"] = "參數";
$MESS["main_mail_sender_password_title"] = "密碼";
$MESS["main_mail_sender_user_id_title"] = "用戶身份";
