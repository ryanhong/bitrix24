<?php
$MESS["main_mail_smtp_connection_failed"] = "授權錯誤。請確保您的登錄名和密碼正確。<br>請注意，如果您使用應用程序密碼或兩因素身份驗證，則必須使用特殊集成密碼。";
