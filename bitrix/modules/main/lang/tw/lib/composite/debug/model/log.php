<?php
$MESS["LOG_ENTITY_AJAX_FIELD"] = "AJAX請求";
$MESS["LOG_ENTITY_CREATED_FIELD"] = "創建於";
$MESS["LOG_ENTITY_HOST_FIELD"] = "主持人";
$MESS["LOG_ENTITY_MESSAGE_FIELD"] = "訊息";
$MESS["LOG_ENTITY_PAGE_ID_FIELD"] = "頁面ID";
$MESS["LOG_ENTITY_TITLE_FIELD"] = "頁名";
$MESS["LOG_ENTITY_TYPE_FIELD"] = "事件類型";
$MESS["LOG_ENTITY_URI_FIELD"] = "頁面地址";
$MESS["LOG_ENTITY_USER_ID_FIELD"] = "用戶";
