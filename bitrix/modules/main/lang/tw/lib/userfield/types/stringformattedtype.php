<?php
$MESS["USER_TYPE_STRINGFMT_DEFAULT_VALUE"] = "預設值";
$MESS["USER_TYPE_STRINGFMT_DESCRIPTION"] = "模板";
$MESS["USER_TYPE_STRINGFMT_MAX_LEGTH_ERROR"] = "\“＃field_name＃\”的長度不得超過＃max_length＃符號。";
$MESS["USER_TYPE_STRINGFMT_MAX_LENGTH"] = "最大長度（0-無驗證）";
$MESS["USER_TYPE_STRINGFMT_MIN_LEGTH"] = "最小長度（0-無驗證）";
$MESS["USER_TYPE_STRINGFMT_MIN_LEGTH_ERROR"] = "\“＃field_name＃\”的長度必須為＃min_length＃或更多符號。";
$MESS["USER_TYPE_STRINGFMT_PATTERN"] = "模板（＃value＃ -  value）";
$MESS["USER_TYPE_STRINGFMT_REGEXP"] = "驗證正則表達式";
$MESS["USER_TYPE_STRINGFMT_REGEXP_ERROR"] = "\“＃field_name＃\”不匹配驗證正則表達式。";
$MESS["USER_TYPE_STRINGFMT_ROWS"] = "行計";
$MESS["USER_TYPE_STRINGFMT_SIZE"] = "輸入字段大小";
