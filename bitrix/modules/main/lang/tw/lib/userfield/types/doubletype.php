<?php
$MESS["USER_TYPE_DOUBLE_DEFAULT_VALUE"] = "預設值";
$MESS["USER_TYPE_DOUBLE_DESCRIPTION"] = "數字";
$MESS["USER_TYPE_DOUBLE_MAX_VALUE"] = "最大值（0-無驗證）";
$MESS["USER_TYPE_DOUBLE_MAX_VALUE_ERROR"] = "\“＃field_name＃\”的值不得超過＃max_value＃。";
$MESS["USER_TYPE_DOUBLE_MIN_VALUE"] = "最小值（0-無驗證）";
$MESS["USER_TYPE_DOUBLE_MIN_VALUE_ERROR"] = "\“＃field_name＃\”的值不得小於＃min_value＃。";
$MESS["USER_TYPE_DOUBLE_PRECISION"] = "精度（十進制點之後的數字數）";
$MESS["USER_TYPE_DOUBLE_SIZE"] = "輸入字段大小";
$MESS["USER_TYPE_DOUBLE_TYPE_ERROR"] = "\“＃field_name＃\”的值必須是一個數字";
