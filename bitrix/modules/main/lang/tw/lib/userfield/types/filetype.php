<?php
$MESS["USER_TYPE_FILE_DESCRIPTION"] = "文件";
$MESS["USER_TYPE_FILE_EXTENSIONS"] = "擴展";
$MESS["USER_TYPE_FILE_EXTENSIONS_HINT"] = "輸入允許的文件擴展名，逗號分隔";
$MESS["USER_TYPE_FILE_MAX_ALLOWED_SIZE"] = "上傳的最大文件長度（無限制）";
$MESS["USER_TYPE_FILE_MAX_SHOW_SIZE"] = "列表中的最大文件大小（0-無限制）";
$MESS["USER_TYPE_FILE_MAX_SIZE_ERROR"] = "\“＃field_name＃\”的文件長度不得超過＃max_allowed_size＃bytes。";
$MESS["USER_TYPE_FILE_SIZE"] = "輸入字段大小";
$MESS["USER_TYPE_FILE_TARGET_BLANK"] = "在新標籤中打開文件";
$MESS["USER_TYPE_FILE_VALUE_IS_MULTIPLE"] = "字段\“＃field_name＃\”不能是多個字段。";
$MESS["USER_TYPE_FILE_WIDTH_AND_HEIGHT"] = "列表視圖中的縮略圖寬度和高度";
