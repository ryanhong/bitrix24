<?php
$MESS["MAIN_SRV_GEOIP_MM_DESCRIPTION"] = "MaxMind是IP智能和在線欺詐檢測工具的行業領先提供商。 <br>
網站：<a href='https://www.maxmind.com'> https://www.maxmind.com </a> <br>
從MaxMind獲得的用戶ID和許可密鑰必須使用MaxMind處理程序。";
$MESS["MAIN_SRV_GEOIP_MM_F_LICENSE_KEY"] = "註冊碼";
$MESS["MAIN_SRV_GEOIP_MM_F_USER_ID"] = "用戶身份";
$MESS["MAIN_SRV_GEOIP_MM_SETT_EMPTY"] = "處理程序設置中缺少用戶ID和/或許可證密鑰";
$MESS["main_geoip_mm_service"] = "使用Web服務";
