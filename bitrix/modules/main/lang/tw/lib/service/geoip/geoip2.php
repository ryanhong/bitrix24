<?php
$MESS["geoip_geoip2_desc"] = "使用GEOIP2二進制MMDB本地文件檢測國家和城市。支持Geoip2和Geolite2數據庫。<br>網站：<a href= \"https://www.maxmind.com \"> https://www.maxmind.com </a>";
$MESS["geoip_geoip2_err_reading"] = "錯誤讀取數據庫文件。";
$MESS["geoip_geoip2_file"] = "數據庫絕對文件路徑（*.mmdb）";
$MESS["geoip_geoip2_file_not_found"] = "找不到數據庫文件。";
$MESS["geoip_geoip2_no_file"] = "數據庫文件路徑未指定。";
$MESS["geoip_geoip2_type"] = "數據庫類型";
