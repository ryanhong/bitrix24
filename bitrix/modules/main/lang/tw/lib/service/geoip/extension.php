<?php
$MESS["MAIN_SRV_GEOIP_EXT_DB_ASN"] = "asn";
$MESS["MAIN_SRV_GEOIP_EXT_DB_AVIALABLE"] = "可用數據庫";
$MESS["MAIN_SRV_GEOIP_EXT_DB_CITY"] = "城市";
$MESS["MAIN_SRV_GEOIP_EXT_DB_COUNTRY"] = "國家";
$MESS["MAIN_SRV_GEOIP_EXT_DB_ISP"] = "ISP的";
$MESS["MAIN_SRV_GEOIP_EXT_DB_ORG"] = "組織";
$MESS["MAIN_SRV_GEOIP_EXT_DESCRIPTION"] = "地理擴展程序使您可以找到IP地址的位置。可以在Geoip的幫助下獲得城市，州，國家，經度，緯度和其他信息，例如ISP和連接類型。 <br>
您將在此處找到有關安裝和配置的詳細信息：<a href='http://php.net/manual/manual/en/book.geoip.php'> http://php.net/manual/ manual/en/en/book.geoip。 php </a>";
$MESS["MAIN_SRV_GEOIP_EXT_NOT_REQ"] = "無需其他設置";
$MESS["MAIN_SRV_GEOIP_EXT_TITLE"] = "地理擴展";
$MESS["main_srv_geoip_ext_unsupported"] = "注意力！這種格式已過時，不再支持。";
