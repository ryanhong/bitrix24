<?php
$MESS["argument_out_of_range_array_exception_message"] = "“＃參數＃”的值必須是以下一個：＃allowable_values＃。";
$MESS["argument_out_of_range_between_exception_message"] = "“＃參數＃”的值必須在範圍內＃lower_limit＃to＃upper_limit＃。";
$MESS["argument_out_of_range_exception_message"] = "“＃參數＃”的值超出了可能的範圍。";
$MESS["argument_out_of_range_lower_exception_message"] = "“＃參數＃”的值不得小於＃lower_limit＃。";
$MESS["argument_out_of_range_upper_exception_message"] = "“＃參數＃”的值不得超過＃upper_limit＃。";
