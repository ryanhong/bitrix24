<?php
$MESS["SALE_USER_CONDITION_CONTROL_FIELD_USER_GROUP_EQ"] = "是用戶組的成員";
$MESS["SALE_USER_CONDITION_CONTROL_FIELD_USER_GROUP_ID"] = "用戶組";
$MESS["SALE_USER_CONDITION_CONTROL_FIELD_USER_GROUP_NOT_EQ"] = "不是用戶組的成員";
$MESS["SALE_USER_CONDITION_CONTROL_FIELD_USER_GROUP_PREFIX"] = "用戶";
$MESS["SALE_USER_CONDITION_CONTROL_FIELD_USER_ID"] = "用戶";
$MESS["SALE_USER_CONDITION_CONTROL_FIELD_USER_PREFIX"] = "用戶";
$MESS["SALE_USER_CONDITION_CONTROL_GROUP_NAME"] = "用戶";
