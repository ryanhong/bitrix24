<?php
$MESS["MAIN_BX_COMPONENT_CACHE_CLEAR"] = "清除組件的緩存";
$MESS["MAIN_BX_COMPONENT_CACHE_CLEAR_CONF"] = "您確定要清除組件的緩存嗎？";
$MESS["main_comp_button_menu_title"] = "編輯組件參數";
$MESS["main_comp_copy_templ"] = "複製組件模板";
$MESS["main_comp_copy_title"] = "製作用於編輯的系統模板的副本";
$MESS["main_comp_disable"] = "禁用組件";
$MESS["main_comp_disable_title"] = "暫時禁用組件（保持設置）";
$MESS["main_comp_edit_css"] = "編輯模板CSS文件";
$MESS["main_comp_edit_epilog"] = "編輯文件component_epilog.php";
$MESS["main_comp_edit_res_mod"] = "編輯文件RESURD_MODIFIER.PHP";
$MESS["main_comp_edit_templ"] = "編輯組件模板";
$MESS["main_comp_enable"] = "啟用組件";
$MESS["main_comp_enable_title"] = "啟用組件（帶有保存的設置）";
$MESS["main_incl_comp_component"] = "成分";
$MESS["main_incl_file_comp_param"] = "編輯組件參數";
