<?php
$MESS["comp_prop_err_comp"] = "無法找到組件調用。";
$MESS["comp_prop_err_open"] = "無法打開包含組件調用的腳本。";
$MESS["comp_prop_err_param"] = "不正確的輸入參數。";
$MESS["comp_prop_err_save"] = "錯誤保存腳本文件。更改未保存。";
