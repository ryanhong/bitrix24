<?php
$MESS["FILEMAN_FILEEDIT_FILENAME_EMPTY"] = "文件名丟失。";
$MESS["FILEMAN_FILEEDIT_FILE_EXISTS"] = "這個名稱的文件已經存在。";
$MESS["FILEMAN_FILEEDIT_FOLDER_EXISTS"] = "存在此名稱的文件夾。";
$MESS["FILEMAN_FILEEDIT_NAME"] = "文件名：";
$MESS["FILEMAN_FILEEDIT_PAGE_TITLE"] = "文件編輯器";
$MESS["FILEMAN_NEWFILEEDIT_TITLE"] = "新文件";
$MESS["FILEMAN_SESSION_EXPIRED"] = "您的會議已經過期。請再次保存文檔。";
$MESS["pub_src_edit_err"] = "錯誤保存文件..";
$MESS["public_file_edit_edit_cp"] = "在控制面板中編輯文件";
