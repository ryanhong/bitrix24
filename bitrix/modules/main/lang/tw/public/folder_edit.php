<?php
$MESS["FOLDER_EDIT_ACCESS_DENIED"] = "編輯對部分的訪問被拒絕。";
$MESS["FOLDER_EDIT_ADD_PROPS"] = "其他屬性";
$MESS["FOLDER_EDIT_DELETE_PROP"] = "刪除屬性";
$MESS["FOLDER_EDIT_FILEMAN_ERROR"] = "未安裝站點Explorer模塊。";
$MESS["FOLDER_EDIT_FOLDER_NAME"] = "部分名稱";
$MESS["FOLDER_EDIT_IN_ADMIN_SECTION"] = "在控制面板中編輯文件夾屬性";
$MESS["FOLDER_EDIT_NAME"] = "姓名";
$MESS["FOLDER_EDIT_NEW_PROP"] = "添加新屬性";
$MESS["FOLDER_EDIT_PROP_TITLE"] = "此屬性是從父段繼承的。單擊輸入字段以修改屬性。";
$MESS["FOLDER_EDIT_WINDOW_TITLE"] = "部分屬性";
$MESS["FOLDER_NAME_TITLE"] = "<b>部分名稱</b>將在麵包屑導航中顯示，作為指向相應站點部分的鏈接。";
$MESS["FOLDER_PROP_TITLE"] = "<b>部分屬性</b>定義了各節的其他屬性。例如，“關鍵字”的值。屬性將用作部分的所有頁面（以元/關鍵字標籤）的關鍵字。";
