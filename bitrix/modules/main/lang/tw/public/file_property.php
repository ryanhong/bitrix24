<?php
$MESS["PAGE_PROP_ACCESS_DENIED"] = "編輯訪問頁面被拒絕。";
$MESS["PAGE_PROP_DESCIPTION"] = "<b>頁面屬性</b>定義頁面的其他屬性。例如，“描述”的價值。屬性將用於元 /描述標籤）。";
$MESS["PAGE_PROP_EDIT_IN_ADMIN"] = "控制面板中的編輯頁面屬性";
$MESS["PAGE_PROP_FILE_NOT_FOUND"] = "指定的文件不存在。";
$MESS["PAGE_PROP_FOLDER_NAME"] = "頁面標題";
$MESS["PAGE_PROP_INHERIT_TITLE"] = "此屬性是從父段繼承的。單擊輸入字段以修改屬性。";
$MESS["PAGE_PROP_IN_ADMIN_SECTION"] = "控制面板中的編輯頁面屬性";
$MESS["PAGE_PROP_NAME"] = "標題";
$MESS["PAGE_PROP_TAGS"] = "標籤";
$MESS["PAGE_PROP_TAGS_DESCIPTION"] = "在此處鍵入與此頁面相關聯的逗號分隔單詞或短語。這些單詞將用於查找此頁面。";
$MESS["PAGE_PROP_TAGS_NAME"] = "頁面標籤";
$MESS["PAGE_PROP_WINDOW_TITLE"] = "頁面屬性";
