<?php
$MESS["PAGE_DELETE_ACCESS_DENIED"] = "您無權刪除此文件。";
$MESS["PAGE_DELETE_BUTTON_NO"] = "不，不要刪除";
$MESS["PAGE_DELETE_BUTTON_YES"] = "是的，刪除它";
$MESS["PAGE_DELETE_CONFIRM_TEXT"] = "您確定要刪除所選頁面<b> #filename＃</b>嗎？";
$MESS["PAGE_DELETE_ERROR_OCCURED"] = "錯誤刪除文件。可能的原因：您沒有足夠的權限來刪除文件。";
$MESS["PAGE_DELETE_FILE_NOT_FOUND"] = "指定的文件不存在。";
$MESS["PAGE_DELETE_FROM_MENU"] = "從菜單中刪除頁面";
$MESS["PAGE_DELETE_INDEX_WARNING"] = "注意力！刪除索引頁可能會導致站點部分無法可用。";
$MESS["PAGE_DELETE_WINDOW_TITLE"] = "刪除頁面確認";
