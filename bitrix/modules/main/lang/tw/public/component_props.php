<?php
$MESS["comp_prop_cancel"] = "取消";
$MESS["comp_prop_cancel_title"] = "關閉而沒有保存更改";
$MESS["comp_prop_close"] = "關閉窗口";
$MESS["comp_prop_close_w"] = "關閉";
$MESS["comp_prop_desc"] = "描述";
$MESS["comp_prop_err_comp"] = "無法找到組件調用。";
$MESS["comp_prop_err_file"] = "包含組件調用的腳本的權限不足。";
$MESS["comp_prop_err_open"] = "無法打開包含組件調用的腳本。";
$MESS["comp_prop_err_param"] = "不正確的輸入參數。";
$MESS["comp_prop_err_save"] = "錯誤保存腳本文件。更改未保存。";
$MESS["comp_prop_name"] = "姓名";
$MESS["comp_prop_not_sel"] = "（未選中的）";
$MESS["comp_prop_other"] = "（其他）";
$MESS["comp_prop_path"] = "組件包括路徑";
$MESS["comp_prop_save"] = "節省";
$MESS["comp_prop_save_title"] = "保存組件參數";
$MESS["comp_prop_title"] = "組件參數";
