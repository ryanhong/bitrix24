<?php
$MESS["get_start_menu_add_fav"] = "加入“最喜歡的...";
$MESS["get_start_menu_add_fav_title"] = "將當前頁面添加到收藏夾";
$MESS["get_start_menu_dbl"] = "（雙擊打開）";
$MESS["get_start_menu_fav"] = "最愛";
$MESS["get_start_menu_fav_title"] = "最喜歡的鏈接";
$MESS["get_start_menu_loading"] = "載入中...";
$MESS["get_start_menu_loading_title"] = "加載菜單項...";
$MESS["get_start_menu_no_data"] = "- 沒有數據 -";
$MESS["get_start_menu_org_fav"] = "最愛的組織者...";
$MESS["get_start_menu_org_fav_title"] = "管理喜歡的鏈接";
