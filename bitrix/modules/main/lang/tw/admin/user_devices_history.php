<?php
$MESS["main_user_devices_history_app_pass"] = "應用程序密碼";
$MESS["main_user_devices_history_city"] = "城市";
$MESS["main_user_devices_history_city_id"] = "城市代碼";
$MESS["main_user_devices_history_country"] = "國家";
$MESS["main_user_devices_history_country_id"] = "國家代碼";
$MESS["main_user_devices_history_date"] = "最後登錄日期";
$MESS["main_user_devices_history_device_id"] = "設備ID";
$MESS["main_user_devices_history_hash_pass"] = "一次性密碼";
$MESS["main_user_devices_history_ip"] = "IP地址";
$MESS["main_user_devices_history_page"] = "頁";
$MESS["main_user_devices_history_region"] = "地區";
$MESS["main_user_devices_history_region_id"] = "區域代碼";
$MESS["main_user_devices_history_stored_pass"] = "保存密碼";
$MESS["main_user_devices_history_title"] = "登錄歷史記錄";
