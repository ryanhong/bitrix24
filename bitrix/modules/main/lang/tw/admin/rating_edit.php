<?php
$MESS["MAIN_RATING_EDIT_RECORD"] = "編輯評分＃＃ ID＃";
$MESS["MAIN_RATING_NEW_RECORD"] = "新評分";
$MESS["RATING_DEF_NAME"] = "用戶評分";
$MESS["RATING_EDIT_ADD"] = "添加";
$MESS["RATING_EDIT_ADD_TITLE"] = "添加新評級";
$MESS["RATING_EDIT_CALC_METHOD_AVG"] = "平均的";
$MESS["RATING_EDIT_CALC_METHOD_SUM"] = "數量";
$MESS["RATING_EDIT_CAT_RATING_COMPONENT"] = "評級標準";
$MESS["RATING_EDIT_CAT_WHAT_NEW_CALC"] = "評分結果";
$MESS["RATING_EDIT_DEL"] = "刪除";
$MESS["RATING_EDIT_DEL_CONF"] = "您確定要刪除此評級嗎？";
$MESS["RATING_EDIT_DEL_TITLE"] = "刪除此評級";
$MESS["RATING_EDIT_ERROR"] = "錯誤保存記錄。";
$MESS["RATING_EDIT_FRM_ACTIVE"] = "積極的：";
$MESS["RATING_EDIT_FRM_AUTHORITY"] = "使用此評級來計算權威";
$MESS["RATING_EDIT_FRM_CALC_METHOD"] = "計算方法：";
$MESS["RATING_EDIT_FRM_CUR_VAL"] = "當前值的其他字段：";
$MESS["RATING_EDIT_FRM_NAME"] = "標題：";
$MESS["RATING_EDIT_FRM_NEW_CALC"] = "保存參數後重置評級結果";
$MESS["RATING_EDIT_FRM_POSITION"] = "計算評級位置";
$MESS["RATING_EDIT_FRM_PREV_VAL"] = "以前值的其他字段：";
$MESS["RATING_EDIT_FRM_TYPE_ID"] = "評估對象：";
$MESS["RATING_EDIT_SUCCESS"] = "該記錄已成功保存下來。";
$MESS["RATING_EDIT_TAB_MAIN"] = "主要參數";
$MESS["RATING_EDIT_TAB_MAIN_TITLE"] = "名稱，對象和評分標準";
$MESS["RATING_FIELDS_DEF_DESC"] = "價值取決於投票結果。";
$MESS["RATING_FIELDS_DEF_FORMULA"] = "總計 * k";
$MESS["RATING_FIELDS_DEF_FORMULA_DESC"] = "總數 - 投票結果； K-用戶定義因子。";
$MESS["RATING_FIELDS_DEF_NAME"] = "因素：";
$MESS["RATING_LIST"] = "所有評分";
$MESS["RATING_LIST_TITLE"] = "查看所有評分";
