<?php
$MESS["MAIN_MENU_AGENT"] = "代理商";
$MESS["MAIN_MENU_AGENT_ALT"] = "代理管理";
$MESS["MAIN_MENU_CACHE"] = "緩存設置";
$MESS["MAIN_MENU_CACHE_ALT"] = "緩存設置";
$MESS["MAIN_MENU_CAPTCHA"] = "驗證碼";
$MESS["MAIN_MENU_CAPTCHA_TITLE"] = "驗證設置";
$MESS["MAIN_MENU_CHECKLIST"] = "項目質量控制";
$MESS["MAIN_MENU_COMPOSITE"] = "複合網站";
$MESS["MAIN_MENU_COMPOSITE_ALT"] = "管理複合模式";
$MESS["MAIN_MENU_COMPOSITE_LOG"] = "調試日誌";
$MESS["MAIN_MENU_COMPOSITE_LOG_ALT"] = "查看調試消息";
$MESS["MAIN_MENU_COMPOSITE_PAGES"] = "頁面";
$MESS["MAIN_MENU_COMPOSITE_PAGES_ALT"] = "複合頁面";
$MESS["MAIN_MENU_COMPOSITE_SETTINGS"] = "設定";
$MESS["MAIN_MENU_DUMP"] = "備份";
$MESS["MAIN_MENU_DUMP_ALT"] = "文件和數據庫備份";
$MESS["MAIN_MENU_DUMP_AUTO"] = "配置時間表";
$MESS["MAIN_MENU_DUMP_LIST"] = "查看現有備份";
$MESS["MAIN_MENU_DUMP_LOG"] = "備份日誌";
$MESS["MAIN_MENU_DUMP_NEW"] = "創建備份";
$MESS["MAIN_MENU_EVENT1"] = "電子郵件和短信活動";
$MESS["MAIN_MENU_EVENT_LOG"] = "事件日誌";
$MESS["MAIN_MENU_EVENT_LOG_ALT"] = "查看事件日誌";
$MESS["MAIN_MENU_EVENT_THEMES"] = "視覺主題";
$MESS["MAIN_MENU_EVENT_THEMES_TITLE"] = "視覺主題";
$MESS["MAIN_MENU_EVENT_TITLE1"] = "管理電子郵件和短信模板";
$MESS["MAIN_MENU_EVENT_TYPES1"] = "事件類型";
$MESS["MAIN_MENU_EVENT_TYPES_TITLE1"] = "電子郵件和短信類型";
$MESS["MAIN_MENU_FAVORITE"] = "書籤列表";
$MESS["MAIN_MENU_FAVORITE_ALT"] = "書籤列表";
$MESS["MAIN_MENU_FAVORITE_HEADER"] = "書籤";
$MESS["MAIN_MENU_FILE_CHECKER"] = "文件檢查器";
$MESS["MAIN_MENU_FILE_CHECKER_ALT"] = "文件完整性檢查器";
$MESS["MAIN_MENU_GEOIP_HANDLERS"] = "地理位置";
$MESS["MAIN_MENU_GROUPS"] = "用戶組";
$MESS["MAIN_MENU_GROUPS_ALT"] = "管理服務器用戶組";
$MESS["MAIN_MENU_HOTKEYS"] = "鍵盤快捷鍵";
$MESS["MAIN_MENU_HOTKEYS_ALT"] = "自定義鍵盤快捷鍵";
$MESS["MAIN_MENU_HTTPS"] = "HTTPS配置";
$MESS["MAIN_MENU_HTTPS_ALT"] = "網站HTTPS配置";
$MESS["MAIN_MENU_INTERFACE"] = "介面";
$MESS["MAIN_MENU_INTERFACE_TITLE"] = "用戶界面設置";
$MESS["MAIN_MENU_LANG"] = "接口語言";
$MESS["MAIN_MENU_LANGS_ALT"] = "管理語言";
$MESS["MAIN_MENU_MAIN"] = "系統設置";
$MESS["MAIN_MENU_MAIN_MODULE"] = "主模塊";
$MESS["MAIN_MENU_MANAG"] = "管理用戶";
$MESS["MAIN_MENU_MODULES"] = "模塊";
$MESS["MAIN_MENU_MODULES_ALT"] = "管理系統模塊";
$MESS["MAIN_MENU_MODULE_SETT"] = "模塊的設置";
$MESS["MAIN_MENU_MODULE_SETTINGS"] = "模塊設置";
$MESS["MAIN_MENU_MP_CATEGORY"] = "類別的解決方案";
$MESS["MAIN_MENU_OPTIMIZE_DB"] = "數據庫優化";
$MESS["MAIN_MENU_OPTIMIZE_DB_ALT"] = "數據庫分析和優化";
$MESS["MAIN_MENU_PHP"] = "PHP命令行";
$MESS["MAIN_MENU_PHPINFO"] = "PHP設置";
$MESS["MAIN_MENU_PHPINFO_ALT"] = "所有PHP設置，PHP模塊和環境變量的完整列表。";
$MESS["MAIN_MENU_PHP_ALT"] = "在服務器上運行任意PHP命令";
$MESS["MAIN_MENU_PROFILE"] = "我的簡歷";
$MESS["MAIN_MENU_PROFILE_ALT"] = "管理個人資料設置";
$MESS["MAIN_MENU_PROFILE_HISTORY"] = "個人資料歷史記錄";
$MESS["MAIN_MENU_PROFILE_HISTORY_TITLE"] = "用戶配置文件更新日誌";
$MESS["MAIN_MENU_RATING"] = "評分";
$MESS["MAIN_MENU_RATING_ALT"] = "評級管理";
$MESS["MAIN_MENU_RATING_LIST"] = "評分";
$MESS["MAIN_MENU_RATING_LIST_ALT"] = "評級管理";
$MESS["MAIN_MENU_RATING_RULE_LIST"] = "處理規則";
$MESS["MAIN_MENU_RATING_RULE_LIST_ALT"] = "管理評級處理規則";
$MESS["MAIN_MENU_RATING_SETTINGS"] = "評分參數";
$MESS["MAIN_MENU_RATING_SETTINGS_ALT"] = "投票權重，計算方法和投票權限的參數";
$MESS["MAIN_MENU_REPAIR_DB"] = "數據庫檢查";
$MESS["MAIN_MENU_REPAIR_DB_ALT"] = "檢查 /維修數據庫表";
$MESS["MAIN_MENU_SETTINGS"] = "系統設置";
$MESS["MAIN_MENU_SETTINGS_ALT"] = "服務器和模塊設置";
$MESS["MAIN_MENU_SETTINGS_TITLE"] = "系統和模塊設置";
$MESS["MAIN_MENU_SHORT_URLS"] = "短URL的";
$MESS["MAIN_MENU_SHORT_URLS_ALT"] = "簡短的URL參數";
$MESS["MAIN_MENU_SITES"] = "網站";
$MESS["MAIN_MENU_SITES_ALT"] = "管理系統中的站點";
$MESS["MAIN_MENU_SITES_LIST"] = "網站";
$MESS["MAIN_MENU_SITES_TITLE"] = "管理站點";
$MESS["MAIN_MENU_SITE_CHECKER_ALT"] = "測試站點參數和報告錯誤消息以支持";
$MESS["MAIN_MENU_SMILE"] = "笑臉";
$MESS["MAIN_MENU_SMILE_GALLERY_LIST"] = "畫廊和包裝";
$MESS["MAIN_MENU_SMILE_GALLERY_LIST_ALT"] = "管理表情孔畫廊和包裝";
$MESS["MAIN_MENU_SMILE_IMPORT_LIST"] = "導入笑臉";
$MESS["MAIN_MENU_SMILE_IMPORT_LIST_ALT"] = "從外部文件導入笑臉";
$MESS["MAIN_MENU_SMTP_CONFIG"] = "SMTP設置";
$MESS["MAIN_MENU_SMTP_CONFIG_TITLE"] = "SMTP設置";
$MESS["MAIN_MENU_SQL"] = "SQL查詢";
$MESS["MAIN_MENU_SQL_ALT"] = "執行數據庫SQL查詢";
$MESS["MAIN_MENU_SYSTEM_CHECKER"] = "系統檢查";
$MESS["MAIN_MENU_TASKS"] = "訪問級別";
$MESS["MAIN_MENU_TASKS_ALT"] = "配置訪問級別";
$MESS["MAIN_MENU_TEMPLATE"] = "站點模板";
$MESS["MAIN_MENU_TEMPLATES"] = "電子郵件模板";
$MESS["MAIN_MENU_TEMPLATES_ALT"] = "電子郵件模板";
$MESS["MAIN_MENU_TEMPL_TITLE"] = "站點設計模板";
$MESS["MAIN_MENU_TOOLS"] = "工具";
$MESS["MAIN_MENU_TOOLS_TITLE"] = "服務工具";
$MESS["MAIN_MENU_UPDATES"] = "更新";
$MESS["MAIN_MENU_UPDATES_A"] = "更新";
$MESS["MAIN_MENU_UPDATES_ALT"] = "下載系統更新";
$MESS["MAIN_MENU_UPDATES_ALT_A"] = "下載系統更新";
$MESS["MAIN_MENU_UPDATES_MARKET"] = "市場";
$MESS["MAIN_MENU_UPDATES_MARKET_ALT"] = "市場";
$MESS["MAIN_MENU_UPDATES_MARKET_CATALOG"] = "解決方案";
$MESS["MAIN_MENU_UPDATES_MARKET_CATALOG_ALT"] = "查看解決方案";
$MESS["MAIN_MENU_UPDATES_NEW"] = "平台更新";
$MESS["MAIN_MENU_UPDATES_NEW_ALT"] = "安裝系統更新";
$MESS["MAIN_MENU_UPDATES_PARTNER"] = "合作夥伴更新";
$MESS["MAIN_MENU_UPDATES_PARTNER_ALT"] = "下載合作夥伴模塊更新";
$MESS["MAIN_MENU_UPDATES_PARTNER_MODULES"] = "安裝解決方案";
$MESS["MAIN_MENU_UPDATES_PARTNER_MODULES_ALT"] = "查看安裝的解決方案";
$MESS["MAIN_MENU_UPDATES_PARTNER_NEW"] = "解決方案更新";
$MESS["MAIN_MENU_UPDATES_PARTNER_NEW_ALT"] = "安裝第三方組件的更新";
$MESS["MAIN_MENU_URLREWRITE"] = "URL處理";
$MESS["MAIN_MENU_URLREWRITE_ALT"] = "URL處理規則設置";
$MESS["MAIN_MENU_USERS"] = "用戶";
$MESS["MAIN_MENU_USERS_ALT"] = "管理用戶配置文件";
$MESS["MAIN_MENU_USER_CONSENT"] = "同意";
$MESS["MAIN_MENU_USER_CONSENT_1"] = "協議和同意";
$MESS["MAIN_MENU_USER_FIELD"] = "自定義字段";
$MESS["MAIN_MENU_USER_FIELD_TITLE"] = "自定義字段";
$MESS["MAIN_MENU_USER_GROUPS_ALT"] = "組的用戶";
$MESS["MAIN_MENU_USER_IMPORT"] = "用戶導入";
$MESS["MAIN_MENU_USER_IMPORT_ALT"] = "用戶導入";
$MESS["MAIN_MENU_USER_LIST"] = "用戶列表";
$MESS["MAIN_MENU_WIZARDS"] = "嚮導列表";
$MESS["MAIN_MENU_WIZARDS_TITLE"] = "巫師管理";
$MESS["main_admin_menu_devices"] = "用戶設備";
$MESS["main_admin_menu_devices_title"] = "用戶設備";
$MESS["main_menu_diag"] = "系統管理";
$MESS["main_menu_diag_title"] = "管理員的工具";
$MESS["main_menu_interface"] = "個人喜好";
$MESS["main_menu_interface_title"] = "用戶界面設置";
$MESS["main_menu_lang_param"] = "語言參數";
$MESS["main_menu_lang_param_title"] = "設置用戶界面語言和環境參數";
$MESS["main_menu_reg_sett"] = "區域選擇和文化";
$MESS["main_menu_reg_sett_title"] = "設置數據輸出格式，編碼和其他參數";
$MESS["main_menu_sms_templates"] = "SMS模板";
$MESS["main_menu_sms_templates_title"] = "SMS模板";
$MESS["main_menu_urlrewrite_title"] = "友好的URL設置";
