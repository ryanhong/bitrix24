<?php
$MESS["MAIN_OPTION_CACHE_BUTTON_OFF"] = "禁用緩存";
$MESS["MAIN_OPTION_CACHE_BUTTON_ON"] = "啟用緩存";
$MESS["MAIN_OPTION_CACHE_ERROR"] = "組件的類型緩存已經設置為此值";
$MESS["MAIN_OPTION_CACHE_OFF"] = "默認情況下禁用組件緩存";
$MESS["MAIN_OPTION_CACHE_OK"] = "清理緩存文件";
$MESS["MAIN_OPTION_CACHE_ON"] = "默認啟用組件緩存";
$MESS["MAIN_OPTION_CACHE_SUCCESS"] = "組件的類型緩存成功切換";
$MESS["MAIN_OPTION_CLEAR_CACHE"] = "刪除緩存文件";
$MESS["MAIN_OPTION_CLEAR_CACHE_ALL"] = "全部";
$MESS["MAIN_OPTION_CLEAR_CACHE_CLEAR"] = "清除";
$MESS["MAIN_OPTION_CLEAR_CACHE_LANDING"] = "站點24";
$MESS["MAIN_OPTION_CLEAR_CACHE_MANAGED"] = "所有託管緩存";
$MESS["MAIN_OPTION_CLEAR_CACHE_MENU"] = "菜單";
$MESS["MAIN_OPTION_CLEAR_CACHE_OLD"] = "只有過時";
$MESS["MAIN_OPTION_CLEAR_CACHE_STATIC"] = "HTML緩存中的所有頁面";
$MESS["MAIN_OPTION_PUBL"] = "組件緩存設置";
$MESS["MAIN_TAB_3"] = "刪除緩存文件";
$MESS["MAIN_TAB_4"] = "組件緩存";
$MESS["MCACHE_TITLE"] = "緩存設置";
$MESS["cache_admin_note1"] = "
<p>使用自動氣管模式驚人地加速您的網站！</p>
<p>在自動氣管模式下，由組件呈現的信息根據這些組件的設置進行刷新。</p>
<p>要刷新頁面上的緩存對象，您可以：</p>
<p> 1。通過單擊管理工具欄上的特殊更新數據按鈕打開所需的頁面並刷新其對象。</p>
<img src = \“/bitrix/images/main/page_cache_en.png \” vspace = \ \“ 5 \”/>
<p> 2。在站點編輯模式時，您可以單擊給定組件的清除緩存按鈕。 </p>
<img src = \“/bitrix/images/main/comp_cache_en.png \” vspace = \ \“ 5 \”/>
<p> 3。轉到組件設置，然後將所需的組件切換為未切換的模式。</p>
<img src = \“/bitrix/images/main/spisok_en.png \” vspace = \ \“ 5 \”/>
<p>啟用緩存模式後，默認情況下所有帶有自動緩存設置的組件<i> \“ auto \” </i>將切換到使用CACH。</p>
<p>帶有緩存設置的組件<i> \“ CACH \” </i> </i>和緩存時間大於0（零），始終在緩存模式下工作。</p>
<p>帶有緩存設置的組件<i> \“請勿緩存\” </i>或與0等於0（零）的緩存時間，始終在不緩存的情況下工作。</p>";
$MESS["cache_admin_note2"] = "刪除緩存文件後，所有顯示的內容將根據新數據進行更新。
將在帶有緩存區域的請求頁面上逐漸創建新的緩存文件。";
$MESS["cache_admin_note4"] = "<p> html緩存建議用於更改很少發生的站點部分，並且大多是由匿名用戶訪問的。啟用HTML緩存時，將進行以下過程：</p>
<ul style = \“ font-size：100％\”>
<li> html緩存過程僅在包含蒙版中列出的頁面，而在ublufusion mask中未列出； </li>
<li>對於非授權訪問者，系統檢查頁面副本存儲在HTML緩存中。如果該頁面在緩存中找到，則顯示不包括系統模塊。統計信息不會跟踪訪客。廣告，內核和其他模塊也不會包括在內; </li>
<li>如果在緩存生成時安裝壓縮模塊； </li>
<li>如果頁面沒有找到緩存，則以通常的方式處理。完成頁面加載後，該頁面的副本將保存在HTML緩存中； </li>
</ul>
<p>緩存清理：</p>
<ul style = \“ font-size：100％\”>
<li>如果保存數據原因超過了磁盤配額，則緩存已完全丟棄； </li>
<li>如果通過控制面板更改了任何數據，也可以執行完整的緩存轉儲; </li>
<li>如果數據是從網站的公共頁面發布的（例如，添加評論或票數），則僅拋棄緩存的相關部分； </li>
</ul>
<p>請注意，當未經授權的用戶訪問非臨床網站頁面時，將啟動會話，而HTML-CACHE將不再處於活動狀態。</p>
<p>重要說明：</p>
<ul style = \“ font-size：100％\”>
<li>未跟踪統計信息； </li>
<li>廣告模塊只有在創建HTML緩存的那一刻才能工作。請注意，它不會影響外部廣告模塊（Google Ad Sense等）; </li>
<li>比較項目的結果不會保存給未經授權的用戶（應該啟動會話）; </li> </li>
<li>應指定磁盤配額以避免在磁盤空間上進行DOS攻擊； </li>
<li>啟用HTML緩存後，應檢查所有站點部分功能（例如，博客評論將不適用於舊博客模板等）; </li>
</ul>";
$MESS["cache_admin_note5"] = "在此版本中始終啟用HTML緩存。";
$MESS["main_cache_files_continue"] = "繼續";
$MESS["main_cache_files_delete_errors"] = "刪除錯誤：＃值＃";
$MESS["main_cache_files_deleted_count"] = "刪除：＃值＃";
$MESS["main_cache_files_deleted_size"] = "刪除文件的大小：＃值＃";
$MESS["main_cache_files_last_path"] = "當前文件夾：＃值＃";
$MESS["main_cache_files_scanned_count"] = "處理：＃值＃";
$MESS["main_cache_files_scanned_size"] = "處理的文件大小：＃值＃";
$MESS["main_cache_files_start"] = "開始";
$MESS["main_cache_files_stop"] = "停止";
$MESS["main_cache_finished"] = "緩存文件已刪除。";
$MESS["main_cache_in_progress"] = "刪除緩存文件。";
$MESS["main_cache_managed"] = "託管緩存";
$MESS["main_cache_managed_const"] = "定義了bx_comp_managed_cache常數。始終啟用託管緩存。";
$MESS["main_cache_managed_note"] = "<b>緩存依賴項</b>技術每次發生數據更改時都會更新緩存。如果此功能啟用，則在更新新聞或產品時無需手動更新緩存：網站訪問者將始終看到最新信息。

<br> <br>在Bitrix網站上獲取有關緩存依賴性技術的更多信息。
<br> <br> <span style = \“顏色：灰色\”>注意：並非所有組件都可以支持此功能。</span>";
$MESS["main_cache_managed_off"] = "託管緩存被禁用（不建議）。";
$MESS["main_cache_managed_on"] = "啟用了託管緩存。";
$MESS["main_cache_managed_saved"] = "託管緩存設置已保存。";
$MESS["main_cache_managed_sett"] = "託管緩存參數";
$MESS["main_cache_managed_turn_off"] = "禁用託管緩存（不建議）";
$MESS["main_cache_managed_turn_on"] = "啟用託管緩存";
$MESS["main_cache_wrong_cache_path"] = "無效的高速緩存文件路徑。";
$MESS["main_cache_wrong_cache_type"] = "無效的緩存類型。";
