<?php
$MESS["MAIN_TEMPLATE_LOAD_ERR_EX"] = "模板＃template_name＃已經存在。";
$MESS["MAIN_TEMPLATE_LOAD_ERR_ID"] = "請輸入模板標識符";
$MESS["MAIN_TEMPLATE_LOAD_ERR_LOAD"] = "請選擇上傳的文件";
$MESS["MAIN_TEMPLATE_LOAD_FILE"] = "用於上傳的模板文件（*.gz）：";
$MESS["MAIN_TEMPLATE_LOAD_GOTO_EDIT"] = "保存之後，轉到模板編輯頁面：";
$MESS["MAIN_TEMPLATE_LOAD_ID"] = "模板ID：";
$MESS["MAIN_TEMPLATE_LOAD_OK"] = "模板＃template_name＃成功上傳。";
$MESS["MAIN_TEMPLATE_LOAD_SITE_ID"] = "為網站做模板默認值：";
$MESS["MAIN_TEMPLATE_LOAD_SITE_ID_N"] = "（沒有任何）";
$MESS["MAIN_TEMPLATE_LOAD_SUBMIT"] = "上傳";
$MESS["MAIN_TEMPLATE_LOAD_TITLE"] = "導入模板";
$MESS["MAIN_T_EDIT_IMP_ERR"] = "導入模板的錯誤";
$MESS["MAIN_T_EDIT_TEMPL_LIST"] = "模板列表";
