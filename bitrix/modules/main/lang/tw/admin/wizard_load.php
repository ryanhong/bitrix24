<?php
$MESS["MAIN_WIZARD_IMPORT_ERROR"] = "嚮導導入錯誤";
$MESS["MAIN_WIZARD_LOAD_ERROR_EX"] = "嚮導＃wizard_name＃已經存在。";
$MESS["MAIN_WIZARD_LOAD_ERROR_ID"] = "輸入嚮導ID";
$MESS["MAIN_WIZARD_LOAD_ERROR_LOAD"] = "選擇要下載的文件";
$MESS["MAIN_WIZARD_LOAD_FILE"] = "嚮導文件（*.gz）";
$MESS["MAIN_WIZARD_LOAD_LINK_LIST"] = "巫師";
$MESS["MAIN_WIZARD_LOAD_OK"] = "巫師已成功進口。";
$MESS["MAIN_WIZARD_LOAD_SUBMIT"] = "進口";
$MESS["MAIN_WIZARD_LOAD_TITLE"] = "進口嚮導";
$MESS["MAIN_WIZARD_TAR_GZ"] = "數據文件不包含數據。無法進口。";
