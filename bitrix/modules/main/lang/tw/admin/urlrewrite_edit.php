<?php
$MESS["MURL_2_LIST"] = "記錄列表";
$MESS["MURL_2_LIST_ALT"] = "返回記錄列表";
$MESS["MURL_ACT_ADD"] = "添加";
$MESS["MURL_ACT_ADD_ALT"] = "添加新記錄";
$MESS["MURL_ACT_DEL"] = "刪除";
$MESS["MURL_ACT_DEL_CONF"] = "您確定要刪除此唱片嗎？";
$MESS["MURL_ADD"] = "添加新記錄";
$MESS["MURL_COMPONENT"] = "成分";
$MESS["MURL_DUPL_CONDITION"] = "記錄條件“＃條件＃”已經存在";
$MESS["MURL_EDIT"] = "編輯記錄";
$MESS["MURL_FILE"] = "文件";
$MESS["MURL_NO_USL"] = "條件未指定";
$MESS["MURL_RULE"] = "規則";
$MESS["MURL_TAB"] = "設定";
$MESS["MURL_TAB_ALT"] = "記錄設置";
$MESS["MURL_USL"] = "狀態";
$MESS["SAE_ERROR"] = "錯誤保存規則";
