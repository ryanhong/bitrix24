<?php
$MESS["IMPORT_FROM_1C_REQ_NOTES"] = "需要\“ Intranet Portal \”和\“信息塊\”模塊";
$MESS["LDAP_MODULE_NOT_INSTALLED"] = "需要AD/LDAP模塊";
$MESS["USER_IMPORT_1C_HELP"] = "從1C進口用戶：可以通過兩個步驟執行人力資源：<br />
1. BitRix24設置<br />
2.從1C進口：人力資源
<br /> <br />
BITRI24可以通過<a target= \"_blank \" href= \"/bitrix/admin/settings.php?mid=Intranet&lang=en&back_url_settings=&tabcontrol_tabcontrol_tabcontrol_tab=editiT2 \"> Intranet模塊設置Page </ai。
<br /> <br />
請注意將允許從1C：人力資源導入的用戶組的用戶組的正確選擇。選擇正確的網站以在導入後鏈接用戶。
<br /> <br />
您可以在保存Bitrix24中的設置後繼續從1C：人力資源導入用戶。
請閱讀管理員指南以獲取有關用戶導入的更多信息。";
$MESS["USER_IMPORT_1C_USER_CONFIRM_PASS"] = "確認密碼";
$MESS["USER_IMPORT_1C_USER_EMAIL"] = "電子郵件";
$MESS["USER_IMPORT_1C_USER_GROUP"] = "將用戶添加到組";
$MESS["USER_IMPORT_1C_USER_GROUP_EMPTY"] = "沒有指定組";
$MESS["USER_IMPORT_1C_USER_LOGIN"] = "登錄（3個或更多字符）：";
$MESS["USER_IMPORT_1C_USER_PASS"] = "密碼（3個或更多字符）：";
$MESS["USER_IMPORT_ALL"] = "（全部）";
$MESS["USER_IMPORT_ALLOW_LDAP_AUTH"] = "允許LDAP授權";
$MESS["USER_IMPORT_ATTACH_TO_IBLOCK"] = "將用戶綁定到信息塊部分";
$MESS["USER_IMPORT_ATTTACH_GROUP"] = "將用戶添加到組";
$MESS["USER_IMPORT_BACK_TO_START"] = "返回開始";
$MESS["USER_IMPORT_CREATE_1C_USER"] = "通過從1C：人力資源導入的權限創建用戶。";
$MESS["USER_IMPORT_CSV_NOT_FOUND"] = "找不到數據文件。";
$MESS["USER_IMPORT_CSV_SAMPLE"] = "樣本";
$MESS["USER_IMPORT_DATA_FILE"] = "數據文件";
$MESS["USER_IMPORT_DELIMETER"] = "場分離器";
$MESS["USER_IMPORT_DELIMETER_COMMA"] = "逗號";
$MESS["USER_IMPORT_DELIMETER_SEMICOLON"] = "分號";
$MESS["USER_IMPORT_DELIMETER_SPACE"] = "空間";
$MESS["USER_IMPORT_DELIMETER_TABULATION"] = "標籤";
$MESS["USER_IMPORT_EMAIL_TEMPLATE1"] = "電子郵件模板網站";
$MESS["USER_IMPORT_FINISHED"] = "導入已經完成";
$MESS["USER_IMPORT_FROM"] = "導入源";
$MESS["USER_IMPORT_FROM_1C"] = "1C：人類歸因";
$MESS["USER_IMPORT_FROM_CSV"] = "CSV文件";
$MESS["USER_IMPORT_FROM_LDAP"] = "Active Directory / LDAP";
$MESS["USER_IMPORT_GROUP_PERM_NAME"] = "從1C導入用戶";
$MESS["USER_IMPORT_IGNORE_DUPLICATE"] = "忽略重複的登錄";
$MESS["USER_IMPORT_IMAGE_PATH"] = "相對於站點根的圖像途徑";
$MESS["USER_IMPORT_LDAP_IMP_ADD"] = "導入其他字段：";
$MESS["USER_IMPORT_LDAP_IMP_DEF"] = "導入字段：";
$MESS["USER_IMPORT_LDAP_IMP_DEF_NOTE"] = "（從服務器設置）";
$MESS["USER_IMPORT_LDAP_SERVER"] = "LDAP服務器";
$MESS["USER_IMPORT_LDAP_SERVER_AUTH_ERROR"] = "服務器授權錯誤。";
$MESS["USER_IMPORT_LDAP_SERVER_CONN_ERROR"] = "連接到服務器的錯誤。";
$MESS["USER_IMPORT_LDAP_SERVER_NOT_FOUND"] = "找不到LDAP服務器。";
$MESS["USER_IMPORT_NEW_LDAP_SERVER"] = "創造";
$MESS["USER_IMPORT_NEXT_BUTTON"] = "下一個";
$MESS["USER_IMPORT_NO"] = "（沒有可用字段）";
$MESS["USER_IMPORT_OPEN_DIALOG"] = "打開";
$MESS["USER_IMPORT_PREV_BUTTON"] = "後退";
$MESS["USER_IMPORT_RESULT_TAB"] = "進口";
$MESS["USER_IMPORT_RESULT_TAB_DESC"] = "導入用戶";
$MESS["USER_IMPORT_SELECT_FROM_LIST"] = "選擇";
$MESS["USER_IMPORT_SELECT_IBLOCK"] = "選擇信息塊";
$MESS["USER_IMPORT_SEND_MAIL"] = "通知用戶如果有可用的電子郵件";
$MESS["USER_IMPORT_SETTINGS_TAB"] = "導入設置";
$MESS["USER_IMPORT_SETTINGS_TAB_DESC"] = "導入首選項";
$MESS["USER_IMPORT_SOURCE_TAB"] = "數據源";
$MESS["USER_IMPORT_SOURCE_TAB_DESC"] = "選擇數據源";
$MESS["USER_IMPORT_TITLE"] = "導入用戶";
$MESS["USER_IMPORT_USERS_COUNT"] = "用戶導入";
$MESS["USER_IMPORT_UTF"] = "注意力！數據文件應在UTF-8編碼中。";
