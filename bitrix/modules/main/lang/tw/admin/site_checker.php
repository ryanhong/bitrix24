<?php
$MESS["ERR_MAX_INPUT_VARS"] = "max_input_vars的值必須為＃min＃或更高。當前值是：＃當前＃";
$MESS["ERR_NO_MODS"] = "未安裝所需的擴展名：";
$MESS["ERR_NO_MODS_DOC_GENERATOR"] = "文檔生成器模塊需要PHP-XML和PHP-ZIP擴展。";
$MESS["ERR_NO_SSL"] = "對於PHP，未啟用SSL支持";
$MESS["ERR_NO_VM"] = "BitRix24保證僅在Bitrix環境上順利運行。您正在使用自定義服務器環境。";
$MESS["ERR_OLD_VM"] = "您正在運行Bitrix環境（＃當前＃）的過時版本。請安裝最新版本以防止配置問題（＃last_version＃）。";
$MESS["MAIN_AGENTS_HITS"] = "系統代理以命中率運行。將代理商遷移到克朗。";
$MESS["MAIN_BX_CRONTAB_DEFINED"] = "定義了BX_Crontab常數，該常數只能在Cron上運行的腳本中完成。";
$MESS["MAIN_CATDOC_WARN"] = "壞CATDOC版本：＃版本＃<br>
詳細信息：https：//bugs.debian.org/cgi-bin/bugreport.cgi?bug=679877 <br>
安裝較早的CATDOC版本或帶有修復程序的較新版本。";
$MESS["MAIN_CRON_NO_START"] = "cron_events.php不配置為在cron上運行；最新的經紀人已在24小時前執行。";
$MESS["MAIN_FAST_DOWNLOAD_ERROR"] = "nginx供電的快速文件下載不可用，但是在內核設置中啟用。";
$MESS["MAIN_FAST_DOWNLOAD_SUPPORT"] = "NGINX供電的快速文件下載可用，但在內核設置中禁用。";
$MESS["MAIN_IS_CORRECT"] = "正確的";
$MESS["MAIN_NO_OPTION_PULL"] = "可以在推送和拉模塊設置中未選中啟用推動通知的選項。移動設備將不會收到推送通知。";
$MESS["MAIN_NO_PULL"] = "未安裝推和拉模塊。";
$MESS["MAIN_NO_PULL_MODULE"] = "未安裝推和拉模塊。移動設備將不會收到推送通知。";
$MESS["MAIN_PAGES_PER_SECOND"] = "每秒頁";
$MESS["MAIN_PERF_HIGH"] = "高的";
$MESS["MAIN_PERF_LOW"] = "低的";
$MESS["MAIN_PERF_MID"] = "平均的";
$MESS["MAIN_PERF_VERY_LOW"] = "低低";
$MESS["MAIN_SC_ABS"] = "沒有任何";
$MESS["MAIN_SC_ABSENT_ALL"] = "沒有任何";
$MESS["MAIN_SC_AGENTS_CRON"] = "使用Cron來運行代理商";
$MESS["MAIN_SC_ALL_FUNCS_TESTED"] = "所有Intranet特徵都已檢查並發現已井井有條。";
$MESS["MAIN_SC_ALL_MODULES"] = "安裝了所有必需的模塊。";
$MESS["MAIN_SC_AVAIL"] = "可用的";
$MESS["MAIN_SC_BUSINESS"] = "Intranet業務功能";
$MESS["MAIN_SC_CANT_CHANGE"] = "無法使用ini_set修改pcre.backtrack_limit的值。";
$MESS["MAIN_SC_CLOUD_TEST"] = "訪問Bitrix雲服務";
$MESS["MAIN_SC_COMPRESSION_TEST"] = "頁面壓縮和加速";
$MESS["MAIN_SC_COMP_DISABLED"] = "使用BitRix壓縮模塊，該服務器不支持壓縮（PHP）";
$MESS["MAIN_SC_COMP_DISABLED_MOD"] = "服務器不支持壓縮，壓縮模塊禁用";
$MESS["MAIN_SC_CORRECT"] = "正確的";
$MESS["MAIN_SC_CORRECT_DESC"] = "Intranet需要對服務器環境的特殊配置。 <a href= \之http://www.bitrixsoft.com/products/virtual_appliance/ \“target= \"_blank \"> Bitrix Virtual Appliance </a>被正確配置在開箱即用。如果您未能調整所需的參數，則某些功能可能不可用。";
$MESS["MAIN_SC_CORRECT_SETTINGS"] = "設置是正確的";
$MESS["MAIN_SC_DEFAULT_CHARSET"] = "Default_charset參數不得為空。";
$MESS["MAIN_SC_DOCS_EDIT_MS_OFFICE"] = "Microsoft Office的編輯文檔";
$MESS["MAIN_SC_ENABLED"] = "服務器支持壓縮，需要卸載BITRIX壓縮模塊";
$MESS["MAIN_SC_ENABLED_MOD"] = "使用Web服務器模塊進行壓縮";
$MESS["MAIN_SC_ENC_EQUAL"] = "mbString.internal_encoding和default_charset值不匹配。建議清除mbString.internal_encoding並設置DEFAULT_CHARSET值。";
$MESS["MAIN_SC_ENC_NON_UTF"] = "Default_charset的值必須設置為UTF-8以外的編碼。";
$MESS["MAIN_SC_ENC_UTF"] = "必須將DEFAULT_CHARSET的值設置為UTF-8。";
$MESS["MAIN_SC_ERROR_PRECISION"] = "\“ Precision \”參數值無效。";
$MESS["MAIN_SC_EXTERNAL_ANSWER_INCORRECT"] = "與Intranet的外部連接成功，但服務器返回了不正確的狀態。";
$MESS["MAIN_SC_EXTERNAL_APPS_TEST"] = "通過安全連接的應用程序（MS Office，Outlook，Exchange）";
$MESS["MAIN_SC_EXTERNAL_CALLS"] = "外部視頻通話";
$MESS["MAIN_SC_EXTRANET_ACCESS"] = "外部訪問外部";
$MESS["MAIN_SC_FAST_FILES_TEST"] = "快速文件和文檔訪問";
$MESS["MAIN_SC_FULL_TEST_DESC"] = "運行完整的系統檢查以查找弱點並修復網站問題或避免將來問題。為每個測試提供的簡短但全面的描述將幫助您解決問題的根源並解決問題。";
$MESS["MAIN_SC_FUNC_OVERLOAD"] = "遺留參數MBSTRING.FUNC_OVERLOAD已指定。請刪除它。";
$MESS["MAIN_SC_FUNC_WORKS_FINE"] = "該功能還可以";
$MESS["MAIN_SC_FUNC_WORKS_PARTIAL"] = "該功能可能有問題；檢查並修復它們。";
$MESS["MAIN_SC_FUNC_WORKS_WRONG"] = "該功能不合時間。修正錯誤。";
$MESS["MAIN_SC_GENERAL"] = "一般的Intranet特徵";
$MESS["MAIN_SC_GENERAL_SITE"] = "一般網站功能";
$MESS["MAIN_SC_GOT_ERRORS"] = "Intranet有錯誤。 <a href= \"#link# \ \">檢查和維修</a>";
$MESS["MAIN_SC_MAIL_INTEGRATION"] = "外部電子郵件帳戶集成還可以，但是沒有一個用戶配置了集成設置。";
$MESS["MAIN_SC_MAIL_IS_NOT_INSTALLED"] = "未安裝郵件模塊。";
$MESS["MAIN_SC_MAIL_TEST"] = "電子郵件通知";
$MESS["MAIN_SC_MBSTRING_SETTIGNS_DIFFER"] = "<i>/bitrix/.settings.php </i>（utf_mode）和<i>/bitrix/php_interface/dbconn.php </i>（bx_utf）中的mbString首選項是不同的。";
$MESS["MAIN_SC_MCRYPT"] = "加密功能";
$MESS["MAIN_SC_METHOD_NOT_SUP"] = "該服務器不支持方法＃方法＃。";
$MESS["MAIN_SC_NOT_AVAIL"] = "不可用";
$MESS["MAIN_SC_NOT_SUPPORTED"] = "服務器不支持此功能。";
$MESS["MAIN_SC_NO_ACCESS"] = "無法訪問BitRix24服務器。更新和Bitrix雲服務不可用。";
$MESS["MAIN_SC_NO_CONFLICT"] = "沒有衝突。";
$MESS["MAIN_SC_NO_CONNECTTO"] = "無法連接到＃主機＃";
$MESS["MAIN_SC_NO_EXTERNAL_ACCESS_"] = "此功能不可用，因為Intranet在外部無法訪問。";
$MESS["MAIN_SC_NO_EXTERNAL_ACCESS_MOB"] = "此功能不可用，因為Intranet與移動應用程序相比是無法訪問的。";
$MESS["MAIN_SC_NO_EXTERNAL_CONNECT_WARN"] = "無法在外部連接到Intranet。移動應用程序將無法運行。";
$MESS["MAIN_SC_NO_EXTRANET_CONNECT"] = "由於Interanet通過Internet在外部無法訪問，因此外部集無法正常運行。";
$MESS["MAIN_SC_NO_IM"] = "未安裝Web Messenger模塊。";
$MESS["MAIN_SC_NO_LDAP_INTEGRATION"] = "AD服務器集成未配置。";
$MESS["MAIN_SC_NO_LDAP_MODULE"] = "未安裝LDAP模塊。";
$MESS["MAIN_SC_NO_NTLM"] = "當前連接不使用NTLM身份驗證";
$MESS["MAIN_SC_NO_PULL_EXTERNAL_2"] = "建立了與您的Bitrix24的外部連接。但是，推動服務器讀取端口不可用。即時消息傳遞將在移動應用中不可用。";
$MESS["MAIN_SC_NO_PUSH_STREAM_2"] = "推動服務器未在推送和拉模塊設置中配置。該服務器需要實時顯示提要評論。";
$MESS["MAIN_SC_NO_PUSH_STREAM_CONNECTION"] = "無法連接到nginx-push-stream模塊以發送即時消息";
$MESS["MAIN_SC_NO_PUSH_STREAM_CONNECTION_2"] = "無法連接到推服務服務器發送即時消息";
$MESS["MAIN_SC_NO_PUSH_STREAM_VIDEO_2"] = "推動服務器未在推送和拉模塊設置中配置。該服務器需要進行視頻通話。";
$MESS["MAIN_SC_NO_REST_MODULE"] = "未安裝其餘模塊。";
$MESS["MAIN_SC_NO_SOCIAL_MODULE"] = "沒有安裝社交網絡模塊。";
$MESS["MAIN_SC_NO_SOCIAL_SERVICES"] = "在社交網絡模塊設置中沒有配置的社交網絡服務。";
$MESS["MAIN_SC_NO_SOCIAL_SERVICES_24NET"] = "Bitrix24.NET集成未在社會服務模塊設置中配置。";
$MESS["MAIN_SC_NO_SUB_CONNECTION_2"] = "無法連接到推動服務器以讀取即時消息";
$MESS["MAIN_SC_NO_WEBDAV_MODULE"] = "未安裝文檔庫模塊。";
$MESS["MAIN_SC_NTLM_SUCCESS"] = "NTLM身份驗證還可以，當前用戶：";
$MESS["MAIN_SC_OPTION_SWITCHED_OFF"] = "NTLM身份驗證是在LDAP模塊設置中啟用的。";
$MESS["MAIN_SC_PATH_PUB"] = "在推送和拉模塊設置中指定的不正確發布路徑";
$MESS["MAIN_SC_PATH_SUB"] = "讀取URL的消息在推送和拉模塊設置中是不正確的。";
$MESS["MAIN_SC_PERFORM"] = "表現";
$MESS["MAIN_SC_PERF_TEST"] = "服務器性能測試";
$MESS["MAIN_SC_PULL_NOT_REGISTERED"] = "在Bitrix上註冊的錯誤提供了推動服務器";
$MESS["MAIN_SC_PULL_UNSUPPORTED_VERSION"] = "推送模塊設置指定了推動服務器的棄用版本。您必須更新推送服務器。 <a href= \"https://training.bitrix24.com/support/training/course/index.php?course_id = 26& lesson_id = 21596 \ \">了解更多。</a>";
$MESS["MAIN_SC_PUSH_INCORRECT"] = "nginx-push-stream模塊函數錯誤。";
$MESS["MAIN_SC_REAL_TIME"] = "實時通信和視頻通話";
$MESS["MAIN_SC_REQUIRED_MODS_DESC"] = "檢查是否安裝了所有必需的模塊，並且最重要的設置是正確的。否則，Intranet可能無法正常運行。";
$MESS["MAIN_SC_SEARCH_INCORRECT"] = "文檔內容索引無法正常運行。";
$MESS["MAIN_SC_SITE_GOT_ERRORS"] = "該網站有錯誤。 <a href= \"#link# \ \">檢查和維修</a>";
$MESS["MAIN_SC_SOME_WARNING"] = "警告";
$MESS["MAIN_SC_SSL_NOT_VALID"] = "服務器的SSL證書無效";
$MESS["MAIN_SC_STREAM_DISABLED_2"] = "推動服務器未在推送和拉模塊設置中配置。";
$MESS["MAIN_SC_SYSTEST_LOG"] = "系統檢查日誌";
$MESS["MAIN_SC_TEST_COMMENTS"] = "現場評論";
$MESS["MAIN_SC_TEST_DOCS"] = "在線編輯文檔和Microsoft Office在線";
$MESS["MAIN_SC_TEST_FAST_FILES"] = "Bitrix24.Drive。快速文件操作";
$MESS["MAIN_SC_TEST_IS_INCORRECT"] = "該測試未能產生正確的結果。";
$MESS["MAIN_SC_TEST_LDAP"] = "Active Directory集成";
$MESS["MAIN_SC_TEST_MAIL_INTEGRATION"] = "內部公司郵件集成";
$MESS["MAIN_SC_TEST_MAIL_PUSH"] = "中繼電子郵件到活動流";
$MESS["MAIN_SC_TEST_MOBILE"] = "Bitrix24移動應用";
$MESS["MAIN_SC_TEST_NTLM"] = "Windows NTLM身份驗證";
$MESS["MAIN_SC_TEST_PUSH"] = "移動設備的通知（推送通知）";
$MESS["MAIN_SC_TEST_PUSH_SERVER"] = "推動服務器";
$MESS["MAIN_SC_TEST_REST"] = "REST API使用";
$MESS["MAIN_SC_TEST_RESULT"] = "測試結果：";
$MESS["MAIN_SC_TEST_SEARCH_CONTENTS"] = "搜索文檔內容";
$MESS["MAIN_SC_TEST_SOCNET_INTEGRATION"] = "社會服務整合";
$MESS["MAIN_SC_TEST_SSL1"] = "建立了安全的HTTPS連接，但無法驗證SSL證書，因為未從Bitrix Server下載認證機構列表";
$MESS["MAIN_SC_TEST_SSL_WARN"] = "無法牢固地連接。您可能會遇到與外部應用程序交流的問題。";
$MESS["MAIN_SC_TEST_VIDEO"] = "視頻通話";
$MESS["MAIN_SC_UNKNOWN_ANSWER"] = "＃主機＃的未知響應";
$MESS["MAIN_SC_WARNINGS"] = "移動通知";
$MESS["MAIN_SC_WARN_EXPAND_SESSION"] = "如果安裝了即時Messenger模塊，請在<a href='/bitrix/admin/settings.php中禁用會話保持活躍功能，以減少服務器負載。";
$MESS["MAIN_SC_WINDOWS_ENV"] = "Windows環境集成";
$MESS["MAIN_TMP_FILE_ERROR"] = "無法創建臨時測試文件";
$MESS["MAIN_WRONG_ANSWER_PULL"] = "推送服務器以未知的響應回复。";
$MESS["PHP_VER_NOTIFY"] = "注意力！您沒有接收系統或安全更新，因為您的PHP版本＃CUR＃已過時。請將您的PHP更新為版本＃REQ＃。確保您已經閱讀了此<a href= \"https://helpdesk.bitrix24.com/open/17347208/forder/17347208/ \ \"> helpdesk page </a>在更新PHP之前。";
$MESS["SC_BX_UTF"] = "在<i>/bitrix/php_interface/dbconn.php </i>中使用以下代碼：
<code> define（'bx_utf'，true）; </code>";
$MESS["SC_BX_UTF_DISABLE"] = "不得定義BX_UTF常數";
$MESS["SC_CACHED_EVENT_WARN"] = "找到了緩存的電子郵件發送數據可能是由於錯誤引起的。嘗試清除緩存。";
$MESS["SC_CHARSET_CONN_VS_RES"] = "連接charset（＃conn＃）與結果charset（＃res＃）不同。";
$MESS["SC_CHECK_B"] = "查看";
$MESS["SC_CHECK_FILES"] = "檢查文件權限";
$MESS["SC_CHECK_FILES_ATTENTION"] = "注意力！";
$MESS["SC_CHECK_FILES_WARNING"] = "文件權限檢查腳本可以在服務器上生成大量負載。";
$MESS["SC_CHECK_FOLDER"] = "文件夾檢查";
$MESS["SC_CHECK_FULL"] = "全部檢查";
$MESS["SC_CHECK_KERNEL"] = "內核檢查";
$MESS["SC_CHECK_TABLES_ERRORS"] = "數據庫表具有＃Val＃編碼錯誤（S），＃Val1＃可以自動修復。";
$MESS["SC_CHECK_TABLES_STRUCT_ERRORS"] = "數據庫結構中存在錯誤。總問題：＃val＃。 ＃val1＃可以立即修復。";
$MESS["SC_CHECK_TABLES_STRUCT_ERRORS_FIX"] = "這些問題已經解決，但是某些字段（＃Val＃）具有不同的類型。您將必須通過查看網站檢查日誌來手動修復它們。";
$MESS["SC_CHECK_UPLOAD"] = "上傳文件夾檢查";
$MESS["SC_COLLATE_WARN"] = "“＃＃table＃＆quot”的整理值（＃val0＃）與數據庫值（＃Val1＃）不同。";
$MESS["SC_CONNECTION_CHARSET"] = "連接字符集";
$MESS["SC_CONNECTION_CHARSET_NA"] = "由於連接編碼錯誤而導致的驗證失敗。";
$MESS["SC_CONNECTION_CHARSET_WRONG"] = "數據庫連接charset必須為＃val＃，當前值是＃val1＃。";
$MESS["SC_CONNECTION_CHARSET_WRONG_NOT_UTF"] = "數據庫連接charset不得是UTF-8，當前值是：＃val＃。";
$MESS["SC_CONNECTION_COLLATION_WRONG_UTF"] = "數據庫連接整理必須是utf8_unicode_ci，當前值是＃val＃。";
$MESS["SC_CRON_WARN"] = "常數bx_crontab_support在/bitrix/php_interface/dbconn.php中定義，這需要使用cron運行代理。";
$MESS["SC_DATABASE_CHARSET_DIFF"] = "數據庫charset（＃val1＃）與連接charset（＃val0＃）不匹配。";
$MESS["SC_DATABASE_COLLATION_DIFF"] = "數據庫整理（＃val1＃）與連接平面（＃Val0＃）不匹配。";
$MESS["SC_DB_CHARSET"] = "數據庫charset";
$MESS["SC_DB_ERR"] = "問題數據庫版本：";
$MESS["SC_DB_ERR_INNODB_STRICT"] = "Innodb_strict_mode =＃值＃，需要關閉";
$MESS["SC_DB_ERR_MODE"] = "MySQL中的SQL_Mode變量必須為空。當前值：";
$MESS["SC_DB_MISC_CHARSET"] = "表＃tbl＃charset（＃t_char＃）與數據庫charset（＃charset＃）不匹配。";
$MESS["SC_DELIMITER_ERR"] = "當前定界符：“＃val＃＆quot”。“。”。是必須的。";
$MESS["SC_ERROR0"] = "錯誤！";
$MESS["SC_ERROR1"] = "測試未能完成。";
$MESS["SC_ERRORS_FOUND"] = "那裡的錯誤";
$MESS["SC_ERRORS_NOT_FOUND"] = "未檢測到的錯誤";
$MESS["SC_ERR_CONNECT_MAIL001"] = "無法連接郵件服務器郵件-001.bitrix24.com";
$MESS["SC_ERR_CONN_DIFFER"] = "在.settings.php和dbconn.php中是不同的。";
$MESS["SC_ERR_DNS"] = "無法獲得域＃域＃的MX記錄";
$MESS["SC_ERR_DNS_WRONG"] = "DNS配置不正確。只有一個MX記錄必須在那裡：Mail-001.bitrix24.com（當前：＃domain＃）。";
$MESS["SC_ERR_FIELD_DIFFERS"] = "表＃表＃：字段＃字段＃\“＃cur＃\”不匹配描述\“＃新＃\”";
$MESS["SC_ERR_NO_FIELD"] = "表＃表＃缺少字段＃字段＃";
$MESS["SC_ERR_NO_INDEX"] = "索引＃索引＃從表＃表＃缺少";
$MESS["SC_ERR_NO_INDEX_ENABLED"] = "全文搜索索引＃索引＃未啟用表＃表＃";
$MESS["SC_ERR_NO_SETTINGS"] = "配置文件 /bitrix/.settings.php找不到";
$MESS["SC_ERR_NO_TABLE"] = "表＃表＃不存在。";
$MESS["SC_ERR_NO_VALUE"] = "沒有系統記錄＃SQL＃用於表＃表＃";
$MESS["SC_ERR_PHP_PARAM"] = "參數＃param＃是＃cur＃，但是＃req＃是必需的。";
$MESS["SC_ERR_TEST_MAIL_PUSH"] = "無法連接到電子郵件服務器的＃域＃";
$MESS["SC_FIELDS_COLLATE_WARN"] = "字段＆quot＃field＃＆quot'結果“＃＃表＃” （＃val1＃）在數據庫（＃Val1＃）中不匹配。";
$MESS["SC_FILES_CHECKED"] = "檢查的文件：<b> #num＃</b> <br>當前路徑：<i> #path＃</i>";
$MESS["SC_FILES_FAIL"] = "無法閱讀或寫作（前10）：";
$MESS["SC_FILES_OK"] = "所有檢查的文件都可以讀取和寫作。";
$MESS["SC_FILE_EXISTS"] = "文件已存在：";
$MESS["SC_FIX"] = "使固定";
$MESS["SC_FIX_DATABASE"] = "修復數據庫錯誤";
$MESS["SC_FIX_DATABASE_CONFIRM"] = "系統現在將嘗試修復數據庫錯誤。這一行動可能是危險的。在進行之前，請創建數據庫備份副本。

繼續？";
$MESS["SC_FIX_MBSTRING"] = "維修配置";
$MESS["SC_FIX_MBSTRING_CONFIRM"] = "注意力！

這將更改配置文件。如果操作失敗，則您的網站只能從Web託管控制面板中恢復。

繼續？";
$MESS["SC_FULL_CP_TEST"] = "完整的系統檢查";
$MESS["SC_GR_EXTENDED"] = "高級功能";
$MESS["SC_GR_FIX"] = "修正錯誤";
$MESS["SC_GR_MYSQL"] = "數據庫測試";
$MESS["SC_HELP"] = "幫助。";
$MESS["SC_HELP_CHECK_ACCESS_DOCS"] = "要使用Google文檔或MS Office在線查看或編輯文檔，創建了特殊的外部訪問URL並將其傳遞給這些服務，以獲取文檔。 URL是唯一的，一旦文檔關閉，就會立即變得無效。

此功能要求您的Intranet可以通過Internet遠程訪問。";
$MESS["SC_HELP_CHECK_ACCESS_MOBILE"] = "移動應用程序要求您的Intranet可以通過Internet遠程訪問。

該測試在Checker.internal.bitrix24.com上使用特殊服務器，該服務器使用Web瀏覽器提供的當前BitRix24 URL嘗試與Intranet連接。與遠程服務器的連接處於活動狀態時，沒有傳輸用戶數據。

即時消息傳遞要求可以將Nginx的讀取端口連接到。端口號來自<a href= \"http://www.bitrixsoft.com/support/training/course/course/index.php?course_id = 26& lesson_id = 51444 \ \">推送和拉動</a>模塊設置。";
$MESS["SC_HELP_CHECK_AD"] = "如果您在本地網絡上設置了Windows AD或LDAP服務器，建議檢查AD是否<a href = \ \” http://www.bitrixsoft.com/support/support/training/course/index.php?course_id = 20＆Chapter_id = 04264 \“>正確配置</a>。

此功能要求安裝PHP LDAP模塊。";
$MESS["SC_HELP_CHECK_BX_CRONTAB"] = "要遷移非週期性代理並將電子郵件遷移到Cron中，請將以下常數添加到<i>/bitrix/php_interface/dbconn.php </i>：：
<code> define（'bx_crontab_support'，true）; </code>

將此常數設置為\“ true \”，系統將僅在發生擊球時運行週期性代理。現在將任務添加到cron中以執行<i>/var/www/bitrix/modules/main/tools/cron_events.php </i>每分鐘（替換<i>/var/var/www </i>）小路）。

腳本定義了常數<b> bx_crontab </b>，該腳本表明腳本被cron激活並僅運行非週期性代理。如果您錯誤地在<i> dbconn.php </i>中定義了這個常數，那麼定期代理將永遠不會運行。";
$MESS["SC_HELP_CHECK_CACHE"] = "這將檢查php進程是否可以在緩存目錄中創建<b> .tmp </b>文件，然後將其重命名為<b> .php </b>。如果不正確配置了用戶權限，一些Windows Web服務器可能無法重命名該文件。";
$MESS["SC_HELP_CHECK_CA_FILE"] = "測試嘗試連接到www.bitrixsoft.com。

許多相關雲的例程任務（CDN，雲備份，安全掃描儀等）需要此連接才能更新當前的自由空間配額和服務狀態。執行這些操作時未發送用戶信息。

然後，測試下載了SSL證書測試所需的BITRIX服務器的認證中心列表。
";
$MESS["SC_HELP_CHECK_COMPRESSION"] = "HTML壓縮減少文件大小並減少文件傳輸時間。

要減少服務器加載，請確保使用特殊的Web服務器模塊來壓縮HTML文件。

如果服務器不支持HTML頁面壓縮，則使用BITRIX壓縮模塊。請記住，此模塊<a href= \"/bitrix/admin/module_admin.php \">否則不應安裝</a>否則。";
$MESS["SC_HELP_CHECK_CONNECT_MAIL"] = "要通過Intranet通知有關新電子郵件的通知，用戶必須在Intranet用戶配置文件頁面上指定郵箱連接參數。";
$MESS["SC_HELP_CHECK_DBCONN"] = "這將檢查“配置文件” <i> dbconn.php </i>和<i> init.php </i>中的文本輸出。

即使是多餘的空間或新線，也可能導致壓縮頁面被客戶瀏覽器解開和無法讀取。

此外，可能會出現授權和驗證碼問題。";
$MESS["SC_HELP_CHECK_DBCONN_SETTINGS"] = "該測試將在<i>/bitrix/php_interface/dbconn.php </i>中指定的數據庫連接參數與<i>/bitrix/.settings.php </i>中的數據庫連接參數。
這些設置在兩個文件中都必須相同。否則，某些SQL查詢可能會將其轉移到另一個數據庫，這將帶來不可預測的後果。

新的D7內核在<i> .settings.php </i>中使用參數。由於向後兼容性問題，無法避免使用<i> dbconn.php </i>。

但是，如果<i> .settings.php </i>根本沒有指定連接參數，則新內核使用<i> dbconn.php </i>中的內核。";
$MESS["SC_HELP_CHECK_EXEC"] = "如果PHP在UNIX系統上以CGI/FASTCGI模式運行，則腳本需要執行權限，否則將不運行。
如果此測試失敗，請與您的託管TechSupport聯繫以獲取必要的文件權限，並將pontants <bx_file_permissions </b>和<bx_dir_permissions </b> in <i> dbconn.php </i> in <i> dbconn.php <i> bx_dir_permissions </b>。

如果可能的話，將PHP配置為APACHE模塊。";
$MESS["SC_HELP_CHECK_EXTRANET"] = "<a href= \"http://www.bitrixsoft.com/products/intranet/features/collaboration/collaboration/extranet.php \"> extranet </a>模塊要求您的Interanet可以通過Internet向外訪問。

如果您不需要此模塊提供的功能，則只需<a href= \"/bitrix/admin/module_admin.php \">卸載它</a>即可。";
$MESS["SC_HELP_CHECK_FAST_DOWNLOAD"] = "快速文件下載使用<a href= \"http://wiki.nginx.org/x-accel \"> nginx的內部重定向</a>。使用PHP調用檢查文件訪問權限，而實際下載由NGINX處理。

一旦提出請求，PHP資源就會被釋放以處理隊列中的後續請求。這可以顯著改善Intranet性能，並在通過Bitrix訪問時提高文件下載速度。驅動器，文檔庫或從活動流帖子下載附件時。

在<a href= \"/bitrix/admin/settings.php?mid=main \">內核設置</a>中啟用此選項。 <a href= \"http://www.bitrixsoft.com/products/virtual_appliance/ \ \"> bitrix虛擬設備</a>默認支持快速文件下載。

";
$MESS["SC_HELP_CHECK_GETIMAGESIZE"] = "當您添加閃存對象時，可視編輯器需要獲取對像大小並調用標準的PHP函數<b> getimagesize </b>它需要<b> zlib </b>擴展。如果將<b> zlib </b>擴展名作為模塊安裝時，則在調用壓縮閃存對象時可能會失敗。它需要靜態建造。

要解決此問題，請聯繫您的託管TechSupport。";
$MESS["SC_HELP_CHECK_HTTP_AUTH"] = "該測試將使用HTTP標頭髮送授權數據，然後嘗試使用remote_user Server變量（或redirect_remote_user）來解決數據。與第三方軟件集成需要HTTP授權。

如果PHP以CGI/FASTCGI模式運行（請聯繫您的託管以獲取詳細信息），則Apache Server將需要MOD_REWRITE模塊和以下規則.htaccess：
<b>重寫。

如果可能的話，將PHP配置為APACHE模塊。";
$MESS["SC_HELP_CHECK_INSTALL_SCRIPTS"] = "在系統恢復或安裝之後，用戶可能有時會忘記刪除安裝腳本（Restore.php，BitrixSetup.php）。這可能成為嚴重的安全威脅，並導致網站劫持。如果您忽略了Autodelete警告，請記住手動刪除這些文件。";
$MESS["SC_HELP_CHECK_LOCALREDIRECT"] = "保存控制面板的表單（即，單擊“保存”或“應用”）之後，將客戶端重定向到初始頁面。這樣做是為了防止重複的表單帖子，如果用戶刷新頁面可能會發生。僅當正確定義Web服務器上的某些關鍵變量並允許重寫HTTP標頭時，重定向才能成功。

如果某些服務器變量在<i> dbconn.php中重新定義，則測試將使用重新定義。換句話說，重定向完全模擬現實生活中的情況。";
$MESS["SC_HELP_CHECK_MAIL"] = "這將使用標準PHP函數\“ Mail \”將電子郵件發送到hosting_test@bitrixsoft.com。存在一個特殊的郵箱，以使測試條件盡可能現實。

此測試將站點檢查腳本作為測試消息發送，<b>永遠不會發送任何用戶數據</b>。

請注意，該測試不會驗證消息傳遞。交付給其他郵箱也無法驗證。

如果電子郵件發送時間超過一秒鐘，則服務器性能可能會遇到嚴重的退化。請與您的託管TechSupport聯繫，以便他們配置使用Spooler的延遲電子郵件發送。

另外，您可以使用Cron發送電子郵件。為此，添加<code> define（'bx_crontab_support'，true）; </code>到dbconn.php。然後，將cron設置為執行php/var/www/bitrix/modules/main/tools/cron_events.php </i>每一分鐘（用您的網站root替換<i>/var/www </i>） 。

如果撥打郵件（）失敗，則無法使用常規方法從服務器發送電子郵件。

如果您的託管提供商提供替代電子郵件發送服務，則可以通過調用函數\“ custom_mail \”來使用它們。在<i>/bitrix/php_interface/init.php </i>中定義此功能。如果系統找到此功能定義，則將使用後者使用後者，而不是使用相同的輸入參數的PHP \“ mail \”。";
$MESS["SC_HELP_CHECK_MAIL_BIG"] = "這將通過發送與上一個文本（網站檢查腳本）相同的消息來測試批量電子郵件。此外，將newline字符插入到消息主題中，並向noreply@bitrixsoft.com插入該消息。

如果將服務器配置不正確，則可能不會發送此類消息。

如果出現任何問題，請聯繫您的託管提供商。如果要在本地計算機上運行系統，則必須手動配置服務器。";
$MESS["SC_HELP_CHECK_MAIL_B_EVENT"] = "數據庫表B_Event存儲網站的電子郵件隊列並記錄電子郵件發送事件。如果某些消息未能發送，則可能的原因是無效的收件人地址，錯誤的電子郵件模板參數或服務器的電子郵件子系統。";
$MESS["SC_HELP_CHECK_MAIL_PUSH"] = "<a href= \之https://helpdesk.bitrix24.com/open/1612393/1612393/xtart target=_blank>消息中繼</a>功能將發布來自電子郵件的消息到活動流的消息使參與可能參與討論任何在您的Bitrix24上沒有帳戶的用戶。

您將必須正確配置DNS，並使您的Bitrix24外部訪問以使用此功能。";
$MESS["SC_HELP_CHECK_MBSTRING"] = "多語言支持需要MBSTRING模塊。

網站編碼需要指定為Default_charset參數的值。例如：

<b> default_charset = utf-8 </b>

配置錯誤將導致許多問題：文本將隨意截斷，XML導入，更新系統將被打破等。

將此代碼添加到<i>/bitrix/php_interface/dbconn.php </i>中以在您的網站上啟用UTF-8：
<code> define（'bx_utf'，true）; </code>
並將此代碼添加到<i>/bitrix/.settings.php </i>：
<code>'utf_mode'=>
  大批 （
    'value'=> true，
    'readonly'=> true，
  ），</code>";
$MESS["SC_HELP_CHECK_MEMORY_LIMIT"] = "該測試創建了一個孤立的PHP過程，以生成逐漸增加大小的變量。最後，這將產生PHP進程可用的內存量。

PHP通過設置<b> memory_limit </b>參數來定義php.ini中的內存限制。但是，這可能被共享主機所覆蓋。您不應該相信此參數。

該測試嘗試使用代碼來增加<b> memory_limit </b>的值：
<code> ini_set（“ memory_limit” 512m＆quot;

如果當前值小於此，則將此代碼行添加到<i>/bitrix/php_interface/dbconn.php </i>。
";
$MESS["SC_HELP_CHECK_METHOD_EXISTS"] = "在某些PHP版本上調用<i> method_exists <i> method_exists時，該腳本會失敗。請參閱此討論以獲取更多信息：<a href='http://bugs.php.net/bug.php?id=51425'target=_blank> http://bugs.php.net/bug.php ？ id = 51425 </a>
安裝其他PHP版本以解決問題。";
$MESS["SC_HELP_CHECK_MYSQL_BUG_VERSION"] = "有已知的MySQL版本，其中包含可能導致網站故障的錯誤。
<b> 4.1.21 </b>  - 在某些條件下排序功能不正確地工作；
<b> 5.0.41 </b>  - 存在函數工作不正確；搜索功能返回不正確的結果；
<b> 5.1.34 </b>  - 默認情況下，auto_increment步驟為2，而需要1個。
<b> 5.1.66 </b>  - 返回不正確的論壇主題帖子計數，可能會破壞麵包屑導航。

如果安裝了這些版本之一，則需要更新MySQL。";
$MESS["SC_HELP_CHECK_MYSQL_CONNECTION_CHARSET"] = "該測試將檢查系統在將數據發送到MySQL Server時使用的Charset並進行整理。

如果您的網站使用<i> utf -8 </i>，則必須將Charset設置為<i> utf8 </i>和Collat​​ion -utf8_unicode_ci </i>。如果網站使用<i> iso-8859-1 </i>，該連接必須使用相同的字符集。

要更改連接charset（例如，將其設置為UTF-8），請將以下代碼添加到<i>/bitrix/php_interface/after_connect_d7.php </i>：：
<code> \ $ connection = BitRix \\ Main \\ application :: getConnection（）;
\ $ connection-> queryExecute（'set name＆quot'utf8''）; </code>

要更改整理，請在CharSet聲明</b>之後添加代碼<b>：
<code> \ $ connection-> queryExecute（'SET Collat​​ion_Connection =＆quort; utf8_unicode_ci''）; </code>>

<b>注意！</b>一旦您更改了新值，請確保您的網站正常運行。
";
$MESS["SC_HELP_CHECK_MYSQL_DB_CHARSET"] = "此測試檢查數據庫CHARSET和整理是否匹配連接的連接。 MySQL使用這些偏好來創建新表。

如果當前用戶具有數據庫寫入權限（ALTER數據庫），則可以自動修復此類錯誤。
";
$MESS["SC_HELP_CHECK_MYSQL_MODE"] = "參數<i> sql_mode </i>指定MySQL操作模式。請注意，它可能會接受與Bitrix解決方案不兼容的值。將以下代碼添加到<i>/bitrix/php_interface/after_connect_d7.php </i>設置默認模式：
<code> \ $ connection = BitRix \\ Main \\ application :: getConnection（）;
\ $ connection-＆gt; queryExecute（'set s sql_mode =''';）;
\ $ connection-＆gt; queryExecute（'設置Innodb_strict_mode = 0＆quot;）; </code>

請注意，您可能需要在MySQL 8.0.26上具有數據庫用戶特權session_variables_admin且更新。如果您當前的特權不足，則必須聯繫數據庫管理員或編輯MySQL配置文件。";
$MESS["SC_HELP_CHECK_MYSQL_TABLE_CHARSET"] = "所有表和字段的字符集必須與數據庫Charset匹配。如果任何表格的char集都呈現效果，則必須使用SQL命令手動修復它。

表平面也應匹配數據庫校正。如果正確配置了炭框，則將自動修復錯配的整理。

<b>注意！</b>在更改Charset之前，請始終創建數據庫的完整備份副本。";
$MESS["SC_HELP_CHECK_MYSQL_TABLE_STATUS"] = "該測試使用常規的MySQL表格檢查機制。如果測試找到一個或多個損壞的表，將提示您修復它們。";
$MESS["SC_HELP_CHECK_MYSQL_TABLE_STRUCTURE"] = "模塊安裝軟件包始終包含有關其使用數據庫表結構的信息。更新時，模塊安裝程序可能會更改表結構和模塊文件（腳本）。

如果模塊腳本與當前表結構不匹配，則肯定會帶來運行時錯誤。

可能有新的數據庫索引已添加到新的分發軟件包中，但在更新中不包含。這是因為更新系統以包含新索引將花費太長時間，並且最終失敗了。

網站檢查將診斷已安裝的模塊<b>創建和/或更新丟失的索引和字段以確保數據完整性。但是，如果字段類型已更改，則必須手動查看日誌。";
$MESS["SC_HELP_CHECK_MYSQL_TIME"] = "該測試將數據庫系統時間與Web服務器時間進行比較。如果將這兩個安裝在各個機器上，這兩個可能會變得雜亂無章，但最常見的原因是時區配置不正確。

在<i>/bitrix/php_interface/dbconn.php </i>中設置PHP時區：
<code> date_default_timezone_set（'歐洲/倫敦）; </code>（使用您的地區和城市）。

通過將以下代碼添加到<i>/bitrix/php_interface/after_connect_d7.php </i>：
<code> \ $ connection = BitRix \\ Main \\ application :: getConnection（）;
\ $ connection-> queryExecute（“設置local time_zone ='＆quot; date（'p'）。; quot;'）; </code>

請參閱http://en.wikipedia.org/wiki/list_of_tz_database_time_time_zones，以找到適合您所在地區和城市的正確標準值。";
$MESS["SC_HELP_CHECK_NTLM"] = "<a href= \之http://en.wikipedia.org/wiki/single_sign-on \ \">單個簽名</a>身份驗證技術要求以特殊方式配置Web服務器，並啟用NTLM Authartication並在Intranet上配置。

在Linux上設置NTLM絕對不是一項瑣碎的任務。 <a href= \之http://www.bitrixsoft.com/products/virtual_appliance/ \ \"> Bitrix虛擬設備</a>包括自版4.2以來完全配置的NTLM身份驗證。";
$MESS["SC_HELP_CHECK_PCRE_RECURSION"] = "默認情況下，參數<i> pcre.recursion_limit </i>設置為100000。如果遞歸的記憶比系統堆棧大小所能提供的更多（通常是8 MB），則PHP會在復雜的正則表達式上出現錯誤，以顯示A分段故障</i>錯誤消息。

要禁用堆棧尺寸限制，請編輯Apache啟動腳本：<code> ulimit -s unlimited </code>
在FreeBSD上，您將不得不使用該選項重建PCRE-Disable-stack-for-Recursion

另外，您可以將<i> pcre.recursion_limit </i>的值降低至1000或更少。該解決方案還適用於基於Windows的安裝。

這將防止PHP災難性的失敗，但可能導致字符串功能行為的不一致：例如，論壇可能開始顯示空的帖子。";
$MESS["SC_HELP_CHECK_PERF"] = "服務器性能評估由<a href= \"http://www.bitrixsoft.com/support/training/course/course/index.php?course_id = 20＆chapter_id = 04955 \ \"> performance monitor </a>。

顯示服務器每秒可以使用的空頁數。該值是生成僅包含強制內核包含調用的空頁面所需時間的倒數。

參考<a href= \"http://www.bitrixsoft.com/products/virtual_appliance/ \ \"> Bitrix Virtual Appliance </a>通常得分30分。

在未加載的機器上獲得不良價值表示配置不佳。在高負載期間下降的降低可能是由於硬件資源不足所致。";
$MESS["SC_HELP_CHECK_PHP_MODULES"] = "這將檢查系統所需的PHP擴展。如果缺少擴展，則顯示沒有這些擴展的模塊。

要添加缺少的PHP擴展名，請聯繫您的託管TechSupport。如果您在本地機器上運行系統，則必須手動安裝它們；請參閱php.net上可用的文檔。";
$MESS["SC_HELP_CHECK_PHP_SETTINGS"] = "這將檢查php.ini中定義的關鍵參數。顯示其值將導致系統故障的參數。您將在php.net上找到詳細的參數描述。";
$MESS["SC_HELP_CHECK_POST"] = "這將發送帶有大量參數的發布請求。 \“ suhosin \”這樣的一些服務器保護器軟件可能會阻止詳細的請求。這可能會阻止信息塊元素被保存，這絕對是一個問題。";
$MESS["SC_HELP_CHECK_PULL_COMMENTS"] = "為了在所有讀者立即使用AvTivity流中發表評論，推動和拉模塊可能需要其他配置。也就是說，您的NGINX實例需要安裝Push-Stream-module，然後在<a a href = \'http://www.bitrixsoft.com/support/support/training/course/index.php?course_id = 26 http ://www.bitrixsoft.com.course_id=26&ellson_id_id== 5144 \“>推和拉</a>模塊設置。

<a href= \之http://www.bitrixsoft.com/products/virtual_appliance/index.php \"> Bitrix Virtual Appliance </a>自版4.2以來，完全預先配置以支持此功能。
";
$MESS["SC_HELP_CHECK_PULL_STREAM"] = "<a href= \之http://www.bitrixsoft.com/support/training/course/course/index.php?course_id = 26＆lesson_id = 5144 \ \"> push and luck </a>模塊需要您的服務器支持此功能此功能。

該模塊處理向Web Messenger和移動應用程序的即時消息的交付。它也用於更新活動流。

<a href= \之http://www.bitrixsoft.com/products/virtual_appliance/ \ \"> Bitrix Virtual Appliance </a>自版4.2以來支持此模塊。
";
$MESS["SC_HELP_CHECK_PUSH_BITRIX"] = "<a href= \"http://www.bitrixsoft.com/support/training/course/course/index.php?course_id = 26& lesson_id = 5144 \ \"> push and luck </a>模塊</a>模塊處理即時消息（拉動），並將推送通知發送到移動設備（<a href= \"http://www.bitrixsoft.com/products/intranet/features/bitrixmobile.php \"> bitrixmobile.php \"> bitrix Mobile Application </a>）。

使用安全（https）Bitrix消息傳遞中心https://cloud-messaging.bitrix24.com執行向Apple和Android設備發送通知。

您的Intranet需要訪問該服務器，以便按設計工作。
";
$MESS["SC_HELP_CHECK_REST"] = "需要重新集成外部應用程序並運行許多BitRix24.Market應用程序。要將自己的應用程序集成到BITRIX24中，請關注<a href= \"https://training.bitrix24.com/rest_help/xhelp/xtart target = \"_blank \">指南</a>。";
$MESS["SC_HELP_CHECK_SEARCH"] = "該系統可以從開箱即用的文檔中以打開的XML格式（Microsoft Office 2007中引入）搜索文本。要支持其他文件格式，請在Intranet Module設置中指定<a href= \"/bitrix/admin/settings.php？mid=intranet \">的路徑</a>。否則，系統將只能搜索文件名。

<a href= \"http://www.1c-bitrix.ru/products/vmbitrix/index.php \"> bitrix virtual Appliance </a>默認支持它。";
$MESS["SC_HELP_CHECK_SECURITY"] = "像PHP的Suhosin一樣，Apache的Mod_security模塊旨在保護網站免受黑客攻擊，但最終它只是防止了正常的用戶操作。建議您使用標準\“主動保護\”模塊而不是mod_security。";
$MESS["SC_HELP_CHECK_SERVER_VARS"] = "這將檢查服務器變量。

HTTP_HOST的值源自當前虛擬主機（域）。一些瀏覽器無法將Cookie保存用於無效的域名，這將導致Cookie授權故障。";
$MESS["SC_HELP_CHECK_SESSION"] = "這將檢查服務器是否能夠使用會話存儲數據。這是保留命中之間的授權所必需的。

如果服務器上沒有安裝會話支持，則該測試將失敗，如果php.ini中指定了無效的會話目錄，或者僅讀取此目錄。";
$MESS["SC_HELP_CHECK_SESSION_UA"] = "這還將測試會話存儲功能，但不設置<i> <i>用戶代理</i> http標頭。

許多外部應用程序和附加組件沒有設置此標頭：文件和照片上傳器，WebDav客戶端等。

如果測試失敗，則最可能的問題是<b> suhosin </b> PHP模塊的配置不正確。";
$MESS["SC_HELP_CHECK_SITES"] = "驗證一般多站點參數。如果網站指定根目錄路徑（僅對於存在在不同域上的網站需要），則該目錄必須包含符號鏈接到Writable \“ Bitrix \”文件夾。

所有共享相同Bitrix系統實例的網站都必須使用相同的編碼：UTF-8或單個字節。";
$MESS["SC_HELP_CHECK_SOCKET"] = "這將設置Web服務器以建立與自身的連接，以驗證網絡功能和其他後續測試所需的連接。

如果此測試失敗，則無法執行需要兒童PHP過程的後續測試。此問題通常是由防火牆，IP訪問限製或HTTP/NTLM授權引起的。執行測試時禁用這些功能。";
$MESS["SC_HELP_CHECK_SOCKET_SSL"] = "始終使用<a href= \"http://en.wikipedia.org/wiki/https \"> https </a>協議建立加密連接。需要有效的SSL證書以確保連接真正安全。

如果證書已由發行當局進行驗證，並且由要在其上使用的網站所有，則有效。您通常可以從託管公司購買證書。

如果您在HTTPS連接上使用自我發行的證書，則在連接WebDav驅動器或與Microsoft Outlook進行通信時，您的訪問者可能會使用外部軟件遇到問題。
";
$MESS["SC_HELP_CHECK_SOCNET"] = "為了從社交資源接收更新，必須配置社交網站集成模塊，為將要使用的每個服務提供身份驗證鍵。";
$MESS["SC_HELP_CHECK_TURN"] = "視頻通話要求相關用戶的瀏覽器可以相互連接。如果呼叫者坐在不同的網絡上（例如，在不同位置的辦公室中），並且無需直接連接，則您將需要特殊的轉向服務器來建立連接。

Bitrix24在turn.calls.bitrix24.com上免費提供預配置的轉向服務器。

另外，您可以設置自己的服務器並在Web Messenger模塊設置中指定服務器URL。";
$MESS["SC_HELP_CHECK_UPDATE"] = "這將嘗試使用內核模塊當前設置建立與更新服務器的測試連接。如果無法建立連接，您將無法安裝更新或激活試用版。

最常見的原因是不正確的代理設置，防火牆限製或無效的服務器網絡參數。";
$MESS["SC_HELP_CHECK_UPLOAD"] = "這將嘗試連接到Web服務器，並以文件作為文件發送大量二進制數據。然後，服務器將將接收到的數據與原始示例進行比較。如果出現問題，則可能是由php.ini <i> php.ini </i>禁止二進制數據傳輸的某個參數引起的，也可能是由無法訪問的臨時文件夾（或<i>/bitrix/ tmp </i>）引起的。

如果出現問題，請聯繫您的託管提供商。如果要在本地計算機上運行系統，則必須手動配置服務器。";
$MESS["SC_HELP_CHECK_UPLOAD_BIG"] = "這將上傳大型二進製文件（超過4MB）。如果此測試在上一項成功時失敗，則問題可能是php.ini（<b> post_max_size </b>或<b> upload_max_filesize </b>）的限制。使用phpinfo獲取當前值（設置 - 工具 - 系統管理 -  PHP設置）。

磁盤空間不足也可能導致此問題。";
$MESS["SC_HELP_CHECK_UPLOAD_RAW"] = "在POST請求的正文中發送二進制數據。但是，在服務器端可能會損壞數據，在這種情況下，基於Flash的圖像上傳器將無法正常工作。";
$MESS["SC_HELP_CHECK_WEBDAV"] = "<a href= \之http://en.wikipedia.org/wiki/webdav \"> webdav </a>是使用戶可以直接在Microsoft Office中或直接從Intranet打開，編輯和保存文檔的協議無需下載或將其從/上傳到服務器。強制性的要求是，安裝Intranet的服務器將WebDAV請求完全按照接收到，未經修改。如果服務器阻止了這些請求，則將無法進行直接編輯。

請注意，可能需要一些額外的配置<a href= \"http://www.bitrixsoft.com/support/training/course/course/index.php?course_id = 27& lesson_id = 1466#office \ \">客戶端</ a>支持直接編輯，並且無法遠程驗證它。
";
$MESS["SC_HELP_NOTOPIC"] = "抱歉，對此話題沒有幫助。";
$MESS["SC_MBSTRING_NA"] = "驗證由於UTF配置錯誤而失敗";
$MESS["SC_MB_NOT_UTF"] = "該網站以單個字節編碼運行";
$MESS["SC_MB_UTF"] = "該網站在UTF編碼中運行";
$MESS["SC_MEMORY_CHANGED"] = "測試時使用ini_set的內存_limit的值從＃Val0＃增加到＃Val1＃。";
$MESS["SC_MOD_GD"] = "GD庫";
$MESS["SC_MOD_GD_JPEG"] = "GD JPEG支持";
$MESS["SC_MOD_JSON"] = "支持JSON";
$MESS["SC_MOD_MBSTRING"] = "MBSTRING支持";
$MESS["SC_MOD_PERL_REG"] = "正則表達支持（與Perl兼容）";
$MESS["SC_MOD_XML"] = "XML支持";
$MESS["SC_MYSQL_ERR_VER"] = "MySQL＃cur＃當前已安裝，但是＃req＃是必需的。";
$MESS["SC_NOT_FILLED"] = "需要問題描述。";
$MESS["SC_NOT_LESS"] = "不少於＃Val＃M。";
$MESS["SC_NO_PROXY"] = "無法連接到代理服務器。";
$MESS["SC_NO_ROOT_ACCESS"] = "無法訪問文件夾";
$MESS["SC_NO_TMP_FOLDER"] = "臨時文件夾不存在。";
$MESS["SC_PATH_FAIL_SET"] = "網站根路徑必須為空，當前路徑是：";
$MESS["SC_PCRE_CLEAN"] = "由於系統限制，可能會錯誤地處理長文本字符串。";
$MESS["SC_PORTAL_WORK"] = "Intranet可操作性";
$MESS["SC_PORTAL_WORK_DESC"] = "Intranet可操作性檢查";
$MESS["SC_PROXY_ERR_RESP"] = "無效的代理輔助更新服務器響應。";
$MESS["SC_READ_MORE_ANC"] = "請參閱<a href= \"#link# \" target=_blank>系統檢查日誌</a>中的詳細信息。";
$MESS["SC_RUS_L1"] = "網站票";
$MESS["SC_SEC"] = "秒";
$MESS["SC_SENT"] = "在發送：";
$MESS["SC_SITE_CHARSET_FAIL"] = "混合編碼：UTF-8和非UTF-8";
$MESS["SC_SOCKET_F"] = "插座支撐";
$MESS["SC_SOCK_NA"] = "驗證由於套接字錯誤而失敗。";
$MESS["SC_START_TEST_B"] = "開始測試";
$MESS["SC_STOP_B"] = "停止";
$MESS["SC_STOP_TEST_B"] = "停止";
$MESS["SC_STRLEN_FAIL_PHP56"] = "字符串功能工作不正確。";
$MESS["SC_STRTOUPPER_FAIL"] = "字符串功能strtoupper和strtolower產生不正確的結果";
$MESS["SC_SUBTITLE_DISK"] = "檢查磁盤訪問";
$MESS["SC_SUBTITLE_DISK_DESC"] = "站點腳本必須具有對站點文件的寫入訪問權限。這是對文件管理器，文件上傳和用於保持站點內核最新的更新系統的正確運行所必需的。";
$MESS["SC_SUPPORT_COMMENT"] = "如果您在發送消息方面遇到問題，請使用我們網站上的聯繫表：";
$MESS["SC_SWF_WARN"] = "SWF對象可能無法運行。";
$MESS["SC_SYSTEM_TEST"] = "系統檢查";
$MESS["SC_TABLES_NEED_REPAIR"] = "桌子完整性損壞了，需要修復它們。";
$MESS["SC_TABLE_BROKEN"] = "表＃＃＆quot”由於內部MySQL錯誤已被破壞。自動發現將重新創建桌子。";
$MESS["SC_TABLE_CHARSET_WARN"] = "“＃＃table＃＆quot”表包含編碼不匹配數據庫編碼的字段。";
$MESS["SC_TABLE_CHECK_NA"] = "由於數據庫CHARSET錯誤而導致的驗證失敗。";
$MESS["SC_TABLE_COLLATION_NA"] = "由於表charset錯誤未檢查";
$MESS["SC_TABLE_ERR"] = "表＃val＃中的錯誤：";
$MESS["SC_TABLE_SIZE_WARN"] = "“＃＃table＃＆quot”的大小表可能太大（＃size＃m）。";
$MESS["SC_TAB_2"] = "訪問檢查";
$MESS["SC_TAB_5"] = "技術支援";
$MESS["SC_TESTING"] = "現在檢查...";
$MESS["SC_TESTING1"] = "測試...";
$MESS["SC_TEST_CONFIG"] = "配置檢查";
$MESS["SC_TEST_DOMAIN_VALID"] = "當前域是無效的（＃Val＃）。域名只能包含數字，拉丁字母和連字符。必須用一個週期（例如.com）分離第一個域級別。";
$MESS["SC_TEST_FAIL"] = "無效的服務器響應。測試無法完成。";
$MESS["SC_TEST_START"] = "開始測試";
$MESS["SC_TEST_SUCCESS"] = "成功";
$MESS["SC_TEST_WARN"] = "服務器配置報告即將收集。
如果發生錯誤，請取消選中“發送測試日誌\”選項，然後重試。";
$MESS["SC_TIK_ADD_TEST"] = "發送測試日誌";
$MESS["SC_TIK_DESCR"] = "問題描述";
$MESS["SC_TIK_DESCR_DESCR"] = "導致錯誤，錯誤描述的操作順序...";
$MESS["SC_TIK_LAST_ERROR"] = "最後一個錯誤文本";
$MESS["SC_TIK_LAST_ERROR_ADD"] = "隨附的";
$MESS["SC_TIK_SEND_MESS"] = "發信息";
$MESS["SC_TIK_SEND_SUCCESS"] = "該消息已成功發送。一段時間後，請檢查您的收件箱＃電子郵件＃以確認來自技術支持系統的消息收據。";
$MESS["SC_TIK_TITLE"] = "將消息發送到技術支持系統";
$MESS["SC_TIME_DIFF"] = "＃val＃seconds關閉時間。";
$MESS["SC_TMP_FOLDER_PERMS"] = "未足夠的權限寫入臨時文件夾。";
$MESS["SC_T_APACHE"] = "Web服務器模塊";
$MESS["SC_T_AUTH"] = "HTTP授權";
$MESS["SC_T_CACHE"] = "使用緩存文件";
$MESS["SC_T_CHARSET"] = "數據庫表charset";
$MESS["SC_T_CHECK"] = "桌子檢查";
$MESS["SC_T_CLONE"] = "通過參考傳遞對象";
$MESS["SC_T_DBCONN"] = "配置文件中的冗餘輸出";
$MESS["SC_T_DBCONN_SETTINGS"] = "數據庫連接參數";
$MESS["SC_T_EXEC"] = "文件創建和執行";
$MESS["SC_T_GETIMAGESIZE"] = "Getimagesize對SWF的支持";
$MESS["SC_T_INSTALL_SCRIPTS"] = "網站根部的服務腳本";
$MESS["SC_T_MAIL"] = "電子郵件發送";
$MESS["SC_T_MAIL_BIG"] = "大型電子郵件發送（超過64 kb）";
$MESS["SC_T_MAIL_B_EVENT"] = "檢查未輸入的消息";
$MESS["SC_T_MAIL_B_EVENT_ERR"] = "發送系統電子郵件時發生錯誤。消息未發送：";
$MESS["SC_T_MBSTRING"] = "UTF配置參數（MBSTRING和BX_UTF）";
$MESS["SC_T_MEMORY"] = "內存限制";
$MESS["SC_T_METHOD_EXISTS"] = "Method_Exists在線上調用";
$MESS["SC_T_MODULES"] = "所需的PHP模塊";
$MESS["SC_T_MYSQL_VER"] = "mysql版本";
$MESS["SC_T_PHP"] = "PHP所需參數";
$MESS["SC_T_POST"] = "發表多個參數的請求";
$MESS["SC_T_RECURSION"] = "堆棧尺寸； pcre.recursion_limit";
$MESS["SC_T_REDIRECT"] = "本地重定向（localredirect函數）";
$MESS["SC_T_SERVER"] = "服務器變量";
$MESS["SC_T_SESS"] = "會話保留";
$MESS["SC_T_SESS_UA"] = "會話保留而無需用戶";
$MESS["SC_T_SITES"] = "網站參數";
$MESS["SC_T_SOCK"] = "使用插座";
$MESS["SC_T_SQL_MODE"] = "mysql模式";
$MESS["SC_T_STRUCTURE"] = "數據庫結構";
$MESS["SC_T_TIME"] = "數據庫和Web服務器時間";
$MESS["SC_T_UPLOAD"] = "上傳文件";
$MESS["SC_T_UPLOAD_BIG"] = "超過4MB上傳文件";
$MESS["SC_T_UPLOAD_RAW"] = "使用php：//輸入上傳文件";
$MESS["SC_UPDATE_ACCESS"] = "訪問更新服務器";
$MESS["SC_UPDATE_ERROR"] = "未連接到更新服務器";
$MESS["SC_UPDATE_ERR_RESP"] = "無效的更新服務器響應。";
$MESS["SC_VER_ERR"] = "PHP版本是＃CUR＃，但是需要＃REQ＃或更高版本。";
$MESS["SC_WARN"] = "未配置";
$MESS["SC_WARNINGS_FOUND"] = "有警告，但沒有錯誤。";
$MESS["SC_WARN_DAV"] = "由於已加載模塊mod_dav/mod_dav_fs，因此禁用了WebDav。";
$MESS["SC_WARN_SECURITY"] = "加載的mod_security模塊，可能會出現一些控制面板問題。";
$MESS["SC_WARN_SUHOSIN"] = "加載的Suhosin模塊可能會出現一些控制面板問題（Suhosin.simulation =＃Val＃）。";
