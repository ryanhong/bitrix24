<?php
$MESS["ACTION"] = "動作";
$MESS["ACTIVE"] = "積極的";
$MESS["ADD_LANG"] = "添加語言＆gt;＆gt;";
$MESS["ADD_LANG_TITLE"] = "添加新語言";
$MESS["CHANGE"] = "調整";
$MESS["CHANGE_HINT"] = "更改語言設置";
$MESS["CONFIRM_DEL"] = "您確定要刪除語言嗎？";
$MESS["COPY"] = "複製";
$MESS["COPY_HINT"] = "複製語言設置";
$MESS["DEF"] = "預設";
$MESS["DELETE"] = "刪除";
$MESS["DELETE_ERROR"] = "錯誤刪除語言。它可能與其他一些對象連接。";
$MESS["DELETE_HINT"] = "刪除語言";
$MESS["DIR"] = "文件夾";
$MESS["LANG_EDIT_TITLE"] = "編輯語言";
$MESS["MAIN_ADMIN_LIST_ACTIVATE"] = "啟用";
$MESS["MAIN_ADMIN_LIST_DEACTIVATE"] = "停用";
$MESS["NAME"] = "姓名";
$MESS["PAGES"] = "語言";
$MESS["SAVE"] = "保存更改";
$MESS["SAVE_ERROR"] = "保存錯誤的語言";
$MESS["SORT"] = "種類";
$MESS["TITLE"] = "語言";
$MESS["lang_admin_code"] = "ID";
