<?php
$MESS["MURL_COMPONENT"] = "成分";
$MESS["MURL_DELETE"] = "刪除";
$MESS["MURL_DELETE_CONF"] = "您確定要刪除此唱片嗎？";
$MESS["MURL_EDIT"] = "編輯";
$MESS["MURL_FILE"] = "文件";
$MESS["MURL_FILTER_PATH"] = "小路";
$MESS["MURL_FILTER_SITE"] = "地點";
$MESS["MURL_NEW"] = "新紀錄";
$MESS["MURL_NEW_TITLE"] = "添加新記錄";
$MESS["MURL_REINDEX"] = "娛樂";
$MESS["MURL_REINDEX_TITLE"] = "重新創建URL處理規則";
$MESS["MURL_RULE"] = "規則";
$MESS["MURL_TITLE"] = "URL處理規則的設置";
$MESS["MURL_USL"] = "狀態";
$MESS["SAA_NAV"] = "規則";
