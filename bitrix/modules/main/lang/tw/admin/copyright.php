<?php
$MESS["main_copyright_3d_party"] = "第三方組件";
$MESS["main_copyright_bitrix_license"] = "使用＆quot＃＃產品＃＆quot的權限根據＃URL＃許可協議＃/url＃的條款授予。";
$MESS["main_copyright_license"] = "執照";
$MESS["main_copyright_owner"] = "版權所有者";
$MESS["main_copyright_popup_title"] = "執照";
$MESS["main_copyright_program"] = "軟體";
$MESS["main_copyright_program_license"] = "執照";
$MESS["main_copyright_title"] = "版權所有者";
