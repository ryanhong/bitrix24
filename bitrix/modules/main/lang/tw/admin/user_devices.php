<?php
$MESS["main_user_devices_agent"] = "代理人";
$MESS["main_user_devices_browser"] = "瀏覽器";
$MESS["main_user_devices_device_type"] = "設備類型";
$MESS["main_user_devices_device_uid"] = "設備UID";
$MESS["main_user_devices_menu_history"] = "設備登錄歷史記錄";
$MESS["main_user_devices_not_selected"] = "（未選中的）";
$MESS["main_user_devices_page"] = "頁";
$MESS["main_user_devices_platform"] = "平台";
$MESS["main_user_devices_row_title"] = "查看歷史";
$MESS["main_user_devices_title"] = "用戶設備";
$MESS["main_user_devices_user_id"] = "用戶身份";
