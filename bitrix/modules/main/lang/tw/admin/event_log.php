<?php
$MESS["MAIN_ALL"] = "（全部）";
$MESS["MAIN_EVENTLOG_AUDIT_TYPE_ID"] = "事件";
$MESS["MAIN_EVENTLOG_DESCRIPTION"] = "描述";
$MESS["MAIN_EVENTLOG_FORUM_MESSAGE"] = "訊息";
$MESS["MAIN_EVENTLOG_FORUM_TOPIC"] = "話題";
$MESS["MAIN_EVENTLOG_GUEST_ID"] = "客人";
$MESS["MAIN_EVENTLOG_IBLOCK"] = "信息塊";
$MESS["MAIN_EVENTLOG_IBLOCK_DELETE"] = "刪除";
$MESS["MAIN_EVENTLOG_ID"] = "ID";
$MESS["MAIN_EVENTLOG_ITEM_ID"] = "目的";
$MESS["MAIN_EVENTLOG_LIST_PAGE"] = "記錄";
$MESS["MAIN_EVENTLOG_MODULE_ID"] = "來源";
$MESS["MAIN_EVENTLOG_PAGE_TITLE"] = "事件日誌";
$MESS["MAIN_EVENTLOG_REMOTE_ADDR"] = "IP";
$MESS["MAIN_EVENTLOG_REQUEST_URI"] = "URL";
$MESS["MAIN_EVENTLOG_SEARCH"] = "尋找";
$MESS["MAIN_EVENTLOG_SEVERITY"] = "嚴重程度";
$MESS["MAIN_EVENTLOG_SITE_ID"] = "地點";
$MESS["MAIN_EVENTLOG_STOP_LIST"] = "停止列表";
$MESS["MAIN_EVENTLOG_TIMESTAMP_X"] = "時間";
$MESS["MAIN_EVENTLOG_USER_AGENT"] = "用戶代理";
$MESS["MAIN_EVENTLOG_USER_ID"] = "用戶";
$MESS["MAIN_EVENTLOG_WRONG_TIMESTAMP_X_FROM"] = "在過濾器中輸入正確的日誌啟動日期。";
$MESS["MAIN_EVENTLOG_WRONG_TIMESTAMP_X_TO"] = "在過濾器中輸入正確的日誌結束日期。";
$MESS["eventlog_notifications"] = "事件日誌通知";
$MESS["eventlog_notifications_title"] = "事件日誌通知參數";
