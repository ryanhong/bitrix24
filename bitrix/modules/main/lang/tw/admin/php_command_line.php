<?php
$MESS["php_cmd_button"] = "跑步";
$MESS["php_cmd_button_clear"] = "清除";
$MESS["php_cmd_confirm"] = "此命令將以PHP腳本在服務器上執行。繼續？";
$MESS["php_cmd_error"] = "執行命令的錯誤。";
$MESS["php_cmd_exec_time"] = "執行時間處理時間：";
$MESS["php_cmd_input"] = "PHP命令";
$MESS["php_cmd_note"] = "要更改標題標題，請使用單行PHP註釋開始代碼：//標題：yourtitle";
$MESS["php_cmd_php"] = "用於服務器上的一般執行的PHP腳本";
$MESS["php_cmd_result"] = "命令執行結果";
$MESS["php_cmd_text_result"] = "將命令輸出顯示為文本";
$MESS["php_cmd_title"] = "PHP命令行";
