<?php
$MESS["ACTIVE"] = "積極的：";
$MESS["ADD"] = "添加新語言";
$MESS["CHARSET"] = "編碼：";
$MESS["DAY_OF_WEEK_0"] = "星期日";
$MESS["DAY_OF_WEEK_1"] = "週一";
$MESS["DAY_OF_WEEK_2"] = "週二";
$MESS["DAY_OF_WEEK_3"] = "週三";
$MESS["DAY_OF_WEEK_4"] = "週四";
$MESS["DAY_OF_WEEK_5"] = "星期五";
$MESS["DAY_OF_WEEK_6"] = "週六";
$MESS["DEF"] = "預設:";
$MESS["DIR"] = "站點文件夾：";
$MESS["DIRECTION"] = "文本方向：";
$MESS["DIRECTION_LTR"] = "左到右";
$MESS["DIRECTION_RTL"] = "右到左";
$MESS["EDIT_LANG_TITLE"] = "編輯語言\“＃id＃\”";
$MESS["ERROR_DELETE"] = "無法刪除該語言，因為它可以綁定到其他對象。";
$MESS["FORMAT_DATE"] = "日期格式：";
$MESS["FORMAT_DATETIME"] = "日期時間格式：";
$MESS["FORMAT_NAME"] = "名稱格式：";
$MESS["LANG_EDIT_WEEK_START"] = "一周的第一天：";
$MESS["LANG_EDIT_WEEK_START_DEFAULT"] = "0";
$MESS["MAIN_COPY_RECORD"] = "添加副本";
$MESS["MAIN_COPY_RECORD_TITLE"] = "複製語言";
$MESS["MAIN_DELETE_RECORD"] = "刪除語言";
$MESS["MAIN_DELETE_RECORD_CONF"] = "您確定要刪除這種語言嗎？";
$MESS["MAIN_DELETE_RECORD_TITLE"] = "刪除當前語言";
$MESS["MAIN_ERROR_SAVING"] = "保存時發生了錯誤";
$MESS["MAIN_NEW_RECORD"] = "創建新語言";
$MESS["MAIN_NEW_RECORD_TITLE"] = "添加新語言";
$MESS["MAIN_PARAM"] = "參數";
$MESS["MAIN_PARAM_TITLE"] = "系統語言參數";
$MESS["NAME"] = "姓名：";
$MESS["NEW_LANG_TITLE"] = "添加語言";
$MESS["RECORD_LIST"] = "語言列表";
$MESS["RECORD_LIST_TITLE"] = "查看語言清單";
$MESS["RESET"] = "重置";
$MESS["SAVE"] = "保存更改";
$MESS["SORT"] = "排序索引：";
$MESS["lang_edit_code"] = "語言代碼（IETF BCP 47）：";
$MESS["lang_edit_culture"] = "語言和文化：";
$MESS["lang_edit_culture_edit"] = "編輯選定的選項";
