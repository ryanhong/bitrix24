<?php
$MESS["ACTION"] = "動作";
$MESS["ACTIVE"] = "積極的";
$MESS["ADD_TEMPL"] = "添加模板";
$MESS["ADD_TEMPL_TITLE"] = "添加新的電子郵件模板";
$MESS["ALL"] = "（全部）";
$MESS["CHANGE"] = "調整";
$MESS["CHANGE_ALT"] = "修改模板";
$MESS["CONFIRM_DEL_MESSAGE"] = "您確定要刪除模板嗎？";
$MESS["DELETE"] = "刪除";
$MESS["DELETE_ALT"] = "刪除模板";
$MESS["DELETE_ERROR"] = "錯誤刪除模板。它可能與其他對象連接。";
$MESS["EVENT_TYPE"] = "事件類型";
$MESS["F_ACTIVE"] = "積極的：";
$MESS["F_BCC"] = "BCC";
$MESS["F_BODY_TYPE"] = "消息主體格式";
$MESS["F_CONTENT"] = "模板內容";
$MESS["F_DEL"] = "卸下過濾器";
$MESS["F_D_MODIF"] = "修改的";
$MESS["F_FILTER"] = "篩選";
$MESS["F_FILTER_ALL"] = "（全部）";
$MESS["F_FROM"] = "從";
$MESS["F_ID"] = "ID";
$MESS["F_LANG"] = "地點：";
$MESS["F_SEARCH"] = "搜尋:";
$MESS["F_SEARCH_TITLE"] = "輸入文本以搜索";
$MESS["F_SITE"] = "地點";
$MESS["F_SUBJECT"] = "主題：";
$MESS["F_SUBMIT"] = "設置過濾器";
$MESS["F_THEME"] = "主題";
$MESS["F_TO"] = "到";
$MESS["F_TYPE"] = "電子郵件事件類型";
$MESS["LANG"] = "地點";
$MESS["MAIN_ADMIN_ADD_COPY"] = "添加副本";
$MESS["MAIN_ADMIN_LIST_ACTIVATE"] = "啟用";
$MESS["MAIN_ADMIN_LIST_DEACTIVATE"] = "停用";
$MESS["MAIN_ADMIN_MENU_DELETE"] = "刪除";
$MESS["MAIN_ADMIN_MENU_EDIT"] = "編輯";
$MESS["MAIN_ADMIN_MENU_EDIT_TITLE"] = "編輯模板";
$MESS["MAIN_ALL"] = "（全部）";
$MESS["MAIN_EDIT_ERROR"] = "錯誤更新記錄";
$MESS["MAIN_FROM_TILL_TIMESTAMP"] = "修改日期\“ till \”必須大於\ \“";
$MESS["MAIN_F_BCC"] = "BCC：";
$MESS["MAIN_F_BODY_TYPE"] = "消息身體類型：";
$MESS["MAIN_F_EVENT_TYPE"] = "事件類型：";
$MESS["MAIN_F_FROM"] = "從：";
$MESS["MAIN_F_ID"] = "ID：";
$MESS["MAIN_F_LID"] = "地點：";
$MESS["MAIN_F_MESSAGE_BODY"] = "郵件正文：";
$MESS["MAIN_F_TIMESTAMP"] = "修改的：";
$MESS["MAIN_F_TO"] = "到：";
$MESS["MAIN_HTML"] = "html";
$MESS["MAIN_NO"] = "不";
$MESS["MAIN_TEXT"] = "文字";
$MESS["MAIN_WRONG_TIMESTAMP_FROM"] = "輸入正確的修改日期\“來自\”";
$MESS["MAIN_WRONG_TIMESTAMP_TILL"] = "輸入正確的修改日期\“ till \”";
$MESS["MAIN_YES"] = "是的";
$MESS["NO"] = "不";
$MESS["PAGES"] = "模板";
$MESS["SAVE"] = "保存更改";
$MESS["SAVE_ERROR"] = "錯誤保存模板＃";
$MESS["SUBJECT"] = "主題";
$MESS["TIMESTAMP"] = "修改的";
$MESS["TITLE"] = "電子郵件模板";
$MESS["YES"] = "是的";
$MESS["main_mess_admin_lang"] = "語言";
$MESS["main_mess_admin_lang1"] = "語言";
$MESS["main_mess_admin_lang2"] = "語言：";
