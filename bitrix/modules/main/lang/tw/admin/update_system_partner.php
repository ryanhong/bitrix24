<?php
$MESS["SUPP_SUBR_BUTTON"] = "註冊模塊";
$MESS["SUPP_SUBR_ERR"] = "註冊錯誤";
$MESS["SUPP_SUBR_HINT"] = "您的許可證允許獲得以下模塊的功能齊全的版本：";
$MESS["SUPP_SUBR_REG"] = "模塊註冊";
$MESS["SUP_ACTIVE"] = "可用的更新：";
$MESS["SUP_ACTIVE_PERIOD"] = "從＃date_from＃直到＃date_to＃";
$MESS["SUP_APPLY"] = "申請";
$MESS["SUP_CANT_CONNECT"] = "無法連接到更新服務器。";
$MESS["SUP_CHECK_UPDATES"] = "檢查更新";
$MESS["SUP_CHECK_UPDATES_SYSTEM"] = "系統模塊的更新";
$MESS["SUP_EDITION"] = "產品版：";
$MESS["SUP_ERROR"] = "錯誤";
$MESS["SUP_HISTORY"] = "更新歷史記錄";
$MESS["SUP_INITIAL"] = "初始化...";
$MESS["SUP_LICENSE_KEY"] = "註冊碼";
$MESS["SUP_LICENSE_KEY_MD5"] = "許可密鑰哈希（MD5）";
$MESS["SUP_MYSQL_L4111"] = "您使用的是MySQL版本＃ver＃，而係統則需要版本5.0.0或更高版本。請更新您的MySQL安裝或聯繫技術支持。";
$MESS["SUP_MYSQL_LCP_ERROR"] = "您的MySQL數據庫以“＃CP1＃”編碼運行，而SiteUpdate需要'＃CP＃'編碼。請聯繫您的MySQL管理員以運行以下數據庫查詢：ALTER數據庫＃DB＃默認字符集＃CP＃";
$MESS["SUP_PHP_L439"] = "您使用的是PHP版本＃ver＃，而係統則需要5.3.0版或更高版本。請更新您的PHP安裝或聯繫技術支持。";
$MESS["SUP_REGISTERED"] = "註冊：";
$MESS["SUP_SERVER"] = "更新服務器：";
$MESS["SUP_SERVER_ANSWER"] = "更新服務器響應";
$MESS["SUP_SETTINGS"] = "編輯設置";
$MESS["SUP_SITES"] = "站點數：";
$MESS["SUP_STABLE_OFF_PROMT"] = "允許在您的網站上安裝更新的Beta版本。如果您只想安裝穩定的更新，請在更新系統設置中的此頁面上禁用Beta版本。";
$MESS["SUP_STABLE_ON_PROMT"] = "僅允許穩定的更新安裝在您的網站上。如果要下載並安裝Beta更新，請在此頁面或內核模塊設置中啟用此功能。";
$MESS["SUP_SUAC_ADD"] = "激活優惠券";
$MESS["SUP_SUAC_COUPON_ERROR"] = "激活優惠券的錯誤。";
$MESS["SUP_SUAC_LOAD_M_BUTTON"] = "下載";
$MESS["SUP_SUAC_LOAD_M_BUTTON_CONF"] = "注意力！ Bitrix公司對任何第三方模塊，組件或小工具概不負責。有關與模塊工作或網站故障或網站性能差的任何問題或第三方軟件引起的其他問題，請聯繫合作夥伴模塊開發人員公司。繼續加載？";
$MESS["SUP_SUAC_NO_COUP"] = "請提供優惠券代碼以激活";
$MESS["SUP_SUAC_SUCCESS"] = "該模塊已成功添加。";
$MESS["SUP_SUAC_TEXT"] = "請激活您的優惠券以添加新解決方案。";
$MESS["SUP_SUAC_TEXT2"] = "輸入優惠券代碼：";
$MESS["SUP_SUBA_ACTIVATE"] = "許可證密鑰激活";
$MESS["SUP_SUBA_ACTIVATE_BUTTON"] = "激活密鑰";
$MESS["SUP_SUBA_ACTIVATE_HINT"] = "在使用更新系統之前，您必須激活許可證密鑰。";
$MESS["SUP_SUBA_CONFIRM_ERROR"] = "缺少一些必需的字段！";
$MESS["SUP_SUBA_FE_CONF_ERR"] = "密碼確認不匹配密碼";
$MESS["SUP_SUBA_FE_CONTACT"] = "聯繫信息";
$MESS["SUP_SUBA_FE_CONTACT_EMAIL"] = "電子郵件";
$MESS["SUP_SUBA_FE_CONTACT_PERSON"] = "聯絡人";
$MESS["SUP_SUBA_FE_CONTACT_PHONE"] = "電話";
$MESS["SUP_SUBA_FE_EMAIL"] = "聯繫電子郵件";
$MESS["SUP_SUBA_FE_ERRGEN"] = "密鑰激活錯誤";
$MESS["SUP_SUBA_FE_FNAME"] = "使用者名稱";
$MESS["SUP_SUBA_FE_LNAME"] = "用戶姓氏";
$MESS["SUP_SUBA_FE_LOGIN"] = "用戶登錄";
$MESS["SUP_SUBA_FE_NAME"] = "公司/所有者名稱";
$MESS["SUP_SUBA_FE_PASSWORD"] = "密碼";
$MESS["SUP_SUBA_FE_PASSWORD_CONF"] = "確認密碼";
$MESS["SUP_SUBA_FE_PHONE"] = "電話";
$MESS["SUP_SUBA_FE_PROMT"] = "以下字段包含錯誤：";
$MESS["SUP_SUBA_FE_URI"] = "網站地址";
$MESS["SUP_SUBA_REGINFO"] = "註冊信息";
$MESS["SUP_SUBA_RI_CONTACT"] = "其他重要聯繫人：";
$MESS["SUP_SUBA_RI_CONTACT1"] = "您認為重要的地址和其他聯繫信息。";
$MESS["SUP_SUBA_RI_CONTACT_EMAIL"] = "聯繫人電子郵件：";
$MESS["SUP_SUBA_RI_CONTACT_EMAIL1"] = "通過電子郵件發送有關技術問題的聯繫，發送註冊信息和TechSupport訂閱更新提醒。";
$MESS["SUP_SUBA_RI_CONTACT_PERSON"] = "負責的聯繫人：";
$MESS["SUP_SUBA_RI_CONTACT_PERSON1"] = "負責使用此產品副本的人的全名。";
$MESS["SUP_SUBA_RI_CONTACT_PHONE"] = "聯繫人電話：";
$MESS["SUP_SUBA_RI_CONTACT_PHONE1"] = "聯繫人電話";
$MESS["SUP_SUBA_RI_EMAIL"] = "許可並使用聯繫電子郵件：";
$MESS["SUP_SUBA_RI_EMAIL1"] = "公司或私人人士的電子郵件地址，以聯繫許可和使用。";
$MESS["SUP_SUBA_RI_NAME"] = "全公司或私人名稱";
$MESS["SUP_SUBA_RI_NAME1"] = "擁有此產品副本的人的名稱或第一個和姓氏。";
$MESS["SUP_SUBA_RI_PHONE"] = "產品副本所有者（公司或個人）的電話號碼";
$MESS["SUP_SUBA_RI_PHONE1"] = "產品副本所有者（公司或個人）的電話號碼";
$MESS["SUP_SUBA_RI_URI"] = "所有將由Bitrix站點管理器提供動力的域，包括測試域";
$MESS["SUP_SUBA_RI_URI1"] = "所有將由Bitrix站點管理器提供動力的域，包括測試域";
$MESS["SUP_SUBA_UI_CREATE"] = "我沒有一個帳戶，請訪問<a href= \"http://www.bitrixsoft.com \"> www.bitrixsoft.com </a>我想創建一個";
$MESS["SUP_SUBA_UI_EXIST"] = "我已經有一個用戶帳戶，想使用它來訪問helpdesk並下載站點部分";
$MESS["SUP_SUBA_UI_HINT"] = "您可以訪問<a href= \“http://www.bitrixsoft.com/support/xupport/xtreport/xtreport </a>，<a href = \ \“ http://www.bitrixsoft.com/support / forum/forum7/\“>私人客戶端論壇</a>和<a href= \"http://www.bitrixsoft.com/download/private/index.php \">下載系統源代碼</ A>通過在<a href= \"http://www.bitrixsoft.com \"> www.bitrixsoft.com </a>上創建一個帳戶。";
$MESS["SUP_SUBA_UI_LASTNAME"] = "你的姓氏";
$MESS["SUP_SUBA_UI_LOGIN"] = "登錄（3個或更多字符）";
$MESS["SUP_SUBA_UI_LOGIN_EXIST"] = "現有的用戶登錄";
$MESS["SUP_SUBA_UI_PASSWORD"] = "密碼（6個或更多字符）";
$MESS["SUP_SUBA_UI_PASSWORD_CONF"] = "確認密碼";
$MESS["SUP_SUBA_USERINFO"] = "Bitrix Company網站用戶";
$MESS["SUP_SUBA__UI_NAME"] = "你的名字";
$MESS["SUP_SUBI_CHECK"] = "最後更新檢查";
$MESS["SUP_SUBI_UPD"] = "已安裝更新";
$MESS["SUP_SUBK_BUTTON"] = "提交許可證密鑰";
$MESS["SUP_SUBK_ERROR"] = "錯誤保存密鑰。";
$MESS["SUP_SUBK_GET_KEY"] = "請求審判許可證密鑰";
$MESS["SUP_SUBK_HINT"] = "請輸入您的許可證密鑰。否則，您將在CD框或電子郵件中找到它。一個好的許可密鑰看起來像<span style = \“白色空間：nowrap \”> smx-xx-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx的的內容。";
$MESS["SUP_SUBK_KEY"] = "註冊碼";
$MESS["SUP_SUBK_NO_KEY"] = "沒有提供許可證密鑰。";
$MESS["SUP_SUBK_PROMT"] = "註冊碼";
$MESS["SUP_SUBK_SAVE"] = "保存許可證密鑰";
$MESS["SUP_SUBR_BUTTON"] = "登記";
$MESS["SUP_SUBR_ERR"] = "註冊錯誤";
$MESS["SUP_SUBR_HINT"] = "您的許可證允許獲得沒有時間限制的徹底功能的系統版本。";
$MESS["SUP_SUBR_REG"] = "登記";
$MESS["SUP_SUBS_BUTTON"] = "下載源代碼";
$MESS["SUP_SUBS_HINT"] = "您的許可證允許獲得系統模塊的源代碼。但是，您必須安裝所有系統模塊的最新版本（這意味著不可用）。如果仍有任何更新，則必須先安裝它。";
$MESS["SUP_SUBS_MED"] = "下載的源代碼";
$MESS["SUP_SUBS_SOURCES"] = "下載源代碼";
$MESS["SUP_SUBS_SUCCESS"] = "源代碼已成功下載。";
$MESS["SUP_SUBT_AGREE"] = "我接受許可協議";
$MESS["SUP_SUBT_AGREE_PRIVACY"] = "我已經閱讀並同意<a href= \“https://www.bitrix24.com/privacy/xtart target=\"_blank \">隱私策略</a>";
$MESS["SUP_SUBT_ERROR_LICENCE"] = "錯誤接受許可證。";
$MESS["SUP_SUBT_LICENCE"] = "許可協議";
$MESS["SUP_SUBT_LICENCE_BUTTON"] = "查看許可協議";
$MESS["SUP_SUBT_LICENCE_HINT"] = "您必須接受新的許可協議來使用更新系統。";
$MESS["SUP_SUBU_BUTTON"] = "安裝新的SiteUpdate版本";
$MESS["SUP_SUBU_ERROR"] = "更新錯誤";
$MESS["SUP_SUBU_HINT"] = "可以使用新版本的更新系統。在進行更新之前，必須安裝它。";
$MESS["SUP_SUBU_UPDATE"] = "新的更新系統";
$MESS["SUP_SUBV_BETA"] = "更改Beta版本安裝模式";
$MESS["SUP_SUBV_BETB"] = "啟用Beta版本";
$MESS["SUP_SUBV_ERROR"] = "錯誤切換模式。";
$MESS["SUP_SUBV_HINT"] = "\“ beta版本\”是一項更新，在發布之前已在內部對其進行了測試。 Beta版本完全包括所有最新更改和修復程序。儘管beta相對穩定，但在某些情況下它們可能會引起潛在的誤操作。";
$MESS["SUP_SUBV_STABB"] = "僅允許穩定的更新";
$MESS["SUP_SUB_ERROR"] = "一些更新未能安裝。";
$MESS["SUP_SUB_PROGRESS"] = "在安裝更新";
$MESS["SUP_SUB_STOP"] = "停止";
$MESS["SUP_SUB_SUCCESS"] = "更新已成功安裝。";
$MESS["SUP_SUG_NOTES"] = "注意力！ Bitrix公司對任何第三方模塊，組件或小工具概不負責。有關與模塊工作或網站故障或網站性能差的任何問題或第三方軟件引起的其他問題，請聯繫合作夥伴模塊開發人員公司。";
$MESS["SUP_SUG_NOTES1"] = "更新系統不會從您的網站收集或發送任何私人信息。";
$MESS["SUP_SULD_CLOSE"] = "關閉";
$MESS["SUP_SULD_DESC"] = "描述";
$MESS["SUP_SULL_ADD"] = "額外的";
$MESS["SUP_SULL_ADD1"] = "幫助其他語言";
$MESS["SUP_SULL_BUTTON"] = "安裝更新";
$MESS["SUP_SULL_CBT"] = "檢查全部 /取消選中";
$MESS["SUP_SULL_CNT"] = "可用的更新";
$MESS["SUP_SULL_HELP"] = "幫助“＃＃名稱＃”中的部分";
$MESS["SUP_SULL_LANG"] = "語言文件：＆quot＃name＃＆quot;";
$MESS["SUP_SULL_MODULE"] = "模塊“＃名稱＃”";
$MESS["SUP_SULL_MODULE_PATH"] = "http://marketplace.bitrixsoft.com/#name#";
$MESS["SUP_SULL_NAME"] = "姓名";
$MESS["SUP_SULL_NOTE"] = "細節";
$MESS["SUP_SULL_NOTE_D"] = "細節";
$MESS["SUP_SULL_PARTNER_NAME"] = "公司";
$MESS["SUP_SULL_REF_N"] = "新的附加字段";
$MESS["SUP_SULL_REF_O"] = "更新";
$MESS["SUP_SULL_REL"] = "版本";
$MESS["SUP_SULL_TYPE"] = "類型";
$MESS["SUP_SULL_VERSION"] = "版本＃VER＃";
$MESS["SUP_SUSU_BUTTON"] = "訂閱";
$MESS["SUP_SUSU_EMAIL"] = "電子郵件地址";
$MESS["SUP_SUSU_ERROR"] = "錯誤訂閱通知。";
$MESS["SUP_SUSU_HINT"] = "通過電子郵件通知有關新更新的通知";
$MESS["SUP_SUSU_NO_EMAIL"] = "需要電子郵件地址。";
$MESS["SUP_SUSU_SUCCESS"] = "您已成功訂閱了有關新更新的通知。";
$MESS["SUP_SUSU_TITLE"] = "訂閱更新通知";
$MESS["SUP_SU_OPTION"] = "可選更新";
$MESS["SUP_SU_OPTION_HELP"] = "<b> #num＃</b>提供幫助部分";
$MESS["SUP_SU_OPTION_LAN"] = "<b> #num＃</b>用於語言文件";
$MESS["SUP_SU_RECOMEND"] = "推薦更新";
$MESS["SUP_SU_RECOMEND_LAN"] = "<b> #num＃</b>用於語言文件";
$MESS["SUP_SU_RECOMEND_MOD"] = "<b> #num＃</b>用於模塊";
$MESS["SUP_SU_RECOMEND_NO"] = "沒有任何";
$MESS["SUP_SU_TITLE1"] = "為您的網站安裝更新";
$MESS["SUP_SU_TITLE2"] = "該系統已更新。";
$MESS["SUP_SU_UPD_BUTTON"] = "安裝建議的更新";
$MESS["SUP_SU_UPD_HINT"] = "強烈建議您安裝所有可用的更新，以獲得新功能，提高網站的安全性和性能。";
$MESS["SUP_SU_UPD_INSMED"] = "安裝更新";
$MESS["SUP_SU_UPD_INSSUC"] = "成功安裝了更新";
$MESS["SUP_SU_UPD_MP_NEW"] = "以下解決方案已添加到您的網站上：";
$MESS["SUP_SU_UPD_MP_NEW2"] = "如果您現在不想立即安裝解決方案，可以稍後在Marketplace＆GT;解決方案。";
$MESS["SUP_SU_UPD_MP_NEW_INST"] = "安裝";
$MESS["SUP_SU_UPD_VIEW"] = "查看更新";
$MESS["SUP_TAB_COUPON"] = "激活優惠券";
$MESS["SUP_TAB_COUPON_ALT"] = "激活優惠券";
$MESS["SUP_TAB_UPDATES"] = "安裝更新";
$MESS["SUP_TAB_UPDATES_ALT"] = "為您的網站安裝更新";
$MESS["SUP_TAB_UPDATES_LIST"] = "更新";
$MESS["SUP_TAB_UPDATES_LIST_ALT"] = "選擇更新以安裝";
$MESS["SUP_TITLE_BASE"] = "合作夥伴模塊更新系統";
$MESS["SUP_USERS"] = "用戶計數：";
$MESS["main_update_partner_block"] = "無法安裝第三方解決方案。";
