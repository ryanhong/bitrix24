<?php
$MESS["COMPOSITE_BANNER_TEXT"] = "比特里克斯更快";
$MESS["COMPOSITE_BANNER_URL"] = "http://www.bitrixsoft.com/compoitse/";
$MESS["MAIN_ADD"] = "添加";
$MESS["MAIN_COMPOSITE_ANONYMOUS_GROUP"] = "未經授權的用戶";
$MESS["MAIN_COMPOSITE_AUTO_BUTTON_OFF"] = "禁用自動複合材料模式";
$MESS["MAIN_COMPOSITE_AUTO_BUTTON_ON"] = "啟用自動複合模式";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE"] = "啟用自動複合模式";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_AUTOMATION"] = "您無需聘請網絡開發人員來使您的網站自動複雜材料。";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_AUTOMATION2"] = "您將看不到技術Mumbo -Jumbo-您所獲得的只是您網站最快的新現實！只需單擊按鈕 - 我們將為您做其他所有操作。";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_CHECKLIST_SUBTITLE"] = "現在，請確保您的網站通過新的自動複合材料技術眨眼間就可以打開。通過訪客的眼光看待它：";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_CHECKLIST_TASK1"] = "打開一個新的隱身瀏覽器窗口。";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_CHECKLIST_TASK2"] = "訪問您網站的主頁以立即查看其加載。";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_CHECKLIST_TASK3"] = "現在遵循訪問者可能會採取的典型單擊路徑。如果您的網站是網站商店，那麼最好查看產品，將其添加到購物車中並結帳。";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_CHECKLIST_TITLE"] = "現在檢查您的網站 - 它可以快速起作用！";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_FAST_PING"] = "最終用戶立即獲取頁面內容。";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_FEATURE1"] = "站點響應時間更快一百倍";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_FEATURE2"] = "Google中更好的排名";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_FEATURE3"] = "網站的轉換比率超出圖表";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_SIMPLE_TECH"] = "自動複雜材料＆mdash;沒有痛苦，收穫很多";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_SUBTITLE"] = "技術";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_TAB_TITLE"] = "自動複合材料";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_TITLE"] = "自動複雜材料站點";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_TITLE_DESC"] = "保持競爭對手的領先地位。<br />任何Bitrix動力的網站現在都可以利用複合技術。";
$MESS["MAIN_COMPOSITE_AUTO_COMPOSITE_UNIQUE_TECH"] = "獨特的網站建築技術可提供靜態站點的高負載速度，並且動態站點可以提供的所有功能。";
$MESS["MAIN_COMPOSITE_AUTO_OFF"] = "自動複合模式禁用";
$MESS["MAIN_COMPOSITE_AUTO_ON"] = "啟用自動複合模式";
$MESS["MAIN_COMPOSITE_BANNER_BGCOLOR"] = "更改背景顏色";
$MESS["MAIN_COMPOSITE_BANNER_DISCLAIMER"] = "激活<b>複合網站</b>技術將在使用此功能的頁面右下角自動添加一個按鈕。可以配置顯示按鈕的參數。如果您需要將按鈕放在其他位置，則可以使用<b> id =“ bx-composite-banner＆quort” </b>進入站點模板中的位置。";
$MESS["MAIN_COMPOSITE_BANNER_SELECT_STYLE"] = "按鈕視圖";
$MESS["MAIN_COMPOSITE_BANNER_SEP"] = "按鈕";
$MESS["MAIN_COMPOSITE_BANNER_STYLE"] = "更改徽標和標題";
$MESS["MAIN_COMPOSITE_BANNER_STYLE_WHITE"] = "白色背景";
$MESS["MAIN_COMPOSITE_BUTTON_OFF"] = "禁用複合模式";
$MESS["MAIN_COMPOSITE_BUTTON_ON"] = "啟用複合模式";
$MESS["MAIN_COMPOSITE_CACHE_MODE_NO_UPDATE"] = "沒有背景AJAX查詢（HTML緩存）";
$MESS["MAIN_COMPOSITE_CACHE_MODE_NO_UPDATE_DESC"] = "緩存頁面直到到期之前才能執行背景AJAX查詢。 AJAX查詢始終覆蓋現有的緩存數據。";
$MESS["MAIN_COMPOSITE_CACHE_MODE_NO_UPDATE_TTL"] = "頁面壽命";
$MESS["MAIN_COMPOSITE_CACHE_MODE_STANDARD"] = "標準";
$MESS["MAIN_COMPOSITE_CACHE_MODE_STANDARD_DESC"] = "一個緩存的頁面總是執行背景ajax查詢。每當頁面內容更改時，緩存覆蓋。";
$MESS["MAIN_COMPOSITE_CACHE_MODE_STANDARD_TTL"] = "具有延期覆蓋的標準模式（建議）";
$MESS["MAIN_COMPOSITE_CACHE_MODE_STANDARD_TTL_DESC"] = "一個緩存的頁面總是執行背景ajax查詢。在頁面內容更改上設置的覆蓋超時後，緩存覆蓋已過期。";
$MESS["MAIN_COMPOSITE_CACHE_MODE_TTL"] = "覆蓋延遲";
$MESS["MAIN_COMPOSITE_CACHE_MODE_TTL_UNIT_SEC"] = "秒";
$MESS["MAIN_COMPOSITE_CACHE_REWRITING"] = "緩存覆蓋模式";
$MESS["MAIN_COMPOSITE_CHECK_CONNECTION"] = "測試連接";
$MESS["MAIN_COMPOSITE_CHECK_CONNECTION_ERR1"] = "未安裝php memcache";
$MESS["MAIN_COMPOSITE_CHECK_CONNECTION_ERR2"] = "無法連接到Memcached";
$MESS["MAIN_COMPOSITE_CHECK_CONNECTION_OK"] = "連接成功";
$MESS["MAIN_COMPOSITE_CLEAR_CACHE"] = "重置緩存";
$MESS["MAIN_COMPOSITE_CLUSTER_HINT"] = "使用＃a_start＃Web群集＃A_END＃模塊設置頁面為MEMCACHED設置連接參數。";
$MESS["MAIN_COMPOSITE_COMPOSITE_SWITCH_DESC1"] = "將網站遷移到新技術並不復雜，但是大型且經驗豐富的項目可能需要一些時間。";
$MESS["MAIN_COMPOSITE_COMPOSITE_SWITCH_DESC2"] = "根據我們的經驗，像Bitrix24網站一樣大的網絡項目可以在四天內遷移，僅涉及一個開發人員。";
$MESS["MAIN_COMPOSITE_COMPOSITE_SWITCH_DESC3"] = "執行以下操作以將您的項目遷移到自動複合材料技術：";
$MESS["MAIN_COMPOSITE_COMPOSITE_SWITCH_ON"] = "啟用自動複合材料技術！";
$MESS["MAIN_COMPOSITE_COMPOSITE_SWITCH_TASK1"] = "大量文檔將很快提供。";
$MESS["MAIN_COMPOSITE_COMPOSITE_SWITCH_TASK2"] = "通過為複合模式添加佈局標籤來修改組件模板。";
$MESS["MAIN_COMPOSITE_COMPOSITE_SWITCH_TASK3"] = "如果您遇到困難，請與任何伴侶聯繫以幫助您遷移。";
$MESS["MAIN_COMPOSITE_COMPOSITE_SWITCH_TASK4"] = "點擊'啟用複合模式＆raquo;";
$MESS["MAIN_COMPOSITE_COMPOSITE_TITLE_DESC"] = "高速帶來了巨大的可能性。 <br>新型獨特的網站技術提供了靜態站點固有的快速載荷速度<br>，同時保持了極大的靈活性和動態項目的其他功能。";
$MESS["MAIN_COMPOSITE_CORPORATE_PORTAL_DOMAIN_WARNING"] = "<span class = \“必需\”>注意！</span>不要將BitRix24域名添加到“設置”選項卡上的域名列表中。 BitRix24模板不支持複合模式。";
$MESS["MAIN_COMPOSITE_DOMAINS"] = "域名（在新線上啟動）";
$MESS["MAIN_COMPOSITE_ENABLED"] = "啟用";
$MESS["MAIN_COMPOSITE_EXCLUDE_BY_PARAMS"] = "對於這些參數，將為URL禁用複合模式";
$MESS["MAIN_COMPOSITE_EXC_MASK"] = "排除面具";
$MESS["MAIN_COMPOSITE_EXT_ERROR"] = "＃擴展＃不安裝擴展";
$MESS["MAIN_COMPOSITE_FIRST_SITE_RESTRICTION"] = "在此版本中始終啟用複合模式。";
$MESS["MAIN_COMPOSITE_FRAME_DESC"] = "這些參數可以在組件參數中覆蓋。";
$MESS["MAIN_COMPOSITE_FRAME_MODE"] = "默認組件模板投票";
$MESS["MAIN_COMPOSITE_FRAME_MODE_CONTRA"] = "禁用（需要手動配置）";
$MESS["MAIN_COMPOSITE_FRAME_MODE_PRO"] = "使能夠";
$MESS["MAIN_COMPOSITE_FRAME_TYPE"] = "組件內容";
$MESS["MAIN_COMPOSITE_FRAME_TYPE_DYNAMIC_WITH_STUB"] = "使用存根的動態內容";
$MESS["MAIN_COMPOSITE_FRAME_TYPE_STATIC"] = "靜止的";
$MESS["MAIN_COMPOSITE_HOST_HINT"] = "<b>主機</b>字段可以指定一個unix套接字，例如：<i> unix：///tmp/memcached.socket </i>，<br>在這種情況下，<b> port </> B>必須將字段設置為零（<b> 0 </b>）。";
$MESS["MAIN_COMPOSITE_IGNORED_PARAMETERS"] = "忽略這些URL參數";
$MESS["MAIN_COMPOSITE_INC_MASK"] = "包容面膜";
$MESS["MAIN_COMPOSITE_MEMCACHED_HOST"] = "主持人";
$MESS["MAIN_COMPOSITE_MEMCACHED_PORT"] = "港口";
$MESS["MAIN_COMPOSITE_MODULE_ERROR"] = "＃模塊＃未安裝模塊";
$MESS["MAIN_COMPOSITE_NO_PARAMETERS"] = "保存到磁盤僅無參數的頁面";
$MESS["MAIN_COMPOSITE_OFF"] = "複合模式關閉";
$MESS["MAIN_COMPOSITE_ON"] = "複合模式開啟";
$MESS["MAIN_COMPOSITE_ONLY_PARAMETERS"] = "也只使用這些參數保存頁面";
$MESS["MAIN_COMPOSITE_OPT"] = "設定";
$MESS["MAIN_COMPOSITE_PATENT_TAB"] = "專利";
$MESS["MAIN_COMPOSITE_PATENT_TAB_DESC"] = "專利申請中";
$MESS["MAIN_COMPOSITE_QUOTA"] = "磁盤報價（MB）";
$MESS["MAIN_COMPOSITE_RESET"] = "還原為默認值";
$MESS["MAIN_COMPOSITE_SAVE"] = "保存設置";
$MESS["MAIN_COMPOSITE_SAVE_ERROR"] = "由於發生錯誤而無法保存。";
$MESS["MAIN_COMPOSITE_SELECT_GROUP"] = "（選擇用戶組）";
$MESS["MAIN_COMPOSITE_SETTINGS_TAB"] = "設定";
$MESS["MAIN_COMPOSITE_SHORT_TITLE"] = "合成的";
$MESS["MAIN_COMPOSITE_SHOW_BANNER"] = "顯示＆quot＆quot bitrix＆quot'按鈕";
$MESS["MAIN_COMPOSITE_STAT_FILE_SIZE"] = "當前的緩存大小：";
$MESS["MAIN_COMPOSITE_STORAGE"] = "存儲緩存";
$MESS["MAIN_COMPOSITE_STORAGE_FILES"] = "文件";
$MESS["MAIN_COMPOSITE_STORAGE_TITLE"] = "緩存存儲模式";
$MESS["MAIN_COMPOSITE_TAB"] = "複合模式";
$MESS["MAIN_COMPOSITE_TAB_GROUPS"] = "組";
$MESS["MAIN_COMPOSITE_TAB_GROUPS_TITLE_NEW"] = "選擇將使用複合模式的用戶組";
$MESS["MAIN_COMPOSITE_TAB_SITES"] = "網站";
$MESS["MAIN_COMPOSITE_TAB_SITES_TITLE"] = "為單個網站啟用複合模式";
$MESS["MAIN_COMPOSITE_TAB_TITLE"] = "參數和設置";
$MESS["MAIN_COMPOSITE_TITLE"] = "複合網站";
$MESS["MAIN_COMPOSITE_VOTING_TITLE"] = "複合模式投票";
$MESS["MAIN_COMPOSITE_WARNING"] = "靜態緩存模式已打開。請禁用它。";
$MESS["MAIN_COMPOSITE_WARNING_EDUCATION"] = " - ";
