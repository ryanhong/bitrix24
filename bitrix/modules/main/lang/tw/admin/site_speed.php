<?php
$MESS["MAIN_SITE_SPEED_CHOOSE_DOMAIN"] = "選擇列表中的另一個域。";
$MESS["MAIN_SITE_SPEED_COMPOSITE_HITS"] = "複合命中";
$MESS["MAIN_SITE_SPEED_COMPOSITE_SITE"] = "複合網站";
$MESS["MAIN_SITE_SPEED_CONNECTION_ERROR"] = "無法連接到統計服務器。";
$MESS["MAIN_SITE_SPEED_DISABLED"] = "離開";
$MESS["MAIN_SITE_SPEED_DOMAINS_LABEL"] = "域的統計數據";
$MESS["MAIN_SITE_SPEED_DOMAIN_NOT_FOUND"] = "該域沒有數據。";
$MESS["MAIN_SITE_SPEED_ENABLED"] = "在";
$MESS["MAIN_SITE_SPEED_GRAPH_TITLE"] = "最近的熱門歌曲";
$MESS["MAIN_SITE_SPEED_HISTO_TITLE"] = "站點速度隨著時間的推移";
$MESS["MAIN_SITE_SPEED_HITS_LABEL"] = "命中處理";
$MESS["MAIN_SITE_SPEED_MAP_EXT_HINT_COMPOSITE"] = "複合命中";
$MESS["MAIN_SITE_SPEED_MAP_EXT_HINT_HITS"] = "總命中";
$MESS["MAIN_SITE_SPEED_MAP_EXT_HINT_SPEED"] = "站點速度";
$MESS["MAIN_SITE_SPEED_MAP_EXT_HITS_1"] = "位置命中";
$MESS["MAIN_SITE_SPEED_MAP_EXT_HITS_2"] = "當前命中率";
$MESS["MAIN_SITE_SPEED_MAP_EXT_MARK_1"] = "非常快";
$MESS["MAIN_SITE_SPEED_MAP_EXT_MARK_2"] = "快速地";
$MESS["MAIN_SITE_SPEED_MAP_EXT_MARK_3"] = "平均的";
$MESS["MAIN_SITE_SPEED_MAP_EXT_MARK_4"] = "慢的";
$MESS["MAIN_SITE_SPEED_MAP_EXT_MARK_5"] = "非常慢";
$MESS["MAIN_SITE_SPEED_MAP_EXT_VALUE_1"] = "網站速度位置";
$MESS["MAIN_SITE_SPEED_MAP_EXT_VALUE_2"] = "沒有復合命中";
$MESS["MAIN_SITE_SPEED_MAP_EXT_VDOTS_1"] = "位置平均值";
$MESS["MAIN_SITE_SPEED_MAP_EXT_VDOTS_2"] = "值類型";
$MESS["MAIN_SITE_SPEED_MAP_TITLE"] = "訪問者位置的網站性能（最近有1000個）";
$MESS["MAIN_SITE_SPEED_NOTES"] = "<b>頁面渲染</b>＆mdash;自導航的那一刻以來已經過去的時間
開始直到屏幕上可見頁面。此值用於得出<b>網站
速度</b>。<br> <b> dns </b>＆mdash; DNS請求完成的時間。<br> <b>服務器
連接</b>＆mdash;客戶機連接到一個
服務器。<br> <b>服務器響應</b>＆mdash;服務器消耗處理的時間
客戶端請求（包括網絡談判）。<br> <b> html下載</b>＆mdash;
下載頁面html代碼所需的時間（沒有圖像，CSS，CSS，
JavaScript）。<br> <b> html processing </b>＆mdash;瀏覽器消耗的時間
處理頁面（包括HTML解析，CSS解析，處理JavaScript
代碼和頁面渲染）。此值表示頁面後測量的時間
下載直到準備在屏幕上繪製。<br>";
$MESS["MAIN_SITE_SPEED_PERF"] = "性能監視器";
$MESS["MAIN_SITE_SPEED_PERF_NO_RES"] = "沒有進行性能測試";
$MESS["MAIN_SITE_SPEED_PERIOD_LABEL"] = "報告期";
$MESS["MAIN_SITE_SPEED_SECONDS"] = "秒";
$MESS["MAIN_SITE_SPEED_TITLE"] = "站點速度";
$MESS["MAIN_SITE_SPEED_TITLE_DESC"] = "在最新的1000個訪問者中，在客戶瀏覽器中顯示了一個站點頁面的平均時間。";
