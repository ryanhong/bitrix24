<?php
$MESS["ERROR_DEL_SMILE"] = "錯誤刪除畫廊。";
$MESS["SMILE_BTN_ADD_NEW"] = "新畫廊";
$MESS["SMILE_BTN_ADD_NEW_ALT"] = "點擊創建一個新畫廊";
$MESS["SMILE_DELETE_DESCR"] = "刪除";
$MESS["SMILE_DEL_CONF"] = "您確定要刪除畫廊嗎？這還將刪除畫廊包含的表情符號。";
$MESS["SMILE_EDIT"] = "編輯";
$MESS["SMILE_EDIT_DESCR"] = "編輯";
$MESS["SMILE_GALLERY_NAME"] = "畫廊：＃ID＃";
$MESS["SMILE_ID"] = "ID";
$MESS["SMILE_NAME"] = "姓名";
$MESS["SMILE_NAV"] = "畫廊";
$MESS["SMILE_SMILE_COUNT"] = "表情符號";
$MESS["SMILE_SORT"] = "種類";
$MESS["SMILE_STRING_ID"] = "畫廊代碼";
$MESS["SMILE_TITLE"] = "表情片畫廊";
