<?php
$MESS["SQL_ADD_TO_FAVORITES"] = "書籤SQL查詢";
$MESS["SQL_AFFECTED_ROWS"] = "處理的記錄";
$MESS["SQL_CONFIRM_EXECUTE"] = "您確定要在數據庫中執行此SQL查詢嗎？";
$MESS["SQL_EXECUTE"] = "執行查詢";
$MESS["SQL_EXEC_TIME"] = "執行時間處理時間：";
$MESS["SQL_PAGES"] = "記錄";
$MESS["SQL_PAGE_TITLE"] = "SQL查詢";
$MESS["SQL_QUERY_ERROR_1"] = "執行查詢的錯誤";
$MESS["SQL_QUERY_RESULT"] = "結果：";
$MESS["SQL_RESET"] = "重置";
$MESS["SQL_SEC"] = "秒";
$MESS["SQL_SUCCESS_EXECUTE"] = "查詢成功";
$MESS["SQL_TAB"] = "SQL查詢";
$MESS["SQL_TAB_TITLE"] = "SQL查詢到數據庫";
$MESS["SQL_TOTAL"] = "總記錄：";
