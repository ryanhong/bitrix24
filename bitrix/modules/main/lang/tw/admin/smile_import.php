<?php
$MESS["ERROR_BAD_SESSID"] = "您的會議已經過期。請再試一次。";
$MESS["ERROR_COPY_FILE"] = "存檔上傳錯誤（寫入不足？）";
$MESS["ERROR_EXISTS_FILE"] = "存檔上傳錯誤。請再試一次。";
$MESS["ERROR_IMPORT_SMILE"] = "錯誤導入笑臉";
$MESS["IM_IMPORT_COMPLETE"] = "笑臉已經進口。";
$MESS["IM_IMPORT_HELP_1"] = "存檔文件可能包括文本說明和消息本地化。可以使用示例存檔＃link_start＃werfe＃link_end＃。";
$MESS["IM_IMPORT_HELP_2"] = "If the archive doesn't include any service files at all, the import procedure will assign smileys according to their filenames (prefixes: \"smile_\" - a smiley, \"icon_\" - an icon; suffix: \"_hr\ \" - 高解析度).";
$MESS["IM_IMPORT_HELP_3"] = "例如：當導入名為\“ test \”的類別時，文件<b> smile_green_hr.png </b>將被拖延為高分辨率微笑，並分配了代碼<b>：test/test/green：< /b >。";
$MESS["IM_IMPORT_HELP_4"] = "<b>注意！這是標準包。新的更新可能會恢復對該包進行的任何更改。<br>建議將＃link_start＃創建＃link_end＃自定義包並添加表情符號。</b>";
$MESS["IM_IMPORT_TOTAL"] = "成功導入：＃計數＃";
$MESS["SMILE_FILE"] = "檔案";
$MESS["SMILE_FILE_NOTE"] = "檔案格式：未壓縮拉鍊";
$MESS["SMILE_IMPORT_TITLE"] = "導入笑臉";
$MESS["SMILE_SET_ID"] = "放";
$MESS["SMILE_TAB_SMILE"] = "參數";
$MESS["SMILE_TAB_SMILE_DESCR"] = "導入設置";
