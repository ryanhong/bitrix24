<?php
$MESS["MAIN_AGENT_ACTIVATE"] = "啟用";
$MESS["MAIN_AGENT_ACTIVE"] = "積極的";
$MESS["MAIN_AGENT_ACTIVE_NO"] = "不";
$MESS["MAIN_AGENT_ACTIVE_YES"] = "是的";
$MESS["MAIN_AGENT_ADD_AGENT"] = "添加一個代理";
$MESS["MAIN_AGENT_ADD_AGENT_TITLE"] = "添加新代理";
$MESS["MAIN_AGENT_ALERT_DELETE"] = "您確定要刪除此代理嗎？";
$MESS["MAIN_AGENT_DEACTIVATE"] = "停用";
$MESS["MAIN_AGENT_DELETE"] = "刪除";
$MESS["MAIN_AGENT_EDIT"] = "調整";
$MESS["MAIN_AGENT_FLT_ACTIVE"] = "積極的";
$MESS["MAIN_AGENT_FLT_ID"] = "代理ID";
$MESS["MAIN_AGENT_FLT_IS_PERIOD"] = "再次發生的";
$MESS["MAIN_AGENT_FLT_LAST_EXEC"] = "最後運行日期";
$MESS["MAIN_AGENT_FLT_MODULE_ID"] = "模塊";
$MESS["MAIN_AGENT_FLT_NAME"] = "代理名稱";
$MESS["MAIN_AGENT_FLT_NEXT_EXEC"] = "下一次運行的日期";
$MESS["MAIN_AGENT_FLT_PERIODICAL1"] = "運行模式：";
$MESS["MAIN_AGENT_FLT_PERIODICAL_INTERVAL"] = "定期間隔";
$MESS["MAIN_AGENT_FLT_PERIODICAL_TIME"] = "確切時間";
$MESS["MAIN_AGENT_FLT_SEARCH"] = "尋找：";
$MESS["MAIN_AGENT_FLT_SEARCH_TITLE"] = "輸入您的查詢字符串";
$MESS["MAIN_AGENT_FLT_USER_ID"] = "用戶身份";
$MESS["MAIN_AGENT_ID"] = "ID";
$MESS["MAIN_AGENT_INTERVAL"] = "間隔";
$MESS["MAIN_AGENT_LAST_EXEC"] = "最後一步";
$MESS["MAIN_AGENT_LIST_DATE_CHECK"] = "檢查日期";
$MESS["MAIN_AGENT_LIST_PAGE"] = "代理列表：";
$MESS["MAIN_AGENT_LIST_PERIODICAL"] = "運行模式";
$MESS["MAIN_AGENT_LIST_PERIODICAL_INTERVAL"] = "定期間隔";
$MESS["MAIN_AGENT_LIST_PERIODICAL_TIME"] = "確切時間";
$MESS["MAIN_AGENT_LIST_RETRY_COUNT"] = "嘗試";
$MESS["MAIN_AGENT_LIST_RUNNING"] = "跑步";
$MESS["MAIN_AGENT_MODULE_ID"] = "模塊";
$MESS["MAIN_AGENT_NAME"] = "代理功能";
$MESS["MAIN_AGENT_NEXT_EXEC"] = "下一個運行";
$MESS["MAIN_AGENT_PAGE_TITLE"] = "代理列表";
$MESS["MAIN_AGENT_SORT"] = "排序";
$MESS["MAIN_AGENT_SYSTEM_USER"] = "系統";
$MESS["MAIN_AGENT_USER_ID"] = "登入";
$MESS["MAIN_AGENT_WRONG_LAST_EXEC"] = "請在過濾器中輸入上次運行的正確日期！";
$MESS["MAIN_AGENT_WRONG_NEXT_EXEC"] = "請在過濾器中輸入下一個運行的正確日期！";
$MESS["MAIN_ALL"] = "（全部）";
$MESS["MAIN_YES"] = "是的";
