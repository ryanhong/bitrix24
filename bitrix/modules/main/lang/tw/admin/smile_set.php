<?php
$MESS["ERROR_DEL_SMILE"] = "刪除集合的錯誤。";
$MESS["SMILE_BTN_ADD_NEW"] = "新集合";
$MESS["SMILE_BTN_ADD_NEW_ALT"] = "單擊添加新集合";
$MESS["SMILE_BTN_BACK"] = "表情片畫廊";
$MESS["SMILE_DELETE_DESCR"] = "刪除";
$MESS["SMILE_DEL_CONF"] = "您確定要刪除此組嗎？這還將刪除該集合所包含的所有笑臉。";
$MESS["SMILE_EDIT"] = "編輯";
$MESS["SMILE_EDIT_DESCR"] = "編輯";
$MESS["SMILE_ID"] = "ID";
$MESS["SMILE_NAME"] = "姓名";
$MESS["SMILE_NAV"] = "套";
$MESS["SMILE_SET_NAME"] = "設置名稱＃";
$MESS["SMILE_SMILE_COUNT"] = "笑臉";
$MESS["SMILE_SORT"] = "種類";
$MESS["SMILE_STRING_ID"] = "設置名稱";
$MESS["SMILE_TITLE"] = "笑臉";
