<?php
$MESS["MURL_REINDEX_ALL"] = "（全部）";
$MESS["MURL_REINDEX_COMPLETE"] = "重新創建規則已完成。";
$MESS["MURL_REINDEX_CONTINUE"] = "繼續";
$MESS["MURL_REINDEX_MAX_SIZE"] = "最大文檔大小：";
$MESS["MURL_REINDEX_MAX_SIZE_kb"] = "KB";
$MESS["MURL_REINDEX_REINDEX_BUTTON"] = "重新創建";
$MESS["MURL_REINDEX_SITE"] = "地點：";
$MESS["MURL_REINDEX_STEP"] = "步：";
$MESS["MURL_REINDEX_STEPPED"] = "逐步創建：";
$MESS["MURL_REINDEX_STEP_sec"] = "秒";
$MESS["MURL_REINDEX_STOP"] = "停止";
$MESS["MURL_REINDEX_TAB"] = "娛樂";
$MESS["MURL_REINDEX_TAB_TITLE"] = "重新創建參數";
$MESS["MURL_REINDEX_TITLE"] = "重新創建URL處理規則";
$MESS["MURL_REINDEX_TOTAL"] = "處理的文件：";
$MESS["url_rewrite_mess_title"] = "現在重新索引：";
