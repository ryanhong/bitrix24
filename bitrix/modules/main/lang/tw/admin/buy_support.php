<?php
$MESS["BUY_SUP_AMOUNT"] = "總金額＃";
$MESS["BUY_SUP_BUTTON"] = "提交請求";
$MESS["BUY_SUP_BUY"] = "買";
$MESS["BUY_SUP_BUY_1_1"] = "當您的商業許可到期時，將對更新系統施加某些限制，某些系統功能和技術支持可用性。<br /> <br />

到期後，無法安裝平台更新；您將無法獲得新產品版本，安裝市場解決方案或擴展其訂閱。<br>
諸如CDN或云備份之類的雲服務將不可用。<br /> <br />

<b>沒有技術支持。< /b> <br /> <br />

您必須延長許可證才能繼續使用全部商業許可提供的所有功能。<br /> <br />
請注意，您仍然可以根據有限的商業許可條款繼續使用該產品。<br /> <br />
有關詳細信息，請參閱<a href= \"#link# \" target= \"_blank \">許可協議</a>。<br /> <br />";
$MESS["BUY_SUP_BUY_3"] = "<b>如果通過Bitrix合作夥伴購買的更多好處：< /b> <br />
<ul>
 <li style = \“ padding-bottom：10px; \”>安裝更新的幫助; </li>
 <li style = \“ padding-bottom：10px; \”>系統相關服務和建議; </li>
 <li style = \“ padding-bottom：10px; \”>免費的售後支持。</li>
</ul>";
$MESS["BUY_SUP_BUY_EULA_LINK_CP"] = "https://www.bitrix24.com/eula/";
$MESS["BUY_SUP_BUY_SELF"] = "續訂訂閱而無需幫助";
$MESS["BUY_SUP_CONTACT"] = "您的聯繫方式";
$MESS["BUY_SUP_CONTACT_OK2"] = "您的請求已發送給合作夥伴。合作夥伴的代表將盡快與您聯繫。";
$MESS["BUY_SUP_EMAIL"] = "電子郵件：";
$MESS["BUY_SUP_NAME"] = "你的名字：";
$MESS["BUY_SUP_PARTNER"] = "你的搭檔：";
$MESS["BUY_SUP_PARTNER_TITLE"] = "我們建議您通過伴侶更新您的訂閱";
$MESS["BUY_SUP_PHONE"] = "電話：";
$MESS["BUY_SUP_PREQUEST1"] = "合作夥伴收到您的請求後，代表將與您聯繫並隨附的詳細信息。";
$MESS["BUY_SUP_SHT"] = "件。";
$MESS["BUY_SUP_TITLE"] = "訂閱更新選項";
$MESS["BUY_SUP_TOBUY"] = "您需要購買：";
$MESS["BUY_SUP_YEMAIL"] = "你的郵件：";
$MESS["BUY_SUP_YPHONE"] = "您的手機：";
