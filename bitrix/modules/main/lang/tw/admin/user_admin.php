<?php
$MESS["ACTION"] = "動作";
$MESS["ACTIVE"] = "積極的";
$MESS["CHANGE"] = "調整";
$MESS["CHANGE_USER"] = "修改用戶設置";
$MESS["CONFIRM_DEL_USER"] = "您確定要刪除用戶嗎？";
$MESS["DATE_REGISTER"] = "註冊日期";
$MESS["DELETE"] = "刪除";
$MESS["DELETE_ERROR"] = "錯誤刪除用戶。用戶可能與其他對象連接。";
$MESS["DELETE_USER"] = "刪除用戶";
$MESS["EMAIL"] = "電子郵件";
$MESS["EXTERNAL_AUTH_ID"] = "外部授權來源（ID）";
$MESS["F_ACTIVE"] = "積極的";
$MESS["F_FIND_INTRANET_USERS"] = "Intranet用戶";
$MESS["F_GROUP"] = "組";
$MESS["F_LOGIN"] = "登入";
$MESS["F_NAME"] = "首先，最後一個或第二個名字";
$MESS["LAST_LOGIN"] = "最後授權";
$MESS["LAST_NAME"] = "姓";
$MESS["LOGIN"] = "登入";
$MESS["MAIN_ADD_USER"] = "添加用戶";
$MESS["MAIN_ADD_USER_TITLE"] = "添加新用戶";
$MESS["MAIN_ADMIN_ADD_COPY"] = "添加副本";
$MESS["MAIN_ADMIN_AUTH"] = "授權";
$MESS["MAIN_ADMIN_AUTH_TITLE"] = "授權作為此用戶";
$MESS["MAIN_ADMIN_LIST_ACTIVATE"] = "啟用";
$MESS["MAIN_ADMIN_LIST_ADD_GROUP"] = "添加到組";
$MESS["MAIN_ADMIN_LIST_ADD_STRUCT"] = "添加到公司結構";
$MESS["MAIN_ADMIN_LIST_DEACTIVATE"] = "停用";
$MESS["MAIN_ADMIN_LIST_GROUP"] = "（選擇組）";
$MESS["MAIN_ADMIN_LIST_INTRANET_DEACTIVATE"] = "清除“最後授權”字段";
$MESS["MAIN_ADMIN_LIST_REM_GROUP"] = "從組中刪除";
$MESS["MAIN_ADMIN_LIST_REM_STRUCT"] = "從公司結構中刪除";
$MESS["MAIN_ADMIN_MENU_DELETE"] = "刪除";
$MESS["MAIN_ADMIN_MENU_EDIT"] = "編輯";
$MESS["MAIN_ADMIN_MENU_VIEW"] = "看法";
$MESS["MAIN_ALL"] = "（全部）";
$MESS["MAIN_EDIT_ERROR"] = "錯誤更新記錄：";
$MESS["MAIN_EDIT_TITLE"] = "編輯用戶資料";
$MESS["MAIN_FLT_ACTIVE"] = "積極的";
$MESS["MAIN_FLT_AUTH_DATE"] = "授權";
$MESS["MAIN_FLT_EMAIL"] = "電子郵件";
$MESS["MAIN_FLT_FIO"] = "最後，第一個和/或第二個名字";
$MESS["MAIN_FLT_LOGIN"] = "登入";
$MESS["MAIN_FLT_MOD_DATE"] = "修改的";
$MESS["MAIN_FLT_PROFILE_FIELDS"] = "個人資料字段";
$MESS["MAIN_FLT_SEARCH"] = "搜尋:";
$MESS["MAIN_FLT_SEARCH_TITLE"] = "輸入文本以搜索";
$MESS["MAIN_FLT_USER_GROUP"] = "用戶組";
$MESS["MAIN_FLT_USER_ID"] = "用戶身份";
$MESS["MAIN_FROM_TILL_LAST_LOGIN"] = "上次授權\“ till \”日期必須大於\ \“日期";
$MESS["MAIN_FROM_TILL_TIMESTAMP"] = "修改日期\“ till \ \”必須大於\ \”中的\ \“";
$MESS["MAIN_F_EMAIL"] = "電子郵件";
$MESS["MAIN_F_ID"] = "ID";
$MESS["MAIN_F_KEYWORDS"] = "個人資料字段";
$MESS["MAIN_F_LAST_LOGIN"] = "最後授權";
$MESS["MAIN_F_TIMESTAMP"] = "修改的";
$MESS["MAIN_USER_ADMIN_FIELD_ID"] = "ID";
$MESS["MAIN_USER_ADMIN_PAGES"] = "頁";
$MESS["MAIN_WRONG_LAST_LOGIN_FROM"] = "輸入正確的最後授權日期\“來自\”";
$MESS["MAIN_WRONG_LAST_LOGIN_TILL"] = "輸入正確的最後授權日期\“ thil \ \”";
$MESS["MAIN_WRONG_TIMESTAMP_FROM"] = "在過濾器中輸入正確的修改日期\“來自\”";
$MESS["MAIN_WRONG_TIMESTAMP_TILL"] = "在過濾器中輸入正確的修改日期\“ till \”";
$MESS["MAIN_YES"] = "是的";
$MESS["NAME"] = "名";
$MESS["PAGES"] = "用戶";
$MESS["PERSONAL_BIRTHDAY"] = "出生日期";
$MESS["PERSONAL_CITY"] = "城市";
$MESS["PERSONAL_GENDER"] = "性別";
$MESS["PERSONAL_ICQ"] = "ICQ";
$MESS["PERSONAL_MOBILE"] = "私人蜂窩";
$MESS["PERSONAL_PHONE"] = "私人電話";
$MESS["PERSONAL_PROFESSION"] = "職稱";
$MESS["PERSONAL_STREET"] = "街道";
$MESS["PERSONAL_WWW"] = "主頁";
$MESS["SAVE_ERROR"] = "錯誤保存用戶＃";
$MESS["SECOND_NAME"] = "中間名字";
$MESS["TIMESTAMP"] = "修改的";
$MESS["TITLE"] = "用戶";
$MESS["USER_ADMIN_TITLE"] = "致敬";
$MESS["USER_DONT_KNOW"] = "（未知）";
$MESS["USER_FEMALE"] = "女性";
$MESS["USER_MALE"] = "男性";
$MESS["WORK_CITY"] = "公司城市";
$MESS["WORK_COMPANY"] = "公司";
$MESS["WORK_DEPARTMENT"] = "部門";
$MESS["WORK_PHONE"] = "公司電話";
$MESS["WORK_POSITION"] = "位置";
$MESS["WORK_WWW"] = "公司網站";
$MESS["XML_ID"] = "外部代碼";
$MESS["main_user_admin_blocked"] = "阻止";
$MESS["main_user_admin_logout"] = "登出";
$MESS["main_user_admin_logout_title"] = "強制在所有設備上註銷";
