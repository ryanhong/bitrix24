<?php
$MESS["MAIN_ADMIN_GROUP_DESC"] = "完全訪問。";
$MESS["MAIN_ADMIN_GROUP_NAME"] = "管理員";
$MESS["MAIN_DEFAULT_LANGUAGE_AM_VALUE"] = "是";
$MESS["MAIN_DEFAULT_LANGUAGE_CODE"] = "en";
$MESS["MAIN_DEFAULT_LANGUAGE_DAY_MONTH_FORMAT"] = "縮略詞";
$MESS["MAIN_DEFAULT_LANGUAGE_DAY_OF_WEEK_MONTH_FORMAT"] = "L，F J";
$MESS["MAIN_DEFAULT_LANGUAGE_DAY_SHORT_MONTH_FORMAT"] = "M J";
$MESS["MAIN_DEFAULT_LANGUAGE_FORMAT_CHARSET"] = "ISO-8859-1";
$MESS["MAIN_DEFAULT_LANGUAGE_FORMAT_DATE"] = "mm/dd/yyyy";
$MESS["MAIN_DEFAULT_LANGUAGE_FORMAT_DATETIME"] = "mm/dd/yyyy h：mi：ss t";
$MESS["MAIN_DEFAULT_LANGUAGE_FORMAT_NAME"] = "＃名稱＃＃last_name＃";
$MESS["MAIN_DEFAULT_LANGUAGE_FULL_DATE_FORMAT"] = "L，F J，Y";
$MESS["MAIN_DEFAULT_LANGUAGE_LONG_DATE_FORMAT"] = "F J，Y";
$MESS["MAIN_DEFAULT_LANGUAGE_LONG_TIME_FORMAT"] = "G：I：S A";
$MESS["MAIN_DEFAULT_LANGUAGE_MEDIUM_DATE_FORMAT"] = "M J，Y";
$MESS["MAIN_DEFAULT_LANGUAGE_NAME"] = "英語";
$MESS["MAIN_DEFAULT_LANGUAGE_NUMBER_DECIMAL_SEPARATOR"] = "。";
$MESS["MAIN_DEFAULT_LANGUAGE_NUMBER_THOUSANDS_SEPARATOR"] = "，，，，";
$MESS["MAIN_DEFAULT_LANGUAGE_PM_VALUE"] = "下午";
$MESS["MAIN_DEFAULT_LANGUAGE_SHORT_DATE_FORMAT"] = "N/J/Y";
$MESS["MAIN_DEFAULT_LANGUAGE_SHORT_DAY_OF_WEEK_MONTH_FORMAT"] = "D，F J";
$MESS["MAIN_DEFAULT_LANGUAGE_SHORT_DAY_OF_WEEK_SHORT_MONTH_FORMAT"] = "D，M J";
$MESS["MAIN_DEFAULT_LANGUAGE_SHORT_TIME_FORMAT"] = "G：我";
$MESS["MAIN_DEFAULT_SITE_FORMAT_CHARSET"] = "ISO-8859-1";
$MESS["MAIN_DEFAULT_SITE_FORMAT_DATE"] = "mm/dd/yyyy";
$MESS["MAIN_DEFAULT_SITE_FORMAT_DATETIME"] = "mm/dd/yyyy h：mi：ss t";
$MESS["MAIN_DEFAULT_SITE_FORMAT_NAME"] = "＃名稱＃＃last_name＃";
$MESS["MAIN_DEFAULT_SITE_NAME"] = "默認網站";
$MESS["MAIN_DESKTOP_CREATEDBY_KEY"] = "由...製作";
$MESS["MAIN_DESKTOP_CREATEDBY_VALUE"] = "Bitrix24";
$MESS["MAIN_DESKTOP_EMAIL_KEY"] = "電子郵件";
$MESS["MAIN_DESKTOP_EMAIL_VALUE"] = "<a href= \"mailto:info@bitrixsoft.com \"> info@bitrixsoft.com </a>";
$MESS["MAIN_DESKTOP_INFO_TITLE"] = "網站信息";
$MESS["MAIN_DESKTOP_PRODUCTION_KEY"] = "發行";
$MESS["MAIN_DESKTOP_PRODUCTION_VALUE"] = "12.12.2011";
$MESS["MAIN_DESKTOP_RESPONSIBLE_KEY"] = "行政人員";
$MESS["MAIN_DESKTOP_RESPONSIBLE_VALUE"] = "約翰·多伊";
$MESS["MAIN_DESKTOP_RSS_TITLE"] = "Bitrix新聞";
$MESS["MAIN_DESKTOP_URL_KEY"] = "網址";
$MESS["MAIN_DESKTOP_URL_VALUE"] = "<a href= \"http://www.bitrixsoft.com \"> www.bitrixsoft.com </a>";
$MESS["MAIN_EVENT_MESS_NOTIFICATION"] = "事件日誌通知：＃名稱＃";
$MESS["MAIN_EVENT_MESS_NOTIFICATION_TEXT"] = "發現與通知參數匹配的日誌事件：

事件類型：＃audit_type_id＃
對象：＃item_id＃
用戶：＃user_id＃
IP地址：＃遠程_ADDR＃
瀏覽器：＃user_agent＃
頁面網址：＃request_uri＃

事件數：＃event_count＃

＃附加_text＃

轉到事件日誌：
http：//#server_name#/bitrix/admin/admin/event_log.php？set_filter = y＆find_audit_type_id =＃audit_type_id＃";
$MESS["MAIN_EVERYONE_GROUP_DESC"] = "所有用戶（包括未授權的用戶）。";
$MESS["MAIN_EVERYONE_GROUP_NAME"] = "所有用戶（未經授權的用戶）";
$MESS["MAIN_INSTALL_DB_ERROR"] = "無法連接到數據庫。請檢查參數。";
$MESS["MAIN_INSTALL_EVENT_MESSAGE_NEW_DEVICE_LOGIN"] = "你好＃名稱＃，

新設備剛剛使用您的登錄＃登錄＃簽名。
 
設備：＃設備＃
瀏覽器：＃瀏覽器＃
平台：＃平台＃
位置：＃位置＃（大約）
日期：＃日期＃

我們建議您立即更改密碼，如果不是您，或者登錄不代表您。
";
$MESS["MAIN_INSTALL_EVENT_MESSAGE_NEW_DEVICE_LOGIN_SUBJECT"] = "簽名的新設備";
$MESS["MAIN_INSTALL_EVENT_MESS_USER_CODE_REQUEST"] = "＃site_name＃：請求驗證代碼";
$MESS["MAIN_INSTALL_EVENT_MESS_USER_CODE_REQUEST_MESS"] = "使用以下代碼登錄：

＃校驗＃＃

登錄後，您可以在用戶配置文件中更改密碼。

您的註冊信息：

用戶ID：＃USER_ID＃
帳戶狀態：＃狀態＃
登錄：＃登錄＃

此消息是自動創建的。";
$MESS["MAIN_INSTALL_EVENT_TYPE_NEW_DEVICE_LOGIN"] = "簽名的新設備";
$MESS["MAIN_INSTALL_EVENT_TYPE_NEW_DEVICE_LOGIN_DESC"] = "＃USER_ID＃ - 用戶ID
＃電子郵件＃-用戶電子郵件：
＃登錄＃-用戶登錄
＃名稱＃-用戶名
＃last_name＃ - 用戶姓氏
＃設備＃-設備
＃瀏覽器＃ - 瀏覽器
＃平台＃ - 平台
＃user_agent＃ - 用戶代理
＃IP＃ -  IP地址
＃日期＃-日期
＃國家 - 國家 /地區
＃區域＃-區域
＃城市＃城市
＃位置＃ - 完整位置（城市，地區，國家）
";
$MESS["MAIN_INSTALL_EVENT_TYPE_NOTIFICATION"] = "事件日誌通知";
$MESS["MAIN_INSTALL_EVENT_TYPE_NOTIFICATION_DESC"] = "＃電子郵件＃-收件人電子郵件
＃附加_TEXT＃ - 操作其他文本
＃名稱＃ - 通知名稱
＃audit_type_id＃ - 事件類型
＃item_id＃ - 對象
＃USER_ID＃ - 用戶
＃遠程_ADDR＃ -IP地址
＃user_agent＃ -browser
＃request_uri＃ - 頁面URL
＃event_count＃ - 事件數量";
$MESS["MAIN_INSTALL_EVENT_TYPE_NOTIFICATION_DESC_SMS"] = "＃phone_number＃ - 收件人電話號碼
＃附加_TEXT＃ - 操作其他文本
＃名稱＃ - 通知名稱
＃audit_type_id＃ - 事件類型
＃item_id＃ - 對象
＃USER_ID＃ - 用戶
＃遠程_ADDR＃ -IP地址
＃user_agent＃ -browser
＃request_uri＃ - 頁面URL
＃event_count＃ - 事件數量";
$MESS["MAIN_INSTALL_EVENT_TYPE_USER_CODE_REQUEST"] = "請求驗證代碼";
$MESS["MAIN_INSTALL_EVENT_TYPE_USER_CODE_REQUEST_DESC"] = "＃USER_ID＃ - 用戶ID
＃狀態＃-登錄狀態
＃登錄＃-登錄
＃校大＃-驗證代碼
＃名稱＃-名字
＃last_name＃ - 姓氏
＃電子郵件＃-用戶電子郵件
";
$MESS["MAIN_MAIL_CONFIRM_EVENT_TYPE_DESC"] = "

＃email_to＃ - 確認電子郵件地址
＃message_subject＃ - 消息主題
＃juccess_code＃ - 確認代碼";
$MESS["MAIN_MAIL_CONFIRM_EVENT_TYPE_NAME"] = "確認發件人的電子郵件地址";
$MESS["MAIN_MODULE_DESC"] = "產品內核";
$MESS["MAIN_MODULE_NAME"] = "主模塊";
$MESS["MAIN_NEW_USER_CONFIRM_EVENT_DESC"] = "來自＃site_name＃的問候！
-------------------------------------------------- --------

你好，

您已經收到此消息，因為您（或其他人）使用您的電子郵件在＃server_name＃上註冊。

您的註冊確認代碼：＃vensir_code＃

請使用以下鏈接驗證和激活您的註冊：
http：//#server_name#/auth/index.php？eskigres_registration = yes＆escresenge_user_id =＃user_id＃＆virScker_code =＃juccess_code＃＃

或者，在瀏覽器中打開此鏈接，然後手動輸入代碼：
http：//#server_name#/auth/index.php？enckern_registration = yes＆escresenge_user_id =＃user_id＃＃

注意力！在確認註冊之前，您的帳戶將不會被激活。

-------------------------------------------------- --------------------------- ----------------------- ----

自動生成的消息。";
$MESS["MAIN_NEW_USER_CONFIRM_EVENT_NAME"] = "＃site_name＃：新用戶註冊確認";
$MESS["MAIN_NEW_USER_CONFIRM_TYPE_DESC"] = "

＃USER_ID＃ - 用戶ID
＃登錄＃-登錄
＃電子郵件＃-電子郵件
＃名稱＃-名字
＃last_name＃ - 姓氏
＃USER_IP＃ - 用戶IP
＃USER_HOST＃ - 用戶主機
＃juccess_code＃ - 確認代碼
";
$MESS["MAIN_NEW_USER_CONFIRM_TYPE_NAME"] = "新用戶註冊確認";
$MESS["MAIN_NEW_USER_EVENT_DESC"] = "來自＃site_name＃的信息消息
-------------------------------------------------- -----

新用戶已在網站＃server_name＃上成功註冊。

用戶詳細信息：
用戶ID：＃USER_ID＃

名稱：＃名稱＃
姓氏：＃last_name＃
用戶的電子郵件：＃電子郵件＃

登錄：＃登錄＃

自動生成的消息。";
$MESS["MAIN_NEW_USER_EVENT_NAME"] = "＃site_name＃：新用戶已在網站上註冊";
$MESS["MAIN_NEW_USER_TYPE_DESC"] = "

＃USER_ID＃ - 用戶ID
＃登錄＃-登錄
＃電子郵件＃-電子郵件
＃名稱＃-名稱
＃last_name＃ - 姓氏
＃USER_IP＃ - 用戶IP
＃USER_HOST＃ - 用戶主機
";
$MESS["MAIN_NEW_USER_TYPE_NAME"] = "新用戶已註冊";
$MESS["MAIN_RATING_AUTHORITY_NAME"] = "權威";
$MESS["MAIN_RATING_NAME"] = "評分";
$MESS["MAIN_RATING_TEXT_LIKE_D"] = "喜歡";
$MESS["MAIN_RATING_TEXT_LIKE_N"] = "與眾不同";
$MESS["MAIN_RATING_TEXT_LIKE_Y"] = "喜歡";
$MESS["MAIN_RULE_ADD_GROUP_AUTHORITY_NAME"] = "註冊允許投票給權限的小組用戶";
$MESS["MAIN_RULE_ADD_GROUP_RATING_NAME"] = "註冊小組用戶允許投票給評級";
$MESS["MAIN_RULE_AUTO_AUTHORITY_VOTE_NAME"] = "用戶權威自動錄取";
$MESS["MAIN_RULE_REM_GROUP_AUTHORITY_NAME"] = "刪除從被禁止投票給權限的小組用戶";
$MESS["MAIN_RULE_REM_GROUP_RATING_NAME"] = "刪除從被禁止投票給評級的小組用戶";
$MESS["MAIN_SMILE_DEF_SET_NAME"] = "默認設置";
$MESS["MAIN_USER_INFO_EVENT_DESC"] = "來自＃site_name＃的信息消息
-------------------------------------------------- -----

＃名稱＃＃last_name＃，

#訊息#

您的註冊信息：

用戶ID：＃USER_ID＃
帳戶狀態：＃狀態＃
登錄：＃登錄＃

要更改密碼，請訪問下面的鏈接：
http：//#server_name#/auth/index.php？change_password = yes＆lang = en＆user_checkword =＃checkword＃＆user_login =＃url_login＃

自動生成的消息。";
$MESS["MAIN_USER_INFO_EVENT_NAME"] = "＃site_name＃：註冊信息";
$MESS["MAIN_USER_INFO_TYPE_DESC"] = "

＃USER_ID＃ - 用戶ID
＃狀態＃-帳戶狀態
＃消息＃ - 用戶的消息
＃登錄＃-登錄
＃url_login＃ - 用於URL的編碼登錄
＃名稱＃-檢查字符串是否密碼更改
＃名稱＃-名稱
＃last_name＃ - 姓氏
＃電子郵件＃-用戶電子郵件
";
$MESS["MAIN_USER_INFO_TYPE_NAME"] = "帳戶信息";
$MESS["MAIN_USER_INVITE_EVENT_DESC"] = "網站＃site_name＃的信息消息＃
-------------------------------------------------- --------
你好＃名稱＃＃last_name＃！

管理員已將您添加到註冊網站用戶中。

我們邀請您訪問我們的網站。

您的註冊信息：

用戶ID：＃ID＃
登錄：＃登錄＃

我們建議您更改自動生成的密碼。

要更改密碼，請點擊鏈接：
http：//#server_name#/auth.php？change_password = yes＆user_login =＃url_login＃＆user_checkword =＃chockword＃＃";
$MESS["MAIN_USER_INVITE_EVENT_NAME"] = "＃site_name＃：網站邀請";
$MESS["MAIN_USER_INVITE_TYPE_DESC"] = "＃id＃ - 用戶ID
＃登錄＃-登錄
＃url_login＃ - 用於URL的編碼登錄
＃電子郵件＃-電子郵件
＃名稱＃-名稱
＃last_name＃ - 姓氏
＃密碼＃ - 用戶密碼
＃校大＃-密碼檢查字符串
＃xml_id＃ - 與外部數據源鏈接的用戶ID

";
$MESS["MAIN_USER_INVITE_TYPE_NAME"] = "新網站用戶的邀請";
$MESS["MAIN_USER_PASS_CHANGED_EVENT_DESC"] = "來自＃site_name＃的信息消息
-------------------------------------------------- -----

＃名稱＃＃last_name＃，

#訊息#

您的註冊信息：

用戶ID：＃USER_ID＃
帳戶狀態：＃狀態＃
登錄：＃登錄＃

自動生成的消息。";
$MESS["MAIN_USER_PASS_CHANGED_EVENT_NAME"] = "＃site_name＃：密碼更改確認確認";
$MESS["MAIN_USER_PASS_CHANGED_TYPE_NAME"] = "密碼更改確認";
$MESS["MAIN_USER_PASS_REQUEST_EVENT_DESC"] = "來自＃site_name＃的信息消息
-------------------------------------------------- -----

＃名稱＃＃last_name＃，

#訊息#

要更改密碼，請訪問下面的鏈接：
http：//#server_name#/auth/index.php？change_password = yes＆lang = en＆user_checkword =＃checkword＃＆user_login =＃url_login＃

您的註冊信息：

用戶ID：＃USER_ID＃
帳戶狀態：＃狀態＃
登錄：＃登錄＃

自動生成的消息。";
$MESS["MAIN_USER_PASS_REQUEST_EVENT_NAME"] = "＃site_name＃：密碼更改請求";
$MESS["MAIN_USER_PASS_REQUEST_TYPE_NAME"] = "密碼更改請求";
$MESS["MAIN_VOTE_AUTHORITY_GROUP_DESC"] = "該用戶組的成員資格自動管理。";
$MESS["MAIN_VOTE_AUTHORITY_GROUP_NAME"] = "用戶允許投票給權威";
$MESS["MAIN_VOTE_RATING_GROUP_DESC"] = "該用戶組的成員資格自動管理。";
$MESS["MAIN_VOTE_RATING_GROUP_NAME"] = "用戶允許投票給評級";
$MESS["MF_EVENT_DESCRIPTION"] = "＃作者＃ - 消息作者
＃fure_email＃ - 作者的電子郵件地址
＃文字＃ - 消息文字
＃email_from＃ - 發件人的電子郵件地址
＃email_to＃ - 收件人的電子郵件地址";
$MESS["MF_EVENT_MESSAGE"] = "來自＃site_name＃的通知
-------------------------------------------------- --------

從反饋表中發送了一條消息。

發送者：＃作者＃
發件人的電子郵件：＃fure_email＃

消息文字：
#文字#

該通知已自動生成。";
$MESS["MF_EVENT_NAME"] = "使用反饋表發送消息";
$MESS["MF_EVENT_SUBJECT"] = "＃site_name＃：反饋表格消息";
$MESS["main_install_sms_event_confirm_descr"] = "＃USER_PHONE＃ - 電話號碼
＃代碼＃-確認代碼";
$MESS["main_install_sms_event_confirm_name"] = "使用短信驗證電話號碼";
$MESS["main_install_sms_event_restore_descr"] = "＃USER_PHONE＃ - 電話號碼
＃代碼＃ - 恢復確認代碼";
$MESS["main_install_sms_event_restore_name"] = "使用短信恢復密碼";
$MESS["main_install_sms_template_confirm_mess"] = "確認代碼：＃代碼＃";
$MESS["main_install_sms_template_notification_mess"] = "＃名稱＃：＃其他_text＃（事件：＃event_count＃）";
$MESS["main_install_sms_template_restore_mess"] = "恢復密碼的代碼：＃代碼＃";
