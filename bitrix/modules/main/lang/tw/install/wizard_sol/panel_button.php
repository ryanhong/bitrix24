<?php
$MESS["SOL_BUTTON_CONFIRM_W2"] = "注意：刪除按鈕後，您可以使用嚮導頁面或“添加新站點”表單從控制面板啟動新的解決方案嚮導。您想從面板中刪除按鈕嗎？";
$MESS["SOL_BUTTON_DEL_TEXT"] = "刪除按鈕";
$MESS["SOL_BUTTON_DEL_TITLE"] = "從面板中刪除按鈕";
$MESS["SOL_BUTTON_GOTOSITE"] = "訪問網站";
$MESS["SOL_BUTTON_TEST_MENU_HINT"] = "單擊此按鈕以選擇安裝解決方案，刪除此按鈕或打開另一個站點。";
$MESS["SOL_BUTTON_TEST_TEXT"] = "測試新的＃BR＃解決方案";
$MESS["SOL_BUTTON_TEST_TEXT_HINT"] = "選擇一個新解決方案要安裝。";
$MESS["SOL_BUTTON_TEST_TITLE"] = "創建一個新網站並啟動新解決方案嚮導";
