<?php
$MESS["ACCESS_DENIED"] = "拒絕訪問。";
$MESS["ACCESS_DENIED_FILE"] = "文件＃文件＃無法查看。";
$MESS["main_include_decode_pass_err"] = "密碼解密錯誤（＃errcode＃）。";
$MESS["main_include_decode_pass_sess"] = "您的會議已經過期。請再次授權。";
