<?php
$MESS["ADMIN_INCLAREA_DBLCLICK"] = "按兩下";
$MESS["ADMIN_SHOW_MODE_OFF"] = "離開";
$MESS["ADMIN_SHOW_MODE_OFF_HINT"] = "編輯模式關閉";
$MESS["ADMIN_SHOW_MODE_ON"] = "在";
$MESS["ADMIN_SHOW_MODE_ON_HINT"] = "編輯模式已打開。該頁面正在顯示組件的控件，並包括區域。";
$MESS["AMDIN_SHOW_MODE_TITLE"] = "編輯模式";
