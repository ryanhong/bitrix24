<?php
$MESS["MAIN_MAP_NAME"] = "站點圖";
$MESS["MAIN_MAP_TEMPLATE_DESCRIPTION"] = "站點地圖顯示的標準組件";
$MESS["MAIN_MAP_TEMPLATE_NAME"] = "站點地圖（默認情況下）";
$MESS["MAIN_SECTION_NAME"] = "核心";
$MESS["T_MAIN_PROFILE"] = "用戶資料";
$MESS["T_MAIN_PROFILE_DESCRIPTION"] = "編輯用戶資料";
