<?php
$MESS["GD_INFO_LASTUPDATE"] = "最後更新：＃值＃";
$MESS["GD_INFO_PERFMON"] = "當前性能得分：＃值＃";
$MESS["GD_INFO_PERFMON_NO_RESULT"] = "未測試";
$MESS["GD_INFO_USERS"] = "用戶計數：＃值＃";
$MESS["GD_INFO_product"] = "供電";
$MESS["GD_INFO_product_name_1c_bitrix"] = "1C-Bitrix網站經理";
$MESS["GD_INFO_product_name_1c_bitrix_eduportal"] = "1C-Bitrix教育門戶";
$MESS["GD_INFO_product_name_1c_bitrix_gosportal"] = "1C-Bitrix政府門戶";
$MESS["GD_INFO_product_name_1c_bitrix_gossite"] = "1C-Bitrix政府站點";
$MESS["GD_INFO_product_name_1c_bitrix_portal"] = "1C-Bitrix Intranet";
$MESS["GD_INFO_product_name_bitrix"] = "Bitrix網站管理器";
$MESS["GD_INFO_product_name_bitrix_portal"] = "Bitrix24";
