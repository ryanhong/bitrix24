<?php
$MESS["GD_SPEED_FAST"] = "快速地";
$MESS["GD_SPEED_NOT_FAST"] = "平均的";
$MESS["GD_SPEED_NO_DATA"] = "沒有數據";
$MESS["GD_SPEED_SLOW"] = "慢的";
$MESS["GD_SPEED_TITLE"] = "站點速度";
$MESS["GD_SPEED_UNIT"] = "秒";
$MESS["GD_SPEED_VERY_FAST"] = "非常快";
$MESS["GD_SPEED_VERY_SLOW"] = "非常<br>慢";
