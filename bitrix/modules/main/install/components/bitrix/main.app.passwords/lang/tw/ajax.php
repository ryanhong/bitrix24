<?php
$MESS["main_app_passwords_ajax_deleted"] = "密碼已刪除。";
$MESS["main_app_passwords_ajax_error"] = "錯誤處理請求。";
$MESS["main_app_passwords_ajax_error_auth"] = "你沒有登錄。";
$MESS["main_app_passwords_ajax_error_sess"] = "您的會議已經過期。";
$MESS["main_app_passwords_ajax_no_app"] = "未指定應用程序。";
