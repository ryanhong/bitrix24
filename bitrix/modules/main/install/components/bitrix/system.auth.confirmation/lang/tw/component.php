<?php
$MESS["CC_BSAC_MESSAGE_E01"] = "找不到用戶。";
$MESS["CC_BSAC_MESSAGE_E02"] = "授權成功完成。";
$MESS["CC_BSAC_MESSAGE_E03"] = "用戶註冊已經確認。";
$MESS["CC_BSAC_MESSAGE_E04"] = "缺少用戶註冊代碼。";
$MESS["CC_BSAC_MESSAGE_E05"] = "用戶註冊代碼不正確。";
$MESS["CC_BSAC_MESSAGE_E06"] = "用戶註冊已成功確認。";
$MESS["CC_BSAC_MESSAGE_E07"] = "確認註冊時發生了錯誤。請聯繫服務器管理員。";
