<?php
$MESS["MPL_MAIL_ADD_COMMENT_BUTTON"] = "添加評論";
$MESS["MPL_MAIL_ADD_COMMENT_LINK"] = "添加評論";
$MESS["MPL_MAIL_DESCRIPTION"] = "在上面或下方的評論中回复此電子郵件。";
$MESS["MPL_MAIL_MORE_COMMENTS"] = "查看以前的評論（＃num＃）";
$MESS["MPL_MAIL_MORE_COMMENTS1"] = "以前的評論（＃num＃）";
