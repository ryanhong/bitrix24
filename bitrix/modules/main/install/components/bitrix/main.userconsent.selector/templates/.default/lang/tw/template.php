<?php
$MESS["MAIN_USER_CONSENT_SELECTOR_BTN_CONSENT"] = "獲得了同意";
$MESS["MAIN_USER_CONSENT_SELECTOR_BTN_CREATE"] = "創建新的同意";
$MESS["MAIN_USER_CONSENT_SELECTOR_BTN_CREATE_1"] = "創建新的同意";
$MESS["MAIN_USER_CONSENT_SELECTOR_BTN_EDIT"] = "編輯設置";
$MESS["MAIN_USER_CONSENT_SELECTOR_CHOOSE"] = "選擇同意";
$MESS["MAIN_USER_CONSENT_SELECTOR_DEF_NOT_SELECTED"] = "（未選中的）";
