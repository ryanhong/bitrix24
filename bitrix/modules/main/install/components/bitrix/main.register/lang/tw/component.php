<?php
$MESS["REGISTER_DEFAULT_TITLE"] = "新的用戶註冊";
$MESS["REGISTER_FIELD_REQUIRED"] = "字段＃field_name＃是必需的";
$MESS["REGISTER_USER_WITH_EMAIL_EXIST"] = "具有此電子郵件地址（＃電子郵件＃）的用戶已經存在。";
$MESS["REGISTER_WRONG_CAPTCHA"] = "錯誤的驗證碼代碼";
$MESS["main_register_decode_err"] = "密碼解密錯誤（＃errcode＃）。";
$MESS["main_register_error_sms"] = "不正確的SMS確認代碼。";
$MESS["main_register_sess_expired"] = "您的會議已經過期。請嘗試再次註冊。";
