<?php
$MESS["AUTH_TIP"] = "指定在成功註冊後自動授權用戶。否則，將顯示授權表格。";
$MESS["REQUIRED_FIELDS_TIP"] = "在此處指定強制性申請。";
$MESS["SEF_FOLDER_TIP"] = "在此處指定頁面處於活動狀態時將在地址欄中顯示的文件夾名稱。";
$MESS["SEF_MODE_TIP"] = "啟用S​​EF模式。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將將頁面標題設置為<b> <b>新用戶註冊</b>。";
$MESS["SHOW_FIELDS_TIP"] = "在此處選擇要在註冊表單中顯示的其他字段。";
$MESS["SUCCESS_PAGE_TIP"] = "指定最終頁面在成功註冊後將用戶重定向到該頁面。";
$MESS["USER_PROPERTY_TIP"] = "選擇要在用戶配置文件中顯示的其他屬性";
$MESS["USE_BACKURL_TIP"] = "如果頁面URL包含<b> Backurl </b>參數，則在註冊後將用戶重定向到此頁面。";
