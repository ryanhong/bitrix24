<?php
$MESS["inerface_grid_earlier"] = "前";
$MESS["inerface_grid_exact"] = "確切地";
$MESS["inerface_grid_interval"] = "日期範圍";
$MESS["inerface_grid_last"] = "最近的";
$MESS["inerface_grid_later"] = "後";
$MESS["inerface_grid_month"] = "這個月";
$MESS["inerface_grid_month_ago"] = "上個月";
$MESS["inerface_grid_today"] = "今天";
$MESS["inerface_grid_tomorrow"] = "明天";
$MESS["inerface_grid_week"] = "本星期";
$MESS["inerface_grid_week_ago"] = "上星期";
$MESS["inerface_grid_yesterday"] = "昨天";
$MESS["interface_grid_no_no_no_2"] = "（不）";
