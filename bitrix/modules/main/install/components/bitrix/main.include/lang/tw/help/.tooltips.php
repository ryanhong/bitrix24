<?php
$MESS["AREA_FILE_RECURSIVE_TIP"] = "如果檢查了，則將遞歸包含子部分的包含區域（即，如果嵌套部分包含一個包含區域，則將被激活）。如果當前部分沒有包含區域，則係統將遞歸地將部分行走至根部，並包括第一個發現的區域。";
$MESS["AREA_FILE_SHOW_TIP"] = "指定包含區域範圍：<br /> <b>頁面< /b>  - 僅將包含區域添加到當前頁面； <br /> <b>節。";
$MESS["AREA_FILE_SUFFIX_TIP"] = "指定將附加到所有新的包含區域的文件名稱後綴。所有後綴的所有頁面都將被視為包括區域。";
$MESS["EDIT_MODE_TIP"] = "當用戶打開包含區域以從公共部分編輯時，將使用選定的編輯模式。";
$MESS["EDIT_TEMPLATE_TIP"] = "在此處選擇所需的頁面模板（存儲在/bitrix/templates/.default/page_templates/中）。您可以選擇<b> <i>其他</i>並指定所需模板的完整路徑。";
