<?php
$MESS["MAIN_AUTH_OTP_FIELD_CAPTCHA"] = "輸入您在圖片上看到的字符";
$MESS["MAIN_AUTH_OTP_FIELD_OTP"] = "一次性密碼";
$MESS["MAIN_AUTH_OTP_FIELD_REMEMBER"] = "在這台計算機上記住代碼";
$MESS["MAIN_AUTH_OTP_FIELD_SUBMIT"] = "登入";
$MESS["MAIN_AUTH_OTP_HEADER"] = "請輸入您的一次密碼";
$MESS["MAIN_AUTH_OTP_URL_AUTH_URL"] = "驗證";
$MESS["MAIN_AUTH_OTP_URL_REGISTER_URL"] = "登記";
