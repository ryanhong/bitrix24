<?php
$MESS["MAIN_NUMERATOR_EDIT_SEQUENCE_ERROR_NUMBER_LESS"] = "數字應該大於當前的數字";
$MESS["MAIN_NUMERATOR_EDIT_SEQUENCE_ERROR_NUMBER_NOT_NUMERIC"] = "數字應僅包括數字字符";
$MESS["MAIN_NUMERATOR_EDIT_SEQUENCE_ERROR_NUMERATOR_NOT_FOUND"] = "找不到自動編號模板";
$MESS["MAIN_NUMERATOR_EDIT_SEQUENCE_ERROR_TITLE"] = "內部錯誤。";
$MESS["MAIN_NUMERATOR_EDIT_SEQUENCE_PAGE_TITLE"] = "當前數字";
$MESS["NUMERATOR_EDIT_SEQUENCE_COLUMN_HEADER_NEXT_NUMBER"] = "下一個數字將是";
$MESS["NUMERATOR_EDIT_SEQUENCE_COLUMN_HEADER_TEXT_KEY"] = "編號模板鍵";
