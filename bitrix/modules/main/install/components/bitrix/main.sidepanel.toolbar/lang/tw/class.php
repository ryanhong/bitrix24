<?php
$MESS["SIDEPANEL_TOOLBAR_HINT"] = "最小化頁面並在它們之間快速切換。這是處理多個頁面或表格之類的最簡單方法，例如任務，交易，潛在客戶等。";
$MESS["SIDEPANEL_TOOLBAR_HINT_TITLE"] = "保留您的頁面";
