<?php
$MESS["MAIN_MENU_ADD"] = "添加菜單項";
$MESS["MAIN_MENU_ADD_NEW"] = "在當前部分創建菜單";
$MESS["MAIN_MENU_ADD_TOP_PANEL_ITEM_ALT"] = "創建\“＃菜單_title＃\”當前部分中";
$MESS["MAIN_MENU_ADD_TOP_PANEL_ITEM_TEXT"] = "創建＃Menu_title＃";
$MESS["MAIN_MENU_DEL_TOP_PANEL_ITEM_ALT"] = "刪除菜單\“＃Menu_title＃\”當前部分";
$MESS["MAIN_MENU_DEL_TOP_PANEL_ITEM_TEXT"] = "刪除\“＃菜單_title＃\”";
$MESS["MAIN_MENU_EDIT"] = "修改菜單項";
$MESS["MAIN_MENU_TOP_PANEL_BUTTON_ALT"] = "編輯菜單項";
$MESS["MAIN_MENU_TOP_PANEL_BUTTON_HINT"] = "打開菜單編輯表格。單擊箭頭編輯所有當前頁面菜單或創建新菜單。";
$MESS["MAIN_MENU_TOP_PANEL_BUTTON_TEXT"] = "菜單";
$MESS["MAIN_MENU_TOP_PANEL_ITEM_ALT"] = "編輯\“＃菜單_title＃\”菜單的項目";
$MESS["MAIN_MENU_TOP_PANEL_ITEM_TEXT"] = "編輯＃menu_title＃";
$MESS["menu_comp_del_conf"] = "您要在當前部分中刪除菜單文件\“＃菜單_title＃\”？";
$MESS["menu_comp_del_menu"] = "當前部分刪除菜單";
