<?php
$MESS["CP_SPN_TPL_LINE_COUNT"] = "每行項目";
$MESS["CP_SPN_TPL_MAX_RATIO_LINE_COUNT"] = "每頁最大項目行";
$MESS["CP_SPN_TPL_MIN_RATIO_LINE_COUNT"] = "每頁最小項目行";
$MESS["CP_SPN_TPL_PAGE_SIZES"] = "頁面大小";
$MESS["CP_SPN_TPL_PAGE_SIZE_FROM_LINE_COUNT"] = "頁面大小是表行項目計數的多個";
$MESS["CP_SPN_TPL_USE_PAGE_SIZE"] = "用戶可以選擇頁面大小";
$MESS["LINE_COUNT_TIP"] = "每行的項目數用於估計頁面大小";
$MESS["MAX_RATIO_LINE_COUNT_TIP"] = "此值指定每個頁面可能的最大項目。";
$MESS["MIN_RATIO_LINE_COUNT_TIP"] = "此值指定每個頁面可能的最低項目。如果每行的項目數為1，則此值不能小於3。";
$MESS["PAGE_SIZES_TIP"] = "標準頁面尺寸。用逗號分開多個值。";
$MESS["PAGE_SIZE_FROM_LINE_COUNT_TIP"] = "如果已檢查，頁面大小是表行項目計數的多個";
$MESS["USE_PAGE_SIZE_TIP"] = "用戶可以更改每個頁面的項目數量";
