<?php
$MESS["nav_all"] = "全部";
$MESS["nav_all_descr"] = "所有條目";
$MESS["nav_next_title"] = "下一頁";
$MESS["nav_of"] = "的";
$MESS["nav_page_current_title"] = "當前頁面";
$MESS["nav_page_num_title"] = "頁＃數字＃";
$MESS["nav_pages"] = "頁：";
$MESS["nav_prev_title"] = "上一頁";
$MESS["nav_show_pages"] = "頁面";
$MESS["nav_size_descr"] = "每頁項目：";
