<?php
$MESS["MAIN_MAIL_BLACKLIST_BLACKLIST_TITLE"] = "黑名單";
$MESS["MAIN_MAIL_BLACKLIST_BTN_REMOVE_FROM_L"] = "從列表中刪除";
$MESS["MAIN_MAIL_BLACKLIST_BTN_REMOVE_FROM_L_TITLE"] = "從列表中刪除";
$MESS["MAIN_MAIL_BLACKLIST_DELETE_CONFIRM"] = "你確定要刪除這個項目嗎？";
$MESS["MAIN_MAIL_BLACKLIST_DELETE_CONFIRM_TITLE"] = "確認刪除";
$MESS["MAIN_MAIL_BLACKLIST_DELETE_ERROR"] = "無法刪除項目";
$MESS["MAIN_MAIL_BLACKLIST_REMOVE_BTN"] = "刪除";
$MESS["MAIN_MAIL_BLACKLIST_REMOVE_BTN_TITLE"] = "刪除";
