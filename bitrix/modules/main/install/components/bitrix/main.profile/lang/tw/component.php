<?php
$MESS["PROFILE_DEFAULT_TITLE"] = "用戶資料";
$MESS["USER_DONT_KNOW"] = "（未知）";
$MESS["main_profile_decode_err"] = "密碼解密錯誤（＃errcode＃）。";
$MESS["main_profile_sess_expired"] = "您的會議已經過期。請再試一次。";
$MESS["main_profile_sms_error"] = "不正確的SMS確認代碼。";
$MESS["main_profile_update"] = "用戶個人資料更新了。";
