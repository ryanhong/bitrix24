<?php
$MESS["AUTH_OTP_AUTHORIZE"] = "登入";
$MESS["AUTH_OTP_AUTH_BACK"] = "返回登錄屏幕";
$MESS["AUTH_OTP_CAPTCHA_PROMT"] = "輸入您在圖片上看到的字符";
$MESS["AUTH_OTP_OTP"] = "一次性密碼";
$MESS["AUTH_OTP_PLEASE_AUTH"] = "請輸入您的一次密碼";
$MESS["AUTH_OTP_REMEMBER_ME"] = "記住這台計算機上的一次性密碼";
