<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>否CAHCE </i>：沒有執行緩存。";
$MESS["COL_NUM_TIP"] = "指定網站圖的列數。";
$MESS["LEVEL_TIP"] = "指定在網站圖中顯示的級別數量。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將將頁面標題設置為<b>站點地圖</b>。";
$MESS["SHOW_DESCRIPTION_TIP"] = "如果已檢查，則將顯示部分描述（如果有）。";
