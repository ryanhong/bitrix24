<?php
$MESS["INPUT_NAME_FINISH_TIP"] = "範圍內最後一個字段的名稱";
$MESS["INPUT_NAME_TIP"] = "範圍內的第一個字段的名稱";
$MESS["INPUT_VALUE_FINISH_TIP"] = "範圍內最後值的默認值";
$MESS["INPUT_VALUE_TIP"] = "範圍中第一個值的默認值";
$MESS["SHOW_INPUT_TIP"] = "日曆顯示模式";
$MESS["SHOW_TIME_TIP"] = "允許在日期字段中輸入時間";
