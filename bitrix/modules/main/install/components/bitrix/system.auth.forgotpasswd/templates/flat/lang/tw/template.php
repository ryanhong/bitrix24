<?php
$MESS["AUTH_AUTH"] = "授權";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "選擇密碼更改方法：";
$MESS["AUTH_GET_CHECK_STRING"] = "獲取檢查字符串";
$MESS["AUTH_LOGIN_EMAIL"] = "登錄或電子郵件";
$MESS["AUTH_SEND"] = "發送";
$MESS["forgot_pass_email_note"] = "您的帳戶信息將通過電子郵件發送給您。";
$MESS["forgot_pass_phone_number"] = "電話號碼";
$MESS["forgot_pass_phone_number_note"] = "更改密碼的代碼將發送給您的手機。";
$MESS["system_auth_captcha"] = "輸入您在圖片上看到的字符";
