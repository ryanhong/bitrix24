<?php
$MESS["MAIN_MAIL_CONFIRM_MESSAGE_FAQ_A1"] = "我們要求您確認您的電子郵件地址，以防止欺騙，並確保擁有發送電子郵件的地址。";
$MESS["MAIN_MAIL_CONFIRM_MESSAGE_FAQ_Q1"] = "為什麼我需要驗證我的電子郵件地址？";
$MESS["MAIN_MAIL_CONFIRM_MESSAGE_HINT"] = "在您的BITRIX24上輸入此確認代碼以確認您的電子郵件地址。";
