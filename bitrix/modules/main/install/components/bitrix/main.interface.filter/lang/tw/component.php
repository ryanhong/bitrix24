<?php
$MESS["interface_filter_earlier"] = "前";
$MESS["interface_filter_exact"] = "確切地";
$MESS["interface_filter_interval"] = "日期範圍";
$MESS["interface_filter_last"] = "最近的";
$MESS["interface_filter_later"] = "後";
$MESS["interface_filter_month"] = "這個月";
$MESS["interface_filter_month_ago"] = "上個月";
$MESS["interface_filter_no_no_no_1"] = "（不）";
$MESS["interface_filter_today"] = "今天";
$MESS["interface_filter_week"] = "本星期";
$MESS["interface_filter_week_ago"] = "上星期";
$MESS["interface_filter_yesterday"] = "昨天";
