<?php
$MESS["interface_filter_days"] = "天";
$MESS["interface_grid_additional"] = "更多過濾器";
$MESS["interface_grid_find"] = "搜尋";
$MESS["interface_grid_find_title"] = "查找匹配搜索標準的記錄";
$MESS["interface_grid_flt_cancel"] = "取消";
$MESS["interface_grid_flt_cancel_title"] = "顯示所有記錄";
$MESS["interface_grid_from_head"] = "擴張";
$MESS["interface_grid_hide"] = "隱藏過濾器";
$MESS["interface_grid_hide_all"] = "隱藏所有過濾器";
$MESS["interface_grid_no_no_no"] = "（不）";
$MESS["interface_grid_not_used"] = "過濾器是不活動的，顯示了所有記錄。";
$MESS["interface_grid_search"] = "搜尋";
$MESS["interface_grid_show_all"] = "顯示所有過濾器";
$MESS["interface_grid_to_head"] = "坍塌";
$MESS["interface_grid_used"] = "過濾器處於活動狀態，僅顯示匹配記錄。";
