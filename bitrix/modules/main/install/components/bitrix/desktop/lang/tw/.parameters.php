<?php
$MESS["CMDESKTOP_PARAMS_CAN_EDIT"] = "允許所有授權用戶管理自己的個人儀表板";
$MESS["CMDESKTOP_PARAMS_COLUMNS"] = "列數";
$MESS["CMDESKTOP_PARAMS_COLUMN_WITH"] = "柱寬度（PX或％）";
$MESS["CMDESKTOP_PARAMS_DATE_FORMAT"] = "日期格式";
$MESS["CMDESKTOP_PARAMS_DATE_FORMAT_NO_YEAR"] = "日期格式（無年份）";
$MESS["CMDESKTOP_PARAMS_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["CMDESKTOP_PARAMS_GADGETS"] = "可用的小工具";
$MESS["CMDESKTOP_PARAMS_GADGETS_ALL"] = "（全部可用）";
$MESS["CMDESKTOP_PARAMS_GADGET_PAR"] = "小工具的默認用戶設置";
$MESS["CMDESKTOP_PARAMS_GADGET_SET"] = "小工具設置";
$MESS["CMDESKTOP_PARAMS_ID"] = "個人儀表板ID";
$MESS["CMDESKTOP_PARAMS_NAME_TEMPLATE"] = "名稱格式";
$MESS["CMDESKTOP_PARAMS_NAME_TEMPLATE_DEFAULT"] = "＃名稱＃＃last_name＃";
$MESS["CMDESKTOP_PARAMS_PATH_TO_CONPANY_DEPARTMENT"] = "部門頁面路徑模板";
$MESS["CMDESKTOP_PARAMS_PATH_TO_VIDEO_CALL"] = "視頻通話頁面";
$MESS["CMDESKTOP_PARAMS_PM_URL"] = "個人消息頁面";
$MESS["CMDESKTOP_PARAMS_SHOW_LOGIN"] = "顯示登錄名，如果沒有必需的用戶名字段";
$MESS["CMDESKTOP_PARAMS_SHOW_YEAR"] = "出生年份";
$MESS["CMDESKTOP_PARAMS_SHOW_YEAR_VALUE_M"] = "只有男性";
$MESS["CMDESKTOP_PARAMS_SHOW_YEAR_VALUE_N"] = "沒有人";
$MESS["CMDESKTOP_PARAMS_SHOW_YEAR_VALUE_Y"] = "全部";
