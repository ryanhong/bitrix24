<?php
$MESS["SCALE_INSTALL_TITLE"] = "\“可伸縮性\”模塊安裝";
$MESS["SCALE_MODULE_DESCRIPTION"] = "提供可伸縮性功能和可擴展性管理功能。";
$MESS["SCALE_MODULE_NAME"] = "可伸縮性";
$MESS["SCALE_UNINSTALL_TITLE"] = "\“可伸縮性\”模塊卸載";
