<?php
$MESS["MAIL_GROUP_DESC"] = "用戶，通過電子郵件中的直接鏈接進行系統認證";
$MESS["MAIL_GROUP_NAME"] = "電子郵件用戶";
$MESS["MAIL_INSTALL_BACK"] = "返回模塊管理部分";
$MESS["MAIL_INSTALL_TITLE"] = "郵件模塊卸載";
$MESS["MAIL_MODULE_DESC"] = "郵件模塊的目的是接收消息，過濾和執行指定的操作。";
$MESS["MAIL_MODULE_NAME"] = "郵件";
$MESS["MAIL_UNINSTALL_COMPLETE"] = "卸載完成。";
$MESS["MAIL_UNINSTALL_DEL"] = "解除安裝";
$MESS["MAIL_UNINSTALL_ERROR"] = "卸載錯誤：";
$MESS["MAIL_UNINSTALL_SAVEDATA"] = "要保存存儲在數據庫表中的數據，請檢查“保存表”。複選框。";
$MESS["MAIL_UNINSTALL_SAVETABLE"] = "保存表";
$MESS["MAIL_UNINSTALL_WARNING"] = "警告！該模塊將從系統中卸載。";
