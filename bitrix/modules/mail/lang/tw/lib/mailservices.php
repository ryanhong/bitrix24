<?php
$MESS["mail_mailservice_bitrix24_icon"] = "bitrix24-icon-en.png";
$MESS["mail_mailservice_entity_active_field"] = "積極的";
$MESS["mail_mailservice_entity_encryption_field"] = "安全連接";
$MESS["mail_mailservice_entity_flags_field"] = "標誌";
$MESS["mail_mailservice_entity_icon_field"] = "標識";
$MESS["mail_mailservice_entity_link_field"] = "Web接口URL";
$MESS["mail_mailservice_entity_name_field"] = "姓名";
$MESS["mail_mailservice_entity_port_field"] = "港口";
$MESS["mail_mailservice_entity_server_field"] = "服務器地址";
$MESS["mail_mailservice_entity_site_field"] = "網站";
$MESS["mail_mailservice_entity_sort_field"] = "種類";
$MESS["mail_mailservice_entity_token_field"] = "令牌";
$MESS["mail_mailservice_entity_type_field"] = "類型";
$MESS["mail_mailservice_not_found"] = "沒有找到電子郵件服務。";
