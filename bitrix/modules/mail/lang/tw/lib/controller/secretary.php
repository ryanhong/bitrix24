<?php
$MESS["MAIL_SECRETARY_ACCESS_DENIED"] = "此操作僅適用於郵箱所有者";
$MESS["MAIL_SECRETARY_ACCESS_DENIED_CALENDAR"] = "此操作僅適用於日曆事件創建者";
$MESS["MAIL_SECRETARY_CALENDAR_EVENT_DESC"] = "
發送者：[url =＃link_from＃]＃來自＃[/url]

主題：＃主題＃

日期：＃日期＃

[url =＃鏈接＃]查看電子郵件[/url]
";
$MESS["MAIL_SECRETARY_CREATE_CHAT_LOCK_ERROR"] = "錯誤創建聊天。請再試一次。";
$MESS["MAIL_SECRETARY_POST_MESSAGE_CALENDAR_EVENT"] = "基於[url =＃鏈接＃]電子郵件[/url]創建";
