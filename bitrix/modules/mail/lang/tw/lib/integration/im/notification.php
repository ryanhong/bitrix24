<?php
$MESS["MAIL_NOTIFY_FULL_MAILBOX_TARIFF_RESTRICTIONS_HAVE_BEEN_IMPOSED"] = "您已將支持郵箱少的計劃降級。<br/> <br/>訪問\ \“ <a target = \"_blank \" href= \“#view_url# \ \"> #email＃</email A> \”已暫時關閉。<br/> <br/>不會通過傳入的電子郵件創建潛在客戶或交易。客戶發送到此電子郵件地址的響應不會保存到CRM。<br/> <br/>激活此郵箱，禁用您不使用的郵箱，或升級到主要計劃之一。";
$MESS["MAIL_NOTIFY_IMPOSE_TARIFF_RESTRICTIONS_ON_THE_MAILBOX"] = "超出郵箱的最大數量";
$MESS["MAIL_NOTIFY_MAILBOX_TARIFF_RESTRICTIONS_HAVE_BEEN_IMPOSED"] = "訪問\ \“ <a target= \"_blank \" href= \"#view_url# \ \">＃email＃</a> \”已暫時關閉，因為郵箱比您當前的計劃支持更多。<br/ <br/ > <br/> <a target = \"_blank \" href= \ \"#view_url# \ \">了解更多</a>";
$MESS["MAIL_NOTIFY_NEW_MESSAGE"] = "新消息";
$MESS["MAIL_NOTIFY_NEW_MESSAGE_MULTI_1"] = "新電子郵件：＃count＃<br/> <br/> <a target= \"_blank \" href= \ \"#view_url# \ \"> view </a>";
$MESS["MAIL_NOTIFY_NEW_MESSAGE_TITLE"] = "電子郵件";
$MESS["MAIL_NOTIFY_NEW_SINGLE_MESSAGE_IN_MAIL_CLIENT_1"] = "新電子郵件：<a target= \"_blank \" href= \"#view_url# \ \"> \ \“＃主題＃\” </a>";
$MESS["MAIL_NOTIFY_NEW_SINGLE_MESSAGE_IN_MAIL_CLIENT_EMPTY_SUBJECT"] = "新電子郵件";
