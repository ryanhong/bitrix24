<?php
$MESS["MAIL_MAILBOX_CONNECTOR_CLIENT_EMAIL_EXISTS_ERROR"] = "該郵箱已經連接。";
$MESS["MAIL_MAILBOX_CONNECTOR_CLIENT_ERR_OAUTH"] = "外部傳入郵件服務器的身份驗證失敗。確保使用此電子郵件的用戶在該服務器上存在。如果您的電子郵件帳戶在管理員的監督下，請與他們聯繫以獲取進一步的幫助。";
$MESS["MAIL_MAILBOX_CONNECTOR_CLIENT_ERR_OAUTH_SMTP"] = "外部發出郵件服務器的身份驗證失敗。確保電子郵件服務器允許通過SMTP發送電子郵件，或嘗試取消選項\“使用外部SMTP \”的選項\。如果您的電子郵件帳戶在管理員的監督下，請與他們聯繫以獲取進一步的幫助。";
$MESS["MAIL_MAILBOX_CONNECTOR_CLIENT_FORM_ERROR"] = "連接錯誤。刷新頁面或重新啟動應用程序。";
$MESS["MAIL_MAILBOX_CONNECTOR_CLIENT_IMAP_AUTH_ERR_EXT"] = "授權錯誤。請檢查您的登錄和/或密碼是否正確。";
$MESS["MAIL_MAILBOX_CONNECTOR_CLIENT_LIMIT_ERROR"] = "最大郵箱數量超過了。升級到主要計劃之一。";
$MESS["MAIL_MAILBOX_CONNECTOR_CLIENT_OAUTH_ERROR"] = "授權錯誤。";
$MESS["MAIL_MAILBOX_CONNECTOR_CLIENT_THERE_ARE_NO_MAIL_SERVICES"] = "電子郵件服務提供商不在服務列表中。請聯繫您的BitRix24管理員。";
$MESS["MAIL_MAILBOX_CONNECTOR_SMTP_PASS_BAD_SYMBOLS"] = "SMTP密碼包含無效字符。請聯繫您的BitRix24管理員。";
