<?php
$MESS["MAIL_IMAP_ERR_APPEND"] = "錯誤保存消息";
$MESS["MAIL_IMAP_ERR_AUTH"] = "請在郵箱設置中再次授權。";
$MESS["MAIL_IMAP_ERR_AUTH_MECH"] = "該服務器不支持所需的身份驗證方法。";
$MESS["MAIL_IMAP_ERR_AUTH_OAUTH"] = "無法獲得許可";
$MESS["MAIL_IMAP_ERR_BAD_SERVER"] = "服務器返回了未知的響應。";
$MESS["MAIL_IMAP_ERR_CAPABILITY"] = "錯誤獲得功能";
$MESS["MAIL_IMAP_ERR_COMMAND_REJECTED"] = "命令拒絕";
$MESS["MAIL_IMAP_ERR_COMMUNICATE"] = "通信故障。";
$MESS["MAIL_IMAP_ERR_CONNECT"] = "服務器連接錯誤";
$MESS["MAIL_IMAP_ERR_DEFAULT"] = "未知錯誤";
$MESS["MAIL_IMAP_ERR_EMPTY_RESPONSE"] = "服務器返回沒​​有響應";
$MESS["MAIL_IMAP_ERR_FETCH"] = "錯誤獲取消息";
$MESS["MAIL_IMAP_ERR_LIST"] = "錯誤獲取文件夾列表";
$MESS["MAIL_IMAP_ERR_REJECTED"] = "連接被拒絕";
$MESS["MAIL_IMAP_ERR_SEARCH"] = "郵件搜索失敗";
$MESS["MAIL_IMAP_ERR_STARTTLS"] = "錯誤初始化加密API";
$MESS["MAIL_IMAP_ERR_STORE"] = "錯誤更新消息";
