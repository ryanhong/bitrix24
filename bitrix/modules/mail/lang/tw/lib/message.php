<?php
$MESS["MAIL_QUOTE_MESSAGE_HEADER"] = "
</br> </br>
來自：＃from_list＃</br>
到：＃to_list＃</br>
cc：＃cc_list＃</br>
日期：＃日期＃</br>
主題：＃主題＃</br>
[blockquote] #body＃[/blockquote]
</br>";
$MESS["MAIL_QUOTE_MESSAGE_HEADER_CONTACT"] = "＃名稱＃＆lt;＃電子郵件＃＆gt;";
$MESS["MAIL_QUOTE_MESSAGE_HEADER_WITHOUT_CC"] = "
</br> </br>
來自：＃from_list＃</br>
到：＃to_list＃</br>
日期：＃日期＃</br>
主題：＃主題＃</br>
[blockquote] #body＃[/blockquote]
</br>";
