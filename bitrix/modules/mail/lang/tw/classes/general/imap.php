<?php
$MESS["MAIL_IMAP_ERR_AUTH"] = "授權錯誤";
$MESS["MAIL_IMAP_ERR_AUTH_MECH"] = "該服務器不支持所需的身份驗證方法。";
$MESS["MAIL_IMAP_ERR_BAD_SERVER"] = "服務器返回了未知的響應。";
$MESS["MAIL_IMAP_ERR_COMMUNICATE"] = "通信故障。";
$MESS["MAIL_IMAP_ERR_CONNECT"] = "服務器連接錯誤";
