<?php
$MESS["MAIL_CHECK_CHECK"] = "接收郵件：";
$MESS["MAIL_CHECK_CHECK_ALL"] = "（所有郵箱）";
$MESS["MAIL_CHECK_CHECK_OK"] = "好的";
$MESS["MAIL_CHECK_CNT"] = "收到";
$MESS["MAIL_CHECK_CNT_NEW"] = "新消息。";
$MESS["MAIL_CHECK_ERR"] = "接收郵件時錯誤。";
$MESS["MAIL_CHECK_LOG"] = "查看日誌";
$MESS["MAIL_CHECK_MBOX_PARAMS"] = "郵箱設置";
$MESS["MAIL_CHECK_TEXT"] = "郵箱檢查";
$MESS["MAIL_CHECK_TITLE"] = "檢查新郵件";
$MESS["MAIL_CHECK_VIEW"] = "查看消息";
