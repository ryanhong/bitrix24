<?php
$MESS["SENDER_CONSENT_ERROR_APPLY"] = "無法提交同意以接收新聞通訊。您可能已經嘗試多次打開此鏈接。";
$MESS["SENDER_CONSENT_ERROR_REJECT"] = "不能退訂。您可能已經嘗試多次打開此鏈接。";
$MESS["SENDER_CONSENT_WRONG_LINK"] = "錯誤的鏈接";
