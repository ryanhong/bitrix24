<?php
$MESS["SENDER_CALL_NUMBER_HINT"] = "使用電話設置頁面添加或配置一個數字。";
$MESS["SENDER_CALL_NUMBER_MARKETPLACE_LINK"] = "選擇其他服務";
$MESS["SENDER_CALL_NUMBER_PHONE"] = "從數字";
$MESS["SENDER_CALL_NUMBER_PROVIDER"] = "致電通過";
$MESS["SENDER_CALL_NUMBER_SELECT"] = "從電話號碼打電話";
$MESS["SENDER_CALL_NUMBER_SETUP"] = "配置號碼";
