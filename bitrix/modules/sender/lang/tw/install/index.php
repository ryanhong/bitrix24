<?php
$MESS["SENDER_MODULE_DESC"] = "該模塊管理並發送新聞通訊。";
$MESS["SENDER_MODULE_DESC1"] = "處理批量電子郵件和短信發送以及社交廣告。";
$MESS["SENDER_MODULE_INST_TITLE"] = "電子郵件營銷模塊安裝";
$MESS["SENDER_MODULE_INST_TITLE1"] = "\“ Marketing24 \”模塊安裝";
$MESS["SENDER_MODULE_NAME"] = "電子郵件營銷";
$MESS["SENDER_MODULE_NAME1"] = "Marketing24";
$MESS["SENDER_MODULE_UNINST_TITLE"] = "電子郵件營銷模塊刪除";
$MESS["SENDER_MODULE_UNINST_TITLE1"] = "\“ Marketing24 \”模塊卸載";
