<?php
$MESS["SUBSCRIBE_CONFIRM_DESC"] = "＃電子郵件＃-訂閱URL
＃日期＃-添加或更新地址的日期
＃juccess_url＃ - 確認URL
＃mailing_list＃ - 訂閱
";
$MESS["SUBSCRIBE_CONFIRM_MESSAGE"] = "此消息是從＃site_name＃發送的。
-------------------------------------------------- --------

你好，

您正在收到此消息，因為您的電子郵件已訂閱＃server_name＃新聞通訊。

訂閱詳細信息：

地址（電子郵件）......................＃電子郵件＃
添加或更新的日期....＃日期＃
新聞通訊：
＃郵件列表＃


單擊此鏈接以確認您的訂閱：
http：//＃server_name ## juccess_url＃


注意力！在您確認訂閱之前，不會向您發送新聞通訊。
如果您尚未訂閱此郵件列表並錯誤地收到此消息，請忽略它。

此消息是由機器人發送的。
";
$MESS["SUBSCRIBE_CONFIRM_NAME"] = "確認訂閱";
$MESS["SUBSCRIBE_CONFIRM_SUBJECT"] = "＃site_name＃：確認訂閱";
