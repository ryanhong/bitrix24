<?php
$MESS["MAIN_AJAX_ACTION_APPLY"] = "申請";
$MESS["MAIN_AJAX_ACTION_CANCEL"] = "取消";
$MESS["MAIN_AJAX_ACTION_CLOSE"] = "關閉";
$MESS["MAIN_AJAX_ACTION_CONFIRM"] = "你想繼續嗎？";
$MESS["MAIN_AJAX_ACTION_CONFIRM_DELETE"] = "您要刪除\“％name％\”？";
$MESS["MAIN_AJAX_ACTION_DELETE"] = "刪除";
$MESS["MAIN_AJAX_ACTION_ERROR"] = "這是一個錯誤。請再試一次。";
$MESS["MAIN_AJAX_ACTION_SUCCESS"] = "成功！";
