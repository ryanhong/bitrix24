<?php
$MESS["sender_convert_form_button_start"] = "轉變";
$MESS["sender_convert_form_button_stop"] = "停止";
$MESS["sender_convert_form_time_desc"] = "秒";
$MESS["sender_convert_form_time_name"] = "最大轉換步驟時間：";
$MESS["sender_convert_status_done"] = "完全的。";
$MESS["sender_convert_status_run"] = "進行中...";
$MESS["sender_convert_status_title"] = "數據轉換";
$MESS["sender_convert_tab_convert_name"] = "轉換";
$MESS["sender_convert_tab_convert_title"] = "數據轉換";
$MESS["sender_convert_title"] = "電子郵件營銷模塊數據轉換";
