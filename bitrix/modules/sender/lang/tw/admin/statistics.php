<?php
$MESS["SENDER_STATS_CHART_CLICK"] = "點擊";
$MESS["SENDER_STATS_CHART_EFFICIENCY"] = "效率動態";
$MESS["SENDER_STATS_CHART_READ"] = "視圖";
$MESS["SENDER_STATS_CHART_UNSUB"] = "未訂閱";
$MESS["SENDER_STATS_COUNTER_CLICK"] = "點擊";
$MESS["SENDER_STATS_COUNTER_POSTINGS"] = "消息計數";
$MESS["SENDER_STATS_COUNTER_READ"] = "視圖";
$MESS["SENDER_STATS_COUNTER_SEND_ALL"] = "發送的總郵件";
$MESS["SENDER_STATS_COUNTER_SUBS"] = "訂閱";
$MESS["SENDER_STATS_COUNTER_UNSUB"] = "未訂閱";
$MESS["SENDER_STATS_CREATE_NEW"] = "創建新的新聞通訊消息";
$MESS["SENDER_STATS_CREATE_NEW_LETTER"] = "創建新的新聞通訊";
$MESS["SENDER_STATS_EFFICIENCY_LEVEL_1"] = "壞的";
$MESS["SENDER_STATS_EFFICIENCY_LEVEL_2"] = "可通過";
$MESS["SENDER_STATS_EFFICIENCY_LEVEL_3"] = "普通的";
$MESS["SENDER_STATS_EFFICIENCY_LEVEL_4"] = "好的";
$MESS["SENDER_STATS_EFFICIENCY_LEVEL_5"] = "出色的";
$MESS["SENDER_STATS_EFFICIENCY_TITLE"] = "新聞通訊效率";
$MESS["SENDER_STATS_FILTER_FROM_AUTHOR"] = "經過";
$MESS["SENDER_STATS_FILTER_PERIOD_FOR"] = "為了";
$MESS["SENDER_STATS_IN"] = "在";
$MESS["SENDER_STATS_NO_DATA"] = "沒有數據。";
$MESS["SENDER_STATS_RECENT_POSTINGS"] = "最近的消息";
$MESS["SENDER_STATS_TITLE"] = "開始";
