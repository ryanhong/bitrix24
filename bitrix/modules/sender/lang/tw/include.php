<?php
$MESS["POST_HTML"] = "html";
$MESS["POST_STATUS_DRAFT"] = "草稿";
$MESS["POST_STATUS_ERROR"] = "發送錯誤";
$MESS["POST_STATUS_PART"] = "進行中";
$MESS["POST_STATUS_SENT"] = "發送";
$MESS["POST_STATUS_WAIT"] = "中止";
$MESS["POST_TEXT"] = "文字";
$MESS["POST_WRONG_AUTO_FROM"] = "第一個自動日期不正確。";
$MESS["POST_WRONG_AUTO_TILL"] = "最後一個自動日期不正確。";
$MESS["POST_WRONG_DATE_SENT_FROM"] = "第一個發送日期不正確。";
$MESS["POST_WRONG_DATE_SENT_TILL"] = "最後一個發送日期不正確。";
$MESS["POST_WRONG_TIMESTAMP_FROM"] = "第一個修改的日期是不正確的。";
$MESS["POST_WRONG_TIMESTAMP_TILL"] = "最後修改的日期不正確。";
