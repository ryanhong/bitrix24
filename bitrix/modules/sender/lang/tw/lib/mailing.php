<?php
$MESS["SENDER_ENTITY_MAILING_CHAIN_ITEM_NUMBER"] = "訊息 #";
$MESS["SENDER_ENTITY_MAILING_FIELD_TITLE_DESCRIPTION"] = "描述";
$MESS["SENDER_ENTITY_MAILING_FIELD_TITLE_EMAIL_FROM"] = "發件人";
$MESS["SENDER_ENTITY_MAILING_FIELD_TITLE_NAME"] = "姓名";
$MESS["SENDER_ENTITY_MAILING_FIELD_TITLE_SORT"] = "種類";
$MESS["SENDER_ENTITY_MAILING_VALID_EMAIL_FROM"] = "\“發送者\”字段不包含電子郵件地址。";
