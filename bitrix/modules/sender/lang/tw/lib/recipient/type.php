<?php
$MESS["SENDER_TYPE_CAPTION_CRM_COMPANY_ID"] = "公司";
$MESS["SENDER_TYPE_CAPTION_CRM_CONTACT_ID"] = "接觸";
$MESS["SENDER_TYPE_CAPTION_CRM_DEAL_PRODUCT_COMPANY_ID"] = "公司（交易中的產品）";
$MESS["SENDER_TYPE_CAPTION_CRM_DEAL_PRODUCT_CONTACT_ID"] = "聯繫（交易中的產品）";
$MESS["SENDER_TYPE_CAPTION_CRM_ORDER_PRODUCT_COMPANY_ID"] = "公司（訂單的產品）";
$MESS["SENDER_TYPE_CAPTION_CRM_ORDER_PRODUCT_CONTACT_ID"] = "聯繫（訂單的產品）";
$MESS["SENDER_TYPE_CAPTION_EMAIL"] = "電子郵件";
$MESS["SENDER_TYPE_CAPTION_IM"] = "信使";
$MESS["SENDER_TYPE_CAPTION_PHONE"] = "電話";
