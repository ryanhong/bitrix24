<?php
$MESS["PRESET_MAILBLOCK_NAME"] = "姓名";
$MESS["PRESET_MAILBLOCK_facebook"] = "Facebook";
$MESS["PRESET_MAILBLOCK_image"] = "圖像";
$MESS["PRESET_MAILBLOCK_image2"] = "兩個圖像";
$MESS["PRESET_MAILBLOCK_image3"] = "三張圖像";
$MESS["PRESET_MAILBLOCK_image_text"] = "圖像和文字";
$MESS["PRESET_MAILBLOCK_line"] = "線";
$MESS["PRESET_MAILBLOCK_text"] = "文字";
$MESS["PRESET_MAILBLOCK_text2"] = "兩個文字";
$MESS["PRESET_MAILBLOCK_text3"] = "三個文字";
$MESS["PRESET_MAILBLOCK_text_image"] = "文字和圖像";
$MESS["PRESET_MAILBLOCK_unsub"] = "退訂";
$MESS["PRESET_MAILBLOCK_unsub_TEXT_UNSUB_LINK"] = "關聯";
$MESS["PRESET_MAILBLOCK_unsub_TEXT_UNSUB_TEXT"] = "要取消訂閱，請關注此";
$MESS["TYPE_PRESET_MAILBLOCK_BASE"] = "基本塊";
$MESS["TYPE_PRESET_MAILBLOCK_PERSONALISE"] = "個性化";
$MESS["TYPE_PRESET_MAILBLOCK_SOCIAL"] = "社交網絡";
