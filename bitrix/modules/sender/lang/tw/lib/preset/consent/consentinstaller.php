<?php
$MESS["sender_approve_confirmation_text"] = "<p class = \“ u-paragraph \”>謝謝你花點時間！</p> \ r \ n <p class = \“ u-paragraph \”>如果您想成為第一個知道的人新的折扣和優惠- 請確認您同意接收我們的新聞通訊。</p> \ r \ n <p class = \“ u -paragraph \”>您可以隨時取消訂閱。未經您的同意，我們將永遠不會發送電子郵件。</p> \ r \ n";
$MESS["sender_approve_confirmation_title"] = "同意接收新聞通訊";
$MESS["sender_privacy_label"] = "電子郵件跟踪<a href= \“#consent_url# \ \">隱私政策</a>。";
$MESS["sender_privacy_title"] = "同意跟踪電子郵件讀取";
