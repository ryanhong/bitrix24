<?php
$MESS["SENDER_PRESET_TEMPLATE_RC_BIRTHDAY_DESC"] = "祝賀客戶，增長忠誠度，增加銷售額";
$MESS["SENDER_PRESET_TEMPLATE_RC_BIRTHDAY_NAME"] = "生日是特殊的一天";
$MESS["SENDER_PRESET_TEMPLATE_RC_BIRTHDAY_TEXT"] = "致電客戶在生日那天給他們最好的祝愿，並做禮物 -  30％的折扣優惠券。

例子：
 - 你好＃名稱＃！我們在\“％company％\”在這裡表達了我們的最良好祝愿，我們為您提供禮物，折扣優惠券。我可以向您發送帶有優惠券代碼的文字嗎？祝你生日快樂，祝你有美好的一天！";
$MESS["SENDER_PRESET_TEMPLATE_RC_BIRTHDAY_TITLE"] = "祝賀＃名字＃在他們的生日";
$MESS["SENDER_PRESET_TEMPLATE_RC_EMPTY"] = "風俗";
$MESS["SENDER_PRESET_TEMPLATE_RC_EMPTY_DESC"] = "發揮創造力並增加銷售";
$MESS["SENDER_PRESET_TEMPLATE_RC_HINT_EVERY_DAY"] = "該活動將每天舉辦。<br>
它將選擇五天內生日的客戶（可以在細分市場中指定）。<br>
潛在客戶和交易將在指定的時間創建。";
$MESS["SENDER_PRESET_TEMPLATE_RC_HINT_NPS"] = "該活動將每天舉辦。<br>
它將選擇30天前完成交易的客戶（可以在細分市場中指定）。<br>
潛在客戶和交易將在指定的時間創建。";
$MESS["SENDER_PRESET_TEMPLATE_RC_HINT_ONE_DAY"] = "該廣告系列將每年以％run_date％進行。<br>
它將選擇從％date_from％到％date_to％購買的客戶（可以在細分市場中指定）。<br>
潛在客戶和交易將在指定的時間創建。";
$MESS["SENDER_PRESET_TEMPLATE_RC_HOLIDAY_DESC"] = "％holiday_date％是增加銷售的絕佳機會";
$MESS["SENDER_PRESET_TEMPLATE_RC_HOLIDAY_NAME"] = "％holiday_name％銷售";
$MESS["SENDER_PRESET_TEMPLATE_RC_HOLIDAY_TEXT"] = "致電客戶，並給他們15％的優惠券，以獲取％holday_name％。

例子：
 - 你好＃名稱＃！這是％公司％。我們想為您提供15％的折扣優惠券，即即將到來的％Holiday_name％。我可以向您發送帶有優惠券代碼的文本嗎？ happy％holiday_name％，祝你有美好的一天！";
$MESS["SENDER_PRESET_TEMPLATE_RC_HOLIDAY_TITLE"] = "呼叫＃名稱＃，％holday_name％銷售";
$MESS["SENDER_PRESET_TEMPLATE_RC_NPS_DESC"] = "永恆的規則：服務越好，他們留下的時間越長。";
$MESS["SENDER_PRESET_TEMPLATE_RC_NPS_NAME"] = "滿意的客戶可以保證更多的銷售";
$MESS["SENDER_PRESET_TEMPLATE_RC_NPS_TEXT"] = "您的任務是獲得客戶對他們最近交易的評論。請在打電話之前查看交易。

例子：
 - 你好＃名稱＃！這是百分比呼叫的％。您如何看待您最近的購買？我們的銷售人員是否提供了您期望的服務？您對您的購買經驗有任何評論嗎？

經過積極的審查：
 - 謝謝＃名稱＃！等不及要再次見到你了！

經過負面評價：
 - 非常感謝您的評論＃名稱＃。請確保我們將盡一切努力糾正這些問題。再次感謝，祝您有美好的一天！";
$MESS["SENDER_PRESET_TEMPLATE_RC_NPS_TITLE"] = "＃名稱＃的質量保證";
