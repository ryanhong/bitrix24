<?php
$MESS["SENDER_POSTING_MANAGER_ERR_CHARSET"] = "網站“＃site_id＃”的首選項中未指定字符集。此字符集用作電子郵件編碼。";
$MESS["SENDER_POSTING_MANAGER_ERR_LOCK"] = "該新聞通訊由另一個用戶鎖定。現在可能是其他人發送的。";
$MESS["SENDER_POSTING_MANAGER_ERR_SITE"] = "找不到具有ID“＃site_id＃”的網站。";
