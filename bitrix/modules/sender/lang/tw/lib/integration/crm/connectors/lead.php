<?php
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_FIELD_ASSIGNED_BY_ID"] = "負責人";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_FIELD_BIRTHDATE"] = "出生日期";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_FIELD_COMMUNICATION_TYPE"] = "聯繫人通過";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_FIELD_DATE_CREATE"] = "創建於";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_FIELD_HONORIFIC"] = "致敬";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_FIELD_LEAD"] = "帶領";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_FIELD_POST"] = "位置";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_FIELD_PRODUCT_ID"] = "產品";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_FIELD_RC_LEAD"] = "重複引線";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_FIELD_SENDER_SELECT_ALL"] = "全選";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_FIELD_SOURCE_ID"] = "來源";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_FIELD_STATUS_CONVERTED"] = "轉換";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_FIELD_STATUS_ID"] = "地位";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_FIELD_STATUS_SEMANTIC_ID"] = "狀態組";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_NAME"] = "鉛";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_PRESET_ALL"] = "全部";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_PRESET_BIRTH"] = "五天的生日";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_PRESET_CONV"] = "轉換";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_PRESET_INW"] = "進行中";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_PRESET_SEGMENT_ALL"] = "所有線索";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_PRESET_SEGMENT_BIRTH"] = "鉛。五天的生日";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_PRESET_SEGMENT_CONV"] = "轉換的線索";
$MESS["SENDER_INTEGRATION_CRM_CONNECTOR_LEAD_PRESET_SEGMENT_INW"] = "領導正在進行";
