<?php
$MESS["SENDER_INTEGRATION_BITRIX24_RATING_BLOCKED1"] = "我們正在收到有關從您的Bitrix24發送的郵件列表的投訴。您的郵件列表已被暫停。請聯繫HelpDesk解決問題。";
$MESS["SENDER_INTEGRATION_BITRIX24_RATING_DOWNGRADED1"] = "我們減少了您的每日電子郵件限制。請改進您的新聞通訊，以發送更多電子郵件。";
$MESS["SENDER_INTEGRATION_BITRIX24_RATING_UPGRADED1"] = "恭喜！您的郵件列表受到客戶的好評。我們增加了您的每日電子郵件限制。保持良好的工作！";
