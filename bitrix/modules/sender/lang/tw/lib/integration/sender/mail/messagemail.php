<?php
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_APPROVE_CONFIRMATION"] = "需要新聞通訊同意";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_APPROVE_CONFIRMATION_HINT"] = "向您的客戶展示您感謝他們的關注並尊重他們的隱私。僅將新聞通訊發送給給予同意接受的客戶。客戶將收到一封電子郵件要求他們同意接受您的新聞通訊。您可以使用罐頭同意書之一，也可以使用自己的同意。您通過CRM營銷發送的新聞通訊將僅發送給已同意的客戶。";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_ATTACHMENT"] = "附件";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_EMAIL_FROM"] = "發件人";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_LINK_PARAMS"] = "鏈接參數";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_MESSAGE"] = "郵件正文";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_OTHER"] = "其他";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_OTHER_HINT"] = "其他";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_PRIORITY"] = "優先事項";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_PRIORITY_HIGHEST"] = "高的";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_PRIORITY_HINT"] = "在客戶端的郵箱中突出顯示了高優先級電子郵件。
建議您僅將高優先級標記為真正重要的消息。
不要濫用這種狀態，因為沒有太多重要的信息。";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_PRIORITY_LOWEST"] = "低的";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_PRIORITY_NORMAL"] = "普通的";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_SENDING_START"] = "如果將此選項與空模板一起使用，請確保將您在下面的塊中看到的文本添加到消息正文中。將\“頁腳\”塊添加到現有模板中 - 它包含所有必需的參數。";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_SUBJECT"] = "主題";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_TRACK_MAIL"] = "跟踪電子郵件讀取";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONFIG_TRACK_MAIL_HINT"] = "知道哪個客戶實際上打開了您的新聞通訊消息，以啟動進一步的步驟來達成交易。此功能使用\“隱藏像素\”技術。\ r \ nineptant！您的當地法律需要接受接收者同意，然後才能開始發送使用此技術的電子郵件。客戶將收到一封電子郵件，要求他們同意接受包含隱藏像素的新聞通訊。跟踪僅針對已同意的客戶進行激活。您可以使用罐頭同意書之一，也可以使用自己的同意。";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_CONSENT_WARNING"] = "如果將此選項與空模板一起使用，請確保將您在下面的塊中看到的文本添加到消息正文中。將\“頁腳\”塊添加到現有模板中 - 它包含所有必需的參數。";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_COPY"] = "複製";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_ERR_NO_APPROVE_CONFIRMATION_CONSENT"] = "未選擇與“要求新聞通訊同意\”一起使用的同意";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_ERR_NO_TRACK_MAIL_CONSENT"] = "未選擇與選項\“ Track Email reads” \”選擇同意。";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_ERR_NO_TRACK_MAIL_CONSENT_IN_BODY"] = "無法創建可跟踪的通訊。從\“更多電子郵件選項\”區域中添加到消息主體一個塊。";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_ERR_NO_UNSUB_LINK"] = "您無法創建不包含未取消訂閱鏈接的新聞通訊。將頁腳塊添加到消息主體中 - 它包含所需的項目。";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_NAME"] = "電子郵件活動";
$MESS["SENDER_INTEGRATION_MAIL_MESSAGE_NO"] = "沒有任何";
