<?php
$MESS["sender_connector_recipient_all"] = "任何";
$MESS["sender_connector_recipient_click"] = "鏈接單擊：";
$MESS["sender_connector_recipient_mailing"] = "通訊：";
$MESS["sender_connector_recipient_n"] = "不";
$MESS["sender_connector_recipient_name"] = "電子郵件營銷 - 新聞通訊的收件人";
$MESS["sender_connector_recipient_name1"] = "新聞通訊收件人";
$MESS["sender_connector_recipient_read"] = "查看：";
$MESS["sender_connector_recipient_sent"] = "發送：";
$MESS["sender_connector_recipient_unsub"] = "未訂閱：";
$MESS["sender_connector_recipient_y"] = "是的";
