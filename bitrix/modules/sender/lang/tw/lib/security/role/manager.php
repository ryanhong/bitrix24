<?php
$MESS["SENDER_SECURITY_ROLE_MANAGER_INSTALLER_ADMIN"] = "行政人員";
$MESS["SENDER_SECURITY_ROLE_MANAGER_INSTALLER_MANAGER"] = "主管";
$MESS["SENDER_SECURITY_ROLE_MANAGER_TRIAL_TEXT"] = "CRM營銷訪問權限可在選定的商業計劃中獲得。
<br>
您可以通過允許訪問銷售提升，新聞通訊或廣告來輕鬆在員工之間分發角色。
<br> <br>
了解有關<a href= \“/settings/license_all.php \" target = \"_blank \">定價頁面上的計劃和功能的更多信息。";
$MESS["SENDER_SECURITY_ROLE_MANAGER_TRIAL_TEXT_NEW"] = "您當前的計劃對CRM營銷功能中可能的訪問權限有一定的限制。
<br>
您可以通過允許訪問銷售提升，新聞通訊或廣告來輕鬆在員工之間分發角色。
<br> <br>
升級到管理訪問權限的選定主要計劃之一。<br>
了解有關<a href= \"https://www.bitrix24.com/prices/prices/xprices/prices/xpricing </a>頁面上的計劃和功能的更多信息。";
