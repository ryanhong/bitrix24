<?php
$MESS["SENDER_SECURITY_AGREEMENT_ERROR"] = "您必須接受條款。";
$MESS["SENDER_SECURITY_AGREEMENT_HTML_RICH"] = "<div class = \“ tracker-agreement-popup-content \”>
 Bitrix24 CRM營銷 - 是一套工具，可以幫助您更有效地提高銷售和市場。您可以將客戶數據庫細分，進行電子郵件和SMS營銷，發送語音消息，即時消息，在各種廣告網絡中顯示廣告。
 <br> <br>
 別忘了，任何批量郵件都應遵守您執行該國的相應法律。
 <br> <br>
 我們不會檢查您的營銷材料的內容，也無法驗證您是否已獲得與您要聯繫的人的許可。因此，我們保留停止郵寄或在您的Bitrix24中禁用CRM營銷功能的權利，而沒有透露原因，如果我們發現違規行為（通過我們自己的調查，來自其他用戶或政府機構的投訴）。
 
 <ol class = \“ tracker-agreement-popup-list \”>
  <li class = \“ tracker-agreement-popup-list-item \”>
   BITRIX24 CRM營銷 - 是Bitrix24 CRM的功能，能夠通過電子郵件，短信，聊天和其他資源大量發送消息。
    <br>
   注意力。未經收件人同意，郵件郵件以及通過CRM營銷的批量郵件明確禁止！
  </li>
  <li class = \“ tracker-agreement-popup-list-item \”>
    通過按\ \“我同意\”按鈕，您表示並保證以下內容：
   
   <ul class = \“ tracker-agreement-popup-inner-list \”>
    <li class = \“ tracker-agreement-popup-inner-list-item \”>
     您應在CRM營銷的完整時期內完全遵守法律要求，適用於廣告，知識產權等（以下是適用的立法）；
    </li>
    <li class = \“ tracker-agreement-popup-inner-list-item \”>
     您保證將獲得（-s）的書面或任何其他必要的批准，您通過CRM營銷向其發送消息；
    </li>
    <li class = \“ tracker-agreement-popup-inner-list-item \”>
     在開始使用CRM營銷之前，如果您對收集批准及其格式有疑問，則應諮詢相應的專家（法律專家）；
    </li>
    <li class = \“ tracker-agreement-popup-inner-list-item \”>
     您保證，您收到了通過CRM營銷發送的任何信息，並符合適用的立法；
    </li>
    <li class = \“ tracker-agreement-popup-inner-list-item \”>
     CRM營銷的任何使用都是由您自己責任執行的；
    </li>
    <li class = \“ tracker-agreement-popup-inner-list-item \”>
     您確認並確認，Bitrix24不接受發送消息的說明，不修改信息，通過CRM營銷發送，並且不會啟動此類信息的轉移，也不能也不了解其潛在的非法性。
    </li>
   </ul>
   
  </li>
  <li class = \“ tracker-agreement-popup-list-item \”>
   如果您違反了上述規定和保證，Bitrix24保留全面或部分將CRM營銷功能封鎖CRM營銷功能的權利。
  </li>
 </ol>
</div>";
