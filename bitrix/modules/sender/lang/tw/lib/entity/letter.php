<?php
$MESS["SENDER_ENTITY_LETTER_COPY_PREFIX"] = "複製";
$MESS["SENDER_ENTITY_LETTER_ERROR_NOT_AVAILABLE"] = "此選項僅在商業計劃上可用。";
$MESS["SENDER_ENTITY_LETTER_ERROR_NO_SEGMENTS"] = "沒有收件人段。";
