<?php
$MESS["SENDER_B24_LICENSE_AD_TEXT1"] = "此功能僅在商業計劃上可用。現在升級並享受更多CRM營銷工具：
<br> <br>
1）廣告管理
<br> <br>
2）銷售提升
<br> <br>
3）向客戶發送批量SMS消息
<br> <br>
4）無限的電子郵件新聞通訊
<br> <br>
5）基於您的客戶群的呼叫列表
<br> <br>
<a target = \"_blank \" href= \"https://www.bitrix24.com/prices/ \ \">了解更多有關所包括的商業計劃和功能的信息。";
$MESS["SENDER_B24_LICENSE_AD_TITLE"] = "廣告系列管理";
$MESS["SENDER_B24_LICENSE_MAILING_TITLE"] = "短信營銷，使者和機器人";
$MESS["SENDER_B24_LICENSE_MAIL_LIMIT_TEXT"] = "請升級到增加每日發送限額的主要計劃之一。
<br> <br>
您還將享受：
<br> <br>
1）銷售計劃
<br> <br>
2）更多交易管道
<br> <br>
3）自動化規則以自動化銷售流程
<br> <br>
以及其他生產力工具和功能！
<br> <br>
閱讀有關計劃的更多信息，並選擇最適合您的計劃<a href= \"https://www.bitrix24.com/prices/prices/xprices/vrices/xproice/frices/frices/frices/frices/frices /frices/frices/frices/frices/frices/frices/frices/var\">。
";
$MESS["SENDER_B24_LICENSE_MAIL_LIMIT_TITLE"] = "每日發送限制";
$MESS["SENDER_B24_LICENSE_RC_TITLE"] = "銷售提升";
