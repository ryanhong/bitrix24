<?php
$MESS["Char"] = "特點";
$MESS["CharTitle"] = "當前角色";
$MESS["Chars"] = "人物";
$MESS["DarkTheme"] = "黑暗背景";
$MESS["EnableHighlight"] = "語法突出顯示";
$MESS["EnableHighlightTitle"] = "切換語法突出顯示";
$MESS["GoToLine"] = "上線";
$MESS["HighlightWrongwarning"] = "該瀏覽器可能不支持在一定程度上強調語法。";
$MESS["LightTheme"] = "光背景";
$MESS["Line"] = "線";
$MESS["LineTitle"] = "當前行";
$MESS["Lines"] = "線";
$MESS["Total"] = "全部的";
