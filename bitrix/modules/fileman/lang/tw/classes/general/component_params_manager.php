<?php
$MESS["PAR_MAN_DEFAULT"] = "通用模板";
$MESS["PAR_MAN_DEF_TEMPLATE"] = "內置模板";
$MESS["PAR_MAN_NO_SEARCH_RESULTS"] = "找不到條目。";
$MESS["PAR_MAN_SEARCH"] = "搜尋";
$MESS["PAR_MAN_SELECT_NO_VALUE"] = "（未指定）";
$MESS["PAR_MAN_SELECT_OTHER"] = "（其他）";
$MESS["PAR_MAN_TEMPLATE_GROUP"] = "組件模板";
