<?php
$MESS["AddLI"] = "添加列表項目";
$MESS["AddLITitle"] = "添加列表項目（輸入）";
$MESS["AlignCenter"] = "居中對齊";
$MESS["AlignJustify"] = "證明合法";
$MESS["AlignLeft"] = "左對齊";
$MESS["AlignRight"] = "對齊";
$MESS["BXEdAddListItem"] = "添加列表項目";
$MESS["BXEdAddSnippet"] = "添加摘要";
$MESS["BXEdAnchor"] = "書籤";
$MESS["BXEdAnchorInsertTitle"] = "按Enter插入";
$MESS["BXEdAnchorName"] = "書籤名稱";
$MESS["BXEdBackColor"] = "背景顏色";
$MESS["BXEdBbCode"] = "BB代碼模式";
$MESS["BXEdBgColor"] = "背景顏色";
$MESS["BXEdCSSStyle"] = "CSS樣式";
$MESS["BXEdCode"] = "將文本包裝在代碼標籤中";
$MESS["BXEdColorOther"] = "其他...";
$MESS["BXEdColorpickerDialog"] = "文本和背景顏色";
$MESS["BXEdCompSearchPlaceHolder"] = "搜索組件...";
$MESS["BXEdComplexComp"] = "複合組件";
$MESS["BXEdContMenuComponent"] = "組件參數";
$MESS["BXEdContMenuComponentRemove"] = "刪除組件";
$MESS["BXEdContMenuHtmlComment"] = "編輯HTML評論";
$MESS["BXEdContMenuIframe"] = "編輯框架（iframe）";
$MESS["BXEdContMenuJavascript"] = "編輯JavaScript代碼";
$MESS["BXEdContMenuObject"] = "編輯媒體";
$MESS["BXEdContMenuPhpCode"] = "編輯PHP代碼";
$MESS["BXEdContMenuStyle"] = "編輯樣式（樣式）";
$MESS["BXEdCopilotButtonText"] = "副駕駛";
$MESS["BXEdCopilotPlaceholder"] = "輸入 @提及某人或調用AI的空間";
$MESS["BXEdCopilotPlaceholderWithoutMention"] = "按空格開始AI";
$MESS["BXEdCopilotPlaceholderWithoutMention_MSGVER_1"] = "按空間使用副駕駛";
$MESS["BXEdCopilotPlaceholder_MSGVER_1"] = "輸入 @提及某人或激活AI的空間";
$MESS["BXEdCopilotPlaceholder_MSGVER_2"] = "鍵入 @提及某人或使用副駕駛的空間";
$MESS["BXEdCssClass"] = "CSS課程";
$MESS["BXEdDefaultColor"] = "預設";
$MESS["BXEdDefaultPropDialog"] = "屬性：＃nodes_list＃";
$MESS["BXEdDefaultPropDialogTextNode"] = "文字";
$MESS["BXEdDelFromText"] = "從文本中刪除";
$MESS["BXEdDropCatConfirm"] = "此類別及其包含的所有數據將不可逆轉地刪除。你確定你要刪除嗎？";
$MESS["BXEdEditAnchor"] = "書籤屬性";
$MESS["BXEdEditSnippetDialogTitle"] = "摘要參數";
$MESS["BXEdExitConfirm"] = "注意力！所有未保存的信息將丟失。你真的想退出嗎？";
$MESS["BXEdForeColor"] = "文字顏色";
$MESS["BXEdFullscreen"] = "全螢幕";
$MESS["BXEdFullscreenBack"] = "全屏（按ESC退出全屏模式）";
$MESS["BXEdHtmlCode"] = "HTML模式";
$MESS["BXEdHtmlComment"] = "HTML評論";
$MESS["BXEdIframe"] = "框架（iframe）";
$MESS["BXEdImgAlign"] = "對齊";
$MESS["BXEdImgAlignBottom"] = "底部";
$MESS["BXEdImgAlignLeft"] = "左邊";
$MESS["BXEdImgAlignMiddle"] = "中間";
$MESS["BXEdImgAlignNone"] = "不要指定";
$MESS["BXEdImgAlignRight"] = "正確的";
$MESS["BXEdImgAlignTop"] = "頂部";
$MESS["BXEdImgAlt"] = "alt屬性";
$MESS["BXEdImgHeight"] = "高度";
$MESS["BXEdImgLinkOnImage"] = "創建鏈接";
$MESS["BXEdImgNoSize"] = "不要指定尺寸";
$MESS["BXEdImgSize"] = "尺寸";
$MESS["BXEdImgSrc"] = "圖像源";
$MESS["BXEdImgSrcRequired"] = "圖像路徑（必需）";
$MESS["BXEdImgTitle"] = "標題";
$MESS["BXEdImgWidth"] = "寬度";
$MESS["BXEdInsert"] = "插入";
$MESS["BXEdInsertHr"] = "水平線";
$MESS["BXEdInsertTable"] = "插入表...";
$MESS["BXEdInsertTableTitle"] = "表屬性對話框";
$MESS["BXEdInsertVideo"] = "插入視頻";
$MESS["BXEdInvalidBrowser"] = "您的瀏覽器已經過時了。某些功能可能不可用，也可能無法設計。";
$MESS["BXEdLinkAdditionalTitle"] = "更多參數";
$MESS["BXEdLinkHref"] = "關聯";
$MESS["BXEdLinkHrefAnch"] = "書籤";
$MESS["BXEdLinkHrefEmail"] = "電子郵件";
$MESS["BXEdLinkHrefExtPh"] = "網站URL";
$MESS["BXEdLinkHrefPh"] = "鏈接URL";
$MESS["BXEdLinkId"] = "ID（ID）";
$MESS["BXEdLinkInnerHtml"] = "鏈接內容";
$MESS["BXEdLinkNoindex"] = "拒絕搜索引擎（nofollow，noindex）";
$MESS["BXEdLinkRel"] = "rel屬性";
$MESS["BXEdLinkSelectAnchor"] = "選擇書籤";
$MESS["BXEdLinkTarget"] = "目標窗口";
$MESS["BXEdLinkTargetBlank"] = "新窗口（_blank）";
$MESS["BXEdLinkTargetDef"] = "- 預設 -";
$MESS["BXEdLinkTargetParent"] = "父窗口（_Parent）";
$MESS["BXEdLinkTargetSelf"] = "自窗（_ self）";
$MESS["BXEdLinkTargetTop"] = "最上方的窗口（_top）";
$MESS["BXEdLinkText"] = "文字";
$MESS["BXEdLinkTextPh"] = "鏈接文字";
$MESS["BXEdLinkTitle"] = "工具提示";
$MESS["BXEdLinkType"] = "鏈接類型";
$MESS["BXEdLinkTypeAnchor"] = "在此頁面中";
$MESS["BXEdLinkTypeEmail"] = "電子郵件地址";
$MESS["BXEdLinkTypeInner"] = "該網站上的頁面";
$MESS["BXEdLinkTypeOuter"] = "其他網站";
$MESS["BXEdLoremIpsum"] = "Lorem Ipsum Dolor Sit Amet，Consectur adipiscing Elit。在Ultrics Mi Sit Amet前庭軟體動物中。 Nunc Nibh Enim，Convallis Vel Tortor Eget，Interdum Dapibus DUI。 Pellentesque rhoncus tortor vel leo distum tincidunt。 Curabitur Nec Sollicitudin lacus，Nec Hendrerit Urna。 Morbi Adipiscing，Risus ID會眾Eleifend，Nibh Velit Fringilla Ligula，rutrum ante ligula vel erat。毛里斯·薩特（Houris）的毛里斯（Mauris）在Ante Velit中，在DUI UT的Posuere，超級等級DUI。整數Est Odio，在Erat in的發酵，Tristique luctus turpis。提取一個urna pulvinar，rajectat quam sed，subilisis ligula。 Mauris Sed Justo Mauris。 Cras Tincidunt Tincidunt Laoreet。 Curabitur eget ante tortor。 Cras Malesuada累積了元素。 Aliquam NEC InterDum NIBH。 Sed在Tristique Massa，Quis luctus erat。 Nulla Imperdiet Hendrerit Lectus Nec Blandit。";
$MESS["BXEdManageCategories"] = "管理類別";
$MESS["BXEdManageCategoriesTitle"] = "摘要類別";
$MESS["BXEdMetaClass_block"] = "文本塊";
$MESS["BXEdMetaClass_block_icon"] = "帶有圖像的文本塊";
$MESS["BXEdMetaClass_list"] = "列表";
$MESS["BXEdMetaClass_quote"] = "引用";
$MESS["BXEdMetaClass_text"] = "選定的文本";
$MESS["BXEdMore"] = "更多的...";
$MESS["BXEdNoPspellWarning"] = "未安裝Pspell擴展名。請聯繫您的管理員。";
$MESS["BXEdObjectEmbed"] = "媒體";
$MESS["BXEdPageBreak"] = "插入頁面折斷<break />";
$MESS["BXEdPageBreakSur"] = "<break />";
$MESS["BXEdPageBreakSurTitle"] = "頁面隔板<break />";
$MESS["BXEdPasteDefault"] = "預設";
$MESS["BXEdPasteFormattedText"] = "豐富的格式文本";
$MESS["BXEdPasteSetBgBorders"] = "刪除塊顏色和邊框樣式";
$MESS["BXEdPasteSetColors"] = "刪除文字和背景色彩樣式";
$MESS["BXEdPasteSetDecor"] = "刪除文本變體（粗體，斜體，下劃線）";
$MESS["BXEdPasteSetTableDimen"] = "表和單元格的清晰寬度和高度屬性";
$MESS["BXEdPasteSettings"] = "淨化粘貼的內容";
$MESS["BXEdPasteText"] = "純文本";
$MESS["BXEdPhpCode"] = "PHP代碼";
$MESS["BXEdPhpCodeProtected"] = "PHP代碼（受訪問權限保護）";
$MESS["BXEdPrintBreak"] = "插入打印的頁面休息";
$MESS["BXEdPrintBreakName"] = "分頁符";
$MESS["BXEdPrintBreakTitle"] = "打印的頁面休息";
$MESS["BXEdQuote"] = "引用文字";
$MESS["BXEdRefreshNotice"] = "刷新頁面後，更改將生效。";
$MESS["BXEdSettings"] = "設定";
$MESS["BXEdSettingsCleanSpans"] = "刪除＆lt; span＆gt;沒有屬性的標籤";
$MESS["BXEdShowSnippets"] = "顯示摘要欄";
$MESS["BXEdSmile"] = "笑臉";
$MESS["BXEdSnipAddSettings"] = "更多的";
$MESS["BXEdSnipBaseSettings"] = "一般參數";
$MESS["BXEdSnipCatAdd"] = "添加類別";
$MESS["BXEdSnipCatAddBut"] = "創造";
$MESS["BXEdSnipCatAddName"] = "姓名";
$MESS["BXEdSnipCatDelete"] = "刪除類別及其包含的所有數據";
$MESS["BXEdSnipCatEdit"] = "重命名類別";
$MESS["BXEdSnipCategory"] = "類別";
$MESS["BXEdSnipCode"] = "摘要代碼";
$MESS["BXEdSnipCodePlaceHolder"] = "摘要HTML代碼（必需）";
$MESS["BXEdSnipDescription"] = "附加信息";
$MESS["BXEdSnipDescriptionPlaceholder"] = "工具提示（當鼠標指針在片段上時可見）";
$MESS["BXEdSnipEdit"] = "編輯摘要";
$MESS["BXEdSnipFileName"] = "文件名";
$MESS["BXEdSnipName"] = "姓名";
$MESS["BXEdSnipNamePlaceHolder"] = "摘要名稱（必需）";
$MESS["BXEdSnipNoSnippets"] = "沒有摘要";
$MESS["BXEdSnipParCategory"] = "父類別";
$MESS["BXEdSnipRemove"] = "刪除片段";
$MESS["BXEdSnipRemoveConfirm"] = "您確定要刪除摘要嗎？";
$MESS["BXEdSnipSearchPlaceHolder"] = "搜索片段...";
$MESS["BXEdSnipSiteTemplate"] = "站點模板";
$MESS["BXEdSnippetsTitle"] = "摘要";
$MESS["BXEdSpecialchar"] = "特殊字符";
$MESS["BXEdSpecialcharMore"] = "其他角色...";
$MESS["BXEdSpecialcharMoreTitle"] = "插入其他特殊角色";
$MESS["BXEdSpellErrorLabel"] = "找不到字";
$MESS["BXEdSpellNoErrors"] = "找不到錯誤";
$MESS["BXEdSpellSuggestion"] = "建議";
$MESS["BXEdSpellWait"] = "現在檢查拼寫，請等待...";
$MESS["BXEdSpellcheck"] = "拼寫";
$MESS["BXEdSpellcheckAddCustom"] = "添加到字典中";
$MESS["BXEdSpellcheckReplace"] = "代替";
$MESS["BXEdSpellcheckReplaceAll"] = "全部替換";
$MESS["BXEdSpellcheckSkip"] = "跳過";
$MESS["BXEdSpellcheckSkipAll"] = "跳過所有";
$MESS["BXEdStyle"] = "樣式（樣式）";
$MESS["BXEdSub"] = "下標";
$MESS["BXEdSup"] = "上標";
$MESS["BXEdTable"] = "桌子";
$MESS["BXEdTableAlign"] = "對齊";
$MESS["BXEdTableAlignCenter"] = "中心";
$MESS["BXEdTableAlignLeft"] = "左邊";
$MESS["BXEdTableAlignRight"] = "正確的";
$MESS["BXEdTableBorder"] = "邊界";
$MESS["BXEdTableCaption"] = "桌子標題";
$MESS["BXEdTableCellPadding"] = "細胞填充";
$MESS["BXEdTableCellSpacing"] = "細胞間距";
$MESS["BXEdTableCols"] = "列";
$MESS["BXEdTableDelCol"] = "刪除列";
$MESS["BXEdTableDelRow"] = "刪除行";
$MESS["BXEdTableDeleteTable"] = "刪除表";
$MESS["BXEdTableDellCell"] = "刪除單元格";
$MESS["BXEdTableDellSelectedCells"] = "刪除選定的單元格";
$MESS["BXEdTableHeads"] = "標題";
$MESS["BXEdTableHeight"] = "高度";
$MESS["BXEdTableId"] = "ID（ID）";
$MESS["BXEdTableInsCell"] = "細胞";
$MESS["BXEdTableInsCellAfter"] = "向右插入單元格";
$MESS["BXEdTableInsCellBefore"] = "向左插入電池";
$MESS["BXEdTableInsColLeft"] = "在左側插入列";
$MESS["BXEdTableInsColRight"] = "在右側插入列";
$MESS["BXEdTableInsColumn"] = "柱子";
$MESS["BXEdTableInsRow"] = "排";
$MESS["BXEdTableInsRowLower"] = "在下面插入行";
$MESS["BXEdTableInsRowUpper"] = "在上面插入行";
$MESS["BXEdTableInsertMenu"] = "插入";
$MESS["BXEdTableMergeBottom"] = "合併到底部";
$MESS["BXEdTableMergeColCells"] = "合併所有細胞";
$MESS["BXEdTableMergeError"] = "無法合併所選單元。";
$MESS["BXEdTableMergeMenu"] = "合併";
$MESS["BXEdTableMergeRight"] = "合併到正確";
$MESS["BXEdTableMergeRowCells"] = "合併所有細胞";
$MESS["BXEdTableMergeSelectedCells"] = "合併選定的細胞";
$MESS["BXEdTableModel"] = "佈局";
$MESS["BXEdTableRemoveMenu"] = "消除";
$MESS["BXEdTableRows"] = "行";
$MESS["BXEdTableSplitCellHor"] = "水平拆分單元";
$MESS["BXEdTableSplitCellVer"] = "垂直分裂";
$MESS["BXEdTableSplitMenu"] = "拆分單元";
$MESS["BXEdTableTableCellProps"] = "細胞性質";
$MESS["BXEdTableTableProps"] = "表屬性";
$MESS["BXEdTableWidth"] = "寬度";
$MESS["BXEdTextAlign"] = "對齊...";
$MESS["BXEdThLeft"] = "左列";
$MESS["BXEdThNone"] = "沒有前進";
$MESS["BXEdThTop"] = "上排";
$MESS["BXEdThTopLeft"] = "左上方";
$MESS["BXEdThTopLeftTitle"] = "上排，左列";
$MESS["BXEdVideoDel"] = "刪除視頻";
$MESS["BXEdVideoInfoTitle"] = "視頻標題";
$MESS["BXEdVideoPreview"] = "預覽";
$MESS["BXEdVideoProps"] = "視頻屬性";
$MESS["BXEdVideoSize"] = "視頻尺寸";
$MESS["BXEdVideoSizeAuto"] = "汽車";
$MESS["BXEdVideoSizeCustom"] = "其他尺寸";
$MESS["BXEdVideoSource"] = "視頻來源";
$MESS["BXEdVideoSourcePlaceholder"] = "YouTube或Vimeo視頻URL";
$MESS["BXEdVideoTitle"] = "影片";
$MESS["BXEdVideoTitleProvider"] = "來自＃provider_name＃的視頻";
$MESS["BXEdViewSettings"] = "查看設置";
$MESS["Bold"] = "大膽的";
$MESS["ButtonSearch"] = "搜索和替換";
$MESS["ButtonViewMode"] = "查看模式";
$MESS["ComponentPropsTitle"] = "\“＃component_name＃\”參數";
$MESS["ComponentPropsWait"] = "請等待，加載參數...";
$MESS["ComponentsTitle"] = "成分";
$MESS["ContMenuCleanDiv"] = "刪除塊";
$MESS["ContMenuCleanDiv_Title"] = "塊元素將被其內容（子元素）替換";
$MESS["ContMenuDefProps"] = "特性";
$MESS["ContMenuImgDel"] = "刪除圖像";
$MESS["ContMenuImgEdit"] = "編輯圖像";
$MESS["ContMenuLinkDel"] = "刪除鏈接";
$MESS["ContMenuLinkEdit"] = "編輯鏈接";
$MESS["CutTitle"] = "剪切標題";
$MESS["DelListItem"] = "刪除列表項目";
$MESS["DialogCancel"] = "取消";
$MESS["DialogClose"] = "關閉";
$MESS["DialogSave"] = "節省";
$MESS["DragFloatingToolbar"] = "工具欄";
$MESS["EditLink"] = "編輯鏈接";
$MESS["FontSelectorTitle"] = "字體";
$MESS["FontSizeTitle"] = "字體大小";
$MESS["Heading"] = "標題";
$MESS["HeadingMore"] = "更多標題";
$MESS["Height"] = "高度";
$MESS["Indent"] = "縮進";
$MESS["InsertCode"] = "將文本包裝在代碼標籤中";
$MESS["InsertCut"] = "插入切割";
$MESS["InsertHr"] = "水平線";
$MESS["InsertImage"] = "圖像";
$MESS["InsertLink"] = "關聯";
$MESS["Italic"] = "斜體";
$MESS["ListItems"] = "列出項目";
$MESS["NoFontTitle"] = "不要指定";
$MESS["NodeProps"] = "特性";
$MESS["NodeRemove"] = "刪除";
$MESS["NodeRemoveBodyContent"] = "重置內容";
$MESS["NodeSelect"] = "選擇";
$MESS["NodeSelectBody"] = "全選";
$MESS["Off"] = "離開";
$MESS["On"] = "在";
$MESS["OrderedList"] = "編號列表";
$MESS["Outdent"] = "八個";
$MESS["Preformatted"] = "預製";
$MESS["Quote"] = "引用文字";
$MESS["Redo"] = "重做";
$MESS["RefreshTaskbar"] = "重新整理";
$MESS["RemoveFormat"] = "刪除格式";
$MESS["RemoveLink"] = "刪除鏈接";
$MESS["SpecialChar"] = "插入特殊字符";
$MESS["SrcTitle"] = "圖像";
$MESS["Strike"] = "罷工";
$MESS["StyleDiv"] = "塊（DIV標籤）";
$MESS["StyleH1"] = "標題1";
$MESS["StyleH2"] = "標題2";
$MESS["StyleH3"] = "標題3";
$MESS["StyleH4"] = "標題4";
$MESS["StyleH5"] = "標題5";
$MESS["StyleH6"] = "標題6";
$MESS["StyleNormal"] = "普通的";
$MESS["StyleParagraph"] = "段落（p標籤）";
$MESS["StyleSelectorName"] = "風格";
$MESS["StyleSelectorTitle"] = "樣式和格式";
$MESS["TemplateSelectorTitle"] = "站點模板";
$MESS["Translit"] = "音譯文本";
$MESS["Underline"] = "強調";
$MESS["Undo"] = "撤消";
$MESS["UnorderedList"] = "項目符號列表";
$MESS["UrlTitle"] = "關聯";
$MESS["ViewCode"] = "原始碼";
$MESS["ViewSplitHor"] = "拆分視圖（水平）";
$MESS["ViewSplitVer"] = "拆分視圖（垂直）";
$MESS["ViewWysiwyg"] = "視覺模式";
$MESS["Width"] = "寬度";
