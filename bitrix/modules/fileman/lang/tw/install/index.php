<?php
$MESS["FILEMAN_ACCESSABLE_FOLDERS"] = "僅到可訪問的文件夾";
$MESS["FILEMAN_DENIED"] = "否認";
$MESS["FILEMAN_HOTKEY_TITLE"] = "在控制面板中應用文件編輯器的更改";
$MESS["FILEMAN_INSTALL_TITLE"] = "站點Explorer模塊安裝";
$MESS["FILEMAN_MODULE_DESCRIPTION"] = "該模塊管理站點內容，結構，菜單和用戶訪問權限。";
$MESS["FILEMAN_MODULE_NAME"] = "網站資源管理器";
$MESS["FILEMAN_UNINSTALL_TITLE"] = "站點資源管理器模塊卸載";
$MESS["FILEMAN_VIEW"] = "在只讀模式下查看模塊設置";
