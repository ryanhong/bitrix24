<?php
$MESS["FILEMAN_FILEUPLOAD_ACCESS_DENIED"] = "沒有足夠的權限來創建文件";
$MESS["FILEMAN_FILEUPLOAD_FILE"] = "上傳文件";
$MESS["FILEMAN_FILEUPLOAD_FILE_CREATE_ERROR"] = "創建文件時錯誤";
$MESS["FILEMAN_FILEUPLOAD_FILE_EXISTS1"] = "命名的文件";
$MESS["FILEMAN_FILEUPLOAD_FILE_EXISTS2"] = "已經存在";
$MESS["FILEMAN_FILEUPLOAD_NAME"] = "文件名";
$MESS["FILEMAN_FILEUPLOAD_PHPERROR"] = "沒有足夠的權限上傳PHP文件";
$MESS["FILEMAN_FILEUPLOAD_SIZE_ERROR"] = "上傳文件超出允許的文件大小：\“＃file_name＃\”";
$MESS["FILEMAN_FILEUPLOAD_UPLOAD"] = "上傳文件";
$MESS["FILEMAN_FILE_UPLOAD_TITLE"] = "文件上傳";
$MESS["FILEMAN_UPL_TAB"] = "文件加載";
$MESS["FILEMAN_UPL_TAB_ALT"] = "文件加載";
