<?php
$MESS["FM_ST_ACCESS"] = "使用權";
$MESS["FM_ST_ACCESS_DEFAULT"] = "預設";
$MESS["FM_ST_ACCESS_NOTE"] = "要使用戶可以使用Web-IT註釋功能，請確保用戶在用戶組中，其中具有＃link_begin＃site Explorer模塊訪問權限＃link_end＃和文件夾/bitrix/admin//的讀取訪問權限。";
$MESS["FM_ST_ACCESS_TITLE"] = "Web-It注意訪問";
$MESS["FM_ST_ADD_GROUP_TASK"] = "添加訪問權限";
$MESS["FM_ST_CLEAR_ALL"] = "刪除所有貼紙";
$MESS["FM_ST_CLEAR_ALL_CONFIRM"] = "您確定要刪除所有網絡註釋嗎？";
$MESS["FM_ST_EDIT_GROUP_TITLE"] = "查看用戶組參數";
$MESS["FM_ST_SELECT_GROUP"] = "選擇組";
$MESS["FM_ST_SETTINGS"] = "設定";
$MESS["FM_ST_SETTINGS_TITLE"] = "Web-IT註釋參數";
$MESS["FM_ST_SET_HIDE_BOTTOM"] = "隱藏web-it注意底欄";
$MESS["FM_ST_SET_SIZES"] = "初始的Web-IT音符大小（寬度x高，pix）。";
$MESS["FM_ST_SET_SMART_MARKER"] = "將選定的區域綁定到網頁元素";
$MESS["FM_ST_SET_SUPAFLY"] = "使用視覺效果進行Web-It音符";
$MESS["FM_ST_USE_HOTKEYS"] = "使用Hotkeys添加Web-It筆記";
