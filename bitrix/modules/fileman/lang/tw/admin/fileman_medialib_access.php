<?php
$MESS["FM_ML_ACCESS_TITLE"] = "媒體庫集合訪問權限";
$MESS["FM_ML_BACK_IN_ML"] = "返回媒體庫";
$MESS["FM_ML_TAB_NAME"] = "訪問權限";
$MESS["FM_ML_TAB_TITLE"] = "設置訪問媒體庫集合的權限";
$MESS["ML_ACCESS_FOR_ALL"] = "所有收藏的共同訪問";
$MESS["ML_ACCESS_GROUP"] = "團體";
$MESS["ML_ACCESS_TASK"] = "訪問權限";
$MESS["ML_SELECT_COLLECTION"] = "選擇集合";
$MESS["ML_TASK_INHERIT"] = "繼承";
$MESS["ML_TASK_MEDIALIB_DENIED"] = "否定";
$MESS["ML_TASK_MEDIALIB_EDITOR"] = "編輯";
$MESS["ML_TASK_MEDIALIB_EDIT_ITEMS"] = "僅編輯元素";
$MESS["ML_TASK_MEDIALIB_FULL"] = "完全訪問";
$MESS["ML_TASK_MEDIALIB_ONLY_NEW"] = "創建新的";
$MESS["ML_TASK_MEDIALIB_VIEW"] = "看法";
