<?php
$MESS["FILEMAN_MNU_F_AND_F"] = "文件和文件夾";
$MESS["FILEMAN_MNU_F_AND_F_TITLE"] = "服務器上的文件和文件夾結構";
$MESS["FILEMAN_MNU_STRUC"] = "站點結構";
$MESS["FILEMAN_MNU_WN"] = "<untitled>";
$MESS["FMST_STICKERS"] = "網絡註釋";
$MESS["FMST_STICKERS_TITLE"] = "管理Web-It筆記";
$MESS["FM_MENU_DESC"] = "文件和站點結構管理";
$MESS["FM_MENU_MEDIALIB"] = "媒體庫";
$MESS["FM_MENU_MEDIALIB_TITLE"] = "管理媒體庫中的收藏和元素";
$MESS["FM_MENU_TITLE"] = "網站資源管理器";
$MESS["FM_STATIC_CONTROL"] = "瀏覽";
$MESS["FM_STATIC_CONTROL_ALT"] = "管理網站結構的文件，文件夾，菜單和用戶訪問權限";
