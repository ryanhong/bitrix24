<?php
$MESS["USER_TYPE_ADDRESS_DESCRIPTION"] = "地址";
$MESS["USER_TYPE_ADDRESS_NO_KEY_HINT"] = "需要使用Google API密鑰才能使用地圖。請在設置表單上輸入您的密鑰<a href= \“#settings_path_path #google_api_key_key \”target= \"_blank \">。";
$MESS["USER_TYPE_ADDRESS_NO_KEY_HINT_B24"] = "需要使用Google API密鑰才能使用地圖。請在設置表單上輸入您的密鑰<a href= \“#settings_path_path #google_api_key_key \”target= \"_blank \">。";
$MESS["USER_TYPE_ADDRESS_SHOW_MAP"] = "顯示地圖";
$MESS["USER_TYPE_ADDRESS_TRIAL"] = "<span>享受Google Maps：</span> <ol> <li>獲取地址建議; </li> <li>在交互式地圖上顯示地址。</li> </ol> </ol> <span > Google Maps in CRM實體可在選定的商業計劃中獲得。</span>";
$MESS["USER_TYPE_ADDRESS_TRIAL_TITLE"] = "您的Google地圖試用期結束了";
