<?php
$MESS["ERROR_EMPTY_PATH"] = "播放列表的不正確路徑";
$MESS["PC_DEFAUL_SKIN"] = "預設";
$MESS["PC_GROUP_ADDITIONAL_FLV"] = "額外的閃光播放器設置";
$MESS["PC_GROUP_ADDITIONAL_SETTINGS"] = "其他參數";
$MESS["PC_GROUP_APPEARANCE_COMMON"] = "通用設計參數";
$MESS["PC_GROUP_APPERANCE"] = "設計";
$MESS["PC_GROUP_APPERANCE_FLV"] = "Flash Player Design";
$MESS["PC_GROUP_APPERANCE_WMV"] = "WMV播放器設計";
$MESS["PC_GROUP_BASE_SETTINGS"] = "一般參數";
$MESS["PC_GROUP_FILE_PARAMETERS"] = "視頻文件參數";
$MESS["PC_GROUP_PLAYBACK"] = "播放設置";
$MESS["PC_GROUP_PLAYBACK_FLV"] = "Flash Player的其他設置";
$MESS["PC_PAR_ADDITIONAL_FLASHVARS"] = "其他閃光變量";
$MESS["PC_PAR_ADDITIONAL_WMVVARS"] = "其他Silverlight變量";
$MESS["PC_PAR_ADVANCED_MODE"] = "高級模式";
$MESS["PC_PAR_ALLOW_SWF"] = "允許SWF文件播放（不建議）";
$MESS["PC_PAR_AUTOLOAD"] = "自動加載視頻";
$MESS["PC_PAR_AUTOSTART"] = "自動啟動";
$MESS["PC_PAR_AUTOSTART_ON_SCROLL"] = "當玩家可見時自動播放媒體";
$MESS["PC_PAR_BGCOLOR"] = "控制面板背景的顏色";
$MESS["PC_PAR_BUFFER_LENGTH"] = "緩衝長度，秒。";
$MESS["PC_PAR_COLOR"] = "控件的顏色";
$MESS["PC_PAR_CONTROLS"] = "播放控制位置";
$MESS["PC_PAR_CONTROLS_BOTTOM"] = "底部";
$MESS["PC_PAR_CONTROLS_NONE"] = "隱藏";
$MESS["PC_PAR_CONTROLS_OVER"] = "超過";
$MESS["PC_PAR_CREATE"] = "創造";
$MESS["PC_PAR_DOWNLOAD_LINK"] = "文件下載鏈接";
$MESS["PC_PAR_EDIT"] = "編輯";
$MESS["PC_PAR_FILE_AUTHOR"] = "作者";
$MESS["PC_PAR_FILE_DATE"] = "出版";
$MESS["PC_PAR_FILE_DESCRIPTION"] = "文件描述";
$MESS["PC_PAR_FILE_DURATION"] = "持續時間（SEC）";
$MESS["PC_PAR_FILE_PATH"] = "文件路徑";
$MESS["PC_PAR_FILE_TITLE"] = "標題";
$MESS["PC_PAR_FILE_TYPE"] = "文件MIME類型";
$MESS["PC_PAR_HEIGHT"] = "高度";
$MESS["PC_PAR_HIDE_MENU"] = "隱藏上下文菜單";
$MESS["PC_PAR_LINK_TARGET"] = "打開鏈接";
$MESS["PC_PAR_LINK_TARGET_BLANK"] = "新窗戶";
$MESS["PC_PAR_LINK_TARGET_SELF"] = "當前窗口";
$MESS["PC_PAR_LOGO"] = "水印路徑";
$MESS["PC_PAR_LOGO_LINK"] = "徽標URL";
$MESS["PC_PAR_LOGO_POSITION"] = "徽標位置";
$MESS["PC_PAR_LOGO_POS_BOTTOM_LEFT"] = "左下方";
$MESS["PC_PAR_LOGO_POS_BOTTOM_RIGHT"] = "右下";
$MESS["PC_PAR_LOGO_POS_NONE"] = "隱";
$MESS["PC_PAR_LOGO_POS_TOP_LEFT"] = "左上方";
$MESS["PC_PAR_LOGO_POS_TOP_RIGHT"] = "右上";
$MESS["PC_PAR_MUTE"] = "靜音負載";
$MESS["PC_PAR_NO_PREVIEW"] = "沒有圖像";
$MESS["PC_PAR_OVER_COLOR"] = "盤旋控件的顏色";
$MESS["PC_PAR_PLAYBACK_RATE"] = "播放速度";
$MESS["PC_PAR_PLAYER_AUTODETECT"] = "自動偵測";
$MESS["PC_PAR_PLAYER_FLV"] = "HTML5 / Flash Player";
$MESS["PC_PAR_PLAYER_ID"] = "玩家ID";
$MESS["PC_PAR_PLAYER_TYPE"] = "玩家類型";
$MESS["PC_PAR_PLAYER_WMV"] = "WMV播放器";
$MESS["PC_PAR_PLAYLIST"] = "播放列表位置";
$MESS["PC_PAR_PLAYLIST_BUT"] = "播放列表";
$MESS["PC_PAR_PLAYLIST_HIDE"] = "隱藏播放列表";
$MESS["PC_PAR_PLAYLIST_NUM"] = "播放列表條目的數量";
$MESS["PC_PAR_PLAYLIST_PATH"] = "播放列表路徑";
$MESS["PC_PAR_PLAYLIST_PREVIEW_HEIGHT"] = "播放列表圖像高度";
$MESS["PC_PAR_PLAYLIST_PREVIEW_WIDTH"] = "播放列表圖像寬度";
$MESS["PC_PAR_PLAYLIST_RIGHT"] = "正確的";
$MESS["PC_PAR_PLAYLIST_SIZE"] = "播放列表大小，PX";
$MESS["PC_PAR_PLAYLIST_TYPE"] = "播放列表格式";
$MESS["PC_PAR_PLUGINS"] = "插件";
$MESS["PC_PAR_PLUGIN_NAME"] = "\“＃plugin_name＃\”的參數";
$MESS["PC_PAR_PREVIEW_IMAGE"] = "預覽圖像路徑";
$MESS["PC_PAR_PROVIDER"] = "媒體提供商";
$MESS["PC_PAR_PROVIDER_HTTP"] = "HTTP-流視頻";
$MESS["PC_PAR_PROVIDER_IMAGE"] = "圖像（gif / jpg / png）";
$MESS["PC_PAR_PROVIDER_NONE"] = "沒有任何";
$MESS["PC_PAR_PROVIDER_RTMP"] = "RTMP-流視頻";
$MESS["PC_PAR_PROVIDER_SOUND"] = "音頻（MP3）";
$MESS["PC_PAR_PROVIDER_VIDEO"] = "視頻（FLV / MP4）";
$MESS["PC_PAR_PROVIDER_YOUTUBE"] = "youtube.com視頻";
$MESS["PC_PAR_REPEAT"] = "重複文件或播放列表";
$MESS["PC_PAR_REPEAT_ALWAYS"] = "永恆地重複文件/列表";
$MESS["PC_PAR_REPEAT_LIST"] = "播放清單一次";
$MESS["PC_PAR_REPEAT_NONE"] = "不要重複";
$MESS["PC_PAR_REPEAT_SINGLE"] = "重複單個文件";
$MESS["PC_PAR_SCREEN_COLOR"] = "屏幕顏色";
$MESS["PC_PAR_SHOWICONS"] = "在負載上的播放器屏幕中心顯示播放按鈕";
$MESS["PC_PAR_SHOW_CONTROLS"] = "顯示播放控件";
$MESS["PC_PAR_SHOW_DIGITS"] = "顯示當前和剩餘時間";
$MESS["PC_PAR_SHUFFLE"] = "洗牌播放列表";
$MESS["PC_PAR_SIZES_ABSOLUTE"] = "絕對大小";
$MESS["PC_PAR_SIZES_AUTO"] = "使用視頻維度";
$MESS["PC_PAR_SIZES_FLUID"] = "適合容器";
$MESS["PC_PAR_SIZES_TYPE"] = "尺碼類型";
$MESS["PC_PAR_SKIN"] = "皮膚";
$MESS["PC_PAR_SKIN_PATH"] = "通往皮膚文件夾的路徑";
$MESS["PC_PAR_START_FROM"] = "初始文件";
$MESS["PC_PAR_START_TIME"] = "播放開始時間（秒）";
$MESS["PC_PAR_STREAM"] = "使用流視頻";
$MESS["PC_PAR_STREAMER"] = "流視頻服務器端應用程序（流媒體）";
$MESS["PC_PAR_STREAM_TYPE"] = "流視頻類型";
$MESS["PC_PAR_USE_PLAYLIST"] = "使用播放列表";
$MESS["PC_PAR_USE_PLAYLIST_AS_SOURCES"] = "使用播放列表作為替代文件源";
$MESS["PC_PAR_VOLUME"] = "將音量設置為最大百分比";
$MESS["PC_PAR_WIDTH"] = "寬度";
$MESS["PC_PAR_WMODE"] = "窗口模式（WMODE）";
$MESS["PC_PAR_WMODE_OPAQUE"] = "不透明";
$MESS["PC_PAR_WMODE_TRANSPARENT"] = "透明的";
$MESS["PC_PAR_WMODE_WINDOW"] = "窗戶";
$MESS["PC_PAR_WMODE_WMV"] = "窗口模式";
