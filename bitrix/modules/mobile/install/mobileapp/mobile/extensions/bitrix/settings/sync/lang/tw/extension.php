<?php
$MESS["SE_SYNC_CAL_TITLE"] = "同步日曆";
$MESS["SE_SYNC_CONTACTS_TITLE"] = "同步觸點";
$MESS["SE_SYNC_PROFILE_DESCRIPTION"] = "通過將服務配置文件添加到您的iOS中，可以提供聯繫和日曆同步。要停止同步，請打開配置文件視圖並刪除同步配置文件。配置文件名稱與您的Bitrix24域相同。";
$MESS["SE_SYNC_SUBTITLE_TITLE"] = "日曆和聯繫人";
$MESS["SE_SYNC_TITLE"] = "同步";
