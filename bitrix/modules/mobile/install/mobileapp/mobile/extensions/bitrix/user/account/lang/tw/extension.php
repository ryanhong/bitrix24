<?php
$MESS["DELETE_ACCOUNT_CONTINUE"] = "繼續";
$MESS["DELETE_ACCOUNT_TITLE"] = "刪除用戶帳戶";
$MESS["DELETE_ACCOUNT_WARNING"] = "注意力！";
$MESS["DISCLAIMER_DELETE_ACCOUNT"] = "您將轉發到需要身份驗證的用戶配置文件刪除請求。
從Bitrix24客戶帳戶中刪除的用戶配置文件將在30天內發生，並將終止訪問該BITRIX24客戶帳戶。
您可以在用戶配置文件刪除請求中找到詳細信息。";
