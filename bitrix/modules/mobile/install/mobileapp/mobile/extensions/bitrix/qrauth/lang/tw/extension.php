<?php
$MESS["ACCEPT_QR_AUTH"] = "是的，繼續";
$MESS["AUTH_WAIT"] = "記錄你...";
$MESS["BROWSERS"] = "＃1＃Google Chrome，Microsoft Edge，Safari，Mozilla Firefox，Opera和其他瀏覽器
  ";
$MESS["DECLINE_QR_AUTH"] = "不";
$MESS["OPEN_BROWSER"] = "打開[size = 13] [b] [color =＃4578e7] #domain＃[/color] [/b] [/size]在您的計算機上";
$MESS["QR_HOW_TO_AUTH_MSGVER_1"] = "打開網絡版本：";
$MESS["QR_SCANNER_HINT_MSGVER_1"] = "僅在Web版本中可用";
$MESS["QR_WARNING"] = "您將使用QR碼登錄[B] #Domain＃[/b]的Bitrix24。繼續？";
$MESS["SCAN_QR"] = "用您的移動相機掃描[B] QR代碼[/b]";
$MESS["SCAN_QR_BUTTON"] = "掃描二維碼";
$MESS["STEP_CAMERA_TITLE"] = "掃描QR碼";
$MESS["STEP_OPEN_SITE_MSGVER_1"] = "在計算機上打開[B] [B] [Color =＃4578E7] #Domain＃[/color] [/b]";
$MESS["STEP_PRESS_CLOUD"] = "單擊[b]登錄[/b]和[img width = \“ 22 \” height = \“ 22 \”]＃url＃[/img] [b] qr code [/b]";
$MESS["STEP_PRESS_SELF_HOSTED"] = "單擊[B]使用QR代碼登錄[/b]";
$MESS["STEP_SCAN"] = "掃描QR碼";
$MESS["WRONG_QR"] = "QR碼不正確";
