<?php
$MESS["SE_SYS_ALLOW_INVITE_USERS"] = "任何人都可以邀請";
$MESS["SE_SYS_ALLOW_INVITE_USERS_DESC"] = "啟用此選項將允許在Bitrix24上註冊的任何員工邀請新員工。";
$MESS["SE_SYS_ENERGY_BACKGROUND"] = "在後台運行";
$MESS["SE_SYS_INVITE"] = "Bitrix24的邀請";
$MESS["SE_SYS_LOW_PUSH_ACTIVITY"] = "增加通知間隔";
$MESS["SE_SYS_LOW_PUSH_ACTIVITY_DESC"] = "使用此選項節省電池。您將獲得更少的服務通知和反更新。這不會影響基於內容的通知。";
$MESS["SE_SYS_TITLE"] = "其他設置";
