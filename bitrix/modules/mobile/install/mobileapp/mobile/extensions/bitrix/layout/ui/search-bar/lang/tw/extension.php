<?php
$MESS["M_CRM_ET_SEARCH_ITEM_IS_DISABLED_MESSAGE_FILED_TEMPLATE"] = "\“＃field_name＃\”";
$MESS["M_CRM_ET_SEARCH_ITEM_IS_DISABLED_MESSAGE_MANY_FIELDS"] = "保存的過濾器包括不可搜索的字段＃field_name_formatted＃。";
$MESS["M_CRM_ET_SEARCH_ITEM_IS_DISABLED_MESSAGE_ONE_FIELD"] = "保存的過濾器包括不可搜索的字段＃field_name_formatted＃。";
$MESS["M_CRM_ET_SEARCH_ITEM_IS_DISABLED_TITLE"] = "無法使用此過濾器搜索";
