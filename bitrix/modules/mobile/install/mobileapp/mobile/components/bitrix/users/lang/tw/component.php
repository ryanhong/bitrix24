<?php
$MESS["ACTION_DELETE"] = "刪除";
$MESS["INVITE_USERS"] = "邀請用戶";
$MESS["INVITE_USERS_ERROR"] = "錯誤";
$MESS["LOAD_MORE_RESULT"] = "展示更多";
$MESS["LOAD_MORE_USERS"] = "裝載更多";
$MESS["RECENT_SEARCH"] = " 最近的搜索";
$MESS["SEARCH_EMPTY_RESULT"] = "不幸的是您的搜索請求返回沒有結果";
$MESS["SEARCH_LOADING"] = "搜尋...";
$MESS["SEARCH_PLACEHOLDER"] = "輸入姓名或部門";
$MESS["USER_LOADING"] = "載入中...";
