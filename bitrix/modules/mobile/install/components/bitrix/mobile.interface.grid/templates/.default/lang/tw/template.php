<?php
$MESS["M_GRID_DOWN_TEXT"] = "釋放以刷新...";
$MESS["M_GRID_EMPTY_LIST"] = "列表為空。";
$MESS["M_GRID_EMPTY_SEARCH"] = "找不到條目。";
$MESS["M_GRID_LOAD_TEXT"] = "更新...";
$MESS["M_GRID_MORE_BUTTON"] = "更多的";
$MESS["M_GRID_PULL_TEXT"] = "下拉以刷新...";
$MESS["M_GRID_SEARCH"] = "搜尋";
