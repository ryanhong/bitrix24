<?php
$MESS["BX24_INVITE_DIALOG_CONF_PAGE_TITLE"] = "確認註冊";
$MESS["BX24_INVITE_DIALOG_EMAIL"] = "輸入要邀請的人的電子郵件地址。帶有逗號或空間的多個條目。";
$MESS["BX24_INVITE_DIALOG_INVITE"] = "邀請";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>恭喜！</b> <br>邀請已發送到指定的電子郵件。";
$MESS["BX24_INVITE_DIALOG_INVITE_LIMIT"] = "您不能邀請更多員工，因為它將超過您的許可條款。";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_1"] = "請加入我們的Bitrix24帳戶。在這個地方，每個人都可以在任務和項目上進行溝通，協作，管理客戶並做更多的事情。";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TITLE"] = "邀請文字";
$MESS["BX24_INVITE_DIALOG_INVITE_MORE"] = "邀請更多用戶";
$MESS["BX24_INVITE_DIALOG_INVITE_TITLE"] = "邀請用戶";
$MESS["DOWN_TEXT"] = "釋放以刷新...";
$MESS["LOAD_TEXT"] = "更新...";
$MESS["PULL_TEXT"] = "重新整理...";
