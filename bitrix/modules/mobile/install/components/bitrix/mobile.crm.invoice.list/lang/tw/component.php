<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
$MESS["M_CRM_INVOICE_LIST_CHANGE_STATUS"] = "更改狀態";
$MESS["M_CRM_INVOICE_LIST_COMPANY"] = "鏈接到公司";
$MESS["M_CRM_INVOICE_LIST_CONTACT"] = "鏈接到聯繫人";
$MESS["M_CRM_INVOICE_LIST_DEAL"] = "鏈接到交易";
$MESS["M_CRM_INVOICE_LIST_DELETE"] = "刪除";
$MESS["M_CRM_INVOICE_LIST_EDIT"] = "編輯";
$MESS["M_CRM_INVOICE_LIST_FILTER_CUSTOM"] = "搜索結果";
$MESS["M_CRM_INVOICE_LIST_FILTER_NONE"] = "所有發票";
$MESS["M_CRM_INVOICE_LIST_MORE"] = "更多的...";
$MESS["M_CRM_INVOICE_LIST_PRESET_MY_PAID"] = "我的付費發票";
$MESS["M_CRM_INVOICE_LIST_PRESET_MY_UNPAID"] = "我未決的發票";
$MESS["M_CRM_INVOICE_LIST_QUOTE"] = "鏈接到報價";
$MESS["M_CRM_INVOICE_LIST_SEND"] = "發送";
$MESS["M_CRM_LEAD_LIST_PRESET_USER"] = "自定義過濾器";
