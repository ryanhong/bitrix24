<?php
$MESS["M_CRM_ACTIVITY_EDIT_DOWN_TEXT"] = "釋放以刷新...";
$MESS["M_CRM_ACTIVITY_EDIT_FIELD_OWNER"] = "交易";
$MESS["M_CRM_ACTIVITY_EDIT_FIELD_OWNER_NOT_SPECIFIED"] = "[未指定]";
$MESS["M_CRM_ACTIVITY_EDIT_FIELD_RESPONSIBLE"] = "負責人";
$MESS["M_CRM_ACTIVITY_EDIT_FIELD_RESPONSIBLE_NOT_SPECIFIED"] = "[未指定]";
$MESS["M_CRM_ACTIVITY_EDIT_LOAD_TEXT"] = "更新...";
$MESS["M_CRM_ACTIVITY_EDIT_MEETING_ADD_COMM"] = "添加與會者";
$MESS["M_CRM_ACTIVITY_EDIT_MEETING_CANCEL_BTN"] = "取消";
$MESS["M_CRM_ACTIVITY_EDIT_MEETING_CREATE_BTN"] = "創造";
$MESS["M_CRM_ACTIVITY_EDIT_MEETING_DETAIL_SECTION"] = "會議細節";
$MESS["M_CRM_ACTIVITY_EDIT_MEETING_FIELD_COMM"] = "和";
$MESS["M_CRM_ACTIVITY_EDIT_MEETING_FIELD_DESCRIPTION"] = "描述";
$MESS["M_CRM_ACTIVITY_EDIT_MEETING_FIELD_LOCATION"] = "在哪裡";
$MESS["M_CRM_ACTIVITY_EDIT_MEETING_FIELD_NOTIFY"] = "設置提醒";
$MESS["M_CRM_ACTIVITY_EDIT_MEETING_FIELD_SUBJECT"] = "主題";
$MESS["M_CRM_ACTIVITY_EDIT_MEETING_FIELD_TIME"] = "日期和時間";
$MESS["M_CRM_ACTIVITY_EDIT_MEETING_UPDATE_BTN"] = "節省";
$MESS["M_CRM_ACTIVITY_EDIT_METING_TIME_NOT_SPECIFIED"] = "[沒有設置]";
$MESS["M_CRM_ACTIVITY_EDIT_NEW_MEETING"] = "新會議";
$MESS["M_CRM_ACTIVITY_EDIT_PULL_TEXT"] = "下拉以刷新...";
$MESS["M_CRM_ACTIVITY_EDIT_USER_SELECTOR_CANCEL_BTN"] = "取消";
$MESS["M_CRM_ACTIVITY_EDIT_USER_SELECTOR_OK_BTN"] = "選擇";
