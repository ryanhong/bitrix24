<?php
$MESS["CRM_ALL"] = "全部的";
$MESS["CRM_BUTTON_SELECT"] = "選擇";
$MESS["CRM_FIELD_ADDRESS"] = "地址";
$MESS["CRM_FIELD_ASSIGNED_BY_ID"] = "負責人";
$MESS["CRM_FIELD_BP_EMPTY_EVENT"] = "不要跑";
$MESS["CRM_FIELD_BP_EVENTS"] = "當前狀態";
$MESS["CRM_FIELD_BP_PARAMETERS"] = "工作流參數";
$MESS["CRM_FIELD_BP_STATE_MODIFIED"] = "當前狀態日期";
$MESS["CRM_FIELD_BP_STATE_NAME"] = "當前狀態";
$MESS["CRM_FIELD_BP_TEMPLATE_DESC"] = "描述";
$MESS["CRM_FIELD_COMMENTS"] = "評論";
$MESS["CRM_FIELD_COMPANY_ID"] = "公司";
$MESS["CRM_FIELD_COMPANY_TITLE"] = "公司名稱";
$MESS["CRM_FIELD_CREATED_BY_ID"] = "主要創造者";
$MESS["CRM_FIELD_CURRENCY_ID"] = "貨幣";
$MESS["CRM_FIELD_EMAIL"] = "電子郵件";
$MESS["CRM_FIELD_EXCH_RATE"] = "匯率";
$MESS["CRM_FIELD_FIND"] = "搜尋";
$MESS["CRM_FIELD_HONORIFIC"] = "致敬";
$MESS["CRM_FIELD_ID"] = "ID";
$MESS["CRM_FIELD_LAST_NAME"] = "姓";
$MESS["CRM_FIELD_LEAD_EVENT"] = "領先事件";
$MESS["CRM_FIELD_MESSENGER"] = "信使";
$MESS["CRM_FIELD_NAME"] = "名";
$MESS["CRM_FIELD_OPENED"] = "每個人都可以使用";
$MESS["CRM_FIELD_OPENED_TITLE"] = "任何用戶都可以查看此引線。";
$MESS["CRM_FIELD_OPPORTUNITY"] = "交易機會";
$MESS["CRM_FIELD_PHONE"] = "電話";
$MESS["CRM_FIELD_POST"] = "位置";
$MESS["CRM_FIELD_PRODUCT_ID"] = "產品";
$MESS["CRM_FIELD_PRODUCT_ROWS"] = "主要產品";
$MESS["CRM_FIELD_SECOND_NAME"] = "第二個名字";
$MESS["CRM_FIELD_SOURCE_DESCRIPTION"] = "來源信息";
$MESS["CRM_FIELD_SOURCE_ID"] = "來源";
$MESS["CRM_FIELD_STATUS_DESCRIPTION"] = "狀態信息";
$MESS["CRM_FIELD_STATUS_ID"] = "地位";
$MESS["CRM_FIELD_TITLE"] = "線索名稱";
$MESS["CRM_FIELD_WEB"] = "網站";
$MESS["CRM_HONORIFIC_NOT_SELECTED"] = "未選中的";
$MESS["CRM_LEAD_EDIT_FIELD_BIRTHDATE"] = "出生日期";
$MESS["CRM_LEAD_EDIT_NOT_FOUND"] = "找不到線索## ID＃。";
$MESS["CRM_MOBILE_MODULE_NOT_INSTALLED"] = "未安裝\“移動\”模塊。";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
