<?php
$MESS["BLOG_ALREADY_READ"] = "我讀過";
$MESS["BLOG_BLOG_BLOG_CATEGORY"] = "標籤：";
$MESS["BLOG_BLOG_BLOG_DELETE"] = "刪除";
$MESS["BLOG_BLOG_BLOG_EDIT"] = "編輯";
$MESS["BLOG_BLOG_BLOG_MORE"] = "閱讀更多...";
$MESS["BLOG_BLOG_BLOG_NO_AVAIBLE_MES"] = "沒有找到對話。";
$MESS["BLOG_COMMENTS"] = "評論";
$MESS["BLOG_C_ADD_TITLE"] = "添加評論...";
$MESS["BLOG_C_BUTTON_SEND"] = "發送";
$MESS["BLOG_DESTINATION_ALL"] = "給所有員工";
$MESS["BLOG_DESTINATION_ALL_BSM"] = "全部";
$MESS["BLOG_DESTINATION_HIDDEN_0"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_1"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_2"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_3"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_4"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_5"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_6"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_7"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_8"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_9"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_MORE"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_0"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_1"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_2"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_3"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_4"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_5"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_6"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_7"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_8"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_9"] = "和＃num＃更多收件人";
$MESS["BLOG_FILES"] = "文件：";
$MESS["BLOG_IMPORTANT_READ_LIST_0"] = "＃num＃用戶已確認閱讀此信息";
$MESS["BLOG_IMPORTANT_READ_LIST_1"] = "＃num＃用戶已確認閱讀此書";
$MESS["BLOG_IMPORTANT_READ_LIST_2"] = "＃num＃用戶已確認閱讀此信息";
$MESS["BLOG_IMPORTANT_READ_LIST_3"] = "＃num＃用戶已確認閱讀此信息";
$MESS["BLOG_IMPORTANT_READ_LIST_4"] = "＃num＃用戶已確認閱讀此信息";
$MESS["BLOG_IMPORTANT_READ_LIST_5"] = "＃num＃用戶已確認閱讀此信息";
$MESS["BLOG_IMPORTANT_READ_LIST_6"] = "＃num＃用戶已確認閱讀此信息";
$MESS["BLOG_IMPORTANT_READ_LIST_7"] = "＃num＃用戶已確認閱讀此信息";
$MESS["BLOG_IMPORTANT_READ_LIST_8"] = "＃num＃用戶已確認閱讀此信息";
$MESS["BLOG_IMPORTANT_READ_LIST_9"] = "＃num＃用戶已確認閱讀此信息";
$MESS["BLOG_LINK"] = "關聯";
$MESS["BLOG_LOG_EXPAND"] = "更多的";
$MESS["BLOG_LOG_MORE"] = "更多的";
$MESS["BLOG_MES_DELETE_POST_CONFIRM"] = "您確定要刪除對話嗎？";
$MESS["BLOG_MES_HIDE"] = "隱藏";
$MESS["BLOG_MES_HIDE_POST_CONFIRM"] = "您確定要隱藏對話嗎？";
$MESS["BLOG_MOBILE_COMMENTS"] = "評論";
$MESS["BLOG_MOBILE_COMMENTS_2"] = "評論：";
$MESS["BLOG_MOBILE_COMMENTS_ACTION"] = "評論";
$MESS["BLOG_MOBILE_DATETIME_DAYS"] = "＃天＃D";
$MESS["BLOG_MOBILE_DESTINATION"] = "到";
$MESS["BLOG_MOBILE_LIKE2_ACTION"] = "喜歡：";
$MESS["BLOG_MOBILE_LIKE2_ACTION_PATTERN"] = "＃喜歡＃：";
$MESS["BLOG_MOBILE_LIKE_ACTION"] = "喜歡";
$MESS["BLOG_MOBILE_LIKE_COUNT_USERS"] = "＃count＃＃用戶＃";
$MESS["BLOG_MOBILE_LIKE_ME"] = "你";
$MESS["BLOG_MOBILE_LIKE_OTHERS"] = "＃count_users＃喜歡它。";
$MESS["BLOG_MOBILE_LIKE_USERS_1"] = "用戶";
$MESS["BLOG_MOBILE_LIKE_USERS_2"] = "用戶";
$MESS["BLOG_MOBILE_LIKE_YOU"] = "＃你喜歡它。";
$MESS["BLOG_MOBILE_LIKE_YOU_OTHERS"] = "＃您＃和＃count_users＃喜歡它。";
$MESS["BLOG_MOBILE_TITLE_24"] = "對話";
$MESS["BLOG_MONTH_01"] = "一月";
$MESS["BLOG_MONTH_02"] = "二月";
$MESS["BLOG_MONTH_03"] = "行進";
$MESS["BLOG_MONTH_04"] = "四月";
$MESS["BLOG_MONTH_05"] = "可能";
$MESS["BLOG_MONTH_06"] = "六月";
$MESS["BLOG_MONTH_07"] = "七月";
$MESS["BLOG_MONTH_08"] = "八月";
$MESS["BLOG_MONTH_09"] = "九月";
$MESS["BLOG_MONTH_10"] = "十月";
$MESS["BLOG_MONTH_11"] = "十一月";
$MESS["BLOG_MONTH_12"] = "十二月";
$MESS["BLOG_PHOTO"] = "照片：";
$MESS["BLOG_POST"] = "對話";
$MESS["BLOG_READ_"] = "標記為已讀";
$MESS["BLOG_READ_F"] = "標記為已讀";
$MESS["BLOG_READ_M"] = "標記為已讀";
