<?php
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_DISCLAIMER_ATTENTION"] = "注意力！";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_DISCLAIMER_BUTTON_N"] = "我不同意";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_DISCLAIMER_BUTTON_Y"] = "我同意";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_DISCLAIMER_EN_TEXT"] = "
＃p＃這是由WellTory Inc提供的第三方遊戲化服務，並由其獨立的＃A_TERMS_BEGIN＃服務協議＃A_TERMS_END＃和＃A_PRIVACY_BEGIN＃隱私策略＃A_PRIVACY_END＃綁定。
＃p＃使用此應用程序可能會導致Bitrix24系統之外的個人數據傳輸。就數據傳輸到Bitrix24系統而言，Bitrix24對該數據的隱私，安全性或完整性概不負責。該服務僅用於信息和遊戲化目的，不提供醫學或心理建議，診斷或結果保證，並且不代表個人的敏感數據。
＃P＃通過安裝此應用程序，您確認BitRix24責任限制。";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_DISCLAIMER_PRIVACY_LINK"] = "https://welltory.com/privacy/";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_DISCLAIMER_RU_TEXT"] = "
＃p＃“壓力水平”遊戲化服務（以下簡稱為服務）由Welltory Inc.提供，根據＃A_TERMS_BEGIN＃協議＃A_TERMS_END＃的規定。
＃p＃bitrix24對服務績效不承擔任何責任，也不提供有關結果可以通過使用服務收到的結果的正確性，準確性或有效性的保證或索賠，且對維護和支持不承擔任何責任。
＃P＃通過接受服務條款，您同意並確認Bitrix24不收集，接受處理請求，處理和修改數據，並通過服務下載，並且不會啟動傳輸此類數據。";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_DISCLAIMER_TERMS_EN_LINK"] = "https://welltory.com/terms/";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_DISCLAIMER_TERMS_LINK"] = "https://welltory.com/terms/";
$MESS["INTRANET_STRESSLEVEL_TEMPLATE_DISCLAIMER_TERMS_RU_LINK"] = "https://welltory.com/ru/terms/";
