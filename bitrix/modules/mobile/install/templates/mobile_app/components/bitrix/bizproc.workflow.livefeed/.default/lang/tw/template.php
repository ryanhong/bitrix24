<?php
$MESS["BPATL_BEGIN"] = "開始";
$MESS["BPATL_REQUEST_ERROR"] = "執行任務時發送請求的錯誤。";
$MESS["BPATL_TASK_LINK_TITLE"] = "細節";
$MESS["BPATL_TASK_TITLE"] = "任務";
$MESS["BPATL_USER_STATUS_NO"] = "你拒絕了文件";
$MESS["BPATL_USER_STATUS_OK"] = "您閱讀了文檔";
$MESS["BPATL_USER_STATUS_YES"] = "您批准了文件";
