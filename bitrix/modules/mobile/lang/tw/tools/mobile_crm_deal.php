<?php
$MESS["CRM_DEAL_ACCESS_DENIED"] = "拒絕訪問";
$MESS["CRM_DEAL_DELETE_ERROR"] = "刪除對象時發生錯誤。";
$MESS["CRM_DEAL_ERROR_CHANGE_STATUS"] = "更改錯誤狀態";
$MESS["CRM_DEAL_ID_NOT_DEFINED"] = "沒有找到交易ID。";
$MESS["CRM_DEAL_NOT_FOUND"] = "沒有找到交易。";
