<?php
$MESS["CRM_CONTACT_ACCESS_DENIED"] = "拒絕訪問";
$MESS["CRM_CONTACT_DELETE_ERROR"] = "刪除對象時發生錯誤。";
$MESS["CRM_CONTACT_ID_NOT_DEFINED"] = "沒有找到聯繫人ID";
$MESS["CRM_CONTACT_NOT_FOUND"] = "找不到接觸。";
