<?php
$MESS["APP_DENIED"] = "拒絕訪問";
$MESS["APP_INSTALL_TITLE"] = "\“移動應用程序\”模塊安裝";
$MESS["APP_MODULE_DESCRIPTION"] = "用於門戶的移動應用程序";
$MESS["APP_MODULE_NAME"] = "移動應用程序";
$MESS["APP_UNINSTALL_TITLE"] = "\“移動應用程序\”模塊卸載";
