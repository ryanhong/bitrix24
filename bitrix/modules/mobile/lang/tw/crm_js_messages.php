<?php
$MESS["CRM_JS_BUTTON_CANCEL"] = "取消";
$MESS["CRM_JS_BUTTON_OK"] = "是的";
$MESS["CRM_JS_DELETE"] = "刪除";
$MESS["CRM_JS_DELETE_CONFIRM"] = "你確定要刪除這個項目嗎？";
$MESS["CRM_JS_DELETE_CONFIRM_TITLE"] = "刪除項目";
$MESS["CRM_JS_EDIT"] = "編輯";
$MESS["CRM_JS_ERROR"] = "錯誤";
$MESS["CRM_JS_ERROR_DELETE"] = "刪除元素時發生錯誤。";
$MESS["CRM_JS_GRID_FIELDS"] = "可見的字段";
$MESS["CRM_JS_GRID_FILTER"] = "配置過濾器";
$MESS["CRM_JS_GRID_SORT"] = "排序方式";
$MESS["CRM_JS_MORE"] = "更多的...";
