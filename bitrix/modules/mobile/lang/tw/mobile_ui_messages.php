<?php
$MESS["MUI_B24DISK_MSGVER_1"] = "駕駛";
$MESS["MUI_CAMERA_ROLL"] = "拍照";
$MESS["MUI_CANCEL"] = "取消";
$MESS["MUI_CHOOSE_FILE_TITLE"] = "文件";
$MESS["MUI_CHOOSE_PHOTO"] = "從畫廊中選擇";
$MESS["MUI_COPY"] = "複製";
$MESS["MUI_COPY_TEXT"] = "複製文字";
$MESS["MUI_PROCESSING"] = "加工...";
$MESS["MUI_TEXT_COPIED"] = "文本已復製到剪貼板";
