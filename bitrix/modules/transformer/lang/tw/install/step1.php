<?php
$MESS["TRANSFORMER_CHECK_PUBLIC_PATH"] = "指定的公共地址不正確。";
$MESS["TRANSFORMER_PUBLIC_PATH"] = "網站公共地址：";
$MESS["TRANSFORMER_PUBLIC_PATH_DESC"] = "該模塊需要正確的公共網站地址才能正常運行。";
$MESS["TRANSFORMER_PUBLIC_PATH_DESC_2"] = "如果限制了對網絡的外部訪問，請僅訪問某些頁面。請參閱＃link_start＃documentation＃link_end＃有關詳細信息。";
