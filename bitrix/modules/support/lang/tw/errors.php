<?php
$MESS["SUP_ERROR_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["SUP_ERROR_ATTACH_NOT_FOUND"] = "文件未找到。";
$MESS["SUP_ERROR_DELETE"] = "刪除錯誤。";
$MESS["SUP_ERROR_INCORRECT_DATE_CREATE"] = "不正確的創建日期。";
$MESS["SUP_ERROR_INCORRECT_DATE_CREATE_1"] = "不正確的創建日期\“來自\”。";
$MESS["SUP_ERROR_INCORRECT_DATE_CREATE_2"] = "不正確的創建日期\“ till \”。";
$MESS["SUP_ERROR_INCORRECT_DATE_MODIFY"] = "修改日期不正確。";
$MESS["SUP_ERROR_INCORRECT_DATE_MODIFY_1"] = "修改日期\“來自\”的不正確日期。";
$MESS["SUP_ERROR_INCORRECT_DATE_MODIFY_2"] = "修改的日期\“ thil \ \”不正確。";
$MESS["SUP_ERROR_INCORRECT_EMAIL"] = "不正確的電子郵件。";
$MESS["SUP_ERROR_REQUIRED_NAME"] = "\“名稱\”字段尚未填寫。";
$MESS["SUP_ERROR_REQUIRED_TIMETABLE_ID"] = "未提供時間表。";
$MESS["SUP_ERROR_SAVE"] = "錯誤更新記錄## ID＃";
$MESS["SUP_ERROR_SLA_1"] = "SLA＃1無法刪除。";
$MESS["SUP_ERROR_SLA_HAS_TICKET"] = "在SLA＃＃id＃刪除之前，您應該確保此SLA沒有門票。";
$MESS["SUP_FILTER_ERROR"] = "濾波器錯誤。";
