<?php
$MESS["SUP_ST_ERROR_NO_NEW_COUPON"] = "無法創建優惠券。";
$MESS["SUP_ST_ERROR_NO_UPDATES_ROWS"] = "沒有數據更新。";
$MESS["SUP_ST_ERROR_NO_UPDATE_DATA"] = "沒有數據要更新。";
$MESS["SUP_ST_ERR_ACTIVE"] = "\“ Active \”字段不正確。";
$MESS["SUP_ST_ERR_COUNT_TICKETS"] = "優惠券的使用計數必須至少為一個。";
$MESS["SUP_ST_ERR_DATE_INTERVAL"] = "優惠券必須至少活躍一天。";
$MESS["SUP_ST_ERR_SLA_ID"] = "\“支持級別”字段不正確。";
