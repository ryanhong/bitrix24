<?php
$MESS["SUP_CATEGORY"] = "類別";
$MESS["SUP_CRITICALITY"] = "優先事項";
$MESS["SUP_DIFFICULTY"] = "難度級別";
$MESS["SUP_ERROR_ADD_DICTONARY"] = "保存錯誤記錄";
$MESS["SUP_ERROR_UPDATE_DICTONARY"] = "錯誤更新記錄";
$MESS["SUP_FORGOT_NAME"] = "請填寫\“名稱\”字段";
$MESS["SUP_FUA"] = "經常使用的答案";
$MESS["SUP_INCORRECT_SID"] = "錯誤的mnemonic代碼（僅拉丁字母，數字和下劃線符號\“ _ \”允許）";
$MESS["SUP_MARK"] = "答案等級";
$MESS["SUP_SID_ALREADY_IN_USE"] = "對於字典類型\“＃type＃\”和site \“＃lang＃\”此mnemonic代碼已在record＃＃record_id＃中使用。";
$MESS["SUP_SOURCE"] = "來源";
$MESS["SUP_STATUS"] = "地位";
$MESS["SUP_UNKNOWN_ID"] = "記錄＃ID＃不存在。";
