<?php
$MESS["SUP_ERROR_EMPTY_MESSAGE"] = "請填寫\“消息\”字段";
$MESS["SUP_ERROR_EMPTY_TITLE"] = "請填寫\“主題\”字段";
$MESS["SUP_ERROR_INVALID_COUPON"] = "優惠券無效或已過時";
$MESS["SUP_UNKNOWN_GUEST"] = "未註冊的用戶";
$MESS["SUP_UNKNOWN_USER"] = "未知用戶";
