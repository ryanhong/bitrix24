<?php
$MESS["SUP_ERROR_GROUP_ID_EMPTY"] = "組ID為空";
$MESS["SUP_ERROR_NO_GROUP"] = "沒有這樣的群體";
$MESS["SUP_ERROR_NO_SUPPORT_USER"] = "該用戶不是TechSupport團隊的成員。";
$MESS["SUP_ERROR_NO_USER"] = "沒有這樣的用戶";
$MESS["SUP_ERROR_USERGROUP_EXISTS"] = "該用戶已經是該組的成員";
$MESS["SUP_ERROR_USER_ID_EMPTY"] = "用戶ID為空";
$MESS["SUP_ERROR_USER_NO_CLIENT"] = "只有TechSupport客戶端才能添加到該組中";
$MESS["SUP_ERROR_USER_NO_TEAM"] = "只有TechSupport團隊成員才能添加到該組中";
