<?php
$MESS["SUP_DATE_FROM"] = "活躍";
$MESS["SUP_DATE_TILL"] = "通過";
$MESS["SUP_ERROR_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["SUP_ERROR_DB_ERROR"] = "數據庫錯誤。";
$MESS["SUP_ERROR_EMPTY_DATE"] = "需要例外日期。";
$MESS["SUP_ERROR_EMPTY_NAME"] = "需要例外名稱。";
$MESS["SUP_ERROR_EMPTY_OPEN_TIME"] = "需要例外操作。";
