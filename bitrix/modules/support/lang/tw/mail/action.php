<?php
$MESS["SUPPORT_MAIL_ADD_TO_CATEGORY"] = "將新的故障票添加到該類別：";
$MESS["SUPPORT_MAIL_ADD_TO_OPENED_TICKET"] = "在已經打開的故障票中添加新消息：";
$MESS["SUPPORT_MAIL_ADD_TO_OPENED_T_ANY"] = "從任何地址（僅檢查主題）";
$MESS["SUPPORT_MAIL_ADD_TO_OPENED_T_DOMAIN"] = "來自任何麻煩票證所有者域的電子郵件地址（*@domain.com）";
$MESS["SUPPORT_MAIL_ADD_TO_OPENED_T_EMAIL"] = "僅來自麻煩票證所有者的電子郵件地址user@domain.com";
$MESS["SUPPORT_MAIL_ADD_WITH_CRITICALITY"] = "設定新的麻煩票務關鍵：";
$MESS["SUPPORT_MAIL_CONNECT_TICKET_WITH_SITE"] = "將新故障票鏈接到該網站：";
$MESS["SUPPORT_MAIL_DEF_REGISTERED"] = "通過電子郵件識別註冊用戶：";
$MESS["SUPPORT_MAIL_DEF_REGISTERED_N"] = "不，總是創建匿名麻煩門票";
$MESS["SUPPORT_MAIL_DEF_REGISTERED_Y"] = "是的，嘗試將麻煩票與用戶關聯";
$MESS["SUPPORT_MAIL_HIDDEN"] = "作為隱藏的消息（僅適用於TechSupport成員）";
$MESS["SUPPORT_MAIL_MAILBOX"] = "<郵箱>";
$MESS["SUPPORT_MAIL_SUBJECT_TEMPLATE"] = "確定故障票答案的主題模板：";
$MESS["SUPPORT_MAIL_SUBJECT_TEMPLATE_NOTES"] = "（正則表達式，第一個支架包含故障票號）";
