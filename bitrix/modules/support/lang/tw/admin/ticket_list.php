<?php
$MESS["MAIN_ADD"] = "添加";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "檢查：";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "選擇：";
$MESS["MAIN_FIND"] = "尋找";
$MESS["MAIN_FIND_TITLE"] = "輸入文本以搜索";
$MESS["SUP_ACTION"] = "行動";
$MESS["SUP_ADD"] = "添加";
$MESS["SUP_ALL"] = "（全部）";
$MESS["SUP_ASC_ORDER"] = "通過上升";
$MESS["SUP_AUTO_CLOSE"] = "自動關閉：＃天＃D。";
$MESS["SUP_AUTO_CLOSE_DAYS"] = "自動關閉";
$MESS["SUP_CATEGORY"] = "類別";
$MESS["SUP_CATEGORY_SHORT"] = "貓。";
$MESS["SUP_CLOSE"] = "關閉";
$MESS["SUP_CLOSED"] = "關閉";
$MESS["SUP_CLOSE_NOTIFY"] = "關閉並通知";
$MESS["SUP_CONF_ACTION_CLOSE"] = "您確定要關閉選定的門票嗎？";
$MESS["SUP_CONF_ACTION_DELETE"] = "您確定要刪除選定的門票嗎？";
$MESS["SUP_CONF_ACTION_MARK_AS_MAYBE_SPAM"] = "您確定要將選定的門票標記為可能的垃圾郵件嗎？";
$MESS["SUP_CONF_ACTION_MARK_AS_SPAM"] = "您確定要將選定的門票標記為垃圾郵件嗎？";
$MESS["SUP_CONF_ACTION_MARK_AS_SPAM_DELETE"] = "您確定要標記為垃圾郵件和刪除選定的門票嗎？";
$MESS["SUP_CONF_ACTION_OPEN"] = "您確定要打開選定的門票嗎？";
$MESS["SUP_CONF_ACTION_UNMARK_SPAM"] = "您確定要取消標記選定的垃圾郵件門票嗎？";
$MESS["SUP_COUPON"] = "優惠券";
$MESS["SUP_CRITICALITY"] = "優先事項";
$MESS["SUP_CRITICALITY_SHORT"] = "優先事項";
$MESS["SUP_DATE_CREATE"] = "作者";
$MESS["SUP_DAYS"] = "d。";
$MESS["SUP_DEADLINE"] = "最後期限";
$MESS["SUP_DELETE"] = "刪除";
$MESS["SUP_DELETE_TICKET"] = "刪除票";
$MESS["SUP_DELETE_TICKET_CONF"] = "您確定要刪除票嗎？";
$MESS["SUP_DESC_ORDER"] = "通過下降";
$MESS["SUP_DIFFICULTY"] = "難度級別";
$MESS["SUP_DIFFICULTY_1"] = "困難";
$MESS["SUP_EDIT"] = "調整";
$MESS["SUP_EDIT_TICKET"] = "編輯票";
$MESS["SUP_EMAIL_NOTIFY"] = "通知";
$MESS["SUP_EXACT_MATCH"] = "使用精確匹配";
$MESS["SUP_EXPIRING"] = "到期！";
$MESS["SUP_FILTER"] = "顯示 /隱藏過濾器";
$MESS["SUP_FROM_TILL_DATE_CLOSE"] = "\“ till \”的截止日期必須大於\“ filter \”中的\“ clast”日期";
$MESS["SUP_FROM_TILL_DATE_CREATE"] = "\“ till \”創建日期必須大於\“ \”創建日期的日期";
$MESS["SUP_FROM_TILL_DATE_TIMESTAMP"] = "\“ till \”修改日期必須大於“ \”修改日期的\ \“ filter \”中的\ \“”。";
$MESS["SUP_F_AUTO_CLOSE_DAYS_LEFT"] = "自動關閉前幾天";
$MESS["SUP_F_CATEGORY"] = "類別";
$MESS["SUP_F_CLIENT_GROUP"] = "TechSupport客戶群";
$MESS["SUP_F_CLOSE"] = "封閉/打開";
$MESS["SUP_F_COUPON"] = "優惠券";
$MESS["SUP_F_CREATED_BY"] = "由...製作";
$MESS["SUP_F_CRITICALITY"] = "優先事項";
$MESS["SUP_F_DATE_CREATE"] = "創建";
$MESS["SUP_F_DIFFICULTY"] = "難度級別";
$MESS["SUP_F_HOLD_ON"] = "請求擱置";
$MESS["SUP_F_ID"] = "請求ID";
$MESS["SUP_F_LAMP"] = "指標";
$MESS["SUP_F_MARK"] = "答案率";
$MESS["SUP_F_MESSAGE"] = "訊息";
$MESS["SUP_F_MESSAGES_1_2"] = "消息";
$MESS["SUP_F_MODIFIED_BY"] = "修改";
$MESS["SUP_F_OVERDUE_MESSAGES_1_2"] = "逾期消息";
$MESS["SUP_F_OWNER"] = "作者";
$MESS["SUP_F_PROBLEM_TIME_1_2"] = "問題時間（最小）";
$MESS["SUP_F_RESPONSIBLE"] = "負責任的";
$MESS["SUP_F_SITE"] = "站點";
$MESS["SUP_F_SLA"] = "支持水平";
$MESS["SUP_F_SOURCE"] = "來源";
$MESS["SUP_F_SPAM"] = "某些垃圾郵件";
$MESS["SUP_F_SPAM_MAYBE"] = "可能的垃圾郵件";
$MESS["SUP_F_STATUS"] = "地位";
$MESS["SUP_F_SUPPORTTEAM_GROUP"] = "TechSupport團隊";
$MESS["SUP_F_SUPPORT_COMMENTS"] = "評論";
$MESS["SUP_F_TICKET_TIME"] = "解決問題的持續時間（天）";
$MESS["SUP_F_TIMESTAMP"] = "修改的";
$MESS["SUP_F_TITLE"] = "主題";
$MESS["SUP_F_TITLE_MESSAGE"] = "消息主題和文字";
$MESS["SUP_GREEN"] = "綠色的";
$MESS["SUP_GREEN_ALT"] = "您上次回复票的人";
$MESS["SUP_GREEN_S"] = "綠色";
$MESS["SUP_GREEN_S_ALT"] = "Helpdesk工作人員上次回答了票";
$MESS["SUP_GREY"] = "灰色的";
$MESS["SUP_GREY_ALT"] = "票關閉";
$MESS["SUP_GUEST_ID"] = "統計模塊中的用戶ID";
$MESS["SUP_HOURS"] = "H。";
$MESS["SUP_LAMP"] = "印第安";
$MESS["SUP_LANG"] = "語言";
$MESS["SUP_LAST_MESSAGE_DATE"] = "答案日期";
$MESS["SUP_LAST_MESSAGE_DATE_EX"] = "優先事項";
$MESS["SUP_MARK"] = "評分";
$MESS["SUP_MARK_SHORT"] = "鼠。";
$MESS["SUP_MARK_SPAM"] = "標記為垃圾郵件";
$MESS["SUP_MARK_SPAM_DELETE"] = "標記為垃圾郵件並刪除";
$MESS["SUP_MAYBE_SPAM"] = "標記為可能的垃圾郵件";
$MESS["SUP_MESSAGES"] = "味精。";
$MESS["SUP_MESSAGES1_MESSAGES2"] = "\“來自\”消息字段必須小於\ \“ tht \”消息字段";
$MESS["SUP_MINUTES"] = "最小。";
$MESS["SUP_MODIFIED_BY"] = "修改";
$MESS["SUP_NO"] = "不";
$MESS["SUP_ONLINE"] = "在線的";
$MESS["SUP_ONLINE_ALT"] = "用戶查看了最後一個＃時間＃的票證";
$MESS["SUP_OPEN"] = "打開";
$MESS["SUP_OPENED"] = "打開";
$MESS["SUP_OPEN_NOTIFY"] = "打開並通知";
$MESS["SUP_OVERDUE"] = "逾期！";
$MESS["SUP_OWNER"] = "由...製作";
$MESS["SUP_PAGES"] = "門票";
$MESS["SUP_PROBLEM_TIME"] = "問題時間";
$MESS["SUP_RED"] = "紅色的";
$MESS["SUP_RED_ALT"] = "您的對手上次回復了票（您有責任）";
$MESS["SUP_REQUESTOR"] = "從";
$MESS["SUP_RESPONSIBLE"] = "負責任的";
$MESS["SUP_RESPONSIBLE_SHORT"] = "解答";
$MESS["SUP_SELECT_TICKET"] = "請選擇要應用此操作的門票。";
$MESS["SUP_SITE_ID"] = "地點";
$MESS["SUP_SITE_ID_SHORT"] = "地點";
$MESS["SUP_SLA"] = "等級";
$MESS["SUP_SLA_SHORT"] = "SLA";
$MESS["SUP_SORT_AUTO_CLOSE_DAYS_LEFT"] = "幾天到自動關閉";
$MESS["SUP_SORT_BY"] = "排序方式";
$MESS["SUP_SORT_BY_DEFAULT"] = "<默認>";
$MESS["SUP_SORT_CATEGORY"] = "類別";
$MESS["SUP_SORT_CRITICALITY"] = "優先事項";
$MESS["SUP_SORT_DATE_CLOSE"] = "截止日期";
$MESS["SUP_SORT_DATE_CREATE"] = "創建日期";
$MESS["SUP_SORT_ID"] = "ID";
$MESS["SUP_SORT_IS_NOTIFIED"] = "標誌\“到期\”";
$MESS["SUP_SORT_IS_OVERDUE"] = "flag \'uverdue \'";
$MESS["SUP_SORT_LAMP"] = "指標";
$MESS["SUP_SORT_MARK"] = "答案率";
$MESS["SUP_SORT_MESSAGES"] = "數字OD消息";
$MESS["SUP_SORT_MODIFIED_BY"] = "修改";
$MESS["SUP_SORT_ONLINE"] = "在線的";
$MESS["SUP_SORT_OWNER"] = "作者";
$MESS["SUP_SORT_RESPONSIBLE"] = "負責人";
$MESS["SUP_SORT_SITE_ID"] = "地點";
$MESS["SUP_SORT_SLA"] = "支持水平（SLA）";
$MESS["SUP_SORT_STATUS"] = "地位";
$MESS["SUP_SORT_SUPPORT_COMMENTS"] = "評論";
$MESS["SUP_SORT_TIMESTAMP"] = "修改日期";
$MESS["SUP_SORT_TITLE"] = "主題";
$MESS["SUP_SPAM"] = "垃圾郵件";
$MESS["SUP_STATUS"] = "地位";
$MESS["SUP_STATUS_SHORT"] = "英石。";
$MESS["SUP_SUPPORT_COMMENTS"] = "評論";
$MESS["SUP_TICKETS_TITLE"] = "麻煩門票";
$MESS["SUP_TICKET_STATUS"] = "票務狀態";
$MESS["SUP_TILL"] = "直到";
$MESS["SUP_TIMESTAMP"] = "修改";
$MESS["SUP_TITLE"] = "主題";
$MESS["SUP_UNMARK_SPAM"] = "未標記垃圾郵件票";
$MESS["SUP_VIEWED"] = "查看";
$MESS["SUP_WRONG_DATE_CLOSE_FROM"] = "請輸入\“關閉日期\”的正確\“ from \”日期";
$MESS["SUP_WRONG_DATE_CLOSE_TILL"] = "請輸入\“關閉日期\”的正確\“ thil \”日期";
$MESS["SUP_WRONG_DATE_CREATE_FROM"] = "請輸入\“創建日期\”的正確\“ from \”日期";
$MESS["SUP_WRONG_DATE_CREATE_TILL"] = "請輸入正確的\ \“ till \”日期\“創建日期\”";
$MESS["SUP_WRONG_DATE_TIMESTAMP_FROM"] = "請輸入\“修改日期\”的正確\“ from \”日期";
$MESS["SUP_WRONG_DATE_TIMESTAMP_TILL"] = "請輸入\“修改日期\”的正確\“ till \”日期";
$MESS["SUP_YELLOW"] = "黃色的";
$MESS["SUP_YELLOW_ALT"] = "您的對手上次回復了機票（您不負責）";
$MESS["SUP_YES"] = "是的";
