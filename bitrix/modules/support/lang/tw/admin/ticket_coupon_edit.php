<?php
$MESS["SUP_CE_COUPON"] = "優惠券";
$MESS["SUP_CE_COUPONS_LIST"] = "優惠券";
$MESS["SUP_CE_COUPON_TITLE"] = "優惠券";
$MESS["SUP_CE_ERROR"] = "錯誤";
$MESS["SUP_CE_F_ACTIVE"] = "積極的：";
$MESS["SUP_CE_F_ACTIVE_FROM"] = "活躍來自：";
$MESS["SUP_CE_F_ACTIVE_TO"] = "活躍直到：";
$MESS["SUP_CE_F_COUNT"] = "保留使用：";
$MESS["SUP_CE_F_COUPON"] = "優惠券：";
$MESS["SUP_CE_F_SLA"] = "SLA：";
$MESS["SUP_CE_TITLE_EDIT"] = "編輯優惠券％優惠券％";
$MESS["SUP_CE_TITLE_NEW"] = "創建新優惠券";
