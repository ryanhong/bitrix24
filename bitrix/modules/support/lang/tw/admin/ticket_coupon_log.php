<?php
$MESS["SUP_CL_COUPON"] = "優惠券";
$MESS["SUP_CL_COUPON_ID"] = "優惠券ID";
$MESS["SUP_CL_FIRST_NAME"] = "姓名";
$MESS["SUP_CL_FLT_COUPON"] = "優惠券";
$MESS["SUP_CL_FLT_COUPON_ID"] = "優惠券ID";
$MESS["SUP_CL_GUEST_ID"] = "來賓ID";
$MESS["SUP_CL_LAST_NAME"] = "姓";
$MESS["SUP_CL_LOGIN"] = "登入";
$MESS["SUP_CL_PAGES"] = "記錄";
$MESS["SUP_CL_SESSION_ID"] = "會話ID";
$MESS["SUP_CL_TIMESTAMP_X"] = "使用日期";
$MESS["SUP_CL_TITLE"] = "優惠券使用日誌";
$MESS["SUP_CL_USER_ID"] = "用戶身份";
