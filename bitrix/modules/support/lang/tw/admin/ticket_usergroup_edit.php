<?php
$MESS["SUP_GE_ERROR"] = "錯誤";
$MESS["SUP_UGE_ADD_MORE_USERS"] = "添加";
$MESS["SUP_UGE_GROUP"] = "團體";
$MESS["SUP_UGE_NO_GROUP"] = "找不到小組";
$MESS["SUP_UGE_TAB1"] = "用戶";
$MESS["SUP_UGE_TAB1_TITLE"] = "組中的用戶";
$MESS["SUP_UGE_TITLE_ADD"] = "將用戶添加到組";
$MESS["SUP_UGE_USER"] = "用戶";
$MESS["SUP_UGE_USER_CAN_MAIL_GROUP_MESS"] = "用戶可以接收組通知";
$MESS["SUP_UGE_USER_CAN_READ_GROUP_MESS"] = "用戶可以閱讀此組的門票";
