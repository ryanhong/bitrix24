<?php
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "檢查：";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "刪除";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "選擇：";
$MESS["SUP_ADD_NEW"] = "添加";
$MESS["SUP_ADD_NEW_ALT"] = "添加";
$MESS["SUP_DELETE_ALT"] = "刪除";
$MESS["SUP_DELETE_CONF"] = "您確定要刪除記錄嗎？";
$MESS["SUP_DESCRIPTION"] = "描述";
$MESS["SUP_FILTER_NAME"] = "姓名";
$MESS["SUP_GROUP_NAV"] = "時間表";
$MESS["SUP_NAME"] = "姓名";
$MESS["SUP_TITLE"] = "時間表";
$MESS["SUP_UPDATE_ALT"] = "編輯";
