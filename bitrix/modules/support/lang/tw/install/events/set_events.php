<?php
$MESS["SUP_SE_TICKET_CHANGE_BY_AUTHOR_FOR_AUTHOR_TEXT"] = "
＃ID＃ - 票證ID
＃語言＃＃ - 網站語言標識符鏈接到哪個故障票
＃What_change＃ - 已更改的內容列表
＃date_create＃ - 創建日期
＃Timestamp＃ - 更改日期
＃date_close＃ - 關閉日期
＃標題＃ - 門票標題
＃狀態＃-票務狀態
＃類別＃ -門票類別
＃批判性＃ - 機票優先級
＃費率＃ - 答案率
＃SLA＃ - 支持級別
＃源＃ - 初始票證來源（網絡，電子郵件，電話等）
＃spam_mark＃ - 垃圾郵件標記
＃Messages_Amount＃ - 門票中的消息數量
＃admin_edit_url＃ - 更改票證的鏈接（管理部分）
＃public_edit_url＃ - 更改票證的鏈接（公共部分）

＃所有者_email＃ - ＃holly_user_email＃和/或＃holle_sid＃＃
＃所有者_USER_ID＃ - 機票作者ID
＃所有者_USER_NAME＃ - 票證作者名稱
＃所有者_USER_LOGIN＃ - 票證作者登錄
＃所有者_USER_EMAIL＃ - 機票作者電子郵件
＃所有者_text＃ -  [＃所有者_USER_ID＃]（＃hander_user_login＃）＃所有者_USER_NAME＃
＃所有者_SID ＃-機票作者的一些標識符（例如電子郵件，電話等）

＃support_email＃ - ＃負責_user_email＃或＃support_admin_email＃
＃負責人＃NAME＃ - 負責人的全名
＃負責人＃＃ - 負責人的用戶ID
＃負責人_Email ＃-負責人的電子郵件
＃負責任_user_login＃ - 負責人的登錄
＃負責任＃＃ -  [＃負責_USER_ID＃]（＃candmy_user_login＃）＃consem_user_name＃
＃support_admin_email ＃-支持管理員的電子郵件

＃create_user_id＃ - 票務創建者ID
＃create_user_login＃ - 票務創建者登錄
＃create_user_email＃ -Ticket Creator電子郵件
＃create_user_name＃ - 票務創建者名稱
＃create_module_name＃ - 用於創建票證的模塊的標識符
＃create_text＃ -  [＃create_user_id＃]（＃create_user_login＃）＃create_user_name＃

＃modified_user_id＃ - 更改票證的人的用戶
＃modified_user_login＃ - 更改票證的人的登錄
＃modified_user_email＃ - 更改票證的人的電子郵件
＃modified_user_name＃ - 更改票證的人的全名
＃modified_module_name＃ - 用於更改票證的模塊的標識符。
＃modified_text＃ -  [＃modified_user_id＃]（＃modified_user_login＃）＃modified_user_name＃

＃message_author_user_id＃ - 消息作者ID
＃message_author_user_name＃ - 消息作者名稱
＃message_author_user_login＃ - 消息作者登錄
＃message_author_user_email＃ - 消息作者電子郵件
＃message_author_text＃ -  [＃message_author_user_id＃]（＃message_author_user_login＃）＃message_author_user_name＃
＃message_author_sid＃ - 消息作者一些標識符（電子郵件，電話等）
＃message_source＃ - 消息源
＃message_body＃ - 消息正文
＃files_links＃ - 連接文件的鏈接

＃support_comments＃ - 管理評論

";
$MESS["SUP_SE_TICKET_CHANGE_BY_AUTHOR_FOR_AUTHOR_TITLE"] = "作者更改了票證（作者）";
$MESS["SUP_SE_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR_MESSAGE"] = "更改您的請求＃＃＃ID＃at＃server_name＃。

＃what_change＃
主題：＃標題＃

來自：＃Message_source ## Message_author_sid ## Message_author_text＃

>================================================= ========================= =============＃files_links ## Message_body＃
>================================================= =========== ===================

作者 - ＃源## holl_sid ## holl_text＃＃
創建 - ＃create_text ## create_module_name＃[＃date_create＃]
更改 - ＃modified_text ## modified_module_name＃[＃timestamp＃]

負責 - ＃負責任＃＃
類別 - ＃類別＃
優先級 - ＃批判性＃
狀態 - ＃狀態＃
費率 - ＃費率＃
支持級別 - ＃SLA＃

要查看和編輯請求訪問鏈接：
http：//＃server_name ## public_edit_url＃？id =＃id＃＃

我們要求您不要忘記在關閉請求後對TechSupport的答案進行評分：
http：//＃server_name ## public_edit_url＃？id =＃id＃＃

自動生成的消息。
";
$MESS["SUP_SE_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR_SUBJECT"] = "[tid ## id＃]＃server_name＃：更改您的請求";
$MESS["SUP_SE_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR_TEXT"] = "
＃ID＃ - 票證ID
＃語言＃＃ - 網站語言標識符鏈接到哪個故障票
＃What_change＃ - 已更改的內容列表
＃date_create＃ - 創建日期
＃Timestamp＃ - 更改日期
＃date_close＃ - 關閉日期
＃標題＃ - 門票標題
＃狀態＃-票務狀態
＃類別＃ -門票類別
＃批判性＃ - 機票優先級
＃費率＃ - 答案率
＃SLA＃ - 支持級別
＃源＃ - 初始票證來源（網絡，電子郵件，電話等）
＃spam_mark＃ - 垃圾郵件標記
＃Messages_Amount＃ - 門票中的消息數量
＃admin_edit_url＃ - 更改票證的鏈接（管理部分）
＃public_edit_url＃ - 更改票證的鏈接（公共部分）

＃所有者_email＃ - ＃holly_user_email＃和/或＃holle_sid＃＃
＃所有者_USER_ID＃ - 機票作者ID
＃所有者_USER_NAME＃ - 票證作者名稱
＃所有者_USER_LOGIN＃ - 票證作者登錄
＃所有者_USER_EMAIL＃ - 機票作者電子郵件
＃所有者_text＃ -  [＃所有者_USER_ID＃]（＃hander_user_login＃）＃所有者_USER_NAME＃
＃所有者_SID ＃-機票作者的一些標識符（例如電子郵件，電話等）

＃support_email＃ - ＃負責_user_email＃或＃support_admin_email＃
＃負責人＃NAME＃ - 負責人的全名
＃負責人＃＃ - 負責人的用戶ID
＃負責人_Email ＃-負責人的電子郵件
＃負責任_user_login＃ - 負責人的登錄
＃負責任＃＃ -  [＃負責_USER_ID＃]（＃candmy_user_login＃）＃consem_user_name＃
＃support_admin_email ＃-支持管理員的電子郵件

＃create_user_id＃ - 票務創建者ID
＃create_user_login＃ - 票務創建者登錄
＃create_user_email＃ -Ticket Creator電子郵件
＃create_user_name＃ - 票務創建者名稱
＃create_module_name＃ - 用於創建票證的模塊的標識符
＃create_text＃ -  [＃create_user_id＃]（＃create_user_login＃）＃create_user_name＃

＃modified_user_id＃ - 更改票證的人的用戶
＃modified_user_login＃ - 更改票證的人的登錄
＃modified_user_email＃ - 更改票證的人的電子郵件
＃modified_user_name＃ - 更改票證的人的全名
＃modified_module_name＃ - 用於更改票證的模塊的標識符。
＃modified_text＃ -  [＃modified_user_id＃]（＃modified_user_login＃）＃modified_user_name＃

＃message_author_user_id＃ - 消息作者ID
＃message_author_user_name＃ - 消息作者名稱
＃message_author_user_login＃ - 消息作者登錄
＃message_author_user_email＃ - 消息作者電子郵件
＃message_author_text＃ -  [＃message_author_user_id＃]（＃message_author_user_login＃）＃message_author_user_name＃
＃message_author_sid＃ - 消息作者一些標識符（電子郵件，電話等）
＃message_source＃ - 消息源
＃message_body＃ - 消息正文
＃files_links＃ - 連接文件的鏈接

＃support_comments＃ - 管理評論

";
$MESS["SUP_SE_TICKET_CHANGE_BY_SUPPORT_FOR_AUTHOR_TITLE"] = "Techsupport成員更改了票證（作者）";
$MESS["SUP_SE_TICKET_CHANGE_FOR_TECHSUPPORT_MESSAGE"] = "請求＃＃id＃的更改＃at＃server_name＃。
＃spam_mark＃
＃what_change＃
主題：＃標題＃

來自：＃Message_source ## Message_author_sid ## Message_author_text＃

>＃Message_header ## Files_links ## Message_body＃
>＃Message_footer＃

作者 - ＃源## holl_sid ## holl_text＃＃
創建 - ＃create_text ## create_module_name＃[＃date_create＃]
更改 - ＃modified_text ## modified_module_name＃[＃timestamp＃]

負責 - ＃負責任＃＃
類別 - ＃類別＃
優先級 - ＃批判性＃
狀態 - ＃狀態＃
費率 - ＃費率＃
支持級別 - ＃SLA＃

>================================================= ============================ ==========＃support_comments＃
>================================================= =========== ===================

要查看和編輯請求訪問鏈接：
http：//＃server_name ## admin_edit_url＃？id =＃id＃＆lang =＃landagun_id＃＃

自動生成的消息。
";
$MESS["SUP_SE_TICKET_CHANGE_FOR_TECHSUPPORT_SUBJECT"] = "[tid ## id＃]＃server_name＃：請求更改";
$MESS["SUP_SE_TICKET_CHANGE_FOR_TECHSUPPORT_TEXT"] = "
＃ID＃ - 票證ID
＃語言＃＃ - 網站語言標識符鏈接到哪個故障票
＃What_change＃ - 已更改的內容列表
＃date_create＃ - 創建日期
＃Timestamp＃ - 更改日期
＃date_close＃ - 關閉日期
＃標題＃ - 門票標題
＃狀態＃-票務狀態
＃類別＃ -門票類別
＃批判性＃ - 機票優先級
＃費率＃ - 答案率
＃SLA＃ - 支持級別
＃源＃ - 初始票證來源（網絡，電子郵件，電話等）
＃spam_mark＃ - 垃圾郵件標記
＃Messages_Amount＃ - 門票中的消息數量
＃admin_edit_url＃ - 更改票證的鏈接（管理部分）
＃public_edit_url＃ - 更改票證的鏈接（公共部分）

＃所有者_email＃ - ＃holly_user_email＃和/或＃holle_sid＃＃
＃所有者_USER_ID＃ - 機票作者ID
＃所有者_USER_NAME＃ - 票證作者名稱
＃所有者_USER_LOGIN＃ - 票證作者登錄
＃所有者_USER_EMAIL＃ - 機票作者電子郵件
＃所有者_text＃ -  [＃所有者_USER_ID＃]（＃hander_user_login＃）＃所有者_USER_NAME＃
＃所有者_SID ＃-機票作者的一些標識符（例如電子郵件，電話等）

＃support_email＃ - ＃負責_user_email＃或＃support_admin_email＃
＃負責人＃NAME＃ - 負責人的全名
＃負責人＃＃ - 負責人的用戶ID
＃負責人_Email ＃-負責人的電子郵件
＃負責任_user_login＃ - 負責人的登錄
＃負責任＃＃ -  [＃負責_USER_ID＃]（＃candmy_user_login＃）＃consem_user_name＃
＃support_admin_email ＃-支持管理員的電子郵件

＃create_user_id＃ - 票務創建者ID
＃create_user_login＃ - 票務創建者登錄
＃create_user_email＃ -Ticket Creator電子郵件
＃create_user_name＃ - 票務創建者名稱
＃create_module_name＃ - 用於創建票證的模塊的標識符
＃create_text＃ -  [＃create_user_id＃]（＃create_user_login＃）＃create_user_name＃

＃modified_user_id＃ - 更改票證的人的用戶
＃modified_user_login＃ - 更改票證的人的登錄
＃modified_user_email＃ - 更改票證的人的電子郵件
＃modified_user_name＃ - 更改票證的人的全名
＃modified_module_name＃ - 用於更改票證的模塊的標識符。
＃modified_text＃ -  [＃modified_user_id＃]（＃modified_user_login＃）＃modified_user_name＃

＃message_author_user_id＃ - 消息作者ID
＃message_author_user_name＃ - 消息作者名稱
＃message_author_user_login＃ - 消息作者登錄
＃message_author_user_email＃ - 消息作者電子郵件
＃message_author_text＃ -  [＃message_author_user_id＃]（＃message_author_user_login＃）＃message_author_user_name＃
＃message_author_sid＃ - 消息作者一些標識符（電子郵件，電話等）
＃message_source＃ - 消息源
＃Message_header＃ -  \“ *******消息******* \”，或\“ *******隱藏消息********* \”
＃message_body＃ - 消息正文
＃message_footer＃ -  \“ ******************************************** ********************************************
＃files_links＃ - 連接文件的鏈接

＃support_comments＃ - 管理評論

";
$MESS["SUP_SE_TICKET_CHANGE_FOR_TECHSUPPORT_TITLE"] = "機票的更改（用於TechSupport）";
$MESS["SUP_SE_TICKET_GENERATE_SUPERCOUPON_TEXT"] = "＃優惠券＃優惠券
＃優惠券＃＃優惠券ID
＃日期＃-使用日期
＃USER_ID＃ - 用戶ID
＃session_id＃ - 會話ID
＃guest_id＃ - 訪客ID
";
$MESS["SUP_SE_TICKET_GENERATE_SUPERCOUPON_TITLE"] = "優惠券激活";
$MESS["SUP_SE_TICKET_NEW_FOR_AUTHOR_MESSAGE"] = "您的請求已被唯一的編號＃id＃接受。

請不要回复這個信息。這只是生成的
確認，表明TechSupport已收到您的要求
並正在努力。

有關您的要求的信息：

主題 - ＃標題＃
來自 - ＃源## holl_sid ## lonels_text＃＃
類別 - ＃類別＃
優先級 - ＃批判性＃

創建 - ＃create_text ## create_module_name＃[＃date_create＃]
負責 - ＃負責任＃＃
支持級別 - ＃SLA＃

>================================================= ========================= =============

＃files_links ## Message_body＃

>================================================= =========== ===================

要查看和編輯請求訪問鏈接：
http：//＃server_name ## public_edit_url＃？id =＃id＃＃

自動生成的消息。";
$MESS["SUP_SE_TICKET_NEW_FOR_AUTHOR_SUBJECT"] = "[tid ## id＃]＃server_name＃：您的請求已成功接受";
$MESS["SUP_SE_TICKET_NEW_FOR_AUTHOR_TEXT"] = "
＃ID＃ - 票證ID
＃語言＃＃ - 網站語言標識符鏈接到哪個故障票
＃date_create＃ - 創建日期
＃Timestamp＃ - 更改日期
＃date_close＃ - 關閉日期
＃標題＃ - 門票標題
＃類別＃ -門票類別
＃狀態＃-票務狀態
＃批判性＃ - 機票優先級
＃SLA＃ - 支持級別
＃源＃-Ticket Source（網絡，電子郵件，電話等）
＃spam_mark＃ - 垃圾郵件標記
＃message_body＃ - 消息正文
＃files_links＃ - 連接文件的鏈接
＃admin_edit_url＃ - 更改票證的鏈接（管理部分）
＃public_edit_url＃ - 更改票證的鏈接（公共部分）

＃所有者_email＃ - ＃holly_user_email＃和/或＃holle_sid＃＃
＃所有者_USER_ID＃ - 機票作者ID
＃所有者_USER_NAME＃ - 票證作者名稱
＃所有者_USER_LOGIN＃ - 票證作者登錄
＃所有者_USER_EMAIL＃ - 機票作者電子郵件
＃所有者_text＃ -  [＃所有者_USER_ID＃]（＃hander_user_login＃）＃所有者_USER_NAME＃
＃所有者_SID ＃-機票作者的一些標識符（例如電子郵件，電話等）

＃support_email＃ - ＃負責_user_email＃或＃support_admin_email＃
＃負責人＃NAME＃ - 負責人的全名
＃負責人＃＃ - 負責人的用戶ID
＃負責人_Email ＃-負責人的電子郵件
＃負責任_user_login＃ - 負責人的登錄
＃負責任＃＃ -  [＃負責_USER_ID＃]（＃candmy_user_login＃）＃consem_user_name＃
＃support_admin_email ＃-支持管理員的電子郵件

＃create_user_id＃ - 票務創建者ID
＃create_user_login＃ - 票務創建者登錄
＃create_user_email＃ -Ticket Creator電子郵件
＃create_user_name＃ - 票務創建者名稱
＃create_module_name＃ - 用於創建票證的模塊的標識符
＃create_text＃ -  [＃create_user_id＃]（＃create_user_login＃）＃create_user_name＃

＃support_comments＃ - 管理評論

";
$MESS["SUP_SE_TICKET_NEW_FOR_AUTHOR_TITLE"] = "新票（作者）";
$MESS["SUP_SE_TICKET_NEW_FOR_TECHSUPPORT_MESSAGE"] = "新請求＃＃ID＃at＃server_name＃。
＃spam_mark＃
來自：＃source ## holder_sid ## lands_text＃

主題：＃標題＃

>================================================= ========================= =============

＃files_links ## Message_body＃

>================================================= =========== ===================

負責 - ＃負責任＃＃
類別 - ＃類別＃
優先級 - ＃批判性＃
支持級別 - ＃SLA＃
創建 - ＃create_text ## create_module_name＃[＃date_create＃]

要查看和編輯請求訪問鏈接：
http：//＃server_name ## admin_edit_url＃？id =＃id＃＆lang =＃landagun_id＃＃

自動生成的消息。
";
$MESS["SUP_SE_TICKET_NEW_FOR_TECHSUPPORT_SUBJECT"] = "[tid ## id＃]＃server_name＃：新請求";
$MESS["SUP_SE_TICKET_NEW_FOR_TECHSUPPORT_TEXT"] = "
＃ID＃ - 票證ID
＃語言＃＃ - 網站語言標識符鏈接到哪個故障票
＃date_create＃ - 創建日期
＃Timestamp＃ - 更改日期
＃date_close＃ - 關閉日期
＃標題＃ - 門票標題
＃類別＃ -門票類別
＃狀態＃-票務狀態
＃批判性＃ - 機票優先級
＃源＃-Ticket Source（網絡，電子郵件，電話等）
＃SLA＃ - 支持級別
＃spam_mark＃ - 垃圾郵件標記
＃message_body＃ - 消息正文
＃files_links＃ - 連接文件的鏈接
＃admin_edit_url＃ - 更改票證的鏈接（管理部分）
＃public_edit_url＃ - 更改票證的鏈接（公共部分）

＃所有者_email＃ - ＃holly_user_email＃和/或＃holle_sid＃＃
＃所有者_USER_ID＃ - 機票作者ID
＃所有者_USER_NAME＃ - 票證作者名稱
＃所有者_USER_LOGIN＃ - 票證作者登錄
＃所有者_USER_EMAIL＃ - 機票作者電子郵件
＃所有者_text＃ -  [＃所有者_USER_ID＃]（＃hander_user_login＃）＃所有者_USER_NAME＃
＃所有者_SID ＃-機票作者的一些標識符（例如電子郵件，電話等）

＃support_email＃ - ＃負責_user_email＃或＃support_admin_email＃
＃負責人＃NAME＃ - 負責人的全名
＃負責人＃＃ - 負責人的用戶ID
＃負責人_Email ＃-負責人的電子郵件
＃負責任_user_login＃ - 負責人的登錄
＃負責任＃＃ -  [＃負責_USER_ID＃]（＃candmy_user_login＃）＃consem_user_name＃
＃support_admin_email ＃-支持管理員的電子郵件

＃create_user_id＃ - 票務創建者ID
＃create_user_login＃ - 票務創建者登錄
＃create_user_email＃ -Ticket Creator電子郵件
＃create_user_name＃ - 票務創建者名稱
＃create_module_name＃ - 用於創建票證的模塊的標識符
＃create_text＃ -  [＃create_user_id＃]（＃create_user_login＃）＃create_user_name＃

＃support_comments＃ - 管理評論

＃優惠券＃優惠券
";
$MESS["SUP_SE_TICKET_NEW_FOR_TECHSUPPORT_TITLE"] = "新票（用於TechSupport）";
$MESS["SUP_SE_TICKET_OVERDUE_REMINDER_MESSAGE"] = "提醒＃＃＃id＃at＃server_name＃＃id＃的答案必要性。

到期日期 - ＃expiration_date＃（剩下：＃retained_time＃）

>================================================= ========================================== ======== ==

主題 - ＃標題＃

作者 - ＃源## holl_sid ## holl_text＃＃
創建 - ＃create_text ## create_module_name＃[＃date_create＃]

支持級別 - ＃SLA＃

負責 - ＃負責任＃＃
類別 - ＃類別＃
優先級 - ＃批判性＃
狀態 - ＃狀態＃
答案率 - ＃費率＃

> =================訊息需要答案============================= =================
＃郵件正文＃
>================================================= =========== ===================

要查看和編輯請求訪問鏈接：
http：//＃server_name ## admin_edit_url＃？id =＃id＃＆lang =＃landagun_id＃＃

自動生成的消息。
";
$MESS["SUP_SE_TICKET_OVERDUE_REMINDER_SUBJECT"] = "[TID ## ID＃]＃server_name＃：提醒答案必要性";
$MESS["SUP_SE_TICKET_OVERDUE_REMINDER_TEXT"] = "
＃ID＃ - 票證ID
＃語言＃＃ - 網站語言標識符鏈接到哪個故障票
＃date_create＃ - 創建日期
＃標題＃ - 門票標題
＃狀態＃-票務狀態
＃類別＃ -門票類別
＃批判性＃ - 機票優先級
＃費率＃ - 答案率
＃SLA＃ - 支持級別
＃源＃ - 初始票證來源（網絡，電子郵件，電話等）
＃admin_edit_url＃ - 更改票證的鏈接（管理部分）

＃expiration_date＃ - 響應日期到期日期
＃剩下的_time＃ - 響應到期日期剩下多少

＃所有者_email＃ - ＃holly_user_email＃和/或＃holle_sid＃＃
＃所有者_USER_ID＃ - 機票作者ID
＃所有者_USER_NAME＃ - 票證作者名稱
＃所有者_USER_LOGIN＃ - 票證作者登錄
＃所有者_USER_EMAIL＃ - 機票作者電子郵件
＃所有者_text＃ -  [＃所有者_USER_ID＃]（＃hander_user_login＃）＃所有者_USER_NAME＃
＃所有者_SID ＃-機票作者的一些標識符（例如電子郵件，電話等）

＃support_email＃ - ＃負責_user_email＃或＃support_admin_email＃
＃負責人＃NAME＃ - 負責人的全名
＃負責人＃＃ - 負責人的用戶ID
＃負責人_Email ＃-負責人的電子郵件
＃負責任_user_login＃ - 負責人的登錄
＃負責任＃＃ -  [＃負責_USER_ID＃]（＃candmy_user_login＃）＃consem_user_name＃
＃support_admin_email ＃-支持管理員的電子郵件

＃create_user_id＃ - 票務創建者ID
＃create_user_login＃ - 票務創建者登錄
＃create_user_email＃ -Ticket Creator電子郵件
＃create_user_name＃ - 票務創建者名稱
＃create_module_name＃ - 用於創建票證的模塊的標識符
＃create_text＃ -  [＃create_user_id＃]（＃create_user_login＃）＃create_user_name＃

＃message_body＃ - 消息正文需要答案
";
$MESS["SUP_SE_TICKET_OVERDUE_REMINDER_TITLE"] = "提醒答案的必要性（對於TechSupport）";
