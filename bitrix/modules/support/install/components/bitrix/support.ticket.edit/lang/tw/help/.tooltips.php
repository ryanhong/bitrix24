<?php
$MESS["ID_TIP"] = "評估機票ID的表達式。默認值（<b> = {\ $ _請求[\“ id \”]} </b>）打開新的票務創建表單。";
$MESS["MESSAGES_PER_PAGE_TIP"] = "指定每個頁面的消息數。";
$MESS["SET_PAGE_TITLE_TIP"] = "如果\“是\”，則該組件將將頁面標題設置為<b> <b> new Ticket </b>或<b> Ticket＃<i> number </i> </b>。";
$MESS["TICKET_LIST_URL_TIP"] = "門票頁面。";
