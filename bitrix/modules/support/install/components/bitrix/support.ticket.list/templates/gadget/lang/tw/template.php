<?php
$MESS["G_TICKETS_GREEN_ALT"] = "最後一條消息是你的";
$MESS["G_TICKETS_GREEN_ALT_SUP"] = "最後一條消息是你的";
$MESS["G_TICKETS_GREEN_S_ALT_SUP"] = "上次由TechSupport員工發布";
$MESS["G_TICKETS_GREY_ALT"] = "門票關閉";
$MESS["G_TICKETS_GREY_ALT_SUP"] = "門票關閉";
$MESS["G_TICKETS_LIST_EMPTY"] = "沒有支持門票";
$MESS["G_TICKETS_MESSAGES"] = "總郵件";
$MESS["G_TICKETS_MODIFIED_BY"] = "最後一條消息的作者";
$MESS["G_TICKETS_RED_ALT"] = "上次由TechSupport員工發布";
$MESS["G_TICKETS_RED_ALT_SUP"] = "上次由TechSupport客戶端發布（您負責）";
$MESS["G_TICKETS_RESPONSIBLE"] = "負責任的";
$MESS["G_TICKETS_STATUS"] = "地位";
$MESS["G_TICKETS_TIMESTAMP_X"] = "上一次更改";
$MESS["G_TICKETS_YELLOW_ALT_SUP"] = "上次由TechSupport客戶端發布（您不負責）";
