<?php
$MESS["SUP_ALL"] = "（全部）";
$MESS["SUP_ASK"] = "創建新票";
$MESS["SUP_CLOSED"] = "關閉";
$MESS["SUP_EDIT"] = "調整";
$MESS["SUP_EDIT_TICKET"] = "編輯票";
$MESS["SUP_EXACT_MATCH"] = "使用精確匹配";
$MESS["SUP_F_CLOSE"] = "封閉/打開";
$MESS["SUP_F_DEL_FILTER"] = "卸下過濾器";
$MESS["SUP_F_FILTER"] = "篩選";
$MESS["SUP_F_ID"] = "查詢ID";
$MESS["SUP_F_LAMP"] = "指標";
$MESS["SUP_F_MESSAGE"] = "訊息";
$MESS["SUP_F_SET_FILTER"] = "設置過濾器";
$MESS["SUP_GREEN"] = "綠色的";
$MESS["SUP_GREEN_ALT"] = "是你上次寫信給票";
$MESS["SUP_GREEN_ALT_SUP"] = "最後一條消息是你的";
$MESS["SUP_GREEN_S_ALT_SUP"] = "上次由TechSupport員工發布";
$MESS["SUP_GREY"] = "灰色的";
$MESS["SUP_GREY_ALT"] = "票關閉";
$MESS["SUP_GREY_ALT_SUP"] = "門票關閉";
$MESS["SUP_ID"] = "ID";
$MESS["SUP_LAMP"] = "印第安";
$MESS["SUP_MESSAGES"] = "味精。";
$MESS["SUP_MODIFIED_BY"] = "修改";
$MESS["SUP_OPENED"] = "打開";
$MESS["SUP_RED"] = "紅色的";
$MESS["SUP_RED_ALT"] = "上次由TechSupport員工發布";
$MESS["SUP_RED_ALT_2"] = "上次由TechSupport員工發布";
$MESS["SUP_RED_ALT_SUP"] = "上次由TechSupport客戶端發布（您負責）";
$MESS["SUP_STATUS"] = "地位";
$MESS["SUP_TIMESTAMP"] = "修改的";
$MESS["SUP_TITLE"] = "主題";
$MESS["SUP_TOTAL"] = "全部的";
$MESS["SUP_YELLOW_ALT_SUP"] = "上次由TechSupport客戶端發布（您不負責）";
