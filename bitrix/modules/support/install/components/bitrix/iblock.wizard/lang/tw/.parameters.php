<?php
$MESS["WZ_BACKURL"] = "嚮導的返回頁面（用於後按鈕，可以為空）";
$MESS["WZ_IBLOCK"] = "信息塊";
$MESS["WZ_INCLUDE_INTO_CHAIN"] = "將巫師步驟添加到麵包屑導航";
$MESS["WZ_PROPERTY"] = "帶有問題類型的屬性";
$MESS["WZ_PROPERTY_VALUES"] = "包含下拉列表值的多個屬性";
$MESS["WZ_TICKET_EDIT"] = "帶有請求編輯表格的頁面";
$MESS["WZ_TYPE"] = "信息塊類型";
