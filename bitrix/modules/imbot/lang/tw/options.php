<?php
$MESS["IMBOT_ACCOUNT_DEBUG"] = "調試模式";
$MESS["IMBOT_ACCOUNT_ERROR_PUBLIC"] = "未指定正確的公共地址";
$MESS["IMBOT_ACCOUNT_ERROR_PUBLIC_CHECK"] = "錯誤檢查公共地址：＃錯誤＃";
$MESS["IMBOT_ACCOUNT_URL"] = "網站公共地址";
$MESS["IMBOT_BOT_NOTICE"] = "注意：禁用聊天機器人將使其在聊天中不可用並刪除所有關聯的消息日誌。";
$MESS["IMBOT_BOT_POSTFIX_UA"] = "烏克蘭";
$MESS["IMBOT_ENABLE_GIPHY_BOT"] = "啟用giphy";
$MESS["IMBOT_ENABLE_SALE_SUPPORT_BOT"] = "啟用Bitrix24銷售支持渠道";
$MESS["IMBOT_ENABLE_SUPPORT_BOT"] = "啟用BitRix24支持頻道";
$MESS["IMBOT_HEADER_BOTS"] = "聊天機器人";
$MESS["IMBOT_SUPPORT_BOX_ACTIVATION_ERROR"] = "連接打開通道的錯誤：＃錯誤＃";
$MESS["IMBOT_SUPPORT_BOX_ACTIVATION_ERROR_UNKNOWN"] = "連接打開通道的錯誤。";
$MESS["IMBOT_TAB_SETTINGS"] = "設定";
$MESS["IMBOT_TAB_TITLE_SETTINGS_2"] = "連接參數";
$MESS["IMBOT_WAIT_RESPONSE"] = "允許更長的等待時間";
$MESS["IMBOT_WAIT_RESPONSE_DESC"] = "如果由於環境限制，您沒有從聊天機器人服務器中獲得響應，請啟用此選項。";
