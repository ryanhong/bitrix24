<?php
$MESS["CHAT_QUESTION_DESC"] = "在這裡向我們的幫助台代理商提交一個新問題。";
$MESS["CHAT_QUESTION_DESC_FROM_OPERATOR"] = "你好！我們將在此聊天中提供進一步的幫助。";
$MESS["CHAT_QUESTION_DISALLOWED"] = "您無法就當前計劃向HelpDesk發布額外的問題。您可以隨時將問題提交給一般的HelpDesk聊天。";
$MESS["CHAT_QUESTION_GREETING"] = "使用此聊天來問我們的HelpDesk代理更多問題。[BR]您可以將此聊天重命名為更有意義的東西，以幫助您更快地找到它。";
$MESS["CHAT_QUESTION_LIMITED"] = "您已經超過了＃編號＃問題的限制。請等待回复或提取您提交新問題的問題之一。";
$MESS["CHAT_QUESTION_RESUME"] = "繼續";
$MESS["CHAT_QUESTION_TITLE"] = "HelpDesk請求 - ＃號碼＃";
$MESS["SUPPORT_QUESTION_DESCRIPTION"] = "在這裡向我們的幫助台代理商提交一個新問題。";
$MESS["SUPPORT_QUESTION_TITLE"] = "新問題";
