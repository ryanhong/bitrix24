<?php
$MESS["IMBOT_COPILOT_AGREEMENT_RESTRICTION"] = "請接受許可協議開始使用Copilot。";
$MESS["IMBOT_COPILOT_BOT_NAME"] = "副駕駛";
$MESS["IMBOT_COPILOT_ERROR_NETWORK_MSGVER_1"] = "要求向AI提供商定時出現。請稍後再試。";
$MESS["IMBOT_COPILOT_JOB_FAIL_MSGVER_1"] = "要求向AI提供商定時出現。請稍後再試。";
$MESS["IMBOT_COPILOT_RESTRICTION"] = "不能邀請副吉洛特參加聊天。";
$MESS["IMBOT_COPILOT_TARIFF_RESTRICTION"] = "此功能在您的計劃中不可用。請升級到包含Copilot功能的計劃。";
$MESS["IMBOT_COPILOT_UNAVAILABLE"] = "需要\“ AI Integration \”模塊。請聯繫您的BitRix24管理員。";
