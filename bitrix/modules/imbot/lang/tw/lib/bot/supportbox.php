<?php
$MESS["SUPPORT_BOX_ACTIVATE"] = "啟用";
$MESS["SUPPORT_BOX_ACTIVATION_ERROR"] = "錯誤連接支持打開通道：[b] #Error＃[/b]
[url =＃help_desk＃]連接設置指令[/url]";
$MESS["SUPPORT_BOX_ACTIVATION_SUCCESS"] = "支持Open Channel已成功連接到您的Bitrix24。
在此聊天中發布有關Bitrix24本地版的任何問題，我們在這里為您提供幫助。";
$MESS["SUPPORT_BOX_CHANGE_LINE"] = "由於支持水平的變化，當前的對話已經關閉";
$MESS["SUPPORT_BOX_CHANGE_LINE_USER"] = "您好＃user_name＃！
您的支持級別已改變。重要的是：如果您在更改之前提交請求，請在此聊天中再次發布請求，以便我們看到它。";
$MESS["SUPPORT_BOX_CLOSE_LINE"] = "當前的對話已經結束，因為支持開放渠道已斷開連接。";
$MESS["SUPPORT_BOX_NAME"] = "支持Bitrix24本地版本";
$MESS["SUPPORT_BOX_POSITION"] = "開放通道";
$MESS["SUPPORT_BOX_REFRESH_ERROR"] = "錯誤自動化支持開放通道參數：[b] #Error＃[/b]
[url = https：//helpdesk.bitrix24.com/open/12448068]連接設置指令[/url]";
$MESS["SUPPORT_BOX_START_DIALOG_F"] = "對話由＃user_full_name＃開始";
$MESS["SUPPORT_BOX_START_DIALOG_M"] = "對話由＃user_full_name＃開始";
$MESS["SUPPORT_BOX_WELCOME_MESSAGE"] = "您好＃user_full_name＃！
請激活開放頻道以獲得支持。";
$MESS["TELEMETRY_ALL_FAIL_PLURAL_0"] = "在＃日期＃上執行的最後一次系統檢查找到了＃fails_count＃關鍵問題。";
$MESS["TELEMETRY_ALL_FAIL_PLURAL_1"] = "在＃日期＃上執行的最後一個系統檢查找到了＃fails_count＃關鍵問題。";
$MESS["TELEMETRY_ALL_FAIL_PLURAL_2"] = "在＃日期＃上執行的最後一個系統檢查找到了＃fails_count＃關鍵問題。";
$MESS["TELEMETRY_ALL_OK"] = "在＃日期＃上執行的最後一次系統檢查沒有發現任何問題。";
$MESS["TELEMETRY_NO_CHECK_NEVER_BEEN_DONE"] = "從未執行系統檢查。";
