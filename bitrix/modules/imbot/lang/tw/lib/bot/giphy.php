<?php
$MESS["IMBOT_GIPHY_BOT_COLOR"] = "水";
$MESS["IMBOT_GIPHY_BOT_GENDER"] = "m";
$MESS["IMBOT_GIPHY_BOT_NAME"] = "giphy";
$MESS["IMBOT_GIPHY_BOT_WORK_POSITION"] = "該機器人將幫助您找到與請求匹配的圖像。";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_PARAMS"] = "圖像主題";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_RETRY"] = "更多的！";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_TITLE"] = "發布與查詢匹配的圖像";
$MESS["IMBOT_GIPHY_FOUND_ALTER_MESSAGE"] = "我找不到您要尋找的東西:( [br]但是，我發現了類似的東西：";
$MESS["IMBOT_GIPHY_ICON_BROWSE_DESCRIPTION"] = "瀏覽並選擇相關的GIF圖像";
$MESS["IMBOT_GIPHY_ICON_BROWSE_TITLE"] = "瀏覽GIF圖像";
$MESS["IMBOT_GIPHY_ICON_COPYRIGHT"] = "giphy.com";
$MESS["IMBOT_GIPHY_NOT_FOUND_MESSAGE"] = "什麼都沒發現:(";
$MESS["IMBOT_GIPHY_WELCOME_MESSAGE"] = "我可以幫助您找到與您的請求相關的圖像！ [br]輸入某個東西，我會為您選擇一個！ :)";
$MESS["IMBOT_GIPHY_WELCOME_MESSAGE_CHAT"] = "我可以幫助您找到與您的請求相關的圖像！ [BR]在消息中提及我，或單擊我的頭像讓我開始:)";
