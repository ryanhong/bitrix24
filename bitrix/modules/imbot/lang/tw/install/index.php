<?php
$MESS["IMBOT_CHECK_IM"] = "未安裝\“ Instant Messenger \”模塊。";
$MESS["IMBOT_CHECK_IM_VERSION"] = "請將\“ Instant Messenger \”模塊更新為版本16.1.0";
$MESS["IMBOT_CHECK_PUBLIC_PATH"] = "未指定正確的公共地址。";
$MESS["IMBOT_CHECK_PULL"] = "未安裝\“ push and pull \”模塊，或者未配置隊列服務器。";
$MESS["IMBOT_INSTALL_TITLE"] = "\“ BitRix24 Chat Bots \”模塊安裝";
$MESS["IMBOT_MODULE_DESCRIPTION"] = "與Bitrix24一起使用的聊天機器人模塊。";
$MESS["IMBOT_MODULE_NAME"] = "Bitrix24聊天機器人";
$MESS["IMBOT_UNINSTALL_HAS_BOTS"] = "在卸載模塊之前，必須卸載所有註冊的聊天機器人。";
$MESS["IMBOT_UNINSTALL_QUESTION"] = "您確定要刪除模塊嗎？<br>這將禁用所有聊天機器人並刪除其消息歷史記錄。";
$MESS["IMBOT_UNINSTALL_TITLE"] = "\“ bitrix24聊天bots \”模塊卸載";
$MESS["IMBOT_USERS_LINK"] = "用戶列表";
