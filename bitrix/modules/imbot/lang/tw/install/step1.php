<?php
$MESS["IMBOT_PUBLIC_PATH"] = "網站公共地址：";
$MESS["IMBOT_PUBLIC_PATH_DESC"] = "BITRIX24聊天機器人需要一個網站公共地址。";
$MESS["IMBOT_PUBLIC_PATH_DESC_2"] = "如果限制了對網絡的外部訪問，請僅訪問某些頁面。請參閱＃link_start＃documentation＃link_end＃有關詳細信息。";
