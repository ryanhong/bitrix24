<?php
$MESS["MAIN_RESTORE_DEFAULTS"] = "預設";
$MESS["TRANS_BACKUP_FILES"] = "保存之前備份影響文件";
$MESS["TRANS_BACKUP_FOLDER"] = "修改文件的備份文件夾";
$MESS["TRANS_DONT_SORT_LANGUAGES"] = "不要為語言排序";
$MESS["TRANS_EXPORT_CSV_DELIMITER"] = "導出的CSV場分離器";
$MESS["TRANS_EXPORT_CSV_DELIMITER_COMMA"] = "逗號";
$MESS["TRANS_EXPORT_CSV_DELIMITER_SEMICOLON"] = "分號";
$MESS["TRANS_EXPORT_CSV_DELIMITER_TABULATION"] = "標籤";
$MESS["TRANS_EXPORT_FOLDER"] = "將導出的文件保存到文件夾（例如\ \ \ \“上傳/translate/eartert/\”）";
$MESS["TRANS_RESTRICTED_FOLDERS"] = "限制定位位置（文件夾路徑，分離逗號）";
$MESS["TRANS_SHOW_BUTTON_LANG_FILES"] = "在工具欄上顯示\“ Translation \”按鈕";
$MESS["TRANS_SORT_PHRASES"] = "保存本地化文件時，按ID對消息進行排序";
