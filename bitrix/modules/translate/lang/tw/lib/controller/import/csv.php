<?php
$MESS["TR_IMPORT_ACTION_CANCEL"] = "導入已被取消。";
$MESS["TR_IMPORT_ACTION_STATS"] = "導入的消息：＃processed_phrases＃out＃total_phrases＃out。";
$MESS["TR_IMPORT_COMPLETED"] = "導入文件已加載。";
$MESS["TR_IMPORT_EMPTY_FILE_ERROR"] = "未指定導入文件";
$MESS["TR_IMPORT_FILE_DROPPED"] = "導入文件已刪除";
$MESS["TR_IMPORT_UPLOAD_OK"] = "該文件已成功導入。";
$MESS["TR_IMPORT_VOID"] = "無數據導入";
$MESS["TR_INDEX_ACTION_STATS"] = "文件索引：＃processed_files＃of＃total_files＃。";
