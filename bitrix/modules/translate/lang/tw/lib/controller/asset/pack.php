<?php
$MESS["TR_ERROR_ARCHIVE"] = "存檔錯誤";
$MESS["TR_ERROR_CREATE_TEMP_FOLDER"] = "無法創建臨時目錄\“＃路徑＃\”";
$MESS["TR_ERROR_SOURCE_FOLDER"] = "目錄\“＃路徑＃\”要添加到存檔中或拒絕訪問。";
$MESS["TR_LANGUAGE_COLLECTED_ARCHIVE"] = "本地化\“＃lang＃\”已被編譯到存檔文件\“＃file_path＃\”。 <a href='#link#'>下載</a>";
$MESS["TR_PACK_ACTION_EXPORT"] = "文件導出：＃total_files＃。存檔文件大小：＃file_size_format＃。";
