<?php
$MESS["TR_ERROR_CREATE_TEMP_FOLDER"] = "無法創建臨時目錄。";
$MESS["TR_ERROR_ENCODING"] = "編碼無效或未指定。";
$MESS["TR_ERROR_LANGUAGE_CHARSET_NON_UTF"] = "選定的語言編碼不是UTF-8。請禁用編碼轉換的民族角色。";
$MESS["TR_ERROR_LANGUAGE_DATE"] = "需要本地化日期。";
$MESS["TR_ERROR_LANGUAGE_ID"] = "找不到選定的語言";
$MESS["TR_ERROR_OPEN_FILE"] = "無法打開文件\“＃文件＃\”。";
$MESS["TR_ERROR_SELECT_LANGUAGE"] = "需要本地化語言。";
$MESS["TR_LANGUAGE_COLLECTED_FOLDER"] = "本地化\“＃lang＃\”已被編譯為文件夾\“＃路徑＃\”。";
