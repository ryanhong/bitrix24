<?php
$MESS["TR_ERROR_DELETE_TEMP_FOLDER"] = "無法刪除臨時目錄\“＃路徑＃\”";
$MESS["TR_ERROR_TARFILE"] = "未指定本地化文件。";
$MESS["TR_ERROR_TARFILE_EXTENTION"] = "文件類型無效";
$MESS["TR_ERROR_UPLOAD_SIZE"] = "上傳的文件大小超過＃size＃的限制";
$MESS["TR_EXPORT_ACTION_CANCELED"] = "本地化導出已被取消。";
$MESS["TR_EXPORT_FILE_DROPPED"] = "導出文件已刪除";
$MESS["TR_IMPORT_ACTION_CANCELED"] = "本地化導入已被取消。";
$MESS["TR_IMPORT_EMPTY_FILE_ERROR"] = "未指定導入文件";
$MESS["TR_IMPORT_UPLOAD_OK"] = "文件上傳成功";
