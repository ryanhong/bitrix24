<?php
$MESS["TR_EXPORT_ACTION_CANCEL"] = "出口已取消。";
$MESS["TR_EXPORT_ACTION_EXPORT"] = "導出消息：＃total_phrases＃。文件大小：＃file_size_format＃。";
$MESS["TR_EXPORT_COMPLETED"] = "已經創建了導出文件。";
$MESS["TR_EXPORT_DOWNLOAD"] = "<a href='#file_link#'download='#file_name#'>下載</a>";
$MESS["TR_EXPORT_FILE_DROPPED"] = "導出文件已刪除";
$MESS["TR_EXPORT_SAMPLES"] = "<br>找到了現有的翻譯：＃total_samples＃。文件大小：＃file_size_format＃。";
$MESS["TR_EXPORT_SAMPLES_NOT_FOUND"] = "<br>尚未發現現有的翻譯。";
$MESS["TR_EXPORT_VOID"] = "無數據導出";
