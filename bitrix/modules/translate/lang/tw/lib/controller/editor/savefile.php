<?php
$MESS["TR_EDIT_ERROR_FILE_NOT_LANG"] = "文件\“＃文件＃\”不是語言文件";
$MESS["TR_EDIT_FILE_PATH_ERROR"] = "未指定本地化文件";
$MESS["TR_EDIT_FILE_WRONG_NAME"] = "文件名不正確";
$MESS["TR_EDIT_PARAM_ERROR"] = "數據不足以完成操作";
$MESS["TR_EDIT_SAVING_COMPLETED"] = "保存";
