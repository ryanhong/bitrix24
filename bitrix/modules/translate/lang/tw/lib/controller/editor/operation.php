<?php
$MESS["TR_CREATE_BACKUP_ERROR"] = "無法為文件\“＃文件＃\”創建備份";
$MESS["TR_EDIT_SAVING_COMPLETED"] = "保存";
$MESS["TR_ERROR_DELETE"] = "無法刪除文件\“＃文件＃\”";
$MESS["TR_ERROR_WRITE_CREATE"] = "無法創建文件\“＃文件＃\”";
$MESS["TR_ERROR_WRITE_UPDATE"] = "無法寫入\ \“＃文件＃\”";
