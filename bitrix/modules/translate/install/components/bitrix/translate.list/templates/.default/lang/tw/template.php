<?php
$MESS["TRANS_PATH"] = "小路：";
$MESS["TRANS_PATH_SEARCH"] = "路徑 +搜索";
$MESS["TR_ADD_UPDATE_LANG_FILES"] = "添加新的翻譯，更新消息在CSV文件中找到";
$MESS["TR_CLEAR_ACTION_CLEARING"] = "步驟#num＃/＃len＃：刪除消息";
$MESS["TR_CLEAR_ACTION_WIPE_EMPTY"] = "步驟＃num＃out＃len＃：刪除空語言文件夾";
$MESS["TR_CLEAR_DLG_CANCELED"] = "操作被取消，不會刪除任何消息。";
$MESS["TR_CLEAR_DLG_COMPLETED"] = "消息已刪除。";
$MESS["TR_CLEAR_DLG_SUMMARY"] = "注意力！當前語言中不存在的消息將被刪除。";
$MESS["TR_CLEAR_DLG_TITLE"] = "刪除冗餘消息";
$MESS["TR_CLEAR_PROGRESS"] = "處理路徑";
$MESS["TR_CONVERT_ENCODING"] = "選擇文件編碼";
$MESS["TR_CONVERT_FROM_UTF8"] = "該文件是一個UTF-8文件";
$MESS["TR_DLG_BTN_CLOSE"] = "關閉";
$MESS["TR_DLG_BTN_START"] = "跑步";
$MESS["TR_DLG_BTN_STOP"] = "停止";
$MESS["TR_DLG_REQUEST_CANCEL"] = "取消...";
$MESS["TR_DLG_REQUEST_ERR"] = "錯誤處理請求。";
$MESS["TR_EXPORT_CSV_DLG_BTN_START"] = "出口";
$MESS["TR_EXPORT_CSV_DLG_CANCELED"] = "出口已取消。";
$MESS["TR_EXPORT_CSV_DLG_COMPLETED"] = "消息已導出。";
$MESS["TR_EXPORT_CSV_DLG_SUMMARY"] = "這將導出本地化消息到CSV文件。此操作可能需要一些時間。";
$MESS["TR_EXPORT_CSV_DLG_TITLE"] = "導出消息到CSV";
$MESS["TR_EXPORT_CSV_PARAM_APPEND_SAMPLES"] = "嘗試在翻譯數據庫中查找現有消息";
$MESS["TR_EXPORT_CSV_PARAM_CONVERT_UTF8"] = "轉換為UTF-8";
$MESS["TR_EXPORT_CSV_PARAM_FILE_LIST"] = "僅針對文件和文件夾導出消息";
$MESS["TR_EXPORT_CSV_PARAM_LANGUAGES"] = "選擇輸出語言";
$MESS["TR_EXPORT_CSV_PARAM_LANGUAGES_ALL"] = "全部";
$MESS["TR_EXPORT_CSV_PARAM_SAMPLES_COUNT"] = "每條消息的最大翻譯";
$MESS["TR_EXPORT_CSV_PARAM_SAMPLES_RESTRICTION"] = "僅在選定文件夾中搜索現有翻譯";
$MESS["TR_EXPORT_CSV_PARAM_UNTRANSLATED"] = "僅導出未翻譯消息";
$MESS["TR_EXPORT_CSV_PROGRESS"] = "處理的文件夾";
$MESS["TR_EXPORT_DLG_CLEAR"] = "刪除導出文件";
$MESS["TR_EXPORT_DLG_CLEAR_MULTI"] = "刪除導出的文件";
$MESS["TR_EXPORT_DLG_DOWNLOAD"] = "下載導出的文件";
$MESS["TR_EXPORT_DLG_DOWNLOAD_MULTI"] = "下載導出的文件";
$MESS["TR_IMPORT_ACTION_UPLOAD"] = "步驟#num＃/＃len＃：將文件上傳到服務器";
$MESS["TR_IMPORT_ACTION_UPLOAD_PROGRESS"] = "上傳";
$MESS["TR_IMPORT_CSV_DLG_BTN_MORE"] = "導入更多";
$MESS["TR_IMPORT_CSV_DLG_BTN_START"] = "進口";
$MESS["TR_IMPORT_CSV_DLG_CANCELED"] = "導入已被取消。";
$MESS["TR_IMPORT_CSV_DLG_COMPLETED"] = "消息已導入。";
$MESS["TR_IMPORT_CSV_DLG_SUMMARY"] = "這將從CSV文件導入本地化消息。此操作可能需要一些時間。";
$MESS["TR_IMPORT_CSV_DLG_TITLE"] = "步驟#num＃/＃len＃：從CSV導入消息";
$MESS["TR_IMPORT_CSV_PROGRESS"] = "處理行";
$MESS["TR_IMPORT_DLG_TITLE"] = "從CSV導入消息";
$MESS["TR_IMPORT_UPDATE_METHOD"] = "消息導入模式：";
$MESS["TR_INDEX_ACTION_COLLECT_FILE"] = "步驟#num＃/＃len＃：搜索本地化文件";
$MESS["TR_INDEX_ACTION_COLLECT_FILE_PROGRESS"] = "處理的文件夾";
$MESS["TR_INDEX_ACTION_COLLECT_LANG_PATH"] = "步驟#num＃/＃len＃：搜索語言文件夾";
$MESS["TR_INDEX_ACTION_COLLECT_LANG_PATH_PROGRESS"] = "找到文件夾";
$MESS["TR_INDEX_ACTION_COLLECT_PATH"] = "步驟#num＃/＃len＃：索引路徑樹";
$MESS["TR_INDEX_ACTION_COLLECT_PATH_PROGRESS"] = "處理的文件夾";
$MESS["TR_INDEX_ACTION_COLLECT_PHRASE"] = "步驟#num＃/＃len＃：將消息添加到索引";
$MESS["TR_INDEX_ACTION_COLLECT_PHRASE_PROGRESS"] = "文件索引";
$MESS["TR_INDEX_CSV_DLG_TITLE"] = "步驟＃num＃out＃len＃：索引導入消息";
$MESS["TR_INDEX_CSV_PROGRESS"] = "處理的文件";
$MESS["TR_INDEX_DLG_CANCELED"] = "索引構建已被取消。";
$MESS["TR_INDEX_DLG_COMPLETED"] = "創建了索引。";
$MESS["TR_INDEX_DLG_PARAM_LANGUAGES"] = "選擇索引語言";
$MESS["TR_INDEX_DLG_SUMMARY"] = "這將重建本地化消息索引。此操作可能需要一段時間。";
$MESS["TR_INDEX_DLG_TITLE"] = "更新消息索引";
$MESS["TR_LIST_EXPORT_CSV"] = "導出到CSV";
$MESS["TR_LIST_IMPORT_CSV"] = "從CSV導入";
$MESS["TR_LIST_REFRESH_INDEX"] = "指數";
$MESS["TR_NO_REWRITE_LANG_FILES"] = "僅導入新消息";
$MESS["TR_REINDEX"] = "索引導入消息：";
$MESS["TR_STARTING_PATH"] = "選擇要翻譯的文件夾";
$MESS["TR_UPDATE_LANG_FILES"] = "更新現有消息";
$MESS["TR_UPLOAD_CSV_FILE"] = "CSV文件：";
$MESS["TR_UPLOAD_CSV_FILE_EMPTY_ERROR"] = "需要上傳的文件";
