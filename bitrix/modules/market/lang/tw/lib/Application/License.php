<?php
$MESS["MARKET_INSTALL_EULA_TEXT"] = "我已經閱讀並同意<a href= \"#link# \" target= \"_blank \" class= \"market-popup__agreement_label-link \">許可協議</a>";
$MESS["MARKET_INSTALL_PRIVACY_LINK"] = "https://www.bitrix24.com/privacy/";
$MESS["MARKET_INSTALL_PRIVACY_TEXT"] = "我已經閱讀並同意<a href= \“#link# \" target= \"_blank \" class = \"market-popup__agreement_label-link \">隱私策略</a>";
$MESS["MARKET_INSTALL_TERMS_OF_SERVICE_LINK"] = "https://www.bitrix24.com/terms/apps24_terms_of_service.pdf";
$MESS["MARKET_INSTALL_TERMS_OF_SERVICE_TEXT"] = "通過安裝或下載應用程序或解決方案，您同意<a href= \"#link# \" target = \"_blank \" class = \"market-popup_agreement_label-link \"> bitrix24.market使用條款</ a>";
