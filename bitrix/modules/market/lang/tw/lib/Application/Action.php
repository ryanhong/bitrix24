<?php
$MESS["MARKET_APPLICATION_ACTION_APP_INSTALL"] = "安裝";
$MESS["MARKET_APPLICATION_ACTION_APP_UNINSTALL"] = "解除安裝";
$MESS["MARKET_APPLICATION_ACTION_BTN_BUY"] = "買";
$MESS["MARKET_APPLICATION_ACTION_BTN_BUY_LIMIT_3"] = "3個月";
$MESS["MARKET_APPLICATION_ACTION_BTN_BUY_LIMIT_6"] = "6個月";
$MESS["MARKET_APPLICATION_ACTION_BTN_BUY_LIMIT_12"] = "12個月";
$MESS["MARKET_APPLICATION_ACTION_BTN_CONFIGURATION_SETTING"] = "配置";
$MESS["MARKET_APPLICATION_ACTION_BTN_OPEN_PREVIEW"] = "預覽";
$MESS["MARKET_APPLICATION_ACTION_BTN_PROLONG"] = "更新";
$MESS["MARKET_APPLICATION_ACTION_BTN_PROLONG_LIMIT_3"] = "3個月";
$MESS["MARKET_APPLICATION_ACTION_BTN_PROLONG_LIMIT_6"] = "6個月";
$MESS["MARKET_APPLICATION_ACTION_BTN_PROLONG_LIMIT_12"] = "12個月";
$MESS["MARKET_APPLICATION_ACTION_BTN_RIGHTS"] = "編輯權限";
$MESS["MARKET_APPLICATION_ACTION_BTN_UPDATE"] = "更新";
