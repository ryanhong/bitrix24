<?php
$MESS["MARKET_DETAIL_ACTION_JS_INSTALL"] = "安裝";
$MESS["MARKET_DETAIL_ACTION_JS_REFRESH"] = "更新";
$MESS["MARKET_INSTALL_LICENSE_ERROR"] = "您必須閱讀並同意安裝申請的許可協議和隱私政策。";
$MESS["MARKET_INSTALL_TOS_ERROR"] = "您必須同意Bitrix24.Market使用條款來安裝該應用程序。";
