<?php
$MESS["HLBLOCK_INSTALL_TITLE"] = "高載信息塊模塊安裝";
$MESS["HLBLOCK_MODULE_DESCRIPTION"] = "該模塊用於在高負載條件下處理任意數據。";
$MESS["HLBLOCK_MODULE_NAME"] = "高載信息塊";
$MESS["HLBLOCK_UNINSTALL_TITLE"] = "高載信息塊模塊卸載";
$MESS["HLBLOCK_USERFIELD_EXISTS"] = "無法卸載模塊，因為系統中有\“綁定到Highload信息阻止項目\”系統中的自定義字段。";
