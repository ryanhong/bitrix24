<?php
$MESS["OP_DESC_HL_ELEMENT_DELETE"] = "授予刪除高載信息塊項目的能力。";
$MESS["OP_DESC_HL_ELEMENT_READ"] = "授予讀取高載信息塊項目的能力。";
$MESS["OP_DESC_HL_ELEMENT_WRITE"] = "授予添加和編輯Highload信息塊項目的能力。";
$MESS["OP_NAME_HL_ELEMENT_DELETE"] = "刪除項目";
$MESS["OP_NAME_HL_ELEMENT_READ"] = "讀取項目";
$MESS["OP_NAME_HL_ELEMENT_WRITE"] = "編輯項目";
