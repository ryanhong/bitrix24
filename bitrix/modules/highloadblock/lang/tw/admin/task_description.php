<?php
$MESS["TASK_DESC_HBLOCK_DENIED"] = "拒絕訪問Highload信息塊。";
$MESS["TASK_DESC_HBLOCK_READ"] = "授予對Highload信息塊的訪問。";
$MESS["TASK_DESC_HBLOCK_WRITE"] = "授予對Highload信息塊的寫入訪問。";
$MESS["TASK_NAME_HBLOCK_DENIED"] = "拒絕訪問";
$MESS["TASK_NAME_HBLOCK_READ"] = "讀";
$MESS["TASK_NAME_HBLOCK_WRITE"] = "寫";
