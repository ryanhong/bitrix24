<?php
$MESS["TM_CALENDAR_CHOOSE"] = "選擇日曆";
$MESS["TM_CLOCK_SELECT_DATE_LINK"] = "改變一天";
$MESS["TM_CONFIRM_LATE_OPEN"] = "您確定要在＃Time＃中登錄嗎？";
$MESS["TM_DAY_CLOSED_ADMIN"] = "工作日已被主管關閉。";
$MESS["TM_IN"] = "在";
$MESS["TM_TASK_ACCOMPLICES"] = "參與者";
$MESS["TM_TASK_ACCOMPLICES_PL"] = "參與者";
$MESS["TM_TASK_AUDITORS"] = "觀察者";
$MESS["TM_TASK_AUDITORS_PL"] = "觀察者";
$MESS["TM_TASK_CREATED_BY"] = "創造者";
$MESS["TM_TASK_CREATED_BY_PL"] = "創造者";
$MESS["TM_TASK_CREATED_DATE"] = "創建於";
$MESS["TM_TASK_DATE_END"] = "結尾";
$MESS["TM_TASK_DATE_START"] = "開始";
$MESS["TM_TASK_DEADLINE"] = "最後期限";
$MESS["TM_TASK_RESPONSIBLE_ID"] = "負責人";
$MESS["TM_TASK_RESPONSIBLE_ID_PL"] = "負責人";
$MESS["TM_TASK_SET_STATUS_2"] = "恢復";
$MESS["TM_TASK_SET_STATUS_2_1"] = "接受";
$MESS["TM_TASK_SET_STATUS_3"] = "開始執行";
$MESS["TM_TASK_SET_STATUS_4"] = "完全的";
$MESS["TM_TASK_SET_STATUS_6"] = "推遲";
$MESS["TM_TASK_SET_STATUS_7"] = "衰退";
$MESS["TM_TASK_STATUS"] = "地位";
$MESS["TM_TASK_STATUS_-1"] = "逾期";
$MESS["TM_TASK_STATUS_-2"] = "新的";
$MESS["TM_TASK_STATUS_1"] = "新的";
$MESS["TM_TASK_STATUS_2"] = "公認";
$MESS["TM_TASK_STATUS_3"] = "進行中";
$MESS["TM_TASK_STATUS_4"] = "完全的";
$MESS["TM_TASK_STATUS_5"] = "關閉";
$MESS["TM_TASK_STATUS_6"] = "遞延";
$MESS["TM_TASK_STATUS_7"] = "拒絕";
$MESS["TM_TILL"] = "＃今天＃直到＃時間＃";
