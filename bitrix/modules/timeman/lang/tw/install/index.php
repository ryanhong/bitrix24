<?php
$MESS["TIMEMAN_INSTALL_TITLE"] = "工作時間管理模塊安裝";
$MESS["TIMEMAN_MODULE_DESCRIPTION"] = "跟踪和控制工作時間的模塊。";
$MESS["TIMEMAN_MODULE_NAME"] = "工作時間管理";
$MESS["TIMEMAN_PHP_L439"] = "您使用的是PHP版本＃ver＃，而模塊需要版本5.0.0或更高版本。請更新您的PHP安裝或聯繫技術支持。";
$MESS["TIMEMAN_UNINSTALL_TITLE"] = "工作時間管理模塊卸載";
