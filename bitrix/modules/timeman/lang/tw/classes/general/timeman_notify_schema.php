<?php
$MESS["TIMEMAN_NS_ENTRY"] = "新工作日報告";
$MESS["TIMEMAN_NS_ENTRY_APPROVE"] = "工作日報告已批准或未批准";
$MESS["TIMEMAN_NS_ENTRY_COMMENT"] = "新工作日報告評論";
$MESS["TIMEMAN_NS_REPORT"] = "新的工作時間報告";
$MESS["TIMEMAN_NS_REPORT_APPROVE"] = "批准的工作時間報告更新";
$MESS["TIMEMAN_NS_REPORT_COMMENT"] = "工作時間報告評論";
