<?php
$MESS["COMMENT_AUTHOR"] = "作者：";
$MESS["COMMENT_TEXT"] = "評論：";
$MESS["REPORT_ADD"] = "已提交";
$MESS["REPORT_ADD_24"] = "已提交";
$MESS["REPORT_ADD_24_F"] = "已提交";
$MESS["REPORT_ADD_24_M"] = "已提交";
$MESS["REPORT_ADD_W"] = "已提交";
$MESS["REPORT_APPROVE"] = "報告證實";
$MESS["REPORT_APPROVE_SIMPLE"] = "報告";
$MESS["REPORT_CHANGE"] = "修改的";
$MESS["REPORT_CHANGE_24"] = "修改的";
$MESS["REPORT_CHANGE_24_F"] = "修改的";
$MESS["REPORT_CHANGE_24_M"] = "修改的";
$MESS["REPORT_CHANGE_W"] = "修改的";
$MESS["REPORT_DONE"] = "創建的報告";
$MESS["REPORT_FROM"] = "從";
$MESS["REPORT_FULL_COMMENT_CONFIRM_MOBILE"] = "報告證實";
$MESS["REPORT_FULL_COMMENT_CONFIRM_MOBILE_VALUE_B"] = "得分負";
$MESS["REPORT_FULL_COMMENT_CONFIRM_MOBILE_VALUE_G"] = "得分為正";
$MESS["REPORT_FULL_COMMENT_CONFIRM_VALUE_B"] = "消極的";
$MESS["REPORT_FULL_COMMENT_CONFIRM_VALUE_G"] = "積極的";
$MESS["REPORT_FULL_COMMENT_CONFIRM_WO_MARK"] = "報告確認，沒有標記";
$MESS["REPORT_FULL_COMMENT_CONFIRM_W_MARK"] = "報告已確認，標記為＃值＃";
$MESS["REPORT_FULL_IM_ADD"] = "添加了＃期間＃的工作報告";
$MESS["REPORT_FULL_IM_ADD_F"] = "添加了＃期間＃的工作報告";
$MESS["REPORT_FULL_IM_ADD_M"] = "添加了＃期間＃的工作報告";
$MESS["REPORT_FULL_IM_APPROVE_B"] = "確認了您的＃期間＃的工作報告，得分為負";
$MESS["REPORT_FULL_IM_APPROVE_F_B"] = "確認了您的＃期間＃的工作報告，得分為負";
$MESS["REPORT_FULL_IM_APPROVE_F_G"] = "確認了您的＃期間＃的工作報告，得分為正面";
$MESS["REPORT_FULL_IM_APPROVE_F_N"] = "確認了您的工作報告＃期間＃，沒有分數";
$MESS["REPORT_FULL_IM_APPROVE_F_X"] = "未經證實的＃期間工作報告";
$MESS["REPORT_FULL_IM_APPROVE_G"] = "確認了您的＃期間＃的工作報告，得分為正面";
$MESS["REPORT_FULL_IM_APPROVE_M_B"] = "確認了您的＃期間＃的工作報告，得分為負";
$MESS["REPORT_FULL_IM_APPROVE_M_G"] = "確認了您的＃期間＃的工作報告，得分為正面";
$MESS["REPORT_FULL_IM_APPROVE_M_N"] = "確認了您的工作報告＃期間＃，沒有分數";
$MESS["REPORT_FULL_IM_APPROVE_M_X"] = "未經證實的＃期間工作報告";
$MESS["REPORT_FULL_IM_APPROVE_N"] = "確認了您的工作報告＃期間＃，沒有分數";
$MESS["REPORT_FULL_IM_APPROVE_X"] = "未經證實的＃期間工作報告";
$MESS["REPORT_FULL_IM_COMMENT_1"] = "在您的＃期間＃的工作報告中評論";
$MESS["REPORT_FULL_IM_COMMENT_1_F"] = "在您的＃期間＃的工作報告中評論";
$MESS["REPORT_FULL_IM_COMMENT_1_M"] = "在您的＃期間＃的工作報告中評論";
$MESS["REPORT_FULL_IM_COMMENT_2"] = "在他們的＃期間＃的工作報告中發表了評論";
$MESS["REPORT_FULL_IM_COMMENT_2_F"] = "在他們的＃期間＃的工作報告中發表了評論";
$MESS["REPORT_FULL_IM_COMMENT_2_M"] = "在他們的＃期間＃的工作報告中發表了評論";
$MESS["REPORT_FULL_IM_COMMENT_3"] = "在＃期間＃的工作報告中評論";
$MESS["REPORT_FULL_IM_COMMENT_3_F"] = "在＃期間＃的工作報告中評論";
$MESS["REPORT_FULL_IM_COMMENT_3_M"] = "在＃期間＃的工作報告中評論";
$MESS["REPORT_NEW_COMMENT"] = "評論添加到";
$MESS["REPORT_PERIOD"] = "報告";
$MESS["REPORT_TITLE"] = "工作報告";
$MESS["REPORT_TITLE2"] = "工作報告";
$MESS["REPORT_TITLE_FOR_MAIL"] = "工作報告";
$MESS["REPORT_TO"] = "到";
$MESS["REPORT_WITH_B"] = "得分<span class ='tm-mark-log-b'>負面</span>";
$MESS["REPORT_WITH_G"] = "得分<span class ='tm-mark-log-g'>正面</span>";
$MESS["REPORT_WITH_N"] = "<span class ='tm-mark-log-n'>否得分</span>";
$MESS["REPORT_WITH_X"] = "<span class ='tm-mark-log-x'>未確認</span>";
$MESS["REPORT_WORK_REPORT"] = "工作報告";
