<?php
$MESS["TM_BASE_SERVICE_RESULT_ERROR_CALENDAR_NOT_FOUND"] = "時間表找不到假期日曆";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_NOTHING_TO_START"] = "太早了";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_OTHER_RECORD_FOR_DATES_EXISTS"] = "指定一天的工作時間記錄已經存在";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_PROHIBITED_ACTION"] = "無法執行此動作";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_REASON_IS_REQUIRED"] = "沒有指定更改的原因";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_RECORD_EXPIRED_TIME_END_REQUIRED"] = "需要時鐘時間";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_SCHEDULE_NOT_FOUND"] = "沒有發現時間表";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_SHIFT_NOT_FOUND"] = "找不到轉移。";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_SHIFT_PLAN_NOT_FOUND"] = "找不到班次計劃。";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_START_GREATER_THAN_NOW"] = "不能遲到了";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_VIOLATION_RULES_NOT_FOUND"] = "未找到自定義時間表跟踪設置";
$MESS["TM_BASE_SERVICE_RESULT_ERROR_WORKTIME_RECORD_NOT_FOUND"] = "沒有發現工作時間記錄。";
