<?php
$MESS["TM_SHIFT_FORM_BREAK_DURATION_TITLE"] = "斷斷續續";
$MESS["TM_SHIFT_FORM_END_TIME_TITLE"] = "班次結束";
$MESS["TM_SHIFT_FORM_NAME_ERROR"] = "不正確的移位名稱";
$MESS["TM_SHIFT_FORM_NAME_TITLE"] = "換檔名稱";
$MESS["TM_SHIFT_FORM_NUMBER_INTEGER_ONLY_ERROR"] = "字段\“＃field_name＃\”必須是整數";
$MESS["TM_SHIFT_FORM_NUMBER_LESS_MIN_ERROR"] = "字段\“＃field_name＃\”必須大於＃min＃";
$MESS["TM_SHIFT_FORM_NUMBER_TOO_BIG_ERROR"] = "字段\“＃field_name＃\”必須小於＃max＃";
$MESS["TM_SHIFT_FORM_REQUIRED_ERROR"] = "需要字段\“＃field_name＃\”。";
$MESS["TM_SHIFT_FORM_SCHEDULE_ID_TITLE"] = "計劃ID";
$MESS["TM_SHIFT_FORM_SHIFT_ID_TITLE"] = "Shift ID";
$MESS["TM_SHIFT_FORM_START_TIME_TITLE"] = "班次開始";
$MESS["TM_SHIFT_FORM_TIME_FORMATTED_ERROR"] = "\“＃field_name＃\”中使用的不正確時間格式";
