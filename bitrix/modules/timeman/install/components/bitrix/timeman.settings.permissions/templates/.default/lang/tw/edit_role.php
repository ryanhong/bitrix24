<?php
$MESS["TIMEMAN_ACTION_READ"] = "看法";
$MESS["TIMEMAN_ACTION_UPDATE"] = "編輯";
$MESS["TIMEMAN_DEFAULT_OPERATION_TITLE"] = "拒絕訪問";
$MESS["TIMEMAN_ROLE_CANCEL"] = "取消";
$MESS["TIMEMAN_ROLE_ENTITY"] = "實體";
$MESS["TIMEMAN_ROLE_LABEL"] = "姓名";
$MESS["TIMEMAN_ROLE_PERMISSION"] = "允許";
$MESS["TIMEMAN_ROLE_SAVE"] = "節省";
