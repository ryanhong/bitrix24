<?php
$MESS["SERVICES_INFO"] = "每當您想預訂會議室時，都會在日曆中找到空置的時間；致電您的經理確認預訂，並將其列入預訂時間表。";
$MESS["SERVICES_INIT_DATE"] = " - 顯示當前日期 - ";
$MESS["SERVICES_LINK"] = "會議室預訂表景觀";
$MESS["SERVICES_TITLE"] = "會議室預訂";
