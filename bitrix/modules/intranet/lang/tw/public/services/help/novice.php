<?php
$MESS["SERVICES_INFO"] = "<img width ='173'vspace ='5'hspace ='5'height ='170'align ='right'src ='＃stite＃site＃images/en/new_sm.jpg'/>新團隊成員應服用<< href ='＃站點＃服務/學習/'>新員工課程</a>熟悉公司。
<br />

<p>參加課程後，查看以下頁面：</p>

<p> <a href='#site#about/company/'>關於公司的</a>  - 您應該了解有關公司的官方和非官方信息。在這裡，您將閱讀有關公司成立和主要活動領域的有關的信息。本節還包含有用的信息：公司銀行詳細信息，地址和電話號碼。在與客戶，分支機構和承包商的工作中使用此信息。</p>

<p> <a href='#site#company/vis_structure.php'>公司結構</a>  - 提供了公司部門和辦公室層次結構的視覺表示。在這裡，您可以快速找到負責您關注的業務職能的部門。
  <br />
 </p>

<p> <a href='#site#company/index.php'>找到員工</a>  - 讓您按照任何標准在公司中找到任何人；檢查一個人當前是否在辦公室，並獲取其聯繫信息。您可以按部門和/或名稱找到一個人，也可以利用可以通過電子郵件地址和關鍵字進行搜索的擴展搜索功能。每個員工（或在門戶網站上）都有一個個人頁面，他們可以在其中發布其他信息：照片，文檔等。您可以打開用戶的個人頁面並在其博客文章中發表評論；發送私人消息，等等。
  <br />
 </p>

<p> <a href='#site#about/index.php'>重要新聞</a>  - 顯示公司的新聞頭條，以使您了解時事。
<br />


<p> <a href='#site#about/life.php'>公司生活</a>  - 公司的日常生活：活動和生日；慶祝活動和假期；團隊建設活動和教育 - 任何使我們聚集在一起的事情。
<br />


<p> <a href='# site#about/calendar.php'>活動日曆</a>  - 一種查看當前和即將到來的公司活動的便利方法。</p>

<p> <a href='#site#company/birthdays.php'>生日</a>  - 這樣您就不會忘記同事的生日。</p>

<p> <a href='#site#docs/'>文檔庫</a>  - 可用於團隊合作的公司文檔的公共集合。圖書館可以擁有隻有某些部門成員才能訪問的私人目錄。</p>

<p>請閱讀<a href='#site#services/learning/'target='_blank'>門戶用戶課程</a>在開始使用門戶之前，請徹底閱讀。您將了解所有門戶功能，日常使用和團隊合作功能。</p>

<p>祝您有美好的一天！
  <br />
 </p>";
$MESS["SERVICES_TITLE"] = "新員工的信息";
