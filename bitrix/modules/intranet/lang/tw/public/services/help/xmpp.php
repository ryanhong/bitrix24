<?php
$MESS["SERVICES_INFO"] = "該門戶具有私有<b> XMPP/Jabber服務器</b>！連接並在線！</b>交換消息和文件，
享受語音和視頻聊天 - 全部使用您喜歡的手機或消息軟件！<br />
<br />

<br />

<table cellspacing ='1'cellpadding ='1'border ='0'width ='100％'>
  <tbody>
    <tr><td valign='top'><img hspace='10' height='15' width='12' src='#SITE#images/en/docs/cp/bullet-n.gif' />訊息
交換（im）;
        <br />
       <img hspace = '10'height = '15'width = '12'src ='＃site＃site＃images/en/docs/cp/bullet-n.gif'/> file Exchange;
        <br />
       <img hspace = '10'height = '15'width = '12'src ='＃site＃site＃images/en/docs/cp/bullet-n.gif'/>始終在線;
        <br />
       <img hspace = '10'height = '15'width = '12'src ='＃站點＃images/en/docs/cp/bullet-n.gif'/>支持最受歡迎的支持
Messenger應用程序；
        <br />
       <img hspace = '10'height = '15'width = '12'src ='＃站點＃images/en/docs/cp/bullet-n.gif'/> voip集成，視頻
聊天;
        <br />
       <img hspace = '10'height = '15'width = '12'src ='＃site＃site＃images/en/docs/cp/bullet-n.gif'/>移動門戶;
        <br />
       <img hspace = '10'height = '15'width = '12'src ='＃site＃site＃images/en/docs/cp/bullet-n.gif'/>預先創建的聯繫人列表;
        <br />
       <img hspace = '10'height = '15'width = '12'src ='＃site＃site＃images/en/doc/cp/bullet-n.gif'/>常見消息歷史記錄;
        <br />
       <img hspace = '10'height = '15'width = '12'src ='＃site＃site＃images/en/doc/cp/bullet-n.gif'/> ssl支持。
        <br />

        <br />
       </td> <td>
        <br />
       </td> <td valign ='top'>
        <br />
       <img height = '63'width ='130'src ='＃站點＃images/en/docs/cp/m.jpg'/>
        <br /> <br />
        <a href='http://www.bitrixsoft.com/download/miranda_im.zip'>下載miranda！</a>
        <br />

        <br />
       </td> </tr>
   </tbody>
 </table>

<br />

<h2>私有<b> XMPP服務器</b> </h2>
 創建了私有<b> XMPP服務器</b>是為了解決任何企業可能遇到的最重要的問題：創建
<b>安全消息傳遞系統</b>。專用服務器允許避免使用不安全的ICQ或其他公共消息傳遞
在保持您習慣的工具的同時！門戶網站服務器通過<b> jabber </b>協議運行
由大多數流行的消息傳遞應用程序支持。有效地，這意味著您可以交流
與您的同事一起使用您喜歡的應用程序：<b> Miranda，Pidgin，Psi </b>等。
<br />

<br />
 此外，您不僅可以發送和接收<b>文件，音頻和視頻</b>消息。還要
更多的？沒問題 -  <b>您的移動設備</b>發送和接收消息。門戶現在是您的通用
溝通互換點！
<br />

<br />

<h2>它如何工作？</h2>
 <b> XMPP/Jabber Server </b>是一個功能齊全，經過徹底測試且穩定的軟件應用程序。
<br />

<br />

<table cellspacing ='0'cellpadding ='0'border ='0'width ='100％'>
  <tbody>
    <tr> </tr>

    <tr> <td valign ='top'>

        <br />
                                          </td> <td>
        <br />
       </td> <td valig ='top'>除了消息服務外，該服務器還可以使用您的門戶管理
指示用戶在線狀態。現在，您的員工無論身在何處都將始終在線 - 
XMPP服務器支持的平台範圍從UNIX服務器到移動設備有所不同。只需安裝一個
jabber啟用的使者到您的手機，<b>是
在線！</b>
        <br />

        <br />
       對於最常見的消息傳遞應用程序（Miranda，PSI等）<a name ='contacts'> </a>您沒有
手動創建聯繫人列表。列表是在安裝時自動生成的。
最終的聯繫人列表包含公司結構，其用戶由公司分組
部門。如果您的Messenger支持嵌套組，Messenger聯繫人列表中的組將
精確地反映了部門結構。 </td> <td>
        <br />
       </td> </tr>
   </tbody>
 </table>

<br />

<table cellspacing ='1'cellpadding ='1'border ='0'width ='100％'>
  <tbody>
    <tr> <td valign ='top'>您使用哪個Messenger，
服務器將始終存儲對話歷史記錄！打開“我的消息”在您的個人資料中，您
將看到完整的消息日誌。<br />
<h2>我應該選擇哪個Messenger？</h2>
什麼是最有效的消息傳遞客戶端？任何支持XMPP協議的人。我們測試了幾個使者，可以建議您嘗試<a href='http://www.bitrixsoft.com/download/miranda_im.zip'> miranda im </a>。您會找到Messenger配置
<a href='http://www.bitrixsoft.com/support/training/course/course/lesson.php?course_id = 27&id=1641'> Intranet Portal課程的說明
初學者</a>。<i>

          <br />
         </i>
        <H2> <a name='miranda'> </a> Miranda，請！</h2>

        <p class ='a'>配置miranda的唯一參數是您的登錄和密碼
用於登錄到門戶和門戶域名和端口號。諮詢您的系統
管理員有關這些選項，並詢問您的服務器是否使用SSL。輸入米蘭達的設置
選項對話框。單擊下面的屏幕截圖以查看示例。</p>
       </td> <td>
        <br />
       </td> <td valign ='top'>
        <a href= \“javascript:showimg('#site#images/en/docs/cp/im2.png',266,405,405，'company contact list') \">
        <img src ='＃站點＃images/en/docs/cp/cp/im2-s.png'width ='100'height ='152'border ='0'alt ='0'alt ='公司聯繫人列表'title ='公司聯繫人列表' />
        </a>
        <br />

        <br />
       </td> </tr>
   </tbody>
 </table>

<br />

<table cellspacing ='0'cellpadding ='0'border ='0'width ='100％'樣式='邊界崩潰：collapse;'>
  <tbody>


    <tr> <td valign ='top'>


    <a href= \“javascript:showimg('#site#images/en/docs/cp/cp8_miranda.png',648,472,472,'Miranda settings') \ \">
    <img src ='＃站點＃images/en/docs/cp/cp8_miranda-s.png'border ='0'width ='100'height = '73'alt ='miranda settings'title ='miranda settings'/米蘭達設置'/ >
    </a>
        <br />
       </td> <td valign ='top'> <span class ='text'>
          <ul>
            <li>在<b>用戶</b>和<b>密碼</b>字段; </li>中輸入Intranet門戶網站登錄和密碼

            <li>在<b> server </b>字段中鍵入服務器的域; </li>

            <li>要求您的Intranet門戶網站管理員向您告知XMPP服務器端口。輸入端口
相應字段中的數字; </li>

            <li>詢問您的管理員SSL是否由服務器使用。如果是這樣，請檢查<b> ssl </b>框。</li>

            <li>離開其他領域。單擊<b>確定</b>保存更改。</li>


           </ul>
         </span> </td> </tr>
   </tbody>
 </table>


<p>如果您習慣了其他一些消息傳遞應用程序，則不必切換到Miranda。查找類似的配置
Messenger設置窗口中的選項，並提供與Miranda相同的數據。
在門戶網站見！ </p>
";
$MESS["SERVICES_TITLE"] = "統一通信";
