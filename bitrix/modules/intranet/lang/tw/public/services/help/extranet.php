<?php
$MESS["SERVICES_INFO"] = "<H3>統一相鄰的信息空間</h3>

<p> <b> extranet </b>模塊是Intranet門戶的擴展
公司與承包商，供應商，
分銷商或其他外部用戶，同時禁止第三方訪問
私人公司信息。
</p>
<p> <b> extranet </b>是與外部互動的安全信息空間
環境。集體網絡空間專門設計用於有效
與合作夥伴的合作。邀請您的伴侶和同事參加Extranet
工作組並處理您的集體，共享的任務和問題。這
解決方案確保您的溝通在保留Intranet的同時確保安全
安全。 </p>

<div align ='center'> <img width ='302'height ='218'src ='＃site＃site＃images/en/docs/cp/cp/extranet_main-s300.png'完整='完整='完整/>
  <br />
  <i> extranet主頁</i>＆nbsp; </div>

<h3>訪問和安全</h3>

<p>不是公司員工的受邀用戶獲得有限的訪問權限
允許的，以及必不可少的許可，僅查看Extranet頁面。
這些用戶不允許查看公司的私人信息。同樣
時間，可以邀請公司的員工加入Extranet工作組
與他們的同事和合作夥伴一起工作：討論
論壇和博客；使用共享文件；日曆中的計劃活動；
分配任務並註意他們的進度；發布報告等等。</p>

<H3>團隊合作</h3>

因此
合作？從根本上和技術上來說，解決方案都利用相同
Intranet門戶基於的原則。 Extranet團隊合作絕對是
類似於Intranet門戶網站工作組中的相似之處。唯一的區別是
更嚴格的安全策略。</p>

<h3>如何安裝？</h3>

<p>安裝Bitrix24時，您可以自動安裝Extranet模塊。但是，升級較舊版本時可能並非如此。如果現有模塊列表中不存在Extranet模塊，則必須在繼續之前<a href='/bitrix/admin/module_admin.php'>安裝它</a>

<div align ='center'> <img width ='361'height ='205'src ='＃site＃site＃images/en/docs/cp/extranet_setup.png'完整='完整='完整/>
  <br />
  <i>安裝Extranet模塊</i> </div>

<p>成功安裝了模塊後，您將提示您運行<a href='/bitrix/admin/wizard_install.php?lang= en＆amp; wizardname = bitrix:extranet＆extranet＆amp； > Extranet網站嚮導</a>
您將配置Extranet網站參數。</p>

<p>嚮導完成後，<b> extranet </b>項目將顯示在
頂部菜單。該系統正在啟動並運行，您可以走！</p>

<table width ='100％'樣式='border-collapse：collapse'border ='0'cellspacing ='0'cellpadding ='0'>
  <tbody>
    <tr> <td valign ='top'>
        <br />
      </td> <td valign ='top'> <span class ='text'> <b>要做的事情：</b>
          <ul>

            <li>使用Special <b> Extranet工具</b>以更輕鬆的工作體驗； </li>

            <li>提供<b>公共信息</b>（考慮<b>公司</b>或<b>
              美國</b>部分）; </li>

            <li> <b>邀請您的同事</b>從合作夥伴公司那裡
              團隊合作。 </li>
          </ul>
        </span> </td> </tr>
  </tbody>
</table>";
$MESS["SERVICES_TITLE"] = "外部";
