<?php
$MESS["SERVICES_INFO1"] = "<p> bitrix24用Microsoft Outlook實現了雙向集成。
本質上，這意味著您不僅可以從門戶中導出信息
到Microsoft Outlook。連接後，兩個應用程序執行談判和同步，以便兩者都反映了對
另一端的個人日曆，聯繫人和任務。</p>

<table border ='0'cellSpacing ='1'cellpadding ='1'width ='100％'>
  <tbody>
    在<a href='#my_kalendar'>同步
        個人日曆</a>
        <br />
      <img hspace = '10'src ='＃站點＃images/en/docs/cp/bullet-n.gif'width = '12'height = '15'/> <a href='#company_calendar'> synchronize
        公司日曆</a>
        <br />
      <img hspace = '10'src ='＃站點＃images/en/docs/cp/bullet-n.gif'width = '12'height = '15'/> <a href='#useful'>同步
        個人聯繫</a>
        <br />
      <img hspace = '10'src ='＃站點＃images/en/docs/cp/bullet-n.gif'width = '12'height = '15'/> <a href='#kalendars'>導出
        多個日曆</a>
        <br />
      <img hspace = '10'src ='＃站點＃images/en/docs/cp/bullet-n.gif'width = '12'height = '15'/> <a href='#kalendars'>查看
        MS Outlook中的常見網格中的日曆</a>
        <br />
      <img hspace = '10'src ='＃站點＃images/en/docs/cp/bullet-n.gif'width = '12'height = '15'/> <a href='#useful1'> synChronize
        個人任務</a>
      </td> <td>
        <br />
      </td> <td valign ='top'>
        <b>立即連接到Microsoft Outlook！</b>
";
$MESS["SERVICES_INFO2"] = " <br />
      </td> </tr>
  </tbody>
</table>

<h2>雙向同步有什麼好處？ </h2>
<p>這種類型的同步使您可以保持數據（例如個人聯繫）
迄今為止，在門戶網站和MS Outlook同時。嘗試正確同步它們
現在，看看它有多容易！假設您正在飛機計劃
您公司的會議，任務和演講。在MS進行計劃之後
Outlook，使用雙向同步函數 - 您所有的新計劃都將出現
立即在門戶日曆中。保持日程安排而不乏味
手動複製！快速創建，易於管理！ <a name ='useful'> </a> </p>

<h2>用戶同步</h2>

<p>要將任務分配給其他人或接收任務，您必須將這些人添加到Microsoft Outlook中。它比簡單的簡單：選擇員工>搜索員工，然後單擊<b>與Outlook </b>。</p>同步。</p>
<p>喜歡Bitrix24模板的人：選擇員工>搜索屏幕左側菜單中的員工以打開用戶（員工）瀏覽頁面。現在，單擊工具欄上的更多</b>，然後選擇<b> Outlook </b>。</p>
<p>導入過程已完全自動化；您只需要確認一次或兩次操作即可。現在等待導入執行。</p>
<p>導入過程完成後，您的員工將在日曆中顯示：</p>
<div align ='Center'>
  <table style ='邊界崩潰：崩潰'border ='0'cellspacing ='1'cellpadding ='1'>
    <tbody>
      <tr> <td>
          <div align ='center'> <a href= \"javascript:showimg('#site#images/en/docs/big/big/outlook.png',600,413,413，adding a Employee') \ \"> <img title ='添加員工'border''0'alt ='添加員工'src ='＃site＃site＃site/en/docs/utlook_sm.png'width'width ='300'height ='207'/> </ a >
            <br />
          </div>

          <div align ='Center'> <i>來自門戶網站添加的員工</i>
            <br />
          </div>
        </td> </tr>
    </tbody>
  </table>
</div>

<p>從MS Outlook中刪除員工沒有危險：這不會刪除他們
來自門戶。只有門戶管理員才能做到這一點。</p>

<h2> <a name='my_kalendar'> </a>與MS Outlook同步日曆</h2>
<p>您可以將任何門戶日曆與MS Outlook日曆同步：個人，
員工或公司日曆。嘗試立即同步！打開日曆
頁面，在“動作”菜單中選擇<b>連接到Outlook </b>，然後開始同步：</p>

<div align ='center'> <img src ='＃站點＃images/en/docs/utlook_1.png'/>
<br> <i>連接到Outlook </i> </div>

<p>不關注MS Outlook消息框：其中大多數只是
通知；每當需要確認時確認操作。
例如，如果您遇到<b>'
Outlook？＆quot </b>消息 - 單擊<b>是</b>毫不猶豫。雙向MS
Outlook集成完全符合Microsoft規格和標準
保證絕對平穩的操作。</p>

態
  <br />
</div>

<p>如果要詳細控制該過程，則可以單擊<b>高級... </b>
為您的日曆提供描述：</p>

<div align ='Center'>
  <table style ='邊界崩潰：崩潰'border ='0'cellspacing ='1'cellpadding ='1'>
    <tbody>
      <tr> <td>
          <div align ='center'> <a href= \"javascript:showimg('#site#images/en/docs/big/big/outlook_3.png'3.png'，509，449，449，449，' calendar descripticat ='日曆描述'border ='0'alt ='日曆描述'src ='＃站點＃images/en/docs/utlook_3_sm.png'/> </a>
            <br />
          </div>

          <div align ='Center'> <i>日曆描述</i>
            <br />
          </div>
        </td> </tr>
    </tbody>
  </table>
</div>

<p> <a name='company_calendar'> </a>現在看結果。您的前景有一個新的
日曆顯示您的所有事件！有什麼比觀看更方便的
休假後，公司在Outlook中的活動。跟上您的公司，
連接並同步日曆，並處於厚度！</p>

<div align ='Center'>
  <table style ='邊界崩潰：崩潰'border ='0'cellspacing ='1'cellpadding ='1'>
    <tbody>
      <tr> <td>
          <div align ='center'> <a href = \'javaScript：showimg（'＃site＃site＃site＃images/en/docs/big/outlook_4.png'，709，575，'日曆已添加到Outlook！ '） “> <img title ='日曆已添加到Outlook！” border ='0'alt ='日曆已添加到Outlook！ src ='＃站點＃images/en/docs/utlook_4_sm.png'/> </a>
            <br />
          </div>

          <div align ='center'> <i>添加到Outlook！</i>
            <br />
          </div>
        </td> </tr>
    </tbody>
  </table>
</div>

<p> <a name='kalendars'> </a>現在連接所有必需的門戶日曆
同樣的方式。在一個網格中顯示它們，瞧 - 現在您的日曆看起來只是
喜歡門戶日曆！</p>

<div align ='Center'>
  <table style ='邊界崩潰：崩潰'border ='0'cellspacing ='1'cellpadding ='1'>
    <tbody>
      <tr> <td>
          <div align ='center'> <a href = \'javaScript：showimg（'＃site＃site＃site＃site/en/docs/big/outlook_5.png'，709，575，'導出日曆（s）到Outlook'） “> <img title ='導出日曆（s）到Outlook'border ='0'alt ='導出日曆to Outlook'src ='src ='＃site＃site＃site＃images/en/docs/ utlook_5_sm.png'/> < /a>
            <br />
          </div>

          <div align ='Center'> <i> MS Outlook日曆中的單個網格日曆</i>
            <br />
          </div>
        </td> </tr>
    </tbody>
  </table>
</div>

<H2>添加事件</h2>
<p>雙向整合如何在現實生活中起作用？嘗試將新事件添加到MS
Outlook日曆 - 您將看到它顯示在門戶日曆中！立即進行：</p>

<ul>
  <li>在日曆網格中選擇事件日期; </li>
  <li>雙擊它打開創建新事件窗口； </li>
  <li>填寫主題，開始，結束和描述字段； </li>
  <li>單擊<b>保存並關閉</b>。 </li>
</ul>

現在查看在MS Outlook日曆中添加的新事件。

<div align ='center'> <a href= \“javascript:showimg('#site#images/en/docs/big/big/outlook_6.png',650,500，'new event'ment event') \ \"> <img height =' 192'border ='0'width ='250'src ='＃站點＃site＃images/en/docs/utlook_6_sm.png'title ='click to hirarge'stripl'style'style ='cursor ：pointer; pointer;''' /> </a> </p>
  </div>
<p class ='a1'> <a name ='sinhr'> </a>您無需費心同步
帶有門戶日曆的MS Outlook日曆。執行此任務
自動與Outlook電子郵件檢查一起。右下角
Outlook窗口的角將顯示同步進度。</p>

在
<p>以下圖片在門戶日曆中顯示了相同的事件：</p>

<div align ='center'> <a href= \"javascript:showimg('#site#images/en/docs/big/big/outlook_9.png',452,272,'new ewarted'calendar')高度='150 '邊界='0'寬度='249'src ='＃site＃site＃images/en/docs/utlook_9_sm.png'title ='單擊以放大樣式'style ='光標：指針; pointer;''' /> </a> </div>

<p>您可以在MS Outlook和Portal日曆中編輯和刪除事件
同樣的方式。一側進行的任何更改都將在另一側反映。</p>

<a name ='useful1'> </a>
<H2>同步個人任務</h2>
<p>現在嘗試同步您的任務。請記住，這個過程也是
自動化，不需要您的幫助。</p>

<div align ='Center'>
  <div align ='center'> <a href= \“javascript:showimg('#site#images/en/docs/big/big/outlook_13.png'13.png',600,413,413,ynChronize personal Tasks'J) title ='同步個人任務'border ='0'alt ='同步個人任務'src ='＃site＃site＃site＃images/en/docs/utlook_13_sm.png'width'width ='400'height ='276' /> </> </a >
    <br />
  </div>
  <div align ='Center'> <i>同步個人任務</i>
  </div>
</div>

<p>同步任務：</p>
<ul>
  <li>同步用戶（單擊<b> Outlook </b> <b>搜索用戶</b> page）; </li>
  <li>提示時，輸入門戶訪問密碼； </li>
  <li>用戶列表將自動添加到MS Outlook； </li>
  <li>使用MS Outlook命令將任務分配給任何用戶； </li>
  <li> MS Outlook將連接到門戶並同步任務。 </li>
</ul>
";
$MESS["SERVICES_LINK1"] = "連接聯繫人";
$MESS["SERVICES_LINK2"] = "連接我的任務";
$MESS["SERVICES_TITLE"] = "Microsoft Outlook雙向集成";
