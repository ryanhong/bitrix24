<?php
$MESS["ABOUT_TITLE"] = "管理";
$MESS["ABOUT_TOP1_IMG"] = "＃網站＃images/en/company/about/management/bandurin.jpg";
$MESS["ABOUT_TOP1_INFO"] = "<p> </p>
<H2>阿特金森·摩根</h2>
<p> <b>首席執行官</b> </p>
<p>摩根於2004年6月被任命。2004年之前，他在許多超市連鎖店工作，擔任高管。 1999年，他加入了非全球食品（現已合併），並立即實施了全球戰略。在2003年加入我們之前，他還研究了有關食品金融，商店所有權和食品質量標準的政策問題。</p>。";
$MESS["ABOUT_TOP2_IMG"] = "＃網站＃images/en/company/about/management/pankrashev.jpg";
$MESS["ABOUT_TOP2_INFO"] = "<p> </p>
<h2>米爾本·獅子座</h2>
<p> <b>投資總監</b> </p>
<p>獅子座曾在糧食生產領域和糧食社區的一系列職位工作。他從1997年到2004年擔任該地區主管，當時是地區主任，領導了巴黎交易所的公開募股。作為投資負責人，貝特拉姆（Bertram）負責擴大負擔得起的食品投資計劃，並引入共享股權以協助首次購房者。 </p>";
$MESS["ABOUT_TOP3_IMG"] = "＃網站＃images/en/company/about/abination/porshnev.jpg";
$MESS["ABOUT_TOP3_INFO"] = "<p> </p>
<H2> Tharp Andrew </h2>
<p> <b>調節＆amp;檢查主管</b> </p>
<p>安德魯被任命為監管主任＆amp; 2003年6月的檢查。安德魯（Andrew）在過去的十年中一直在食品行業工作，從政策和計劃轉變為食品生產法規和檢查。 </p>
<p>在我們的角色中，他正在開發一個新框架來評估全球食品的表現。</p>";
