<?php
$MESS["MARKETPLACE_BLOCK1_INFO"] = "<p>  - 創建一個應用程序</p>
<p>  - 將其添加到您的門戶網站</p>";
$MESS["MARKETPLACE_BLOCK1_TITLE"] = "我的帳戶";
$MESS["MARKETPLACE_BLOCK2_INFO"] = "<p>  - 成為合作夥伴</p>
<p>  - 創建一個應用程序</p>
<p>  - 將應用程序上傳到您的合作夥伴配置文件</p>
<p>  - 發布您的應用程序</p>";
$MESS["MARKETPLACE_BLOCK2_LINK"] = "https://www.bitrix24.com/apps/dev.php";
$MESS["MARKETPLACE_BLOCK2_TITLE"] = "市場清單";
$MESS["MARKETPLACE_BUTTON"] = "繼續";
$MESS["MARKETPLACE_BUTTON_ADD"] = "添加";
$MESS["MARKETPLACE_OR"] = "或者";
$MESS["MARKETPLACE_PAGE_TITLE"] = "如何將應用程序添加到Bitrix24？";
$MESS["MARKETPLACE_PAGE_TITLE2"] = "可以由您的開發人員創建一個應用程序，也可以使用<a target='_blank'href= \"https://www.bitrix24.com/partners/predners/xtrok訂購。";
$MESS["MARKETPLACE_TITLE"] = "添加應用程序";
