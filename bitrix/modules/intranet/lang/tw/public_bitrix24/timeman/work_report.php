<?php
$MESS["TARIFF_RESTRICTION_TEXT"] = "所有員工的自動收集工作報告是提高效率的最有用方法之一。您設置時間表（例如，每個星期五或每個月末），門戶將打開一個窗口，員工將不得不在不久的將來的每日/每週/每月成就和概述計劃中輸入報告。然後，員工的主管可以以正面或負面的評分接受報告，以向每個下屬提供有用的反饋。 <a href= \“javascript:void(0) \" onclick='top.bx.helper.helper.show（";
$MESS["TARIFF_RESTRICTION_TEXT2"] = "工作報告僅在<a href= \"/settings/license_all.php \" target= \"_blank \">選定的商業計劃</a>中可用。";
$MESS["TITLE"] = "工作報告";
