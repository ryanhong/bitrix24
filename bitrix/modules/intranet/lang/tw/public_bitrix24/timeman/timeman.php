<?php
$MESS["TARIFF_RESTRICTION_TEXT"] = "工作時間管理是任何主管的重要工具。看看每個員工何時開始並結束工作日，他們工作了幾個小時，工作的工作以及是否有人遲到或離開。 Bitrix24數字打孔時鐘在任何設備上都可以使用，並為您在現場或遙控器上為每個員工提供詳細的工作時間報告。 <a href= \“javascript:void(0) \" onclick='top.bx.helper.helper.show（";
$MESS["TARIFF_RESTRICTION_TEXT2"] = "工作時間管理僅在<a href= \"/settings/license_all.php \" target= \之\"_blank \">選定的商業計劃</a>中可用。";
$MESS["TITLE"] = "工作時間摘要";
