<?php
$MESS["ABSENCE_TARIFF_RESTRICTION_TITLE"] = "缺席圖表和其他有用的工具可在選定的商業計劃上獲得。";
$MESS["TARIFF_RESTRICTION_TEXT"] = "缺席圖表提供了一個人的位置和缺席的理由的視覺呈現：生意，生病或產假。當計劃假期離開時，圖表也將派上用場，因為可以一次看到多個人的所有不需要的同時葉子。它易於使用：葉子可以由員工或人力資源管理器添加。系統還可以根據請求批准工作流程自動添加葉子。 <a href= \“javascript:void(0) \" onclick='top.bx.helper.helper.show（";
$MESS["TITLE"] = "缺席圖表";
