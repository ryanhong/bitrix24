<?php
$MESS["TARIFF_RESTRICTION_TEXT"] = "Bitrix24可以通過特殊會議和簡報計劃者來計劃和組織活動和會議。任務將被分配給指定人員，討論和記錄的訴訟程序，要求為各自議程項目創建報告。 <a href= \"javascript:void(0) \" onclick='top.bx.helper.helper.show( \"redirect = detail＆code = 1436032 \ \");'>詳細信息</a>";
$MESS["TARIFF_RESTRICTION_TEXT2"] = "會議和簡報僅在<a href= \"/settings/license_all.php \" target= \"_blank \">選定的商業計劃</a>中可用。";
$MESS["TARIFF_RESTRICTION_TEXT3"] = "會議和簡報僅適用於專業計劃。";
$MESS["TITLE"] = "會議和簡報";
