<?php
$MESS["MENU_BIZPROC_ACTIVE"] = "運行工作流程";
$MESS["MENU_BIZPROC_ACTIVE_1"] = "主動工作流程";
$MESS["MENU_BIZPROC_CRM"] = "CRM中的工作流程";
$MESS["MENU_BIZPROC_DISK"] = "公司驅動器上的工作流程";
$MESS["MENU_BIZPROC_TASKS"] = "我的作業";
$MESS["MENU_BIZPROC_TASKS_1"] = "我的作業";
$MESS["MENU_BIZPROC_TASKS_2"] = "作業";
$MESS["MENU_MY_PROCESS"] = "我的工作流程";
$MESS["MENU_MY_PROCESS_1"] = "我的工作流程";
$MESS["MENU_PROCESS_STREAM"] = "活動流中的工作流程";
$MESS["MENU_PROCESS_STREAM2"] = "飼料中的工作流程";
$MESS["MENU_PROCESS_STREAM_3"] = "開始工作流程";
