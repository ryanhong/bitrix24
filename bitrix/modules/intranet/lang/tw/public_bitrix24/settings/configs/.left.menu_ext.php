<?php
$MESS["MENU_ADMIN_PANEL"] = "在線商店控制面板";
$MESS["MENU_CONFIGS"] = "帳號設定";
$MESS["MENU_EVENT_LOG"] = "事件日誌";
$MESS["MENU_MAIL_BLACKLIST"] = "黑名單";
$MESS["MENU_MAIL_MANAGE"] = "管理電子郵件帳戶";
$MESS["MENU_UPDATE_DESC"] = "版本歷史記錄";
$MESS["MENU_VM"] = "虛擬設備";
