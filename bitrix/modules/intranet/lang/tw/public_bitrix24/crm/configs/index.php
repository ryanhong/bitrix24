<?php
$MESS["BP"] = "業務流程";
$MESS["CONFIG"] = "其他設置";
$MESS["CURRENCY"] = "貨幣";
$MESS["EXCH1C"] = "1C集成";
$MESS["EXTERNAL_SALE"] = "網絡商店";
$MESS["FIELDS"] = "自定義字段";
$MESS["LOCATIONS"] = "位置";
$MESS["MAIL_TEMPLATES"] = "電子郵件模板";
$MESS["MEASURE"] = "測量單位";
$MESS["PERMS"] = "訪問權限";
$MESS["PRODUCT_PROPS"] = "產品屬性";
$MESS["PS"] = "付款和發票";
$MESS["SENDSAVE"] = "電子郵件集成";
$MESS["SLOT"] = "分析報告";
$MESS["STATUS"] = "選擇列表";
$MESS["TAX"] = "稅";
$MESS["TITLE"] = "設定";
