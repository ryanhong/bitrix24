<?php
$MESS["INTRANET_PUBLIC_CONVERT2_BUTTON"] = "立即轉換";
$MESS["INTRANET_PUBLIC_CONVERT2_DESC"] = "現在，“用戶搜索”頁面和外部用戶頁面使用一個新組件，將條目顯示為帶有高級過濾器的表。";
$MESS["INTRANET_PUBLIC_CONVERT2_DESC_TITLE"] = "轉換公司中的公共頁面，並聯繫部分以查看行動中的此新功能（如果安裝了相應的模塊，也適用於外部網站）。";
$MESS["INTRANET_PUBLIC_CONVERT2_FINISH"] = "轉換已經完成";
$MESS["INTRANET_PUBLIC_CONVERT2_OPTIONS_DESC"] = "（如果您想稍後還要恢復到上一個​​結構，建議創建公共頁面的備份副本）";
$MESS["INTRANET_PUBLIC_CONVERT2_OPTIONS_TITLE"] = "立即轉換公共頁面";
$MESS["INTRANET_PUBLIC_CONVERT2_SKIP_BUTTON"] = "不要轉換";
$MESS["INTRANET_PUBLIC_CONVERT2_SKIP_FINISH"] = "轉換被取消";
$MESS["INTRANET_PUBLIC_CONVERT2_TAB"] = "轉變";
$MESS["INTRANET_PUBLIC_CONVERT2_TAB_TITLE"] = "編輯轉換首選項";
$MESS["INTRANET_PUBLIC_CONVERT2_TITLE"] = "轉換公共頁面";
$MESS["INTRANET_PUBLIC_CONVERT_INDEX"] = "將任何部分用於主頁（通過覆蓋index.php文件，然後將活動流將其複製到 /流 /文件夾）";
