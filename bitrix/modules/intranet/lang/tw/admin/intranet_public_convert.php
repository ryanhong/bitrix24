<?php
$MESS["INTRANET_PUBLIC_CONVERT_BUTTON"] = "立即轉換";
$MESS["INTRANET_PUBLIC_CONVERT_DESC"] = "新的Bitrix24模板帶有更新和改進的導航，以及新的小型個性化左菜單。用戶現在可以自定義每個部分的外觀。現在，工作區域較高20％。現在，頂部菜單具有統一的導航系統。";
$MESS["INTRANET_PUBLIC_CONVERT_DESC2"] = "<ul>
<li>易於自定義的左菜單</li>
<li>所有必需品總是方便的</li>
<li>個性化最常用的工具（CRM，任務或活動流）可適應與不同的客戶端組匹配</li>
<li>工作區域延長了20％：不使用它時折疊左菜單</li>
<li>統一的頂部菜單導航：第二層共享共同結構的菜單</li>
<li>使用搜索欄更快地通過名稱找到頁面</li>
</ul>";
$MESS["INTRANET_PUBLIC_CONVERT_DESC_TITLE"] = "立即轉換您的公共頁面以嘗試令人興奮的新功能：";
$MESS["INTRANET_PUBLIC_CONVERT_FINISH"] = "轉換已經完成。";
$MESS["INTRANET_PUBLIC_CONVERT_INDEX"] = "將任何部分用於主頁（通過覆蓋index.php文件，然後將活動流將其複製到 /流 /文件夾）";
$MESS["INTRANET_PUBLIC_CONVERT_OPTIONS_DESC"] = "（如果您想稍後還要恢復到上一個​​結構，建議創建公共頁面的備份副本）";
$MESS["INTRANET_PUBLIC_CONVERT_OPTIONS_TITLE"] = "立即轉換公共頁面";
$MESS["INTRANET_PUBLIC_CONVERT_SECTIONS"] = "部分（\“ Time and Reports \”，\“ Calendar \”，\“ Workflows \”和\“ Bitrix24.drive \”將創建）";
$MESS["INTRANET_PUBLIC_CONVERT_TAB"] = "轉換";
$MESS["INTRANET_PUBLIC_CONVERT_TAB_TITLE"] = "編輯轉換首選項";
$MESS["INTRANET_PUBLIC_CONVERT_TITLE"] = "轉換公共頁面";
$MESS["INTRANET_PUBLIC_SKIP_CONVERT_BUTTON"] = "不要轉換";
$MESS["INTRANET_PUBLIC_SKIP_CONVERT_FINISH"] = "轉換被取消";
