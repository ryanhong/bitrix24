<?php
$MESS["INTR_EMP_CANCEL"] = "取消";
$MESS["INTR_EMP_CANCEL_TITLE"] = "取消員工選擇";
$MESS["INTR_EMP_SUBMIT"] = "選擇";
$MESS["INTR_EMP_SUBMIT_TITLE"] = "選擇選定的員工";
$MESS["INTR_EMP_WAIT"] = "載入中...";
$MESS["INTR_EMP_WINDOW_CLOSE"] = "關閉";
$MESS["INTR_EMP_WINDOW_TITLE"] = "選擇員工";
