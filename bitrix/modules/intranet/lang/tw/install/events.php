<?php
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_DESC"] = "＃email_to＃ - 管理電子郵件
＃LearnMore_link＃ - 文檔URL
＃support_link＃ - 技術支持URL";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_MESSAGE"] = "<p>我們注意到您已經嘗試創建公司域但從未完成註冊。</p>

<p>如果您需要幫助，請打開電子郵件設置頁面閱讀以獲取設置電子郵件服務並完成BITRIX24域註冊的詳細說明。</p>

<p>如果您想查看通常如何完成，我們有許多使用電子郵件域供您查看的示例。這些示例將揭示域註冊背後的魔術，創建一個郵箱並使用Bitrix24使用整個內容。</p>

<p> <a href= \"#learnmore_link# \ \">了解更多</a> </p>

<p>如果您仍然有疑問，請聯繫我們的<a href= \"#support_link# \"> helpdesk </a>。</p>";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_NAME"] = "完成添加電子郵件域";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_SUBJECT"] = "您尚未完成添加域";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_DESC"] = "＃email_to＃ - 管理電子郵件
＃LearnMore_link＃ - 文檔URL
＃support_link＃ - 技術支持URL";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_MESSAGE"] = "<p>您已經註冊了域並創建了郵箱。但是，您的員工仍然不知道他們可以在公司域中創建自己的郵箱。</p>

<p>分享您的知識，並幫助他們學習創建自己的郵箱，或在其Bitrix24頁面上註冊郵箱。</p>

<p> <a href= \"#learnmore_link# \ \">如何為員工註冊郵箱</a> </p>

<p>如果您仍然有疑問，請聯繫我們的<a href= \"#support_link# \"> helpdesk </a>。</p>";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_NAME"] = "在域中創建員工郵箱";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_SUBJECT"] = "給您的員工的公司電子郵件";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_DESC"] = "＃email_to＃ - 管理電子郵件
＃LearnMore_link＃ - 文檔URL
＃support_link＃ - 技術支持URL";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_MESSAGE"] = "<p>您的域已成功地添加到Bitrix24中，但其中尚無郵箱。</p>
<p>我們有許多使用電子郵件域供您查看的示例。使用示例，您將學習如何創建郵箱並與Bitrix24一起使用。</p>

<p> <a href= \"#learnmore_link# \ \">了解更多</a> </p>

<p>如果您仍然有疑問，請聯繫我們的<a href= \"#support_link# \"> helpdesk </a>。</p>
";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_NAME"] = "在域中創建郵箱";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_SUBJECT"] = "您的公司電子郵件bitrix24";
$MESS["INTRANET_MAILDOMAIN_NOREG_DESC"] = "＃email_to＃ - 管理電子郵件
＃LearnMore_link＃ - 文檔URL
＃support_link＃ - 技術支持URL";
$MESS["INTRANET_MAILDOMAIN_NOREG_MESSAGE"] = "<p>您將為您的公司註冊一個域名，但從外觀上找不到好名字。</p>

<p>我們有許多使用電子郵件域供您查看的示例。使用示例，您將學習如何創建郵箱並與Bitrix24一起使用。</p>

<p> <a href= \"#learnmore_link# \ \">如何為員工註冊郵箱</a> </p>

<p>如果您仍然有疑問，請聯繫我們的<a href= \"#support_link# \"> helpdesk </a>。</p>";
$MESS["INTRANET_MAILDOMAIN_NOREG_NAME"] = "註冊電子郵件域";
$MESS["INTRANET_MAILDOMAIN_NOREG_SUBJECT"] = "您尚未完成添加域";
$MESS["INTRANET_USER_ADD_DESC"] = "＃email_to＃ - 新員工的電子郵件地址
＃鏈接＃ -Intranet URL";
$MESS["INTRANET_USER_ADD_MESSAGE"] = "＃USER_TEXT＃

＃關聯＃

要登錄，請使用您的電子郵件作為登錄和此密碼：＃密碼＃";
$MESS["INTRANET_USER_ADD_NAME"] = "添加員工";
$MESS["INTRANET_USER_ADD_SUBJECT"] = "加入Bitrix24的團隊";
$MESS["INTRANET_USER_INVITATION_DESC"] = "＃email_to＃ - 受邀人員的電子郵件地址
＃鏈接＃ - 新員工激活鏈接";
$MESS["INTRANET_USER_INVITATION_MESSAGE"] = "＃USER_TEXT＃

＃關聯＃

使用您的電子郵件地址作為身份驗證的用戶名。第一次進行身份驗證時，您將必須提出您的個人密碼。";
$MESS["INTRANET_USER_INVITATION_NAME"] = "邀請人";
$MESS["INTRANET_USER_INVITATION_SUBJECT"] = "您已被邀請到Bitrix24";
