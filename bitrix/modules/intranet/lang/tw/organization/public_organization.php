<?php
$MESS["ABSENCE_FORM_2"] = "選擇缺席的用戶
";
$MESS["APP_RIGHTS"] = "用戶訪問權限";
$MESS["AUTH_OTP_HELP_TEXT"] = "為了使您的BitRix24數據更安全，您的管理員啟用了額外的安全選項：兩步身份驗證。<br/> <br/>
兩步身份驗證涉及兩個隨後的驗證階段。第一個需要您的主密碼。第二階段包括發送到您的手機（或使用硬件加密狗）的一次性代碼。<br/> <br/>
<div class = \“ login-text-img \”> <img src = \“＃路徑＃/images/en en/img1.png \”> </div>
<br/>
啟用了兩步身份驗證後，您將必須通過兩個身份驗證屏幕：<br/> <br/>
 - 輸入您的電子郵件和密碼; <br/>
 - 然後，輸入一個一次性代碼，可以使用您在啟用兩步身份驗證時已安裝的Bitrix OTP移動應用程序獲得。<br/> <br/>
在手機上運行該應用程序：<br/> <br/>
<div class = \“ login-text-img \”> <img src = \“＃路徑＃/images/en en/img2.png \”> </div> </div>
<br/>
輸入您在屏幕上看到的代碼：<br/> <br/>
<div class = \“ login-text-img \”> <img src = \“＃路徑＃/images/en en/img3.png \”> </div> </div>
<br/>
如果您使用多個Bitrix24，請在輸入之前確保代碼適用於正確的Bitrix24。<br/> <br/>
<div class = \“ login-text-img \”> <img src = \“＃路徑＃/images/en en/img4.png \”> </div>
<br/>
如果您從來沒有機會啟用和配置兩步身份驗證或丟失手機，請聯繫您的管理員以還原對Bitrix24的訪問。<br/> <br/>
如果您是管理員，請聯繫<a href= \之http://www.bitrix24.com/support/helpdesk/flow\"> bitrix helpdesk </a>以還原訪問權限。";
$MESS["B24_NEW_USER_MENTION"] = "在一條有關新添加的用戶＃標題＃的消息中提到了您。";
$MESS["B24_NEW_USER_MENTION_F"] = "在一條有關新添加的用戶＃標題＃的消息中提到了您。";
$MESS["B24_NEW_USER_MENTION_M"] = "在一條有關新添加的用戶＃標題＃的消息中提到了您。";
$MESS["B24_NEW_USER_TITLE"] = "添加了新用戶";
$MESS["B24_NEW_USER_TITLE_LIST"] = "新用戶";
$MESS["B24_NEW_USER_TITLE_SETTINGS"] = "新用戶";
$MESS["BITRIX24_HELP_VIDEO_TITLE_13"] = "組織的結構";
$MESS["BITRIX24_HELP_VIDEO_TITLE_FULL_13"] = "組織的結構";
$MESS["BITRIX24_INVITE"] = "邀請用戶";
$MESS["BITRIX24_INVITE_ACTION"] = "邀請用戶";
$MESS["BITRIX24_SEARCH_EMPLOYEE"] = "用戶";
$MESS["BITRIX24_USER_INVITATION_DESC"] = "＃email_from＃ - 邀請用戶電子郵件
＃email_to＃ - 邀請用戶電子郵件
＃鏈接＃ - 新用戶激活鏈接";
$MESS["BITRIX24_USER_INVITATION_NAME"] = "邀請人";
$MESS["BLOG_DEMO_MESSAGE_BODY_1"] = "親愛的同事，Intranet門戶網站今天正式開始。

有效地，這意味著現在每個用戶都允許查看公共信息，並給出了訪問其私有區域的密碼。

對門戶區域的可配置訪問為我們提供了團隊合作功能：我們現在可以編輯文檔；創建工作組；詳細說明報告 - 一起！

門戶網站的目的是使用戶之間的通信鏈盡可能短。該門戶是快速而現代的交流手段，是避免書面工作並將業務信息轉化為電子課程的最佳方法。
 
使用Intranet門戶網站，組織用戶可以獲取任何技術，參考或法學信息，包括公司標準和身份。

用戶可以發佈到論壇；向技術支持和供應服務發送請求；共享和交換文件；討論組織新聞，並從管理層獲取最新信息。

該門戶將成為我們組織的重要組成部分！";
$MESS["BLOG_DEMO_MESSAGE_TITLE_1"] = "組織Intranet門戶的開放";
$MESS["BLOG_DESTINATION_ALL"] = "給所有用戶";
$MESS["BLOG_GRATMEDAL_1"] = "添加用戶";
$MESS["BM_USR_CNT"] = "用戶數";
$MESS["BX24_EXTRANET2INTRANET_DESC"] = "<b> #full_name＃</b>是外部用戶。<br> <br>請注意，您<b> </b> </b> </b>以後能夠將用戶從Intranet轉移回Extranet。 < br>要將＃full_name＃傳輸到Intranet，請選擇目標部門。";
$MESS["BX24_EXTRANET2INTRANET_SUCCESS"] = "用戶已成功傳輸。他們現在可以根據自己的權限訪問Intranet的內部內容。";
$MESS["BX24_INVITE_DIALOG_ADDED"] = "<b>恭喜！</b> <br>已將Intranet成員通知發送給該用戶。<br> <br> <br> <br>審查您在<a style = \“ white space：nowrap中邀請的新用戶； \“ href = \”＃site_dir＃company/？show_user = intactive \'>用戶目錄</a>。";
$MESS["BX24_INVITE_DIALOG_EMAIL"] = "輸入要邀請的人的電子郵件地址。帶有逗號或空間的多個條目。";
$MESS["BX24_INVITE_DIALOG_EMPLOYEE"] = "用戶";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>成功！</b> <br>邀請已發送到指定的地址。<br> <br>查看您剛剛邀請的用戶，<a style = \“白空間：nowrap; \ \” href = \ “/company/？show_user = intactive \”>單擊此處</a>。";
$MESS["BX24_INVITE_DIALOG_INVITE_LIMIT"] = "您不能邀請更多用戶，因為它將超過您的許可條款。";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_1"] = "請加入我們的Bitrix24帳戶。在這個地方，每個人都可以在任務和項目上進行溝通，協作，管理客戶並做更多的事情。";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "被邀請人的數量超出了您的許可條款。";
$MESS["BX24_INVITE_TITLE_ADD"] = "添加用戶";
$MESS["BX24_INVITE_TITLE_INVITE"] = "邀請用戶";
$MESS["CAL_COMPANY_SECT_DESC_0"] = "我們參與的表演和會議";
$MESS["CAL_COMPANY_SECT_DESC_1"] = "倫敦辦公室發生的事件";
$MESS["CAL_COMPANY_SECT_DESC_2"] = "巴黎辦公室發生的事件";
$MESS["CAL_COMP_EVENT_DESC_3"] = "對新用戶的培訓";
$MESS["CAL_TYPE_COMPANY_NAME"] = "組織日曆";
$MESS["COMPANY_MENU_EMPLOYEES"] = "查找用戶";
$MESS["COMPANY_MENU_STRUCTURE"] = "組織的結構";
$MESS["COMPANY_TITLE"] = "查找用戶";
$MESS["CONFIG_ALLOW_NEW_USER_LF"] = "將新用戶通知發佈到活動流";
$MESS["CONFIG_CLIENT_LOGO"] = "組織徽標";
$MESS["CONFIG_COMPANY_NAME"] = "機構名稱";
$MESS["CONFIG_COMPANY_TITLE_NAME"] = "在標題中顯示的組織名稱";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_JOIN"] = "將新用戶通知發佈到一般聊天";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_LEAVE"] = "將用戶解僱通知通知一般聊天";
$MESS["CONFIG_LOGO_24"] = "將\“ 24 \”添加到組織徽標";
$MESS["CONFIG_OTP_ADMIN_IS_REQUIRED_INFO"] = "在將用戶遷移到兩步身份驗證系統之前，請先將其設置為您的帳戶。<br/> <br/>請繼續在個人資料的安全頁面上啟用它。";
$MESS["CONFIG_OTP_IMPORTANT_TEXT"] = "今天，您使用登錄名和密碼登錄到Bitrix24。業務和個人數據受數據加密技術保護。但是，惡意人可以使用一些工具來進入您的計算機並竊取您的登錄憑據。

為了防止可能的威脅，我們為Bitrix24：兩步身份驗證啟用了額外的安全功能。

兩步身份驗證是一種特殊的方法，可以防止黑客軟件，尤其是密碼盜竊。每次登錄系統時，您都必須通過兩個級別的驗證。首先，您將輸入電子郵件和密碼，然後輸入從移動設備獲得的一次性安全代碼。

即使任何用戶的登錄和密碼被攻擊者偷走了，這也將使我們的業務數據安全。

您有5天的時間啟用此功能。

要配置新的身份驗證過程，請轉到您的個人資料，然後選擇\“安全設置\”頁面。

如果您沒有合適的移動設備可以運行該應用程序，或者您遇到任何類型的問題，請通過對此帖子發表評論，讓我們知道。";
$MESS["CONFIG_OTP_POPUP_TEXT"] = "這是您可以發佈到活動流的文本<br/>供用戶閱讀。<br/> <br/>
讓您的同事知道兩步身份驗證，<br/>配置過程和新的身份驗證方法<br/>他們將不得不使用將其登錄到其Bitrix24。";
$MESS["CONFIG_OTP_SECURITY"] = "為所有用戶啟用兩步身份驗證";
$MESS["CONFIG_OTP_SECURITY2"] = "為所有用戶進行兩步授權";
$MESS["CONFIG_OTP_SECURITY_DAYS"] = "指定所有用戶必須啟用兩步身份驗證的時間段";
$MESS["CONFIG_OTP_SECURITY_INFO"] = "我們已經開發了一個用戶友好的兩步身份驗證啟用程序，任何用戶都可以在沒有專家幫助的情況下採取的過程。<br/> <br/>
將向每個用戶發送一條消息，通知他們他們必須在輸入的時間段內啟用兩步身份驗證。無法做的用戶
因此，將不再允許登錄。";
$MESS["CONFIG_OTP_SECURITY_INFO2"] = "<br/>要啟用兩步身份驗證，用戶必須將Bitrix24 OTP應用程序安裝到其手機上。該應用程序可以從AppStore或GooglePlay下載。<br/> <br/>
沒有合適的移動設備的用戶必須配備特殊的硬件 -  eToken通行證。您可以從中獲得多種供應商，例如：
<a target = _blank href= \"http://www.safenet-inc.com/multi-factor-authentication/authenticators/pki-usb-authentication/Etoken-pro/ etoken-pro/ \ \"> www.safenet- inc.com < /a>，
<a target=_blank href= \"http://www.authguard.com/etoken-pass.asp.asp \"> www.authguard.com </a>。
在<a target=_blank href = \"https://www.google.com/?q=buy+Eteken+passs＆spell = safe = = = buy+eToken+passe+\"> google </google </google </a上查找其他供應商>。
<br/> <br/>
另外，您可以為某些用戶禁用兩步身份驗證。但是，如果其中一個用戶的登錄和密碼被盜，這將增加未經授權訪問您的Bitrix24的風險。作為管理員，您可以在用戶配置文件中為用戶禁用兩步身份驗證。";
$MESS["CONFIG_USERS2"] = "用戶：";
$MESS["CRM_DEMO_EVENT_12_EVENT_NAME"] = "更改字段\“用戶\”";
$MESS["CRM_DEMO_EVENT_15_EVENT_NAME"] = "更改字段\“用戶\”";
$MESS["CRM_GADGET_NEW_COMPANY_TITLE"] = "新公司";
$MESS["CT_BST_SEARCH_HINT"] = "查找人，文件等";
$MESS["DEMO_FEATURE_1"] = "無限用戶";
$MESS["DEMO_INFO_1"] = "用戶";
$MESS["DEMO_INFO_DESC_1"] = "如果您在試用期間邀請了12個以上的用戶，則只有前12位用戶將能夠訪問您的Bitrix24帳戶。如果要還原對所有用戶的訪問權限，則需要<a class = \"link \" href= \"/settings/license_all.php \"> upgrade </a>。";
$MESS["DIRECTION_GROUP_DESC"] = "管理。";
$MESS["EC_ADD_MEMBERS_FROM_STR"] = "來自組織結構";
$MESS["EC_ADD_MEMBERS_FROM_STR_TITLE"] = "添加組織結構的活動參與者";
$MESS["EC_COMPANY_CALENDAR"] = "組織日曆";
$MESS["EC_COMPANY_STRUCTURE"] = "組織的結構";
$MESS["EC_DESTINATION_1"] = "添加用戶，組或部門";
$MESS["EC_NO_COMPANY_STRUCTURE"] = "組織結構缺失。";
$MESS["EMPLOYEES_GROUP_DESC"] = "所有用戶，在門戶網站上註冊。";
$MESS["FIELD_empl_num"] = "用戶數";
$MESS["GENERAL_FORUM_DESCRIPTION"] = "組織用戶的公共論壇。在這裡討論您的業務並交流意見。";
$MESS["GENERAL_FORUM_MESSAGE_BODY"] = "注意力！組織用戶現在可以在其私有區域維護其文件存儲。

您將找到有關文件存儲管理的詳細信息以及將存儲映射到“幫助”部分中的網絡驅動器的方法：\“我的個人資料 - 文件\”。

如果您對文件存儲配置有任何疑問，請使用支持請求表格將您的請求發送給TechSupport工程師。\ \”";
$MESS["HONOR_FORM_2"] = "選擇榮譽用戶";
$MESS["IFS_EF_Blog"] = "用戶和組博客提要";
$MESS["IFS_EF_Gallery"] = "用戶照片庫";
$MESS["IFS_EF_staff"] = "人員;參考文件;組織的結構;尊敬的用戶";
$MESS["IM_CL_STRUCTURE"] = "組織";
$MESS["IM_CL_USER_B24"] = "成員";
$MESS["IM_CTL_CHAT_STRUCTURE"] = "組織的結構";
$MESS["IM_C_ABOUT_CHAT"] = "僅向受邀用戶提供私人聊天。 ＃br ## br＃此聊天非常適合涉及和關注某些人的業務討論。 ＃br ## br＃不僅是您的同事，還可以邀請您參加此聊天；向您的客戶，合作夥伴和其他使用Bitrix24的公司發送邀請。要添加新的聊天會員，請輸入第一個和姓氏，電子郵件或其顯示名稱。 ＃br ## br＃完成＃profile_start＃您的個人資料＃profile_end＃讓其他人找到您。";
$MESS["IM_C_ABOUT_OPEN"] = "大家都可以看到一般聊天。 ＃br ## br＃使用此聊天來討論對公司中任何人都很重要的主題。＃br ## br＃在創建新聊天時，將通知發送到＃chat_start＃常規聊天＃chat_end＃。然後，您的同事可以查看新聊天的歷史並加入它。＃br ## br＃您的初始消息對於激發他們對新聊天的興趣至關重要。";
$MESS["IM_C_ABOUT_PRIVATE"] = "您和您的聯繫人只能看到人與人之間的聊天。 ＃br ## br＃找到一個您想通過其名稱，職位或部門聊天的人。 ＃br ## br＃您也可以使用Bitrix24與任何客戶，合作夥伴或其他人聊天。通過他們認為他們使用的名稱，電子郵件或顯示名稱找到它們。 ＃BR ## BR＃完成＃profile_start＃您的個人資料＃profile_end＃幫助其他人找到您。";
$MESS["IM_M_CALL_ST_TRANSFER"] = "致電重定向：等待答案";
$MESS["IM_M_CALL_ST_TRANSFER_1"] = "調用重定向：用戶無法回答";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_MESSAGE"] = "<p>我們注意到您已經嘗試為您的組織創建一個域名，但從未完成註冊。</p>

<p>如果您需要幫助，請打開電子郵件設置頁面閱讀以獲取設置電子郵件服務並完成BITRIX24域註冊的詳細說明。</p>

<p>如果您想查看通常如何完成，我們有許多使用電子郵件域供您查看的示例。這些示例將揭示域註冊背後的魔術，創建一個郵箱並使用Bitrix24使用整個內容。</p>

<p> <a href= \"#learnmore_link# \ \">了解更多</a> </p>

<p>如果您仍然有疑問，請聯繫我們的<a href= \"#support_link# \"> helpdesk </a>。</p>
";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_MESSAGE"] = "<p>您已經註冊了域並創建了郵箱。但是，您的員工或用戶仍然不知道他們可以在您的域中創建自己的郵箱。</p>

<p>分享您的知識，並幫助他們學習創建自己的郵箱，或在其Bitrix24頁面上註冊郵箱。</p>

<p> <a href= \"#learnmore_link# \ \">如何為員工或用戶註冊郵箱</a> </p>

<p>如果您仍然有疑問，請聯繫我們的<a href= \"#support_link# \"> helpdesk </a>。</p>
";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_NAME"] = "在域中創建用戶郵箱";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_SUBJECT"] = "為您的用戶提供的公司電子郵件";
$MESS["INTRANET_MAILDOMAIN_NOREG_MESSAGE"] = "<p>您將為您的公司註冊一個域名，但從外觀上找不到好名字。</p>

<p>我們有許多使用電子郵件域供您查看的示例。使用示例，您將學習如何創建郵箱並與Bitrix24一起使用。</p>

<p> <a href= \"#learnmore_link# \ \">如何為員工註冊郵箱</a> </p>

<p>如果您仍然有疑問，請聯繫我們的<a href= \"#support_link# \"> helpdesk </a>。</p> \'";
$MESS["INTRANET_OTP_MANDATORY_TITLE"] = "您的業​​務數據額外安全性";
$MESS["INTRANET_RATING_USER_SUBORDINATE_DESC"] = "該值基於由\“組織結構\”規則計算得出的數據。";
$MESS["INTRANET_RATING_USER_SUBORDINATE_NAME"] = "基於組織結構的額外選票";
$MESS["INTRANET_TAB_USER_STRUCTURE"] = "組織";
$MESS["INTRANET_USER_ADD_DESC"] = "＃email_to＃ - 新用戶的電子郵件地址
＃鏈接＃ -Intranet URL";
$MESS["INTRANET_USER_ADD_NAME"] = "添加用戶";
$MESS["INTRANET_USER_INVITATION_DESC"] = "＃email_to＃ - 受邀人員的電子郵件地址
＃鏈接＃ - 新用戶激活鏈接";
$MESS["INTRANET_USER_INVITATION_NAME"] = "邀請人";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_TITLE"] = "組織脈衝";
$MESS["INTRANET_USTAT_COMPANY_HELP_ACTIVITY"] = "活動索引：用戶在給定時間段的所有Intranet功能上的所有活動的綜合。該索引顯示了用戶如何從事各種工具的工作。";
$MESS["INTRANET_USTAT_COMPANY_HELP_GENERAL"] = "組織脈衝目前是該門戶網站中用戶活動的總體指標（上一個小時內所有用戶的綜合）。";
$MESS["INTRANET_USTAT_COMPANY_HELP_INVOLVEMENT"] = "參與級別：這是一個關鍵指標，它顯示了用戶對Bitrix24的功能的熟悉程度。它顯示了組織中至少有四分之一提供的工具的百分比。";
$MESS["INTRANET_USTAT_COMPANY_HELP_RATING"] = "評級取決於所有使用在給定時間段內執行至少1個操作的用戶的個人活動指數的平均值。";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_CRM"] = "功能用法：CRM";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_DISK"] = "功能用法：驅動器";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_IM"] = "功能用法：聊天";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_LIKES"] = "功能用法：喜歡";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_MOBILE"] = "功能用法：移動應用";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_SOCNET"] = "功能用法：社交網絡";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_TASKS"] = "功能用法：任務";
$MESS["INTRANET_USTAT_SECTION_CRM_HELP_INVOLVEMENT"] = "在給定時間段內活躍於CRM的用戶的百分比。這個數字可能會受到訪問CRM的用戶數量的限制。";
$MESS["INTRANET_USTAT_SECTION_DISK_HELP_INVOLVEMENT"] = "實施Bitrix24.Drive的用戶百分比。";
$MESS["INTRANET_USTAT_SECTION_IM_HELP_INVOLVEMENT"] = "顯示在給定時間段發送即時消息或進行視頻/音頻通話的用戶百分比。";
$MESS["INTRANET_USTAT_SECTION_LIKES_HELP_INVOLVEMENT"] = "在給定時間段內使用\“ like \'函數的用戶百分比。";
$MESS["INTRANET_USTAT_SECTION_MOBILE_HELP_INVOLVEMENT"] = "在給定時間段內使用移動應用程序的用戶百分比。";
$MESS["INTRANET_USTAT_SECTION_SOCNET_HELP_INVOLVEMENT"] = "顯示在給定時間段內使用社交網絡功能的用戶的百分比。";
$MESS["INTRANET_USTAT_SECTION_TASKS_HELP_INVOLVEMENT"] = "在給定時間段內使用任務的用戶百分比。";
$MESS["INTRANET_USTAT_TELLABOUT_SOCNET_TITLE"] = "勞動力團結";
$MESS["INTRANET_USTAT_TOGGLE_COMPANY"] = "組織";
$MESS["INTRANET_USTAT_USER_ACTIVITY_COMPANY_TITLE"] = "組織<br>平均水平";
$MESS["INTRANET_USTAT_USER_GRAPH_COMPANY"] = "組織平均水平";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY_COMPANY"] = "您的組織使用指定時間段的七個可用工具之一在Bitrix24上進行的操作的平均值。<br> <br> <b>使用組織的平均值，查看您的活動如何比整個組織的活動更好。";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY_DEPT"] = "您的部門使用指定時間段的七個可用工具之一在Bitrix24上進行的操作的平均值。<br> <br> <b>使用該部門的平均值來查看您的活動比其他活動更好。";
$MESS["INTRANET_USTAT_USER_HELP_RATING"] = "您在<b>活動等級中的位置</b>摘要列出了在指定時間段內至少一次使用Bitrix24的所有用戶。";
$MESS["INTRANET_USTAT_WIDGET_ACTIVITY_HELP"] = "當前的組織活動水平（在最後一個小時內所有用戶的綜合）";
$MESS["INTRANET_USTAT_WIDGET_INVOLVEMENT_HELP"] = "當前的用戶參與。這顯示了當今所有用戶的百分比，這些用戶在Intranet中使用了至少四個不同的工具。";
$MESS["INTRANET_USTAT_WIDGET_TITLE"] = "組織脈衝";
$MESS["INTR_ABSC_TPL_EDIT_ENTRIES"] = "用戶管理";
$MESS["INTR_ABSC_TPL_IMPORT"] = "導入用戶";
$MESS["INTR_ABSENCE_USER"] = "選擇缺席的人 *";
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK"] = "您可以將用戶導出為Microsoft Outlook的聯繫人";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV_TITLE"] = "將用戶註冊表與軟件和硬件支持CardDav（iPhone，iPad等）同步";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK_TITLE"] = "導出用戶列表為Outlook聯繫人";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_USER_ACTIVE"] = "用戶";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_USER_ADD"] = "邀請用戶";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_USER_INVITE"] = "邀請用戶";
$MESS["INTR_CONFIRM_DELETE"] = "邀請將不可撤銷地刪除。\\ n \\您確定要刪除用戶嗎？";
$MESS["INTR_CONFIRM_FIRE"] = "用戶將無法登錄到門戶，將不會在組織結構中顯示。但是，他或她的所有個人數據（文件，消息，任務等）都將保持完整。";
$MESS["INTR_CONFIRM_RESTORE"] = "用戶將能夠登錄到門戶網站，並將顯示在組織結構中。\\ n \\ nare您確定要允許此用戶訪問嗎？";
$MESS["INTR_EMP_CANCEL_TITLE"] = "取消用戶選擇";
$MESS["INTR_EMP_SEARCH"] = "搜索用戶";
$MESS["INTR_EMP_SUBMIT_TITLE"] = "選擇選定的用戶";
$MESS["INTR_EMP_WINDOW_TITLE"] = "選擇用戶";
$MESS["INTR_IBLOCK_TOP_SECTION_WARNING"] = "組織結構可能僅包含一個頂級部分。";
$MESS["INTR_INSTALL_RATING_RULE"] = "根據組織結構計算額外的投票";
$MESS["INTR_ISBN_TPL_FILTER_ALL"] = "對於整個組織";
$MESS["INTR_ISE_TPL_NOTE_NULL"] = "搜索與任何用戶都不匹配。";
$MESS["INTR_ISIN_TPL_ALL"] = "對於整個組織";
$MESS["INTR_ISL_TPL_NOTE_NULL"] = "您的搜索與任何用戶都不匹配。";
$MESS["INTR_IS_TPL_ACTION_INVITE"] = "邀請用戶";
$MESS["INTR_IS_TPL_OUTLOOK"] = "導出用戶到Outlook";
$MESS["INTR_IS_TPL_OUTLOOK_TITLE"] = "導出用戶列表為Outlook聯繫人";
$MESS["INTR_IS_TPL_SEARCH"] = "查找用戶";
$MESS["INTR_IS_TPL_SEARCH_DEPARTMENT"] = "在此部門中找到用戶";
$MESS["INTR_MAIL_DESCR_B24"] = "您的業​​務免費電子郵件服務器！用於電子郵件，防病毒和反垃圾郵件的無限存儲，以保持收件箱清潔。將您的公司電子郵件遷移到Bitrix24！在Bitrix24上在您的業務域上創建自己的郵箱，或從其他電子郵件服務附加郵箱。";
$MESS["INTR_MAIL_DESCR_BOX"] = "準備將電子郵件服務器用於您的業務。享受無限儲存，防病毒和反垃圾郵件。在您的公司域或附加到現有電子郵件服務上創建郵箱。";
$MESS["INTR_MAIL_DOMAIN_PUBLIC"] = "允許用戶在新域上註冊郵箱";
$MESS["INTR_MAIL_INP_PUBLIC_DOMAIN"] = "用戶可以在此域中註冊郵箱";
$MESS["INTR_MAIL_MANAGE"] = "配置用戶郵箱";
$MESS["INTR_MAIL_MANAGE_HINT"] = "為組織中的每個用戶創建一個郵箱。使用此接口管理公司郵箱：創建，附加或刪除郵箱以及更改郵箱密碼。";
$MESS["INTR_MAIL_MANAGE_SETUP_ALLOW_CRM"] = "會員可以將郵箱連接到CRM CRM";
$MESS["INTR_MAIL_NODOMAIN_USER_INFO"] = "請聯繫您的Intranet管理員以附加公司域。";
$MESS["INTR_OPTION_IBLOCK_CALENDAR"] = "用戶日曆的信息塊";
$MESS["INTR_OPTION_IBLOCK_STRUCTURE"] = "組織結構信息塊";
$MESS["INTR_PROP_EMP_TITLE"] = "鏈接到用戶";
$MESS["ISL_TPL_NOTE_NULL"] = "您的搜索與任何用戶都不匹配。";
$MESS["ISL_WORK_CITY"] = "城市";
$MESS["ISL_WORK_COUNTRY"] = "國家";
$MESS["ISL_WORK_LOGO"] = "組織徽標";
$MESS["ISV_B24_INVITE"] = "邀請用戶";
$MESS["ISV_EMPLOYEES"] = "用戶";
$MESS["ISV_EMP_COUNT_1"] = "＃num＃用戶";
$MESS["ISV_EMP_COUNT_2"] = "＃num＃用戶";
$MESS["ISV_EMP_COUNT_3"] = "＃num＃用戶";
$MESS["ISV_EMP_COUNT_4"] = "＃num＃用戶";
$MESS["ISV_EMP_COUNT_21"] = "＃num＃用戶";
$MESS["ISV_EMP_COUNT_22"] = "＃num＃用戶";
$MESS["ISV_EMP_COUNT_23"] = "＃num＃用戶";
$MESS["ISV_EMP_COUNT_24"] = "＃num＃用戶";
$MESS["ISV_EMP_COUNT_31"] = "＃num＃用戶";
$MESS["ISV_EMP_COUNT_32"] = "＃num＃用戶";
$MESS["ISV_EMP_COUNT_33"] = "＃num＃用戶";
$MESS["ISV_EMP_COUNT_34"] = "＃num＃用戶";
$MESS["ISV_EMP_COUNT_41"] = "＃num＃用戶";
$MESS["ISV_EMP_COUNT_42"] = "＃num＃用戶";
$MESS["ISV_EMP_COUNT_43"] = "＃num＃用戶";
$MESS["ISV_EMP_COUNT_44"] = "＃num＃用戶";
$MESS["ISV_EMP_COUNT_MUL"] = "＃num＃用戶";
$MESS["ISV_EMP_LIST"] = "用戶列表";
$MESS["ISV_ERROR_dpt_not_empty"] = "在刪除該部門之前，您必須將所有部門的用戶重新安置。";
$MESS["ISV_add_emp"] = "添加用戶";
$MESS["ISV_change_department"] = "<b> #name＃</b>已轉移到<b> #department＃</b>。";
$MESS["ISV_confirm_change_department_0"] = "傳輸＃emp_name＃到＃dpt_name＃？";
$MESS["ISV_confirm_change_department_1"] = "將＃emp_name＃分配給＃dpt_name＃？";
$MESS["ISV_confirm_delete_department"] = "您確定要刪除該部門嗎？這將移動所有基礎細分和用戶。";
$MESS["ISV_confirm_set_head"] = "分配＃emp_name＃作為＃dpt_name＃dimist的負責人？";
$MESS["ISV_set_department_head"] = "<b> #name＃</b>已被任命為<b> #department＃</b> division的負責人。";
$MESS["I_NEW_USER_MENTION"] = "在一條有關新添加的用戶＃標題＃的消息中提到了您。";
$MESS["I_NEW_USER_MENTION_F"] = "在一條有關新添加的用戶＃標題＃的消息中提到了您。";
$MESS["I_NEW_USER_MENTION_M"] = "在一條有關新添加的用戶＃標題＃的消息中提到了您。";
$MESS["I_NEW_USER_TITLE"] = "添加了新用戶";
$MESS["I_NEW_USER_TITLE_LIST"] = "新用戶";
$MESS["I_NEW_USER_TITLE_SETTINGS"] = "新用戶";
$MESS["LICENSE_CRM_FEATURE_16"] = "CRM訪問日誌";
$MESS["LICENSE_MORE_USERS"] = "更多用戶";
$MESS["LICENSE_TEL_FEATURE_11"] = "用戶擴展";
$MESS["LICENSE_TEL_FEATURE_16"] = "同時致電所有可用用戶";
$MESS["LM_POPUP_CHECK_STRUCTURE"] = "所有部門和細分用戶";
$MESS["LM_POPUP_TAB_STRUCTURE"] = "用戶和分區";
$MESS["MEETING_DESCRIPTION"] = "我們將不得不舉行會議，討論在組織中的Intranet門戶網站的部署。";
$MESS["MENU_COMPANY"] = "組織";
$MESS["MENU_COMPANY_CALENDAR"] = "組織日曆";
$MESS["MENU_COMPANY_SECTION"] = "組織";
$MESS["MENU_EMPLOYEE"] = "用戶";
$MESS["MENU_STRUCTURE"] = "組織的結構";
$MESS["ML_COL_DESC_0"] = "用戶照片集";
$MESS["ML_COL_NAME_0"] = "用戶照片";
$MESS["MPF_DESTINATION_1"] = "添加用戶，組或部門";
$MESS["MPF_DESTINATION_3"] = "全部用戶";
$MESS["MP_ADD_APP_SCOPE_DEPARTMENT"] = "組織的結構";
$MESS["PP_USER_CONDITION_SUBORDINATE_NAME"] = "組織的結構";
$MESS["PP_USER_CONDITION_SUBORDINATE_TEXT"] = "根據組織結構計算用戶權威的額外投票。";
$MESS["SERVICES_MENU_NOVICE"] = "適用於新用戶";
$MESS["SERVICES_ORG_LIST"] = "我的組織";
$MESS["SERVICES_TITLE"] = "新用戶的信息";
$MESS["SERVICE_COMPANY_STRUCTURE"] = "組織的結構";
$MESS["SOCNET_CONFIRM_DELETE"] = "邀請將不可撤銷地刪除。

您確定要刪除用戶嗎？";
$MESS["SOCNET_CONFIRM_FIRE"] = "用戶將無法登錄到門戶，將不會在組織結構中顯示。但是，他或她的所有個人數據（文件，消息，任務等）將保持完整。

您確定要關閉此用戶的訪問嗎？";
$MESS["SOCNET_CONFIRM_FIRE1"] = "用戶將無法登錄到門戶，將不會在組織結構中顯示。但是，他或她的所有個人數據（文件，消息，任務等）都將保持完整。";
$MESS["SOCNET_CONFIRM_RECOVER"] = "用戶將能夠登錄到該門戶，並將顯示在組織結構中。

您確定要為此用戶打開訪問嗎？";
$MESS["SOCNET_CONFIRM_RECOVER1"] = "用戶將能夠登錄到門戶網站，並將顯示在組織結構中。\\ n \\ nare您確定要允許此用戶訪問嗎？";
$MESS["SONET_GCE_T_ADD_EMPLOYEE"] = "添加用戶";
$MESS["SONET_GCE_T_DEST_TITLE_EMPLOYEE"] = "邀請用戶：";
$MESS["SONET_GCE_T_INVITATION_EMPLOYEES"] = "邀請用戶";
$MESS["SONET_GCE_T_USER_INTRANET"] = "組織用戶：";
$MESS["SONET_GL_DESTINATION_G2"] = "給所有用戶";
$MESS["SONET_GROUP_DESCRIPTION_3"] = "運動生活";
$MESS["SONET_LIVEFEED_RENDERPARTS_USER_ALL"] = "給所有用戶";
$MESS["SONET_TASK_DESCRIPTION_2"] = "邀請新用戶加入Intranet門戶";
$MESS["SONET_TASK_TITLE_2"] = "邀請新用戶";
$MESS["SONET_UP1_WORK_POSITION"] = "在組織中的位置";
$MESS["STATE_FORM_2"] = "選擇已更改狀態的用戶";
$MESS["SUBSCRIBE_POSTING_BODY"] = "你好！我們上傳了新的假期照片。";
$MESS["SUBSCRIBE_POSTING_SUBJECT"] = "我們的照片！";
$MESS["TEMPLATE_DESCRIPTION"] = "該模板旨在強調Intranet的社會方面，並將傳統的創作和生產力工具結合在促進社會交流的情況下。社會Intranet佈局是一種高度直觀的生產力提升，需要最少的採用和取向時間。";
$MESS["TITLE1"] = "組織的結構";
$MESS["TITLE2"] = "組織的結構";
$MESS["UF_PUBLIC"] = "在外部的所有人都可以看到";
$MESS["WD_USER"] = "用戶文檔";
$MESS["W_IB_ABSENCE_2_PREV"] = "商務前往公司分支機構。";
$MESS["W_IB_CLIENTS_TAB1"] = "edit1-＃ - 客戶端 - ， - 名稱 - ＃ - *公司名稱 - ， -  property_person-＃ - 董事 - ， - ， -  property_phone-phone-＃ -  phese-thice-;  -   -   - ";
$MESS["authprov_check_d"] = "所有部門用戶";
$MESS["authprov_check_dr"] = "所有部門和細分用戶";
$MESS["authprov_name"] = "用戶和分區";
$MESS["iblock_dep_name1"] = "我的組織";
$MESS["main_opt_user_comp_logo"] = "組織徽標";
$MESS["main_opt_user_comp_name"] = "機構名稱";
$MESS["main_site_name"] = "我的組織";
$MESS["wiz_company_logo"] = "組織徽標：";
$MESS["wiz_company_name"] = "機構名稱：";
$MESS["wiz_demo_structure"] = "安裝樣本組織結構";
$MESS["wiz_slogan"] = "我的組織";
