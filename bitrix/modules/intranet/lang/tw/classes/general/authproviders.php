<?php
$MESS["authprov_check_d"] = "所有部門的員工";
$MESS["authprov_check_dr"] = "所有部門和子部門員工";
$MESS["authprov_group"] = "部門";
$MESS["authprov_group_extranet"] = "外部";
$MESS["authprov_name"] = "部門";
$MESS["authprov_name_out_group"] = "部門";
$MESS["authprov_name_out_user1"] = "員工和主管";
$MESS["authprov_panel_group"] = "從結構中選擇";
$MESS["authprov_panel_last"] = "最後的";
$MESS["authprov_panel_search"] = "搜尋";
$MESS["authprov_panel_search_text"] = "輸入用戶登錄名，或名稱或部門名稱。";
