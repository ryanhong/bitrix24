<?php
$MESS["WS_OUTLOOK_APP_COMMENT"] = "自動生成";
$MESS["WS_OUTLOOK_APP_DESC"] = "獲取Microsoft Outlook的密碼，以設置日曆，聯繫人或任務的同步。";
$MESS["WS_OUTLOOK_APP_OPTIONS_CAPTION"] = "連接";
$MESS["WS_OUTLOOK_APP_SYSCOMMENT"] = "與Microsoft Outlook同步";
$MESS["WS_OUTLOOK_APP_SYSCOMMENT_TYPE"] = "與Microsoft Outlook同步：＃類型＃";
$MESS["WS_OUTLOOK_APP_TITLE"] = "Microsoft Outlook服務";
$MESS["WS_OUTLOOK_APP_TITLE_OPTION"] = "聯繫人，日曆，任務";
$MESS["WS_OUTLOOK_APP_TYPE_calendar"] = "日曆";
$MESS["WS_OUTLOOK_APP_TYPE_contacts"] = "聯繫人";
$MESS["WS_OUTLOOK_APP_TYPE_tasks"] = "任務";
