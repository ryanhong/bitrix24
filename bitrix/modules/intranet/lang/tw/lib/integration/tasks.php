<?php
$MESS["SONET_INSTALL_APP_TASK_DESCRIPTION"] = "將BitRix24應用程序安裝到您的移動設備並保持連接！

在關閉瀏覽器時，將桌面應用程序安裝到計算機（Mac，Windows或Linux）與同事通信。

[url = https：//www.bitrix24.com/features/mobile.php]下載bitrix24應用程序[/url]
";
$MESS["SONET_INSTALL_APP_TASK_TITLE"] = "下載Bitrix24應用程序";
$MESS["SONET_INVITE_TASK_DESCRIPTION"] = "邀請同事參加您的Bitrix24。

＃anchor_invite＃邀請＃anchor_end＃
";
$MESS["SONET_INVITE_TASK_DESCRIPTION_V2"] = "邀請同事參加您的Bitrix24。

[url =/company/vis_structure.php]邀請[/url]
";
$MESS["SONET_INVITE_TASK_TITLE"] = "邀請同事";
$MESS["SONET_TASK_DESCRIPTION_V2"] = "完成您的個人資料，上傳您的用戶圖像並提供您的個人信息。

＃Anchor_edit_profile＃繼續進行您的個人資料＃Anchor_end＃
";
$MESS["SONET_TASK_TITLE"] = "填寫個人資料";
