<?php
$MESS["intranet_otp_push_code"] = "您的代碼符號：＃代碼＃";
$MESS["intranet_push_otp_notification1"] = "我們已經檢測到登錄您的Bitrix24的嘗試。[BR] [BR]登錄確認代​​碼：[B] #code＃[/b]。切勿將此代碼傳遞給第三方或在其他網站上輸入。[br] [br]是您要求代碼的人，您無需擔心。否則，請立即在設置中更改密碼。[BR] [BR] IP地址：＃IP＃[BR]設備：＃USER_AGENT＃";
