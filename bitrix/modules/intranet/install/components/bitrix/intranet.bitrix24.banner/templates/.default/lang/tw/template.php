<?php
$MESS["B24_BANNER_DOWNLOAD_LINUX_DEB"] = "對於Linux Deb";
$MESS["B24_BANNER_DOWNLOAD_LINUX_RPM"] = "對於Linux RPM";
$MESS["B24_BANNER_MESSENGER_TITLE"] = "桌面客戶端";
$MESS["B24_BANNER_MOBILE_APPSTORE_URL"] = "https://itunes.apple.com/app/bitrix24/id561683423";
$MESS["B24_BANNER_MOBILE_GOOGLE_PLAY_URL"] = "https://play.google.com/store/apps/details?id=com.bitrix24.android";
$MESS["B24_BANNER_MOBILE_TITLE"] = "移動應用程序";
