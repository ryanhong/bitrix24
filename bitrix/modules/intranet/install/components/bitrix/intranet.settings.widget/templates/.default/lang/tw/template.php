<?php
$MESS["INTRANET_SETTINGS_CONFIGURE_CUTAWAY_SITE_BUTTON"] = "編輯著陸頁";
$MESS["INTRANET_SETTINGS_CONFIGURE_REQUISITE_BUTTON"] = "編輯公司詳細信息";
$MESS["INTRANET_SETTINGS_WIDGET_BRANCHES"] = "分支";
$MESS["INTRANET_SETTINGS_WIDGET_BRANCH_LIST"] = "你的樹枝";
$MESS["INTRANET_SETTINGS_WIDGET_COPIED_POPUP"] = "鏈接複製";
$MESS["INTRANET_SETTINGS_WIDGET_COPY_LINK_BUTTON"] = "複製鏈接";
$MESS["INTRANET_SETTINGS_WIDGET_CREATE_LANDING"] = "創建著陸頁";
$MESS["INTRANET_SETTINGS_WIDGET_CREATE_LANDING_ERROR"] = "網站已創建但未發布。";
$MESS["INTRANET_SETTINGS_WIDGET_FILIAL_ABOUT"] = "細節";
$MESS["INTRANET_SETTINGS_WIDGET_FILIAL_NETWORK"] = "分支網絡";
$MESS["INTRANET_SETTINGS_WIDGET_FILIAL_NETWORK_UNAVAILABLE"] = "你沒有任何分支機構";
$MESS["INTRANET_SETTINGS_WIDGET_FILIAL_SETTINGS"] = "編輯";
$MESS["INTRANET_SETTINGS_WIDGET_LINK_COPIED_POPUP"] = "鏈接複製";
$MESS["INTRANET_SETTINGS_WIDGET_MAIN_BRANCH"] = "您的主Bitrix24";
$MESS["INTRANET_SETTINGS_WIDGET_ORDER_PARTNER_LINK"] = "訂單開發";
$MESS["INTRANET_SETTINGS_WIDGET_SECONDARY_BRANCH"] = "分支";
$MESS["INTRANET_SETTINGS_WIDGET_SECTION_MIGRATION_TITLE"] = "導入到Bitrix24";
$MESS["INTRANET_SETTINGS_WIDGET_SECTION_REQUISITE_SITE_TITLE"] = "登陸頁面";
$MESS["INTRANET_SETTINGS_WIDGET_SECTION_REQUISITE_TITLE"] = "公司詳細信息";
$MESS["INTRANET_SETTINGS_WIDGET_SECTION_SECURITY_TITLE"] = "安全";
$MESS["INTRANET_SETTINGS_WIDGET_SECTION_SETTINGS_TITLE"] = "設定";
$MESS["INTRANET_SETTINGS_WIDGET_SUPPORT_BUTTON"] = "支持";
$MESS["INTRANET_SETTINGS_WIDGET_WHERE_TO_BEGIN_LINK"] = "快速開始";
