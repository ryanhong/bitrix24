<?php
$MESS["EC_IBLOCK_ID_EMPTY"] = "未選擇信息塊";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "信息塊模塊未安裝。";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "Intranet模塊未安裝。";
$MESS["INAF_F_DESCRIPTION"] = "描述";
$MESS["INAF_F_FLOOR"] = "地面";
$MESS["INAF_F_ID"] = "ID";
$MESS["INAF_F_NAME"] = "標題";
$MESS["INAF_F_PHONE"] = "電話";
$MESS["INAF_F_PLACE"] = "座位";
$MESS["INAF_MEETING_NOT_FOUND"] = "沒有找到會議室。";
$MESS["INTASK_C25_CONFLICT1"] = "\“＃res1＃\”和\“＃res2＃\”之間＃時間＃期間的時間衝突";
$MESS["INTASK_C25_CONFLICT2"] = "\“＃res1＃\”和\“＃res2＃\”之間＃時間＃期間的時間衝突";
$MESS["INTASK_C36_PAGE_TITLE"] = "會議室時間表";
$MESS["INTASK_C36_PAGE_TITLE1"] = "會議室預訂";
$MESS["INTR_IRMM_NAME_TEMPLATE_DEFAULT"] = "＃NOBR ##名稱＃#last_name ##/nobr＃";
$MESS["INTS_NO_IBLOCK_PERMS"] = "您無權查看會議室信息塊。";
