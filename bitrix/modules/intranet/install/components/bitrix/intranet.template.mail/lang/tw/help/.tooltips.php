<?php
$MESS["PATH_TO_BASKET_TIP"] = "指定購物車頁面的路徑。您可以使用<b>購物車</b>組件創建此頁面。";
$MESS["PATH_TO_ORDER_TIP"] = "訂購頁面的路徑。如果頁面在當前目錄中，則只能指定文件名。";
