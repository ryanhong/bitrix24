<?php
$MESS["INTRANET_BITRIX24"] = "Bitrix24";
$MESS["INTRANET_CHANGE_NOTIFY_SETTINGS"] = "編輯通知設置";
$MESS["INTRANET_INVITE_ACCEPT"] = "接受邀請";
$MESS["INTRANET_INVITE_FOOTER"] = "這是自動生成的通知。您收到此電子郵件是因為邀請您加入Bitrix24帳戶。您可以在接受邀請後立即編輯通知設置。";
$MESS["INTRANET_INVITE_IMG_LINK"] = "http://www.bitrix24.com/images/mail/tutorials/tools_en.png";
$MESS["INTRANET_INVITE_INFO_TEXT"] = "＃span_start＃bitrix24是一個領先的協作平台＃span_end＃全球12,000,000多家公司使用的＃。";
$MESS["INTRANET_INVITE_INFO_TEXT3"] = "什麼是Bitrix24？";
$MESS["INTRANET_INVITE_MORE"] = "了解更多";
$MESS["INTRANET_INVITE_TEXT"] = "＃名稱＃邀請您到＃block_start＃bitrix＃block_middle＃24＃block_end＃";
$MESS["INTRANET_MAIL_EVENTS_UNSUBSCRIBE"] = "取消訂閱";
$MESS["INTRANET_MAIL_TITLE_IM_NEW_MESSAGE"] = "來自＃名稱＃的新消息";
$MESS["INTRANET_MAIL_TITLE_IM_NEW_MESSAGE_GROUP"] = "來自＃名稱＃的新消息";
$MESS["INTRANET_MAIL_TITLE_IM_NEW_NOTIFY"] = "＃名稱＃的新通知";
$MESS["INTRANET_OPEN"] = "看法";
$MESS["INTRANET_OPEN_NOTIFY"] = "查看通知";
$MESS["INTRANET_SITE_LINK"] = "https://www.bitrix24.com/";
