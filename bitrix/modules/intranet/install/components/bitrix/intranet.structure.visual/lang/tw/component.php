<?php
$MESS["ISV_ERROR_dpt_not_empty"] = "在刪除部門之前，您必須搬遷所有部門的員工。";
$MESS["ISV_change_department"] = "<b> #name＃</b>已轉移到<b> #department＃</b>。";
$MESS["ISV_move_department"] = "<b> #department＃</b>部門從屬於<b> #department_to＃</b>。";
$MESS["ISV_set_department_head"] = "<b> #Name＃</b>已被任命為<b> #department＃</b>部門的負責人。";
