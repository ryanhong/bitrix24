<?php
$MESS["INTL_IBLOCK"] = "信息塊";
$MESS["INTL_IBLOCK_TYPE"] = "信息塊類型";
$MESS["INTL_MEETING_ID"] = "會議室ID";
$MESS["INTL_MEETING_VAR"] = "會議室ID的變量";
$MESS["INTL_PAGE_VAR"] = "頁面變量";
$MESS["INTL_PATH_TO_MEETING"] = "預訂室時間表頁面";
$MESS["INTL_PATH_TO_MEETING_LIST"] = "會議室預訂主頁";
$MESS["INTL_SET_NAVCHAIN"] = "設置麵包屑";
$MESS["INTL_USERGROUPS_MODIFY"] = "用戶組允許編輯會議室時間表";
$MESS["INTL_VARIABLE_ALIASES"] = "可變別名";
