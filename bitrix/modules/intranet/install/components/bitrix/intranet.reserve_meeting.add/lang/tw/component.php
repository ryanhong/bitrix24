<?php
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "信息塊模塊未安裝。";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "Intranet模塊未安裝。";
$MESS["INAF_F_DESCRIPTION"] = "描述";
$MESS["INAF_F_FLOOR"] = "地面";
$MESS["INAF_F_ID"] = "ID";
$MESS["INAF_F_NAME"] = "標題";
$MESS["INAF_F_PHONE"] = "電話";
$MESS["INAF_F_PLACE"] = "座位";
$MESS["INAF_MEETING_NOT_FOUND"] = "沒有找到會議室。";
$MESS["INTASK_C36_EMPTY_NAME"] = "會議室的頭銜是空的。";
$MESS["INTASK_C36_NO_PERMS2CREATE"] = "您無權創建新的會議室。";
$MESS["INTASK_C36_PAGE_TITLE"] = "創建預訂室";
$MESS["INTASK_C36_PAGE_TITLE1"] = "會議室預訂";
$MESS["INTASK_C36_PAGE_TITLE2"] = "編輯會議室";
$MESS["INTASK_C36_SHOULD_AUTH"] = "在創建新的會議室之前，請授權。";
$MESS["INTS_NO_IBLOCK_PERMS"] = "您無權查看會議室信息塊。";
