<?php
$MESS["INTR_MAIL_MANAGE_ADD_MAILBOX"] = "添加郵箱";
$MESS["INTR_MAIL_MANAGE_ADD_MAILBOX2"] = "添加";
$MESS["INTR_MAIL_MANAGE_CANCEL_BTN"] = "取消";
$MESS["INTR_MAIL_MANAGE_CONNECT_BTN"] = "連接";
$MESS["INTR_MAIL_MANAGE_CONNECT_TITLE"] = "郵箱集成";
$MESS["INTR_MAIL_MANAGE_CREATE_BTN"] = "創造";
$MESS["INTR_MAIL_MANAGE_CREATE_SUBFORM"] = "創建新的";
$MESS["INTR_MAIL_MANAGE_CREATE_TITLE"] = "創建郵箱";
$MESS["INTR_MAIL_MANAGE_DELETE"] = "刪除";
$MESS["INTR_MAIL_MANAGE_DELETE_CONFIRM"] = "您確定要刪除郵箱嗎？<br> <br>所有消息將與郵箱不可逆地刪除！";
$MESS["INTR_MAIL_MANAGE_DELETE_TITLE"] = "刪除郵箱";
$MESS["INTR_MAIL_MANAGE_DELETE_WARNING"] = "郵箱及其所有消息將不可逆轉地刪除！";
$MESS["INTR_MAIL_MANAGE_DELETE_WT"] = "注意力！";
$MESS["INTR_MAIL_MANAGE_DOMAIN_ADD"] = "附加域";
$MESS["INTR_MAIL_MANAGE_DOMAIN_EDIT"] = "配置連接";
$MESS["INTR_MAIL_MANAGE_DOMAIN_EDIT2"] = "編輯設置";
$MESS["INTR_MAIL_MANAGE_ERR_AJAX"] = "錯誤。請再試一次。";
$MESS["INTR_MAIL_MANAGE_FINP_EMAIL"] = "郵箱";
$MESS["INTR_MAIL_MANAGE_GRID_EMAIL"] = "目前的地址";
$MESS["INTR_MAIL_MANAGE_GRID_NAME"] = "用戶";
$MESS["INTR_MAIL_MANAGE_HINT"] = "為您公司的每個員工創建一個郵箱。使用此接口管理公司郵箱：創建，附加或刪除郵箱以及更改郵箱密碼。";
$MESS["INTR_MAIL_MANAGE_INP_DOMAIN"] = "領域";
$MESS["INTR_MAIL_MANAGE_INP_EXIST_MB"] = "未分配的郵箱";
$MESS["INTR_MAIL_MANAGE_INP_LOGIN"] = "登入";
$MESS["INTR_MAIL_MANAGE_INP_NEW_PASSWORD"] = "新密碼";
$MESS["INTR_MAIL_MANAGE_INP_PASSWORD"] = "創建密碼";
$MESS["INTR_MAIL_MANAGE_INP_PASSWORD2"] = "確認密碼";
$MESS["INTR_MAIL_MANAGE_INP_USER"] = "分配給用戶";
$MESS["INTR_MAIL_MANAGE_INP_USER_REPLACE"] = "編輯";
$MESS["INTR_MAIL_MANAGE_INP_USER_SELECT"] = "選擇";
$MESS["INTR_MAIL_MANAGE_MODE_MAILBOX"] = "郵箱";
$MESS["INTR_MAIL_MANAGE_MODE_USER"] = "用戶";
$MESS["INTR_MAIL_MANAGE_PASSWORD_TITLE"] = "更改密碼";
$MESS["INTR_MAIL_MANAGE_RELEASE_BTN"] = "斷開";
$MESS["INTR_MAIL_MANAGE_RELEASE_CONFIRM"] = "您確定要斷開郵箱嗎？";
$MESS["INTR_MAIL_MANAGE_RELEASE_TITLE"] = "斷開郵箱";
$MESS["INTR_MAIL_MANAGE_SAVE_BTN"] = "節省";
$MESS["INTR_MAIL_MANAGE_SEARCH_BTN"] = "尋找";
$MESS["INTR_MAIL_MANAGE_SEARCH_CANCEL"] = "取消";
$MESS["INTR_MAIL_MANAGE_SEARCH_PROMPT"] = "搜索郵箱";
$MESS["INTR_MAIL_MANAGE_SEARCH_TITLE"] = "篩選";
$MESS["INTR_MAIL_MANAGE_SELECT_SUBFORM"] = "選擇現有";
$MESS["INTR_MAIL_MANAGE_SETTINGS"] = "常見參數";
$MESS["INTR_MAIL_MANAGE_SETUP"] = "設定";
$MESS["INTR_MAIL_MANAGE_SETUP_ALLOW_CRM"] = "允許員工將郵箱連接到CRM";
$MESS["INTR_MAIL_MANAGE_SETUP_BLACKLIST"] = "黑名單";
$MESS["INTR_MAIL_MANAGE_SETUP_BLACKLIST_ADD"] = "添加更多";
$MESS["INTR_MAIL_MANAGE_SETUP_BLACKLIST_HINT"] = "拒絕的電子郵件和域";
$MESS["INTR_MAIL_MANAGE_SETUP_BLACKLIST_MORE"] = "和";
$MESS["INTR_MAIL_MANAGE_SETUP_BLACKLIST_PLACEHOLDER"] = "用逗號或新行分開多個電子郵件和域";
$MESS["INTR_MAIL_MANAGE_SETUP_SAVE"] = "節省";
$MESS["INTR_MAIL_MANAGE_SETUP_SAVE_ERROR"] = "錯誤保存首選項";
$MESS["INTR_MAIL_MANAGE_SETUP_SAVE_OK"] = "參數已成功保存。";
