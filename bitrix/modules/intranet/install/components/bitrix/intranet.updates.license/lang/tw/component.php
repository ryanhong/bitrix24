<?php
$MESS["SUPA_ACE_ACT"] = "錯誤試圖激活優惠券";
$MESS["SUPA_ACE_CPN"] = "沒有提供優惠券代碼";
$MESS["SUPA_AERR_CONTACT_EMAIL"] = "請輸入聯繫人的電子郵件地址";
$MESS["SUPA_AERR_CONTACT_EMAIL1"] = "請檢查聯繫人的電子郵件地址是否正確";
$MESS["SUPA_AERR_CONTACT_PERSON"] = "請輸入聯繫人的名字和姓氏";
$MESS["SUPA_AERR_CONTACT_PHONE"] = "請輸入聯繫人的電話號碼";
$MESS["SUPA_AERR_EMAIL"] = "請輸入聯繫電子郵件";
$MESS["SUPA_AERR_EMAIL1"] = "請檢查電子郵件地址正確性";
$MESS["SUPA_AERR_FNAME"] = "請輸入<a href= \"https://www.bitrixsoft.com \"> www.bitrixsoft.com <a href= \"https://www.bitrixsoft.com </a>的用戶的名字</ a>";
$MESS["SUPA_AERR_LNAME"] = "請輸入<a href= \"https://www.bitrixsoft.com \"> www.bitrixsoft.com <a href= \"https://www.bitrixsoft.com </a>的用戶的姓氏</ a>";
$MESS["SUPA_AERR_LOGIN"] = "請在<a href= \"https://www.bitrixsoft.com \"> www.bitrixsoft.com </a>上輸入登錄";
$MESS["SUPA_AERR_LOGIN1"] = "登錄名：<a href= \"https://www.bitrixsoft.com \"> www.bitrixsoft.com </a>必須至少包含3個符號";
$MESS["SUPA_AERR_NAME"] = "請輸入擁有密鑰的公司的名稱";
$MESS["SUPA_AERR_PASSW"] = "請在<a href= \"https://www.bitrixsoft.com \"> www.bitrixsoft.com </a>";
$MESS["SUPA_AERR_PASSW_CONF"] = "密碼和密碼確認是不同的。";
$MESS["SUPA_AERR_PHONE"] = "請輸入產品副本所有者的電話號碼";
$MESS["SUPA_AERR_URI"] = "請輸入將與密鑰一起使用的網站的地址";
