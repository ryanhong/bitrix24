<?php
$MESS["SUP_ACTIVE_PERIOD_TO"] = "通過＃date_to＃";
$MESS["SUP_ACTIVE_TITLE"] = "許可證密鑰有效";
$MESS["SUP_CURRENT_NUMBER_OF_USERS"] = ";當前用戶計數：＃num＃";
$MESS["SUP_CURRENT_NUMBER_OF_USERS1"] = "活動用戶：＃num＃。";
$MESS["SUP_EDITION"] = "版";
$MESS["SUP_LICENSE_KEY"] = "註冊碼";
$MESS["SUP_REGISTERED"] = "註冊";
$MESS["SUP_SITES"] = "站點數量";
$MESS["SUP_USERS"] = "最大用戶";
$MESS["SUP_USERS_IS_NOT_LIMITED"] = "您的許可證沒有最大用戶限制。";
$MESS["UPDATES_ACTIVATE_CONTACT_EMAIL"] = "聯繫人電子郵件";
$MESS["UPDATES_ACTIVATE_CONTACT_INFO"] = "其他重要的聯繫人";
$MESS["UPDATES_ACTIVATE_CONTACT_PERSON"] = "與負責此產品副本的人聯繫";
$MESS["UPDATES_ACTIVATE_CONTACT_PHONE"] = "聯繫人電話";
$MESS["UPDATES_ACTIVATE_EMAIL"] = "許可和使用聯繫電子郵件";
$MESS["UPDATES_ACTIVATE_GENERATE_USER"] = "在www.bitrixsoft.com上創建用戶";
$MESS["UPDATES_ACTIVATE_GENERATE_USER_NO"] = "我已經有一個用戶帳戶，並且想使用它來訪問HelpDesk和下載站點部分；";
$MESS["UPDATES_ACTIVATE_LICENSE_TITLE"] = "許可證密鑰激活";
$MESS["UPDATES_ACTIVATE_NAME"] = "完整的所有者名稱（公司或個人）";
$MESS["UPDATES_ACTIVATE_PHONE"] = "產品副本電話";
$MESS["UPDATES_ACTIVATE_SITE_TEXT"] = "如果您未在<a href= \"http://www.bitrixsoft.com \" target= \"_blank \"> www.bitrixsoft.com </a>上註冊“
 <br/>選項已選中，然後輸入您的信息（名字和姓氏，登錄和密碼）
<br/>在表單字段中。在www.bitrixsoft.com上註冊後，您可以使用
<br/> <a href= \"http://www.bitrixsoft.com/support/xupport/xtart target= \"_blank \"> TechSupport Service </a>和<a href = \ \ http：// http：// http：/// www.bitrixsoft.com/support/forum/ \“ target = \” _空白\“>私人論壇</a>解決您的問題並獲得答案
<br/>您的問題。";
$MESS["UPDATES_ACTIVATE_SITE_URL_NEW"] = "該系統實例（包括測試域）將由此系統實例驅動的所有域";
$MESS["UPDATES_ACTIVATE_SUCCESS"] = "密鑰已成功激活";
$MESS["UPDATES_ACTIVATE_USER_EMAIL"] = "電子郵件";
$MESS["UPDATES_ACTIVATE_USER_LAST_NAME"] = "姓";
$MESS["UPDATES_ACTIVATE_USER_LOGIN_A"] = "登錄（3個或更多字符）";
$MESS["UPDATES_ACTIVATE_USER_NAME"] = "名";
$MESS["UPDATES_ACTIVATE_USER_PASSWORD"] = "密碼（6個或更多字符）";
$MESS["UPDATES_ACTIVATE_USER_PASSWORD_CONFIRM"] = "確認密碼";
$MESS["UPDATES_COUPON_KEY"] = "輸入優惠券代碼";
$MESS["UPDATES_COUPON_SUCCESS"] = "優惠券已成功應用";
$MESS["UPDATES_COUPON_TITLE"] = "激活優惠券";
$MESS["UPDATES_LICENSE_ACTIVATE"] = "啟用";
$MESS["UPDATES_LICENSE_KEY"] = "註冊碼";
$MESS["UPDATES_LICENSE_SAVE"] = "節省";
$MESS["UPDATES_LICENSE_TITLE"] = "Bitrix24許可證";
