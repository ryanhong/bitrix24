<?php
$MESS["BACKUP_BUTTON"] = "創建備份";
$MESS["BACKUP_CANCEL_BUTTON"] = "取消";
$MESS["BACKUP_DELETE_CONFIRM"] = "您確定要刪除文件嗎？";
$MESS["BACKUP_ERROR"] = "錯誤創建備份";
$MESS["BACKUP_HEADER_DATE"] = "修改";
$MESS["BACKUP_HEADER_NAME"] = "姓名";
$MESS["BACKUP_HEADER_PLACE"] = "地點";
$MESS["BACKUP_HEADER_SIZE"] = "存檔尺寸";
$MESS["BACKUP_PROGRESS_HINT"] = "現在正在創建備份副本。請不要關閉頁面。";
$MESS["BACKUP_PROGRESS_TEXT"] = "創建備份";
$MESS["BACKUP_RENAME_TITLE"] = "重新命名文件";
$MESS["BACKUP_RESTORE_CONFIRM"] = "注意力！恢復備份到運行的站點可能會損壞該站點！繼續？";
$MESS["BACKUP_SAVE_BUTTON"] = "節省";
$MESS["BACKUP_STOP_BUTTON"] = "取消";
$MESS["BACKUP_SUCCESS"] = "備份已成功創建。";
$MESS["BACKUP_SYSTEM_ERROR"] = "系統錯誤：";
