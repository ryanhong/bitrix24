<?php
$MESS["INTRANET_USER_PROFILE_AUTH_TITLE_2"] = "驗證";
$MESS["INTRANET_USER_PROFILE_LOGIN_HISTORY_TITLE"] = "登錄歷史記錄";
$MESS["INTRANET_USER_PROFILE_MAILING_AGREEMENT_TITLE"] = "Bitrix24新聞通訊";
$MESS["INTRANET_USER_PROFILE_PASSWORD_TITLE"] = "更改密碼";
$MESS["INTRANET_USER_PROFILE_SECURITY_MENU_ITEM_LABEL_NEW"] = "新的";
$MESS["INTRANET_USER_PROFILE_SECURITY_OTP_TITLE"] = "安全";
$MESS["INTRANET_USER_PROFILE_SECURITY_PASSWORDS_TITLE"] = "應用程序密碼";
$MESS["INTRANET_USER_PROFILE_SOCNET_EMAIL_TITLE"] = "轉發地址";
$MESS["INTRANET_USER_PROFILE_SOCSERV_TITLE"] = "社交平台";
$MESS["INTRANET_USER_PROFILE_SSO_ERROR_NO_RIGHTS"] = "請聯繫您的BitRix24管理員以正確配置SSO。";
$MESS["INTRANET_USER_PROFILE_SSO_TITLE"] = "SSO和Scim";
$MESS["INTRANET_USER_PROFILE_SYNCHRONIZE_TITLE"] = "同步";
