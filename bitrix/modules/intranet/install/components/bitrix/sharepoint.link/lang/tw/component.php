<?php
$MESS["SL_ERROR_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["SL_ERROR_NO_IBLOCK"] = "未指定信息塊。";
$MESS["SL_ERROR_WRONG_URL"] = "無效的服務器地址。";
$MESS["SL_LINK_ADD"] = "創建鏈接";
$MESS["SL_LINK_EDIT"] = "設定";
$MESS["SL_LINK_SYNC"] = "同步";
