import { EventEmitter } from 'main.core.events';
import { Section, Row } from 'ui.section';
import { SwitcherNestedItem, SwitcherNested } from 'ui.switcher-nested';
import { Loc, Type, Event, Dom } from 'main.core';
import { SettingsSection, SettingsRow, BaseSettingsPage } from 'ui.form-elements.field';

export class ToolsPage extends BaseSettingsPage
{
	constructor()
	{
		super();
		this.titlePage = Loc.getMessage('INTRANET_SETTINGS_TITLE_PAGE_TOOLS');
		this.descriptionPage = Loc.getMessage('INTRANET_SETTINGS_DESCRIPTION_PAGE_TOOLS');
	}

	getType(): string
	{
		return 'tools';
	}

	appendSections(contentNode: HTMLElement): void
	{
		const toolsSection = new Section({
			title: Loc.getMessage('INTRANET_SETTINGS_SECTION_TITLE_TOOLS_SHOW'),
			titleIconClasses: 'ui-icon-set --service',
			isOpen: true,
			canCollapse: false,
		});

		const sectionSettings = new SettingsSection({
			section: toolsSection,
			parent: this,
		});

		const description = new Row({
			content: this.getDescription().getContainer(),
		});
		new SettingsRow({
			row: description,
			parent: sectionSettings,
		});

		if (this.hasValue('tools'))
		{
			const tools = this.getValue('tools');
			Object.keys(tools).forEach((item) => {
				const tool = tools[item];
				const subgroups = tool.subgroups;
				const toolSelectorItems = [];

				if (Object.keys(subgroups).length > 0)
				{
					Object.keys(subgroups).forEach((item) => {
						const subgroupConfig = subgroups[item];

						if (
							Type.isNull(subgroupConfig.name)
							|| Type.isNull(subgroupConfig.code)
							|| Type.isNull(subgroupConfig.enabled)
						)
						{
							return;
						}

						const toolSelectorItem = new SwitcherNestedItem({
							title: subgroupConfig.name,
							id: subgroupConfig.code,
							inputName: subgroupConfig.code,
							isChecked: subgroupConfig.enabled,
							settingsPath: subgroupConfig['settings_path'],
							settingsTitle: subgroupConfig['settings_title'] ?? Loc.getMessage('INTRANET_SETTINGS_SECTION_TOOLS_LINK_SETTINGS'),
							infoHelperCode: subgroupConfig['infohelper-slider'],
							isDefault: subgroupConfig.default ?? subgroupConfig.disabled ?? false,
							helpMessage: subgroupConfig.disabled
								? Loc.getMessage('INTRANET_SETTINGS_FIELD_HELP_MESSAGE_DISABLED', {'#TOOL#': subgroupConfig.name})
								: subgroupConfig.default
									? Loc.getMessage('INTRANET_SETTINGS_FIELD_HELP_MESSAGE_MAIN_TOOL', {'#TOOL#': tool.name})
									: '',
						});

						if (subgroupConfig.disabled)
						{
							Dom.style(toolSelectorItem.getSwitcher().getNode(), {opacity: '0.4'});
						}

						EventEmitter.subscribe(
							toolSelectorItem.getSwitcher(),
							'toggled',
							() =>
							{
								this.getAnalytic()?.addEventToggleTools(
									subgroupConfig.code,
									toolSelectorItem.getSwitcher().isChecked()
								);
							}
						);

						if (subgroupConfig.code === 'tool_subgroup_team_work_instant_messenger')
						{
							Event.bind(
								toolSelectorItem.getSwitcher().getNode(),
								'click',
								() => {
									if (!toolSelectorItem.getSwitcher().isChecked())
									{
										this.#getWarningMessage(subgroupConfig.code, toolSelectorItem.getSwitcher().getNode(), Loc.getMessage('INTRANET_SETTINGS_WARNING_TOOL_INSTANT_MESSENGER')).show();
									}
								}
							);
						}

						toolSelectorItems.push(toolSelectorItem);
					});
				}

				const toolSelectorSection = new SettingsSection({
					section: new SwitcherNested({
						id: tool.code,
						title: tool.name,
						link: tool['settings-path'],
						infoHelperCode: tool['infohelper-slider'],
						linkTitle: tool['settings-title'] ?? Loc.getMessage('INTRANET_SETTINGS_SECTION_TOOLS_LINK_SETTINGS'),
						isChecked: tool.enabled,
						mainInputName: tool.code,
						isOpen: false,
						items: toolSelectorItems,
						isDefault: tool.default,
						helpMessage: Loc.getMessage('INTRANET_SETTINGS_FIELD_HELP_MESSAGE_DISABLED', {'#TOOL#': tool.name}),
					})
				});

				// ToolsPage.addToSectionHelper(toolSelector, sectionSettings);

				const toolRow = new Row({
					content: toolSelectorSection.getSectionView().render(),
				});
				new SettingsRow({
					row: toolRow,
					parent: sectionSettings,
					child: toolSelectorSection
				});
			});
		}

		sectionSettings.renderTo(contentNode);
	}

	getDescription(): BX.UI.Alert
	{
		const descriptionText = `
			${Loc.getMessage('INTRANET_SETTINGS_SECTION_TOOLS_DESCRIPTION')}
			<a class="ui-section__link" onclick="top.BX.Helper.show('redirect=detail&code=18213196')">
				${Loc.getMessage('INTRANET_SETTINGS_CANCEL_MORE')}
			</a>
		`;

		return new BX.UI.Alert({
			text: descriptionText,
			inline: true,
			size: BX.UI.Alert.Size.SMALL,
			color: BX.UI.Alert.Color.PRIMARY,
			animated: true,
		});
	}

	#getWarningMessage(toolId, bindElement, message)
	{
		return BX.PopupWindowManager.create(
			toolId,
			bindElement,
			{
				content: message,
				darkMode: true,
				autoHide: true,
				angle: true,
				offsetLeft: 14,
				bindOptions: {
					position: 'bottom',
				},
				closeByEsc: true,
			}
			);
	}
}
