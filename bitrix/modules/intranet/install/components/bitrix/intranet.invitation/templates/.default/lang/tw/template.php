<?php
$MESS["BX24_INVITE_DIALOG_ACTION_ADD"] = "添加";
$MESS["BX24_INVITE_DIALOG_ACTION_INVITE"] = "邀請";
$MESS["BX24_INVITE_DIALOG_ACTION_INVITE_MORE"] = "邀請更多用戶";
$MESS["BX24_INVITE_DIALOG_ACTION_SAVE"] = "節省";
$MESS["BX24_INVITE_DIALOG_ADDED"] = "<b>恭喜！</b> <br>已將Intranet成員通知發送給該用戶。<br> <br> <br> <br>審查您在<a style = \“ white space：nowrap中邀請的新用戶； \“ href = \”＃site_dir＃company/？show_user = intactive \'>員工目錄</a>。";
$MESS["BX24_INVITE_DIALOG_ADD_DEPARTMENT_PATTERN"] = "將＃標題編號添加到部門＃部門＃和＃SonetGroup＃";
$MESS["BX24_INVITE_DIALOG_ADD_EMAIL_TITLE"] = "電子郵件";
$MESS["BX24_INVITE_DIALOG_ADD_GROUP_PATTERN"] = "將＃標題編號添加到組";
$MESS["BX24_INVITE_DIALOG_ADD_LAST_NAME_TITLE"] = "姓";
$MESS["BX24_INVITE_DIALOG_ADD_NAME_TITLE"] = "名";
$MESS["BX24_INVITE_DIALOG_ADD_POSITION_TITLE"] = "位置";
$MESS["BX24_INVITE_DIALOG_ADD_SEND_PASSWORD_TITLE"] = "將登錄信息發送到指定的電子郵件";
$MESS["BX24_INVITE_DIALOG_ADD_WO_CONFIRMATION_TITLE"] = "沒有確認";
$MESS["BX24_INVITE_DIALOG_BUTTON_ADD"] = "添加";
$MESS["BX24_INVITE_DIALOG_BUTTON_CLOSE"] = "關閉";
$MESS["BX24_INVITE_DIALOG_BUTTON_INVITE"] = "邀請";
$MESS["BX24_INVITE_DIALOG_CONF_PAGE_TITLE"] = "註冊確認";
$MESS["BX24_INVITE_DIALOG_CONTINUE_ADD_BUTTON"] = "添加更多";
$MESS["BX24_INVITE_DIALOG_CONTINUE_INVITE_BUTTON"] = "邀請更多";
$MESS["BX24_INVITE_DIALOG_COPY_LINK"] = "複製鏈接";
$MESS["BX24_INVITE_DIALOG_COPY_URL"] = "鏈接複製到剪貼板";
$MESS["BX24_INVITE_DIALOG_DEPARTMENT_CLOSE"] = "關閉";
$MESS["BX24_INVITE_DIALOG_DEST_LINK_1"] = "添加組";
$MESS["BX24_INVITE_DIALOG_DEST_LINK_2"] = "添加更多";
$MESS["BX24_INVITE_DIALOG_EMAIL"] = "輸入要邀請的人的電子郵件地址。帶有逗號或空間的多個條目。";
$MESS["BX24_INVITE_DIALOG_EMAILS_DESCR"] = "與逗號或空間分開多個電子郵件";
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "錯誤的電子郵件格式。";
$MESS["BX24_INVITE_DIALOG_EMAIL_SHORT"] = "電子郵件";
$MESS["BX24_INVITE_DIALOG_EMPLOYEE"] = "員工";
$MESS["BX24_INVITE_DIALOG_ERROR_EXTRANET_NO_SONET_GROUP_ADD"] = "請選擇一個組以添加外部用戶";
$MESS["BX24_INVITE_DIALOG_ERROR_EXTRANET_NO_SONET_GROUP_INVITE"] = "請選擇一個邀請外部用戶的組";
$MESS["BX24_INVITE_DIALOG_EXTRANET"] = "外部用戶";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR"] = "<b>恭喜！</b> <br> <br>邀請加入您的Bitrix24的邀請已發送給Bitrix24合作夥伴。";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_EMAIL_ERROR"] = "指定的電子郵件沒有Bitrix24合作夥伴";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_INVITE_TEXT"] = "我邀請您加入我們的Bitrix24！您將獲得完整的BitRix24合作夥伴權限，以幫助我們設置和配置Bitrix24帳戶。這些是完整的訪問權限，除非您無法添加或刪除系統管理員。";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_MORE"] = "了解更多";
$MESS["BX24_INVITE_DIALOG_INTEGRATOR_TEXT"] = "經過認證的Bitrix24合作夥伴可以幫助您進行設置或將Bitrix24微調到公司的工作流程：CRM，開放渠道，文檔，電話，報告和其他業務工具。";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>成功！</b> <br>邀請已發送到指定的地址。<br> <br>查看您剛剛邀請的用戶，<a style = \“白空間：nowrap; \ \” href = \ “/company/？show_user = intactive \”>單擊此處</a>。";
$MESS["BX24_INVITE_DIALOG_INVITE_DEPARTMENT_PATTERN"] = "邀請＃標題＃到部門＃部門＃和＃SonetGroup＃";
$MESS["BX24_INVITE_DIALOG_INVITE_GROUP_PATTERN"] = "邀請＃標題編號到小組";
$MESS["BX24_INVITE_DIALOG_INVITE_LIMIT"] = "您不能邀請更多員工，因為它將超過您的許可條款。";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_1"] = "請加入我們的Bitrix24帳戶。在這個地方，每個人都可以在任務和項目上進行溝通，協作，管理客戶並做更多的事情。";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT_PHONE"] = "請加入我們的Bitrix24帳戶 - ＃門戶＃。在這裡接受邀請：＃url＃";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TITLE"] = "消息文字";
$MESS["BX24_INVITE_DIALOG_INVITE_MORE"] = "邀請更多用戶";
$MESS["BX24_INVITE_DIALOG_INVITE_PHONE"] = "<b> <b>恭喜！</b> <br> <br> Bitrix24邀請已發送到選定的數字。<br> <br>要查看邀請誰被邀請，請<a style = \'white = \“白色空間：nowrap; \ \“ href = \”＃site_dir＃company/？show_user = intactive \“> intairive \”>打開員工列表</a>。";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_ACTION_CONNECT"] = "附加現有的郵箱";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_ACTION_CREATE"] = "創建郵箱";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_ACTION_OR"] = "或者";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_DOMAIN"] = "領域";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_NAME"] = "姓名";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_PASSWORD"] = "密碼";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_PASSWORD_CONFIRM"] = "確認密碼";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_ROLLUP"] = "隱藏";
$MESS["BX24_INVITE_DIALOG_MAIL_MAILBOX_SELECT"] = "選擇";
$MESS["BX24_INVITE_DIALOG_PHONES_DESCR"] = "輸入電話號碼，帶有空間或逗號的多個條目";
$MESS["BX24_INVITE_DIALOG_PHONE_ADD"] = "添加";
$MESS["BX24_INVITE_DIALOG_PHONE_SHORT"] = "電話";
$MESS["BX24_INVITE_DIALOG_REGISTER_ALLOW_N"] = "允許快速註冊";
$MESS["BX24_INVITE_DIALOG_REGISTER_EXTENDED_SETTINGS"] = "其他設置";
$MESS["BX24_INVITE_DIALOG_REGISTER_LINK"] = "快速註冊鏈接";
$MESS["BX24_INVITE_DIALOG_REGISTER_NEW_LINK"] = "更新鏈接";
$MESS["BX24_INVITE_DIALOG_REGISTER_NEW_LINK_HELP"] = "創建新鏈接後，現有的鏈接將無效，因此用戶將無法將其用於註冊。";
$MESS["BX24_INVITE_DIALOG_REGISTER_TEXT_PLACEHOLDER_N_1"] = "請加入我們的Bitrix24帳戶。在這個地方，每個人都可以在任務和項目上進行溝通，協作，管理客戶並做更多的事情。";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_CLOSE_HELP"] = "新用戶將能夠在您的Bitrix24帳戶中註冊，並在管理員批准後訪問它。";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_CLOSE_N"] = "主持";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_DOMAINS"] = "不需要確認域：";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_DOMAINS_HELP"] = "如果新用戶的電子郵件地址與指定的電子郵件域匹配，則可以在您的Bitrix24帳戶中註冊並在無批准的情況下訪問它。";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_DOMAINS_PLACEHOLDER"] = "yourcompany.com; Company.com";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_N"] = "註冊類型";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_OPEN_HELP"] = "新用戶將能夠在您的BITRIX24帳戶中註冊，並在未經管理員批准的情況下訪問它。新用戶通知將顯示在活動流和一般聊天中。";
$MESS["BX24_INVITE_DIALOG_REGISTER_TYPE_OPEN_N"] = "民眾";
$MESS["BX24_INVITE_DIALOG_SELF"] = "<b>恭喜！</b> <br>偏好已更新。";
$MESS["BX24_INVITE_DIALOG_SONETGROUP"] = "團體";
$MESS["BX24_INVITE_DIALOG_TAB_ADD_TITLE"] = "添加";
$MESS["BX24_INVITE_DIALOG_TAB_ADD_TITLE_NEW"] = "登記";
$MESS["BX24_INVITE_DIALOG_TAB_INTEGRATOR_TITLE"] = "邀請Bitrix24合作夥伴";
$MESS["BX24_INVITE_DIALOG_TAB_INVITE_TITLE"] = "邀請";
$MESS["BX24_INVITE_DIALOG_TAB_INVITE_TITLE_NEW"] = "使用電子郵件邀請";
$MESS["BX24_INVITE_DIALOG_TAB_INVITE_TITLE_PHONE"] = "邀請使用短信";
$MESS["BX24_INVITE_DIALOG_TAB_SELF_TITLE"] = "快速註冊";
$MESS["BX24_INVITE_DIALOG_TITLE"] = "邀請用戶";
$MESS["BX24_INVITE_DIALOG_USERS_LIMIT_TEXT"] = "您當前的計劃將BITRIX24用戶的最大數量限制為＃num＃<br/> <br/>
請升級到添加更多用戶並享受許多基本業務工具的商業計劃之一。";
$MESS["BX24_INVITE_DIALOG_USERS_LIMIT_TITLE"] = "用戶限制";
$MESS["BX24_INVITE_DIALOG_WARNING_CREATE_MAILBOX_ERROR"] = "錯誤創建新郵箱：";
$MESS["BX24_INVITE_DIALOG_WARNING_MAILBOX_PASSWORD_CONFIRM"] = "您的密碼和確認密碼不匹配。";
$MESS["INTRANET_INVITE_DIALOG_ACCESS_ERROR"] = "拒絕訪問";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_DESC"] = "Bitrix24有三個使用AD的選項";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_DESC1"] = "出口員工";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_DESC2"] = "創建公司結構";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_DESC3"] = "單登錄";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_DESC4"] = "您必須下載並安裝應用程序以使用Active Directory。";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_MORE"] = "了解有關使用Active Directory的更多信息？";
$MESS["INTRANET_INVITE_DIALOG_ACTIVE_DIRECTORY_TITLE"] = "活動目錄";
$MESS["INTRANET_INVITE_DIALOG_ADD_MASSIVE"] = "批量邀請";
$MESS["INTRANET_INVITE_DIALOG_ADD_MORE"] = "添加更多";
$MESS["INTRANET_INVITE_DIALOG_ADD_TITLE"] = "添加新用戶";
$MESS["INTRANET_INVITE_DIALOG_CONFIRM_CREATOR_EMAIL_ERROR"] = "在邀請用戶邀請用戶之前，您必須使用電子郵件中的確認鏈接驗證帳戶所有者的電子郵件地址。";
$MESS["INTRANET_INVITE_DIALOG_EMPTY_ERROR_EMAIL"] = "電子郵件地址丟失。";
$MESS["INTRANET_INVITE_DIALOG_EMPTY_ERROR_EMAIL_AND_PHONE"] = "丟失了電子郵件地址或電話號碼。";
$MESS["INTRANET_INVITE_DIALOG_EXTRANET_GROUP"] = "邀請工作組";
$MESS["INTRANET_INVITE_DIALOG_EXTRANET_TITLE"] = "邀請額外用戶";
$MESS["INTRANET_INVITE_DIALOG_FAST_REG_DOMAINS"] = "不需要確認域";
$MESS["INTRANET_INVITE_DIALOG_FAST_REG_TITLE"] = "通過鏈接邀請";
$MESS["INTRANET_INVITE_DIALOG_FAST_REG_TYPE"] = "請求管理員批准加入";
$MESS["INTRANET_INVITE_DIALOG_GROUP_OR_DEPARTMENT_INPUT"] = "選擇部門或工作組";
$MESS["INTRANET_INVITE_DIALOG_GROUP_OR_DEPARTMENT_TITLE"] = "邀請到部門或工作組";
$MESS["INTRANET_INVITE_DIALOG_INPUT_EMAIL"] = "輸入電子郵件";
$MESS["INTRANET_INVITE_DIALOG_INPUT_EMAIL_AND_PHONE"] = "電子郵件或電話號碼";
$MESS["INTRANET_INVITE_DIALOG_INTEGRATOR_EMAIL"] = "Bitrix24合作夥伴電子郵件";
$MESS["INTRANET_INVITE_DIALOG_MASS_DESC_EMAIL"] = "輸入一個或多個電子郵件地址";
$MESS["INTRANET_INVITE_DIALOG_MASS_DESC_EMAIL_AND_PHONE"] = "您可以輸入幾封電子郵件或電話號碼";
$MESS["INTRANET_INVITE_DIALOG_MASS_INVITE_HINT"] = "批量邀請僅允許當前的國家電話號碼";
$MESS["INTRANET_INVITE_DIALOG_MASS_LABEL_EMAIL"] = "帶有空間或逗號的多個電子郵件";
$MESS["INTRANET_INVITE_DIALOG_MASS_LABEL_EMAIL_AND_PHONE"] = "輸入要邀請的人的電子郵件地址或電話號碼。帶有逗號或空間的多個條目。";
$MESS["INTRANET_INVITE_DIALOG_MASS_TITLE"] = "批量邀請";
$MESS["INTRANET_INVITE_DIALOG_OR"] = "或者";
$MESS["INTRANET_INVITE_DIALOG_PICTURE_TITLE"] = "邀請用戶";
$MESS["INTRANET_INVITE_DIALOG_REG_OFF"] = "#VALUE!";
$MESS["INTRANET_INVITE_DIALOG_REG_ON"] = "#VALUE!";
$MESS["INTRANET_INVITE_DIALOG_SELF_OFF_HINT"] = "通過鏈接的註冊將被禁用。先前發送的註冊鏈接將變得無效。您始終可以再次啟用此功能。";
$MESS["INTRANET_INVITE_DIALOG_SUCCESS_SEND"] = "邀請已發送！";
$MESS["INTRANET_INVITE_DIALOG_TITLE"] = "邀請";
$MESS["INTRANET_INVITE_DIALOG_TITLE_EMAIL"] = "通過電子郵件邀請";
$MESS["INTRANET_INVITE_DIALOG_TITLE_EMAIL_AND_PHONE"] = "通過電子郵件或電話號碼邀請";
$MESS["INTRANET_INVITE_DIALOG_UNLIMITED"] = "無限";
$MESS["INTRANET_INVITE_DIALOG_USER_CURRENT_COUNT"] = "當前在您的Bitrix24上註冊的用戶";
$MESS["INTRANET_INVITE_DIALOG_USER_MAX_COUNT"] = "當前計劃中允許的最大用戶";
$MESS["INTRANET_INVITE_DIALOG_VALIDATE_ERROR_EMAIL"] = "電子郵件地址不正確。";
$MESS["INTRANET_INVITE_DIALOG_VALIDATE_ERROR_EMAIL_AND_PHONE"] = "錯誤的電子郵件或電話號碼";
