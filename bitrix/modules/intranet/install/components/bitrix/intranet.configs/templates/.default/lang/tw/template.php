<?php
$MESS["CAL_OPTION_FIRSTDAY_FR"] = "星期五";
$MESS["CAL_OPTION_FIRSTDAY_MO"] = "週一";
$MESS["CAL_OPTION_FIRSTDAY_SA"] = "週六";
$MESS["CAL_OPTION_FIRSTDAY_SU"] = "星期日";
$MESS["CAL_OPTION_FIRSTDAY_TH"] = "週四";
$MESS["CAL_OPTION_FIRSTDAY_TU"] = "週二";
$MESS["CAL_OPTION_FIRSTDAY_WE"] = "週三";
$MESS["CONFIG_ADD_LOGO_BUTTON"] = "上傳徽標";
$MESS["CONFIG_ADD_LOGO_DELETE"] = "刪除徽標";
$MESS["CONFIG_ADD_LOGO_DELETE_CONFIRM"] = "您確定要刪除徽標嗎？";
$MESS["CONFIG_ALLOW_INVITE_USERS"] = "允許每個人邀請新用戶進入此Bitrix24帳戶";
$MESS["CONFIG_ALLOW_NEW_USER_LF"] = "通知活動流中的新員工";
$MESS["CONFIG_ALLOW_SEARCH_NETWORK"] = "允許在Bitrix24.network上搜索開放頻道";
$MESS["CONFIG_ALLOW_SELF_REGISTER"] = "允許快速註冊";
$MESS["CONFIG_ALLOW_TOALL"] = "在活動流中允許\“所有用戶\”作為選項";
$MESS["CONFIG_BUY_TARIFF_BY_ALL_MSGVER_1"] = "使所有用戶能夠為當前Bitrix24計劃購買升級和擴展";
$MESS["CONFIG_CLIENT_LOGO"] = "公司標誌";
$MESS["CONFIG_CLIENT_LOGO_DESCR"] = "您的徽標必須是<b> png </b>文件。<br/>最大尺寸為222px x 55px。";
$MESS["CONFIG_CLIENT_LOGO_DESC_RETINA"] = "您的徽標必須是<b> png </b>文件。<br/>最大尺寸為444px x 110px。";
$MESS["CONFIG_CLIENT_LOGO_RETINA"] = "高DPI徽標（視網膜）";
$MESS["CONFIG_COLLECT_GEO_DATA"] = "啟用地理位置數據收集";
$MESS["CONFIG_COLLECT_GEO_DATA_CONFIRM"] = "請注意，在某些司法管轄區，您需要明確的用戶同意來處理設備地理位置數據。";
$MESS["CONFIG_COLLECT_GEO_DATA_OK"] = "接受";
$MESS["CONFIG_COMPANY_NAME"] = "公司名稱";
$MESS["CONFIG_COMPANY_TITLE_NAME"] = "在標題中顯示的公司名稱";
$MESS["CONFIG_CREATE_OVERDUE_CHATS"] = "與所有任務參與者在逾期任務上創建聊天";
$MESS["CONFIG_CULTURE_OTHER"] = "其他";
$MESS["CONFIG_DATE_FORMAT"] = "日期格式";
$MESS["CONFIG_DEFAULT_TOALL"] = "使用\“所有用戶\”作為默認收件人";
$MESS["CONFIG_DISK_ALLOW_DOCUMENT_TRANSFORMATION"] = "自動生成文檔的PDF和JPG文件";
$MESS["CONFIG_DISK_ALLOW_EDIT_OBJECT_IN_UF"] = "允許參與的用戶編輯附加到帖子，任務，註釋等上的文檔。<br>（可以在我的驅動器下手動配置訪問權限 - 隨時在上載文件下）";
$MESS["CONFIG_DISK_ALLOW_USE_EXTENDED_FULLTEXT"] = "搜索驅動器文檔";
$MESS["CONFIG_DISK_ALLOW_USE_EXTERNAL_LINK"] = "允許公共鏈接";
$MESS["CONFIG_DISK_ALLOW_VIDEO_TRANSFORMATION"] = "自動生成視頻媒體的MP4和JPG文件";
$MESS["CONFIG_DISK_EXTENDED_FULLTEXT_INFO"] = "搜索暫時不可用。請聯繫HelpDesk以啟用搜索。";
$MESS["CONFIG_DISK_LIMIT_HISTORY_LOCK_POPUP_TEXT"] = "無限文檔歷史記錄可在<a href= \"/settings/license_all.php \" target= \"_blank \">選定的商業計劃</a>中獲得。";
$MESS["CONFIG_DISK_LIMIT_LOCK_POPUP_TEXT"] = "享受具有高級驅動器的更有用的Bitrix24功能：<br/> <br/>
+文檔更新歷史記錄（修改了何時何地）<br/>
+從歷史記錄中恢復任何以前的文檔版本<br/> <br/>
<a href= \"https://www.bitrix24.com/pro/drive.php \" target='_blank'>了解更多</a> <br/> <br/> <br/>
高級驅動器在\“標準\”計劃中可用，並且更高。";
$MESS["CONFIG_DISK_LIMIT_LOCK_POPUP_TITLE"] = "僅在擴展Bitrix24中可用";
$MESS["CONFIG_DISK_LOCK_EXTENDED_FULLTEXT_POPUP_TEXT"] = "可在選定的商業計劃上可用";
$MESS["CONFIG_DISK_LOCK_POPUP_TEXT"] = "高級驅動器功能：

<br/> <br/>
 - 鎖定文件，以防止其他人能夠編輯您當前正在工作的文檔。
<br/> <br/>
 - 禁用公共鏈接，以防止其他人能夠與Bitrix24帳戶之外的用戶公開共享文件
<br/> <br/>
<a href= \"https://www.bitrix24.com/pro/drive.php \" target='_blank'>了解更多</a>
<br/> <br/>
Advanced Bitrix24。從Bitrix24標準計劃開始的商業訂閱中可用。
";
$MESS["CONFIG_DISK_LOCK_POPUP_TITLE"] = "僅在Advanced Bitrix24驅動器中可用";
$MESS["CONFIG_DISK_OBJECT_LOCK_ENABLED"] = "允許文檔鎖定";
$MESS["CONFIG_DISK_TRANSFORM_FILES_ON_OPEN"] = "打開後立即將文件轉換";
$MESS["CONFIG_DISK_VERSION_LIMIT_PER_FILE"] = "最大限度。文檔歷史記錄中的條目。";
$MESS["CONFIG_DISK_VIEWER_SERVICE"] = "使用";
$MESS["CONFIG_EMAIL_FROM"] = "網站管理員的電子郵件<br>（默認發送者地址）";
$MESS["CONFIG_EXAMPLE"] = "例子";
$MESS["CONFIG_FEATURES_CRM"] = "CRM";
$MESS["CONFIG_FEATURES_EXTRANET"] = "外部";
$MESS["CONFIG_FEATURES_LISTS"] = "列表";
$MESS["CONFIG_FEATURES_MEETING"] = "會議和簡報";
$MESS["CONFIG_FEATURES_PROCESSES"] = "行政工作流程";
$MESS["CONFIG_FEATURES_TIMEMAN"] = "時間管理和工作報告";
$MESS["CONFIG_FEATURES_TITLE"] = "服務";
$MESS["CONFIG_GDRP_APP1"] = "GDPR的員工";
$MESS["CONFIG_GDRP_APP2"] = "GDPR的CRM";
$MESS["CONFIG_GDRP_LABEL1"] = "產品更新，特別優惠，網絡研討會邀請，新聞通訊Digests";
$MESS["CONFIG_GDRP_LABEL2"] = "培訓教材";
$MESS["CONFIG_GDRP_LABEL3"] = "我接受數據處理協議";
$MESS["CONFIG_GDRP_LABEL4"] = "客戶法律名稱*";
$MESS["CONFIG_GDRP_LABEL5"] = "主聯繫人名稱*";
$MESS["CONFIG_GDRP_LABEL6"] = "標題*";
$MESS["CONFIG_GDRP_LABEL7"] = "日期*";
$MESS["CONFIG_GDRP_LABEL8"] = "通知電子郵件地址*";
$MESS["CONFIG_GDRP_LABEL11"] = "（根據<a href= \"https://www.bitrix24.ru/about/advertising.php \" target= \“_blank \">協議</a>）";
$MESS["CONFIG_GDRP_TITLE1"] = "我同意收到包含Bitrix24的電子郵件：";
$MESS["CONFIG_GDRP_TITLE2"] = "如果您接受數據處理協議，則還必須提供以下信息：";
$MESS["CONFIG_GDRP_TITLE3"] = "個人數據處理應用程序：";
$MESS["CONFIG_HEADER_GDRP"] = "GDPR合規性";
$MESS["CONFIG_HEADER_SECUTIRY"] = "安全設定";
$MESS["CONFIG_HEADER_SETTINGS"] = "設定";
$MESS["CONFIG_IM_CHAT_RIGHTS"] = "允許用戶將消息發送到一般聊天";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_ADMIN_RIGHTS"] = "郵政管理員分配和管理員將通知更改為一般聊天";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_JOIN"] = "通知一般聊天中的新員工";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_LEAVE"] = "在一般聊天中通知員工/用戶解僱";
$MESS["CONFIG_IP_HELP_TEXT2"] = "沒有管理權限的用戶將無法從與指定範圍不匹配的任何IP地址訪問此Bitrix24。
登錄後，用戶將看到一個錯誤頁面，告知他們從IP中訪問的訪問被拒絕。
您可以觀看事件日誌中任何IP地址登錄到BitRix24的嘗試。";
$MESS["CONFIG_IP_TITLE"] = "IP限制（僅允許從指定的IP地址或地址範圍訪問。示例：192.168.0.7; 192.168.0.1-192.168.0.100）";
$MESS["CONFIG_LIMIT_MAX_TIME_IN_DOCUMENT_HISTORY"] = "保留文檔版本的＃num＃days。";
$MESS["CONFIG_LOCATION_ADDRESS_FORMAT"] = "地址格式";
$MESS["CONFIG_LOCATION_DEFAULT_SOURCE"] = "默認來源";
$MESS["CONFIG_LOCATION_SOURCES_SETTINGS"] = "地址和位置源設置";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_API_KEY_BACKEND"] = "Google位置API和地理編碼API服務器密鑰";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_API_KEY_FRONTEND"] = "Google Maps JavaScript API，位置API和地理編碼API瀏覽器密鑰";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_NOTE"] = "需要使用Google API密鑰才能使用地圖。要獲取您的鑰匙，請<a href= \"https://developers.google.com/maps/documentation/javascript/get-api-key \“target= \”_blank \">使用此表單</a > 。";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_SHOW_PHOTOS_ON_MAP"] = "顯示Google Photos for Your Ploce Map視圖（Google可能為此選項收取添加費用）";
$MESS["CONFIG_LOCATION_SOURCE_GOOGLE_USE_GEOCODING_SERVICE"] = "使用地理編碼服務進行未知地址（Google可能為此選項收取添加費用）";
$MESS["CONFIG_LOCATION_SOURCE_OSM_SERVICE_URL"] = "服務網址";
$MESS["CONFIG_LOGO_24"] = "將\“ 24 \”添加到公司徽標";
$MESS["CONFIG_MARKETPLACE_MORE"] = "了解更多";
$MESS["CONFIG_MARKETPLACE_TITLE"] = "將數據從其他系統遷移到BITRIX24";
$MESS["CONFIG_MORE"] = "閱讀更多";
$MESS["CONFIG_MP_ALLOW_USER_INSTALL"] = "允許用戶從MarketPlace24安裝應用程序";
$MESS["CONFIG_MP_ALLOW_USER_INSTALL1"] = "允許用戶安裝應用程序";
$MESS["CONFIG_NAME_CHANGE_ACTION"] = "改變";
$MESS["CONFIG_NAME_CHANGE_INFO"] = "注意：您只能更改Bitrix24地址一次。";
$MESS["CONFIG_NAME_CHANGE_SECTION"] = "更改您的Bitrix24地址";
$MESS["CONFIG_NAME_CURRENT_MAP_PROVIDER"] = "當前地圖提供商";
$MESS["CONFIG_NAME_FILEMAN_GOOGLE_API_KEY"] = "Google地圖密鑰在網站Explorer模塊設置中指定";
$MESS["CONFIG_NAME_FILEMAN_YANDEX_MAP_API_KEY"] = "Yandex地圖密鑰";
$MESS["CONFIG_NAME_FORMAT"] = "名稱格式";
$MESS["CONFIG_NAME_GOOGLE_API_HOST_HINT"] = "為域<b> #domain＃</b>獲得了鍵。如果您無法使Google Maps工作，請更改鍵設置或<a href = \'https：//developers.google.com/maps/documentation/javascript/get-api-key \“ target = \ \” _ _ _空白\” >獲得一個新的</a>。";
$MESS["CONFIG_NAME_GOOGLE_API_KEY"] = "Google API集成偏好";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_FIELD"] = "Google MAPS API用於BITRIX24集成密鑰";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_FIELD2"] = "Google Maps JavaScript API，位置API和地理編碼API瀏覽器密鑰";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_HINT"] = "需要使用Google API密鑰才能使用地圖。要獲取您的鑰匙，請<a href= \"https://developers.google.com/maps/documentation/javascript/get-api-key \“target= \”_blank \">使用此表單</a > 。";
$MESS["CONFIG_NAME_MAP_PROVIDER_SETTINGS"] = "＃提供商＃提供商設置";
$MESS["CONFIG_NETWORK_AVAILABLE"] = "允許我的Bitrix24的用戶在全局Bitrix24網絡中通信";
$MESS["CONFIG_NETWORK_AVAILABLE_NOT_CONFIRMED"] = "一旦管理員確認了帳戶，此功能將可用。";
$MESS["CONFIG_NETWORK_AVAILABLE_TEXT_NEW"] = "Bitrix24.network中的通信僅適用於商業Bitrix24用戶。<br/> <br/>
這是連接BitRix24.network的優點：<br/>
<ul>
<li>您的所有業務聯繫人和合作夥伴都連接在一個網絡中</li>
<li>與客戶和外部用戶的快速方便通信</li>
<li>不同Bitrix24帳戶中用戶之間的無縫通信</li>
</ul>
<b>所有bitrix24.network工具都可以從Bitrix24 Plus以＃price＃＃個月＃開始。</b>";
$MESS["CONFIG_NETWORK_AVAILABLE_TITLE"] = "僅在Bitrix24的商業計劃中可用";
$MESS["CONFIG_ORGANIZATION"] = "您的組織類型";
$MESS["CONFIG_ORGANIZATION_DEF"] = "公司和員工";
$MESS["CONFIG_ORGANIZATION_GOV"] = "組織和員工";
$MESS["CONFIG_ORGANIZATION_PUBLIC"] = "組織和用戶";
$MESS["CONFIG_OTP_ADMIN_IS_REQUIRED_INFO"] = "在將員工遷移到兩步身份驗證系統之前，請先將其設置為您的帳戶。<br/> <br/>請繼續在個人資料的安全頁面上啟用它。";
$MESS["CONFIG_OTP_IMPORTANT_TEXT"] = "今天，您使用登錄名和密碼登錄到Bitrix24。業務和個人數據受數據加密技術保護。但是，惡意人可以使用一些工具來進入您的計算機並竊取您的登錄憑據。

為了防止可能的威脅，我們為Bitrix24：兩步身份驗證啟用了額外的安全功能。

兩步身份驗證是一種特殊的方法，可以防止黑客軟件，尤其是密碼盜竊。每次登錄系統時，您都必須通過兩個級別的驗證。首先，您將輸入電子郵件和密碼，然後輸入從移動設備獲得的一次性安全代碼。

即使任何員工的登錄和密碼被攻擊者偷走了，這也將使我們的業務數據安全。

您有5天的時間啟用此功能。

要配置新的身份驗證過程，請轉到您的個人資料，然後選擇\“安全設置\”頁面。

如果您沒有合適的移動設備可以運行該應用程序，或者您遇到任何類型的問題，請通過對此帖子發表評論，讓我們知道。";
$MESS["CONFIG_OTP_IMPORTANT_TITLE"] = "兩步身份驗證";
$MESS["CONFIG_OTP_POPUP_CLOSE"] = "不，謝謝";
$MESS["CONFIG_OTP_POPUP_SHARE"] = "分享";
$MESS["CONFIG_OTP_POPUP_TEXT"] = "這是您可以發佈到活動流的文字<br/>供員工閱讀。<br/> <br/>
讓您的同事知道兩步身份驗證，<br/>配置過程和新的身份驗證方法<br/>他們將不得不使用將其登錄到其Bitrix24。";
$MESS["CONFIG_OTP_POPUP_TITLE"] = "將此張貼到活動流中怎麼樣？";
$MESS["CONFIG_OTP_SECURITY"] = "為所有用戶啟用兩步身份驗證";
$MESS["CONFIG_OTP_SECURITY2"] = "為所有用戶進行兩步授權";
$MESS["CONFIG_OTP_SECURITY_DAYS"] = "指定所有員工必須啟用兩步身份驗證的時間段";
$MESS["CONFIG_OTP_SECURITY_INFO"] = "我們已經開發了一個用戶友好的兩步身份驗證啟用程序，任何員工都可以在沒有專家幫助的情況下採取的過程。<br/> <br/>
將向每個員工發送一條消息，通知他們他們將必須在輸入的時間段內啟用兩步身份驗證。無法做的用戶
因此，將不再允許登錄。";
$MESS["CONFIG_OTP_SECURITY_INFO_1"] = "<br/>要啟用兩步身份驗證，用戶必須將Bitrix24 OTP應用程序安裝到其手機上。該應用程序可以從AppStore或GooglePlay下載。<br/> <br/>";
$MESS["CONFIG_OTP_SECURITY_INFO_2"] = "沒有合適的移動設備的員工必須配備特殊的硬件 -  eToken通行證。您可以從中獲得多種供應商，例如：
<a target = _blank href= \"http://www.safenet-inc.com/multi-factor-authentication/authenticators/pki-usb-authentication/Etoken-pro/ etoken-pro/ \ \"> www.safenet- inc.com < /a>，
<a target=_blank href= \"http://www.authguard.com/etoken-pass.asp.asp \"> www.authguard.com </a>。
在<a target=_blank href = \"https://www.google.com/?q=buy+Eteken+passs＆spell = safe = = = buy+eToken+passe+\"> google </google </google </a上查找其他供應商>。
<br/> <br/>";
$MESS["CONFIG_OTP_SECURITY_INFO_3"] = "另外，您可以為某些員工禁用兩步身份驗證。但是，如果其中一名員工的登錄和密碼被盜，這將增加未經授權訪問您的Bitrix24的風險。作為管理員，您可以禁用用戶配置文件中員工的兩步身份驗證。";
$MESS["CONFIG_OTP_SECURITY_SWITCH_OFF_INFO"] = "請注意，取消選中的選項不會停止使用已經啟用了兩步身份驗證的用戶使用一次性密碼。
<br/> <br/>登錄到Bitrix24時，他們仍然必須使用OTP。
<br/> <br/>您可以在相應的用戶配置文件中禁用兩步性身份驗證。";
$MESS["CONFIG_PHONE_NUMBER_DEFAULT_COUNTRY"] = "電話號碼格式：默認國家 /地區";
$MESS["CONFIG_SAVE"] = "節省";
$MESS["CONFIG_SAVE_SUCCESSFULLY"] = "設置已成功更新";
$MESS["CONFIG_SEND_OTP_PUSH"] = "發送有關身份驗證嘗試的聊天和推送通知，<br>還包括代碼中的符號（僅適用於基於時間的OTP）";
$MESS["CONFIG_SHOW_FIRED_EMPLOYEES"] = "表演被解僱的員工";
$MESS["CONFIG_SHOW_YEAR"] = "在用戶個人資料中顯示出生年份";
$MESS["CONFIG_SHOW_YEAR_FOR_FEMALE"] = "顯示女性概況的出生日期";
$MESS["CONFIG_STRESSLEVEL_AVAILABLE"] = "允許在用戶配置文件中測量和顯示壓力水平";
$MESS["CONFIG_TIME_FORMAT"] = "時間格式";
$MESS["CONFIG_TIME_FORMAT_12"] = "12小時";
$MESS["CONFIG_TIME_FORMAT_24"] = "24小時";
$MESS["CONFIG_TOALL_DEL"] = "刪除";
$MESS["CONFIG_TOALL_RIGHTS"] = "\“所有員工\”選項的設置";
$MESS["CONFIG_TOALL_RIGHTS_ADD"] = "添加";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_CLICK"] = "跟踪鏈接點擊在傳出電子郵件中";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_CLICK_HINT"] = "CRM觸發器可以跟踪用戶鏈接點擊。
使用這些觸發器，您可以創建自動化規則以在客戶或員工打開鏈接時發送通知。";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_READ"] = "跟踪讀取電子郵件的狀態";
$MESS["CONFIG_TRACK_OUTGOING_EMAILS_READ_HINT"] = "該系統將跟踪是否實際是否由其各自的收件人打開，並將這些消息標記為讀取。
此選項可用於從CRM，CRM營銷，BITRIX24網絡郵件和自動化規則發送的電子郵件。它還啟用\“電子郵件查看\”觸發器。";
$MESS["CONFIG_URL_PREVIEW_ENABLE"] = "啟用豐富的媒體鏈接";
$MESS["CONFIG_WEBDAV_ALLOW_AUTOCONNECT_SHARE_GROUP_FOLDER"] = "當用戶加入組時，會自動連接組驅動器";
$MESS["CONFIG_WEBDAV_SERVICES_GLOBAL"] = "通過外部<br/>服務激活文檔編輯（Google文檔，MS Office Online和其他）";
$MESS["CONFIG_WEBDAV_SERVICES_LOCAL"] = "允許單個用戶和小組通過外部服務激活<br/>文檔編輯";
$MESS["CONFIG_WEEK_HOLIDAYS"] = "週末";
$MESS["CONFIG_WEEK_START"] = "一周的第一天";
$MESS["CONFIG_WORK_TIME"] = "工作時間參數";
$MESS["CONFIG_YEAR_HOLIDAYS"] = "週末和假期";
$MESS["DAY_OF_WEEK_0"] = "星期日";
$MESS["DAY_OF_WEEK_1"] = "週一";
$MESS["DAY_OF_WEEK_2"] = "週二";
$MESS["DAY_OF_WEEK_3"] = "週三";
$MESS["DAY_OF_WEEK_4"] = "週四";
$MESS["DAY_OF_WEEK_5"] = "星期五";
$MESS["DAY_OF_WEEK_6"] = "週六";
$MESS["config_rating_label_likeN"] = "投票\ \“喜歡\”按鈕文字";
$MESS["config_rating_label_likeY"] = "未打擊\“喜歡\”按鈕文本";
