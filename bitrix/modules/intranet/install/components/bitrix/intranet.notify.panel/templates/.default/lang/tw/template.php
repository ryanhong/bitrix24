<?php
$MESS["INTRANET_NOTIFY_PANEL_FOOTER_LICENSE_NOTIFICATION_TEXT"] = "您的許可已過期。如果您在<b> #block_date＃</b>之前未能續訂許可證，則將無法使用BitRix24。 <a href= \“#link_buy# \" target= \"_blank \">現在續訂許可證</a> <a href= \ \"#article_link# \" target= \"_blank \"_blank \">了解更多</a </a >";
$MESS["INTRANET_NOTIFY_PANEL_LICENSE_NOTIFICATION_BUTTON_MORE"] = "細節";
$MESS["INTRANET_NOTIFY_PANEL_LICENSE_NOTIFICATION_BUTTON_PARTNER"] = "聯繫您的伴侶";
$MESS["INTRANET_NOTIFY_PANEL_LICENSE_NOTIFICATION_BUTTON_RENEW_LICENSE"] = "立即付款並延長許可證";
$MESS["INTRANET_NOTIFY_PANEL_LICENSE_NOTIFICATION_DESCRIPTION_1"] = "您的許可將很快到期。如果您未能及時擴展許可證，則將無法使用BitRix24。";
$MESS["INTRANET_NOTIFY_PANEL_LICENSE_NOTIFICATION_DESCRIPTION_2"] = "您可以自己擴展許可證，也可以讓Bitrix合作夥伴為您做。";
$MESS["INTRANET_NOTIFY_PANEL_LICENSE_NOTIFICATION_EXPIRED_DESCRIPTION_1"] = "請注意，您的許可已過期。如果在＃Day_month＃之前未能擴展許可證，則將無法使用BitRix24。";
$MESS["INTRANET_NOTIFY_PANEL_LICENSE_NOTIFICATION_TITLE"] = "您的BITRIX24許可將在＃Day_month＃上過期。請記住要擴展您的許可證。";
$MESS["INTRANET_NOTIFY_PANEL_LICENSE_NOTIFICATION_TITLE_EXPIRED"] = "您的Bitrix24許可證已過期。請在＃Day_month＃之前擴展您的許可證，以確保您的Bitrix24啟動並運行。";
