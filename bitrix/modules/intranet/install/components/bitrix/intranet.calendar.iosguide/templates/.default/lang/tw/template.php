<?php
$MESS["CAL_IOS_GUIDE_INSTALL_PROFILE"] = "安裝配置文件";
$MESS["CAL_IOS_GUIDE_INSTALL_PROFILE_IN_SETTINGS"] = "在電話設置中，安裝您下載的個人資料";
$MESS["CAL_IOS_GUIDE_IS_INSTALLED_ON_DEVICE"] = "Bitrix24日曆現在與您的手機日曆同步！";
$MESS["CAL_IOS_GUIDE_IS_SUCCESS"] = "成功！";
$MESS["CAL_IOS_GUIDE_PREVIEW_INFO"] = "要在iOS設備上同步Bitrix24日曆，請在設備上下載並安裝BitRix24配置文件。該配置文件包括同步移動日曆所需的所有參數。";
$MESS["CAL_IOS_GUIDE_PROFILE_DOWNLOAD"] = "下載個人資料";
$MESS["CAL_IOS_GUIDE_PROFILE_DOWNLOAD_BITRIX"] = "下載Bitrix24個人資料";
$MESS["CAL_IOS_GUIDE_READ_HOW_IT_WORK"] = "怎麼運行的";
$MESS["EC_CALENDAR_IOS_GUIDE_IMAGEFILE"] = "ios_guide_en.gif";
