<?php
$MESS["CMDESKTOP_DEMO_DATA_BLOCK_DESC"] = "要從您的門戶網站刪除演示數據，請使用<b>清理嚮導</b>。您可以在瀏覽模式下單擊“清理嚮導”工具欄上的“清理嚮導”，或使用<a href='#link_to_wizard#'>此鏈接</a>。";
$MESS["CMDESKTOP_DEMO_DATA_BLOCK_TITLE"] = "清除演示數據";
$MESS["CMDESKTOP_DESC_NAME"] = "個人儀表板";
$MESS["CMDESKTOP_TDEF_ADD"] = "添加小工具";
$MESS["CMDESKTOP_TDEF_CANCEL"] = "取消";
$MESS["CMDESKTOP_TDEF_CLEAR"] = "重置當前設置";
$MESS["CMDESKTOP_TDEF_CLEAR_CONF"] = "默認參數將應用於您的桌面。繼續？";
$MESS["CMDESKTOP_TDEF_CONF"] = "您的個人儀表板設置默認情況下將用於所有新的或未授權的用戶。繼續？";
$MESS["CMDESKTOP_TDEF_CONF_GROUP"] = "默認情況下，您的桌面設置將應用於所有新工作組。你想繼續嗎？";
$MESS["CMDESKTOP_TDEF_CONF_USER"] = "默認情況下，您的桌面設置將應用於所有新的用戶配置文件。你想繼續嗎？";
$MESS["CMDESKTOP_TDEF_DELETE"] = "刪除";
$MESS["CMDESKTOP_TDEF_ERR1"] = "在將小工具位置保存到服務器的同時錯誤。";
$MESS["CMDESKTOP_TDEF_ERR2"] = "在將小工具添加到服務器時出錯。";
$MESS["CMDESKTOP_TDEF_HIDE"] = "隱藏/顯示";
$MESS["CMDESKTOP_TDEF_SET"] = "另存為默認設置";
$MESS["CMDESKTOP_TDEF_SETTINGS"] = "設定";
