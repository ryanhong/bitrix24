<?php
$MESS["MENU_ADD_TO_FAVORITE"] = "添加到我的工作區";
$MESS["MENU_ADD_TO_FAVORITE_ALL"] = "為所有用戶添加到我的工作區";
$MESS["MENU_DELETE_FROM_FAVORITE"] = "從我的工作區中刪除";
$MESS["MENU_DELETE_FROM_FAVORITE_ALL"] = "從我的工作區中刪除所有用戶";
$MESS["MENU_EDIT_READY"] = "好的";
$MESS["MENU_HIDDEN_ITEMS"] = "隱";
$MESS["MENU_HIDE"] = "隱藏";
$MESS["MENU_HIDE_ITEM"] = "隱藏項目";
$MESS["MENU_ITEM_ACCESS_DENIED"] = "拒絕訪問";
$MESS["MENU_ITEM_EDIT_ERROR"] = "錯誤編輯項目。";
$MESS["MENU_ITEM_SET_RIGHTS"] = "設置訪問權限";
$MESS["MENU_MORE_ITEMS_HIDE"] = "隱藏";
$MESS["MENU_MORE_ITEMS_SHOW"] = "更多的...";
$MESS["MENU_SETTINGS_TITLE"] = "配置菜單";
$MESS["MENU_SHOW"] = "展示";
$MESS["MENU_SHOW_ITEM"] = "顯示項目";
