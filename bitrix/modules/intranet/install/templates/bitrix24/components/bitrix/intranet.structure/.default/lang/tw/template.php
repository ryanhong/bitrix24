<?php
$MESS["INTR_IS_TPL_ACTIONS"] = "動作";
$MESS["INTR_IS_TPL_ACTION_ADD_DEP"] = "添加部門";
$MESS["INTR_IS_TPL_ACTION_DELETE_DEP"] = "刪除部門";
$MESS["INTR_IS_TPL_ACTION_EDIT_DEP"] = "編輯部門";
$MESS["INTR_IS_TPL_ACTION_INVITE"] = "邀請用戶";
$MESS["INTR_IS_TPL_EMPLOYEES"] = "僱員";
$MESS["INTR_IS_TPL_HEAD"] = "分配管理器";
$MESS["INTR_IS_TPL_OUTLOOK"] = "出口員工到Outlook";
$MESS["INTR_IS_TPL_OUTLOOK_TITLE"] = "出口員工列表作為Outlook聯繫人";
$MESS["INTR_IS_TPL_SEARCH"] = "找到員工";
$MESS["INTR_IS_TPL_SEARCH_DEPARTMENT"] = "在這個部門找到員工";
$MESS["INTR_IS_TPL_SUB_DEP"] = "部門";
$MESS["INTR_IS_TPL_SUB_DEPARTMENTS"] = "子部門";
$MESS["INTR_IS_TPL_SUB_DEP_HEAD"] = "導師";
$MESS["ISV_confirm_delete_department"] = "您確定要刪除部門嗎？這將移動所有基礎子部門和員工。";
