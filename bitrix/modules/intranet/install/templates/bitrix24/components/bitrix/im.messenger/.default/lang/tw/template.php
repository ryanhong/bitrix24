<?php
$MESS["IM_FULLSCREEN_APPS"] = "下載應用程序";
$MESS["IM_FULLSCREEN_BACK"] = "關閉";
$MESS["IM_FULLSCREEN_BG_0"] = "Bitrix24";
$MESS["IM_FULLSCREEN_BG_1"] = "石頭";
$MESS["IM_FULLSCREEN_BG_2"] = "車";
$MESS["IM_FULLSCREEN_BG_3"] = "路";
$MESS["IM_FULLSCREEN_BG_4"] = "沙子";
$MESS["IM_FULLSCREEN_BG_5"] = "自然";
$MESS["IM_FULLSCREEN_BG_6"] = "山";
$MESS["IM_FULLSCREEN_BG_7"] = "乾草";
$MESS["IM_FULLSCREEN_BG_8"] = "山湖";
$MESS["IM_FULLSCREEN_BG_9"] = "遊艇";
$MESS["IM_FULLSCREEN_BG_10"] = "巴黎";
$MESS["IM_FULLSCREEN_BG_11"] = "海灘";
$MESS["IM_FULLSCREEN_BG_12"] = "天空";
$MESS["IM_FULLSCREEN_BG_NUMBER"] = "＃";
$MESS["IM_FULLSCREEN_BG_TITLE"] = "背景";
$MESS["IM_FULLSCREEN_BG_TRANSPARENT"] = "透明的";
$MESS["IM_FULLSCREEN_COPYRIGHT"] = "＆複製; 2001-2020 Bitrix24";
$MESS["IM_FULLSCREEN_DOWN_ITS"] = "https://itunes.apple.com/en/app/bitrix24/id561683423";
$MESS["IM_HELP_SPOTLIGHT"] = "在此處找到有關Bitrix24問題的答案";
$MESS["IM_MESSENGER_CREATE_CHAT"] = "創建新聊天";
$MESS["IM_MESSENGER_GO_TO_APPS"] = "下載桌面和移動應用程序";
$MESS["IM_MESSENGER_GO_TO_APPS_LINK"] = "https://www.bitrix24.com/apps/mobile-and-desktop-apps.php";
$MESS["IM_MESSENGER_ONLINE"] = "在線的";
$MESS["IM_MESSENGER_OPEN_CALL2"] = "打個電話";
$MESS["IM_MESSENGER_OPEN_EMAIL"] = "查看電子郵件";
$MESS["IM_MESSENGER_OPEN_MESSENGER"] = "打開聊天和電話";
$MESS["IM_MESSENGER_OPEN_MESSENGER_CP"] = "打開Web Messenger";
$MESS["IM_MESSENGER_OPEN_MOBILE"] = "下載免費的移動應用程序";
$MESS["IM_MESSENGER_OPEN_NETWORK"] = "轉到bitrix24.network";
$MESS["IM_MESSENGER_OPEN_OL"] = "查看開放頻道聊天";
$MESS["IM_MESSENGER_OPEN_SEARCH"] = "搜索員工和聊天";
