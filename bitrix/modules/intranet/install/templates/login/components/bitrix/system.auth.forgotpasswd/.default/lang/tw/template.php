<?php
$MESS["AUTH_AUTH"] = "授權";
$MESS["AUTH_CAPTCHA_PROMT"] = "從圖像輸入文字";
$MESS["AUTH_EMAIL"] = "電子郵件";
$MESS["AUTH_FORGOT_EMAIL_LINK"] = "我不記得我的電子郵件地址";
$MESS["AUTH_FORGOT_EMAIL_TITLE"] = "恢復電子郵件地址";
$MESS["AUTH_FORGOT_PASSWORD_1"] = "如果您忘記了密碼，請輸入電子郵件地址或登錄。<br>您的帳戶信息將通過電子郵件以及代碼發送給您以重置密碼。";
$MESS["AUTH_GET_CHECK_STRING"] = "重設密碼";
$MESS["AUTH_LOGIN"] = "登入";
$MESS["AUTH_OR"] = "或者";
$MESS["AUTH_SEND"] = "發送";
