<?php
$MESS["XDI_INSTALL_TITLE"] = "\“外部數據導入\”模塊安裝";
$MESS["XDI_MODULE_DESCRIPTION"] = "提供用於從外部來源導入數據的工具";
$MESS["XDI_MODULE_NAME"] = "外部數據導入";
$MESS["XDI_UNINSTALL_TITLE"] = "\“外部數據導入\”模塊卸載";
