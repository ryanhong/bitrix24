<?php
$MESS["ERR_BLOG_MODULE_NOT_INSTALLED"] = "沒有安裝博客模塊。";
$MESS["ERR_IBLOCK_MODULE_NOT_INSTALLED"] = "\“信息塊\”模塊未安裝。";
$MESS["ERR_IDEA_INSTALL_TITLE"] = "安裝Idea Manager模塊的錯誤。";
$MESS["ERR_SESSION_EXPIRED"] = "您的會議已經過期。請嘗試再次安裝Idea Manager模塊。";
$MESS["IDEA_INSTALL_TITLE"] = "想法模塊安裝";
$MESS["IDEA_MODULE_DESCRIPTION"] = "想法經理";
$MESS["IDEA_MODULE_NAME"] = "想法經理";
$MESS["IDEA_UF_ANSWER_ID_DESCRIPTION"] = "官方答复ID";
$MESS["IDEA_UF_CATEGORY_CODE_DESCRIPTION"] = "類別";
$MESS["IDEA_UF_ORIGINAL_ID_DESCRIPTION"] = "複製";
$MESS["IDEA_UF_STATUS_COMPLETED_TITLE"] = "實施的";
$MESS["IDEA_UF_STATUS_DESCRIPTION"] = "地位";
$MESS["IDEA_UF_STATUS_NEW_TITLE"] = "新的";
$MESS["IDEA_UF_STATUS_PROCESSING_TITLE"] = "進行中";
