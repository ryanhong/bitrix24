<?php
$MESS["ADD_IDEA_COMMENT_TEMPLATE"] = "來自＃site_name＃的消息
-------------------------------------------------- --------

＃server_name＃添加了一個新的評論。

思想主題：
＃indue_title＃

作者：＃作者＃
評論添加：＃date_create＃
評論文字：

＃post_text＃

評論URL：
＃完整路徑＃

此消息已自動生成。
";
$MESS["ADD_IDEA_TEMPLATE"] = "來自＃site_name＃的消息
-------------------------------------------------- --------

在＃類別＃中添加了一個新想法。

主題標題＃

作者：＃作者＃

在：＃date_publish＃上創建

描述：
＃indue_text＃

創意URL：
＃完整路徑＃

此消息已自動生成。
";
$MESS["IDEA_EVENT_ADD_IDEA"] = "添加了新想法";
$MESS["IDEA_EVENT_ADD_IDEA_COMMENT"] = "想法添加了新評論";
$MESS["IDEA_EVENT_ADD_IDEA_COMMENT_PARAM_AUTHOR"] = "評論作者";
$MESS["IDEA_EVENT_ADD_IDEA_COMMENT_PARAM_DATE_CREATE"] = "評論添加了";
$MESS["IDEA_EVENT_ADD_IDEA_COMMENT_PARAM_EMAIL_TO"] = "訂戶電子郵件";
$MESS["IDEA_EVENT_ADD_IDEA_COMMENT_PARAM_FULL_PATH"] = "評論路徑";
$MESS["IDEA_EVENT_ADD_IDEA_COMMENT_PARAM_IDEA_COMMENT_TEXT"] = "評論文字";
$MESS["IDEA_EVENT_ADD_IDEA_COMMENT_PARAM_IDEA_TITLE"] = "想法標題";
$MESS["IDEA_EVENT_ADD_IDEA_PARAM_AUTHOR"] = "創意作者";
$MESS["IDEA_EVENT_ADD_IDEA_PARAM_CATEGORY"] = "類別";
$MESS["IDEA_EVENT_ADD_IDEA_PARAM_DATE_PUBLISH"] = "創建了日期和時間的想法";
$MESS["IDEA_EVENT_ADD_IDEA_PARAM_EMAIL_TO"] = "訂戶電子郵件";
$MESS["IDEA_EVENT_ADD_IDEA_PARAM_FULL_PATH"] = "想法完整的道路";
$MESS["IDEA_EVENT_ADD_IDEA_PARAM_IDEA_TEXT"] = "想法文字";
$MESS["IDEA_EVENT_ADD_IDEA_PARAM_TITLE"] = "想法標題";
