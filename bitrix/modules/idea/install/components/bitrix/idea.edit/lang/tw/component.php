<?php
$MESS["BLOG_ERR_NO_RIGHTS"] = "請授權添加一個想法。";
$MESS["BLOG_MODULE_NOT_INSTALL"] = "沒有安裝博客模塊。";
$MESS["BLOG_POST_EDIT"] = "編輯IDEA \“＃indue_title＃\”";
$MESS["BLOG_P_INSERT"] = "點擊插入圖像";
$MESS["BPE_HIDDEN_POSTED"] = "您的提交已添加。此博客中的評論已預處理；一旦博客所有者批准它，您的評論就會變得可見。";
$MESS["BPE_SESS"] = "您的會議已經過期。請再次保存消息。";
$MESS["B_B_MES_NO_BLOG"] = "沒有找到博客。";
$MESS["IDEA_MODULE_NOT_INSTALL"] = "想法模塊未安裝。";
$MESS["IDEA_NEW_MESSAGE"] = "建議新主意！";
$MESS["IDEA_NEW_MESSAGE_SUCCESS"] = "新消息已成功添加。";
