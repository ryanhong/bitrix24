<?php
$MESS["RMP_ACCESS_DENIED"] = "拒絕訪問。聯繫您的管理員以安裝應用程序。";
$MESS["RMP_ERROR_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["RMP_ERROR_SUBSCRIPTION_REQUIRED"] = "Bitrix24。MarketPlus訂閱需要繼續安裝應用程序";
$MESS["RMP_ERROR_VERIFICATION_NEEDED"] = "錯誤驗證許可證。請在10分鐘內重試。";
$MESS["RMP_INSTALL_ERROR"] = "錯誤！該應用程序未安裝。";
$MESS["RMP_NOT_FOUND"] = "找不到申請。";
$MESS["RMP_TRIAL_HOLD_INSTALL"] = "拒絕訪問。 Bitrix24。MarketPlus訂閱需要繼續安裝應用程序";
