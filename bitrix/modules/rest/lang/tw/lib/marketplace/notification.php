<?php
$MESS["REST_MARKETPLACE_NOTIFICATION_HOLD_REST_OVERLOAD_BTN"] = "看法";
$MESS["REST_MARKETPLACE_NOTIFICATION_HOLD_REST_OVERLOAD_MESS"] = "REST API已部分阻塞。請聯繫HelpDesk解決問題。 ＃BTN＃";
$MESS["REST_MARKETPLACE_NOTIFICATION_REST_BUY_BTN"] = "細節";
$MESS["REST_MARKETPLACE_NOTIFICATION_REST_BUY_MESS"] = "截至2021年1月1日，Bitrix24。市場和休息僅在商業計劃上可用。選擇最適合您的計劃並獲得40％的折扣。 ＃BTN＃";
$MESS["REST_MARKETPLACE_NOTIFICATION_REST_BUY_URL"] = "https://www.bitrix24.com/promo/sales/holiday-sale/";
$MESS["REST_MARKETPLACE_NOTIFICATION_SUBSCRIPTION_MARKET_DEMO_END_BTN"] = "選擇方案";
$MESS["REST_MARKETPLACE_NOTIFICATION_SUBSCRIPTION_MARKET_DEMO_END_MESS"] = "Bitrix24.Market和Rest試用期已經過期。您可以在任何商業計劃中最多安裝2個免費申請。 ＃BTN＃";
$MESS["REST_MARKETPLACE_NOTIFICATION_SUBSCRIPTION_MARKET_DEMO_END_URL"] = "/settings/license_all.php";
$MESS["REST_MARKETPLACE_NOTIFICATION_SUBSCRIPTION_MARKET_TARIFF_MARKET_BTN"] = "選擇方案";
$MESS["REST_MARKETPLACE_NOTIFICATION_SUBSCRIPTION_MARKET_TARIFF_MARKET_MESS"] = "Bitrix24.Market和Rest試用期已經過期。請升級到繼續使用已安裝或其他應用程序的商業計劃之一。 ＃BTN＃";
$MESS["REST_MARKETPLACE_NOTIFICATION_SUBSCRIPTION_MARKET_TARIFF_MARKET_URL"] = "/settings/license_all.php";
$MESS["REST_MARKETPLACE_NOTIFICATION_SUBSCRIPTION_MARKET_TRIAL_END_BTN"] = "購買訂閱";
$MESS["REST_MARKETPLACE_NOTIFICATION_SUBSCRIPTION_MARKET_TRIAL_END_MESS"] = "Bitrix24.Market和Rest試用期已經過期。請升級到繼續使用所有應用程序的商業計劃之一。 ＃BTN＃";
$MESS["REST_MARKETPLACE_NOTIFICATION_SUBSCRIPTION_MARKET_TRIAL_END_URL"] = "/settings/license_buy.php?product=subscr";
