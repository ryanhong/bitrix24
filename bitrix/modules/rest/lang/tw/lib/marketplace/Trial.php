<?php
$MESS["REST_MARKET_ACTIVATE_DEMO_ACCESS_DENIED"] = "權限不足。請聯繫您的BitRix24管理員。";
$MESS["REST_MARKET_ACTIVATE_DEMO_NOT_AVAILABLE"] = "試驗期不可用。";
$MESS["REST_MARKET_CONFIG_ACTIVATE_ERROR"] = "激活錯誤。請聯繫<a href='mailto:sales@bitrix24.com'>銷售部門</a>。";
