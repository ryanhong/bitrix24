<?php
$MESS["REST_CONFIGURATION_ERROR_INSTALL_APP_CONTENT"] = "錯誤安裝其他應用程序";
$MESS["REST_CONFIGURATION_ERROR_INSTALL_APP_CONTENT_DATA"] = "錯誤安裝附加應用程序：＃error_message＃（＃error_code＃）";
$MESS["REST_CONFIGURATION_ERROR_UNKNOWN_APP"] = "安裝其他應用程序時檢測到的未知應用程序。請聯繫應用程序或配置文件的創建者。";
