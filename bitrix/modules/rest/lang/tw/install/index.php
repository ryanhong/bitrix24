<?php
$MESS["REST_IBLOCK_ELEMENT_NAME"] = "專案";
$MESS["REST_IBLOCK_NAME_2"] = "應用程序數據存儲";
$MESS["REST_IBLOCK_SECTION_NAME"] = "部分";
$MESS["REST_INSTALL_TITLE"] = "REST API模塊安裝";
$MESS["REST_MODULE_DESCRIPTION"] = "用於分佈式環境中的應用程序的Web界面。";
$MESS["REST_MODULE_NAME"] = "REST API";
$MESS["REST_MOD_REWRITE_ERROR"] = "需要模塊mod_rewrite才能與Apache正確運行。";
$MESS["REST_UNINSTALL_TITLE"] = "REST API模塊卸載";
