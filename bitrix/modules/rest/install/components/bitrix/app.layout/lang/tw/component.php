<?php
$MESS["REST_AL_ERROR_APP_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["REST_AL_ERROR_APP_GET_OAUTH_TOKEN"] = "接收身份驗證令牌的錯誤";
$MESS["REST_AL_ERROR_APP_INSTALL_NOT_FINISH"] = "該應用程序尚未完全安裝。請聯繫您的BitRix24管理員以完成安裝。";
$MESS["REST_AL_ERROR_APP_NOT_ACCESSIBLE"] = "您無權訪問此應用程序。請聯繫您的BitRix24管理員。";
$MESS["REST_AL_ERROR_APP_NOT_FOUND"] = "找不到申請。";
$MESS["REST_AL_ERROR_APP_NOT_FOUND_MARKETPLACE"] = "該應用程序未在目錄中發布，也沒有被開發人員刪除。";
$MESS["REST_AL_ERROR_APP_NOT_INSTALLED"] = "應用程序未安裝。請聯繫您的Intranet管理員。";
$MESS["REST_AL_ERROR_APP_PLACEMENT_NOT_INSTALLED"] = "無法在當前上下文中顯示該應用程序。";
