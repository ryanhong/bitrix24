<?php
$MESS["REST_CONFIGURATION_INSTALL_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["REST_CONFIGURATION_INSTALL_CLEAR_PREFIX_NAME"] = "老的";
$MESS["REST_CONFIGURATION_INSTALL_DISK_FOLDER_NOT_FOUND"] = "找不到驅動文件夾";
$MESS["REST_CONFIGURATION_INSTALL_ERROR_MANIFEST_NOT_FOUND"] = "導入文件可能包含無法導入BitRix24的數據。建議安裝最新的系統更新。";
$MESS["REST_CONFIGURATION_INSTALL_ERROR_MANIFEST_OLD"] = "導入文件可能包含無法導入BitRix24的數據。建議安裝最新的系統更新。";
$MESS["REST_CONFIGURATION_INSTALL_FILE_CONTENT_ERROR_DECODE"] = "無法從文件##步驟＃解碼JSON數據";
$MESS["REST_CONFIGURATION_INSTALL_FILE_CONTENT_ERROR_SANITIZE"] = "文件##步驟＃的數據被標記為危險並跳過";
$MESS["REST_CONFIGURATION_INSTALL_FILE_CONTENT_ERROR_SANITIZE_SHORT"] = "文件##步驟＃的數據被標記為危險。";
