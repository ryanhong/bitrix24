<?php
$MESS["REST_CONFIGURATION_IMPORT_HOLD_CLOSE_POPUP_BTN_CLOSE"] = "現在流產";
$MESS["REST_CONFIGURATION_IMPORT_HOLD_CLOSE_POPUP_BTN_CONTINUE"] = "繼續進口";
$MESS["REST_CONFIGURATION_IMPORT_HOLD_CLOSE_POPUP_DESCRIPTION"] = "如果您現在選擇現在中止並稍後重新啟動，則導入過程將重新開始。";
$MESS["REST_CONFIGURATION_IMPORT_HOLD_CLOSE_POPUP_TITLE"] = "您確定要中止進口嗎？";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_ACCEPT_CLEAR"] = "我想用下載的CRM解決方案預設中的當前BITRIX24設置替換。";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_ACCEPT_CLEAR_ROLLBACK"] = "我想用所選CRM解決方案預設的當前Bitrix24設置替換當前的BitRix24設置。";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_APP_DESCRIPTION"] = "通過單擊\“繼續\”，您同意，導入的CRM解決方案預設可以更改一些或全部當前的BitRix24設置，此後無法恢復。";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_APP_DESCRIPTION_ROLLBACK"] = "通過單擊\“繼續\”，您同意刪除應用程序將重置某些或全部當前的BitRix24設置並刪除一些現有的CRM數據，並且此操作在此後無法進行。";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_CONFIRM_POPUP_BTN_CANCEL"] = "取消";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_CONFIRM_POPUP_BTN_CONTINUE"] = "繼續";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_CONFIRM_POPUP_CHECKBOX_LABEL"] = "是的我明白。刪除現有數據。";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_CONFIRM_POPUP_TEXT"] = "如果進行安裝，將刪除一些或全部現有數據。您要繼續刪除數據嗎？";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_DESCRIPTION"] = "通過單擊\“繼續\”，您同意，導入的CRM解決方案預設可以更改一些或全部當前的BitRix24設置，此後無法恢復。";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_DESCRIPTION_ROLLBACK"] = "通過單擊\“繼續\”，您同意恢復將重置某些或全部BitRix24設置並刪除一些現有的CRM數據，並且此後無法撤消此操作。";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_FATAL_ERROR"] = "內部錯誤。請刷新頁面，然後再試一次。如果問題仍然存在，請聯繫Helpdesk。";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_FINISH_TEXT"] = "選定的解決方案預設已成功導入";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_LATER_BTN"] = "我稍後會導入";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_LATER_POPUP_CLOSE_BTN"] = "好的";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_PROGRESSBAR_TITLE"] = "現在導入設置";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_START_BTN"] = "繼續";
$MESS["REST_CONFIGURATION_IMPORT_INSTALL_STEP_MSGVER_1"] = "我們現在正在安裝您選擇的模板。";
$MESS["REST_CONFIGURATION_IMPORT_PRE_INSTALL_APP_DESCRIPTION"] = "單擊\“繼續\”以安裝解決方案預設\“＃app_name＃\”。";
$MESS["REST_CONFIGURATION_IMPORT_PRE_INSTALL_LATER_APP_POPUP_DESCRIPTION"] = "您可以隨時安裝解決方案預設\“＃app_name＃\”，只需在解決方案列表中找到它即可。了解更多＃help_desk_link＃。";
$MESS["REST_CONFIGURATION_IMPORT_PRE_INSTALL_LATER_APP_POPUP_HELP_DESK_LINK_LABEL"] = "這裡";
