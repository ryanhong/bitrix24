<?php
$MESS["REST_MARKETPLACE_INSTALL_APP_AUTHOR"] = "開發人員：";
$MESS["REST_MARKETPLACE_INSTALL_APP_VERSION"] = "版本";
$MESS["REST_MARKETPLACE_INSTALL_BTN_CANCEL"] = "取消";
$MESS["REST_MARKETPLACE_INSTALL_BTN_INSTALL"] = "安裝";
$MESS["REST_MARKETPLACE_INSTALL_ERROR"] = "錯誤！該應用程序未安裝。";
$MESS["REST_MARKETPLACE_INSTALL_EULA_LINK"] = "https://www.1c-bitrix.ru/download/files/manuals/ru/oferta_fiz.html";
$MESS["REST_MARKETPLACE_INSTALL_EULA_TEXT"] = "我已經閱讀並同意<a href= \"#link# \" target= \"_blank \">許可協議</a>";
$MESS["REST_MARKETPLACE_INSTALL_HTTPS_WARNING"] = "<b>警告！</b> HTTPS協議才能正確運行已安裝的市場應用程序。";
$MESS["REST_MARKETPLACE_INSTALL_LICENSE_ERROR"] = "您必須閱讀並同意安裝應用程序的隱私政策";
$MESS["REST_MARKETPLACE_INSTALL_MODULE_UNINSTALL"] = "應用程序所需的一些服務已卸載。如果沒有這些服務，該應用程序將不會運行。請安裝所需的模塊。";
$MESS["REST_MARKETPLACE_INSTALL_MODULE_UNINSTALL_BITRIX24"] = "應用程序所需的一些服務已卸載。如果沒有這些服務，該應用程序將不會運行。打開<a href='#path_configs#'> Intranet設置頁面</a>以安裝它們。";
$MESS["REST_MARKETPLACE_INSTALL_PRIVACY_LINK"] = "https://www.bitrix24.com/privacy/";
$MESS["REST_MARKETPLACE_INSTALL_PRIVACY_TEXT"] = "我已經閱讀並同意<a href= \"#link# \" target= \"_blank \">隱私政策</a>";
$MESS["REST_MARKETPLACE_INSTALL_REQUIRED_RIGHTS"] = "該申請要求以下權限：";
$MESS["REST_MARKETPLACE_INSTALL_TERMS_OF_SERVICE_TEXT"] = "通過安裝或下載應用程序或解決方案，您同意<a href= \"#link# \" target= \"_blank \"> bitrix24.market使用條款</a>";
$MESS["REST_MARKETPLACE_INSTALL_TOS_ERROR"] = "您必須同意Bitrix24.Market使用條款來安裝應用程序";
