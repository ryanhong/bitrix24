<?php
$MESS["REST_HEVE_APPLICATION_TOKEN"] = "身份驗證代碼";
$MESS["REST_HEVE_APPLICATION_TOKEN_DESC"] = "此代碼將傳遞給您的處理程序<br />以確保從Bitrix24發出<br />呼叫。";
$MESS["REST_HEVE_APPLICATION_TOKEN_REGEN"] = "生成新";
$MESS["REST_HEVE_COMMENT"] = "評論";
$MESS["REST_HEVE_EVENT_HANDLER"] = "處理程序地址";
$MESS["REST_HEVE_EVENT_NAME"] = "事件類型";
$MESS["REST_HEVE_EVENT_NAME_DESC"] = "選擇事件類型<br ./>對於此事件處理程序";
$MESS["REST_HEVE_SAVE"] = "節省";
$MESS["REST_HEVE_SUCCESS"] = "修改已經保存。";
$MESS["REST_HEVE_TITLE"] = "姓名";
