<?php
$MESS["REST_HAPE_AP"] = "應用程序密碼";
$MESS["REST_HAPE_AP_EXAMPLE"] = "休息電話示例URL";
$MESS["REST_HAPE_AP_HIDDEN"] = "出於安全原因隱藏";
$MESS["REST_HAPE_AP_NOTE"] = "此代碼用於驗證Bitrix24中的Webhook。將其保存在安全的地方，並保持機密。";
$MESS["REST_HAPE_AP_NOT_READY"] = "一旦保存就可以使用。";
$MESS["REST_HAPE_COMMENT"] = "描述";
$MESS["REST_HAPE_DATE_CREATE"] = "創建於";
$MESS["REST_HAPE_DATE_LOGIN"] = "最後使用";
$MESS["REST_HAPE_DATE_LOGIN_NEVER"] = "絕不";
$MESS["REST_HAPE_HTTPS_ERROR"] = "注意力！該技術只能與HTTP一起使用。";
$MESS["REST_HAPE_HTTPS_WARNING"] = "注意力！如果沒有HTTP，就永遠不應使用該技術。";
$MESS["REST_HAPE_LAST_IP"] = "最後一個IP";
$MESS["REST_HAPE_SAVE"] = "節省";
$MESS["REST_HAPE_SCOPE"] = "訪問權限";
$MESS["REST_HAPE_SCOPE_DESC"] = "指定訪問權限<br/>用於用戶數據<br/>允許使用應用程序密碼";
$MESS["REST_HAPE_SUCCESS"] = "修改已經保存。";
$MESS["REST_HAPE_TITLE"] = "姓名";
