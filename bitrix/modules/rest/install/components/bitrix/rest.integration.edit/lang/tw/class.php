<?php
$MESS["REST_INTEGRATION_EDIT_ATTENTION_USES_WEBHOOK"] = "<strong>注意！</strong>此鏈接允許擁有該鏈接的人根據分配給鏈接的權限執行行動。請保留此鏈接秘密。了解更多＃url＃。";
$MESS["REST_INTEGRATION_EDIT_ATTENTION_USES_WEBHOOK_URL_MESSAGE"] = "這裡";
$MESS["REST_INTEGRATION_EDIT_ERROR_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["REST_INTEGRATION_EDIT_ERROR_CREAT"] = "錯誤創建集成：＃error_text＃";
$MESS["REST_INTEGRATION_EDIT_ERROR_NOT_FOUND"] = "找不到集成";
$MESS["REST_INTEGRATION_EDIT_ERROR_NO_HTTPS"] = "<strong>注意！</strong>該技術必須在HTTPS上使用。";
$MESS["REST_INTEGRATION_EDIT_ERROR_REQUIRED_MODULES"] = "此集成需要模塊：＃module_code＃";
$MESS["REST_INTEGRATION_EDIT_HOLD_DUE_TO_OVERLOAD"] = "<strong>注意！</strong>由於負載過多而阻止了該解決方案。請聯繫HelpDesk解決問題。";
