<?php
$MESS["REST_CRM_FF_CANCEL"] = "取消";
$MESS["REST_CRM_FF_CHANGE"] = "編輯";
$MESS["REST_CRM_FF_CHOISE"] = "選擇";
$MESS["REST_CRM_FF_CLOSE"] = "關閉";
$MESS["REST_CRM_FF_COMPANY"] = "公司";
$MESS["REST_CRM_FF_CONTACT"] = "聯繫人";
$MESS["REST_CRM_FF_DEAL"] = "交易";
$MESS["REST_CRM_FF_LAST"] = "最後的";
$MESS["REST_CRM_FF_LEAD"] = "鉛";
$MESS["REST_CRM_FF_NO_RESULT"] = "不幸的是，您的搜索請求沒有返回結果。";
$MESS["REST_CRM_FF_OK"] = "選擇";
$MESS["REST_CRM_FF_QUOTE"] = "引號";
$MESS["REST_CRM_FF_SEARCH"] = "搜尋";
