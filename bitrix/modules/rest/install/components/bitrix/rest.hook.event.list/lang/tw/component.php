<?php
$MESS["REST_EVENT_LIST_TITLE"] = "出站Webhooks";
$MESS["REST_HOOK_COMMENT"] = "評論";
$MESS["REST_HOOK_DATE_CREATE"] = "創建於";
$MESS["REST_HOOK_DELETE"] = "刪除";
$MESS["REST_HOOK_EDIT"] = "編輯";
$MESS["REST_HOOK_EVENT_HANDLER"] = "處理程序";
$MESS["REST_HOOK_EVENT_NAME"] = "事件";
$MESS["REST_HOOK_TITLE"] = "姓名";
