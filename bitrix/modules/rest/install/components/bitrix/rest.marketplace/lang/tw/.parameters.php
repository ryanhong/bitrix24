<?php
$MESS["RMP_APPLICATION_PAGE"] = "申請頁網址模板";
$MESS["RMP_SEF_BUY"] = "購買並安裝了應用程序";
$MESS["RMP_SEF_CATEGORY"] = "應用程序類別索引";
$MESS["RMP_SEF_DETAIL"] = "申請頁";
$MESS["RMP_SEF_SEARCH"] = "搜尋";
$MESS["RMP_SEF_TOP"] = "應用程序頂部";
$MESS["RMP_SEF_UPDATES"] = "更新";
$MESS["RMP_VA_CATEGORY"] = "應用程序類別";
$MESS["RMP_VA_CODE"] = "應用ID";
