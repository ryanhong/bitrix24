<?php
$MESS["LDAP_ERR_BASE_DN"] = "樹根（基本DN）";
$MESS["LDAP_ERR_EMPTY"] = "字段為空：";
$MESS["LDAP_ERR_GROUP_ATTR"] = "組標識符屬性";
$MESS["LDAP_ERR_GROUP_FILT"] = "用戶組過濾器";
$MESS["LDAP_ERR_NAME"] = "姓名";
$MESS["LDAP_ERR_PORT"] = "廣告/LDAP服務器端口";
$MESS["LDAP_ERR_SERVER"] = "廣告/LDAP服務器地址";
$MESS["LDAP_ERR_USER_ATTR"] = "用戶標識符屬性";
$MESS["LDAP_ERR_USER_FILT"] = "用戶過濾器";
