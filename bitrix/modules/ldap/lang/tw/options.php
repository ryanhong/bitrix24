<?php
$MESS["LDAP_BITRIXVM_BLOCK"] = "重定向NTLM身份驗證到端口8890和8891";
$MESS["LDAP_BITRIXVM_HINT"] = "在此處指定通過NTLM進行認證時將重定向的子網。 >將多個IP範圍分開，並用半分離。<br>將字段空白以重定向所有用戶。";
$MESS["LDAP_BITRIXVM_NET"] = "將NTLM重定向限制為此子網：";
$MESS["LDAP_BITRIXVM_SUPPORT"] = "重定向NTLM身份驗證";
$MESS["LDAP_CURRENT_USER"] = "NTLM身份驗證的當前用戶登錄（域\\登錄）：";
$MESS["LDAP_CURRENT_USER_ABS"] = "不明確的";
$MESS["LDAP_DEFAULT_NTLM_SERVER"] = "默認域服務器：";
$MESS["LDAP_DUPLICATE_LOGIN_USER"] = "即使存在指定登錄名稱的用戶，也可以創建用戶：";
$MESS["LDAP_NOT_USE_DEFAULT_NTLM_SERVER"] = "不使用";
$MESS["LDAP_OPTIONS_DEFAULT_EMAIL"] = "默認用戶電子郵件地址（如果未指定）：";
$MESS["LDAP_OPTIONS_GROUP_LIMIT"] = "可以在單個LDAP搜索操作上返回的最大條目數：";
$MESS["LDAP_OPTIONS_NEW_USERS"] = "在首次成功登錄時創建新的用戶帳戶";
$MESS["LDAP_OPTIONS_NTLM_VARNAME"] = "包含NTLM用戶登錄的PHP變量（通常是Remote_user）：";
$MESS["LDAP_OPTIONS_RESET"] = "重置";
$MESS["LDAP_OPTIONS_SAVE"] = "節省";
$MESS["LDAP_OPTIONS_USE_NTLM"] = "使用ntlm身份驗證<sup> <span class = \“必需\”> 1 </span> </sup>";
$MESS["LDAP_OPTIONS_USE_NTLM_MSG"] = "<sup> <span class = \“必需\”> 1 </span> </sup>  - 在使用NTLM身份驗證之前，您必須配置涉及的Web服務器模塊並指定AD服務器設置中的NTLM身份驗證域。";
$MESS["LDAP_WITHOUT_PREFIX"] = "在所有可用的LDAP服務器上檢查身份驗證，如果登錄不包括前綴";
$MESS["LDAP_WRONG_NET_MASK"] = "NTLM身份驗證子網地址和標記不正確。<br>使用以下格式：<br> subnet/mask <br> xxx.xxx.xxx.xxx.xxxxxxxxx xxx.xxx.xxx.xxx.xxx.xxx <br> xxxxx.xx.xxxxx.xxx.xxx.xxx.xxx.xxx.xx。 xxx/xx <br>與半隆分開多個IP範圍。";
