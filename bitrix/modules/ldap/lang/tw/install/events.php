<?php
$MESS["LDAP_USER_CONFIRM_EVENT_DESC"] = "來自＃site_name＃的問候！
-------------------------------------------------- --------
你好，

您已經收到此消息，因為您（或其他人）使用您的電子郵件在＃server_name＃上註冊。
要確認註冊，請單擊以下鏈接，然後在本地網絡中輸入您使用的姓名和密碼：

http：//#server_name#/bitrix/admin/ldap_user_auth.php？ldap_user_id =＃xml_id＃＆back_url =＃back_url＃

這是一個自動消息。";
$MESS["LDAP_USER_CONFIRM_EVENT_NAME"] = "＃site_name＃：註冊確認";
$MESS["LDAP_USER_CONFIRM_TYPE_DESC"] = "＃USER_ID＃ - 用戶ID
＃電子郵件＃-電子郵件
＃登錄＃-登錄
＃xml_id＃ - 外部ID
＃Back_url＃ - 返回URL
";
$MESS["LDAP_USER_CONFIRM_TYPE_NAME"] = "註冊確認";
