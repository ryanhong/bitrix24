<?php
$MESS["LDAP_INSTALL_BACK"] = "返回模塊管理";
$MESS["LDAP_INSTALL_TITLE"] = "AD/LDAP連接器模塊安裝";
$MESS["LDAP_MODULE_DESC"] = "AD/LDAP的連接器模塊";
$MESS["LDAP_MODULE_NAME"] = "廣告/LDAP連接器";
$MESS["LDAP_MOD_INST_ERROR"] = "錯誤安裝AD/LDAP連接器模塊";
$MESS["LDAP_MOD_INST_ERROR_PHP"] = "對於模塊的正確功能，應安裝帶有LDAP模塊的PHP。請聯繫您的系統管理員。";
$MESS["LDAP_UNINSTALL_COMPLETE"] = "卸載完成。";
$MESS["LDAP_UNINSTALL_DEL"] = "解除安裝";
$MESS["LDAP_UNINSTALL_ERROR"] = "錯誤刪除：";
$MESS["LDAP_UNINSTALL_SAVEDATA"] = "要保存存儲在數據庫表中的數據，請檢查\“保存表\”複選框";
$MESS["LDAP_UNINSTALL_SAVETABLE"] = "保存表";
$MESS["LDAP_UNINSTALL_TITLE"] = "AD/LDAP連接器模塊卸載";
$MESS["LDAP_UNINSTALL_WARNING"] = "警告！該模塊將被卸載。";
