<?php
$MESS["IM_DISK_ACTION_SAVE_TO_OWN_FILES_MSGVER_1"] = "保存以驅動";
$MESS["IM_DISK_AVATAR_CHANGE_F"] = "＃user_name＃更改聊天圖標";
$MESS["IM_DISK_AVATAR_CHANGE_M"] = "＃user_name＃更改聊天圖標";
$MESS["IM_DISK_ERR_AVATAR_1"] = "您不能在此聊天中更改化身。";
$MESS["IM_DISK_ERR_UPLOAD"] = "您無法將文件上傳到此聊天。";
$MESS["IM_DISK_LOCAL_FOLDER_B24_TITLE"] = "iMessenger";
$MESS["IM_DISK_LOCAL_FOLDER_TITLE"] = "iMessenger";
$MESS["IM_DISK_STORAGE_TITLE"] = "即時通訊存儲";
