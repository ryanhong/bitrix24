<?php
$MESS["IM_NS_BIZPROC_ACTIVITY"] = "業務流程的通知";
$MESS["IM_NS_CHAT_NEW"] = "私人聊天消息";
$MESS["IM_NS_DEFAULT"] = "無法識別的通知";
$MESS["IM_NS_IM"] = "聊天和電話";
$MESS["IM_NS_LIKE"] = "聊天喜歡";
$MESS["IM_NS_MAIN"] = "評分和喜歡";
$MESS["IM_NS_MAIN_RATING_VOTE"] = "\“我喜歡\”通知";
$MESS["IM_NS_MAIN_RATING_VOTE_MENTIONED"] = "提及您的帖子中有關投票的通知";
$MESS["IM_NS_MENTION"] = "您在公開聊天中被提及";
$MESS["IM_NS_MENTION_2"] = "有人在聊天中提到你";
$MESS["IM_NS_MESSAGE_NEW_MSGVER_1"] = "直接聊天消息";
$MESS["IM_NS_OPEN_NEW"] = "公共聊天消息";
