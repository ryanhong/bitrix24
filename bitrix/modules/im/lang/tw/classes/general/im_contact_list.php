<?php
$MESS["IM_CL_GROUP_ALL"] = "所有聯繫人";
$MESS["IM_CL_GROUP_CHATS"] = "小組聊天";
$MESS["IM_CL_GROUP_EXTRANET"] = "外部";
$MESS["IM_CL_GROUP_FRIENDS"] = "朋友們";
$MESS["IM_CL_GROUP_LAST"] = "最近聯繫人";
$MESS["IM_CL_GROUP_OTHER"] = "外部接觸";
$MESS["IM_CL_GROUP_OTHER_2"] = "其他聯繫人";
$MESS["IM_CL_GROUP_SEARCH"] = "搜索結果";
$MESS["IM_CL_GROUP_SG"] = "Extranet：";
$MESS["IM_CL_SEARCH_EMPTY"] = "搜索查詢是空的。";
$MESS["IM_FILE"] = "文件";
$MESS["IM_QUOTE"] = "引用";
$MESS["IM_SEARCH_OL"] = "開放通道";
