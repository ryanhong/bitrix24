<?php
$MESS["IM_INT_USR_INVITE_USERS"] = "＃user_name＃發送邀請函給＃用戶＃。";
$MESS["IM_INT_USR_INVITE_USERS_F"] = "＃user_name＃發送邀請函給＃用戶＃。";
$MESS["IM_INT_USR_JOIN_2"] = "我加入了團隊。";
$MESS["IM_INT_USR_JOIN_GENERAL_2"] = "我加入了團隊。";
$MESS["IM_INT_USR_LINK_COPIED"] = "＃user_name＃複製鏈接以邀請新員工。";
$MESS["IM_INT_USR_LINK_COPIED_F"] = "＃user_name＃複製鏈接以邀請新員工。";
$MESS["IM_INT_USR_REGISTER_USERS"] = "＃user_name＃註冊＃用戶＃。";
$MESS["IM_INT_USR_REGISTER_USERS_F"] = "＃user_name＃註冊＃用戶＃。";
$MESS["IM_INT_USR_REMOVE_ADMIN_RIGTHS_F"] = "＃USER_NAME＃從＃用戶撤銷管理員權限＃。";
$MESS["IM_INT_USR_REMOVE_ADMIN_RIGTHS_M"] = "＃USER_NAME＃從＃用戶撤銷管理員權限＃。";
$MESS["IM_INT_USR_SET_ADMIN_RIGTHS_F"] = "＃user_name＃授予＃用戶＃的管理員權限。";
$MESS["IM_INT_USR_SET_ADMIN_RIGTHS_M"] = "＃user_name＃授予＃用戶＃的管理員權限。";
