<?php
$MESS["IM_CHAT_CALENDAR_REGISTER_FROM_CHAT_NOTIFICATION"] = "[user =＃user_id＃] [/user]創建了[url =＃鏈接＃] event [/url] \“＃event_title＃\”。";
$MESS["IM_CHAT_CALENDAR_REGISTER_FROM_CHAT_NOTIFICATION_F"] = "[user =＃user_id＃] [/user]創建了[url =＃鏈接＃] event [/url] \“＃event_title＃\”。";
$MESS["IM_CHAT_CALENDAR_REGISTER_FROM_MESSAGE_NOTIFICATION"] = "[user =＃user_id＃] [/user]從[context =＃dialog_id＃/＃message_id＃] message [/context]創建[url =＃link＃] event [/url]。";
$MESS["IM_CHAT_CALENDAR_REGISTER_FROM_MESSAGE_NOTIFICATION_F"] = "[user =＃user_id＃] [/user]從[context =＃dialog_id＃/＃message_id＃] message [/context]創建[url =＃link＃] event [/url]。";
$MESS["IM_CHAT_CALENDAR_SERVICE_FROM_CHAT_NEW_TITLE"] = "聊天\“＃chat_title＃\”的事件";
$MESS["IM_CHAT_CALENDAR_SERVICE_FROM_MESSAGE_NEW_TITLE"] = "\“＃chat_title＃\”中的聊天消息中的事件";
$MESS["IM_CHAT_CALENDAR_SERVICE_FROM_PRIVATE_CHAT_NEW_TITLE"] = "私人聊天的事件\“＃chat_title＃\”";
$MESS["IM_CHAT_CALENDAR_SERVICE_FROM_PRIVATE_MESSAGE_NEW_TITLE"] = "\“＃chat_title＃\”中的私人聊天消息的事件";
