<?php
$MESS["IM_CHAT_TASK_REGISTER_FROM_CHAT_NOTIFICATION_F_MSGVER_1"] = "[user =＃user_id＃] [/user]創建了一個[url =＃鏈接＃] task [/url] \“＃task_title＃\”";
$MESS["IM_CHAT_TASK_REGISTER_FROM_CHAT_NOTIFICATION_MSGVER_1"] = "[user =＃user_id＃] [/user]創建了一個[url =＃鏈接＃] task [/url] \“＃task_title＃\”";
$MESS["IM_CHAT_TASK_REGISTER_FROM_MESSAGE_NOTIFICATION"] = "[user =＃user_id＃] [/user]從[context =＃dialog_id＃/＃message_id＃] message [/context]創建了一個[url =＃link＃] task [/url]。";
$MESS["IM_CHAT_TASK_REGISTER_FROM_MESSAGE_NOTIFICATION_F"] = "[user =＃user_id＃] [/user]從[context =＃dialog_id＃/＃message_id＃] message [/context]創建了一個[url =＃link＃] task [/url]。";
$MESS["IM_CHAT_TASK_SERVICE_FROM_CHAT_NEW_TITLE"] = "聊天的任務\“＃chat_title＃\”";
$MESS["IM_CHAT_TASK_SERVICE_FROM_MESSAGE_NEW_TITLE"] = "\“＃chat_title＃\”中的聊天消息中的任務";
$MESS["IM_CHAT_TASK_SERVICE_FROM_PRIVATE_CHAT_NEW_TITLE_MSGVER_1"] = "\“＃chat_title＃\”中的聊天消息中的任務";
$MESS["IM_CHAT_TASK_SERVICE_FROM_PRIVATE_MESSAGE_NEW_TITLE_MSGVER_1"] = "聊天的任務\“＃chat_title＃\”";
