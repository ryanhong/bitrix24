<?php
$MESS["IM_CHAT_FAVORITE_CREATE_WELCOME"] = "[b]我的筆記[/b] [br]使用此聊天對自己做筆記。保存您要保留的文本，鏈接，圖像和視頻。此聊天中的所有內容僅對您可見。沒有其他人可以訪問它。";
$MESS["IM_CHAT_FAVORITE_DESCRIPTION_V2"] = "使用此聊天對自己做筆記。保存您要保留的文本，鏈接，圖像和視頻。此聊天中的所有內容僅對您可見。沒有其他人可以訪問它。";
$MESS["IM_CHAT_FAVORITE_TITLE_V2"] = "筆記";
$MESS["IM_CHAT_FAVORITE_TITLE_V3"] = "我的筆記";
