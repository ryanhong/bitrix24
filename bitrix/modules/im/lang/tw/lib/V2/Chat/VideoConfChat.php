<?php
$MESS["IM_VIDEOCONF_COPY_LINK"] = "複製鏈接";
$MESS["IM_VIDEOCONF_CREATE_WELCOME"] = "[b]視頻會議[/b] [BR]將視頻會議鏈接發送給外部參與者，或者立即開始呼叫。";
$MESS["IM_VIDEOCONF_LINK_TITLE"] = "公共鏈接";
$MESS["IM_VIDEOCONF_NAME_FORMAT_NEW"] = "視頻會議##號碼＃";
$MESS["IM_VIDEOCONF_SHARE_LINK"] = "邀請同事和合作夥伴參加視頻會議！";
