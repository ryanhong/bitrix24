<?php
$MESS["IM_CHAT_APPOINT_OWNER_F"] = "＃user_1_name＃分配＃user_2_name＃作為所有者。";
$MESS["IM_CHAT_APPOINT_OWNER_M"] = "＃user_1_name＃分配＃user_2_name＃作為所有者。";
$MESS["IM_CHAT_CREATE_F"] = "＃USER_NAME＃創建聊天。";
$MESS["IM_CHAT_CREATE_M"] = "＃USER_NAME＃創建聊天。";
$MESS["IM_CHAT_CREATE_WELCOME"] = "[b]創建的私人聊天！[/b] [br]交流，共享文件並發表評論。";
$MESS["IM_CHAT_JOIN_F"] = "＃user_1_name＃邀請＃user_2_name＃聊天。";
$MESS["IM_CHAT_JOIN_M"] = "＃user_1_name＃邀請＃user_2_name＃聊天。";
$MESS["IM_CHAT_NAME_FORMAT"] = "＃顏色＃聊天##號碼＃";
$MESS["IM_CHAT_NAME_FORMAT_USERS"] = "與＃user_names＃聊天＃";
$MESS["IM_VIDEOCONF_CREATE_F"] = "＃user_name＃創建的視頻會議。";
$MESS["IM_VIDEOCONF_CREATE_M"] = "＃user_name＃創建的視頻會議。";
