<?php
$MESS["IM_CHAT_GENERAL_DESCRIPTION"] = "此聊天可供您公司的所有員工使用。閱讀新聞，討論基本主題並分享您認為有用的一切。";
$MESS["IM_CHAT_GENERAL_JOIN"] = "＃user_name＃已被錄用";
$MESS["IM_CHAT_GENERAL_JOIN_F"] = "＃user_name＃已被錄用";
$MESS["IM_CHAT_GENERAL_JOIN_PLURAL"] = "＃user_name＃已被錄用";
$MESS["IM_CHAT_GENERAL_LEAVE_F"] = "＃user_name＃被解僱";
$MESS["IM_CHAT_GENERAL_LEAVE_M"] = "＃user_name＃已被解僱";
$MESS["IM_CHAT_GENERAL_TITLE"] = "一般聊天";
