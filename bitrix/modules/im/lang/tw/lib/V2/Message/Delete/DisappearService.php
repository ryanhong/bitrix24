<?php
$MESS["DISAPPEAR_MESSAGES_CHANGE_DAY"] = "聊天所有者更改了自動刪除計時器。所有消息將在發布後一天過期。";
$MESS["DISAPPEAR_MESSAGES_CHANGE_HOUR"] = "聊天所有者更改了自動刪除計時器。所有消息將在發布後一個小時到期。";
$MESS["DISAPPEAR_MESSAGES_CHANGE_MONTH"] = "聊天所有者更改了自動刪除計時器。所有消息將在發布後一個月過期。";
$MESS["DISAPPEAR_MESSAGES_CHANGE_WEEK"] = "聊天所有者更改了自動刪除計時器。所有消息將在發布後一周過期。";
$MESS["DISAPPEAR_MESSAGES_OFF"] = "聊天所有者禁用自動刪除計時器。";
$MESS["DISAPPEAR_MESSAGES_ON_DAY"] = "聊天所有者啟用自動刪除功能。所有消息將在發布後一天過期。";
$MESS["DISAPPEAR_MESSAGES_ON_HOUR"] = "聊天所有者啟用自動刪除功能。所有消息將在發布後一個小時到期。";
$MESS["DISAPPEAR_MESSAGES_ON_MONTH"] = "聊天所有者啟用自動刪除功能。所有消息將在發布後一個月過期。";
$MESS["DISAPPEAR_MESSAGES_ON_WEEK"] = "聊天所有者啟用自動刪除功能。所有消息將在發布後一周過期。";
