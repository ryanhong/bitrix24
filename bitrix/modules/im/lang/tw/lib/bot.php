<?php
$MESS["BOT_DEFAULT_WORK_POSITION"] = "聊天機器人";
$MESS["BOT_MESSAGE_FROM"] = "＃bot_name＃聊天機器人";
$MESS["BOT_MESSAGE_INSTALL_SYSTEM"] = "聊天機器人已添加到您的Bitrix24帳戶中";
$MESS["BOT_MESSAGE_INSTALL_USER"] = "＃user_name＃添加了一個新的聊天機器人";
$MESS["BOT_MESSAGE_INSTALL_USER_F"] = "＃user_name＃添加了一個新的聊天機器人";
$MESS["BOT_SUPERVISOR_NOTICE_ALL_MESSAGES"] = "＃bot_name＃聊天機器人可以訪問聊天中的所有消息。";
$MESS["BOT_SUPERVISOR_NOTICE_NEW_MESSAGES"] = "＃bot_name＃聊天機器人可以訪問聊天中的所有新消息。";
