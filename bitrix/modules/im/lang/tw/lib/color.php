<?php
$MESS["IM_COLOR_AQUA"] = "水";
$MESS["IM_COLOR_AZURE"] = "azure";
$MESS["IM_COLOR_BROWN"] = "棕色的";
$MESS["IM_COLOR_DARK_BLUE"] = "藍色的";
$MESS["IM_COLOR_GRAPHITE"] = "石墨";
$MESS["IM_COLOR_GRAY"] = "灰色的";
$MESS["IM_COLOR_GREEN"] = "綠色的";
$MESS["IM_COLOR_KHAKI"] = "卡其色";
$MESS["IM_COLOR_LIGHT_BLUE"] = "天藍色";
$MESS["IM_COLOR_LIME"] = "酸橙";
$MESS["IM_COLOR_MARENGO"] = "Marengo";
$MESS["IM_COLOR_MINT"] = "薄荷";
$MESS["IM_COLOR_ORANGE"] = "橘子";
$MESS["IM_COLOR_PINK"] = "粉色的";
$MESS["IM_COLOR_PURPLE"] = "紫色的";
$MESS["IM_COLOR_RED"] = "紅色的";
$MESS["IM_COLOR_SAND"] = "沙";
