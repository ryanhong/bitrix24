<?php
$MESS["BXD_CALL_BG_MASK_TITLE"] = "3D化身和背景";
$MESS["BXD_CALL_BG_TITLE"] = "選擇背景";
$MESS["BXD_CONFIRM_CLOSE"] = "關閉";
$MESS["BXD_DEFAULT_TITLE"] = "Bitrix24桌面應用程序（版本＃版本＃）";
$MESS["BXD_LOGOUT"] = "切換用戶";
$MESS["BXD_NEED_UPDATE"] = "您的應用程序版本已過時。您必須安裝新版本才能使用該應用程序。";
$MESS["BXD_NEED_UPDATE_BTN"] = "更新";
$MESS["BXD_QUOTE_BLOCK"] = "引用";
$MESS["BXD_RECONNECT"] = "重新連接";
