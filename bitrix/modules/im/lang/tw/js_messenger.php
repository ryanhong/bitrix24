<?php
$MESS["IM_MESSENGER_CL_EMPTY"] = " - 沒有聯繫人 - ";
$MESS["IM_MESSENGER_CONTACT_LIST"] = "聯繫人";
$MESS["IM_MESSENGER_DELIVERED"] = "發送消息";
$MESS["IM_MESSENGER_HISTORY"] = "消息日誌";
$MESS["IM_MESSENGER_HISTORY_DELETE"] = "刪除消息";
$MESS["IM_MESSENGER_HISTORY_DELETE_ALL"] = "刪除所有消息";
$MESS["IM_MESSENGER_HISTORY_DELETE_ALL_CONFIRM"] = "您確定要刪除所有日誌記錄嗎？";
$MESS["IM_MESSENGER_HISTORY_DELETE_CONFIRM"] = "你確定要刪除這條消息嗎？";
$MESS["IM_MESSENGER_LOAD_MESSAGE"] = "加載消息";
$MESS["IM_MESSENGER_MESSAGES"] = "消息";
$MESS["IM_MESSENGER_NEW_MESSAGE"] = "新消息";
$MESS["IM_MESSENGER_NOT_DELIVERED"] = "消息沒有傳遞";
$MESS["IM_MESSENGER_NO_MESSAGE"] = "沒有消息";
$MESS["IM_MESSENGER_OPEN_HISTORY"] = "查看消息日誌";
$MESS["IM_MESSENGER_OPEN_PROFILE"] = "用戶資料";
$MESS["IM_MESSENGER_OPEN_VIDEO"] = "視訊通話";
$MESS["IM_MESSENGER_SEND_FILE"] = "發送文件";
$MESS["IM_MESSENGER_SEND_MESSAGE"] = "發送";
$MESS["IM_MESSENGER_VIEW_GROUP"] = "顯示或隱藏用戶組";
$MESS["IM_MESSENGER_VIEW_OFFLINE"] = "顯示或隱藏離線聯繫人";
$MESS["IM_MESSENGER_WRITE_MESSAGE"] = "發信息";
$MESS["IM_STATUS_DND"] = "不要打擾";
$MESS["IM_STATUS_OFFLINE"] = "離線";
$MESS["IM_STATUS_ONLINE"] = "在線的";
