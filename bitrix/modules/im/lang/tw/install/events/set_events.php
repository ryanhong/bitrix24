<?php
$MESS["IM_NEW_MESSAGE_DESC"] = "＃USER_ID＃ - 用戶ID
＃user_login＃ - 用戶登錄
＃user_name＃ - 用戶名
＃USER_LAST_NAME＃ - 用戶姓氏
＃FROF_USER＃ - 消息發送者名稱
＃消息＃ - 消息
＃email_to＃ - 收件人電子郵件地址";
$MESS["IM_NEW_MESSAGE_GROUP_DESC"] = "＃USER_ID＃ - 用戶ID
＃user_login＃ - 用戶登錄
＃user_name＃ - 用戶名
＃USER_LAST_NAME＃ - 用戶姓氏
＃從_users＃ - 消息發送者名稱
＃消息＃ - 消息
＃email_to＃ - 收件人電子郵件地址";
$MESS["IM_NEW_MESSAGE_GROUP_MESSAGE"] = "您好＃user_name＃！

您從＃from_users＃中獲得了新的即時消息。

＃消息＃

單擊以查看對話：http：//＃server_name＃/？im_dialog = y
編輯通知首選項：http：//＃server_name＃/？im_settings = notify

這是一條自動消息，不要回复它。";
$MESS["IM_NEW_MESSAGE_GROUP_NAME"] = "新消息（組）";
$MESS["IM_NEW_MESSAGE_GROUP_SUBJECT"] = "＃site_name＃：來自＃from_users＃的即時消息＃";
$MESS["IM_NEW_MESSAGE_MESSAGE"] = "您好＃user_name＃！

您從＃from_user＃中獲得了新的即時消息。

-------------------------------------------------- --------
＃消息＃
-------------------------------------------------- --------

單擊開始對話：http：//＃server_name＃/？im_dialog =＃user_id＃＃
編輯通知首選項：http：//＃server_name＃/？im_settings = notify

這是一條自動消息，不要回复它。";
$MESS["IM_NEW_MESSAGE_NAME"] = "新消息";
$MESS["IM_NEW_MESSAGE_SUBJECT"] = "＃site_name＃：＃from_user＃的即時消息＃";
$MESS["IM_NEW_NOTIFY_DESC"] = "＃Message_id＃ - 消息ID
＃USER_ID＃ - 用戶ID
＃user_login＃ - 用戶登錄
＃user_name＃ - 用戶名
＃USER_LAST_NAME＃ - 用戶姓氏
＃FROF_USER_ID＃ - 消息發送者ID
＃FROF_USER＃ - 消息發送者名稱
＃date_create＃ - 創建消息的日期
＃標題＃ - 消息標題
＃消息＃ - 消息正文
＃message_50＃ - 消息文本的前50個字符
＃email_to＃ - 消息收件人電子郵件地址";
$MESS["IM_NEW_NOTIFY_GROUP_DESC"] = "＃Message_id＃ - 消息ID
＃USER_ID＃ - 用戶ID
＃user_login＃ - 用戶登錄
＃user_name＃ - 用戶名
＃USER_LAST_NAME＃ - 用戶姓氏
＃從_users＃ - 消息發送者名稱
＃date_create＃ - 創建消息的日期
＃標題＃ - 消息標題
＃消息＃ - 消息正文
＃message_50＃ - 消息文本的前50個字符
＃email_to＃ - 消息收件人電子郵件地址
";
$MESS["IM_NEW_NOTIFY_GROUP_MESSAGE"] = "您好＃user_name＃！

您有＃From_users＃的新通知

-------------------------------------------------- --------

#訊息#

-------------------------------------------------- --------

查看通知：http：//＃server_name＃/？im_notify = y
編輯通知首選項：http：//＃server_name＃/？im_settings = notify

這是一條自動消息，不要回复它。";
$MESS["IM_NEW_NOTIFY_GROUP_NAME"] = "新通知（組）";
$MESS["IM_NEW_NOTIFY_GROUP_SUBJECT"] = "＃site_name＃：notification \“＃message_50＃\”";
$MESS["IM_NEW_NOTIFY_MESSAGE"] = "您好＃user_name＃！

您有＃From_user＃的新通知

-------------------------------------------------- --------

#訊息#

-------------------------------------------------- --------

查看通知：http：//＃server_name＃/？im_notify = y
編輯通知首選項：http：//＃server_name＃/？im_settings = notify

這是一條自動消息，不要回复它。";
$MESS["IM_NEW_NOTIFY_NAME"] = "新通知";
$MESS["IM_NEW_NOTIFY_SUBJECT"] = "＃site_name＃：notification \“＃message_50＃\”";
