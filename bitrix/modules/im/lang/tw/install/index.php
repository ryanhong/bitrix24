<?php
$MESS["IM_CONVERT_MESSAGE"] = "您已經安裝了\“ Instant Messenger \”模塊。要訪問消息歷史記錄，您必須＃a_tag_start＃轉換歷史記錄數據＃A_TAG_END＃。";
$MESS["IM_INSTALL_TITLE"] = "即時Messenger模塊安裝";
$MESS["IM_MODULE_DESCRIPTION"] = "為即時消息和通知服務提供支持。";
$MESS["IM_MODULE_NAME"] = "即時通訊";
$MESS["IM_UNINSTALL_TITLE"] = "即時通訊模塊卸載";
