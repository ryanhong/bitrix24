<?php
$MESS["IM_M_CALL_BAD_NETWORK_HINT"] = "由於持續的連通性問題，我們為您設備上的某些呼叫參與者中斷了視頻傳輸，以保持聲音質量。";
$MESS["IM_M_CALL_POOR_CONNECTION_WITH_USER"] = "呼叫參與者的互聯網連接緩慢。";
