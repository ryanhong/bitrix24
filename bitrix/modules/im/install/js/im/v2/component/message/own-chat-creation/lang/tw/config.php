<?php
$MESS["IM_MESSAGE_OWN_CHAT_CREATION_DESCRIPTION"] = "使用此聊天對自己做筆記。保存您要保留的文本，鏈接，圖像和視頻。＃br＃此聊天中的所有內容僅對您可見。沒有其他人可以訪問它。";
$MESS["IM_MESSAGE_OWN_CHAT_CREATION_TITLE"] = "我的筆記";
