<?php
$MESS["IM_CONTENT_SETTINGS_DEMO_CHAT_MESSAGE_1"] = "嘿！什麼是新的？";
$MESS["IM_CONTENT_SETTINGS_DEMO_CHAT_MESSAGE_2"] = "你好！我的假期剛剛結束。否則，我很好大聲笑";
$MESS["IM_CONTENT_SETTINGS_DEMO_CHAT_MESSAGE_3"] = "涼爽的！";
$MESS["IM_CONTENT_SETTINGS_DEMO_CHAT_USER_NAME"] = "約翰·史密斯";
$MESS["IM_CONTENT_SETTINGS_EXPERT_NOTIFICATIONS_TYPE_MAIL"] = "電子郵件";
$MESS["IM_CONTENT_SETTINGS_EXPERT_NOTIFICATIONS_TYPE_PUSH"] = "推";
$MESS["IM_CONTENT_SETTINGS_EXPERT_NOTIFICATIONS_TYPE_WEB"] = "Web版本和桌面應用程序";
$MESS["IM_CONTENT_SETTINGS_OPTION_APPEARANCE_ALIGNMENT"] = "消息對齊";
$MESS["IM_CONTENT_SETTINGS_OPTION_APPEARANCE_BACKGROUND"] = "聊天背景";
$MESS["IM_CONTENT_SETTINGS_OPTION_DESKTOP_ALWAYS_OPEN_CHAT"] = "始終在應用程序中打開聊天";
$MESS["IM_CONTENT_SETTINGS_OPTION_DESKTOP_AUTO_START"] = "運行系統啟動";
$MESS["IM_CONTENT_SETTINGS_OPTION_DESKTOP_BLOCK_LINKS"] = "鏈接";
$MESS["IM_CONTENT_SETTINGS_OPTION_DESKTOP_BLOCK_REST"] = "各種各樣的";
$MESS["IM_CONTENT_SETTINGS_OPTION_DESKTOP_BLOCK_STARTUP"] = "啟動";
$MESS["IM_CONTENT_SETTINGS_OPTION_DESKTOP_OPEN_LINKS_IN_SLIDER"] = "打開bitrix24聊天中的鏈接（使用滑塊表單）";
$MESS["IM_CONTENT_SETTINGS_OPTION_DESKTOP_SEND_TELEMETRY"] = "將匿名用法統計信息發送到Bitrix24";
$MESS["IM_CONTENT_SETTINGS_OPTION_DESKTOP_TWO_WINDOW_MODE"] = "啟動兩個應用程序：僅使用一個應用程序進行聊天，而另一個應用程序進行其他Bitrix24活動";
$MESS["IM_CONTENT_SETTINGS_OPTION_HOTKEY_NEW_LINE"] = "新線：＃熱鍵＃";
$MESS["IM_CONTENT_SETTINGS_OPTION_HOTKEY_SEND_COMBINATION"] = "使用以下方式發送消息：";
$MESS["IM_CONTENT_SETTINGS_OPTION_NOTIFICATION_AUTO_READ"] = "自動標記通知為閱讀";
$MESS["IM_CONTENT_SETTINGS_OPTION_NOTIFICATION_BLOCK_FOCUS"] = "通知";
$MESS["IM_CONTENT_SETTINGS_OPTION_NOTIFICATION_BLOCK_MODE"] = "設定";
$MESS["IM_CONTENT_SETTINGS_OPTION_NOTIFICATION_BLOCK_SIMPLE_MODE_TITLE"] = "發送通知到：";
$MESS["IM_CONTENT_SETTINGS_OPTION_NOTIFICATION_ENABLE_MAIL"] = "電子郵件（＃郵件＃）";
$MESS["IM_CONTENT_SETTINGS_OPTION_NOTIFICATION_ENABLE_PUSH_V1"] = "推（在移動設備上）";
$MESS["IM_CONTENT_SETTINGS_OPTION_NOTIFICATION_ENABLE_SOUND"] = "啟用通知聲音";
$MESS["IM_CONTENT_SETTINGS_OPTION_NOTIFICATION_ENABLE_WEB"] = "Web版本和桌面應用程序";
$MESS["IM_CONTENT_SETTINGS_OPTION_NOTIFICATION_EXPERT_MODE"] = "先進的";
$MESS["IM_CONTENT_SETTINGS_OPTION_NOTIFICATION_SIMPLE_MODE"] = "簡單的";
$MESS["IM_CONTENT_SETTINGS_OPTION_RECENT_SHOW_BIRTHDAY"] = "表演生日";
$MESS["IM_CONTENT_SETTINGS_OPTION_RECENT_SHOW_INVITED"] = "節目被邀請的用戶";
$MESS["IM_CONTENT_SETTINGS_OPTION_RECENT_SHOW_TEXT"] = "顯示消息文字";
$MESS["IM_CONTENT_SETTINGS_SECTION_LIST_APPEARANCE"] = "設計和佈局";
$MESS["IM_CONTENT_SETTINGS_SECTION_LIST_DESKTOP"] = "Bitrix24應用程序";
$MESS["IM_CONTENT_SETTINGS_SECTION_LIST_FEEDBACK"] = "回饋";
$MESS["IM_CONTENT_SETTINGS_SECTION_LIST_HELP"] = "幫助";
$MESS["IM_CONTENT_SETTINGS_SECTION_LIST_HOTKEY"] = "熱鍵";
$MESS["IM_CONTENT_SETTINGS_SECTION_LIST_MESSAGE"] = "消息";
$MESS["IM_CONTENT_SETTINGS_SECTION_LIST_NOTIFICATION"] = "通知";
$MESS["IM_CONTENT_SETTINGS_SECTION_LIST_RECENT"] = "最近的聊天";
$MESS["IM_CONTENT_SETTINGS_SECTION_LIST_TITLE"] = "設定";
