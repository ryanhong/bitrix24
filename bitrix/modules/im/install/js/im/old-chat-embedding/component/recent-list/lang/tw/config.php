<?php
$MESS["IM_RECENT_ACTIVE_CALL_HANGUP"] = "掛斷";
$MESS["IM_RECENT_ACTIVE_CALL_JOIN"] = "加入";
$MESS["IM_RECENT_ACTIVE_CALL_JOIN_AUDIO"] = "只有音頻";
$MESS["IM_RECENT_ACTIVE_CALL_JOIN_VIDEO"] = "帶有視頻";
$MESS["IM_RECENT_BIRTHDAY"] = "今天有生日！";
$MESS["IM_RECENT_BIRTHDAY_DATE"] = "今天";
$MESS["IM_RECENT_CHAT_SELF"] = "是你";
$MESS["IM_RECENT_CHAT_TYPE_GROUP"] = "私人聊天";
$MESS["IM_RECENT_CHAT_TYPE_OPEN"] = "公眾聊天";
$MESS["IM_RECENT_DELETED_MESSAGE"] = "此消息已刪除。";
$MESS["IM_RECENT_EMPTY"] = "沒有聊天";
$MESS["IM_RECENT_INVITATION_NOT_ACCEPTED"] = "邀請不接受";
$MESS["IM_RECENT_MESSAGE_DRAFT_2"] = "草稿：＃文本＃";
$MESS["IM_RECENT_NEW_USER_POPUP_TEXT"] = "我現在在你的團隊中！";
