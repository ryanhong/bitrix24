<?php
$MESS["IM_ACCESS_ERROR"] = "行動不可用";
$MESS["IM_ERROR_GROUP_CANCELED"] = "您無法將消息發送到指定的聊天";
$MESS["IM_MODULE_NOT_INSTALLED"] = "即時通訊模塊未安裝。";
$MESS["IM_UNKNOWN_ERROR"] = "未知錯誤。";
