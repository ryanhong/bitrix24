<?php
$MESS["IM_CONFERENCE_CREATE_FIELD_DATE"] = "日期";
$MESS["IM_CONFERENCE_CREATE_FIELD_DURATION"] = "期間";
$MESS["IM_CONFERENCE_CREATE_FIELD_PASSWORD"] = "密碼";
$MESS["IM_CONFERENCE_CREATE_FIELD_PASSWORD_PLACEHOLDER"] = "輸入密碼";
$MESS["IM_CONFERENCE_CREATE_FIELD_TIME"] = "時間";
$MESS["IM_CONFERENCE_CREATE_FIELD_TITLE"] = "姓名";
$MESS["IM_CONFERENCE_CREATE_FIELD_TITLE_PLACEHOLDER"] = "輸入名字";
$MESS["IM_CONFERENCE_CREATE_FIELD_USERS"] = "與會者";
$MESS["IM_CONFERENCE_CREATE_INVITATION_TITLE"] = "邀請文字和鏈接";
$MESS["IM_CONFERENCE_CREATE_TITLE"] = "新視頻會議";
