<?php
$MESS["IMCONNECTOR_COMPONENT_VIBER_API_KEY"] = "鑰匙：";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION_1"] = "Viber已連接到您的開放頻道。現在，發佈到您的公共帳戶的所有消息都將重定向到您的Bitrix24。";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION_2"] = "<br>您現在可以訪問<a href='#url_mobile#'> bitrix24移動應用</a>或<a href='#url#'>在瀏覽器中打開您的Bitrix24帳戶</a>。";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_CHAT_ONE_TO_ONE"] = "人與人的聊天鏈接";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NAME_BOT"] = "連接的公共帳戶";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_1"] = "從剪貼板粘貼Viber鍵，然後單擊\“保存\”。";
