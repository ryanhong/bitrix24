<?php
$MESS["IMCONNECTOR_COMPONENT_WECHAT_APP_ENCRYPT_KEY"] = "消息加密密鑰：";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_APP_ID"] = "開發人員ID（appid）：";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_APP_SECRET"] = "開發人員密碼（AppSecret）：";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CHANGE_ANY_TIME"] = "您可以隨時編輯或斷開連接";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECTED"] = "微信已連接";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_HELP"] = "<div class = \“ imconnector-field-button-subtitle \”>我想</div>
<div class = \“ imconnector-field-button-name \”> <span class = \“ imconnector-field-box-text-bold \”>連接a </span>微信>";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_STEP_NEW"] = "您將必須＃link_start＃創建一個官方帳戶＃link_end＃或使用現有的帳戶進行連接。如果您沒有帳戶，我們將幫助您創建一個帳戶並將其連接到您的Bitrix24。";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_CONNECT_TITLE"] = "將微信連接到打開頻道";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_COPY"] = "複製";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_FINAL_FORM_DESCRIPTION"] = "微信已連接到您的開放頻道。發佈到您的官方帳戶的所有消息將被重定向到您的Bitrix24帳戶。";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_INFO"] = "資訊";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_INFO_CONNECT_ID"] = "redirect =細節＆代碼= 10112784";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_INSTRUCTION_TITLE"] = "<span class = \“ imconnector-field-box-text-bold \”>如何獲取官方帳戶信息</span>：";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_LANG"] = "選擇帳戶語言";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_NAME_CHAT_LINK"] = "實時聊天鏈接";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_SERVER_IP_ADDRESS_DESCRIPTION"] = "在您的官方帳戶配置文件（新行上的每個值）中的IP白名單字段中指定這些值：";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_TOKEN_DESCRIPTION"] = "在您的官方帳戶資料中的令牌字段中指定此值：";
$MESS["IMCONNECTOR_COMPONENT_WECHAT_WEBHOOK_DESCRIPTION"] = "在您的官方帳戶配置文件中的URL字段中指定此地址：";
