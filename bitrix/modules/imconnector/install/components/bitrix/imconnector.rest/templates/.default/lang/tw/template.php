<?php
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECT"] = "連接";
$MESS["IMCONNECTOR_COMPONENT_REST_CONNECTOR_ERROR_STATUS"] = "使用連接器存在錯誤。請再次連接。";
$MESS["IMCONNECTOR_COMPONENT_REST_INDEX_DESCRIPTION_NEW"] = "將連接器與開放頻道一起使用，以將客戶的消息發佈到Bitrix24聊天中。";
$MESS["IMCONNECTOR_COMPONENT_REST_INDEX_SUBTITLE"] = "將連接器與開放頻道一起使用，以將客戶的消息發佈到Bitrix24聊天中。";
$MESS["IMCONNECTOR_COMPONENT_REST_INFO_DESCRIPTION"] = "在設置頁面上了解更多信息";
$MESS["IMCONNECTOR_COMPONENT_REST_INFO_TITLE"] = "渠道已成功配置";
