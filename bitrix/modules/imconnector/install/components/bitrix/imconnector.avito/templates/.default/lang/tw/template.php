<?php
$MESS["IMCONNECTOR_COMPONENT_AVITO_AUTHORIZATION"] = "授權";
$MESS["IMCONNECTOR_COMPONENT_AVITO_AUTHORIZE"] = "登入";
$MESS["IMCONNECTOR_COMPONENT_AVITO_CHANGE_ANY_TIME"] = "隨時更改或禁用";
$MESS["IMCONNECTOR_COMPONENT_AVITO_CLIENT_ID"] = "輸入客戶ID：";
$MESS["IMCONNECTOR_COMPONENT_AVITO_CLIENT_SECRET"] = "輸入客戶秘密：";
$MESS["IMCONNECTOR_COMPONENT_AVITO_CONNECTED"] = "Avito已連接";
$MESS["IMCONNECTOR_COMPONENT_AVITO_CONNECT_HELP"] = "<div class = \“ imconnector-field-button-subtitle \”> procced </div>
<div class = \“ imconnector-field-button-name \”> <span class = \“ imconnector-field-box-text-bold \”>與連接</span> avito account </div>";
$MESS["IMCONNECTOR_COMPONENT_AVITO_CONNECT_STEP_NEW"] = "您必須＃link_start＃創建一個帳戶＃link_end＃或使用已經擁有的帳戶。如果您沒有帳戶，我們將幫助您創建並將其連接到您的Bitrix24。";
$MESS["IMCONNECTOR_COMPONENT_AVITO_CONNECT_TITLE"] = "將Avito連接到開放頻道";
$MESS["IMCONNECTOR_COMPONENT_AVITO_FINAL_FORM_DESCRIPTION"] = "Avito已通過成功連接到您的開放頻道。現在，所有請求將自動轉發到您的Bitrix24帳戶。";
$MESS["IMCONNECTOR_COMPONENT_AVITO_INDEX_ADDITIONAL_DESCRIPTION_NEW"] = "您必須＃link_start＃創建一個帳戶＃link_end＃或使用已經擁有的帳戶。如果您沒有帳戶，我們將幫助您創建並將其連接到您的Bitrix24。";
$MESS["IMCONNECTOR_COMPONENT_AVITO_INDEX_LIST_ITEM_1"] = "處理直接在Bitrix24中從Avito收到的查詢";
$MESS["IMCONNECTOR_COMPONENT_AVITO_INDEX_LIST_ITEM_2"] = "將聯繫人和通信歷史保存到CRM";
$MESS["IMCONNECTOR_COMPONENT_AVITO_INDEX_LIST_ITEM_3"] = "指導客戶通過CRM中的銷售渠道";
$MESS["IMCONNECTOR_COMPONENT_AVITO_INDEX_LIST_ITEM_4"] = "回复您的客戶何時何地";
$MESS["IMCONNECTOR_COMPONENT_AVITO_INDEX_LIST_ITEM_5"] = "根據隊列規則，客戶查詢分佈在銷售代理之間";
$MESS["IMCONNECTOR_COMPONENT_AVITO_INDEX_SUBTITLE"] = "將客戶溝通渠道合併到BITRIX24中，以更快地響應並改善轉化率。";
$MESS["IMCONNECTOR_COMPONENT_AVITO_INDEX_TITLE"] = "在Avito上賣？直接從客戶那裡收到bitrix24的問題";
$MESS["IMCONNECTOR_COMPONENT_AVITO_INFO"] = "資訊";
$MESS["IMCONNECTOR_COMPONENT_AVITO_INFO_CONNECT_ID"] = "redirect =細節＆代碼= 10179780";
$MESS["IMCONNECTOR_COMPONENT_AVITO_INSTRUCTION_TITLE"] = "<span class = \“ imconnector-field-box-text-bold \”>手冊</span>獲取帳戶數據：";
$MESS["IMCONNECTOR_COMPONENT_AVITO_LOG_IN"] = "請使用帶有Active Avito訂閱的Bitrix24帳戶登錄。";
$MESS["IMCONNECTOR_COMPONENT_AVITO_LOG_IN_OAUTH"] = "使用Avito帳戶登錄。";
$MESS["IMCONNECTOR_COMPONENT_AVITO_USER_ID"] = "用戶身份";
