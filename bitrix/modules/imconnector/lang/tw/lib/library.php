<?php
$MESS["IMCONNECTOR_ADD_EXISTING_CONNECTOR"] = "嘗試添加已經存在的連接器";
$MESS["IMCONNECTOR_DELETE_NOT_EXISTING_CONNECTOR"] = "嘗試刪除非活動連接器";
$MESS["IMCONNECTOR_EMPTY_PARAMETRS"] = "提供的空參數";
$MESS["IMCONNECTOR_ERROR_ANSWER_MALFORMED"] = "服務器沒有以JSON格式響應";
$MESS["IMCONNECTOR_ERROR_CONVERTING_PUNYCODE"] = "錯誤將主機名＃主機＃轉換為punycode：＃錯誤＃";
$MESS["IMCONNECTOR_ERROR_COULD_NOT_GET_PROVIDER_OBJECT"] = "無法獲得提供商對象";
$MESS["IMCONNECTOR_ERROR_INCORRECT_INCOMING_DATA"] = "無效的輸入數據";
$MESS["IMCONNECTOR_ERROR_LICENCE_ERROR"] = "本網站的許可無效";
$MESS["IMCONNECTOR_ERROR_NETWORK_ERROR"] = "網絡連接錯誤";
$MESS["IMCONNECTOR_ERROR_NO_CORRECT_PROVIDER"] = "找不到匹配的連接器提供商";
$MESS["IMCONNECTOR_ERROR_NO_DOMAIN"] = "客戶端Bitrix24域未指定。";
$MESS["IMCONNECTOR_ERROR_NO_RESPONSE"] = "從客戶端Bitrix24收到空的響應。";
$MESS["IMCONNECTOR_ERROR_PROVIDER_CONNECTOR"] = "不正確的連接器ID";
$MESS["IMCONNECTOR_ERROR_PROVIDER_CONTROLLER_CONNECTOR_URL"] = "不正確的連接器控制器URL";
$MESS["IMCONNECTOR_ERROR_PROVIDER_DOES_NOT_SUPPORT_THIS_METHOD_CALL"] = "該提供商不支持此呼叫方法";
$MESS["IMCONNECTOR_ERROR_PROVIDER_GENERAL_REQUEST_DYNAMIC_METHOD"] = "嘗試將動態方法稱為靜態方法";
$MESS["IMCONNECTOR_ERROR_PROVIDER_GENERAL_REQUEST_NOT_DYNAMIC_METHOD"] = "嘗試將靜態方法稱為動態";
$MESS["IMCONNECTOR_ERROR_PROVIDER_LICENCE_CODE_PORTAL"] = "錯誤的BitRix24許可證代碼";
$MESS["IMCONNECTOR_ERROR_PROVIDER_LINE"] = "不正確的打開通道ID";
$MESS["IMCONNECTOR_ERROR_PROVIDER_NOT_CALL"] = "無法調用方法";
$MESS["IMCONNECTOR_ERROR_PROVIDER_NO_ACTIVE_CONNECTOR"] = "非活動連接器";
$MESS["IMCONNECTOR_ERROR_PROVIDER_TYPE_PORTAL"] = "錯誤的BitRix24類型";
$MESS["IMCONNECTOR_ERROR_PROVIDER_UNSUPPORTED_TYPE_INCOMING_MESSAGE"] = "不支持的入站服務器消息類型";
$MESS["IMCONNECTOR_ERROR_PUBLIC_URL_ANSWER_MALFORMED"] = "錯誤驗證公共站點地址：未識別的服務器響應";
$MESS["IMCONNECTOR_ERROR_PUBLIC_URL_CONNECT_ERROR"] = "錯誤驗證公共網站地址：無法連接";
$MESS["IMCONNECTOR_ERROR_PUBLIC_URL_EMPTY"] = "請在\“外部Messenger Connectors \”模塊設置中提供公共站點地址";
$MESS["IMCONNECTOR_ERROR_PUBLIC_URL_EMPTY_MSGVER_1"] = "請在\“外部Messenger Connectors \”模塊設置中提供公共站點地址";
$MESS["IMCONNECTOR_ERROR_PUBLIC_URL_FAIL"] = "您指定的公共網站地址不可用（代碼：＃錯誤＃）";
$MESS["IMCONNECTOR_ERROR_PUBLIC_URL_HANDLER_PATH"] = "在公共區域找不到處理程序文件。必需文件：＃路徑＃";
$MESS["IMCONNECTOR_ERROR_PUBLIC_URL_LOCALHOST"] = "公共網站地址解決到本地主機：＃主機＃";
$MESS["IMCONNECTOR_ERROR_PUBLIC_URL_MALFORMED"] = "指定不正確的公共網站地址";
$MESS["IMCONNECTOR_ERROR_PUBLIC_URL_WRONG_ANSWER"] = "錯誤驗證公共站點地址：無效服務器響應";
$MESS["IMCONNECTOR_ERROR_PUBLIC_URL_WRONG_ANSWER_CODE"] = "錯誤驗證公共站點地址：返回的服務器無效HTTP響應代碼";
$MESS["IMCONNECTOR_FAILED_REGISTER_CONNECTOR"] = "無法註冊連接器";
$MESS["IMCONNECTOR_FAILED_TO_ADD_CONNECTOR"] = "無法添加打開的通道連接器";
$MESS["IMCONNECTOR_FAILED_TO_DELETE_CONNECTOR"] = "無法刪除打開的通道連接器";
$MESS["IMCONNECTOR_FAILED_TO_LOAD_MODULE_OPEN_LINES"] = "無法加載開放通道模塊";
$MESS["IMCONNECTOR_FAILED_TO_SAVE_SETTINGS_CONNECTOR"] = "無法保存連接器首選項";
$MESS["IMCONNECTOR_FAILED_TO_TEST_CONNECTOR"] = "無法測試連接器連接";
$MESS["IMCONNECTOR_FAILED_TO_UPDATE_CONNECTOR"] = "無法更新打開的通道連接器";
$MESS["IMCONNECTOR_FEATURE_IS_NOT_SUPPORTED"] = "此功能不支持";
$MESS["IMCONNECTOR_NOT_ACTIVE_LINE"] = "具有此ID的開放通道不活動或不存在。";
$MESS["IMCONNECTOR_NOT_ALL_THE_REQUIRED_DATA"] = "數據不完整";
$MESS["IMCONNECTOR_NOT_AVAILABLE_CONNECTOR"] = "嘗試連接到不活動或不可用的連接器";
$MESS["IMCONNECTOR_NOT_SPECIFIED_CORRECT_COMMAND"] = "提供了不正確的命令";
$MESS["IMCONNECTOR_NOT_SPECIFIED_CORRECT_CONNECTOR"] = "未指定連接器。";
$MESS["IMCONNECTOR_PROXY_NO_ADD_USER"] = "無法創建或將用戶映射到外部Messenger用戶。";
$MESS["IMCONNECTOR_PROXY_NO_USER_IM"] = "無法獲取IM用戶ID。";
$MESS["IMCONNECTOR_REST_APPLICATION_REGISTRATION_ERROR"] = "錯誤註冊應用程序";
$MESS["IMCONNECTOR_REST_APPLICATION_REGISTRATION_ERROR_POINT"] = "申請註冊錯誤。連接器ID不能包含點（。）。";
$MESS["IMCONNECTOR_REST_APPLICATION_UNREGISTRATION_ERROR"] = "錯誤的登記應用程序";
$MESS["IMCONNECTOR_REST_CONNECTOR_ID_REQUIRED"] = "連接器ID未指定";
$MESS["IMCONNECTOR_REST_GENERAL_CONNECTOR_REGISTRATION_ERROR"] = "註冊連接器時通用錯誤";
$MESS["IMCONNECTOR_REST_ICON_REQUIRED"] = "未指定連接器圖標";
$MESS["IMCONNECTOR_REST_NAME_REQUIRED"] = "未指定連接器名稱";
$MESS["IMCONNECTOR_REST_NO_APPLICATION_ID"] = "無法獲得應用ID";
$MESS["IMCONNECTOR_REST_NO_PLACEMENT_HANDLER"] = "無法獲取嵌入式處理程序URL";
$MESS["IMCONNECTOR_UPDATE_NOT_EXISTING_CONNECTOR"] = "嘗試更新非活動連接器";
