<?php
$MESS["CONNECTORS_OLX_ATTACHMENTS_NOTIFY_MESSAGE"] = "（您必須登錄您的OLX帳戶才能查看文件）";
$MESS["CONNECTORS_OLX_RECONNECT_REMINDER_NOTIFICATION"] = "OLX要求您每30天將此渠道重新連接到聯繫中心。
您將必須在頻道設置中再次重新連接此頻道。 ＃link_start＃轉到設置＃link_end＃。";
