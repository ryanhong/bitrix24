<?php
$MESS["IMCONNECTOR_FACEBOOK_ADDITIONAL_DATA"] = "提供的其他詳細信息：";
$MESS["IMCONNECTOR_FACEBOOK_NOT_SEND_MESSAGE_CHAT_7_DAY_LIMIT"] = "沒有傳遞消息，因為等待客戶的答复的7天窗口已過期。 ＃A_START＃詳細信息＃A_END＃";
$MESS["IMCONNECTOR_FACEBOOK_NOT_SEND_MESSAGE_CHAT_24_HOURS_LIMIT"] = "沒有傳遞消息，因為等待客戶的答复的24小時窗口已過期。您可以將消息傳遞窗口擴展到7天。然後，您將必須再次發送此消息。 ＃A_START＃詳細信息＃A_END＃";
$MESS["IMCONNECTOR_FACEBOOK_NOT_SEND_MESSAGE_CHAT_LIMIT"] = "由於客戶在允許的時間範圍內沒有響應，因此未發送消息。 ＃A_START＃詳細信息＃A_END＃";
