<?php
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_CHAT_7_DAY_LIMIT"] = "沒有傳遞消息，因為等待客戶的答复的7天窗口已過期。 ＃A_START＃詳細信息＃A_END＃";
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_CHAT_24_HOURS_LIMIT"] = "沒有傳遞消息，因為等待客戶的答复的24小時窗口已過期。您可以將消息傳遞窗口擴展到7天。然後，您將必須再次發送此消息。 ＃A_START＃詳細信息＃A_END＃";
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_CHAT_LIMIT"] = "由於客戶在允許的時間範圍內沒有響應，因此未發送消息。 ＃A_START＃詳細信息＃A_END＃";
$MESS["IMCONNECTOR_FBINSTAGRAMDIRECT_NOT_SEND_MESSAGE_FOR_COMMENT"] = "無法回复Instagram直接評論，因為最初的評論已刪除或父母廣播結束。";
$MESS["IMCONNECTOR_MESSAGE_LINK_COMMENT_MEDIA_INSTAGRAM"] = "用戶將評論留給Instagram帖子。 [url =＃url＃]查看帖子[/url]";
$MESS["IMCONNECTOR_MESSAGE_UNSUPPORTED_INSTAGRAM"] = "客戶向您發送了一條消息。但是，此消息類型暫時不支持。請使用您的Instagram應用程序閱讀該消息。";
