<?php
$MESS["CONNECTOR_PROXY_NO_ADD_CHAT"] = "無法在傳入消息上創建聊天";
$MESS["CONNECTOR_PROXY_NO_ADD_MESSAGE"] = "無法添加消息";
$MESS["CONNECTOR_PROXY_NO_ADD_USER"] = "無法創建或將用戶映射到外部Messenger用戶。";
$MESS["CONNECTOR_PROXY_NO_USER_IM"] = "無法獲取IM用戶ID。";
$MESS["CONNECTOR_PROXY_TITLE_NEW_BOT_CHAT"] = "與＃user_name＃通過＃連接器＃的新聊天＃";
