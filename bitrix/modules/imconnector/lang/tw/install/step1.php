<?php
$MESS["IMCONNECTOR_PUBLIC_PATH"] = "網站公共地址：";
$MESS["IMCONNECTOR_PUBLIC_PATH_DESC"] = "連接器需要按預期運行的公共站點地址。";
$MESS["IMCONNECTOR_PUBLIC_PATH_DESC_2"] = "如果限制了對網絡的外部訪問，請僅訪問某些頁面。請參閱＃link_start＃documentation＃link_end＃有關詳細信息。";
