<?php
$MESS["FORM_ACCESS_DENIED"] = "您沒有足夠的權限來查看表格。";
$MESS["FORM_ACCESS_DENIED_FOR_FORM"] = "您沒有足夠的權限來查看表格。";
$MESS["FORM_ACCESS_DENIED_FOR_FORM_RESULTS"] = "您沒有足夠的權限來查看此結果。";
$MESS["FORM_ACCESS_DENIED_FOR_FORM_RESULTS_EDITING"] = "您沒有足夠的權限來編輯此結果。";
$MESS["FORM_ACCESS_DENIED_FOR_FORM_WRITE"] = "您沒有足夠的權限來填寫表格。";
$MESS["FORM_DATA_SAVED"] = "所有更改均保存";
$MESS["FORM_DELETE_FILE"] = "刪除";
$MESS["FORM_DOWNLOAD"] = "下載";
$MESS["FORM_DOWNLOAD_FILE"] = "下載文件＃file_name＃";
$MESS["FORM_EMPTY_REQUIRED_FIELDS"] = "以下所需字段留為空白：";
$MESS["FORM_INCORRECT_DATE_FORMAT"] = "Field \“＃field_name＃\”的錯誤日期格式。";
$MESS["FORM_INCORRECT_FILE_TYPE"] = "Field \“＃field_name＃\”的錯誤文件類型（請選擇另一個文件）。";
$MESS["FORM_INCORRECT_FORM_ID"] = "不正確的表單ID。";
$MESS["FORM_MODULE_NOT_INSTALLED"] = "Web表單模塊未安裝！";
$MESS["FORM_NOTE_ADDOK"] = "謝謝。

您的申請表＃結果＃已收到。";
$MESS["FORM_NOTE_EDITOK"] = "所有更改均保存";
$MESS["FORM_PUBLIC_ICON_EDIT"] = "編輯Web形式參數";
$MESS["FORM_PUBLIC_ICON_EDIT_TPL"] = "編輯表單模板";
$MESS["FORM_RECORD_NOT_FOUND"] = "找不到記錄";
$MESS["FORM_RESULT_ACCESS_DENIED"] = "結果訪問被拒絕。";
$MESS["FORM_VIEW_FILE"] = "查看文件";
