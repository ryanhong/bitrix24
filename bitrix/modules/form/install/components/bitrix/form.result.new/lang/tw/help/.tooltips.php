<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>否CAHCE </i>：沒有執行緩存。";
$MESS["CHAIN_ITEM_LINK_TIP"] = "該字段指定導航鏈項目的鏈接。";
$MESS["CHAIN_ITEM_TEXT_TIP"] = "在這裡，您可以指定導航鏈項目標題。例如：<b>新表格</b>。";
$MESS["EDIT_URL_TIP"] = "指定結果編輯頁面的路徑。如果您不需要結果編輯頁面，則可以將此字段留空。";
$MESS["IGNORE_CUSTOM_TEMPLATE_TIP"] = "如果已檢查，將使用默認表單模板。";
$MESS["LIST_URL_TIP"] = "指定結果審核頁面的路徑。如果您不需要在表單提交時打開帶有結果的頁面，則可以將此字段留空。";
$MESS["SEF_FOLDER_TIP"] = "在此處指定頁面處於活動狀態時將在地址欄中顯示的文件夾名稱。";
$MESS["SEF_MODE_TIP"] = "檢查此選項啟用SEF模式。";
$MESS["SUCCESS_URL_TIP"] = "指定頁面的路徑，該頁面顯示有關成功表單提交的消息。如果用戶沒有結果編輯權限，則使用。您可以將此字段留空。";
$MESS["USE_EXTENDED_ERRORS_TIP"] = "如果已檢查，將顯示錯誤消息，將突出顯示錯誤的字段。";
$MESS["VARIABLE_ALIASES_RESULT_ID_TIP"] = "指定將傳遞Web表單結果ID的變量的名稱。默認值是<b> result_id </b>。";
$MESS["VARIABLE_ALIASES_WEB_FORM_ID_TIP"] = "指定將傳遞Web表單ID的變量的名稱。默認值是<b> web_form_id </b>。";
$MESS["WEB_FORM_ID_TIP"] = "下拉列表包含當前所有exssise Web表單。另外，您可以選擇其他<i>其他</i>使用通過_request傳遞的外部表單ID。";
