<?php
$MESS["FRLM_PARAM_EDIT_URL"] = "結果編輯頁面URL";
$MESS["FRLM_PARAM_FORMS"] = "表格（選擇無需處理網站上的所有表格）";
$MESS["FRLM_PARAM_LIST_URL"] = "結果頁網址";
$MESS["FRLM_PARAM_NUM_RESULTS"] = "每張表格的結果";
$MESS["FRLM_PARAM_VIEW_URL"] = "結果查看頁面URL";
