<?php
$MESS["COMP_FORM_GROUP_PARAMS"] = "組件參數";
$MESS["COMP_FORM_NAME_TEMPLATE"] = "名稱格式";
$MESS["COMP_FORM_PARAMS_CHAIN_ITEM_LINK"] = "鏈接的其他導航鏈項目";
$MESS["COMP_FORM_PARAMS_CHAIN_ITEM_TEXT"] = "附加導航鏈項目的名稱";
$MESS["COMP_FORM_PARAMS_EDIT_URL"] = "結果編輯頁面";
$MESS["COMP_FORM_PARAMS_RESULT_ID"] = "結果ID";
$MESS["COMP_FORM_PARAMS_SHOW_ADDITIONAL"] = "顯示輔助Web表單字段";
$MESS["COMP_FORM_PARAMS_SHOW_ANSWER_VALUE"] = "顯示答案_VALUE參數的值";
$MESS["COMP_FORM_PARAMS_SHOW_STATUS"] = "顯示當前結果狀態";
$MESS["COMP_FORM_SEF_RESULT_VIEW_PAGE"] = "查看頁面的URL模板";
$MESS["FORM_COMP_VALUE_NO"] = "不";
$MESS["FORM_COMP_VALUE_YES"] = "是的";
