<?php
$MESS["FORM_CRM_TITLEBAR_EDIT"] = "編輯CRM服務器";
$MESS["FORM_CRM_TITLEBAR_NEW"] = "新的CRM服務器";
$MESS["FORM_DEFAULT_PERMISSION"] = "新Web表單的默認權限：";
$MESS["FORM_EDIT_RESULT_TEMPLATE_PATH"] = "導致編輯模板的途徑：";
$MESS["FORM_PRINT_RESULT_TEMPLATE_PATH"] = "通往結果打印輸出模板的途徑：";
$MESS["FORM_RECORDS_LIMIT"] = "在SQL查詢中獲取的數據庫記錄的最大數量：";
$MESS["FORM_RESET"] = "重置";
$MESS["FORM_RESULTS_PAGEN"] = "每頁的Web表單結果：";
$MESS["FORM_SAVE"] = "節省";
$MESS["FORM_SHOW_RESULT_TEMPLATE_PATH"] = "結果路徑顯示模板：";
$MESS["FORM_SHOW_TEMPLATE_PATH"] = "形成顯示模板的路徑：";
$MESS["FORM_TAB_CRM"] = "CRM連接";
$MESS["FORM_TAB_CRM_ADD_BUTTON"] = "添加";
$MESS["FORM_TAB_CRM_CHECK"] = "檢查連接";
$MESS["FORM_TAB_CRM_CHECK_ERROR"] = "錯誤：＃錯誤＃";
$MESS["FORM_TAB_CRM_CHECK_LOADING"] = "發送授權請求...";
$MESS["FORM_TAB_CRM_CHECK_NO"] = "沒有授權數據";
$MESS["FORM_TAB_CRM_CONFIRM"] = "刪除連接？";
$MESS["FORM_TAB_CRM_DELETE"] = "刪除";
$MESS["FORM_TAB_CRM_EDIT"] = "編輯";
$MESS["FORM_TAB_CRM_FORM_URL_PATH"] = "小路";
$MESS["FORM_TAB_CRM_FORM_URL_SERVER"] = "CRM服務器";
$MESS["FORM_TAB_CRM_NOTE"] = "沒有客戶門戶。";
$MESS["FORM_TAB_CRM_NOTE_LINK"] = "創建鏈接";
$MESS["FORM_TAB_CRM_ROW_ACTIVE"] = "積極的";
$MESS["FORM_TAB_CRM_ROW_AUTH"] = "驗證";
$MESS["FORM_TAB_CRM_ROW_AUTH_LOGIN"] = "登入";
$MESS["FORM_TAB_CRM_ROW_AUTH_PASSWORD"] = "密碼";
$MESS["FORM_TAB_CRM_ROW_AUTH_PASSWORD_SHOW"] = "顯示密碼";
$MESS["FORM_TAB_CRM_ROW_AUTH_SHOW"] = "身份驗證參數";
$MESS["FORM_TAB_CRM_ROW_TITLE"] = "姓名";
$MESS["FORM_TAB_CRM_ROW_URL"] = "URL";
$MESS["FORM_TAB_CRM_SECTION_TITLE"] = "客戶門戶";
$MESS["FORM_TAB_CRM_TITLE"] = "Bitrix Intranet連接參數";
$MESS["FORM_TAB_CRM_UNTITLED"] = " -  無標題  - ";
$MESS["FORM_TAB_CRM_WRONG_URL"] = "不正確的CRM URL";
$MESS["FORM_USE_HTML_EDIT"] = "使用Visual HTML編輯器";
$MESS["MAIN_RESTORE_DEFAULTS"] = "恢復默認值";
$MESS["SIMPLE_MODE"] = "使用簡單的表單編輯器";
