<?php
$MESS["FORM_FORM_EDIT"] = "表格＃＃id＃的參數";
$MESS["FORM_FORM_LIST"] = "表格列表";
$MESS["FORM_FORM_NAME"] = "形式：";
$MESS["FORM_NOT_FOUND"] = "不正確的表單ID";
$MESS["FORM_PAGE_TITLE"] = "表單視圖模板＃＃ID＃";
$MESS["FORM_PAGE_TITLE_NEW"] = "新結果";
$MESS["FORM_SHOW_TEMPLATE"] = "表單視圖模板：";
