<?php
$MESS["FORM_MENU_FORMS"] = "管理表格";
$MESS["FORM_MENU_FORMS_ALT"] = "可用表格列表";
$MESS["FORM_MENU_LIBRARY"] = "形式功能";
$MESS["FORM_MENU_LIBRARY_ALT"] = "包含Web表單功能的文件";
$MESS["FORM_MENU_MAIN"] = "網絡表格";
$MESS["FORM_MENU_MAIN_TITLE"] = "網絡表格";
$MESS["FORM_RESULTS_ALL"] = "結果";
$MESS["FORM_RESULTS_ALL_ALT"] = "表格結果列表";
$MESS["FORM_RESULTS_ALT"] = "表單\“＃名稱＃\”的結果列表";
