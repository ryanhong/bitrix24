<?php
$MESS["FORM_AUTHORIZED"] = "授權";
$MESS["FORM_CHANGE_STATUS"] = "將結果狀態更改為\ \“＃status_name＃\”的形式＃form_sid＃＃";
$MESS["FORM_CHANGE_STATUS_B"] = "您好＃RS​​_USER_NAME＃！
請求的狀態？＃rs_result_id＃已更改為[＃rs_status_name＃]（＃rs_form_name＃）＃server_name＃
-------------------------------------------------- --------------------------- -------
這是一個自動消息。";
$MESS["FORM_CHANGE_STATUS_S"] = "＃server_name＃：請求的狀態？＃rs_result_id＃已更改為[＃rs_status_name＃]（＃rs_form_name＃）";
$MESS["FORM_GENERATING_FINISHED"] = "已經創建了郵件模板。";
$MESS["FORM_L_DATE_CREATE"] = "表格完成日期";
$MESS["FORM_L_EMAIL_TO"] = "接受者";
$MESS["FORM_L_FORM_ID"] = "形式ID";
$MESS["FORM_L_NAME"] = "表格名稱";
$MESS["FORM_L_RESULT_ID"] = "結果ID";
$MESS["FORM_L_SID"] = "形式SID";
$MESS["FORM_L_STATUS_ID"] = "結果狀態ID";
$MESS["FORM_L_STATUS_NAME"] = "結果狀態名稱";
$MESS["FORM_L_USER_EMAIL"] = "用戶電子郵件";
$MESS["FORM_L_USER_ID"] = "用戶身份";
$MESS["FORM_L_USER_NAME"] = "用戶首先和姓氏";
$MESS["FORM_NOT_AUTHORIZED"] = "未經授權";
$MESS["FORM_NOT_REGISTERED"] = "未註冊";
$MESS["FORM_PAGE_TITLE"] = "生成電子郵件模板";
$MESS["FORM_VIEW_TEMPLATE"] = "查看模板";
$MESS["FORM_WEB_FORMS"] = "[Web表格]";
