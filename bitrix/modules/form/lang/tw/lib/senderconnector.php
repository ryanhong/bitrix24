<?php
$MESS["sender_connector_form_empty"] = "（沒有可用表格）";
$MESS["sender_connector_form_field_email"] = "電子郵件字段：";
$MESS["sender_connector_form_field_form"] = "形式：";
$MESS["sender_connector_form_field_name"] = "名稱字段：";
$MESS["sender_connector_form_field_select"] = "（選擇一個字段）";
$MESS["sender_connector_form_name"] = "網絡表格";
$MESS["sender_connector_form_prop_empty"] = "（沒有可用字段）";
$MESS["sender_connector_form_required_settings"] = "此功能需要配置";
$MESS["sender_connector_form_select"] = "（選擇表格）";
