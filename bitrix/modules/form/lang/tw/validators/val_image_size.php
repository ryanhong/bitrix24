<?php
$MESS["FORM_VALIDATOR_IMAGE_SIZE_DESCRIPTION"] = "圖片大小";
$MESS["FORM_VALIDATOR_IMAGE_SIZE_ERROR_HEIGHT_LESS"] = "＃field_name＃：圖像高度太小";
$MESS["FORM_VALIDATOR_IMAGE_SIZE_ERROR_HEIGHT_MORE"] = "＃field_name＃：圖像高度太大";
$MESS["FORM_VALIDATOR_IMAGE_SIZE_ERROR_WIDTH_LESS"] = "＃field_name＃：圖像寬度太小";
$MESS["FORM_VALIDATOR_IMAGE_SIZE_ERROR_WIDTH_MORE"] = "＃field_name＃：圖像寬度太大";
$MESS["FORM_VALIDATOR_IMAGE_SIZE_SETTINGS_HEIGHT_FROM"] = "最小高度";
$MESS["FORM_VALIDATOR_IMAGE_SIZE_SETTINGS_HEIGHT_TO"] = "最大高度";
$MESS["FORM_VALIDATOR_IMAGE_SIZE_SETTINGS_WIDTH_FROM"] = "最小寬度";
$MESS["FORM_VALIDATOR_IMAGE_SIZE_SETTINGS_WIDTH_TO"] = "最大寬度";
