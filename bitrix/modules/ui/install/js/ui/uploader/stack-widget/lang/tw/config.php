<?php
$MESS["STACK_WIDGET_ABORT_UPLOAD"] = "取消上傳";
$MESS["STACK_WIDGET_DRAG_FILES_HINT"] = "將文件放在這裡";
$MESS["STACK_WIDGET_DRAG_FILE_HINT"] = "在此處刪除文件";
$MESS["STACK_WIDGET_FILES_UPLOADING"] = "上傳文件";
$MESS["STACK_WIDGET_FILE_COUNT"] = "文件：＃計數＃";
$MESS["STACK_WIDGET_FILE_UPLOADING"] = "上傳文件";
$MESS["STACK_WIDGET_FILE_UPLOAD_ERROR"] = "上傳錯誤";
$MESS["STACK_WIDGET_OPEN_FILE_GALLERY"] = "開放畫廊";
$MESS["STACK_WIDGET_POPUP_TITLE"] = "上傳文件";
$MESS["STACK_WIDGET_UPLOAD_FILE"] = "上傳文件";
$MESS["STACK_WIDGET_UPLOAD_FILES"] = "上傳文件";
$MESS["STACK_WIDGET_UPLOAD_IMAGE"] = "上傳圖片";
$MESS["STACK_WIDGET_UPLOAD_IMAGES"] = "上傳圖片";
