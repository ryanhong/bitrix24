<?php
$MESS["UI_STEP_PROCESSING_AUTH_ERROR"] = "您的會議已經過期。請再次授權。";
$MESS["UI_STEP_PROCESSING_BTN_CLOSE"] = "關閉";
$MESS["UI_STEP_PROCESSING_BTN_START"] = "跑步";
$MESS["UI_STEP_PROCESSING_BTN_STOP"] = "停止";
$MESS["UI_STEP_PROCESSING_CANCELED"] = "取消";
$MESS["UI_STEP_PROCESSING_CANCELING"] = "取消...";
$MESS["UI_STEP_PROCESSING_COMPLETED"] = "完全的";
$MESS["UI_STEP_PROCESSING_EMPTY_ERROR"] = "空的";
$MESS["UI_STEP_PROCESSING_FILE_DELETE"] = "刪除文件";
$MESS["UI_STEP_PROCESSING_FILE_DOWNLOAD"] = "下載文件";
$MESS["UI_STEP_PROCESSING_FILE_EMPTY_ERROR"] = "需要上傳的文件";
$MESS["UI_STEP_PROCESSING_REQUEST_ERR"] = "錯誤處理請求。";
$MESS["UI_STEP_PROCESSING_WAITING"] = "請稍等...";
