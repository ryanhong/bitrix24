<?php
$MESS["DISK_ONLYOFFICE_MSG_TO_GENERAL_CHAT_AFTER_DEMO_FEATURE_F"] = "＃用戶＃激活了Bitrix24.Docs的試驗期，Docs是一種新的協作工具。快點嘗試一下，直到＃trial_period_end_date＃。 [url =＃helpdesk_link＃]詳細信息[/url]";
$MESS["DISK_ONLYOFFICE_MSG_TO_GENERAL_CHAT_AFTER_DEMO_FEATURE_M"] = "＃用戶＃激活了Bitrix24.Docs的試驗期，Docs是一種新的協作工具。快點嘗試一下，直到＃trial_period_end_date＃。 [url =＃helpdesk_link＃]詳細信息[/url]";
