<?php
$MESS["DISK_B24_FEATURES_DISK_EXTERNAL_LINK_1_DESCR"] = "在您的Bitrix24上共享指向文檔的鏈接。選擇何時希望鏈接到期並設置密碼以確保您的信息安全。共享鏈接和許多其他功能在選定的商業計劃中提供。";
$MESS["DISK_B24_FEATURES_DISK_EXTERNAL_LINK_1_TITLE"] = "公共鏈接到文件和文件夾";
$MESS["DISK_B24_FEATURES_DISK_EXTERNAL_LINK_DESCR"] = "在您的Bitrix24上共享指向文檔的鏈接。選擇何時希望鏈接到期並設置密碼以確保您的信息安全。共享鏈接和許多其他功能在選定的商業計劃中提供。";
$MESS["DISK_B24_FEATURES_DISK_EXTERNAL_LINK_TITLE"] = "公共鏈接到文件和文件夾";
$MESS["DISK_B24_FEATURES_DISK_FILE_SHARING_1_DESCR"] = "如果要為其他用戶提供一個文件或整個文件夾以查看，編輯或上傳文件，請升級到商業計劃。";
$MESS["DISK_B24_FEATURES_DISK_FILE_SHARING_1_TITLE"] = "升級到商業計劃，與其他員工共享您的驅動器文件和文件夾";
$MESS["DISK_B24_FEATURES_DISK_FOLDER_SHARING_1_DESCR"] = "如果要為其他用戶提供一個文件或整個文件夾以查看，編輯或上傳文件，請升級到商業計劃。";
$MESS["DISK_B24_FEATURES_DISK_FOLDER_SHARING_1_TITLE"] = "升級到商業計劃，與其他員工共享您的驅動器文件和文件夾";
$MESS["DISK_B24_FEATURES_DISK_FOLDER_SHARING_DESCR"] = "在當前計劃中，您只能與同事在驅動器上共享文件。如果您想將整個文件夾用於其他用戶以查看，編輯或上傳文件，請升級到商業計劃。文件夾共享可以在選定的商業計劃中獲得。";
$MESS["DISK_B24_FEATURES_DISK_FOLDER_SHARING_TITLE"] = "升級到商業計劃與其他員工共享您的驅動文件夾";
