<?php
$MESS["DISK_OBJECT_ENTITY_ERROR_DELETE_NODE"] = "無法刪除樹節點。首先刪除所有子項目。";
$MESS["DISK_OBJECT_ENTITY_ERROR_FIELD_NAME_HAS_INVALID_CHARS"] = "名字中含有無效字符。";
$MESS["DISK_OBJECT_ENTITY_ERROR_LINK_FILE_ID"] = "＃字段＃已為Symlink文件指定。";
$MESS["DISK_OBJECT_ENTITY_ERROR_NAME"] = "＃字段＃的無效值。";
$MESS["DISK_OBJECT_ENTITY_ERROR_REQUIRED_FILE_ID"] = "文件的＃字段＃未指定。";
$MESS["DISK_OBJECT_ENTITY_ERROR_UPDATE_PARENT_ID"] = "對於現有對象，無法更新\“＃字段＃\”。請使用\“ move \”操作。";
$MESS["DISK_OBJECT_ENTITY_ERROR_UPDATE_STORAGE_ID"] = "對於現有對象，無法更新\“＃字段＃\”。";
$MESS["DISK_OBJECT_ENTITY_NAME_FIELD"] = "姓名";
$MESS["DISK_OBJECT_ENTITY_PARENT_ID_FIELD"] = "父母";
$MESS["DISK_OBJECT_ENTITY_STORAGE_ID_FIELD"] = "貯存";
