<?php
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE01"] = "論壇貼文";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE02"] = "評論";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE03"] = "任務評論";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE04"] = "日曆事件評論";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE05"] = "文檔評論";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE06"] = "CRM評論";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE07"] = "用戶文檔評論";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE08"] = "小組論壇帖子";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE09"] = "用戶論壇發布";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE10"] = "新聞評論";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE11"] = "照片評論";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE12"] = "Wiki評論";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE13"] = "工作報告評論";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE14"] = "工作時間記錄評論";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE15"] = "信息塊項目評論";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE16"] = "會議評論";
