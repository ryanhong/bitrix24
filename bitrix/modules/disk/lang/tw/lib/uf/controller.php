<?php
$MESS["DISK_UF_CONTROLLER_CANCEL"] = "取消";
$MESS["DISK_UF_CONTROLLER_DESCR_DISABLE_ATTACH_NON_PUBLIC_FILE"] = "該文件未發布。運行文檔發佈業務流程。";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_FIND_FIND_FOLDER"] = "找不到文件夾。";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_FIND_USER_STORAGE"] = "找不到用戶存儲。";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE"] = "社會服務＃名稱＃未配置。請聯繫您的門戶管理員。";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE_B24"] = "社交網絡服務＃名稱＃未配置。請聯繫您的BitRix24管理員。";
$MESS["DISK_UF_CONTROLLER_FILE_IS_TOO_BIG_FOR_TRANSFORMATION"] = "視頻文件太大，無法在Bitrix24中查看。 <a class = \“transformer-upgrade-popup \" href= \"javascript:void(0); \ \">詳細信息</a>";
$MESS["DISK_UF_CONTROLLER_MY_DOCUMENTS"] = "我的開車";
$MESS["DISK_UF_CONTROLLER_MY_GROUPS"] = "團體驅動器";
$MESS["DISK_UF_CONTROLLER_RECENTLY_USED"] = "最近的項目";
$MESS["DISK_UF_CONTROLLER_SAVE_DOCUMENT_TITLE"] = "選擇一個文件夾以將文檔保存到";
$MESS["DISK_UF_CONTROLLER_SELECT_DOCUMENT"] = "選擇文檔";
$MESS["DISK_UF_CONTROLLER_SELECT_DOCUMENT_TITLE"] = "選擇一個或多個文檔";
$MESS["DISK_UF_CONTROLLER_SELECT_FOLDER"] = "選擇當前文件夾";
$MESS["DISK_UF_CONTROLLER_SELECT_ONE_DOCUMENT_TITLE"] = "選擇文檔";
$MESS["DISK_UF_CONTROLLER_SHARED_DOCUMENTS"] = "公司驅動器";
$MESS["DISK_UF_CONTROLLER_TITLE_FILE_SIZE"] = "尺寸";
$MESS["DISK_UF_CONTROLLER_TITLE_MODIFIED_BY"] = "修改";
$MESS["DISK_UF_CONTROLLER_TITLE_NAME"] = "姓名";
$MESS["DISK_UF_CONTROLLER_TITLE_SELECT"] = "選擇文檔";
$MESS["DISK_UF_CONTROLLER_TITLE_TIMESTAMP"] = "修改";
$MESS["DISK_UF_CONTROLLER_TRANSFORMATION_UPGRADE_POPUP_CONTENT"] = "
<b>不幸的是，視頻文件的大小太大而無法在Bitrix24中查看。< /b> <br />
我們保留文件，但用戶必須在查看它之前下載它。<br /> <br />
可以在免費計劃中播放最多300 MB的文件。<br />
商業計劃可以播放最多3 GB的文件。<br /> <br />
現在升級並享受更多有用的功能：
<ul class = \“ hide-features-list \”>
<li class = \“ hide-features-list-item \”>無限備份文件副本</li>
<li class = \“ hide-features-list-item \”>文檔鎖定</li>
<li class = \“ hide-features-list-item \”>公共文件夾鏈接</li>
<li class = \“ hide-features-list-item \”>與公司驅動器一起使用工作流</li>
</ul>
請參閱計劃比較圖表和完整功能描述<a target= \"_blank \" href= \"https://www.bitrix24.com/prices/index.php \">在這裡</a>。";
$MESS["DISK_UF_CONTROLLER_TRANSFORMATION_UPGRADE_POPUP_TITLE"] = "僅在擴展Bitrix24中可用";
