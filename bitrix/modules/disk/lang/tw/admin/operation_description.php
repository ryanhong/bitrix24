<?php
$MESS["OP_NAME_DISK_ADD"] = "添加項目";
$MESS["OP_NAME_DISK_CREATE_WF"] = "創建業務流程模板";
$MESS["OP_NAME_DISK_DELETE"] = "刪除";
$MESS["OP_NAME_DISK_DELETE_2"] = "將物品刪除到回收箱";
$MESS["OP_NAME_DISK_DISK_DESTROY"] = "從回收箱中刪除項目";
$MESS["OP_NAME_DISK_DISK_RESTORE"] = "從回收箱中恢復項目";
$MESS["OP_NAME_DISK_EDIT"] = "編輯項目";
$MESS["OP_NAME_DISK_READ"] = "查看項目";
$MESS["OP_NAME_DISK_RIGHTS"] = "管理訪問權限";
$MESS["OP_NAME_DISK_SETTINGS"] = "存儲設置";
$MESS["OP_NAME_DISK_SHARING"] = "共享項目";
$MESS["OP_NAME_DISK_START_BP"] = "運行業務流程";
