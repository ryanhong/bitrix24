<?php
$MESS["DISK_JS_FILE_DIALOG_OAUTH_NOTICE"] = "要從外部磁盤加載文件，<br/>請登錄到您的<a id = \"bx-js-disk-run-oauth-modal \“href= \"# \"> #service＃</ a>帳戶。";
$MESS["DISK_JS_FILE_DIALOG_OAUTH_NOTICE_DETAIL"] = "<p>該文件將保存到Bitrix24.Drive的上傳文件夾。將不會查看外部文件的任何進一步更新。</p> <p>一旦將外部帳戶添加到Bitrix24，它將可以通過任何可能使用它的函數訪問。</p>";
$MESS["DISK_JS_FILE_DIALOG_OAUTH_NOTICE_DETAIL_B24"] = "<p>該文件將保存到Bitrix24.Drive的上傳文件夾。將不會查看外部文件的任何進一步更新。</p> <p>一旦將外部帳戶添加到bitrix24，它將可以通過任何可能使用它的函數訪問。</p> <p>使用您的bitrix 。";
