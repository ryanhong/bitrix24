<?php
$MESS["DISK_JS_INF_POPUPS_EDIT_IN_LOCAL_SERVICE"] = "在我的電腦上編輯";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_GO_TO_FILE"] = "轉到文件";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_TITLE"] = "文件沒有保存！";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_WAS_LOCKED_FORKED_COPY"] = "您在編輯時已鎖定該文檔。您的所有更改都將保存到<a class= \"disk-locked-document-popup-content-link \" href= \“#link# \" target= \"_blank \">存儲<的新文檔中。 /a>文件夾。";
$MESS["DISK_JS_INF_POPUPS_SERVICE_LOCAL_INSTALL_DESKTOP"] = "要在計算機上更有效地處理文檔並享受更好的用戶體驗，請安裝桌面應用程序並連接Bitrix24.Drive。";
$MESS["DISK_JS_INF_POPUPS_SWITCH_ON_BDISK_DESCR"] = "＃a＃連接bitrix24.drive＃a_end＃可以更輕鬆地在計算機上處​​理文檔";
$MESS["DISK_JS_INF_POPUPS_SWITCH_ON_BDISK_TITLE"] = "使用文檔";
