<?php
$MESS["DISK_INSTALL_DESCRIPTION"] = "一個用於創建，編輯和管理文件和文件夾的模塊";
$MESS["DISK_INSTALL_NAME"] = "駕駛";
$MESS["DISK_INSTALL_TITLE"] = "模塊安裝";
$MESS["DISK_NOTIFY_MIGRATE_WEBDAV"] = "請<a href= \"#link# \">轉換文檔庫模塊的數據</a>遷移到驅動器模塊。";
$MESS["DISK_UNINSTALL_ERROR_MIGRATE_PROCESS"] = "由於文檔庫模塊數據仍在轉換，因此無法卸載該模塊。";
$MESS["DISK_UNINSTALL_TITLE"] = "模塊卸載";
