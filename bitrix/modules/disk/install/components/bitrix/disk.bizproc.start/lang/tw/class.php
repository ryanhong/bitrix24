<?php
$MESS["BPABS_EMPTY_DOC_ID"] = "沒有指定要創建業務流程的文檔ID。";
$MESS["BPABS_EMPTY_DOC_TYPE"] = "需要文檔類型。";
$MESS["BPABS_NO_PERMS"] = "您無權為此文檔運行業務流程。";
$MESS["BPABS_TITLE"] = "運行業務流程";
$MESS["BPATT_NO_MODULE_ID"] = "需要模塊ID。";
