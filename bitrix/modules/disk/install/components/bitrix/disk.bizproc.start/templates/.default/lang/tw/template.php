<?php
$MESS["BPABS_DESCRIPTION"] = "業務流程模板描述";
$MESS["BPABS_DO_CANCEL"] = "取消";
$MESS["BPABS_DO_START"] = "開始";
$MESS["BPABS_INVALID_TYPE"] = "參數類型未定義。";
$MESS["BPABS_MESSAGE_ERROR"] = "無法啟動“＃template＃”類型的業務流程。";
$MESS["BPABS_MESSAGE_SUCCESS"] = "“＃模板＃”業務流程已成功啟動。";
$MESS["BPABS_NAME"] = "業務過程模板名稱";
$MESS["BPABS_NO"] = "不";
$MESS["BPABS_NO_TEMPLATES"] = "該文檔類型找不到業務流程模板。";
$MESS["BPABS_TAB"] = "業務流程";
$MESS["BPABS_TAB_TITLE"] = "業務流程運行參數";
$MESS["BPABS_YES"] = "是的";
$MESS["DISK_TITLE_TEMPLATES_OLD"] = "在轉換為新驅動器之前創建的舊模板";
