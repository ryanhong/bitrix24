<?php
$MESS["DFU_CANCEL"] = "取消";
$MESS["DFU_CLOSE"] = "關閉";
$MESS["DFU_DND1"] = "上傳文件或圖像";
$MESS["DFU_DND2"] = "使用Drag'n'Drop添加";
$MESS["DFU_DUPLICATE"] = "這個名稱的文件已經存在。";
$MESS["DFU_REPLACE"] = "代替";
$MESS["DFU_SAVE_BP"] = "保存參數";
$MESS["DFU_SAVE_BP_DIALOG"] = "上傳";
$MESS["DFU_UPLOAD"] = "上傳更多";
$MESS["DFU_UPLOAD_CANCELED"] = "上傳取消";
$MESS["DFU_UPLOAD_TITLE1"] = "上傳新文檔";
$MESS["DFU_UPLOAD_TITLE2"] = "上傳＃＃count＃文件的＃＃文件";
