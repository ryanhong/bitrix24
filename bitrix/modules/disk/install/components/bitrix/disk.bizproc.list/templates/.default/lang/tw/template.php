<?php
$MESS["BPATT_ALL"] = "全部的";
$MESS["BPATT_AUTO_EXECUTE"] = "汽車";
$MESS["BPATT_HELP1_TEXT"] = "國家驅動的業務流程是一個連續的業務流程，具有訪問權限分發，以處理不同狀態的文檔。";
$MESS["BPATT_HELP2_TEXT"] = "順序業務流程是一個簡單的業務流程，可以在文檔上執行一系列連續的操作。";
$MESS["BPATT_MODIFIED"] = "修改的";
$MESS["BPATT_NAME"] = "姓名";
$MESS["BPATT_USER"] = "修改";
$MESS["DISK_BIZPROC_LIST_CREATE_BP"] = "創造";
$MESS["PROMPT_OLD_TEMPLATE"] = "最近的驅動器更新使一些業務流程模板過時了。您仍然可以使用它們。但是，新的模板編輯表單將有所不同。遺產模板被突出顯示。";
$MESS["WD_EMPTY"] = "沒有業務流程模板。創建標準<a href=#href#>業務流程</a>。";
$MESS["WD_EMPTY_NEW"] = "沒有新版本的業務流程模板。 <a href=#href#>創建</a>。";
