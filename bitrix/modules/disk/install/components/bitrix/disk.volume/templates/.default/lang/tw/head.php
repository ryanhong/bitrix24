<?php
$MESS["DISK_VOLUME_ADMIN_MODE"] = "管理模式";
$MESS["DISK_VOLUME_ADMIN_MODE_COMMENT"] = "您現在處於管理員模式。您可以查看並刪除受限制的數據。";
$MESS["DISK_VOLUME_ADMIN_MODE_EXIT"] = "退出管理模式";
$MESS["DISK_VOLUME_CANCEL_BUTTON"] = "取消";
$MESS["DISK_VOLUME_CANCEL_WORKERS"] = "取消所有乾淨的任務";
$MESS["DISK_VOLUME_CLOSE_WARNING"] = "門戶網站必須保持打開狀態才能完成驅動器清理。";
$MESS["DISK_VOLUME_COUNT"] = "文件：＃file_count＃";
$MESS["DISK_VOLUME_DELETE_BUTTON"] = "刪除";
$MESS["DISK_VOLUME_DELETE_CONFIRM_TITLE"] = "確認刪除";
$MESS["DISK_VOLUME_DELETE_FILE_CONFIRM"] = "您確定要刪除文件'＃名稱＃'嗎？";
$MESS["DISK_VOLUME_DELETE_FILE_UNNECESSARY_VERSION_CONFIRM"] = "您確定要刪除“＃名稱＃”的未使用版本？";
$MESS["DISK_VOLUME_DELETE_FOLDER_CONFIRM"] = "您確定要刪除文件夾'＃名稱＃'及其所有內容嗎？";
$MESS["DISK_VOLUME_DELETE_FOLDER_UNNECESSARY_VERSION_CONFIRM"] = "您確定要在“＃名稱＃”中刪除未使用的文件版本？";
$MESS["DISK_VOLUME_DELETE_ROOT_UNNECESSARY_VERSION_CONFIRM"] = "您確定要在驅動器的根文件夾中刪除未使用的文件版本嗎？";
$MESS["DISK_VOLUME_DELETE_STORAGE_SAFE_CLEAR_CONFIRM"] = "您確定要刪除未使用的文件版本並在驅動器'＃名稱＃'上清空回收箱？";
$MESS["DISK_VOLUME_DELETE_TRASHCAN_SAFE_CLEAR_CONFIRM"] = "您確定要在驅動器'＃名稱＃'上清空回收箱嗎？";
$MESS["DISK_VOLUME_DELETE_UPLOADED_SAFE_CLEAR_CONFIRM"] = "您確定要在驅動器'＃名稱＃'上的上傳文件夾中刪除未使用的FIE版本嗎？";
$MESS["DISK_VOLUME_DISK"] = "駕駛：";
$MESS["DISK_VOLUME_DISK_B24"] = "Bitrix24.Drive";
$MESS["DISK_VOLUME_DISK_TOTAL_COUNT"] = "總文件：＃file_count＃";
$MESS["DISK_VOLUME_DISK_TOTAL_USEAGE"] = "總計使用：＃file_size＃";
$MESS["DISK_VOLUME_DISK_TRASHCAN"] = "回收箱";
$MESS["DISK_VOLUME_DISK_USAGE"] = "驅動用法摘要：";
$MESS["DISK_VOLUME_DROPPED_TRASHCAN_HINT"] = "所有已刪除的文件都移至回收箱，以便於恢復。將回收箱清空以供空間。";
$MESS["DISK_VOLUME_EMPTY_FOLDER_CONFIRM"] = "您確定要刪除“＃名稱＃”的內容？";
$MESS["DISK_VOLUME_EMPTY_ROOT_CONFIRM"] = "您確定要刪除驅動器根文件夾的內容嗎？";
$MESS["DISK_VOLUME_EXPERT_MODE"] = "高級模式";
$MESS["DISK_VOLUME_EXPERT_MODE_EXIT"] = "退出專家模式";
$MESS["DISK_VOLUME_EXPERT_MODE_OFF"] = "簡單模式";
$MESS["DISK_VOLUME_EXPERT_MODE_ON"] = "專業級";
$MESS["DISK_VOLUME_GROUP_DELETE_FILE_CONFIRM"] = "您確定要刪除所選文件嗎？";
$MESS["DISK_VOLUME_GROUP_DELETE_FILE_UNNECESSARY_VERSION_CONFIRM"] = "您確定要刪除所選文件的未使用版本嗎？";
$MESS["DISK_VOLUME_GROUP_DISK_SAFE_CLEAR_CONFIRM"] = "您確定要刪除未使用的文件版本並在選定的驅動器上清空回收箱嗎？";
$MESS["DISK_VOLUME_GROUP_FOLDER_DROP_CONFIRM"] = "您確定要刪除所選文件夾及其所有內容嗎？";
$MESS["DISK_VOLUME_GROUP_FOLDER_EMPTY_CONFIRM"] = "您確定要刪除所選文件夾的內容嗎？";
$MESS["DISK_VOLUME_GROUP_FOLDER_SAFE_CLEAR_CONFIRM"] = "您確定要刪除所選文件夾中未使用的文件版本嗎？";
$MESS["DISK_VOLUME_GROUP_TRASHCAN_SAFE_CLEAR_CONFIRM"] = "您確定要清空選定驅動器上的回收箱嗎？";
$MESS["DISK_VOLUME_GROUP_UPLOADED_SAFE_CLEAR_CONFIRM"] = "您確定要在選定驅動器上上傳文件夾中刪除未使用的FIE版本嗎？";
$MESS["DISK_VOLUME_MEASURE"] = "驅動掃描";
$MESS["DISK_VOLUME_MEASURE_ACCEPT"] = "是的，再次掃描";
$MESS["DISK_VOLUME_MEASURE_CONFIRM"] = "驅動掃描將停止清潔過程。只有在掃描驅動器後，您才能重新啟動它。";
$MESS["DISK_VOLUME_MEASURE_CONFIRM_QUESTION"] = "您確定要取消清潔並再次掃描驅動器嗎？";
$MESS["DISK_VOLUME_MEASURE_DATA_REPEAT"] = "重複掃描";
$MESS["DISK_VOLUME_NEED_RELOAD_COMMENT"] = "以前的掃描結果可能已過時。";
$MESS["DISK_VOLUME_PERFORMING_CANCEL_MEASURE"] = "取消掃描";
$MESS["DISK_VOLUME_PERFORMING_CANCEL_WORKERS"] = "取消預定的清理";
$MESS["DISK_VOLUME_PERFORMING_MEASURE_DATA"] = "數據採集";
$MESS["DISK_VOLUME_PERFORMING_QUEUE"] = "收集數據。步驟＃queue_step＃of＃queue_length＃＃";
$MESS["DISK_VOLUME_SETUP_CLEANER"] = "安排清理工作";
$MESS["DISK_VOLUME_SIZE"] = "大小：＃file_size＃";
$MESS["DISK_VOLUME_STEPPER_STEPS_HINT"] = "通過清理 /要刪除的文件數刪除的文件數量";
$MESS["DISK_VOLUME_TRASHCAN"] = "回收箱：＃file_size＃";
$MESS["DISK_VOLUME_UNNECESSARY_VERSION_HINT"] = "BitRix24.Drive將當前文件版本損壞的情況下保留先前的文件版本，以供恢復。刪除未使用的文件版本以自由空間。";
$MESS["DISK_VOLUME_VERSION_FILES"] = "未使用的版本：＃file_size＃";
