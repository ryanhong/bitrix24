<?php
$MESS["DISK_SERVICE"] = "駕駛";
$MESS["DISK_VOLUME_DESC"] = "計算佔用的磁盤空間，並提示如果需要，請使用磁盤清潔工具。";
$MESS["DISK_VOLUME_NAME"] = "計算使用的磁盤空間";
