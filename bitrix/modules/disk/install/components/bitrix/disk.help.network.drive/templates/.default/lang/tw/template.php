<?php
$MESS["DISK_NETWORK_DRIVE_CONNECTOR_HELP_MAPDRIVE"] = "<h3>將庫映射為網絡驅動器</h3>
<p>使用<b>文件管理器將庫作為網絡磁盤連接</b>：
<ul>
<li>運行Windows Explorer </b>; </li>
<li>選擇<i>工具>映射網絡驅動器</i>。網絡光盤嚮導將打開：
<br /> <br /> <a href= \“javascript:showimg('#template_folder#/images/en/network_storage.png',629,459，'map network drive'); \ \">
<img width = \“ 250 \” height = \“ 183 \” border = \“ 0 \” src = \“＃template_folder＃/images/en en nreets_storage_storage_sm.png \” style = \“ style = \” cursor： cursor：pointer; pointer; \ \'\“ alt = \“單擊放大\”/> </a> </li>
<li>在<b> drive </b>字段中，指定一個字母將文件夾映射到; </li>
<li>在<b>文件夾</b>字段中，輸入庫的路徑：<i> http：//＆lt; your_server＆gt;＃template_link＃</i>。如果您希望該文件夾在系統啟動時可用，請檢查<b>在logon </b>選項上重新連接; </li>
<li>單擊<b>準備就緒</b>。如果提示了用戶名和密碼，請輸入您的登錄和密碼，然後單擊<b> ok </b>。</li>
</ul>
</p>
<p>以後，您可以在Windows Explorer中打開文件夾，該文件夾將在我的計算機下方顯示為驅動器或任何文件管理器中的文件夾。</p>";
$MESS["DISK_NETWORK_DRIVE_HELP_OSX"] = "<h3>連接Mac OS和Mac OS X </H3>中的庫
<ul>
<li> SELECT <i> Finder Go->連接到服務器命令</i>; </li>
<li>在<b>服務器地址中的庫地址中輸入</b>：</p>
<p> <a href= \“javascript:showimg('#template_folder#/images/en/macos.png',465,550，'mac os x'); \ \">
<img width = \“ 235 \” height = \“ 278 \” border = \“ 0 \” src = \“＃template_folder＃/images/en en/macos_sm.png \” style = \“ cursor：pointer; \ \” alt = \“單擊放大\”/> </a> </li>
</ul>";
$MESS["DISK_NETWORK_DRIVE_MACOS_TITLE"] = "Mac OS X中的地圖文檔庫";
$MESS["DISK_NETWORK_DRIVE_REGISTERPATCH"] = "當前的安全性首選項要求您<a href= \"#link# \">進行一些註冊表更改</a>以連接網絡驅動器。";
$MESS["DISK_NETWORK_DRIVE_SHAREDDRIVE_TITLE"] = "顯示作為網絡驅動器連接的說明";
$MESS["DISK_NETWORK_DRIVE_USECOMMANDLINE"] = "要使用HTTPS/SSL作為網絡驅動器連接庫，請使用<b> start> run> cmd </b>。在命令行中鍵入以下命令：";
