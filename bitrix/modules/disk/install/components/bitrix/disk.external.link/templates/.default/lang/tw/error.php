<?php
$MESS["DISK_EXT_LINK_B24"] = "Bitrix <span> 24 </span>";
$MESS["DISK_EXT_LINK_B24_ADV_1"] = "任務和項目";
$MESS["DISK_EXT_LINK_B24_ADV_2"] = "CRM";
$MESS["DISK_EXT_LINK_B24_ADV_3"] = "IM和團體聊天";
$MESS["DISK_EXT_LINK_B24_ADV_4"] = "文件";
$MESS["DISK_EXT_LINK_B24_ADV_5"] = "小組驅動";
$MESS["DISK_EXT_LINK_B24_ADV_6"] = "日曆";
$MESS["DISK_EXT_LINK_B24_ADV_CREATE_LINK_HREF"] = "http://www.bitrix24.com/?utm_source=fileShare_button＆utm_medium = referral＆utm_campaign=fileShare_button";
$MESS["DISK_EXT_LINK_B24_ADV_CREATE_LINK_TEXT"] = "立即創建您的BitRix24帳戶";
$MESS["DISK_EXT_LINK_B24_ADV_TEXT"] = "統一協作套件";
$MESS["DISK_EXT_LINK_DESCRIPTION"] = "該文件可能未發布，或者您單擊了無效的鏈接。 <br/>請聯繫文件所有者。";
$MESS["DISK_EXT_LINK_INVALID"] = "公共鏈接無效。";
$MESS["DISK_EXT_LINK_TEXT"] = "文件未找到";
$MESS["DISK_EXT_LINK_TITLE"] = "文件未找到";
