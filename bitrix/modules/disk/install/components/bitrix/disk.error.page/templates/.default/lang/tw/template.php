<?php
$MESS["DISK_ERROR_PAGE_BASE_DESCRIPTION"] = "我無法獲得您要求的信息。您可能沒有訪問權限或鏈接被打破。";
$MESS["DISK_ERROR_PAGE_BASE_DESCRIPTION_V2"] = "確保鏈接有效或要求資源所有者訪問";
$MESS["DISK_ERROR_PAGE_BASE_SOLUTION_1"] = " - 嘗試檢查鏈接是否錯誤或聯繫作者。";
$MESS["DISK_ERROR_PAGE_BASE_SOLUTION_2"] = " - 單擊您的瀏覽器中以返回上一頁。";
$MESS["DISK_ERROR_PAGE_TITLE"] = "什麼都沒找到。";
$MESS["DISK_ERROR_PAGE_TITLE_V2"] = "訪問被拒絕或鏈接不正確";
