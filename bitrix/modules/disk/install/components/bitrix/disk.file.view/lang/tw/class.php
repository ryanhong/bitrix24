<?php
$MESS["DISK_FILE_VIEW_BPABL_RES_1"] = "不";
$MESS["DISK_FILE_VIEW_BPABL_RES_2"] = "成功";
$MESS["DISK_FILE_VIEW_BPABL_RES_3"] = "取消";
$MESS["DISK_FILE_VIEW_BPABL_RES_4"] = "錯誤";
$MESS["DISK_FILE_VIEW_BPABL_RES_5"] = "未初始化";
$MESS["DISK_FILE_VIEW_BPABL_RES_6"] = "不明確的";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_1"] = "初始化";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_2"] = "進行中";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_3"] = "被取消";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_4"] = "完全的";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_5"] = "錯誤";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_6"] = "不明確的";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_1"] = "啟動動作'#Activity＃'＃Note＃";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_2"] = "完成操作'＃活動＃'，返回狀態：'＃狀態＃'，結果：'#result＃'＃Note＃＃";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_3"] = "取消操作'#activity＃'＃Note＃";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_4"] = "動作'#activity＃'＃注意＃錯誤";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_5"] = "動作'#activity＃'＃註釋＃";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_6"] = "在動作'#activity＃'＃Note＃上執行的某些內容";
$MESS["DISK_FILE_VIEW_COPY_LINK_TEXT"] = "複製鏈接";
$MESS["DISK_FILE_VIEW_COPY_LINK_TITLE"] = "複製文件鏈接";
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_FIND_OBJECT"] = "無法找到對象。";
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_FIND_VERSION"] = "找不到版本。";
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_SAVE_FILE"] = "無法保存文件。";
$MESS["DISK_FILE_VIEW_EXT_PARAMS_TIME_DAY"] = "天";
$MESS["DISK_FILE_VIEW_EXT_PARAMS_TIME_HOUR"] = "小時";
$MESS["DISK_FILE_VIEW_EXT_PARAMS_TIME_MIN"] = "分鐘";
$MESS["DISK_FILE_VIEW_FILE_DOWNLOAD_COUNT_BY_EXT_LINK"] = "從公共頁面下載";
$MESS["DISK_FILE_VIEW_GO_BACK_TEXT"] = "後退";
$MESS["DISK_FILE_VIEW_GO_BACK_TITLE"] = "後退";
$MESS["DISK_FILE_VIEW_HISTORY_ACT_DELETE"] = "刪除";
$MESS["DISK_FILE_VIEW_HISTORY_ACT_DOWNLOAD"] = "下載";
$MESS["DISK_FILE_VIEW_HISTORY_ACT_RESTORE"] = "恢復";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_CREATE_TIME"] = "創建於";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_CREATE_TIME_2"] = "修改";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_CREATE_USER"] = "由...製作";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_CREATE_USER_2"] = "修改";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_FORMATTED_SIZE"] = "尺寸";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_NAME"] = "姓名";
