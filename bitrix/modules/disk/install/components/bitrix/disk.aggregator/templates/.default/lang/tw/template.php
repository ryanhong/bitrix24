<?php
$MESS["DISK_AGGREGATOR_BTN_CLOSE"] = "關閉";
$MESS["DISK_AGGREGATOR_CREATE_STORAGE"] = "添加存儲";
$MESS["DISK_AGGREGATOR_DESCRIPTION"] = "員工可以訪問的所有文檔庫的鏈接：私人文檔；其他用戶的個人文件；文檔頁面；外部網和Intranet組的文檔。";
$MESS["DISK_AGGREGATOR_ND"] = "地圖網絡驅動器";
$MESS["DISK_AGGREGATOR_NETWORK_DRIVE"] = "要訪問所有可用的Bitrix24庫並將其作為文件夾和文件處理，請以網絡驅動器連接此頁面。";
$MESS["DISK_AGGREGATOR_NETWORK_DRIVE_URL_TITLE"] = "使用此地址映射網絡驅動器";
$MESS["DISK_AGGREGATOR_TITLE_NETWORK_DRIVE"] = "網絡驅動器";
$MESS["DISK_AGGREGATOR_TITLE_NETWORK_DRIVE_DESCR_MODAL"] = "使用此地址連接";
