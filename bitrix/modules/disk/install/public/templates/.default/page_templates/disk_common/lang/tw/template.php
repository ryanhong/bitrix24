<?php
$MESS["disk_common_wizard_lib_name"] = "存儲名稱：";
$MESS["disk_common_wizard_lib_name_val"] = "共享驅動器";
$MESS["disk_common_wizard_name"] = "駕駛";
$MESS["disk_common_wizard_name_error1"] = "存儲名稱不能包括以下字符： /\\：*？| {}％＆〜";
$MESS["disk_common_wizard_path_to_folder"] = "文件夾路徑（相對於站點根）";
$MESS["disk_common_wizard_path_to_folder_error1"] = "路徑必須以'/'開頭。";
$MESS["disk_common_wizard_path_to_folder_error2"] = "/ bitrix/不能是存儲文件夾。";
$MESS["disk_common_wizard_settings"] = "創建存儲";
$MESS["disk_common_wizard_title"] = "包含文檔存儲（SEF）的部分";
