<?php
$MESS["BPDRMV_ACCESS_DENIED"] = "除了門戶管理員以外，訪問拒絕了任何人。";
$MESS["BPDRMV_EMPTY_SOURCE_ID"] = "未指定的源對象。";
$MESS["BPDRMV_REMOVE_ERROR"] = "無法刪除對象。";
$MESS["BPDRMV_SOURCE_ERROR"] = "找不到源對象。";
