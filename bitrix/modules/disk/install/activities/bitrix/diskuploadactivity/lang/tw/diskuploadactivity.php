<?php
$MESS["BPDUA_ACCESS_DENIED"] = "除了門戶管理員以外，訪問拒絕了任何人。";
$MESS["BPDUA_EMPTY_ENTITY_ID"] = "未指定存儲。";
$MESS["BPDUA_EMPTY_ENTITY_TYPE"] = "未指定存儲類型。";
$MESS["BPDUA_EMPTY_SOURCE_FILE"] = "未選擇上傳文件。";
$MESS["BPDUA_SOURCE_ERROR"] = "錯誤獲取文件上傳。";
$MESS["BPDUA_TARGET_ERROR"] = "無法解析上傳位置。";
$MESS["BPDUA_UPLOAD_ERROR"] = "文件上傳錯誤。";
