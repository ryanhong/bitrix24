<?php
$MESS["F_ACT_NO_TOPICS"] = "沒有選擇主題";
$MESS["F_ERROR_FORUM_NOT_EXISTS"] = "沒有找到論壇。";
$MESS["F_ERR_SESS_FINISH"] = "您的會議已經過期。請重複操作";
$MESS["F_NO_FPERMS"] = "輸入您的登錄和密碼以訪問此論壇";
$MESS["F_NO_MODULE"] = "論壇模塊未安裝";
$MESS["F_TOPICS_IS_CLOSED"] = "主題已成功關閉。";
$MESS["F_TOPICS_IS_DEL"] = "主題已成功刪除。";
$MESS["F_TOPICS_IS_OPENED"] = "主題已成功開放。";
$MESS["F_TOPICS_IS_PINNED"] = "主題已成功地固定。";
$MESS["F_TOPICS_IS_UNPINNED"] = "主題已經成功地沒有鎖定。";
$MESS["F_TOPIC_IS_DEL"] = "該主題已成功刪除。";
$MESS["F_TOPIC_IS_LOST"] = "沒有發現這個話題";
$MESS["F_TOPIC_LIST"] = "主題列表";
$MESS["F_TOPIC_NOT_APPROVED"] = "主持人批准後，主題將變得可見";
