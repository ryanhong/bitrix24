<?php
$MESS["FMM_ALL"] = "主題名稱和描述";
$MESS["FMM_DESCRIPTION"] = "主題描述";
$MESS["FMM_ON_FORUM"] = "在論壇中";
$MESS["FMM_SEARCH"] = "搜尋";
$MESS["FMM_SEARCH_GO"] = "搜尋";
$MESS["FMM_SEARCH_TITLE"] = "搜索主題";
$MESS["FMM_TITLE"] = "主題名稱";
$MESS["F_ALL_FORUMS"] = "所有論壇";
$MESS["F_FORUM"] = "論壇";
$MESS["F_SEARCH_OBJECT"] = "查找";
