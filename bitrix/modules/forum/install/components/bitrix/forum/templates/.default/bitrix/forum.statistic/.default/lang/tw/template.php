<?php
$MESS["F_ACTIVE_USERS"] = "活躍成員";
$MESS["F_FORUMS_ALL"] = "總論壇";
$MESS["F_NOW_FORUM"] = "在線用戶";
$MESS["F_NOW_ONLINE_1"] = "＃客人＃客人";
$MESS["F_NOW_ONLINE_2"] = "＃用戶＃註冊";
$MESS["F_NOW_ONLINE_3"] = "＃hidden_​​users＃隱藏";
$MESS["F_NOW_TOPIC_READ"] = "用戶瀏覽此主題";
$MESS["F_POSTS_ALL"] = "總帖子";
$MESS["F_REGISTER_USERS"] = "當前註冊用戶";
$MESS["F_TODAY_BIRTHDAY"] = "今天的生日";
$MESS["F_TOPICS_ALL"] = "總主題";
$MESS["F_USER_PROFILE"] = "用戶資料";
