<?php
$MESS["COMM_COMMENT_DELETED"] = "評論已刪除。";
$MESS["COMM_COMMENT_HIDDEN"] = "評論被隱藏了。";
$MESS["COMM_COMMENT_OK"] = "評論成功保存了";
$MESS["COMM_COMMENT_OK_AND_NOT_APPROVED"] = "該評論已成功添加。主持人批准後將顯示。";
$MESS["COMM_COMMENT_SHOWN"] = "評論已被批准。";
$MESS["COMM_COMMENT_UPDATED"] = "評論已更新。";
$MESS["F_ERR_DURING_ACTIONS"] = "收集事件數據時錯誤：";
$MESS["F_ERR_EID_EMPTY"] = "未指定信息塊元素";
$MESS["F_ERR_ENT_EMPTY"] = "未指定實體類型。";
$MESS["F_ERR_ENT_INVALID"] = "實體類型不正確。";
$MESS["F_ERR_FID_EMPTY"] = "評論論壇尚未設置";
$MESS["F_ERR_SESSION_TIME_IS_UP"] = "您的會議已經過期。請重新發布您的消息。";
$MESS["F_NO_MODULE"] = "論壇模塊未安裝";
$MESS["POSTM_CAPTCHA"] = "驗證碼代碼不正確。";
