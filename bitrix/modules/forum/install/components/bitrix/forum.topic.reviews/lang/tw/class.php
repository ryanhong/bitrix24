<?php
$MESS["F_ERR_ADD_MESSAGE"] = "錯誤創建帖子。";
$MESS["F_ERR_ADD_TOPIC"] = "錯誤創建主題。";
$MESS["F_ERR_EID_IS_NOT_EXIST"] = "項目＃element_id＃找不到";
$MESS["F_ERR_NOT_RIGHT_FOR_ADD"] = "添加評論的權限不足。";
$MESS["F_ERR_NO_REVIEW_TEXT"] = "請輸入您的評論。";
$MESS["F_ERR_SESSION_TIME_IS_UP"] = "您的會議已經過期。請重新發布您的消息。";
$MESS["F_FORUM_MESSAGE_CNT"] = "評論";
$MESS["F_FORUM_TOPIC_ID"] = "論壇主題";
$MESS["POSTM_CAPTCHA"] = "驗證碼代碼不正確。";
