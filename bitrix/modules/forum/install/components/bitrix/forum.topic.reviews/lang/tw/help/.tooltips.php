<?php
$MESS["CACHE_TIME_TIP"] = "緩存壽命（秒）";
$MESS["CACHE_TYPE_TIP"] = "緩存類型";
$MESS["DATE_TIME_FORMAT_TIP"] = "日期和時間格式";
$MESS["EDITOR_CODE_DEFAULT_TIP"] = "默認為純文本編輯器模式";
$MESS["ELEMENT_ID_TIP"] = "元素ID";
$MESS["FORUM_ID_TIP"] = "討論論壇ID";
$MESS["IBLOCK_ID_TIP"] = "信息塊ID";
$MESS["IBLOCK_TYPE_TIP"] = "信息塊類型（僅用於驗證）";
$MESS["MESSAGES_PER_PAGE_TIP"] = "每頁消息的數量";
$MESS["PAGE_NAVIGATION_TEMPLATE_TIP"] = "麵包屑導航模板";
$MESS["PREORDER_TIP"] = "首先顯示最早的帖子";
$MESS["SHOW_AVATAR_TIP"] = "顯示化身";
$MESS["SHOW_RATING_TIP"] = "顯示等級";
$MESS["URL_TEMPLATES_DETAIL_TIP"] = "信息塊元素頁面";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = "用戶個人資料頁面";
$MESS["URL_TEMPLATES_READ_TIP"] = "論壇主題查看頁面";
$MESS["USE_CAPTCHA_TIP"] = "使用驗證碼";
