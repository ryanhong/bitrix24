<?php
$MESS["CACHE_TIME_TIP"] = "緩存壽命（秒）";
$MESS["CACHE_TYPE_TIP"] = "緩存類型";
$MESS["DATE_FORMAT_TIP"] = "日期格式";
$MESS["DATE_TIME_FORMAT_TIP"] = "日期和時間格式";
$MESS["FID_RANGE_TIP"] = "論壇ID";
$MESS["SEND_ICQ_TIP"] = "在個人資料中顯示ICQ號碼";
$MESS["SEND_MAIL_TIP"] = "在個人資料中啟用電子郵件發送功能";
$MESS["SET_NAVIGATION_TIP"] = "顯示導航控件";
$MESS["SET_TITLE_TIP"] = "設置頁面標題";
$MESS["SHOW_RATING_TIP"] = "顯示等級";
$MESS["UID_TIP"] = "用戶身份";
$MESS["URL_TEMPLATES_MESSAGE_SEND_TIP"] = "消息創建頁面";
$MESS["URL_TEMPLATES_MESSAGE_TIP"] = "郵政查看頁面";
$MESS["URL_TEMPLATES_PM_EDIT_TIP"] = "個人消息編輯頁面";
$MESS["URL_TEMPLATES_PM_LIST_TIP"] = "個人消息頁面";
$MESS["URL_TEMPLATES_PROFILE_TIP"] = "個人資料編輯頁面";
$MESS["URL_TEMPLATES_READ_TIP"] = "主題查看頁面";
$MESS["URL_TEMPLATES_SUBSCR_LIST_TIP"] = "訂閱頁面";
$MESS["URL_TEMPLATES_USER_LIST_TIP"] = "註冊論壇用戶頁面";
$MESS["URL_TEMPLATES_USER_POST_TIP"] = "用戶消息頁";
$MESS["USER_PROPERTY_TIP"] = "顯示其他屬性";
