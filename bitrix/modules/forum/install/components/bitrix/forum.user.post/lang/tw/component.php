<?php
$MESS["F_ERROR_FORUMS_IS_LOST"] = "沒有找到論壇。";
$MESS["F_ERROR_USER_IS_EMPTY"] = "未指定用戶ID。";
$MESS["F_ERROR_USER_IS_LOST"] = "找不到用戶。";
$MESS["F_NO_MODULE"] = "未安裝論壇模塊。";
$MESS["LU_INCORRECT_FORUM_ID"] = "指定的論壇不正確。";
$MESS["LU_INCORRECT_LAST_MESSAGE_DATE"] = "該過濾器包含不正確的最後一條消息日期";
$MESS["LU_TITLE_ALL"] = "消息";
$MESS["LU_TITLE_LT"] = "成員";
$MESS["LU_TITLE_LTA"] = "作者";
$MESS["LU_TITLE_POSTS"] = "用戶的消息";
