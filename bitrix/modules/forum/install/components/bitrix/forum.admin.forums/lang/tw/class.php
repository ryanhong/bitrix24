<?php
$MESS["FORUM_ERROR_DELETE_PERMISSION"] = "刪除論壇的權限不足[＃id＃]＃名稱＃";
$MESS["FORUM_ERROR_DELETE_UNKNOWN"] = "錯誤刪除論壇[＃id＃]＃名稱＃";
$MESS["FORUM_ERROR_EDIT_PERMISSION"] = "未足夠更新論壇的許可[]";
$MESS["FORUM_ERROR_EDIT_UNKNOWN"] = "錯誤更新論壇[]";
$MESS["FORUM_FILTER_ACTIVE"] = "積極的";
$MESS["FORUM_FILTER_CAN_READ"] = "閱讀訪問[許可> =＃允許＃]";
$MESS["FORUM_FILTER_FORUM_GROUP_ID"] = "論壇小組";
$MESS["FORUM_FILTER_SITE"] = "地點";
$MESS["FORUM_PROCESS_ERRORS"] = "論壇錯誤";
$MESS["FORUM_PROCESS_ERRORS_TITLE"] = "錯誤";
