<?php
$MESS["F_DATE_FORMAT"] = "日期格式";
$MESS["F_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["F_DEFAULT_FID"] = "論壇ID";
$MESS["F_DISPLAY_PANEL"] = "將此組件的按鈕添加到管理工具欄";
$MESS["F_INDEX_TEMPLATE"] = "論壇頁面";
$MESS["F_LIST_TEMPLATE"] = "主題列表頁面";
$MESS["F_MESSAGES_PER_PAGE"] = "每頁消息的數量";
$MESS["F_MESSAGE_TEMPLATE"] = "消息查看頁面";
$MESS["F_PAGEN"] = "麵包屑中的主題查看頁面數量";
$MESS["F_PAGE_NAVIGATION_TEMPLATE"] = "麵包屑導航模板的名稱";
$MESS["F_PAGE_NAVIGATION_WINDOW"] = "頁面導航中的頁數";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "用戶個人資料頁面";
$MESS["F_READ_TEMPLATE"] = "主題閱讀頁面";
$MESS["F_SET_NAVIGATION"] = "顯示麵包屑導航";
$MESS["F_TOPICS_PER_PAGE"] = "每頁主題數";
$MESS["F_URL_TEMPLATES"] = "URL處理";
