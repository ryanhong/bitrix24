<?php
$MESS["F_DATE_FORMAT"] = "日期格式";
$MESS["F_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["F_MESSAGE_SEND_TEMPLATE"] = "消息發布頁面";
$MESS["F_PAGE_NAVIGATION_TEMPLATE"] = "麵包屑導航模板的名稱";
$MESS["F_PM_EDIT_TEMPLATE"] = "私人消息頁面";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "個人資料頁";
$MESS["F_SEND_ICQ"] = "可以從個人資料發送到ICQ";
$MESS["F_SEND_MAIL"] = "可以從個人資料發送電子郵件";
$MESS["F_SET_NAVIGATION"] = "顯示麵包屑導航";
$MESS["F_SHOW_USER_STATUS"] = "顯示用戶狀態";
$MESS["F_URL_TEMPLATES"] = "URL處理";
$MESS["F_USERS_PER_PAGE"] = "每個頁面的用戶數量";
$MESS["F_USER_POST_TEMPLATE"] = "用戶消息頁";
