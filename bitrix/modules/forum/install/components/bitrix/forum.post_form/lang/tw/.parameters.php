<?php
$MESS["F_DEFAULT_FID"] = "論壇ID";
$MESS["F_DEFAULT_MESSAGE_TYPE"] = "編輯表單顯示模式（回复，編輯，新主題）";
$MESS["F_DEFAULT_MID"] = "消息ID";
$MESS["F_DEFAULT_PAGE_NAME"] = "呼叫者組件ID";
$MESS["F_DEFAULT_TID"] = "主題ID";
$MESS["F_EDITOR_CODE_DEFAULT"] = "默認為純文本編輯器模式";
$MESS["F_HELP_TEMPLATE"] = "論壇幫助頁面";
$MESS["F_LIST_TEMPLATE"] = "主題列表頁面";
$MESS["F_MESSAGE_TEMPLATE"] = "消息查看頁面";
$MESS["F_RULES_TEMPLATE"] = "論壇規則頁面";
$MESS["F_SHOW_VOTE"] = "顯示民意測驗";
$MESS["F_VOTE_CHANNEL_ID"] = "民意測驗小組";
$MESS["F_VOTE_GROUP_ID"] = "允許創建民意調查的用戶組";
$MESS["F_VOTE_SETTINGS"] = "民意調查設置";
