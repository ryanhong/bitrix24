<?php
$MESS["F_DEFAULT_UID"] = "用戶身份";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "個人資料查看頁面";
$MESS["F_SEND_ICQ"] = "可以從個人資料發送到ICQ";
$MESS["F_SEND_MAIL"] = "可以從個人資料發送電子郵件";
$MESS["F_SET_NAVIGATION"] = "顯示麵包屑導航";
$MESS["F_URL_TEMPLATES"] = "URL處理";
