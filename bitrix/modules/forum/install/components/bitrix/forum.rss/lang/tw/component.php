<?php
$MESS["FILE_DOWNLOAD"] = "下載文件";
$MESS["F_EMPTY_FORUMS"] = "沒有找到論壇。";
$MESS["F_EMPTY_TOPIC"] = "沒有找到主題。";
$MESS["F_EMPTY_TOPIC_ID"] = "主題未指定。";
$MESS["F_EMPTY_TYPE"] = "未指定RSS的類型";
$MESS["F_ERR_BAD_FORUM"] = "該論壇無法通過RSS獲得。";
$MESS["F_NO_MODULE"] = "論壇模塊未安裝";
$MESS["F_TEMPLATES_DESCRIPTION_FORUM"] = "＃forum_title＃at＃site_name＃[＃server_name＃]的新論壇主題";
$MESS["F_TEMPLATES_DESCRIPTION_FORUMS"] = "＃site_name＃[＃server_name＃]的新論壇主題";
$MESS["F_TEMPLATES_DESCRIPTION_TOPIC"] = "＃topic_title＃of＃forum_title＃論壇＃site_name＃[＃server_name＃]中的新帖子";
$MESS["F_TEMPLATES_TITLE_FORUM"] = "＃site_name＃[論壇：＃forum_title＃]";
$MESS["F_TEMPLATES_TITLE_FORUMS"] = "＃site_name＃[論壇]";
$MESS["F_TEMPLATES_TITLE_TOPIC"] = "＃site_name＃[主題：＃topic_title＃]";
