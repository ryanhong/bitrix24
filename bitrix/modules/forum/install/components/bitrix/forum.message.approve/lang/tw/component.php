<?php
$MESS["F_AUTH"] = "您需要被授權查看此頁面";
$MESS["F_ERRROR_FORUM_EMPTY"] = "論壇ID未指定。";
$MESS["F_ERRROR_FORUM_NOT_FOUND"] = "沒有找到論壇。";
$MESS["F_NO_ACTION"] = "沒有選擇動作。";
$MESS["F_NO_MESSAGE"] = "沒有選擇消息";
$MESS["F_NO_MODULE"] = "論壇模塊未安裝";
$MESS["F_NO_PERMS"] = "您沒有足夠的權限";
$MESS["F_TITLE"] = "未批准的消息";
$MESS["F_TITLE_NAV"] = "消息";
