<?php
$MESS["FM_AUTH"] = "您需要被授權查看此頁面。";
$MESS["FM_EMPTY_DEST_FORUM"] = "未知目的地論壇";
$MESS["FM_NO_DEST_FPERMS"] = "您沒有足夠的權限來移動這個話題";
$MESS["FM_NO_FPERMS"] = "您沒有足夠的權限來移動這個話題";
$MESS["FM_TITLE"] = "將主題移至另一個論壇";
$MESS["F_ERROR_FORUM_IS_LOST"] = "沒有找到論壇。";
$MESS["F_ERROR_TOPICS_IS_EMPTY"] = "沒有選擇搬到另一個論壇的話題。";
$MESS["F_NO_MODULE"] = "論壇模塊未安裝";
