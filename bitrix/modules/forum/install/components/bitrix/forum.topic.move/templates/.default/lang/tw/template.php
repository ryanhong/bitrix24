<?php
$MESS["FL_TITLE"] = "移動主題";
$MESS["FM_LEAVE_LINK"] = "在舊論壇中保持指向移動主題的鏈接";
$MESS["FM_MOVE_TOPIC"] = "移動主題";
$MESS["F_EMPTY_TOPIC_LIST"] = "您選擇要移動的主題不存在，也不是另一個論壇的一部分。";
$MESS["F_IN"] = "到論壇";
