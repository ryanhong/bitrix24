<?php
$MESS["F_ERROR_MID_IS_LOST"] = "沒有找到或刪除消息。";
$MESS["F_ERROR_TID_IS_LOST"] = "沒有找到主題。";
$MESS["F_ERROR_TID_MOVED"] = "主題已經移動。";
$MESS["F_ERROR_TID_NOT_APPROVED"] = "主持人批准後，該主題將可用。";
$MESS["F_FPERMS"] = "輸入您登錄和密碼以查看此主題。";
$MESS["F_MESS_SUCCESS_ADD"] = "這些帖子已成功添加。";
$MESS["F_MESS_SUCCESS_ADD_MODERATE"] = "主持人驗證後，您的帖子將可見。";
$MESS["F_MESS_SUCCESS_DEL"] = "這些帖子已成功刪除。";
$MESS["F_MESS_SUCCESS_HIDE"] = "這些帖子已成功地隱藏了。";
$MESS["F_MESS_SUCCESS_SHOW"] = "這些帖子已成功顯示。";
$MESS["F_NO_MODULE"] = "論壇模塊未安裝。";
$MESS["F_TITLE_NAV"] = "消息";
$MESS["F_TOPIC_SUCCESS_CLOSE"] = "封閉的話題";
$MESS["F_TOPIC_SUCCESS_OPEN"] = "公開主題";
$MESS["F_TOPIC_SUCCESS_ORD"] = "未損壞";
$MESS["F_TOPIC_SUCCESS_TOP"] = "固定";
