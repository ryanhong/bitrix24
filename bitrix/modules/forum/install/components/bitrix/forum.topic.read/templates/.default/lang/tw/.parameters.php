<?php
$MESS["F_ATTACH_MODE"] = "顯示附帶的圖像為（消息下方）";
$MESS["F_ATTACH_MODE_NAME"] = "姓名";
$MESS["F_ATTACH_MODE_THUMB"] = "縮略圖";
$MESS["F_ATTACH_SIZE"] = "圖像縮略圖尺寸（像素）";
$MESS["F_HIDE_USER_ACTION"] = "組用戶操作";
$MESS["F_SEND_MAIL"] = "可以從個人資料發送電子郵件";
$MESS["F_SEO_USER"] = "不要索引配置文件鏈接";
$MESS["F_SHOW_NAME_LINK"] = "顯示“名稱”鏈接";
$MESS["F_SHOW_RSS"] = "顯示RSS鏈接";
$MESS["F_SHOW_VOTE"] = "顯示民意測驗";
$MESS["F_VOTE_TEMPLATE"] = "民意測驗模板";
