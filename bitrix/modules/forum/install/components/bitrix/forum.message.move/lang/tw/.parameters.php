<?php
$MESS["F_DATE_FORMAT"] = "日期格式";
$MESS["F_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["F_DEFAULT_FID"] = "論壇ID";
$MESS["F_DEFAULT_MID"] = "消息ID";
$MESS["F_DEFAULT_TID"] = "主題ID";
$MESS["F_IMAGE_SIZE"] = "附件的大小（PX）";
$MESS["F_INDEX_TEMPLATE"] = "論壇頁";
$MESS["F_LIST_TEMPLATE"] = "主題列表頁面";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "個人資料頁";
$MESS["F_READ_TEMPLATE"] = "主題閱讀頁面";
$MESS["F_SET_NAVIGATION"] = "使用麵包屑導航";
$MESS["F_TOPIC_SEARCH_TEMPLATE"] = "論壇主題搜索頁面";
$MESS["F_URL_TEMPLATES"] = "URL處理";
