<?php
$MESS["FM_AUTH"] = "您需要被授權查看此頁面。";
$MESS["FM_ERR_NO_DATA"] = "主題標題";
$MESS["FM_NO_FPERMS"] = "您沒有足夠的權限來調節這個論壇";
$MESS["FM_NO_ICON"] = "沒有圖標";
$MESS["FM_NO_MODULE"] = "論壇模塊未安裝";
$MESS["FM_TITLE_PAGE"] = "消息移動";
$MESS["F_ERROR_MESSAGES_NOT_FOUND"] = "找不到消息。";
$MESS["F_ERROR_TOPIC_NOT_FOUND"] = "沒有找到主題。";
$MESS["F_ERR_SESS_FINISH"] = "您的會議已經過期。請重複操作。";
