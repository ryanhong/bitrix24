<?php
$MESS["F_CACHE_TIME_USER_STAT"] = "論壇用戶緩存的壽命";
$MESS["F_DEFAULT_FID"] = "論壇ID";
$MESS["F_DEFAULT_TID"] = "主題ID";
$MESS["F_DEFAULT_TITLE_SEO"] = "主題SEO ID";
$MESS["F_FORUM_ID"] = "論壇";
$MESS["F_PERIOD"] = "在過去（最小）中顯示用戶統計信息";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "個人資料頁";
$MESS["F_SHOW"] = "在統計中顯示";
$MESS["F_SHOW_BIRTHDAY"] = "生日";
$MESS["F_SHOW_FORUM_ANOTHER_SITE"] = "顯示其他內部站點的論壇";
$MESS["F_SHOW_STATISTIC"] = "論壇統計";
$MESS["F_SHOW_USERS_ONLINE"] = "在線用戶";
$MESS["F_URL_TEMPLATES"] = "URL處理";
