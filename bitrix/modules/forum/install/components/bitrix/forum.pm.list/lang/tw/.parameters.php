<?php
$MESS["F_DATE_FORMAT"] = "日期格式";
$MESS["F_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["F_DISPLAY_PANEL"] = "顯示此組件的面板按鈕";
$MESS["F_NAME_TEMPLATE"] = "名稱格式";
$MESS["F_PAGE_NAVIGATION_TEMPLATE"] = "Pager模板的名稱";
$MESS["F_PM_FOLDER"] = "私人消息文件夾管理頁面";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "個人資料頁";
$MESS["F_SET_NAVIGATION"] = "顯示麵包屑導航";
$MESS["F_URL_TEMPLATES"] = "URL處理";
$MESS["PM_DEFAULT_FID"] = "文件夾ID";
$MESS["PM_EDIT_TEMPLATE"] = "私人消息編輯（創建）頁面";
$MESS["PM_LIST_TEMPLATE"] = "私人消息列表頁面";
$MESS["PM_PER_PAGE"] = "每頁消息的數量";
$MESS["PM_READ_TEMPLATE"] = "私人消息閱讀頁面";
