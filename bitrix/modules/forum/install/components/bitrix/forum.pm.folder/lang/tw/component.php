<?php
$MESS["F_ERR_ACTION"] = "未知的命令。";
$MESS["F_ERR_SESS_FINISH"] = "您的會議已經過期。請重複操作。";
$MESS["F_ERR_SYSTEM_FOLDERS"] = "您無法刪除系統文件夾。";
$MESS["F_NO_PM"] = "PM禁用。";
$MESS["PM_AUTH"] = "您需要被授權查看此頁面。";
$MESS["PM_NOT_DELETE"] = "無法刪除文件夾。";
$MESS["PM_NOT_FOLDER"] = "該文件夾不存在。";
$MESS["PM_NOT_FOLDER_TITLE"] = "該文件夾未指定。";
$MESS["PM_NOT_RIGHT"] = "權限不足。";
$MESS["PM_NO_MODULE"] = "論壇模塊未安裝。";
$MESS["PM_PM"] = "私人信息";
$MESS["PM_SUCC_CREATE"] = "該文件夾已成功創建。";
$MESS["PM_SUCC_DELETE"] = "該文件夾已成功刪除。";
$MESS["PM_SUCC_REMOVE"] = "該文件夾已成功清除。";
$MESS["PM_SUCC_SAVED"] = "更改已保存。";
$MESS["PM_TITLE_EDIT"] = "修改“＃標題＃＆raquo;文件夾";
$MESS["PM_TITLE_NEW"] = "新建文件夾";
