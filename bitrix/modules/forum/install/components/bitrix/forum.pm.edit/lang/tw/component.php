<?php
$MESS["F_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["F_ERR_SESS_FINISH"] = "您的會議已經過期。請重複操作。";
$MESS["F_MESSAGE_NOT_FOUND"] = "沒有找到個人信息。";
$MESS["F_NO_MODULE"] = "論壇模塊未安裝。";
$MESS["F_NO_PM"] = "PM禁用。";
$MESS["PM_AUTH"] = "您需要被授權查看此頁面";
$MESS["PM_FOLDER_ID_0"] = "收件箱";
$MESS["PM_FOLDER_ID_1"] = "收件箱";
$MESS["PM_FOLDER_ID_2"] = "向外";
$MESS["PM_FOLDER_ID_3"] = "輸出箱";
$MESS["PM_FOLDER_ID_4"] = "回收";
$MESS["PM_NOT_RIGHT"] = "權限不足。";
$MESS["PM_REPLY"] = "關於：";
$MESS["PM_TITLE_EDIT"] = "＆laquo;＃標題＃＆raquo; （編輯）";
$MESS["PM_TITLE_NAV"] = "私人信息";
$MESS["PM_TITLE_NEW"] = "新的";
$MESS["PM_USER_NOT_FOUND"] = "一個名為##＆raquo; laquo;沒找到。";
