<?php
$MESS["F_DEFAULT_FID"] = "用戶文件夾ID";
$MESS["F_DEFAULT_MID"] = "消息ID";
$MESS["F_DEFAULT_MODE"] = "消息類型";
$MESS["F_DEFAULT_UID"] = "用戶身份";
$MESS["F_EDITOR_CODE_DEFAULT"] = "默認為純文本編輯器模式";
$MESS["F_NAME_TEMPLATE"] = "名稱格式";
$MESS["F_PM_EDIT_TEMPLATE"] = "私人消息編輯（創建）頁面";
$MESS["F_PM_LIST_TEMPLATE"] = "私人消息列表頁面";
$MESS["F_PM_READ_TEMPLATE"] = "私人消息閱讀頁面";
$MESS["F_PM_SEARCH_TEMPLATE"] = "用戶搜索頁面";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "個人資料頁";
$MESS["F_SET_NAVIGATION"] = "顯示麵包屑導航";
$MESS["F_URL_TEMPLATES"] = "URL處理";
