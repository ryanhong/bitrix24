<?php
$MESS["forum_ALLOW_POST"] = "允許發布：";
$MESS["forum_AVATAR"] = "阿凡達：";
$MESS["forum_DESCRIPTION"] = "描述：";
$MESS["forum_HIDE_FROM_ONLINE"] = "向在線用戶隱藏：";
$MESS["forum_INFO"] = "論壇個人資料";
$MESS["forum_INTERESTS"] = "利益：";
$MESS["forum_SHOW_NAME"] = "顯示名稱：";
$MESS["forum_SIGNATURE"] = "簽名：";
$MESS["forum_SUBSC_GET_MY_MESSAGE"] = "訂戶可以收到我的帖子：";
$MESS["forum_TAB"] = "論壇個人資料";
$MESS["forum_TAB_TITLE"] = "論壇用戶個人資料";
