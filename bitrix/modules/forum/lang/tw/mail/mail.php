<?php
$MESS["FORUM_MAIL_EMPTY_TOPIC_TITLE"] = "（無題）";
$MESS["FORUM_MAIL_ERROR1"] = "沒有找到發件人。";
$MESS["FORUM_MAIL_ERROR2"] = "用戶無法訪問論壇。";
$MESS["FORUM_MAIL_ERROR3"] = "用戶無權創建新主題。";
$MESS["FORUM_MAIL_ERROR4"] = "錯誤創建一個新主題。";
$MESS["FORUM_MAIL_ERROR5"] = "錯誤創建新消息。";
$MESS["FORUM_MAIL_NAME"] = "將消息添加到社交網絡論壇";
$MESS["FORUM_MAIL_OK"] = "添加了一條新消息。";
$MESS["FORUM_MAIL_SOCNET_TITLE_MESSAGE"] = "＃fure_name＃在＃標題＃中創建了一個帖子。";
$MESS["FORUM_MAIL_SOCNET_TITLE_TOPIC"] = "＃fure_name＃在＃標題＃中創建了一個主題。";
$MESS["SONET_FORUM_LOG_TOPIC_TEMPLATE"] = "＃fure_name＃在＃標題＃中創建了一個主題。";
