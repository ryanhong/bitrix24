<?php
$MESS["FORUM_PP_DEL_CONF"] = "您確定要刪除此唱片嗎？";
$MESS["FORUM_PP_DEL_DESCR"] = "刪除";
$MESS["FORUM_PP_EDIT_DESCR"] = "修改記錄";
$MESS["FORUM_PP_ERROR_DELETE"] = "錯誤刪除記錄";
$MESS["FORUM_PP_MIN_MES"] = "最小消息數";
$MESS["FORUM_PP_POINTS"] = "每條消息點";
$MESS["FORUM_PP_POINTS_PER_MES"] = "每條消息點";
$MESS["FORUM_PP_TITLE"] = "每條消息點";
$MESS["FP2PAN_ADD_NEW"] = "新紀錄";
$MESS["FP2PAN_ADD_NEW_ALT"] = "單擊以添加新記錄";
$MESS["FP2PAN_UPDATE_ERROR"] = "錯誤更新記錄";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "檢查：";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "刪除";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "選擇：";
