<?php
$MESS["FM_TITLE_AUTHOR"] = "作者";
$MESS["FM_TITLE_DATE_CREATE"] = "創建";
$MESS["FM_TITLE_DATE_LAST_POST"] = "修改的";
$MESS["FM_TITLE_FORUM"] = "論壇";
$MESS["FM_TITLE_LAST_MESSAGE"] = "最後一條消息";
$MESS["FM_TITLE_MESSAGES"] = "消息";
$MESS["FM_TITLE_NAME"] = "姓名";
$MESS["FM_TITLE_VIEWS"] = "視圖";
$MESS["FM_TOPICS"] = "消息";
$MESS["FM_WRONG_DATE_CREATE_FROM"] = "請輸入創建日期範圍的正確下限";
$MESS["FORUM_TOPICS"] = "管理主題";
