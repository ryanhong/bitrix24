<?php
$MESS["FLT_ACT_ADD"] = "添加";
$MESS["FLT_ACT_DEL"] = "刪除";
$MESS["FLT_ACT_DEL_CONFIRM"] = "字典及其內容將被刪除！你確定你要刪除嗎？";
$MESS["FLT_ACT_EDIT"] = "編輯";
$MESS["FLT_ACT_GEN"] = "生成模板";
$MESS["FLT_ACT_GEN_CONFIRM"] = "＆nbsp; for＆nbsp;";
$MESS["FLT_HEAD_TITLE"] = "姓名";
$MESS["FLT_NOT_UPDATE"] = "更新失敗";
$MESS["FLT_TITLE"] = "字典";
$MESS["FLT_TITLE_"] = "字典";
$MESS["FLT_TITLE_T"] = "音譯詞典";
$MESS["FLT_TITLE_W"] = "褻瀆詞典";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "檢查：";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "選擇：";
