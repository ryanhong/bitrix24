<?php
$MESS["FLTR_HEAD_TITLE"] = "姓名";
$MESS["FLTR_HEAD_TYPE"] = "字典類型";
$MESS["FLTR_HEAD_TYPE_T"] = "音譯詞典";
$MESS["FLTR_HEAD_TYPE_W"] = "單詞詞典";
$MESS["FLTR_IS_NOT_ADD"] = "更新失敗。";
$MESS["FLTR_IS_NOT_UPDATE"] = "更新失敗。";
$MESS["FLTR_LIST"] = "列表";
$MESS["FLTR_NEW"] = "新詞典";
$MESS["FLTR_UPDATE"] = "更新字典";
