<?php
$MESS["ERROR_ADD_GROUP"] = "錯誤創建組";
$MESS["ERROR_ADD_GROUP_BAD_FIELDS"] = "錯誤更新組參數。一些選項無效。";
$MESS["ERROR_EDIT_GROUP"] = "錯誤更新組";
$MESS["FGN_2FLIST"] = "論壇組";
$MESS["FGN_COPY_GROUP"] = "複製組";
$MESS["FGN_DELETE_GROUP"] = "刪除組";
$MESS["FGN_DELETE_GROUP_CONFIRM"] = "您確定要刪除這個組嗎？如果小組包含一個或多個論壇，則不會刪除該小組。";
$MESS["FGN_NEW_GROUP"] = "新小組";
$MESS["FGN_TAB_GROUP"] = "一組論壇";
$MESS["FGN_TAB_GROUP_DESCR"] = "一組論壇";
$MESS["FORUM_CODE"] = "ID";
$MESS["FORUM_DESCR"] = "描述";
$MESS["FORUM_EDIT_RECORD"] = "編輯組N＃ID＃";
$MESS["FORUM_NAME"] = "姓名";
$MESS["FORUM_NEW_RECORD"] = "新小組";
$MESS["FORUM_PARENT_ID"] = "父組";
$MESS["FORUM_SORT"] = "排序";
