<?php
$MESS["FLTR_DEL"] = "刪除";
$MESS["FLTR_DEL_CONFIRM"] = "您確定要刪除記錄嗎？";
$MESS["FLTR_DICTIONARY"] = "字典";
$MESS["FLTR_EDIT"] = "編輯記錄##";
$MESS["FLTR_LETTER"] = "信";
$MESS["FLTR_LIST"] = "列表";
$MESS["FLTR_NEW"] = "新紀錄";
$MESS["FLTR_NOT_SAVE"] = "更新失敗。";
$MESS["FLTR_REPLACEMENT"] = "代替";
$MESS["FLTR_TITLE"] = "音譯詞典";
