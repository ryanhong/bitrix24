<?php
$MESS["FORUM_PPE_EDDOR_UPDATE"] = "更新記錄時錯誤";
$MESS["FORUM_PPE_ERROR_ADD"] = "添加記錄時錯誤";
$MESS["FORUM_PPE_MIN_MES"] = "必需的帖子";
$MESS["FORUM_PPE_NOTES"] = "在記錄更改時，用戶的等級不是重新計算的。用戶的排名將在以下事件上重新計算：用戶發布消息，給用戶進行投票，等等。";
$MESS["FORUM_PPE_PPM"] = "每條消息點";
$MESS["FORUM_PPE_TITLE_ADD"] = "添加新記錄";
$MESS["FORUM_PPE_TITLE_UPD"] = "修改記錄## ID＃";
$MESS["FPPN_2FLIST"] = "記錄";
$MESS["FPPN_DELETE_POINT"] = "刪除記錄";
$MESS["FPPN_DELETE_POINT_CONFIRM"] = "您確定要刪除此唱片嗎？";
$MESS["FPPN_NEW_POINT"] = "新紀錄";
$MESS["FPPN_TAB_POINT"] = "帖子點";
$MESS["FPPN_TAB_POINT_DESCR"] = "帖子點";
