<?php
$MESS["FORUM_PE_ERROR_ADD"] = "添加分數的錯誤";
$MESS["FORUM_PE_ERROR_UPDATE"] = "更新分數時錯誤";
$MESS["FORUM_PE_MIN_POINTS"] = "給出得分的最低點數";
$MESS["FORUM_PE_MNEMOCODE"] = "瘋子代碼";
$MESS["FORUM_PE_NAME"] = "姓名";
$MESS["FORUM_PE_RATING_VALUE"] = "獲得等級所需的授權";
$MESS["FORUM_PE_RATING_VOTES"] = "獲得等級所需的歸一票";
$MESS["FORUM_PE_TITLE_ADD"] = "添加新分數";
$MESS["FORUM_PE_TITLE_UPDATE"] = "修改分數## ID＃";
$MESS["FORUM_PE_VOTES"] = "票數";
$MESS["FPN_2FLIST"] = "排名";
$MESS["FPN_DELETE_POINT"] = "刪除等級";
$MESS["FPN_DELETE_POINT_CONFIRM"] = "您確定要刪除此等級嗎？";
$MESS["FPN_NEW_POINT"] = "新等級";
$MESS["FPN_TAB_POINT"] = "論壇等級";
$MESS["FPN_TAB_POINT_DESCR"] = "論壇等級";
