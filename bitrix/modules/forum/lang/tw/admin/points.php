<?php
$MESS["FORUM_P_DELETE_CONF"] = "您確定要刪除此分數嗎？";
$MESS["FORUM_P_DELETE_DESC"] = "刪除";
$MESS["FORUM_P_EDIT_DESC"] = "修改分數參數";
$MESS["FORUM_P_ERROR_DEL"] = "錯誤刪除分數";
$MESS["FORUM_P_MIN_POINTS"] = "最低點數";
$MESS["FORUM_P_NAME"] = "姓名";
$MESS["FORUM_P_POINTS"] = "點";
$MESS["FORUM_P_RANKS"] = "分數";
$MESS["FORUM_P_RATING_VALUE"] = "要求的權威";
$MESS["FORUM_P_RATING_VOTES"] = "需要正常的選票";
$MESS["FORUM_P_VOTES"] = "投票";
$MESS["FPAN_ADD_NEW"] = "新等級";
$MESS["FPAN_ADD_NEW_ALT"] = "單擊以添加新等級";
$MESS["FPAN_UPDATE_ERROR"] = "錯誤更新記錄";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "檢查：";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "刪除";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "選擇：";
