<?php
$MESS["FLTR_DEL"] = "刪除";
$MESS["FLTR_DEL_CONFIRM"] = "您確定要刪除記錄嗎？";
$MESS["FLTR_DESCRIPTION"] = "描述";
$MESS["FLTR_EDIT"] = "編輯＃ID＃";
$MESS["FLTR_LIST"] = "列表";
$MESS["FLTR_NEW"] = "添加";
$MESS["FLTR_NOT_ACTION"] = "沒有選擇動作。";
$MESS["FLTR_NOT_SAVE"] = "更新失敗。";
$MESS["FLTR_NOT_WORDS"] = "未指定搜索詞。";
$MESS["FLTR_REPLACEMENT"] = "代替";
$MESS["FLTR_SEARCH"] = "搜尋";
$MESS["FLTR_SEARCH_0"] = "單詞（無音典）";
$MESS["FLTR_SEARCH_1"] = "音譯（使用音譯字典）";
$MESS["FLTR_SEARCH_2"] = "正則表達式";
$MESS["FLTR_SEARCH_WHAT"] = "模式創建模式";
$MESS["FLTR_USE_IT"] = "積極的";
