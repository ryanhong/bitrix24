<?php
$MESS["ERROR_DEL_GROUP"] = "錯誤刪除組。";
$MESS["FGAN_ADD_NEW"] = "新論壇小組";
$MESS["FGAN_ADD_NEW_ALT"] = "點擊添加一個新的論壇";
$MESS["FORUM_DELETE_DESCR"] = "刪除組";
$MESS["FORUM_EDIT_DESCR"] = "修改組設置";
$MESS["FORUM_NAME"] = "姓名";
$MESS["GROUP_DEL_CONF"] = "您確定要刪除這個組嗎？如果小組中有一些論壇，則該組將不會被刪除。";
$MESS["GROUP_ID"] = "ID";
$MESS["GROUP_NAV"] = "組";
$MESS["GROUP_SORT"] = "種類";
$MESS["GROUP_TITLE"] = "論壇組";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "檢查：";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "刪除";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "選擇：";
