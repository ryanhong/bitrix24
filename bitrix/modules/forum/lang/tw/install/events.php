<?php
$MESS["FORUM_NEW_MESSAGE_MAIL_DESC"] = "＃forum_name＃ - 論壇名稱
＃作者＃ - 消息作者
＃From_email＃ - ＆amp;從＆amp;發送電子郵件場地
＃收件人＃ - 消息收件人
＃主題_title＃ - 消息主題
＃Message_Text＃ - 消息文本
＃path2forum＃ - 消息URL
＃message_date＃ - 消息日期
＃forum_email＃ - 電子郵件將消息添加到論壇
＃forum_id＃ - 論壇ID
＃主題_id＃ - 主題ID
＃Message_id＃ - 消息ID
＃TAPPRADED＃ - 主題批准和發布
＃MAPPRADED＃ - 批准並發布消息
";
$MESS["FORUM_NEW_MESSAGE_MAIL_MESSAGE"] = "＃message_text＃

-------------------------------------------------- --------
您正在收到此消息，因為您已訂閱了名為＃forum_name＃的論壇

您可以通過電子郵件回复此消息，也可以在http：//＃server_name ## path2forum＃使用該表單。

創建新消息：＃forum_email＃

消息作者：＃作者＃

此消息已由＃site_name＃自動創建。";
$MESS["FORUM_NEW_MESSAGE_MAIL_NAME"] = "論壇上的新消息（電子郵件消息模式）";
$MESS["F_EDITM"] = "更改論壇消息";
$MESS["F_EDITM_TEXT"] = "來自＃site_name＃的信息消息
-------------------------------------------------- --------

消息在論壇上的＃server_name＃進行了修改。

主題標題：
＃主題標題＃

消息作者：＃作者＃
消息日期：＃message_date＃
消息文字：

＃message_text＃

消息地址：
http：//＃server_name ## path2forum＃

自動生成的消息。
";
$MESS["F_FORUM_ID"] = "論壇ID";
$MESS["F_FORUM_NAME"] = "論壇名稱";
$MESS["F_MAIL_FROM_EMAIL"] = "從電子郵件字段發送電子郵件";
$MESS["F_MAIL_MAPPROVED"] = "消息已批准";
$MESS["F_MAIL_PATH2FORUM"] = "消息URL";
$MESS["F_MAIL_RECIPIENT"] = "電子郵件收件人";
$MESS["F_MAIL_TAPPROVED"] = "消息主題已批准";
$MESS["F_MAIL_TEXT"] = "來自＃site_name＃的信息消息
-------------------------------------------------- --------

在論壇上的新消息＃server_name＃。

主題標題：
＃主題標題＃

消息作者：＃作者＃
消息日期：＃message_date＃
消息文字：

＃message_text＃

消息地址：
http：//＃server_name ## path2forum＃

自動生成的消息。
";
$MESS["F_MESSAGE_AUTHOR"] = "消息作者";
$MESS["F_MESSAGE_DATE"] = "消息日期";
$MESS["F_MESSAGE_ID"] = "消息ID";
$MESS["F_MESSAGE_TEXT"] = "消息文字";
$MESS["F_NEW_MESSAGE_ON_FORUM"] = "新的論壇消息";
$MESS["F_PRIV"] = "論壇用戶的私人消息";
$MESS["F_PRIVATE"] = "論壇用戶的私人消息";
$MESS["F_PRIVATE_AUTHOR"] = "發件人的名稱";
$MESS["F_PRIVATE_AUTHOR_EMAIL"] = "發件人的電子郵件";
$MESS["F_PRIVATE_AUTHOR_ID"] = "發件人的ID";
$MESS["F_PRIVATE_MESSAGE"] = "訊息";
$MESS["F_PRIVATE_MESSAGE_DATE"] = "日期";
$MESS["F_PRIVATE_MESSAGE_LINK"] = "鏈接到消息";
$MESS["F_PRIVATE_RECIPIENT_EMAIL"] = "收件人的電子郵件";
$MESS["F_PRIVATE_RECIPIENT_ID"] = "接收者的ID";
$MESS["F_PRIVATE_RECIPIENT_NAME"] = "收件人姓名";
$MESS["F_PRIVATE_SUBJECT"] = "話題";
$MESS["F_PRIVATE_TEXT"] = "來自＃site_name＃的信息消息
-------------------------------------------------- --------

您在論壇上的私人消息＃server_name＃。

主題：＃主題＃

發件人：＃from_name＃（＃from_email＃）
消息日期：＃message_date＃
訊息:

#訊息#

＃message_link＃自動生成的消息。
";
$MESS["F_PRIV_AUTHOR"] = "發件人的名稱";
$MESS["F_PRIV_AUTHOR_EMAIL"] = "發件人的電子郵件";
$MESS["F_PRIV_DATE"] = "日期";
$MESS["F_PRIV_MAIL"] = "來自＃site_name＃的信息消息
-------------------------------------------------- --------

您在論壇上的私人消息＃server_name＃。

主題：＃主題＃

發件人：＃from_name＃
消息日期：＃message_date＃
訊息:

#訊息#

自動生成的消息。
";
$MESS["F_PRIV_RECIPIENT_EMAIL"] = "收件人的電子郵件";
$MESS["F_PRIV_RECIPIENT_NAME"] = "收件人姓名";
$MESS["F_PRIV_TEXT"] = "訊息";
$MESS["F_PRIV_TITLE"] = "話題";
$MESS["F_TOPIC_ID"] = "主題ID";
$MESS["F_TOPIC_TITLE"] = "主題標題";
