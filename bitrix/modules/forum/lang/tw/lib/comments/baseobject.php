<?php
$MESS["FORUM_CM_FORUM_IS_LOST"] = "找不到指定的論壇。";
$MESS["FORUM_CM_FORUM_IS_WRONG"] = "指定的論壇未獲得批准。";
$MESS["FORUM_CM_TOPIC_IS_NOT_CREATED"] = "評論的主題不存在。";
$MESS["FORUM_CM_WRONG_ENTITY"] = "不正確的實體格式。";
$MESS["FORUM_USER_SYSTEM"] = "系統";
