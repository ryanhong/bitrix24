<?php
$MESS["ACCESS_DENIED"] = "拒絕訪問。";
$MESS["FORUM_CM_ERR_COMMENT_IS_LOST1"] = "沒有發現要修改的評論。";
$MESS["FORUM_CM_ERR_COMMENT_IS_LOST2"] = "找不到要刪除的評論。";
$MESS["FORUM_CM_ERR_COMMENT_IS_LOST3"] = "沒有發現要調節的評論。";
$MESS["FORUM_CM_ERR_COMMENT_IS_LOST4"] = "找不到要檢查權限的評論。";
$MESS["FORUM_CM_ERR_DELETE"] = "錯誤刪除評論。";
$MESS["FORUM_CM_ERR_EMPTY_AUTHORS_NAME"] = "未指定作者名稱。";
$MESS["FORUM_CM_ERR_EMPTY_TEXT"] = "評論文本未指定。";
$MESS["FORUM_CM_ERR_MODERATE"] = "錯誤調節評論。";
$MESS["FORUM_CM_ERR_TYPE_INCORRECT"] = "不正確的評論類型。";
