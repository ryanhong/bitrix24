<?php
$MESS["FORUM_ALL"] = "每個人";
$MESS["FORUM_ALL_WITH_CAPTCHA"] = "每個人 +驗證碼用於非授權用戶";
$MESS["FORUM_AUTHORIZED_USERS"] = "授權用戶";
$MESS["FORUM_BOTTOM_PAGER"] = "在列表的底部";
$MESS["FORUM_DAY_OF_WEEK_0"] = "星期日";
$MESS["FORUM_DAY_OF_WEEK_1"] = "週一";
$MESS["FORUM_DAY_OF_WEEK_2"] = "週二";
$MESS["FORUM_DAY_OF_WEEK_3"] = "週三";
$MESS["FORUM_DAY_OF_WEEK_4"] = "週四";
$MESS["FORUM_DAY_OF_WEEK_5"] = "星期五";
$MESS["FORUM_DAY_OF_WEEK_6"] = "週六";
$MESS["FORUM_DOW_0"] = "太陽";
$MESS["FORUM_DOW_1"] = "週一";
$MESS["FORUM_DOW_2"] = "星期二";
$MESS["FORUM_DOW_3"] = "星期三";
$MESS["FORUM_DOW_4"] = "星期四";
$MESS["FORUM_DOW_5"] = "星期五";
$MESS["FORUM_DOW_6"] = "坐著";
$MESS["FORUM_MONTH_1"] = "一月";
$MESS["FORUM_MONTH_2"] = "二月";
$MESS["FORUM_MONTH_3"] = "行進";
$MESS["FORUM_MONTH_4"] = "四月";
$MESS["FORUM_MONTH_5"] = "可能";
$MESS["FORUM_MONTH_6"] = "六月";
$MESS["FORUM_MONTH_7"] = "七月";
$MESS["FORUM_MONTH_8"] = "八月";
$MESS["FORUM_MONTH_9"] = "九月";
$MESS["FORUM_MONTH_10"] = "十月";
$MESS["FORUM_MONTH_11"] = "十一月";
$MESS["FORUM_MONTH_12"] = "十二月";
$MESS["FORUM_MON_1"] = "揚";
$MESS["FORUM_MON_2"] = "2月";
$MESS["FORUM_MON_3"] = "3月";
$MESS["FORUM_MON_4"] = "4月";
$MESS["FORUM_MON_5"] = "可能";
$MESS["FORUM_MON_6"] = "六月";
$MESS["FORUM_MON_7"] = "七月";
$MESS["FORUM_MON_8"] = "八月";
$MESS["FORUM_MON_9"] = "九月";
$MESS["FORUM_MON_10"] = "十月";
$MESS["FORUM_MON_11"] = "十一月";
$MESS["FORUM_MON_12"] = "十二月";
$MESS["FORUM_NAVIGATION"] = "展示";
$MESS["FORUM_NO_ONE"] = "沒有人";
$MESS["FORUM_PAGER_DESC_NUMBERING"] = "使用反向頁面導航";
$MESS["FORUM_PAGER_SETTINGS"] = "Pager設置";
$MESS["FORUM_PAGER_SHOW_ALWAYS"] = "始終顯示尋呼機";
$MESS["FORUM_PAGER_TEMPLATE"] = "Pager模板的名稱";
$MESS["FORUM_PAGER_TITLE"] = "分類名稱";
$MESS["FORUM_TOP_PAGER"] = "在列表的頂部";
$MESS["F_AJAX_TYPE"] = "用戶ajax";
$MESS["F_WORD_CUT"] = "截短";
$MESS["F_WORD_LENGTH"] = "單詞長度";
$MESS["F_WORD_WRAP"] = "裹";
$MESS["F_WORD_WRAP_CUT"] = "短語長度（\“ 0 \”  - 不要截斷）";
