<?php
$MESS["F_DB_CREATEINDEX_FAIL"] = "錯誤創建index \“＃index＃（＃fields＃）\” table \“＃table＃\”。";
$MESS["F_DB_CREATEINDEX_OFFER"] = "要完成更新模塊，您必須為表\“＃table＃\”創建一個索引\“＃index＃（＃fields＃）\”。";
$MESS["F_DB_CREATEINDEX_SUCCESS"] = "索引\“＃index＃（＃fields＃）\”已成功為表\“＃table＃\”創建。";
$MESS["F_DB_DROPINDEX_FAIL"] = "錯誤刪除index \“＃index＃（＃fields＃）\” \“＃table＃\”。";
$MESS["F_DB_DROPINDEX_OFFER"] = "要完成更新模塊，您必須刪除表\“＃table＃\”的索引\“＃索引＃\”。";
$MESS["F_DB_DROPINDEX_SUCCESS"] = "\ \“＃table＃\”的索引\“＃索引＃\”已成功刪除。";
$MESS["F_DB_GO"] = "去";
