<?php
$MESS["EXCEPTION_USER_RATING_FORUM_ACTIVITY_TEXT"] = "需要創建新的索引來啟用用戶論壇活動的計算。";
$MESS["FORUM_RATING_NAME"] = "論壇";
$MESS["FORUM_RATING_USER_RATING_ACTIVITY_DESC"] = "該估計使用了當今創建的主題和帖子的計數；在過去的7天和過去30天中。";
$MESS["FORUM_RATING_USER_RATING_ACTIVITY_FIELDS_ALL_POST_COEF"] = "帖子以上的帖子因素：";
$MESS["FORUM_RATING_USER_RATING_ACTIVITY_FIELDS_ALL_TOPIC_COEF"] = "年齡超過一個月的主題的因素：";
$MESS["FORUM_RATING_USER_RATING_ACTIVITY_FIELDS_MONTH_POST_COEF"] = "最近一個月的評級乘數：";
$MESS["FORUM_RATING_USER_RATING_ACTIVITY_FIELDS_MONTH_TOPIC_COEF"] = "k <ub> p1 </sub>，k <sub> p7 </sub>，k <ub> p30 </sub>  - 當今，上周和上個月帖子的用戶定義因素。";
$MESS["FORUM_RATING_USER_RATING_ACTIVITY_FIELDS_TODAY_POST_COEF"] = "評級今天的帖子：";
$MESS["FORUM_RATING_USER_RATING_ACTIVITY_FIELDS_TODAY_TOPIC_COEF"] = "k <ub> t1 </sub>，k <ub> t7 </sub>，k <ub> t30 </sub>  - 當今，上周和上個月的用戶定義因素。<br>";
$MESS["FORUM_RATING_USER_RATING_ACTIVITY_FIELDS_WEEK_POST_COEF"] = "最近一周的評級乘數：";
$MESS["FORUM_RATING_USER_RATING_ACTIVITY_FIELDS_WEEK_TOPIC_COEF"] = "p <ub> 1 </sub>，p <sub> 7 </sub>，p <ub> 30 </sub>  - 今天創建的帖子數，分別為上一周和上個月； <<br >";
$MESS["FORUM_RATING_USER_RATING_ACTIVITY_FORMULA_DESC"] = "t <sub> 1 </sub>，t <sub> 7 </sub>，t <sub> 30 </sub>，t <sub>全部</sub>  - 創建的主題數：今天；在上週；在上個月和永恆的； <br>
k <sub> t1 </sub>，k <sub> t7 </sub>，k <sub> t30 </sub>，k <ub>高>高> </sub>  - 創建的主題的用戶定義因素：今天；在上週；在上個月和永恆的。<br>
p <sub> 1 </sub>，p <sub> 7 </sub>，p <ub> 30 </sub>，p <sub>所有</sub>  - 創建的帖子數：今天；在上週；在上個月和永恆的； <br>
k <sub> p1 </sub>，k <sub> p7 </sub>，k <ub> p30 </sub>，k <ub> pall </sub>  - 創建的帖子的用戶定義因素：今天；在上週；在過去的一個月和永恆。";
$MESS["FORUM_RATING_USER_RATING_ACTIVITY_NAME"] = "用戶論壇活動";
$MESS["FORUM_RATING_USER_VOTE_POST_DESC"] = "使用指定天數的投票結果計算評級。<br>將天數設置為\“ 0 \”以使用所有數據進行計算。";
$MESS["FORUM_RATING_USER_VOTE_POST_FIELDS_COEFFICIENT"] = "過渡因素：";
$MESS["FORUM_RATING_USER_VOTE_POST_FORMULA_DESC"] = "總數 - 投票結果； K-用戶定義的過渡因子。";
$MESS["FORUM_RATING_USER_VOTE_POST_LIMIT_NAME"] = "白天跨度：";
$MESS["FORUM_RATING_USER_VOTE_POST_NAME"] = "在論壇上投票支持用戶的帖子";
$MESS["FORUM_RATING_USER_VOTE_TOPIC_DESC"] = "使用指定天數的投票結果計算評級。<br>將天數設置為\“ 0 \”以使用所有數據進行計算。";
$MESS["FORUM_RATING_USER_VOTE_TOPIC_FIELDS_COEFFICIENT"] = "過渡因素：";
$MESS["FORUM_RATING_USER_VOTE_TOPIC_FORMULA_DESC"] = "總數 - 投票結果； K-用戶定義的過渡因子。";
$MESS["FORUM_RATING_USER_VOTE_TOPIC_LIMIT_NAME"] = "白天跨度：";
$MESS["FORUM_RATING_USER_VOTE_TOPIC_NAME"] = "在論壇上投票支持用戶的主題";
