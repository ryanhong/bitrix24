<?php
$MESS["FLT_ALREADY_EXIST"] = "記錄已經存在。";
$MESS["FLT_ERR_BAD_DELIMITER"] = "正則表達式的第一個字符不是\“/\”。";
$MESS["FLT_ERR_BAD_MODIFICATOR"] = "正則表達式使用保留的標識符“ E”。";
$MESS["FLT_ERR_BAD_PATTERN"] = "正則表達不正確。";
$MESS["FLT_ERR_DICTIONARY_MISSED"] = "詞典缺少。";
$MESS["FLT_ERR_DICT_PATT_MISSED"] = "圖案丟失了。";
$MESS["FLT_ERR_ID_NOT_ENTER"] = "ID未指定。";
$MESS["FLT_ERR_SIMBOL_MISSED"] = "缺少符號。";
$MESS["FLT_ERR_TITLE_MISSED"] = "詞典標題缺少。";
$MESS["FLT_ERR_TYPE_MISSED"] = "詞典類型缺失。";
