<?php
$MESS["PM_ERR_FOLDER_EXIST"] = "一個帶有此名稱的文件夾已經存在。";
$MESS["PM_ERR_NO_SPACE"] = "沒有自由空間。請清理收件箱。";
$MESS["PM_ERR_SUBJ_EMPTY"] = "請指定主題。";
$MESS["PM_ERR_TEXT_EMPTY"] = "請輸入消息主體。";
$MESS["PM_ERR_USER_EMPTY"] = "請指定用戶。";
