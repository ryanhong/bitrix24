<?php
$MESS["F_GL_ERR_DATE_REG"] = "註冊日期不正確";
$MESS["F_GL_ERR_EMPTY_USER_ID"] = "未指定用戶ID";
$MESS["F_GL_ERR_LAST_VISIT"] = "上次訪問日期不正確";
$MESS["F_GL_ERR_USER_IS_EXIST"] = "代碼＃UID＃的用戶已經在論壇上註冊";
$MESS["F_GL_ERR_USER_NOT_EXIST"] = "用戶使用代碼＃UID＃已經存在。";
