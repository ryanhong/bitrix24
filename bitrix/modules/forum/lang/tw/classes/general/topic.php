<?php
$MESS["F_ERR_EMPTY_TO_MOVE"] = "主題未選擇。請選擇主題。";
$MESS["F_ERR_FORUM_NOT_EXIST"] = "ID =（＃forum_id＃）的論壇不存在。";
$MESS["F_ERR_THIS_TOPIC_IS_NOT_MOVE"] = "主題＃標題＃（＃id＃）未移動。您嘗試將主題轉移到同一論壇。";
$MESS["F_LOGS_MOVE_TOPIC"] = "主題＃topic_title＃[＃topic_id＃]已移至論壇＃forum_title＃[＃forum_id＃]。";
$MESS["F_LOGS_MOVE_TOPIC_WITH_LINK"] = "主題＃topic_title＃[＃topic_id＃]已移動到論壇＃forum_title＃[＃forum_id＃];在上一個位置創建了一個鏈接。";
