<?php
$MESS["AVTOR_PREF"] = "作者：";
$MESS["F_ATTACHED_FILES"] = "附加的文件：";
$MESS["F_ERR_EMPTY_AUTHOR_NAME"] = "帖子作者未指定。";
$MESS["F_ERR_EMPTY_FORUM_ID"] = "該論壇未指定。";
$MESS["F_ERR_EMPTY_POST_MESSAGE"] = "消息文本為空。";
$MESS["F_ERR_EMPTY_TOPIC_ID"] = "主題未指定..";
$MESS["F_ERR_FORUM_IS_LOST"] = "沒有找到論壇。";
$MESS["F_ERR_INVALID_FORUM_ID"] = "無效的論壇ID。";
$MESS["F_ERR_MESSAGE_ALREADY_EXISTS"] = "主題中已經存在一條帶有此類文本的消息。";
$MESS["F_ERR_TOPIC_IS_LINK"] = "指定的主題是鏈接（它不能包含帖子）。";
$MESS["F_ERR_TOPIC_IS_NOT_EXISTS"] = "指定的主題不存在。";
$MESS["F_ERR_UPOAD_FILES_IS_LOST"] = "未找到附件＃file_id＃。";
$MESS["F_ERR_UPOAD_IS_DENIED"] = "文件不能上傳到論壇。";
$MESS["F_SONET_MESSAGE_TITLE"] = "在主題中添加了一個帖子\“＃標題＃\”";
$MESS["F_SONET_TOPIC_TITLE"] = "在論壇中創建了一個主題\“＃標題＃\”";
