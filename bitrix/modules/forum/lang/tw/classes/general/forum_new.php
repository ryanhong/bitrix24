<?php
$MESS["AVTOR_PREF"] = "作者：";
$MESS["FG_ERROR_CONTENT_FORUM"] = "組＃group_name＃[＃group_id＃]包含論壇。";
$MESS["FG_ERROR_CONTENT_GROUP"] = "組＃group_name＃[＃group_id＃]包含其他組。";
$MESS["FG_ERROR_EMPTY_LID"] = "沒有為語言指定組的名稱[＃lid＃]＃lid_name＃";
$MESS["FG_ERROR_EMPTY_PARENT_ID"] = "沒有找到母體。";
$MESS["FG_ERROR_PARENT_ID_IS_CHILD"] = "一個孩子群體不能成為父母。";
$MESS["FG_ERROR_SELF_PARENT_ID"] = "一個小組不能成為自己的父母。";
$MESS["FS_ERROR_EMPTY_IMAGE"] = "未指定笑臉圖像。";
$MESS["FS_ERROR_EMPTY_NAME"] = "笑臉名稱未針對語言＃lid_name＃[＃lid＃]指定。";
$MESS["FS_ERROR_EMPTY_TYPE"] = "未指定笑臉類型。";
$MESS["FS_ERROR_TYPING"] = "笑臉打字包含保留的字符（＆gt;＆lt; \“'）。";
$MESS["FS_ERROR_UNKNOWN_TYPE"] = "未知的笑臉類型：＃類型＃。";
$MESS["F_ERROR_EMPTY_NAME"] = "未指定論壇名稱。";
$MESS["F_ERROR_EMPTY_SITES"] = "該論壇不綁定到任何站點。";
$MESS["F_ERROR_EMPTY_SITE_PATH"] = "沒有為網站＃site_name＃[＃site_id＃]指定的路徑模板。";
$MESS["F_FORUM"] = "論壇";
$MESS["F_FORUMS_LIST"] = "論壇";
$MESS["F_FORUM_EDIT"] = "論壇設置管理";
$MESS["F_FORUM_TITLE"] = "論壇管理";
