<?php
$MESS["ADDMESS_AFTER_MODERATE"] = "調節後將變得可見";
$MESS["ADDMESS_ERROR_ADD_MESSAGE"] = "錯誤創建新消息";
$MESS["ADDMESS_ERROR_ADD_TOPIC"] = "錯誤創建一個新主題";
$MESS["ADDMESS_ERROR_EDIT_MESSAGE"] = "錯誤更新消息";
$MESS["ADDMESS_ERROR_EDIT_TOPIC"] = "錯誤更新主題";
$MESS["ADDMESS_INPUT_AUTHOR"] = "消息的名稱作者的名稱";
$MESS["ADDMESS_INPUT_EDITOR"] = "消息編輯器的名稱";
$MESS["ADDMESS_INPUT_MESSAGE"] = "消息文字";
$MESS["ADDMESS_INPUT_TITLE"] = "主題標題";
$MESS["ADDMESS_NO_PERMS2EDIT"] = "編輯此消息的權限不足";
$MESS["ADDMESS_NO_PERMS2NEW"] = "在這個論壇中創建新主題的權限不足";
$MESS["ADDMESS_NO_PERMS2REPLY"] = "在此主題中創建消息的權限不足";
$MESS["ADDMESS_SUCCESS_ADD"] = "該消息已成功添加";
$MESS["ADDMESS_SUCCESS_EDIT"] = "該消息已成功更新";
$MESS["ADDMES_NO_TYPE"] = "未指定回复模式";
$MESS["AUTHOR"] = "作者";
$MESS["DELMES_NO"] = "消息未刪除";
$MESS["DELMES_NO_MESS"] = "沒有選擇刪除的消息";
$MESS["DELMES_NO_PERMS"] = "刪除此消息的權限不足";
$MESS["DELMES_OK"] = "消息已成功刪除";
$MESS["DELTOP_NO"] = "主題未刪除";
$MESS["DELTOP_NO_PERMS"] = "刪除此主題的權限不足";
$MESS["DELTOP_NO_TOPIC"] = "沒有選擇主題";
$MESS["DELTOP_OK"] = "主題已成功刪除";
$MESS["FANSWER_ACCESS"] = "回覆";
$MESS["FASC"] = "上升";
$MESS["FAUTHOR_TOPIC"] = "主題作者";
$MESS["FDATE_LAST_MESSAGE"] = "最後一條消息的日期";
$MESS["FDESC"] = "下降";
$MESS["FEDIT_ACCESS"] = "編輯";
$MESS["FFULL_ACCESS"] = "滿的";
$MESS["FMESSAGE_TOPIC"] = "消息主題";
$MESS["FMM_NO_MESSAGE"] = "沒有選擇消息";
$MESS["FMM_NO_MESSAGE_MOVE"] = "無法移動消息（ID = ##）";
$MESS["FMM_NO_MODERATE"] = "您沒有足夠的權限來調節這個論壇";
$MESS["FMM_NO_TOPIC_EQUAL"] = "指定當前主題";
$MESS["FMM_NO_TOPIC_NOT_CREATED"] = "新主題不是創建的";
$MESS["FMM_NO_TOPIC_RECIPIENT0"] = "主題未指定（目標）";
$MESS["FMM_NO_TOPIC_RECIPIENT1"] = "主題（目標）不存在";
$MESS["FMM_NO_TOPIC_SOURCE0"] = "主題（來源）未指定";
$MESS["FMM_NO_TOPIC_SOURCE1"] = "主題（來源）不存在";
$MESS["FMM_TOPIC_IS_LINK"] = "主題是鏈接，不能是目的地";
$MESS["FMM_YES_MESSAGE_MOVE"] = "消息被成功地移動";
$MESS["FMODERATE_ACCESS"] = "緩和";
$MESS["FMT_NO_PERMS_EDIT"] = "沒有可用的編輯主題";
$MESS["FMT_NO_TOPICS"] = "沒有選擇主題";
$MESS["FNEW_MESSAGE_ACCESS"] = "創建新主題";
$MESS["FNO_ACCESS"] = "無訪問";
$MESS["FNUM_ANSWERS"] = "答案數量";
$MESS["FNUM_VIEWS"] = "視圖數量";
$MESS["FORUMS"] = "論壇";
$MESS["FORUM_AT_LAST_PERIOD"] = "在論壇的最後##分鐘";
$MESS["FORUM_BUTTON_FILTER"] = "篩選";
$MESS["FORUM_BUTTON_RESET"] = "重置";
$MESS["FORUM_COUNT_ALL_USER"] = "訪客總數";
$MESS["FORUM_COUNT_GUEST"] = "客人數量";
$MESS["FORUM_COUNT_USER"] = "註冊成員";
$MESS["FORUM_COUNT_USER_HIDEFROMONLINE"] = "隱";
$MESS["FORUM_ERROR_NO_TITLE"] = "請輸入名稱";
$MESS["FORUM_ERROR_NO_URL"] = "請輸入地址（URL）";
$MESS["FORUM_FILTER"] = "篩選";
$MESS["FORUM_FROM_THIS"] = "總共";
$MESS["FORUM_GV_ALREADY_VOTE"] = "您的投票以前已經被接受。";
$MESS["FORUM_GV_ERROR_A"] = "投票參數錯誤。";
$MESS["FORUM_GV_ERROR_AUTH"] = "只有授權用戶才能在這裡投票。請授權或註冊。";
$MESS["FORUM_GV_ERROR_NO_VOTE"] = "您還不能投票給其他用戶";
$MESS["FORUM_GV_ERROR_VOTE"] = "投票錯誤。請重試或向管理員報告。";
$MESS["FORUM_GV_ERROR_VOTE_ADD"] = "投票錯誤。請重試或向管理員報告。";
$MESS["FORUM_GV_ERROR_VOTE_UPD"] = "投票錯誤。請重試或向管理員報告。";
$MESS["FORUM_GV_OTHER"] = "你不能為自己投票。";
$MESS["FORUM_GV_SUCCESS_UNVOTE"] = "謝謝你！您的投票已被撤銷。";
$MESS["FORUM_GV_SUCCESS_VOTE_ADD"] = "您的投票已被接受。感謝你的投票！";
$MESS["FORUM_GV_SUCCESS_VOTE_UPD"] = "您的投票已被接受。感謝你的投票！";
$MESS["FORUM_HELP_BOLD"] = "大膽（alt + b）";
$MESS["FORUM_HELP_CLICK_CLOSE"] = "點擊關閉";
$MESS["FORUM_HELP_CLOSE"] = "關閉所有未封閉的標籤";
$MESS["FORUM_HELP_CODE"] = "代碼部分（ALT + P）";
$MESS["FORUM_HELP_COLOR"] = "選擇文本顏色";
$MESS["FORUM_HELP_FONT"] = "選擇文本字體";
$MESS["FORUM_HELP_IMG"] = "插入圖像（alt + g）";
$MESS["FORUM_HELP_ITALIC"] = "斜體（alt + i）";
$MESS["FORUM_HELP_LIST"] = "列表（Alt + L）";
$MESS["FORUM_HELP_QUOTE"] = "報價（alt + q）";
$MESS["FORUM_HELP_TRANSLIT"] = "音譯（Alt + T）";
$MESS["FORUM_HELP_UNDER"] = "下劃線（Alt + U）";
$MESS["FORUM_HELP_URL"] = "超鏈接（Alt + H）";
$MESS["FORUM_LIST"] = "論壇列表";
$MESS["FORUM_LIST_PROMPT"] = "項目清單。單擊“取消”或鍵入完成空間";
$MESS["FORUM_NAME"] = "論壇名稱";
$MESS["FORUM_NONE"] = "沒有任何";
$MESS["FORUM_NO_ACTION"] = "沒有指定動作";
$MESS["FORUM_NO_AUTHORIZE"] = "您需要被授權查看此頁面";
$MESS["FORUM_NO_MODULE"] = "論壇模塊未安裝";
$MESS["FORUM_NO_PERMS"] = "您沒有足夠的權限";
$MESS["FORUM_POST"] = "消息";
$MESS["FORUM_POSTM_CAPTCHA"] = "驗證碼不正確";
$MESS["FORUM_POSTS"] = "帖子";
$MESS["FORUM_SELECT_ALL"] = "全選";
$MESS["FORUM_SHOW_ALL_FILTER"] = "顯示所有過濾器";
$MESS["FORUM_SUB_ERR_ALREADY_ALL"] = "您已經訂閱了所有新論壇帖子。";
$MESS["FORUM_SUB_ERR_ALREADY_ALL_HELP"] = "請退訂＃＃forum_name＃＆raquo; raquo;訂閱所有新帖子。";
$MESS["FORUM_SUB_ERR_ALREADY_NEW"] = "您已經訂閱了這個論壇的新主題。";
$MESS["FORUM_SUB_ERR_ALREADY_NEW_HELP"] = "請退訂所有＃forum_name＃＆raquo＆raquo＆raquo;訂閱新主題。";
$MESS["FORUM_SUB_ERR_ALREADY_TOPIC"] = "您已經被訂閱了此主題。";
$MESS["FORUM_SUB_ERR_AUTH"] = "僅允許註冊用戶訂閱。";
$MESS["FORUM_SUB_ERR_PERMS"] = "訪問此論壇的權限不足。";
$MESS["FORUM_SUB_ERR_UNKNOWN"] = "試圖訂閱論壇消息時發生了錯誤。";
$MESS["FORUM_SUB_ERR_UNSUBSCR"] = "從論壇帖子中取消訂閱的錯誤。";
$MESS["FORUM_SUB_OK_MESSAGE"] = "您已成功訂閱了論壇消息。您可以在個人資料中編輯訂閱設置。";
$MESS["FORUM_SUB_OK_MESSAGE_TOPIC"] = "訂閱新主題消息是成功的。您可以在個人資料中編輯訂閱設置。";
$MESS["FORUM_TEXT_ENTER_IMAGE"] = "圖像地址（URL）";
$MESS["FORUM_TEXT_ENTER_URL"] = "完整地址（URL）";
$MESS["FORUM_TEXT_ENTER_URL_NAME"] = "站點名稱";
$MESS["FORUM_TOPICS"] = "主題";
$MESS["FORUM_USER_PROFILE"] = "用戶資料";
$MESS["FREAD_ACCESS"] = "讀";
$MESS["FR_2SUPPORT"] = "為了幫助工作員";
$MESS["FR_2TOP"] = "回到頂部";
$MESS["FR_ADMIN"] = "行政人員";
$MESS["FR_ADMINISTRATOR"] = "行政人員";
$MESS["FR_AUTHOR_PROFILE"] = "作者個人資料";
$MESS["FR_DATE_CREATE"] = "創建：";
$MESS["FR_DATE_REGISTER"] = "註冊日期：";
$MESS["FR_DELETE"] = "刪除";
$MESS["FR_DELETE_MESS"] = "刪除消息";
$MESS["FR_EDIT"] = "編輯";
$MESS["FR_EDITOR"] = "編輯";
$MESS["FR_EDIT_MESS"] = "編輯消息";
$MESS["FR_EMAIL_AUTHOR"] = "將電子郵件發送給作者";
$MESS["FR_FTITLE"] = "論壇";
$MESS["FR_GUEST"] = "客人";
$MESS["FR_HIDE"] = "隱藏";
$MESS["FR_HIDE_MESS"] = "隱藏消息";
$MESS["FR_ICQ_AUTHOR"] = "將ICQ消息發送給作者";
$MESS["FR_IMAGE_SIZE_NOTICE"] = "最大圖像大小為<b> #width＃</b> x <b> #height＃</b>像素。較大的圖像將自動縮放。";
$MESS["FR_INSERT_NAME"] = "插入名稱回复";
$MESS["FR_MODERATOR"] = "主持人";
$MESS["FR_MOVE2SUPPORT"] = "複製到Helpdesk";
$MESS["FR_NAME"] = "姓名";
$MESS["FR_NO_VOTE"] = "您沒有為此用戶投票。";
$MESS["FR_NO_VOTE1"] = "你可以給他＃積分＃投票";
$MESS["FR_NO_VOTE_DO"] = "投票";
$MESS["FR_NO_VOTE_UNDO"] = "刪除投票";
$MESS["FR_NO_VPERS"] = "沒有用戶sp";
$MESS["FR_NUM_MESS"] = "總郵件：";
$MESS["FR_PRIVATE_MESSAGE"] = "訊息";
$MESS["FR_PROFILE"] = "輪廓";
$MESS["FR_QUOTE"] = "引用";
$MESS["FR_QUOTE_HINT"] = "引用消息在回復中選擇它，然後單擊此處";
$MESS["FR_REAL_IP"] = "/ 真實的";
$MESS["FR_SHOW"] = "展示";
$MESS["FR_SHOW_MESS"] = "顯示消息";
$MESS["FR_TOP"] = "回到頂部";
$MESS["FR_USER"] = "用戶";
$MESS["FR_USER_ID"] = "訪客ID：";
$MESS["FR_USER_ID_USER"] = "用戶身份：";
$MESS["FR_VIDEO"] = "插入視頻";
$MESS["FR_VIDEO_P"] = "視頻路徑";
$MESS["FR_VIDEO_PATH_EXAMPLE"] = "示例：<i> http://www.youtube.com/watch?v=j8yclyzjoeg </i> <br/>或<i> www.mysite.com/video/my_video.mp4 </i>";
$MESS["FR_VIEW"] = "查看消息：＆nbsp;";
$MESS["FR_VOTE_ADMIN"] = "在用戶配置文件中可以找到更多功能性投票。";
$MESS["FR_YOU_ALREADY_VOTE1"] = "您已經投票贊成這個用戶，並給了他＃積分＃投票";
$MESS["FR_YOU_ALREADY_VOTE3"] = "自上次投票以來，您可以給山一次＃積分＃投票。";
$MESS["FSTART_DATE"] = "主題創建日期";
$MESS["FSTP_PAGES"] = "頁面";
$MESS["FSUBSC_NO_SPERMS"] = "您沒有足夠的權限來刪除此訂閱";
$MESS["F_ATTACH_IS_MODIFIED"] = "附件進行了修改。";
$MESS["F_MESSAGE_EDITED_BY_AUTHOR"] = "該帖子由作者編輯。";
$MESS["F_MESSAGE_WAS_MOVED"] = "帖子[＃id＃]已從主題＃topic_title＃[＃topic_id＃]移動到＃new_topic_title＃[＃new_topic_id＃]。";
$MESS["F_MESSAGE_WAS_MOVED_TO_NEW"] = "帖子[＃id＃]已從主題＃topic_title＃[＃topic_id＃]轉移到新主題＃new_topic_title＃[＃new_topic_id＃]。";
$MESS["IS_NEW_MESS"] = "新消息！";
$MESS["JERROR_MAX_LEN1"] = "最大消息長度是";
$MESS["JERROR_MAX_LEN2"] = "符號。總符號：";
$MESS["JERROR_NO_MESSAGE"] = "請輸入消息主體。";
$MESS["JERROR_NO_TOPIC_NAME"] = "請輸入主題名稱。";
$MESS["JQOUTE_AUTHOR_WRITES"] = "寫信";
$MESS["LAST_POST"] = "最後一篇文章";
$MESS["MODMESS_ERROR_MODER"] = "試圖調節消息時發生錯誤";
$MESS["MODMESS_NO_MESS"] = "沒有信息適度";
$MESS["MODMESS_NO_MESSAGES"] = "未選擇調節消息";
$MESS["MODMESS_NO_PERMS"] = "不足以調節此消息的權限";
$MESS["MODMESS_SUCCESS_HIDE"] = "該消息已成功隱藏";
$MESS["MODMESS_SUCCESS_SHOW"] = "該消息已成功發布";
$MESS["MOVEMES_ERROR_SMOVE"] = "無法將消息複製到TechSupport";
$MESS["MOVEMES_NO_ANONYM"] = "匿名消息不能複製到TechSupport";
$MESS["MOVEMES_NO_MESS_EX"] = "沒有消息要復制";
$MESS["MOVEMES_NO_PERMS2MOVE"] = "將消息複製到TechSupport的權限不足";
$MESS["MOVEMES_NO_SUPPORT"] = "未安裝TechSupport模塊。";
$MESS["MOVEMES_SUCCESS_SMOVE"] = "該消息已復製到TechSupport";
$MESS["NO_NEW_MESS"] = "沒有新消息";
$MESS["OCTOP_ERROR_CLOSE"] = "試圖關閉主題時發生錯誤";
$MESS["OCTOP_ERROR_OPEN"] = "試圖打開主題時發生錯誤";
$MESS["OCTOP_NO_PERMS"] = "開放此主題的權限不足";
$MESS["OCTOP_NO_PERMS1"] = "關閉此主題的權限不足";
$MESS["OCTOP_NO_TOPIC"] = "沒有選擇主題";
$MESS["OCTOP_SUCCESS_CLOSE"] = "該主題已成功關閉";
$MESS["OCTOP_SUCCESS_OPEN"] = "該主題已成功開放";
$MESS["SPAMTOP_NO"] = "主題沒有標記為垃圾郵件";
$MESS["SPAMTOP_NO_PERMS"] = "權限不足。";
$MESS["SPAMTOP_OK"] = "該主題已被標記為垃圾郵件並成功刪除。";
$MESS["SPAM_NO"] = "該帖子沒有標記為垃圾郵件";
$MESS["SPAM_NO_MESS"] = "未選擇消息";
$MESS["SPAM_NO_PERMS"] = "權限不足。";
$MESS["SPAM_OK"] = "該帖子已被標記為垃圾郵件並成功刪除。";
$MESS["TOPIC"] = "話題";
$MESS["TOTOP_ERROR_TOP"] = "錯誤固定主題";
$MESS["TOTOP_ERROR_TOP1"] = "錯誤解開主題";
$MESS["TOTOP_NO_PERMS"] = "固定此主題的權限不足";
$MESS["TOTOP_NO_PERMS1"] = "取消本主題的權限不足";
$MESS["TOTOP_NO_TOPIC"] = "沒有選擇主題";
$MESS["TOTOP_SUCCESS_TOP"] = "這個話題已成功固定。";
$MESS["TOTOP_SUCCESS_TOP1"] = "這個話題已成功地沒有鎖定。";
