<?php
$MESS["SALESCENTER_SALEMANAGER_SYSTEM_ORDER_EMAIL_WARNING"] = "[b]注意！[/b]訂單屬性中缺少客戶信息；未指定您的Bitrix24的管理員電子郵件。無法創建收據。添加缺少的客戶詳細信息或管理員數據。";
$MESS["SALESCENTER_SALEMANAGER_SYSTEM_ORDER_NOT_PAID_TEXT"] = "付款：不";
$MESS["SALESCENTER_SALEMANAGER_SYSTEM_ORDER_NO_CASHBOXES_WARNING"] = "[b]注意！[/b]沒有現金寄存器。";
$MESS["SALESCENTER_SALEMANAGER_SYSTEM_ORDER_NO_CHECK_URL_WARNING"] = "[b]注意！[/b]訂單屬性中缺少客戶信息。添加客戶的電子郵件或電話號碼。";
$MESS["SALESCENTER_SALEMANAGER_SYSTEM_ORDER_NO_CONTACT_EMAIL_WARNING"] = "[b]注意！[/b]訂單屬性中缺少客戶信息。添加客戶的電子郵件或電話號碼。否則，您的BitRix24的管理員數據將在收據上打印。";
$MESS["SALESCENTER_SALEMANAGER_SYSTEM_ORDER_PAID_TEXT"] = "使用\“＃paysystem＃\”付款＃日期＃";
