<?php
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_1"] = "謝謝您的訂單！";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_2"] = "合計訂單：";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_3"] = "付款方式：";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_4"] = "支付";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_5"] = "有薪酬的";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_6"] = "請選擇其他付款系統以在線付款";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_7"] = "選擇其他付款方式";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_8"] = "不幸的是有一個錯誤。";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_9"] = "選擇其他付款方式或聯繫我們的一位銷售代表。";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_10"] = "資訊";
$MESS["PAYMENT_PAY_PAYMENT_SYSTEM_COMPONENTS_11"] = "金額：＃sum＃";
