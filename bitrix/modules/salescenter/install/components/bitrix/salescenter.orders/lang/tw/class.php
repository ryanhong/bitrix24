<?php
$MESS["SALESCENTER_MODULE_ERROR"] = "\“銷售中心\”模塊未安裝。";
$MESS["SALESCENTER_ORDERS_ADD_ORDER"] = "新命令";
$MESS["SALESCENTER_ORDERS_LIMITS_MESSAGE"] = "請升級到繼續處理訂單並通過銷售中心接收付款的商業計劃之一。";
$MESS["SALESCENTER_ORDERS_LIMITS_TITLE"] = "您已經達到了可以使用銷售中心創建的最大訂單數量。";
$MESS["SALESCENTER_SESSION_ERROR"] = "會話ID未指定";
