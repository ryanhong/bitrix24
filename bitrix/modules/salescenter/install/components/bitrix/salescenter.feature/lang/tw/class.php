<?php
$MESS["SALESCENTER_FEATURE_MESSAGE"] = "銷售中心不可用於您的訂閱計劃。請升級到使用銷售中心的主要商業計劃之一";
$MESS["SALESCENTER_FEATURE_MODULE_ERROR"] = "\“銷售中心\”模塊未安裝";
$MESS["SALESCENTER_FEATURE_TITLE"] = "僅用於主要商業計劃";
$MESS["SALESCENTER_LIMITS_MESSAGE"] = "恭喜！ <br />您已經使用實時聊天和短信創建了100個訂單。<br /> <br />請升級到一個商業計劃之一，以連續處理訂單並通過銷售中心接收付款。";
$MESS["SALESCENTER_LIMITS_TITLE"] = "僅在選定的商業計劃上可用";
