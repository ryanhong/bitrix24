<?php
$MESS["socserv_mailru2_id"] = "應用ID：";
$MESS["socserv_mailru2_key"] = "密鑰：";
$MESS["socserv_mailru2_note"] = "使用您的Mail.ru帳戶登錄。";
$MESS["socserv_mailru2_note_intranet"] = "使用mail.ru登錄";
$MESS["socserv_mailru2_sett_note_2"] = "您將必須要<a href= \"https://o2.mail.ru/app/new \ \">創建一個應用程序</a>才能接收代碼。<br />
在\“ All Redirect_url的\”字段中指定以下地址：<a href= \"#url# \ \">＃url＃</a> <br> <br> <br>
要對郵箱進行身份驗證，請在\“ All Redirect_url's \”字段中指定以下地址：<a href= \“#mail_url# \ \">＃mail_url＃</a>，<br>
並選擇以下權限：
<ul style = \“ text-align：左; \”>
<li>名字和姓氏；出生日期;電子郵件地址</li>
<li>通過IMAP訪問電子郵件</li>
</ul>
";
