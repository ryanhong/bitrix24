<?php
$MESS["socserv_tw_key"] = "消費者密鑰：";
$MESS["socserv_tw_note"] = "使用您的Twitter.com個人資料登錄。";
$MESS["socserv_tw_note_intranet"] = "使用Twitter帳戶登錄";
$MESS["socserv_tw_secret"] = "應用程序秘密代碼（消費者秘密）：";
$MESS["socserv_tw_sett_note"] = "要獲取ID和密鑰，請<a href= \"https://apps.twitter.com/app/new \">註冊Twitter應用程序</a>。";
$MESS["socserv_tw_sett_note1"] = "要獲取代碼，您必須<a href= \"https://apps.twitter.com/app/new \">註冊您的Twitter應用程序</a>。<br>提示時，指定<a href = \“＃url＃\”>＃url＃</a>在\“ callback url \”字段中。<br>選擇以下權限：\“訪問：讀取，寫和寫入和訪問直接消息\”。 <br / >在應用程序設置中取消選中\“啟用回調鎖定\”選項。";
