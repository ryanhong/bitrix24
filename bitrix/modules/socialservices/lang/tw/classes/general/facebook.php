<?php
$MESS["socserv_fb_id"] = "應用ID（應用ID）：";
$MESS["socserv_fb_instagram_url_preview"] = "使用應用程序創建Instagram豐富鏈接：";
$MESS["socserv_fb_note"] = "使用您的Facebook.com個人資料登錄。";
$MESS["socserv_fb_note_intranet"] = "使用Facebook帳戶登錄";
$MESS["socserv_fb_secret"] = "應用程序秘密代碼（App Secret）：";
$MESS["socserv_fb_sett_note"] = "要獲取ID和密鑰，請<a href= \"https://developers.facebook.com/apps/xprol \ \">註冊Facebook應用程序</a>。";
$MESS["socserv_fb_sett_note1"] = "要獲取ID和鍵，<a href= \"https://developers.facebook.com/apps/xpers/xprocy </a>。<br>。<br> <br>在應用程序中，啟用\“ facebook登錄\”選項。<br>在\“有效的OAuth重定向uris \”字段中，指定以下URL：<a href= \"#url# \ \">＃url＃</a>";
$MESS["socserv_fb_sett_note_oembed_2"] = "您必須添加函數<a href= \"https://developers.facebook.com/docs/instagram/oembed \“target= \"_blank \"> oembed read </a>讀取</a>應用程序供您審查到Facebook。";
