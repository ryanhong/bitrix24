<?php
$MESS["MAIN_OPTION_COMMENT"] = "使用Office365帳戶登錄。";
$MESS["socserv_office365_client_id"] = "客戶端ID：";
$MESS["socserv_office365_client_secret"] = "鑰匙：";
$MESS["socserv_office365_form_note"] = "您必須使用<a href = \"https://msdn.microsoft.com/en-us/office/office/office365/howto/add-common-common-consent-manally \">手冊</ a>。 <br>在\“回复url \”字段中提供此地址：<a href= \"#url# \ \">＃url＃</a> <br /> <br />啟用身份驗證和用戶帳戶，選擇<b >登錄並閱讀用戶配置文件</b> <b> Microsoft Graph </b>。<br />啟用Bitrix24.Drive Integration的權限，選擇<b> <b>讀取並寫入用戶文件</b>權限</b> <b> Office365 SharePoint Online < /b>。<br /> <br />如果您添加了公司域名（租戶），則僅允許該域的Office365帳戶進行身份驗證。 <br /> <br /> <br / >使用<b> \“ Office365 \” </b>，<b> \“ Outlook \” Outlook \“ </b>和<b> \” Exchange Online \ /b>郵箱：<br />在\“ Authentication \”頁面上的\“重定向uris \”字段中輸入以下地址：<a href= \“#mmail_url# \ \">＃mail_url＃</a> <br /> <br />添加這些權限在\“ API Permissions \”頁面上：<li> \“ Imap.accessasuser.all \” </li> <li> \ \“ Offline_access \” </li>";
$MESS["socserv_office365_tenant"] = "租戶：";
