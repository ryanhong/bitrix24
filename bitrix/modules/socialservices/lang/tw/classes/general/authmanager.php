<?php
$MESS["SC_ADD_ERROR"] = "您的帳戶已經鏈接到指定的社交網絡帳戶。";
$MESS["SC_MESSAGE_FROM_TWITTER"] = "來自Twitter的消息";
$MESS["SC_USER_WROTE_YOU"] = "給你發消息";
$MESS["socserv_controller_error"] = "授權錯誤發生在＃Service_name＃。";
$MESS["socserv_error_new_user"] = "要登錄，您必須在個人資料設置頁面上附加帳戶＃service_name＃。";
