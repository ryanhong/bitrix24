<?php
$MESS["socserv_dropbox_client_id"] = "ID（應用程序鍵）：";
$MESS["socserv_dropbox_client_secret"] = "秘密代碼（App Secret）：";
$MESS["socserv_dropbox_form_note"] = "使用您的Dropbox帳戶登錄。";
$MESS["socserv_dropbox_form_note_intranet"] = "使用Dropbox帳戶登錄。";
$MESS["socserv_dropbox_note"] = "要獲得ID，您必須<a href= \"https://www.dropbox.com/developers/apps/create \ \">創建一個Dropbox API應用程序</a>。 <br>創建後，在\“重定向URIS \”字段中指定此URL：<a href= \"#url# \">＃url＃</a> <br />僅支持HTTPS URL。";
