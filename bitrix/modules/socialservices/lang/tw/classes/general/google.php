<?php
$MESS["socserv_google_api_key"] = "應用API密鑰（API_KEY）（可選）：";
$MESS["socserv_google_client_id"] = "客戶端ID：";
$MESS["socserv_google_client_secret"] = "秘密代碼（客戶秘密）：";
$MESS["socserv_google_form_note"] = "使用您的Google帳戶登錄。";
$MESS["socserv_google_form_note_intranet"] = "使用Google帳戶登錄";
$MESS["socserv_google_note_2"] = "<a href= \“https://console.developers.google.com/ \">在Google API控制台中創建一個應用程序</a> <br>
在\“授權重定向uris”字段中指定此地址：<a href= \"#url# \">＃url＃</a>
<ul style = \“ text-align：左; \”>
<li>如果您打算將Bitrix24日曆與Google日曆同步。</li>
<li>允許訪問應用程序設置中的驅動器API與Bitrix24.Drive集成。</li>
<li>要啟用對郵箱的訪問，請將此地址添加到“授權重定向URI”中。字段：<a href= \"#mail_url# \ \">＃mail_url＃</a>，允許在應用程序設置中訪問Gmail API。
</li>
</ul>";
$MESS["socserv_google_sync_proxy"] = "使用Bitrix代理服務器同步：";
$MESS["socserv_googleplus_note"] = "使用Google App參數。";
