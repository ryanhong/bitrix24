<?php
$MESS["SOCSERV_APPLE_ID"] = "應用ID";
$MESS["SOCSERV_APPLE_KEY_ID"] = "密鑰ID";
$MESS["SOCSERV_APPLE_KEY_PEM"] = "PEM格式的秘密鑰匙";
$MESS["SOCSERV_APPLE_NOTE"] = "使用您的Apple ID登錄";
$MESS["SOCSERV_APPLE_NOTE_INTRANET"] = "使用Apple ID登錄";
$MESS["SOCSERV_APPLE_SETT_NOTE_2"] = "請關注<a href= \"https://developer.apple.com/sign-in-with-apple/ \ \">指令</a>與Apple配置簽名。<br />";
$MESS["SOCSERV_APPLE_TEAM_ID"] = "團隊ID";
