<?php
$MESS["socserv_yandex_client_id"] = "應用ID：";
$MESS["socserv_yandex_client_secret"] = "應用程序密碼：";
$MESS["socserv_yandex_form_note"] = "使用您的Yandex帳戶登錄。";
$MESS["socserv_yandex_form_note_intranet"] = "使用Box.com帳戶登錄。";
$MESS["socserv_yandex_note_2"] = "您必須要<a href= \"https://oauth.yandex.ru/client/new \">創建一個應用程序</a>以獲取ID。<br>
在應用程序設置中指定此地址為回調URI：<a href= \"#url# \">＃url＃</a> <br />
在Yandex.Passport API部分中，選擇“權限”區域中的所有權限。<br /> <br />
要與Bitrix24的完全集成。Drive，在Yandex中選擇這些權限。DiskREST API部分：
<ul style = \“ text-align：左; \”>
<li>閱讀驅動器信息</li>
<li>閱讀所有驅動器</li>
</ul>
將此地址指定為回調uri以驗證郵箱：<a href= \“#mail_url# \ \">＃mail_url＃</a>，<br>
並在Yandex.mail部分中選擇這些權限：
<ul style = \“ text-align：左; \”>
<li>閱讀郵箱消息</li>
<li>閱讀和刪除郵箱消息</li>
</ul>";
