<?php
$MESS["SOCSERV_ZOOM_CLIENT_ID"] = "客戶端ID：";
$MESS["SOCSERV_ZOOM_CLIENT_SECRET"] = "客戶秘密：";
$MESS["SOCSERV_ZOOM_NOTE"] = "使用您的Zoom帳戶登錄。";
$MESS["SOCSERV_ZOOM_NOTE_INTRANET"] = "使用變焦登錄";
$MESS["SOCSERV_ZOOM_SETT_NOTE_2"] = "要獲得ID，您必須<a href= \"https://marketplace.zoom.us.us/develop/create?source=devdocs \">創建一個OAuth應用程序</a>。 <br>然後，在應用程序設置中的oauth \“重定向URL”中指定此地址：<a href= \“#url# \ \">＃url＃</a> <br />只有https是支持的。
<br>選擇會議：寫入和用戶：在提示選擇權限時閱讀。
";
