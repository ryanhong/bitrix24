<?php
$MESS["SOC_OPT_B24NET_CLIENT_ID"] = "門戶ID";
$MESS["SOC_OPT_B24NET_CLIENT_SECRET"] = "門戶秘密代碼";
$MESS["SOC_OPT_B24NET_GET"] = "得到";
$MESS["SOC_OPT_B24NET_PUT"] = "粘貼";
$MESS["SOC_OPT_B24NET_SITE"] = "獲取網站的身份驗證參數";
$MESS["SOC_OPT_B24NET_TITLE"] = "在Bitrix24.network中註冊網站";
$MESS["SOC_OPT_CRYPTO_ACTIVATE"] = "啟用身份驗證令牌加密";
$MESS["SOC_OPT_CRYPTO_CONFIRM"] = "確定要啟用身份驗證令牌加密嗎？";
$MESS["SOC_OPT_CRYPTO_FIELD_TITLE"] = "地位：";
$MESS["SOC_OPT_CRYPTO_MESSAGE_ACTIVE"] = "啟用了身份驗證令牌加密";
$MESS["SOC_OPT_CRYPTO_NOTE"] = "啟用此選項將以加密形式存儲社交媒體網站的用戶身份驗證令牌。
<br>
<b>注意！</b>無法撤消此動作！";
$MESS["SOC_OPT_CRYPTO_NO_CRYPTOKEY"] = "未安裝加密密鑰";
$MESS["SOC_OPT_CRYPTO_TAB_DESCR"] = "身份驗證令牌加密";
$MESS["SOC_OPT_CRYPTO_TAB_TITLE"] = "加密";
$MESS["SOC_OPT_MAIN_DENY_AUTH"] = "無法使用社交服務登錄的用戶組";
$MESS["SOC_OPT_MAIN_DENY_SPLIT"] = "無法附加社會服務的用戶組";
$MESS["SOC_OPT_MAIN_REG"] = "新的用戶註冊";
$MESS["SOC_OPT_MAIN_REG_N"] = "不允許";
$MESS["SOC_OPT_MAIN_REG_Y"] = "允許";
$MESS["SOC_OPT_SOC_REG"] = "新用戶可以通過社交網絡（社交登錄）進行註冊";
$MESS["soc_serv_opt_allow"] = "允許使用外部服務授權";
$MESS["soc_serv_opt_down"] = "向下";
$MESS["soc_serv_opt_list"] = "可用服務：";
$MESS["soc_serv_opt_list_title"] = "外部服務";
$MESS["soc_serv_opt_settings_of"] = "＃服務＃的設置";
$MESS["soc_serv_opt_up"] = "向上";
$MESS["soc_serv_send_activity"] = "將用戶活動發佈到社交網絡";
$MESS["socserv_sett_common"] = "常見的";
$MESS["socserv_sett_common_title"] = "所有網站共有的參數";
$MESS["socserv_sett_site"] = "網站的設置";
$MESS["socserv_sett_site_apply"] = "將單個參數應用於本網站：";
$MESS["socserv_twit_to_buzz2"] = "繼電器用戶用哈希＃哈希＃饋送推文";
