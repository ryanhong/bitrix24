<?php
$MESS["SS_JOIN_NOTIFY"] = "您的朋友加入了Bitrix24。現在，您可以直接從Bitrix24與它們聊天。";
$MESS["SS_JOIN_NOTIFY_MORE"] = "＃num＃更多";
$MESS["SS_JOIN_NOTIFY_MULTIPLE"] = "您的朋友正在使用Bitrix24。與他們的人聊天或私人聊天。";
$MESS["SS_JOIN_NOTIFY_POSSIBLE"] = "您認識的人正在使用Bitrix24。與他們聯繫，並邀請他們直接從您的Bitrix24進行私人聊天。";
