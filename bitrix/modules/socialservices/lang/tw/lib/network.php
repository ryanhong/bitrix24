<?php
$MESS["B24NET_ERROR_INCORRECT_PARAMS"] = "提供了不正確的參數。";
$MESS["B24NET_NETWORK_IN_NOT_ENABLED"] = "BitRix24.Network 2.0未在門戶網站上啟用。";
$MESS["B24NET_POPUP_CONNECT"] = "連接";
$MESS["B24NET_POPUP_DONTSHOW"] = "不要再顯示";
$MESS["B24NET_POPUP_TEXT"] = "<b>連接您的Bitrix24 < /b>，在所有網站上使用一個登錄名和密碼。< /b> <br /> <br />忘記其他密碼，Bitrix24就是這樣。";
$MESS["B24NET_POPUP_TITLE"] = "Bitrix24-單登錄";
$MESS["B24NET_SEARCH_STRING_TO_SHORT"] = "搜索字符串太短。";
$MESS["B24NET_SEARCH_USER_NOT_FOUND"] = "找不到用戶。";
$MESS["B24NET_SOCSERV_NOT_INSTALLED"] = "未安裝\“社會服務\”模塊。";
$MESS["B24NET_SOCSERV_TRANSPORT_ERROR"] = "在Bitrix24.network上驗證錯誤。請再次登錄。";
