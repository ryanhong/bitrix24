<?php
$MESS["SS_USERTAB_CREATE_LINK"] = "關聯";
$MESS["SS_USERTAB_DELETE_LINK"] = "UNLINK";
$MESS["SS_USERTAB_LINKED_PROFILE"] = "鏈接配置文件";
$MESS["SS_USERTAB_NOT_CONNECTED_OTHER"] = "用戶的配置文件未鏈接到Bitrix24。";
$MESS["SS_USERTAB_NOT_CONNECTED_SELF"] = "您仍然沒有將Bitrix24鏈接到您的個人資料。";
$MESS["socialservices_TAB"] = "Bitrix24";
$MESS["socialservices_TAB_TITLE"] = "Bitrix24配置文件設置";
