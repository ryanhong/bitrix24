<?php
$MESS["B24NET_REG_WRONG_URL"] = "無效的網站地址";
$MESS["JS_CORE_SS_SETUP_ACCOUNT"] = "將您的個人資料鏈接到社交網絡帳戶<a #class#href='#link#'>您的用戶個人資料頁面</a>。";
$MESS["JS_CORE_SS_WORKDAY_END"] = "我用＃bitrix24完成了工作日。完成：＃任務＃任務，＃事件＃會議。";
$MESS["JS_CORE_SS_WORKDAY_START"] = "我開始使用＃Bitrix24的工作日。";
