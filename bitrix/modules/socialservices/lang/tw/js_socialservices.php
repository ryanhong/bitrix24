<?php
$MESS["JS_CORE_SS_EVENT_CANCEL"] = "取消";
$MESS["JS_CORE_SS_EVENT_SEND"] = "節省";
$MESS["JS_CORE_SS_EVENT_SETUP"] = "配置";
$MESS["JS_CORE_SS_SEND_ERROR"] = "未發送";
$MESS["JS_CORE_SS_SEND_SUCCESS"] = "成功";
$MESS["JS_CORE_SS_SEND_TO_END"] = "發送時鐘";
$MESS["JS_CORE_SS_SEND_TO_SOCSERV"] = "共享社交網絡";
$MESS["JS_CORE_SS_SEND_TO_START"] = "發送時鐘";
$MESS["JS_CORE_SS_WORKDAY_END"] = "我用＃bitrix24完成了工作日。完成：＃任務＃任務，＃事件＃會議。";
$MESS["JS_CORE_SS_WORKDAY_START"] = "我開始使用＃Bitrix24的工作日。";
