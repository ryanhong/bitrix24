<?php
$MESS["GD_BITRIXCLOUD_MONITOR"] = "雲<br>檢查員";
$MESS["GD_BITRIXCLOUD_MONITOR_BTN_ALERT"] = "細節";
$MESS["GD_BITRIXCLOUD_MONITOR_BTN_OK"] = "細節";
$MESS["GD_BITRIXCLOUD_MONITOR_MESS_ALERT1_MONTH"] = "每月<br>損失";
$MESS["GD_BITRIXCLOUD_MONITOR_MESS_ALERT1_QUARTER"] = "季度<br>損失";
$MESS["GD_BITRIXCLOUD_MONITOR_MESS_ALERT1_WEEK"] = "每週<br>損失";
$MESS["GD_BITRIXCLOUD_MONITOR_MESS_ALERT1_YEAR"] = "每年<br>損失";
$MESS["GD_BITRIXCLOUD_MONITOR_MESS_ALERT2_MONTH"] = "每月<br>停機時間";
$MESS["GD_BITRIXCLOUD_MONITOR_MESS_ALERT2_QUARTER"] = "季度<br>停機時間";
$MESS["GD_BITRIXCLOUD_MONITOR_MESS_ALERT2_WEEK"] = "每週停機時間";
$MESS["GD_BITRIXCLOUD_MONITOR_MESS_ALERT2_YEAR"] = "年度停機時間";
$MESS["GD_BITRIXCLOUD_MONITOR_MESS_OK"] = "沒有問題。";
$MESS["GD_BITRIXCLOUD_MONITOR_PROBLEMS"] = "檢測到的問題：＃計數＃。";
$MESS["GD_BITRIXCLOUD_MONITOR_TEST_DOMAIN_REGISTRATION"] = "域＃域＃將在＃天＃天到期。";
$MESS["GD_BITRIXCLOUD_MONITOR_TEST_LIC"] = "＃域＃的許可將在＃天＃天到期。";
$MESS["GD_BITRIXCLOUD_MONITOR_TEST_SSL_CERT_VALIDITY"] = "＃域＃的SSL證書將在＃天＃天到期。";
