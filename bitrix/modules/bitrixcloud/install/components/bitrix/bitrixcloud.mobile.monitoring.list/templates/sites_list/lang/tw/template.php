<?php
$MESS["BCLMMSL_MONITORING_BUT_DETAIL"] = "細節";
$MESS["BCLMMSL_MONITORING_DOMAIN_REGISTRATION"] = "該域將到期";
$MESS["BCLMMSL_MONITORING_FAILED_PERIOD"] = "停機時間";
$MESS["BCLMMSL_MONITORING_HTTP_RESPONSE_TIME"] = "網站響應";
$MESS["BCLMMSL_MONITORING_LICENSE"] = "許可將到期";
$MESS["BCLMMSL_MONITORING_MONITORING_PERIOD"] = "日期範圍";
$MESS["BCLMMSL_MONITORING_MONITORING_SSL"] = "SSL證書將過期";
$MESS["BCLMMSL_MONITORING_MONTH_LOST"] = "每月損失";
$MESS["BCLMMSL_MONITORING_NO_PROBS"] = "當前您的網站沒有問題。";
$MESS["BCLMMSL_MONITORING_TITLE"] = "網站檢查員";
$MESS["BCLMMSL_NO_DATA"] = "沒有數據";
$MESS["BCLMMSL_NO_DOMAINS"] = "沒有配置域";
$MESS["BCLMMSL_NO_SITES"] = "當前沒有網站。<br>單擊（+）添加一個。";
$MESS["BCLMMSL_NO_SITES_TITLE"] = "訊息";
$MESS["BCLMMSL_TITLE"] = "雲檢查員";
