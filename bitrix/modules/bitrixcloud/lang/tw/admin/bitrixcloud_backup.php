<?php
$MESS["BCL_BACKUP_DO_BACKUP"] = "創建備份";
$MESS["BCL_BACKUP_FILE_NAME"] = "文件名";
$MESS["BCL_BACKUP_FILE_SIZE"] = "文件大小";
$MESS["BCL_BACKUP_NOTE"] = "
<p>
Bitrix站點管理器的所有版本都可以在雲存儲中創建備份。不同的<a target= \"_blank \" href= \"http://www.bitrixsoft.com/products/cms/features/cdn.php \">雲空間</a>用於不同版本。將備份保存到雲存儲僅適用於在活動和有效許可下運行的網站。
</p>
<p>
將備份保存到Bitrix Cloud Storage；最多可以使用三個最新的備份副本</b>。每當將較新的副本發送到雲時，就會清除其中一份舊副本。但是，如果備份副本的總尺寸超過可用的雲空間，則最大副本數量可能會減少。如果單個備份副本的大小超過可用的雲空間，則備份功能將無法使用。
</p>
<p>
備份副本是使用密碼加密的，您在創建備份時必須提供。 Bitrix24不存儲密碼，因此無法訪問您的信息。因此，如果丟失密碼，您將無法從備份副本中恢復網站。
</p>
<p>
雲存儲條件和條款可以更改而無需事先通知。
</p>
";
$MESS["BCL_BACKUP_TITLE"] = "備份";
$MESS["BCL_BACKUP_USAGE"] = "使用的空間：＃usage＃of＃配額＃。";
