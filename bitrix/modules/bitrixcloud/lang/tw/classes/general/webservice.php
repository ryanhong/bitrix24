<?php
$MESS["BCL_CDN_WS_CMS_LICENSE_NOT_FOUND"] = "該網站的許可無效。";
$MESS["BCL_CDN_WS_DOMAIN_NOT_REACHABLE"] = "指定的URL無法從雲訪問。";
$MESS["BCL_CDN_WS_LICENSE_DEMO"] = "對於此許可類型，服務不可用。";
$MESS["BCL_CDN_WS_LICENSE_EXPIRE"] = "您的許可已過期。";
$MESS["BCL_CDN_WS_LICENSE_NOT_ACTIVE"] = "許可證不活動。";
$MESS["BCL_CDN_WS_LICENSE_NOT_FOUND"] = "您的許可證無效。";
$MESS["BCL_CDN_WS_NOT_POWERED_BY_BITRIX_CMS"] = "指定的網站不由Bitrix網站管理器提供動力。";
$MESS["BCL_CDN_WS_QUOTA_EXCEEDED"] = "交通配額超過。";
$MESS["BCL_CDN_WS_SERVER"] = "錯誤從服務器獲取參數（代碼：＃狀態＃）。";
$MESS["BCL_CDN_WS_WRONG_DOMAIN_SPECIFIED"] = "指定的網站使用其他許可證密鑰。";
$MESS["BCL_CDN_WS_XML_PARSE"] = "錯誤解析XML配置文件（代碼：＃代碼＃）。";
