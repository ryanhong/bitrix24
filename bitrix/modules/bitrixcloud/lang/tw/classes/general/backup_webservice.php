<?php
$MESS["BCL_BACKUP_EMPTY_SECRET_KEY"] = "未指定備份鍵。";
$MESS["BCL_BACKUP_WRONG_TIME"] = "備份開始時間不正確。";
$MESS["BCL_BACKUP_WRONG_URL"] = "備份URL不正確。";
$MESS["BCL_BACKUP_WRONG_WEEKDAYS"] = "每周備份的日子不正確。";
$MESS["BCL_BACKUP_WS_SERVER"] = "錯誤將請求發送到備份服務器（代碼：＃狀態＃）。";
