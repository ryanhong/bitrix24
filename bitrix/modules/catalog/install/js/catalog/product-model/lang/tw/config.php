<?php
$MESS["CATALOG_PRODUCT_MODEL_ERROR_NOTIFICATION"] = "填寫所需的字段";
$MESS["CATALOG_PRODUCT_MODEL_SAVING_NOTIFICATION_ACCEPT"] = "是的";
$MESS["CATALOG_PRODUCT_MODEL_SAVING_NOTIFICATION_BRAND_CHANGED_QUERY"] = "產品品牌將保存到目錄中。";
$MESS["CATALOG_PRODUCT_MODEL_SAVING_NOTIFICATION_DECLINE_SAVE"] = "不要保存";
$MESS["CATALOG_PRODUCT_MODEL_SAVING_NOTIFICATION_MEASURE_CHANGED_QUERY"] = "測量的產品單位將保存到目錄。";
$MESS["CATALOG_PRODUCT_MODEL_SAVING_NOTIFICATION_PRICE_CHANGED_QUERY"] = "產品價格將保存到目錄。";
