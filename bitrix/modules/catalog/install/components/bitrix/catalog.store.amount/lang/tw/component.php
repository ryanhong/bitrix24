<?php
$MESS["ABSENT"] = "沒存貨";
$MESS["BALANCE"] = "總倉庫股票";
$MESS["CATALOG_MODULE_NOT_INSTALL"] = "未安裝商業目錄模塊。";
$MESS["LOT_OF_GOOD"] = "有存貨";
$MESS["NOT_MUCH_GOOD"] = "只剩下幾個";
$MESS["PRODUCT_NOT_EXIST"] = "該產品不存在。";
