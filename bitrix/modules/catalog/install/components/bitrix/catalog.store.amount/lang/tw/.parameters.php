<?php
$MESS["COMPARE_PAGE"] = "比較圖表";
$MESS["CP_CSA_GROUP_STORE"] = "倉庫";
$MESS["CP_CSA_PARAM_ADDRESS"] = "地址";
$MESS["CP_CSA_PARAM_COORDINATES"] = "坐標";
$MESS["CP_CSA_PARAM_DESCRIPTION"] = "描述";
$MESS["CP_CSA_PARAM_ELEMENT_CODE"] = "產品ID";
$MESS["CP_CSA_PARAM_ELEMENT_ID"] = "產品";
$MESS["CP_CSA_PARAM_EMAIL"] = "電子郵件";
$MESS["CP_CSA_PARAM_FIELDS"] = "字段";
$MESS["CP_CSA_PARAM_IBLOCK_ID"] = "信息塊";
$MESS["CP_CSA_PARAM_IBLOCK_TYPE"] = "信息塊類型";
$MESS["CP_CSA_PARAM_IMAGE_ID"] = "圖像";
$MESS["CP_CSA_PARAM_MAIN_TITLE"] = "標題";
$MESS["CP_CSA_PARAM_MIN_AMOUNT"] = "正供應狀態的最小數量";
$MESS["CP_CSA_PARAM_OFFER_ID"] = "sku";
$MESS["CP_CSA_PARAM_PHONE"] = "電話號碼";
$MESS["CP_CSA_PARAM_SCHEDULE"] = "展示倉庫營業時間";
$MESS["CP_CSA_PARAM_SEF_MODE"] = "使用sef";
$MESS["CP_CSA_PARAM_SHOW_EMPTY_STORE"] = "展示倉庫時";
$MESS["CP_CSA_PARAM_STORES"] = "倉庫";
$MESS["CP_CSA_PARAM_STORE_PATH"] = "URL顯示倉庫詳細信息";
$MESS["CP_CSA_PARAM_TITLE"] = "姓名";
$MESS["CP_CSA_PARAM_USER_FIELDS"] = "特性";
$MESS["CP_CSA_PARAM_USE_MIN_AMOUNT"] = "顯示可用性狀態而不是精確數量";
$MESS["CP_CSA_PARAM_USE_STORE_PHONE"] = "顯示倉庫電話號碼";
$MESS["CP_CSA_SHOW_GENERAL_STORE_INFORMATION"] = "展示倉庫摘要";
$MESS["DETAIL_PAGE"] = "細節";
$MESS["ELEMENT_ID_TIP"] = "要顯示庫存的產品的ID";
$MESS["MAIN_TITLE_TIP"] = "指定倉庫股票報告的標題";
$MESS["MIN_AMOUNT_TIP"] = "如果數量大於指定值，則產品具有足夠的庫存";
$MESS["SCHEDULE_TIP"] = "指定顯示倉庫業務號碼";
$MESS["SECTIONS_TOP_PAGE"] = "部分";
$MESS["SECTION_PAGE"] = "部分";
$MESS["SHOW_EMPTY_STORE_TIP"] = "展示倉庫";
$MESS["STORES_TIP"] = "如果未選擇倉庫，則該組件將在所有可用倉庫中顯示信息。";
$MESS["STORE_PATH_TIP"] = "指定頁面的URL顯示倉庫詳細信息";
$MESS["USE_MIN_AMOUNT_TIP"] = "如果已檢查，報告只會指示產品在某個倉庫中是否有足夠的庫存而不是精確的數字";
$MESS["USE_STORE_PHONE_TIP"] = "指定顯示倉庫電話號碼";
