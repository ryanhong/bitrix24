<?php
$MESS["CATALOG_SEF_DETAIL"] = "倉庫詳細信息";
$MESS["CATALOG_SEF_INDEX"] = "倉庫";
$MESS["DEFAULT_TITLE"] = "倉庫列表和詳細信息";
$MESS["MAP_TYPE"] = "地圖類型";
$MESS["SHOW_PHONE"] = "顯示電話";
$MESS["SHOW_SCHEDULE"] = "顯示工作時間";
$MESS["TITLE"] = "頁面標題";
$MESS["USE_TITLE"] = "設置頁面標題";
