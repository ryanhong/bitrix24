<?php
$MESS["STORE_STOCK_REPORT_EMPTY_STORE_NAME"] = "無標題";
$MESS["STORE_STOCK_REPORT_GRID_AMOUNT_SUM_COLUMN"] = "數量";
$MESS["STORE_STOCK_REPORT_GRID_AMOUNT_SUM_COLUMN_HINT"] = "當前物品庫存";
$MESS["STORE_STOCK_REPORT_GRID_NO_READ_RIGHTS_ERROR"] = "未足夠查看項目的許可";
$MESS["STORE_STOCK_REPORT_GRID_OVERALL_TOTAL"] = "全部的";
$MESS["STORE_STOCK_REPORT_GRID_QUANTITY_COLUMN"] = "可用的庫存";
$MESS["STORE_STOCK_REPORT_GRID_QUANTITY_COLUMN_HINT"] = "當前可出售的股票";
$MESS["STORE_STOCK_REPORT_GRID_QUANTITY_RESERVED_SUM_COLUMN"] = "預訂的";
$MESS["STORE_STOCK_REPORT_GRID_QUANTITY_RESERVED_SUM_COLUMN_HINT"] = "目前保留的項目";
$MESS["STORE_STOCK_REPORT_GRID_TITLE_COLUMN"] = "倉庫";
$MESS["STORE_STOCK_REPORT_MEASURE_TEMPLATE"] = "＃號碼＃＃MEATE_SYMBOL＃";
