<?php
$MESS["BX_CPS_COMP_ERR_CATALOG_MODULE_NOT_INSTALL"] = "未安裝商業目錄模塊。";
$MESS["SOPS_ACTIVE"] = "積極的";
$MESS["SOPS_BALANCE"] = "餘額（總 /倉庫）";
$MESS["SOPS_BALANCE2"] = "平衡";
$MESS["SPS_ACTIVE"] = "積極的";
$MESS["SPS_ANY"] = "任何";
$MESS["SPS_A_PROP_NOT_SET"] = "（沒有設置）";
$MESS["SPS_CHOOSE_CATALOG"] = "選擇目錄";
$MESS["SPS_CODE"] = "符號代碼";
$MESS["SPS_COLLAPSE"] = "隱藏";
$MESS["SPS_EXPAND"] = "擴張";
$MESS["SPS_FIELD_ACTION"] = "行動";
$MESS["SPS_FIELD_CODE"] = "符號代碼";
$MESS["SPS_FIELD_DETAIL_PICTURE"] = "完整的圖像";
$MESS["SPS_FIELD_DETAIL_TEXT"] = "詳細說明";
$MESS["SPS_FIELD_PREVIEW_PICTURE"] = "預覽圖像";
$MESS["SPS_FIELD_PREVIEW_TEXT"] = "預覽文本";
$MESS["SPS_FIELD_SHOW_COUNTER"] = "印象";
$MESS["SPS_FIELD_SHOW_COUNTER_START"] = "第一印像日期";
$MESS["SPS_FIELD_SORT"] = "排序";
$MESS["SPS_FIELD_XML_ID"] = "外部ID";
$MESS["SPS_GOTO_PARENT_SECTION"] = "父部分";
$MESS["SPS_ID_FROM_TO"] = "開始和結束";
$MESS["SPS_NAME"] = "姓名";
$MESS["SPS_NAV_LABEL"] = "產品";
$MESS["SPS_NO"] = "不";
$MESS["SPS_NO_CATALOGS"] = "沒有目錄可用。";
$MESS["SPS_NO_PERMS"] = "查看此目錄的權限不足。";
$MESS["SPS_OFFER"] = "sku";
$MESS["SPS_PRODUCT_ACTIVE"] = "是的";
$MESS["SPS_PRODUCT_NO_ACTIVE"] = "不";
$MESS["SPS_QUANTITY"] = "數量";
$MESS["SPS_ROOT_NAME"] = "根";
$MESS["SPS_SEARCH_PLACEHOLDER"] = "開始鍵入搜索";
$MESS["SPS_SELECT"] = "選擇";
$MESS["SPS_SKU_HIDE"] = "隱藏";
$MESS["SPS_SKU_SHOW"] = "擴張";
$MESS["SPS_TIMESTAMP"] = "修改";
$MESS["SPS_VALUE_ANY"] = "任何";
$MESS["SPS_XML_ID"] = "外部ID";
$MESS["SPS_YES"] = "是的";
