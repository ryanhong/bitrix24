<?php
$MESS["CP_CSC_TPL_TEMPLATE_THEME"] = "顏色主題";
$MESS["CP_CSC_TPL_THEME_BLUE"] = "藍色（默認主題）";
$MESS["CP_CSC_TPL_THEME_GREEN"] = "綠色的";
$MESS["CP_CSC_TPL_THEME_RED"] = "紅色的";
$MESS["CP_CSC_TPL_THEME_SITE"] = "使用站點主題（用於bitrix.shop）";
$MESS["CP_CSC_TPL_THEME_YELLOW"] = "黃色的";
$MESS["TEMPLATE_THEME_TIP"] = "定義用於渲染網站文本和圖形的顏色。藍色主題是默認選項。";
