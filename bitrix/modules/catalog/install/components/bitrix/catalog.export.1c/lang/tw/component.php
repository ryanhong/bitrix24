<?php
$MESS["CC_BCE1_ELEMENTS_PER_STEP"] = "在一個步驟中導出的元素（0-一次導出）";
$MESS["CC_BCE1_ERROR_AUTHORIZE"] = "授權錯誤：登錄或密碼不正確。";
$MESS["CC_BCE1_ERROR_CATALOG_MODULE"] = "未安裝商業目錄模塊。";
$MESS["CC_BCE1_ERROR_IBLOCK_MODULE"] = "信息塊模塊未安裝。";
$MESS["CC_BCE1_ERROR_INIT"] = "錯誤初始化導出過程。";
$MESS["CC_BCE1_ERROR_SESSION_ID_CHANGE"] = "會話ID更改處於活動狀態。編輯包含交換組件的文件，並在包含Prolog代碼之前定義BX_SESSION_ID_CHANGE常數：define（'bx_session_id_change'，false）;";
$MESS["CC_BCE1_ERROR_UNKNOWN_COMMAND"] = "未知的命令。";
$MESS["CC_BCE1_IBLOCK_ID"] = "目錄信息塊";
$MESS["CC_BCE1_INTERVAL"] = "步驟持續時間，秒。 （0-進口一步）";
$MESS["CC_BCE1_PERMISSION_DENIED"] = "您沒有足夠的權限來導入目錄。請檢查導入組件設置。";
$MESS["CC_BCE1_PROGRESS_OFFERS"] = "＃總＃sku's的＃count＃。";
$MESS["CC_BCE1_PROGRESS_PRODUCT"] = "＃總＃產品的導出＃計數＃。";
$MESS["CC_BCE1_USE_ZIP"] = "盡可能使用ZIP壓縮";
