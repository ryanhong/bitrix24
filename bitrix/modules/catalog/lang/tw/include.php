<?php
$MESS["BX_CATALOG_PRODUCT_BASKET_ERR_EMPTY_QUANTITY"] = "未指定要添加到購物車的產品數量。";
$MESS["BX_CATALOG_PRODUCT_BASKET_ERR_NO_PRODUCT"] = "找不到產品。";
$MESS["CATALOG_ERR_CANNOT_ADD_SKU"] = "您不能添加帶有多個SKU的產品。只能將特定的SKU添加到購物車中。";
$MESS["CATALOG_ERR_CANNOT_SUBSCRIBE_SKU"] = "您無法訂閱具有多個SKU的產品。只能訂閱特定的SKU。";
$MESS["CATALOG_ERR_EMPTY_PRODUCT_ID"] = "產品ID缺失。";
$MESS["CATALOG_ERR_NO_IBLOCK_ELEMENT"] = "找不到信息塊元素。";
$MESS["CATALOG_ERR_NO_PRODUCT"] = "找不到產品。";
$MESS["CATALOG_ERR_NO_PRODUCT_SET"] = "找不到捆綁物內容。";
$MESS["CATALOG_ERR_NO_PRODUCT_SET_ITEM"] = "找不到捆綁產品。";
$MESS["CATALOG_ERR_NO_SALE_MODULE"] = "沒有安裝電子商店模塊。";
$MESS["CATALOG_ERR_NO_SUBSCRIBE"] = "該產品不能訂閱。";
$MESS["CATALOG_ERR_PRODUCT_BAD_TYPE"] = "產品類型不正確。";
$MESS["CATALOG_ERR_PRODUCT_MEASURE_RATIO_NOT_FOUND"] = "找不到測量的產品單位。";
$MESS["CATALOG_ERR_PRODUCT_RUN_OUT"] = "該產品缺貨。";
$MESS["CATALOG_ERR_SESS_SEARCHER"] = "搜索系統不能是買家。";
$MESS["CATALOG_PRODUCT_PRICE_NOT_FOUND"] = "找不到產品價格";
$MESS["CATALOG_PRODUCT_PRICE_TYPE_NOT_FOUND"] = "沒有發現產品價格類型。";
$MESS["CAT_ERROR_CURRENCY_NOT_INSTALLED"] = "錯誤！貨幣模塊未安裝。";
$MESS["CAT_ERROR_IBLOCK_NOT_INSTALLED"] = "錯誤！信息塊模塊未安裝。";
$MESS["CAT_INCLUDE_CURRENCY"] = "商業目錄模塊需要安裝貨幣模塊。";
$MESS["CAT_VAT_REF_NOT_SELECTED"] = " - - 未選中的  - -";
$MESS["I_CATALOG_NOT_SUBSCR"] = "目錄號。 ＃id＃不支持訂閱";
$MESS["I_NO_IBLOCK_ELEM"] = "信息塊元素號。 ＃ID＃找不到";
$MESS["I_NO_PRODUCT"] = "產品號。 ＃ID＃找不到";
$MESS["I_NO_TRIAL_PRODUCT"] = "產品號。 ＃沒有找到ID＃（用於試用號碼＃trial_id＃）";
$MESS["I_PRODUCT_NOT_SUBSCR"] = "產品號。 ＃id＃不支持訂閱";
$MESS["I_PRODUCT_SOLD"] = "產品號。 ＃id＃沒收了";
