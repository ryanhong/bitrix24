<?php
$MESS["BT_CAT_TOOLS_GEN_CPN_ERR_AUTH"] = "您沒有權限";
$MESS["BT_CAT_TOOLS_GEN_CPN_ERR_RIGHTS"] = "管理折扣的權限不足。";
$MESS["BT_CAT_TOOLS_GEN_CPN_ERR_SESSION"] = "您的會議已經過期。請再次授權。";
$MESS["BT_CAT_TOOLS_GEN_CPN_ERR_USER"] = "無法定義用戶。";
