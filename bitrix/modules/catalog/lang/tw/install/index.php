<?php
$MESS["CATALOG_INSTALL_BACK"] = "返回模塊管理";
$MESS["CATALOG_INSTALL_COMPLETE_ERROR"] = "安裝完成了錯誤";
$MESS["CATALOG_INSTALL_COMPLETE_OK"] = "安裝完成。請參閱“幫助”部分以獲取更多信息。";
$MESS["CATALOG_INSTALL_DESCRIPTION"] = "商業目錄模塊用於創建使用價格，價格調整（折扣）以及數據導入和出口功能的商品和服務的購物目錄。沒有信息塊模塊，商業目錄模塊將無法運行。";
$MESS["CATALOG_INSTALL_DESCRIPTION2"] = "商業目錄模塊是創建產品目錄，價格，加價和折扣的理想選擇；以及出口和導入數據。";
$MESS["CATALOG_INSTALL_ERROR"] = "安裝錯誤";
$MESS["CATALOG_INSTALL_NAME"] = "商業目錄";
$MESS["CATALOG_INSTALL_PUBLIC_DIR"] = "公共文件夾";
$MESS["CATALOG_INSTALL_PUBLIC_SETUP"] = "安裝";
$MESS["CATALOG_INSTALL_SETUP"] = "安裝";
$MESS["CATALOG_INSTALL_TITLE"] = "商業目錄模塊安裝";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_DESC"] = "
＃token＃ - 確認代碼
＃token_url＃ - 確認代碼鏈接
＃list_subscribes＃ - 可用訂閱
";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_MESSAGE"] = "
此消息是從＃site_name＃發送的
-------------------------------------------------- -------------------

你好，

您正在收到此消息，因為您的電子郵件已指定
要訪問＃server_name＃可用的訂閱。

您的訂閱確認代碼：＃token＃

單擊此鏈接以訪問您的訂閱：
＃token_url＃

另外，您可以在此處手動輸入確認代碼：
＃list_subscribes＃

-------------------------------------------------- --------------------------- ----------------------- ----
此消息包含身份驗證詳細信息。
使用確認代碼訪問您的訂閱。
-------------------------------------------------- --------------------------- ----------------------- ----

此消息是由機器人發送的。";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_NAME"] = "驗證碼";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_SUBJECT"] = "＃site_name＃：確認代碼";
$MESS["CATALOG_UNINSTALL_COMPLETE"] = "卸載完成。";
$MESS["CATALOG_UNINSTALL_DEL"] = "解除安裝";
$MESS["CATALOG_UNINSTALL_ERROR"] = "卸載錯誤：";
$MESS["CATALOG_UNINSTALL_SAVECURRENCY"] = "保存貨幣表（商業目錄和電子商店模塊都使用）";
$MESS["CATALOG_UNINSTALL_SAVEDATA"] = "要保存存儲在數據庫表中的數據，請檢查“保存表”。複選框。";
$MESS["CATALOG_UNINSTALL_SAVETABLE"] = "保存表";
$MESS["CATALOG_UNINSTALL_WARNING"] = "警告！該模塊將從系統中卸載。";
$MESS["CATALOG_UNINS_CURRENCY"] = "商業目錄模塊需要安裝貨幣模塊。<br />首先安裝貨幣模塊。";
$MESS["CATALOG_UNINS_IBLOCK"] = "\“ Catalog \”模塊需要安裝\“信息塊\”模塊。";
