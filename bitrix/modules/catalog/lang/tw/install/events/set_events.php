<?php
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_CONFIRM_DESC"] = "
＃token＃ - 確認代碼
＃token_url＃ - 確認代碼鏈接
＃list_subscribes＃ - 可用訂閱
";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_CONFIRM_HTML_SUB_TITLE"] = "親愛的＃user_name＃！";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_CONFIRM_HTML_TEXT"] = "
您之所以收到此消息，是因為您的電子郵件地址用於請求確認代碼以訪問＃server_name＃上可用的訂閱。 <br> <br>
您的確認代碼：＃token＃<br> <br>
要訪問訂閱，請點擊以下鏈接：＃token_url＃<br> <br>
您也可以在此處手動輸入代碼：＃list_subscribes＃<br> <br>
此消息包含身份驗證信息。<br>
使用確認代碼訪問您的訂閱。<br>
此消息是由機器人發送的，不要回复。<br> <br>
謝謝你和我們在一起！<br>
";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_CONFIRM_HTML_TITLE"] = "來自＃site_name＃的通知";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_CONFIRM_NAME"] = "驗證碼";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_CONFIRM_SUBJECT"] = "＃site_name＃：確認代碼";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_DESC"] = "＃user_name＃ - 用戶名
＃email_to＃ - 用戶電子郵件
＃名稱＃-產品名稱
＃page_url＃ - 產品詳細信息頁面
＃Checkout_url＃ - 將產品添加到購物車
＃product_id＃ - 用於鏈接中的產品ID
";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_HTML_SUB_TITLE"] = "親愛的＃user_name＃！";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_HTML_TEXT"] = "
\“＃名稱＃\”（＃page_url＃）現在又回來了。<br> <br>
您要求我們讓您知道產品何時可用。<br> <br>
它現在是庫存的，您現在可以立即訂購：（＃checkout_url＃）<br> <br>
不要回复此消息。<br> <br>
謝謝你和我們在一起！<br>
";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_HTML_TITLE"] = "產品回到＃site_name＃的庫存庫存";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_NAME"] = "返回庫存通知";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_REPEATED_DESC"] = "＃user_name＃ - 用戶名
＃email_to＃ - 用戶電子郵件
＃名稱＃-產品名稱
＃page_url＃ - 產品詳細信息頁面
＃product_id＃ - 用於鏈接中的產品ID
";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_REPEATED_HTML_SUB_TITLE"] = "親愛的＃user_name＃！";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_REPEATED_HTML_TEXT"] = "
不幸的是，\“＃名稱＃\”（＃page_url＃）再次售罄。<b​​r> <br> <br>
我們會在回到庫存的時候讓您知道。<br> <br>
不要回复此消息。<br> <br>
謝謝你和我們在一起！<br>
";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_REPEATED_HTML_TITLE"] = "返回庫存通知：＃site_name＃";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_REPEATED_NAME"] = "返回庫存通知";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_REPEATED_SUBJECT"] = "返回庫存通知：＃site_name＃";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_SUBJECT"] = "＃site_name＃：產品又回來了";
$MESS["SMAIL_FOOTER_BR"] = "此致，
行政。";
$MESS["SMAIL_FOOTER_SHOP"] = "網上商店";
$MESS["SMAIL_UNSUBSCRIBE"] = "退訂";
