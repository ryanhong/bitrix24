<?php
$MESS["IB1C_ERROR_CATALOG"] = "錯誤導入目錄";
$MESS["IB1C_ERROR_CATEGORY"] = "錯誤導入組";
$MESS["IB1C_ERROR_DATA_LOAD"] = "無數據導入。";
$MESS["IB1C_ERROR_IBTYPE"] = "無法導入目錄，因為沒有定義的信息塊類型。";
$MESS["IB1C_ERROR_PRODUCT"] = "錯誤導入商品項目";
$MESS["IB1C_ERROR_PROPERTY"] = "錯誤導入屬性";
$MESS["IB1C_ERROR_WRONG_DATA"] = "無效的文件格式。導入失敗。";
