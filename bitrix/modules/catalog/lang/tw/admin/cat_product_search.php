<?php
$MESS["SPS_ACT"] = "積極的";
$MESS["SPS_ACTIVE"] = "積極的";
$MESS["SPS_ANY"] = "任何";
$MESS["SPS_CATALOG"] = "目錄";
$MESS["SPS_CHANGER"] = "修改";
$MESS["SPS_CLOSE"] = "關閉";
$MESS["SPS_DESCR"] = "描述";
$MESS["SPS_ID_FROM_TO"] = "開始和結束";
$MESS["SPS_INCLUDING_SUBS"] = "包括小節";
$MESS["SPS_NAME"] = "姓名";
$MESS["SPS_NO"] = "不";
$MESS["SPS_NO_PERMS"] = "您沒有足夠的權限來查看此目錄";
$MESS["SPS_SEARCH_TITLE"] = "產品";
$MESS["SPS_SECTION"] = "部分";
$MESS["SPS_SELECT"] = "選擇";
$MESS["SPS_SET"] = "放";
$MESS["SPS_STATUS"] = "地位";
$MESS["SPS_TIMESTAMP"] = "修改的";
$MESS["SPS_TOP_LEVEL"] = "頂層";
$MESS["SPS_UNSET"] = "重置";
$MESS["SPS_YES"] = "是的";
$MESS["prod_search_cancel"] = "取消";
$MESS["prod_search_cancel_title"] = "顯示所有記錄";
$MESS["prod_search_find"] = "設置過濾器";
$MESS["prod_search_find_title"] = "選擇匹配記錄";
$MESS["sale_prod_search_nav"] = "商品";
