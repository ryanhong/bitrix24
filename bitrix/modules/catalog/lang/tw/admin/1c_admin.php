<?php
$MESS["CAT_1C_CATALOG_MODULE_IS_EMPTY"] = "未安裝\“商業目錄\”模塊。";
$MESS["CAT_1C_CREATE"] = "如果需要，請創建";
$MESS["CAT_1C_CURRENT"] = "當前的";
$MESS["CAT_1C_DEACTIVATE"] = "停用";
$MESS["CAT_1C_DELETE"] = "刪除";
$MESS["CAT_1C_DETAIL_HEIGHT"] = "細節圖片的最大高度";
$MESS["CAT_1C_DETAIL_RESIZE"] = "調整細節圖片";
$MESS["CAT_1C_DETAIL_WIDTH"] = "細節圖片的最大寬度";
$MESS["CAT_1C_DISABLE_CHANGE_PRICE_NAME"] = "如果使用外部ID（XML_ID），請勿更改價格類型名稱";
$MESS["CAT_1C_ELEMENT_ACTION_2"] = "在導入文件中發現的現有項目執行的措施";
$MESS["CAT_1C_EXTENDED_SETTINGS"] = "擴展參數";
$MESS["CAT_1C_FILE_SIZE_LIMIT"] = "上載文件塊的大小，字節";
$MESS["CAT_1C_FORCE_OFFERS_2"] = "將價格保存到SKU信息塊";
$MESS["CAT_1C_GENERATE_PREVIEW"] = "生成預覽圖片";
$MESS["CAT_1C_GROUP_PERMISSIONS"] = "允許上傳的用戶組";
$MESS["CAT_1C_IBLOCK_CACHE_MODE"] = "標記的信息塊緩存";
$MESS["CAT_1C_IBLOCK_TYPE"] = "信息塊類型";
$MESS["CAT_1C_INTERVAL"] = "步驟持續時間，秒。 （0-一步出口）";
$MESS["CAT_1C_MESS_ONLY_BASE_1C_MODULE"] = "僅在使用內置1C交換模塊時，此選項才有效。";
$MESS["CAT_1C_NONE"] = "離開";
$MESS["CAT_1C_PICTURE"] = "預覽圖像";
$MESS["CAT_1C_PREVIEW_HEIGHT"] = "預覽圖片的最大高度";
$MESS["CAT_1C_PREVIEW_WIDTH"] = "預覽圖片的最大寬度";
$MESS["CAT_1C_SECTION_ACTION_2"] = "在導入文件中未發現的現有組執行的措施";
$MESS["CAT_1C_SETTINGS_SAVE_DENIED"] = "您需要將內核（MAIN）模塊的“編輯php Code \”訪問權限（edit_php）在此選項卡上編輯參數。";
$MESS["CAT_1C_SITE_LIST"] = "將新信息塊綁定到網站";
$MESS["CAT_1C_SKIP_ROOT_SECTION_2"] = "不要導入孤兒根組節點";
$MESS["CAT_1C_TRANSLIT_ON_ADD_2"] = "從產品或產品組名稱中得出符號代碼（如有必要，請使用音譯）";
$MESS["CAT_1C_TRANSLIT_ON_UPDATE_2"] = "更新時從產品或產品組名稱中得出符號代碼（必要時使用音譯）";
$MESS["CAT_1C_TRANSLIT_REPLACE_CHAR"] = "用";
$MESS["CAT_1C_USE_CRC"] = "使用元素校驗和優化目錄更新";
$MESS["CAT_1C_USE_IBLOCK_PICTURE_SETTINGS"] = "使用信息塊設置進行圖像處理";
$MESS["CAT_1C_USE_IBLOCK_TYPE_ID"] = "出口尊重信息塊類型";
$MESS["CAT_1C_USE_OFFERS_2"] = "將SKU的（屬性）導入單獨的信息塊";
$MESS["CAT_1C_USE_ZIP"] = "如果可用的話，請使用ZIP壓縮";
