<?php
$MESS["CAT_1CE_ELEMENTS_PER_STEP"] = "在一個步驟中導出的元素（0-一次導出）";
$MESS["CAT_1CE_GROUP_PERMISSIONS"] = "允許出口";
$MESS["CAT_1CE_IBLOCK_ID"] = "目錄信息塊";
$MESS["CAT_1CE_IBLOCK_ID_EMPTY"] = "未選中的";
$MESS["CAT_1CE_INTERVAL"] = "進口步驟持續時間，秒。 （0-一次導入）";
$MESS["CAT_1CE_SETTINGS_SAVE_DENIED"] = "您需要將內核（MAIN）模塊的“編輯php Code \”訪問權限（edit_php）在此選項卡上編輯參數。";
$MESS["CAT_1CE_USE_ZIP"] = "盡可能使用ZIP壓縮";
$MESS["CAT_CATALOG_MODULE_IS_EMPTY"] = "未安裝商業目錄模塊。";
