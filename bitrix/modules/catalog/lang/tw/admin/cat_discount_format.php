<?php
$MESS["CAT_DISC_CONVERT_BUTTON"] = "創建索引";
$MESS["CAT_DISC_CONVERT_COMPLETE"] = "創建了索引。";
$MESS["CAT_DISC_CONVERT_ERR"] = "創建索引時發現的錯誤：";
$MESS["CAT_DISC_CONVERT_ERROR_RESUME"] = "請更正這些折扣的申請條件。";
$MESS["CAT_DISC_CONVERT_IN_PROGRESS"] = "索引...";
$MESS["CAT_DISC_CONVERT_ONE_ERROR"] = "折扣<a href= \“#link# \”target= \"_blank \">＃名稱＃</a> <br> <br>＃Mess＃";
$MESS["CAT_DISC_CONVERT_RESULT"] = "折扣索引：<b> #count＃</b>";
$MESS["CAT_DISC_CONVERT_STEP"] = "索引步驟：";
$MESS["CAT_DISC_CONVERT_STEP_SEC"] = "秒";
$MESS["CAT_DISC_CONVERT_STOP"] = "停止";
$MESS["CAT_DISC_CONVERT_TAB"] = "索引";
$MESS["CAT_DISC_CONVERT_TAB_TITLE"] = "索引參數";
$MESS["CAT_DISC_CONVERT_TITLE"] = "折扣索引";
$MESS["CAT_DISC_CONVERT_TOTAL"] = "折扣索引：<b> #count＃</b>（＃％＃％，在＃時間＃）";
$MESS["CAT_DISC_CONVERT_TOTAL_MIN"] = "＃最小＃最低。";
$MESS["CAT_DISC_CONVERT_TOTAL_SEC"] = "＃秒＃秒。";
$MESS["CAT_DISC_FATAL_ERR"] = "無法創建臨時表。";
$MESS["ICAT_DISC_CONVERT_COMPLETE_ALL_OK"] = "所有折扣均已索引";
