<?php
$MESS["CAT_DISC_CONVERT_BUTTON"] = "轉變";
$MESS["CAT_DISC_CONVERT_COMPLETE"] = "轉換已經完成。";
$MESS["CAT_DISC_CONVERT_ERR"] = "在轉換過程中發生了一些錯誤：";
$MESS["CAT_DISC_CONVERT_ERROR_RESUME"] = "請檢查並糾正折扣條件";
$MESS["CAT_DISC_CONVERT_IN_PROGRESS"] = "轉換...";
$MESS["CAT_DISC_CONVERT_ONE_ERROR"] = "折扣<a href= \“#link# \”target= \"_blank \">＃名稱＃</a> <br> <br>＃Mess＃";
$MESS["CAT_DISC_CONVERT_RESULT"] = "轉換的折扣：<b> #count＃</b>";
$MESS["CAT_DISC_CONVERT_STEP"] = "最大轉換步驟時間：";
$MESS["CAT_DISC_CONVERT_STEP_SEC"] = "秒";
$MESS["CAT_DISC_CONVERT_STOP"] = "停止";
$MESS["CAT_DISC_CONVERT_TAB"] = "轉變";
$MESS["CAT_DISC_CONVERT_TAB_TITLE"] = "轉換參數";
$MESS["CAT_DISC_CONVERT_TITLE"] = "轉換折扣";
$MESS["CAT_DISC_CONVERT_TOTAL"] = "轉換的折扣：<b> #count＃</b>（＃％＃％，Time Est。：＃Time＃）";
$MESS["CAT_DISC_CONVERT_TOTAL_MIN"] = "＃最小＃最低。";
$MESS["CAT_DISC_CONVERT_TOTAL_SEC"] = "＃秒＃秒。";
$MESS["ICAT_DISC_CONVERT_COMPLETE_ALL_OK"] = "所有折扣都已轉換。";
