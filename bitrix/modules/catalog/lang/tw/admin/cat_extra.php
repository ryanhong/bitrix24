<?php
$MESS["CEN_ADD_NEW"] = "新標記";
$MESS["CEN_ADD_NEW_ALT"] = "單擊以添加新標記";
$MESS["CEN_DELETE_ALT"] = "刪除";
$MESS["CEN_DELETE_CONF"] = "您確定要刪除此標記嗎？";
$MESS["CEN_ERROR_UPDATE"] = "錯誤更新標記參數";
$MESS["CEN_UPDATE_ALT"] = "編輯";
$MESS["CEN_VIEW_ALT"] = "看法";
$MESS["EXTRA_ACTIONS"] = "刪除";
$MESS["EXTRA_DELETE_ERROR"] = "刪除標記時出錯";
$MESS["EXTRA_NAME"] = "標記";
$MESS["EXTRA_NOTES"] = "價格被計算為基本價格<b> plus < / b>基本價格的給定百分比。<br> [price] = [基本價格] *（1 + [百分比] / 100）= [基本價格] + [基本價格] * [百分比] / 100";
$MESS["EXTRA_PERCENTAGE"] = "百分";
$MESS["EXTRA_RECALCULATE"] = "重新計算價格";
$MESS["EXTRA_TITLE"] = "標記";
$MESS["EXTRA_UPD"] = "節省";
$MESS["cat_extra_nav"] = "標記";
