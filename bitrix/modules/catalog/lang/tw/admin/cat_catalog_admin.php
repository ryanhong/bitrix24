<?php
$MESS["CAT_CADM_ACTIVE"] = "積極的";
$MESS["CAT_CADM_CATALOG_MODULE_IS_MISSING"] = "使用商業目錄模塊的錯誤。";
$MESS["CAT_CADM_IBLOCK_MODULE_IS_MISSING"] = "使用信息塊模塊的錯誤。";
$MESS["CAT_CADM_ID"] = "ID";
$MESS["CAT_CADM_LANG"] = "網站";
$MESS["CAT_CADM_NAME"] = "姓名";
$MESS["CAT_CADM_SORT"] = "種類";
$MESS["CAT_CADM_TITLE"] = "產品目錄";
