<?php
$MESS["BX_MOD_CATALOG_ADMIN_CIS_BAD_IBLOCK_TYPE_ID"] = "無效的信息塊類型";
$MESS["BX_MOD_CATALOG_ADMIN_CIS_HEAD_ACTIVE"] = "積極的";
$MESS["BX_MOD_CATALOG_ADMIN_CIS_HEAD_CODE"] = "符號代碼";
$MESS["BX_MOD_CATALOG_ADMIN_CIS_HEAD_IBLOCK_TYPE_ID"] = "信息塊類型";
$MESS["BX_MOD_CATALOG_ADMIN_CIS_HEAD_ID"] = "ID";
$MESS["BX_MOD_CATALOG_ADMIN_CIS_HEAD_NAME"] = "姓名";
$MESS["BX_MOD_CATALOG_ADMIN_CIS_HEAD_XML_ID"] = "外部ID";
$MESS["BX_MOD_CATALOG_ADMIN_CIS_NAV"] = "信息塊";
$MESS["BX_MOD_CATALOG_ADMIN_CIS_SELECT"] = "選擇";
$MESS["BX_MOD_CATALOG_ADMIN_CIS_TITLE"] = "選擇信息塊";
