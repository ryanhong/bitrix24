<?php
$MESS["CES_ACTIONS"] = "動作";
$MESS["CES_ADD_PROFILE"] = "添加配置文件";
$MESS["CES_ADD_PROFILE_DESCR"] = "添加新的導入配置文件";
$MESS["CES_AUTO_CRON"] = "自動設置：";
$MESS["CES_AUTO_CRON_DEL"] = "自動刪除：";
$MESS["CES_CLOSE"] = "關閉";
$MESS["CES_COPY_PROFILE"] = "複製";
$MESS["CES_COPY_PROFILE_ERR_DEFAULT"] = "無法複製默認配置文件。";
$MESS["CES_COPY_PROPFILE_DESCR"] = "複製個人資料";
$MESS["CES_CREATED_BY"] = "由...製作";
$MESS["CES_CRON_AGENT_ERRORS"] = "配置文件設置為使用代理和cron運行。不建議同時使用兩者。";
$MESS["CES_DATE_CREATE"] = "創建於";
$MESS["CES_DEFAULT"] = "預設";
$MESS["CES_DEFAULT_PROFILE"] = "系統";
$MESS["CES_DELETE"] = "刪除";
$MESS["CES_DELETE_PROFILE"] = "刪除配置文件";
$MESS["CES_DELETE_PROFILE_CONF"] = "您確定要刪除此個人資料嗎？";
$MESS["CES_DELETE_PROFILE_DESCR"] = "刪除此配置文件";
$MESS["CES_EDIT_PROFILE"] = "編輯";
$MESS["CES_EDIT_PROFILE_ERR_DEFAULT"] = "默認配置文件無法修改。";
$MESS["CES_EDIT_PROFILE_ERR_ID_ABSENT"] = "配置文件ID未指定。";
$MESS["CES_EDIT_PROFILE_ERR_ID_BAD"] = "不良的配置文件ID。";
$MESS["CES_EDIT_PROPFILE_DESCR"] = "編輯個人資料";
$MESS["CES_ERRORS"] = "執行操作時錯誤：";
$MESS["CES_ERROR_ADD2CRON"] = "使用Cron安裝配置文件的錯誤：";
$MESS["CES_ERROR_ADD_PROFILE"] = "錯誤添加配置文件。";
$MESS["CES_ERROR_BAD_FILENAME"] = "導入文件名包含無效的字符。";
$MESS["CES_ERROR_BAD_FILENAME2"] = "導入腳本文件名包含無效字符。";
$MESS["CES_ERROR_COPY_PROFILE"] = "錯誤複製配置文件。";
$MESS["CES_ERROR_FILE_NOT_EXIST"] = "未找到導入文件：";
$MESS["CES_ERROR_NOT_AGENT"] = "該配置文件不能用於代理，因為默認情況下使用它，並且為當前進口商定義了設置文件。";
$MESS["CES_ERROR_NOT_CRON"] = "此配置文件不能與CRON一起使用，因為默認情況下使用它，並且為當前進口商定義了設置文件。";
$MESS["CES_ERROR_NO_ACTION"] = "沒有設置動作。";
$MESS["CES_ERROR_NO_FILE"] = "未設置導入文件。";
$MESS["CES_ERROR_NO_PROFILE1"] = "輪廓 ＃";
$MESS["CES_ERROR_NO_PROFILE2"] = "找不到。";
$MESS["CES_ERROR_NO_SETUP_FILE"] = "未找到導入設置文件。";
$MESS["CES_ERROR_PROFILE_UPDATE"] = "錯誤更新配置文件。";
$MESS["CES_ERROR_SAVE_PROFILE"] = "錯誤保存導入配置文件。";
$MESS["CES_ERROR_UNKNOWN"] = "未知錯誤。";
$MESS["CES_IMPORTER"] = "進口商";
$MESS["CES_IMPORT_FILE"] = "導入數據文件：";
$MESS["CES_IN_AGENT"] = "在代理商中";
$MESS["CES_IN_CRON"] = "在克朗上";
$MESS["CES_IN_MENU"] = "在菜單中";
$MESS["CES_MODIFIED_BY"] = "修改";
$MESS["CES_NEED_EDIT"] = "個人資料需要配置";
$MESS["CES_NO"] = "不";
$MESS["CES_NOTES1"] = "代理是PHP函數，以給定的間隔定期運行。每次請求頁面時，系統都會自動檢查需要執行並運行它們的代理。不建議將冗長或大型進口工作分配給代理商。您應該將Cron守護程序用於此目的。";
$MESS["CES_NOTES2"] = "Cron守護程序僅在基於UNIX的服務器上可用。";
$MESS["CES_NOTES3"] = "Cron守護程序在背景模式下工作，並在指定的時間運行分配的任務。您需要指定配置文件以將導入操作添加到任務列表";
$MESS["CES_NOTES4"] = "在克朗。該文件包含導入操作的指​​令。更改CRON任務集後，您必須再次安裝配置文件。";
$MESS["CES_NOTES5"] = "要設置配置文件，您必須通過SSH或SSH2或提供商為Shell Remote操作支持的任何其他類似協議連接到網站。在命令行中，運行命令";
$MESS["CES_NOTES6"] = "要查看當前安裝任務的列表，請運行命令";
$MESS["CES_NOTES7"] = "要刪除分配給cron的所有任務，請運行命令";
$MESS["CES_NOTES8"] = "Cron任務的當前列表：";
$MESS["CES_NOTES10"] = "注意力！這還將刪除未在配置文件中的任何任務。";
$MESS["CES_NOTES11_EXT"] = "該文件充當基於CRON的任務執行的外殼：<br> <b> #file＃</b>（相對於站點root）。";
$MESS["CES_NOTES12_EXT"] = "確保文件包含正確的php和站點root路徑（<b> \ $ _ server ['document_root'] </b>），並且網站ID（<b> site_id </b>常數需要在包括序言）。";
$MESS["CES_NOTES13_EXT"] = "如果指定路徑上不存在<b> cron_frame.php </b>，則必須從<b> #folder＃</b>文件夾（相對於站點root）複製它。";
$MESS["CES_OR"] = "或者";
$MESS["CES_PHP_PATH"] = "通往PHP的路徑：";
$MESS["CES_PROFILE"] = "輪廓";
$MESS["CES_RUN_IMPORT"] = "進口";
$MESS["CES_RUN_IMPORT_DESCR"] = "啟動數據導入";
$MESS["CES_RUN_INTERVAL"] = "啟動之間（小時）之間的時期：";
$MESS["CES_RUN_TIME"] = "發射時間：";
$MESS["CES_SET"] = "安裝";
$MESS["CES_SHOW_VARS_LIST"] = "變量列表";
$MESS["CES_SHOW_VARS_LIST_DESCR"] = "顯示此導入配置文件的變量列表";
$MESS["CES_SUCCESS"] = "操作成功完成。";
$MESS["CES_TIMESTAMP_X"] = "修改";
$MESS["CES_TO_AGENT"] = "創建代理";
$MESS["CES_TO_AGENT_DEL"] = "刪除代理";
$MESS["CES_TO_AGENT_DESCR"] = "創建自動啟動的代理";
$MESS["CES_TO_AGENT_DESCR_DEL"] = "刪除自動啟動的代理";
$MESS["CES_TO_CRON"] = "使用Cron";
$MESS["CES_TO_CRON_DEL"] = "停止克朗";
$MESS["CES_TO_CRON_DESCR"] = "使用Cron進行自動發射";
$MESS["CES_TO_CRON_DESCR_DEL"] = "從cron中取出";
$MESS["CES_TO_LEFT_MENU"] = "添加到菜單";
$MESS["CES_TO_LEFT_MENU_DEL"] = "從菜單中刪除";
$MESS["CES_TO_LEFT_MENU_DESCR"] = "在左菜單中添加菜單鏈接";
$MESS["CES_TO_LEFT_MENU_DESCR_DEL"] = "刪除菜單鏈接從左菜單";
$MESS["CES_USED"] = "最後一步";
$MESS["CES_YES"] = "是的";
$MESS["CICML_CONVERT_NO"] = "不";
$MESS["CICML_CONVERT_UTF8"] = "轉換為UTF-8";
$MESS["CICML_CONVERT_YES"] = "是的";
$MESS["CICML_STEP_AUTITLE"] = "增量導入";
$MESS["TITLE_IMPORT_PAGE"] = "導入設置";
$MESS["import_setup_begin"] = "啟動數據導入";
$MESS["import_setup_cat"] = "導入腳本位於文件夾中：";
$MESS["import_setup_file"] = "文件";
$MESS["import_setup_name"] = "姓名";
$MESS["import_setup_script"] = "導入腳本";
