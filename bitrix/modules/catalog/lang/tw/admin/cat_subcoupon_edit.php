<?php
$MESS["BT_CAT_DISC_SUBCOUPON_DISCOUNT_ID_ABSENT"] = "新優惠券沒有指定折扣。";
$MESS["BT_CAT_DISC_SUBCOUPON_ERR_COUNT_BAD"] = "未指定優惠券的數量。";
$MESS["BT_CAT_DISC_SUBCOUPON_ERR_COUPON_TYPE_BAD"] = "無效的優惠券類型";
$MESS["BT_CAT_DISC_SUBCOUPON_FIELD_COUNT"] = "優惠券";
$MESS["CDEN_TAB_DISCOUNT_MULTI"] = "參數";
$MESS["DSC_SUB_CPN_ONE_ORDER_NOTE"] = "注意力！一張一張備用優惠券分別應用於每個訂單項目，而不是對總訂單。<br>示例：客戶的售價為一張優惠券\ $ 100，並將其應用於包含三個項目的訂單：\ $ 30，\ $ 50和50和\ $ 110。優惠券將僅適用於價值\ $ 110的物品。";
$MESS["DSC_TITLE_ADD_MULTI"] = "添加優惠券";
