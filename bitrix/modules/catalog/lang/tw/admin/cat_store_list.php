<?php
$MESS["ADDRESS"] = "地址";
$MESS["BX_CATALOG_STORE_LIST_ACTION_ACTIVATE"] = "啟用";
$MESS["BX_CATALOG_STORE_LIST_ACTION_DEACTIVATE"] = "停用";
$MESS["BX_CATALOG_STORE_LIST_ACTION_SET_DEFAULT"] = "製作默認倉庫";
$MESS["BX_CATALOG_STORE_LIST_EMPTY_FILTER"] = "（不要過濾）";
$MESS["BX_CATALOG_STORE_LIST_ERR_CANNOT_ACTIVATE_STORE"] = "無法激活倉庫## ID＃";
$MESS["BX_CATALOG_STORE_LIST_ERR_CANNOT_DEACTIVATE_DEFAULT_STORE"] = "無法停用默認倉庫";
$MESS["BX_CATALOG_STORE_LIST_ERR_CANNOT_DEACTIVATE_STORE"] = "不能停用倉庫## ID＃";
$MESS["BX_CATALOG_STORE_LIST_ERR_CANNOT_DELETE_DEFAULT_STORE"] = "無法刪除默認倉庫";
$MESS["BX_CATALOG_STORE_LIST_ERR_CANNOT_DELETE_STORE"] = "無法刪除倉庫## ID＃";
$MESS["BX_CATALOG_STORE_LIST_ERR_CANNOT_SET_DEFAULT_NON_ACTIVE_STORE"] = "不活動的倉庫不能成為默認倉庫";
$MESS["BX_CATALOG_STORE_LIST_ERR_CANNOT_SET_DEFAULT_SITE_STORE"] = "默認倉庫不能屬於任何一個站點";
$MESS["BX_CATALOG_STORE_LIST_ERR_CANNOT_SET_DEFAULT_STORE_INTERNAL"] = "錯誤製作默認倉庫：＃錯誤＃";
$MESS["BX_CATALOG_STORE_LIST_FIELD_IS_DEFAULT"] = "預設";
$MESS["BX_CATALOG_STORE_LIST_FILTER_ANY_VALUE"] = "（全部）";
$MESS["BX_CATALOG_STORE_LIST_FILTER_EMPTY_SITE_ID"] = "沒有站點綁定";
$MESS["BX_CATALOG_STORE_LIST_FILTER_NO_VALUE"] = "不";
$MESS["BX_CATALOG_STORE_LIST_FILTER_YES_VALUE"] = "是的";
$MESS["BX_CATALOG_STORE_LIST_MESS_RANGE_FROM"] = "從";
$MESS["BX_CATALOG_STORE_LIST_MESS_RANGE_TO"] = "到";
$MESS["CAT_STORE_INDEX_TITLE"] = "倉庫地點清單";
$MESS["CSTORE_SORT"] = "種類";
$MESS["DATE_CREATE"] = "創建日期";
$MESS["DATE_MODIFY"] = "修改日期";
$MESS["DELETE_STORE_ALT"] = "刪除";
$MESS["DELETE_STORE_CONFIRM"] = "您確定要刪除倉庫嗎？";
$MESS["DESCRIPTION"] = "描述";
$MESS["EDIT_STORE_ALT"] = "編輯";
$MESS["GPS_N"] = "GPS緯度";
$MESS["GPS_S"] = "GPS經度";
$MESS["ISSUING_CENTER"] = "接人的地方";
$MESS["MODIFIED_BY"] = "修改";
$MESS["PHONE"] = "電話";
$MESS["SCHEDULE"] = "營業時間";
$MESS["SHIPPING_CENTER"] = "準備發貨";
$MESS["STORE_ACTIVE"] = "積極的";
$MESS["STORE_ADD_NEW"] = "添加";
$MESS["STORE_ADD_NEW_ALT"] = "添加新倉庫";
$MESS["STORE_CODE"] = "符號代碼";
$MESS["STORE_IMAGE"] = "圖像";
$MESS["STORE_SITE_ID"] = "網站";
$MESS["STORE_TITLE"] = "倉庫";
$MESS["STORE_XML_ID"] = "外部ID";
$MESS["TITLE"] = "姓名";
$MESS["USER_ID"] = "由...製作";
$MESS["group_admin_nav"] = "倉庫";
