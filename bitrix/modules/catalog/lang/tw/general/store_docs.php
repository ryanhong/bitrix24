<?php
$MESS["CAT_DOC_AND_MORE"] = "＃計數＃更多產品。";
$MESS["CAT_DOC_BARCODE_ALREADY_EXIST"] = "條形碼＃條形碼＃為＃產品＃創建的＃已經存在。";
$MESS["CAT_DOC_CONDUCT_UNCONDUCT_NOT_AVAILABLE"] = "庫存管理必須允許處理庫存對象";
$MESS["CAT_DOC_ERROR_CANCEL_STATUS"] = "交易仍在等待中。";
$MESS["CAT_DOC_ERROR_ELEMENT_IN_DOCUMENT_EXISTS"] = "產品## ID＃（＃名稱＃）當前正在使用庫存管理對象。";
$MESS["CAT_DOC_ERROR_ELEMENT_IN_DOCUMENT_EXT"] = "清除的庫存文件包含此項目。請更改文檔狀態並刪除項目。";
$MESS["CAT_DOC_ERROR_ELEMENT_IN_DOCUMENT_EXT_2"] = "該產品在倉庫數據庫中註冊。您必須將其刪除在那裡才能刪除產品。";
$MESS["CAT_DOC_ERROR_MODULE_SALE_NOT_INSTALLED"] = "未安裝在線商店模塊。";
$MESS["CAT_DOC_ERROR_SHIPMENT_NOT_FOUND"] = "找不到銷售訂單的發貨";
$MESS["CAT_DOC_ERROR_STORE_TO"] = "目的地倉庫未針對產品＃產品＃指定";
$MESS["CAT_DOC_PURCHASING_INFO_ERROR"] = "錯誤更新購買價格和貨幣。";
$MESS["CAT_DOC_SAVE_CONDUCTED_DOCUMENT"] = "無法修改處理的項目";
$MESS["CAT_DOC_STATUS_ALREADY_YES"] = "該文件已經實現。";
$MESS["CAT_DOC_WRONG_AMOUNT"] = "＃產品＃的數量不正確。";
$MESS["CAT_DOC_WRONG_BARCODE"] = "條形碼＃條形碼＃在數據庫中找不到。";
$MESS["CAT_DOC_WRONG_CONTRACTOR"] = "未指定供應商。";
$MESS["CAT_DOC_WRONG_COUNT"] = "創建的條形碼的數量與總數不符。";
$MESS["CAT_DOC_WRONG_ELEMENT_COUNT"] = "輸入至少一個產品。";
$MESS["CAT_DOC_WRONG_RESERVED_AMOUNT"] = "＃產品＃沒有足夠的保留庫存。";
$MESS["CAT_DOC_WRONG_RESPONSIBLE"] = "未指定負責人。";
$MESS["CAT_DOC_WRONG_SITE_ID"] = "指定的site_id不正確。";
$MESS["CAT_DOC_WRONG_STATUS"] = "無法刪除創建交易的文檔。";
$MESS["CAT_DOC_WRONG_STORE_BARCODE"] = "＃產品＃帶有條形碼＃條形碼＃在Warehouse＃Store＃上沒有庫存。";
$MESS["CAT_DOC_WRONG_TYPE"] = "錯誤的文檔類型。";
