<?php
$MESS["BT_MOD_CAT_DSC_CONV_ENTITY_ELEMENT_ERR"] = "不正確或不存在的商品ID：＃IDS＃";
$MESS["BT_MOD_CAT_DSC_CONV_ENTITY_IBLOCK_ERR"] = "不正確或不存在的信息塊IDS：＃IDS＃";
$MESS["BT_MOD_CAT_DSC_CONV_ENTITY_SECTION_ERR"] = "錯誤或不存在的部分IDS ID：＃IDS＃";
$MESS["BT_MOD_CAT_DSC_CONV_INACTIVE"] = "由於以下錯誤，折扣已停用：";
$MESS["BT_MOD_CAT_DSC_CONV_INVITE"] = "重要的！折扣將不活動，直到將其轉換為新格式為止。您必須轉換所有折扣才能激活它們。請在此處使用轉換器<a href= \“#link# \ \"> </a>。";
$MESS["BT_MOD_CAT_DSC_FORMAT_ERR"] = "索引創建錯誤。";
$MESS["BT_MOD_CAT_DSC_FORMAT_INVITE"] = "注意力！將產品折扣索引以提高性能。要立即創建索引，<a href= \"#link# \ \">單擊此處</a>。";
$MESS["BT_MOD_CAT_DSC_FORMAT_INVITE_WITHOUT_MODULE"] = "注意力！已經找到了商業目錄模塊的表格，但目前尚未安裝模塊本身。如果稍後將安裝此模塊，則需要對商品的折扣進行重新索引，以實現最佳功能。索引可訪問<a href= \"#link# \ \">在這裡</a>。";
