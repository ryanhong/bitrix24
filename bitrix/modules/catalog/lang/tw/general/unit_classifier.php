<?php
$MESS["CAT_UC_100_L"] = "100L";
$MESS["CAT_UC_100_YASH"] = "100 Bx。";
$MESS["CAT_UC_1000_M2"] = "1000平方米";
$MESS["CAT_UC_A"] = "A";
$MESS["CAT_UC_AREA_UNITS"] = "區域單位";
$MESS["CAT_UC_AR_100_M2"] = "是";
$MESS["CAT_UC_BOB"] = "RL。";
$MESS["CAT_UC_BOBINA"] = "捲軸";
$MESS["CAT_UC_CH"] = "人力資源";
$MESS["CAT_UC_CHAS"] = "小時";
$MESS["CAT_UC_CHAST"] = "部分";
$MESS["CAT_UC_DEK"] = "十年";
$MESS["CAT_UC_DEKADA"] = "十年";
$MESS["CAT_UC_DETCILITR"] = "十分列";
$MESS["CAT_UC_DETCIMETR"] = "十分集";
$MESS["CAT_UC_DL"] = "DL";
$MESS["CAT_UC_DM"] = "DM";
$MESS["CAT_UC_DM2"] = "DM2";
$MESS["CAT_UC_DYUJM"] = "在。";
$MESS["CAT_UC_DYUJM2"] = "in2";
$MESS["CAT_UC_DYUJM3"] = "in3";
$MESS["CAT_UC_DYUJM_25_4_MM"] = "英寸";
$MESS["CAT_UC_DYUZHINA_12_SHT"] = "打";
$MESS["CAT_UC_DYUZHINA_UPAKOVOK"] = "十二包";
$MESS["CAT_UC_ECONOMIC_UNITS"] = "財務單位";
$MESS["CAT_UC_ELEMENT"] = "元素";
$MESS["CAT_UC_ENGINEERING_UNITS"] = "工程單位";
$MESS["CAT_UC_FUT"] = "英尺";
$MESS["CAT_UC_FUT2"] = "FT2";
$MESS["CAT_UC_FUT3"] = "FT3";
$MESS["CAT_UC_FUT_0_3048_M"] = "腳";
$MESS["CAT_UC_G"] = "G";
$MESS["CAT_UC_GA"] = "哈";
$MESS["CAT_UC_GEKTAR"] = "公頃";
$MESS["CAT_UC_GEKTOLITR"] = "hectolitre";
$MESS["CAT_UC_GL"] = "GL";
$MESS["CAT_UC_GOD"] = "年";
$MESS["CAT_UC_GRADUS_SYMBOL"] = "＆deg;";
$MESS["CAT_UC_GRAMM"] = "公克";
$MESS["CAT_UC_IZD"] = "產品。";
$MESS["CAT_UC_IZDELIE"] = "產品";
$MESS["CAT_UC_KG"] = "公斤";
$MESS["CAT_UC_KILOGRAMM"] = "公斤";
$MESS["CAT_UC_KILOMETR"] = "公里";
$MESS["CAT_UC_KILOVATT"] = "千瓦";
$MESS["CAT_UC_KILOVOL_T"] = "千萬";
$MESS["CAT_UC_KM"] = "公里";
$MESS["CAT_UC_KM2"] = "KM2";
$MESS["CAT_UC_KUBICHESKIJ_DYUJM_16387_1_MM3"] = "立方英寸";
$MESS["CAT_UC_KUBICHESKIJ_FUT_0_02831685_M3"] = "立方英尺";
$MESS["CAT_UC_KUBICHESKIJ_METR"] = "立方米";
$MESS["CAT_UC_KUBICHESKIJ_MILLIMETR"] = "方形毫米";
$MESS["CAT_UC_KUBICHESKIJ_SANTIMETR"] = "方厘米";
$MESS["CAT_UC_KUBICHESKIJ_YARD_0_764555_M3"] = "立方場";
$MESS["CAT_UC_KV"] = "KV";
$MESS["CAT_UC_KVADRATNIJ_DETCIMETR"] = "正方形十分限";
$MESS["CAT_UC_KVADRATNIJ_DYUJM_645_16_MM2"] = "平方英寸";
$MESS["CAT_UC_KVADRATNIJ_FUT_0_092903_M2"] = "平方英尺";
$MESS["CAT_UC_KVADRATNIJ_KILOMETR"] = "平方公里";
$MESS["CAT_UC_KVADRATNIJ_METR"] = "平方米";
$MESS["CAT_UC_KVADRATNIJ_MILLIMETR"] = "方形毫米";
$MESS["CAT_UC_KVADRATNIJ_SANTIMETR"] = "方厘米";
$MESS["CAT_UC_KVADRATNIJ_YARD_0_8361274_M2"] = "方院";
$MESS["CAT_UC_KVART"] = "QRT。";
$MESS["CAT_UC_KVARTAL"] = "四分之一";
$MESS["CAT_UC_KVT"] = "KW";
$MESS["CAT_UC_L"] = "l";
$MESS["CAT_UC_LENGTH_UNITS"] = "長度的單位";
$MESS["CAT_UC_LIST"] = "床單";
$MESS["CAT_UC_LITR"] = "升";
$MESS["CAT_UC_M"] = "m";
$MESS["CAT_UC_M3"] = "M3";
$MESS["CAT_UC_MASS_UNITS"] = "質量單位";
$MESS["CAT_UC_MEGALITR"] = "Megalitre";
$MESS["CAT_UC_MEGAM"] = "毫米";
$MESS["CAT_UC_MEGAMETR"] = "Megametre";
$MESS["CAT_UC_MES"] = "莫。";
$MESS["CAT_UC_MESYATC"] = "月";
$MESS["CAT_UC_METR"] = "儀表";
$MESS["CAT_UC_MG"] = "毫克";
$MESS["CAT_UC_MILLIGRAMM"] = "毫克";
$MESS["CAT_UC_MILLIMETR"] = "毫米";
$MESS["CAT_UC_MIN"] = "最小";
$MESS["CAT_UC_MINUTA"] = "分鐘";
$MESS["CAT_UC_ML"] = "ML";
$MESS["CAT_UC_MM"] = "毫米";
$MESS["CAT_UC_MM2"] = "M2";
$MESS["CAT_UC_MM3"] = "MM3";
$MESS["CAT_UC_NABOR"] = "放";
$MESS["CAT_UC_NED"] = "WK。";
$MESS["CAT_UC_NEDELYA"] = "星期";
$MESS["CAT_UC_PARA_2_SHT"] = "一對（2個PC。）";
$MESS["CAT_UC_POLGODA"] = "半年";
$MESS["CAT_UC_POLUGODIE"] = "半年";
$MESS["CAT_UC_POSILKA"] = "包裹";
$MESS["CAT_UC_RULON"] = "卷";
$MESS["CAT_UC_SANTIMETR"] = "厘米";
$MESS["CAT_UC_SEKUNDA"] = "第二";
$MESS["CAT_UC_SHT"] = "件。";
$MESS["CAT_UC_SHTUKA"] = "片";
$MESS["CAT_UC_SM"] = "厘米";
$MESS["CAT_UC_SM2"] = "CM2";
$MESS["CAT_UC_SM3"] = "CM3";
$MESS["CAT_UC_STO_LISTOV"] = "一百張";
$MESS["CAT_UC_STO_SHTUK"] = "一百件";
$MESS["CAT_UC_STO_UPAKOVOK"] = "數百包";
$MESS["CAT_UC_STO_YASHIKOV"] = "數百個盒子";
$MESS["CAT_UC_SUT"] = "天";
$MESS["CAT_UC_SUTKI"] = "天";
$MESS["CAT_UC_T"] = "t";
$MESS["CAT_UC_TC"] = "QL。";
$MESS["CAT_UC_TCENTNER_METRICHESKIJ_100_KG"] = "五分之一";
$MESS["CAT_UC_TIME_UNITS"] = "時間單位";
$MESS["CAT_UC_TISYACHA_KVADRATNIH_METROV"] = "一千平方米";
$MESS["CAT_UC_TISYACHA_SHTUK"] = "一千件";
$MESS["CAT_UC_TITLE1"] = "一般測量單位";
$MESS["CAT_UC_TONNA_METRICHESKAYA_TONNA_1000_KG"] = "噸（公噸）";
$MESS["CAT_UC_UPAK"] = "PKG。";
$MESS["CAT_UC_UPAKOVKA"] = "包裹";
$MESS["CAT_UC_V"] = "v";
$MESS["CAT_UC_VATT"] = "瓦";
$MESS["CAT_UC_VOLUME_UNITS"] = "數量單位";
$MESS["CAT_UC_VOL_T"] = "伏特";
$MESS["CAT_UC_VT"] = "w";
$MESS["CAT_UC_YARD"] = "yd。";
$MESS["CAT_UC_YARD2"] = "YD2";
$MESS["CAT_UC_YARD3"] = "YD3";
$MESS["CAT_UC_YARD_0_9144_M"] = "院子";
