<?php
$MESS["BX_CATALOG_REINDEX_ERR_OFFER_PRICE_UPDATE_FAIL_EXT"] = "SKU的錯誤重新索引價格[＃id＃]＃名稱＃：＃錯誤＃。";
$MESS["BX_CATALOG_REINDEX_ERR_OFFER_UPDATE_FAIL_EXT"] = "錯誤重新索引SKU [＃id＃]＃名稱＃：＃錯誤＃。";
$MESS["BX_CATALOG_REINDEX_ERR_PRODUCT_PRICE_UPDATE_FAIL_EXT"] = "錯誤重新索引項目[＃id＃]＃名稱＃：＃錯誤＃。";
$MESS["BX_CATALOG_REINDEX_ERR_PRODUCT_UPDATE_FAIL_EXT"] = "錯誤重新索引項目[＃id＃]＃名稱＃：＃錯誤＃。";
$MESS["BX_CATALOG_REINDEX_NOTIFY"] = "注意力！要獲取正確的定價和產品信息，您必須在<a href= \"#link# \">商業目錄設置頁面上重新索引產品。";
$MESS["BX_STEP_OPERATION_CATALOG_TITLE"] = "信息塊中的產品＃名稱＃[＃id＃]";
$MESS["BX_STEP_OPERATION_OFFERS_TITLE"] = "SKU的信息塊＃名稱＃[＃ID＃]";
$MESS["BX_STEP_OPERATION_PROGRESS_TEMPLATE"] = "<p>總項目：＃全＃<br>處理：#count＃</p>";
$MESS["BX_STEP_OPERATION_SETS_TITLE"] = "捆綁";
