<?php
$MESS["CS_ALREADY_HAVE_STORE"] = "您的版本中只有一個倉庫。在創建新倉庫之前，您必須刪除現有倉庫。";
$MESS["CS_EMPTY_ADDRESS"] = "未指定倉庫地址。";
$MESS["CS_STORE_HAVE_DOCS"] = "連接到此倉庫的庫存文件可防止刪除。";
$MESS["CS_WRONG_IMG"] = "圖像無效。";
$MESS["CS_WRONG_LOC"] = "位置無效。";
