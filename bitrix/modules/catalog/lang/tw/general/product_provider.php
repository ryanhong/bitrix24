<?php
$MESS["CATALOG_ERR_NO_PRODUCT"] = "找不到產品。";
$MESS["CATALOG_ERR_NO_PRODUCT_SET"] = "找不到捆綁物內容。";
$MESS["CATALOG_ERR_NO_PRODUCT_SET_ITEM"] = "找不到捆綁產品。";
$MESS["CATALOG_ERR_SKU_PRODUCT"] = "您不能添加帶有多個SKU的產品。只能將特定的SKU添加到購物車中。";
$MESS["CATALOG_NO_QUANTITY_PRODUCT"] = "＃名稱＃沒有庫存。";
$MESS["CATALOG_QUANTITY_NOT_ENOGH"] = "您想購買＃數量＃＃MEATE_NAME＃。 ＃名稱＃;但是，只有＃catalog_quantity＃＃sues_name＃。可用。";
$MESS["DDCT_DEDUCTION_BARCODE_ERROR"] = "條形碼\“＃barcode＃\”是為產品\“＃product_name＃\”（## product_id＃）找到的。";
$MESS["DDCT_DEDUCTION_MULTI_BARCODE_EMPTY"] = "產品的條形碼\“＃product_name＃\”倉庫## store_id＃尚未指定";
$MESS["DDCT_DEDUCTION_PRODUCT_NOT_FOUND_ERROR"] = "找不到要發貨的產品（## Product_id＃）";
$MESS["DDCT_DEDUCTION_QUANTITY_ERROR"] = "運輸產品＃product_name＃（## product_id＃）的數量不足。";
$MESS["DDCT_DEDUCTION_QUANTITY_STORE_ERROR"] = "產品＃product_name＃（## product_id＃）的產品數量在倉庫## Store_id＃不足以發貨。";
$MESS["DDCT_DEDUCTION_QUANTITY_STORE_ERROR_2"] = "產品＃product_name＃（## product_id＃）的產品數量在倉庫＃store_name＃（## store_id＃）不足以發貨。";
$MESS["DDCT_DEDUCTION_SAVE_ERROR"] = "在倉庫中保存＃product_name＃（## product_id＃）的數量時，發生了一個未知錯誤。";
$MESS["DDCT_DEDUCTION_SHIPMENT_QUANTITY_NOT_ENOUGH"] = "\“＃product_name＃\”的數量超過倉庫可用的庫存";
$MESS["DDCT_DEDUCTION_STORE_ERROR"] = "倉庫未指定用於運送產品＃product_name＃（## product_id＃）。";
$MESS["DDCT_DEDUCTION_UNDO_ERROR_RESERVE_QUANTITY"] = "無法取消與實際保留更多產品項目的預訂（＃product_name＃，## product_id＃）";
$MESS["DDCT_DEDUCTION_WRITE_ERROR"] = "錯誤更新產品＃product_name＃（## product_id＃）的貨運信息。";
$MESS["DDCT_UNKNOWN_ERROR"] = "運輸產品＃product_name＃（## product_id＃）時未知錯誤。";
$MESS["RSRV_ID_NOT_FOUND"] = "未找到產品## Product_ID＃。";
$MESS["RSRV_INCORRECT_ID"] = "無效的產品ID。";
$MESS["RSRV_QUANTITY_NEGATIVE_ERROR"] = "倉庫顯示產品＃product_name＃（## product_id＃）的負數。";
$MESS["RSRV_QUANTITY_NOT_ENOUGH_ERROR"] = "產品＃product_name＃（## product_id＃）庫存不足。";
$MESS["RSRV_SKU_FOUND"] = "產品## product_id＃具有多個SKU。只能保留或釋放特定的SKU。";
$MESS["RSRV_UNKNOWN_ERROR"] = "保留產品＃product_name＃（## product_id＃）時未知錯誤。";
