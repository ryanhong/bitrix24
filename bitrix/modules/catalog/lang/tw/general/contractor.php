<?php
$MESS["CC_CONTRACTOR_HAVE_DOCS_EXT"] = "無法刪除供應商。請在刪除供應商之前刪除涉及供應商的所有倉儲文檔。";
$MESS["CC_EMPTY_ADDRESS"] = "地址為空。";
$MESS["CC_EMPTY_COMPANY"] = "請輸入公司名稱。";
$MESS["CC_WRONG_PERSON_LASTNAME"] = "請輸入供應商名稱。";
