<?php
$MESS["CATALOG_MEASURE_RATIO_BAD_ACTION"] = "指定的不正確操作（僅創建或更新）";
$MESS["CATALOG_MEASURE_RATIO_BAD_PRODUCT_ID"] = "不正確的產品ID";
$MESS["CATALOG_MEASURE_RATIO_EMPTY_CLEAR_FIELDS"] = "鍵驗證後，數據陣列為空";
$MESS["CATALOG_MEASURE_RATIO_PRODUCT_ID_IS_ABSENT"] = "產品ID未指定。";
$MESS["CATALOG_MEASURE_RATIO_RATIO_ALREADY_EXIST"] = "＃比率＃該產品的係數已經存在";
