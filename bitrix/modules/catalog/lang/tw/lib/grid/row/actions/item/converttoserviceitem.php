<?php
$MESS["CATALOG_GRID_ROW_ACTIONS_CONVERT_TO_SERVICE_BACK_BUTTON"] = "取消";
$MESS["CATALOG_GRID_ROW_ACTIONS_CONVERT_TO_SERVICE_CONFIRM_BUTTON"] = "是的，轉換";
$MESS["CATALOG_GRID_ROW_ACTIONS_CONVERT_TO_SERVICE_CONFIRM_MESSAGE_CONTENT"] = "這將從所有倉庫中刪除產品信息。當前的庫存值將不可用。";
$MESS["CATALOG_GRID_ROW_ACTIONS_CONVERT_TO_SERVICE_CONFIRM_MESSAGE_TITLE"] = "您確定要將產品轉換為服務嗎？";
$MESS["CATALOG_GRID_ROW_ACTIONS_CONVERT_TO_SERVICE_TEXT"] = "轉換為服務";
