<?php
$MESS["CATALOG_REPORT_PRODUCT_LIST_DEFAULT_STORE_NAME"] = "無標題";
$MESS["CATALOG_REPORT_PRODUCT_LIST_FILTER_PRODUCTS_TITLE"] = "產品";
$MESS["CATALOG_REPORT_PRODUCT_LIST_MEASURE_TEMPLATE"] = "＃號碼＃＃MEATE_SYMBOL＃";
$MESS["CATALOG_REPORT_PRODUCT_LIST_NO_PRODUCTS"] = "倉庫中沒有物品。";
$MESS["CATALOG_REPORT_PRODUCT_LIST_NO_READ_RIGHTS_ERROR"] = "您沒有視圖許可。";
$MESS["CATALOG_REPORT_PRODUCT_LIST_PRODUCT_FILTER_STUB"] = "開始鍵入產品名稱或SKU";
