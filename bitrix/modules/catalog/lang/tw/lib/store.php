<?php
$MESS["CATALOG_STORE_ENTITY_ADDRESS_FIELD"] = "地址";
$MESS["CATALOG_STORE_ENTITY_DESCRIPTION_FIELD"] = "描述";
$MESS["CATALOG_STORE_ENTITY_ID_FIELD"] = "ID";
$MESS["CATALOG_STORE_ENTITY_PHONE_FIELD"] = "電話";
$MESS["CATALOG_STORE_ENTITY_SCHEDULE_FIELD"] = "營業時間";
$MESS["CATALOG_STORE_ENTITY_TITLE_FIELD"] = "姓名";
$MESS["STORE_ENTITY_ACTIVE_FIELD"] = "積極的";
$MESS["STORE_ENTITY_CODE_FIELD"] = "符號代碼";
$MESS["STORE_ENTITY_DATE_CREATE_FIELD"] = "創建";
$MESS["STORE_ENTITY_DATE_MODIFY_FIELD"] = "修改的";
$MESS["STORE_ENTITY_EMAIL_FIELD"] = "電子郵件";
$MESS["STORE_ENTITY_GPS_N_FIELD"] = "GPS緯度";
$MESS["STORE_ENTITY_GPS_S_FIELD"] = "GPS經度";
$MESS["STORE_ENTITY_IMAGE_ID_FIELD"] = "圖像";
$MESS["STORE_ENTITY_ISSUING_CENTER_FIELD"] = "運輸點";
$MESS["STORE_ENTITY_IS_DEFAULT_FIELD"] = "默認倉庫";
$MESS["STORE_ENTITY_LOCATION_ID_FIELD"] = "地點";
$MESS["STORE_ENTITY_MODIFIED_BY_FIELD"] = "修改";
$MESS["STORE_ENTITY_SHIPPING_CENTER_FIELD"] = "用於運輸";
$MESS["STORE_ENTITY_SITE_ID_FIELD"] = "網站";
$MESS["STORE_ENTITY_SORT_FIELD"] = "排序ID";
$MESS["STORE_ENTITY_USER_ID_FIELD"] = "由...製作";
$MESS["STORE_ENTITY_XML_ID_FIELD"] = "XML ID";
