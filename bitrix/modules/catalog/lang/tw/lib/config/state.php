<?php
$MESS["CATALOG_STATE_ERR_PRODUCT_IN_SECTION_LIMIT"] = "一旦移動部分，在登錄頁面中使用的產品數將達到＃Count＃。您將超過著陸頁（＃limit＃）中允許的最大產品數量。";
$MESS["CATALOG_STATE_ERR_PRODUCT_LIMIT"] = "允許的最大產品數量超過了。在目錄中找到的產品 - ＃count＃（限制允許 - ＃limit＃）。";
$MESS["CATALOG_STATE_ERR_PRODUCT_LIMIT_1"] = "本節配置為在著陸頁上顯示其產品。您已經達到了當前計劃中允許的此類產品的最大數量。您可以在不同部分中創建一個產品，前提是後者與著陸頁沒有鏈接，也可以在本節中未出版的產品為新產品騰出空間。您的目錄當前包括＃Count＃可顯示產品，最多為＃limit＃。";
