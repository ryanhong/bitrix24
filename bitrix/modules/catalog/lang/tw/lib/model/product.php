<?php
$MESS["BX_CATALOG_MODEL_PRODUCT_ERR_BAD_NUMERIC_FIELD"] = "＃字段＃字段包含無效數據。";
$MESS["BX_CATALOG_MODEL_PRODUCT_ERR_BAD_PRODUCT_TYPE"] = "產品類型不正確。";
$MESS["BX_CATALOG_MODEL_PRODUCT_ERR_ELEMENT_NOT_EXISTS"] = "找不到要創建產品屬性的目標信息塊項目。";
$MESS["BX_CATALOG_MODEL_PRODUCT_ERR_QUANTITY_RESERVE_LESS_ZERO"] = "field dentity_保留小於零。";
$MESS["BX_CATALOG_MODEL_PRODUCT_ERR_SIMPLE_IBLOCK"] = "信息塊不是商業目錄模塊的一部分。";
$MESS["BX_CATALOG_MODEL_PRODUCT_ERR_WRONG_PRODUCT_ID"] = "產品ID缺失。";
$MESS["BX_CATALOG_MODEL_PRODUCT_ERR_WRONG_PURCHASING_CURRENCY"] = "購買貨幣是空的。";
