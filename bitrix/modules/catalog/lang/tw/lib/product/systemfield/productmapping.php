<?php
$MESS["PRODUCT_MAPPING_FIELD_TITLE"] = "產品可見性";
$MESS["PRODUCT_MAPPING_FIELD_TITLE_HINT"] = "使用此選項將產品顯示在在線商店的頁面上。 <a onclick = \"top.bx.helper.show('redirect=detail&code=154440280') \“style stylet = \"cursor：pointer \ \">詳細信息</a>";
$MESS["PRODUCT_MAPPING_STORAGE_TITLE"] = "產品可見性";
$MESS["PRODUCT_MAPPING_TYPE_FACEBOOK"] = "Facebook";
$MESS["PRODUCT_MAPPING_TYPE_LANDING"] = "網上商城";
$MESS["PRODUCT_MAPPING_UF_FIELD_NAME"] = "姓名";
$MESS["PRODUCT_MAPPING_UF_FIELD_XML_ID"] = "外部ID";
