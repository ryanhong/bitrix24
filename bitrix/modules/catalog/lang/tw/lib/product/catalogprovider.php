<?php
$MESS["DDCT_DEDUCTION_BARCODE_ERROR"] = "條形碼\“＃barcode＃\”是為產品\“＃product_name＃\”（## product_id＃）找到的。";
$MESS["DDCT_DEDUCTION_MULTI_BARCODE_EMPTY"] = "\“＃product_name＃\”條形碼丟失。";
$MESS["DDCT_DEDUCTION_QUANTITY_ERROR"] = "＃product_name＃（## product_id＃）的庫存不足。";
$MESS["DDCT_DEDUCTION_QUANTITY_STORE_ERROR"] = "＃product_name＃（## product_id＃）的庫存不足，用於從倉庫## store_id＃出貨。";
$MESS["DDCT_DEDUCTION_QUANTITY_STORE_ERROR_2"] = "＃product_name＃（## product_id＃）的庫存不足，用於從倉庫＃store_name＃（## store_id＃）發貨。";
$MESS["DDCT_DEDUCTION_SHIPMENT_QUANTITY_NOT_ENOUGH"] = "\“＃product_name＃\”的數量超過倉庫可用的庫存";
$MESS["DDCT_DEDUCTION_STORE_EMPTY_ERROR"] = "無法運送＃product_name＃（## product_id＃），因為它在倉庫中沒有庫存";
$MESS["DDCT_DEDUCTION_STORE_ERROR"] = "沒有選擇運送產品＃product_name＃（## product_id＃）的倉庫";
$MESS["SALE_PROVIDER_PRODUCT_NOT_AVAILABLE"] = "＃product_name＃（## product_id＃）無庫存。";
$MESS["SALE_PROVIDER_PRODUCT_SERVICE_NOT_AVAILABLE"] = "服務＃product_name＃（## product_id＃）不可用。";
