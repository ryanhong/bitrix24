<?php
$MESS["BX_CAT_ENABLE_PROCESSING_DEPRECATED_EVENTS"] = "啟用舊事件支持：";
$MESS["BX_CAT_RESERVE_CONDITION_EMPTY"] = "未分配";
$MESS["BX_CAT_SYSTEM_SETTINGS"] = "系統設置";
$MESS["CAT_AGENT_ACTIVE"] = "積極的";
$MESS["CAT_AGENT_EVENT_LOG"] = "事件日誌";
$MESS["CAT_AGENT_EVENT_LOG_SHOW_ERROR"] = "檢查出口錯誤";
$MESS["CAT_AGENT_FILEPATH"] = "下載導出的文件＃文件＃";
$MESS["CAT_AGENT_FILE_ABSENT"] = "沒有找到導出文件。";
$MESS["CAT_AGENT_LAST_EXEC"] = "最後一步";
$MESS["CAT_AGENT_NEXT_EXEC"] = "下一個運行";
$MESS["CAT_AGENT_START"] = "添加代理";
$MESS["CAT_AGENT_WAIT_START"] = "代理將很快運行。";
$MESS["CAT_ALLOW_CAN_BUY_ZERO_EXT"] = "製作庫存的物品可供購買，包括那些負數的物品";
$MESS["CAT_ALL_STORES"] = "所有倉庫";
$MESS["CAT_CATALOGS_LIST"] = "目錄";
$MESS["CAT_CLEAR_ACTION"] = "清除";
$MESS["CAT_CLEAR_QUANTITY"] = "清除\“庫存中的數量\”字段";
$MESS["CAT_CLEAR_RESERVED_QUANTITY"] = "clear \“保留數量\”字段";
$MESS["CAT_CLEAR_STORE"] = "清除倉庫庫存數量";
$MESS["CAT_COMMON_EXPIMP_SETTINGS"] = "常見參數";
$MESS["CAT_DEACT_NOPRICE"] = "停用commerceml項目沒有價格";
$MESS["CAT_DEF_IBLOCK"] = "從Commerceml導入數據塊類型：";
$MESS["CAT_DEF_OUTFILE"] = "commerceml文件中缺少的項目：";
$MESS["CAT_DEF_OUTFILE_D"] = "刪除";
$MESS["CAT_DEF_OUTFILE_F"] = "不要觸摸";
$MESS["CAT_DEF_OUTFILE_H"] = "停用";
$MESS["CAT_DEL_CATALOG1"] = "目錄";
$MESS["CAT_DEL_CATALOG2"] = "無法刪除";
$MESS["CAT_DISCOUNT"] = "折扣";
$MESS["CAT_DISCOUNT_PERCENT_FROM_BASE_PRICE"] = "折扣百分比來自全產品價格。";
$MESS["CAT_DISCOUNT_PERCENT_FROM_BASE_PRICE_NO"] = "不";
$MESS["CAT_DISCOUNT_PERCENT_FROM_BASE_PRICE_YES"] = "是的";
$MESS["CAT_DISCOUNT_PERCENT_FROM_BASE_SALE"] = "更改電子商店模塊設置頁面";
$MESS["CAT_DISCSAVE_APPLY"] = "漸進折扣方法";
$MESS["CAT_ENABLE_QUANTITY_TRACE"] = "啟用股票控制：";
$MESS["CAT_ENABLE_RESERVATION"] = "啟用預訂";
$MESS["CAT_ENABLE_RESERVATION_HINT"] = "此設置定義了所有產品的默認保留行為，其\“啟用股票控制\”選項設置為\“默認\”。";
$MESS["CAT_ENABLE_SHOW_CATALOG_TAB_WITH_OFFERS"] = "這是一個遺產選項。檢查它將禁用基於SKU的產品庫存的自動計算。取而代之的是，將根據產品數據計算產品內庫存狀態。";
$MESS["CAT_ENABLE_VIEWED_PRODUCTS"] = "為最近查看的產品保存偏好：";
$MESS["CAT_EXPORT_DEFAULT_PATH"] = "導出文件的默認路徑：";
$MESS["CAT_IBLOCK_CATALOG_SUCCESSFULLY_UPDATE"] = "目錄參數已成功保存。";
$MESS["CAT_IBLOCK_OFFERS_EMPTY"] = "未選中的";
$MESS["CAT_IBLOCK_OFFERS_ERR_CANNOT_CREATE_IBLOCK"] = "為目錄＃產品＃創建SKU信息塊的權限不足。";
$MESS["CAT_IBLOCK_OFFERS_ERR_CANNOT_CREATE_LINK"] = "發生錯誤的綁定SKU信息塊＃提供＃：＃err＃";
$MESS["CAT_IBLOCK_OFFERS_ERR_CAT_ADD"] = "錯誤添加目錄＃產品＃：＃錯誤＃";
$MESS["CAT_IBLOCK_OFFERS_ERR_CAT_UPDATE"] = "錯誤更新目錄＃產品＃：＃錯誤＃";
$MESS["CAT_IBLOCK_OFFERS_ERR_CREATE_TYPE"] = "錯誤創建目錄＃產品＃的SKU信息塊類型。";
$MESS["CAT_IBLOCK_OFFERS_ERR_IBLOCK_ADD"] = "發生錯誤，為信息塊創建SKU信息塊＃產品＃：＃err＃";
$MESS["CAT_IBLOCK_OFFERS_ERR_LINKS_MULTIPLE"] = "信息塊＃要約＃由多個屬性鏈接到目錄＃產品＃。";
$MESS["CAT_IBLOCK_OFFERS_ERR_MODIFY_DENIED"] = "您無權將屬性添加到＃要約＃。";
$MESS["CAT_IBLOCK_OFFERS_ERR_MODIFY_PROP_IS_REQ"] = "錯誤更新SKU信息塊的綁定屬性＃提供＃：＃err＃";
$MESS["CAT_IBLOCK_OFFERS_ERR_NEW_IBLOCK_TYPE_EMPTY"] = "為目錄＃產品＃指定的SKU信息塊類型是空的。";
$MESS["CAT_IBLOCK_OFFERS_ERR_NEW_IBLOCK_TYPE_NOT_ADD"] = "為目錄＃產品＃：＃錯誤＃創建新的SKU信息塊類型時發生了錯誤";
$MESS["CAT_IBLOCK_OFFERS_ERR_PRODUCT_AND_OFFERS"] = "信息塊＃產品＃不能同時是產品目錄和SKU信息塊。";
$MESS["CAT_IBLOCK_OFFERS_ERR_SELF_MADE"] = "信息塊＃產品＃不能是自身的SKU信息塊。";
$MESS["CAT_IBLOCK_OFFERS_ERR_SITELIST_DEFFERENT"] = "目錄＃產品＃的網站與SKU＃提供＃的網站不匹配。";
$MESS["CAT_IBLOCK_OFFERS_ERR_TOO_MANY_LINKS"] = "信息塊＃要約＃具有鏈接到目錄＃產品＃的多個屬性。";
$MESS["CAT_IBLOCK_OFFERS_ERR_TOO_MANY_PRODUCT_IBLOCK"] = "信息塊＃要約＃由多個目錄使用。";
$MESS["CAT_IBLOCK_OFFERS_ERR_UNLINK_SKU"] = "錯誤從＃propers＃：＃錯誤＃鏈接SKU信息塊：＃錯誤＃";
$MESS["CAT_IBLOCK_OFFERS_NAME_TEPLATE"] = "提供＃產品＃的包裹";
$MESS["CAT_IBLOCK_OFFERS_NEW"] = "新信息塊";
$MESS["CAT_IBLOCK_OFFERS_NEWTYPE"] = "ID";
$MESS["CAT_IBLOCK_OFFERS_NEW_IBTYPE"] = "創建新信息塊類型";
$MESS["CAT_IBLOCK_OFFERS_OLD_IBTYPE"] = "使用現有信息塊類型";
$MESS["CAT_IBLOCK_OFFERS_TITLE"] = "姓名";
$MESS["CAT_IBLOCK_OFFERS_TITLE_LINK_NAME"] = "目錄項目";
$MESS["CAT_IBLOCK_OFFERS_TYPE"] = "類型";
$MESS["CAT_IBLOCK_SELECT_CAT"] = "用作商業目錄";
$MESS["CAT_IBLOCK_SELECT_NAME"] = "信息塊";
$MESS["CAT_IBLOCK_SELECT_OFFERS"] = "SKU信息塊";
$MESS["CAT_IBLOCK_SELECT_VAT"] = "稅";
$MESS["CAT_IBLOCK_SELECT_YANDEX_EXPORT"] = "導出到yandex.market";
$MESS["CAT_NUM_CATALOG_LEVELS"] = "包括父母在內的最大目錄級別要出口和導入：";
$MESS["CAT_OPTIONS_BTN_HINT_RESTORE_DEFAULT"] = "恢復默認設置";
$MESS["CAT_OPTIONS_BTN_HINT_RESTORE_DEFAULT_WARNING"] = "警告！所有設置都將恢復為其默認值。你想繼續嗎？";
$MESS["CAT_OPTIONS_BTN_RESET"] = "重置";
$MESS["CAT_OPTIONS_BTN_RESTORE_DEFAULT"] = "恢復默認值";
$MESS["CAT_OPTIONS_BTN_SAVE"] = "節省";
$MESS["CAT_PATH_ERR_EXPORT_FOLDER_BAD"] = "不良默認導出文件路徑。";
$MESS["CAT_POPUP_WINDOW_CLOSE_BTN"] = "關閉窗口";
$MESS["CAT_PROC_REINDEX_CATALOG"] = "產品重新索引";
$MESS["CAT_PROC_REINDEX_CATALOG_ALERT"] = "注意力！僅在遇到產品可用性問題或分類問題時才能執行此操作。";
$MESS["CAT_PROC_REINDEX_CATALOG_BTN"] = "發射";
$MESS["CAT_PROC_REINDEX_DISCOUNT"] = "折扣索引";
$MESS["CAT_PROC_REINDEX_DISCOUNT_ALERT"] = "注意力！僅在使用折扣問題或HelpDesk或更新系統建議時執行此操作。";
$MESS["CAT_PROC_REINDEX_DISCOUNT_BTN"] = "開始";
$MESS["CAT_PROC_REINDEX_SETS_AVAILABLE"] = "捆綁庫存和可用性";
$MESS["CAT_PROC_REINDEX_SETS_AVAILABLE_ALERT"] = "注意力！僅當控制捆綁庫存時或幫助台或更新系統的建議時，僅執行此操作。";
$MESS["CAT_PROC_REINDEX_SETS_AVAILABLE_BTN"] = "更新庫存";
$MESS["CAT_PRODUCT_CARD"] = "產品屬性";
$MESS["CAT_PRODUCT_CARD_DEFAULT_VALUES"] = "默認產品參數值";
$MESS["CAT_PRODUCT_CARD_SLIDER_ENABLED"] = "啟用新產品詳細信息表";
$MESS["CAT_PRODUCT_DEFAULT_VAT_INCLUDED"] = "在新商品和服務的價格中包括稅";
$MESS["CAT_PRODUCT_FORM_SETTINGS"] = "產品選擇表格設置";
$MESS["CAT_PRODUCT_QUANTITY_DECREASE"] = "減少產品庫存：";
$MESS["CAT_PRODUCT_RESERVED"] = "預備項目：";
$MESS["CAT_PRODUCT_SETTINGS_CHANGE"] = "改變";
$MESS["CAT_PRODUCT_SETTINGS_STATUS_NO"] = "不";
$MESS["CAT_PRODUCT_SETTINGS_STATUS_YES"] = "是的";
$MESS["CAT_PRODUCT_SUBSCRIBE"] = "允許訂閱庫存的產品";
$MESS["CAT_PRODUCT_SUBSCRIBE_LABLE_ENABLE"] = "啟用產品訂閱";
$MESS["CAT_PRODUCT_SUBSCRIBE_LABLE_REPEATED_NOTIFY"] = "當產品再次售罄時，請發送另一條消息";
$MESS["CAT_PRODUCT_SUBSCRIBE_TITLE"] = "產品訂閱";
$MESS["CAT_QUANTITY_CONTROL"] = "清晰的商業目錄";
$MESS["CAT_QUANTITY_CONTROL_TAB"] = "清晰的目錄";
$MESS["CAT_RESERVATION_CLEAR_PERIOD"] = "保持預訂的日子：";
$MESS["CAT_SAVE_PRODUCTS_WITHOUT_PRICE"] = "允許無價的產品";
$MESS["CAT_SAVE_PRODUCT_WITH_EMPTY_PRICE_RANGE"] = "允許儲蓄產品缺乏一些價格範圍：";
$MESS["CAT_SELECT_CATALOG"] = "選擇目錄";
$MESS["CAT_SELECT_STORE"] = "選擇倉庫";
$MESS["CAT_SHOW_CATALOG_TAB"] = "使用SKU的產品啟用商業目錄選項卡";
$MESS["CAT_SHOW_OFFERS_IBLOCK"] = "在產品選擇表中顯示SKU信息塊";
$MESS["CAT_SHOW_OFFERS_NAME"] = "顯示SKU名稱";
$MESS["CAT_SHOW_STORE_SHIPPING_CENTER"] = "顯示\“運輸中心\”倉庫選項";
$MESS["CAT_SHOW_STORE_SHIPPING_CENTER_HINT"] = "此舊式選項只能與使用庫存管理功能的自定義產品提供商一起使用。標準提供商不再使用此選項。";
$MESS["CAT_SIMPLE_SEARCH"] = "產品的強製文本搜索過濾器選擇形式";
$MESS["CAT_SKU_SETTINGS"] = "sku";
$MESS["CAT_STORE_1"] = "庫存管理";
$MESS["CAT_STORE_ACTIVE_ERROR"] = "您有一個不活動的倉庫。在實現庫存控制之前，請激活或刪除此倉庫。";
$MESS["CAT_STORE_DEACTIVATE_NOTICE_1"] = "注意力！禁用庫存管理將使不可用的庫存控製文檔。";
$MESS["CAT_STORE_LIST_IS_EMPTY"] = "沒有倉庫。";
$MESS["CAT_STORE_NAME"] = "倉庫";
$MESS["CAT_STORE_SYNCHRONIZE_ERROR"] = "注意力！對於某些產品來說，總和倉庫的股票價值不一致。";
$MESS["CAT_STORE_SYNCHRONIZE_WARNING_1"] = "注意力！您已經啟用了庫存管理。由於倉庫已經被添加到系統中，因此產品總計和倉庫庫存可能不匹配某些產品。";
$MESS["CAT_USE_OFFER_MARKING_CODE_GROUP"] = "允許SKUS的特殊QR碼：";
$MESS["CAT_USE_STORE_CONTROL_1"] = "啟用庫存管理";
$MESS["CAT_VIEWED_COUNT"] = "查看產品列表中的最大項目：";
$MESS["CAT_VIEWED_PERIOD"] = "（幾天）之後刪除過時的物品：";
$MESS["CAT_VIEWED_PRODUCTS_TITLE"] = "最近查看產品的偏好";
$MESS["CAT_VIEWED_TIME"] = "保持查看的產品信息（天）：";
$MESS["CAT_YANDEX_AGENT_ADD_NO_EXPORT"] = "沒有目錄將導出到yandex.market";
$MESS["CAT_YANDEX_AGENT_ADD_SUCCESS"] = "Yandex.Market代理已添加";
$MESS["CAT_YANDEX_CUSTOM_AGENT_FILE"] = "yandex.market自定義導出代理文件：";
$MESS["CAT_YANDEX_CUSTOM_AGENT_FILE_NOT_FOUND"] = "yandex.market未找到自定義出口代理文件";
$MESS["CAT_YANDEX_MARKET_XML_PERIOD"] = "每小時出口到yandex.market：";
$MESS["COP_SYS_ROU"] = "服務例程";
$MESS["COP_TAB2_YANDEX_AGENT"] = "yandex.market代理";
$MESS["COP_TAB2_YANDEX_AGENT_TITLE"] = "yandex.market出口代理";
$MESS["COP_TAB_RECALC"] = "重新索引數據";
$MESS["COP_TAB_RECALC_TITLE"] = "重新索引數據";
$MESS["CO_AVAIL_PRICE_FIELDS"] = "可用的價格字段：";
$MESS["CO_IB_ELEM_ALT"] = "查看信息塊元素";
$MESS["CO_IB_TYPE_ALT"] = "查看信息塊類型";
$MESS["CO_PAR_DPG_CSV"] = "可用目錄部分字段：";
$MESS["CO_PAR_DPP_CSV"] = "可用產品字段：";
$MESS["CO_PAR_DV1_CSV"] = "可用貨幣：";
$MESS["CO_PAR_IE_CSV"] = "CSV出口和進口";
$MESS["CO_SALE_AVAIL"] = "可出售";
$MESS["CO_SALE_CONTENT"] = "內容銷售";
$MESS["CO_SALE_GROUPS"] = "可以出售會員資格的團體";
$MESS["CO_SITE_ALT"] = "查看網站詳細信息";
$MESS["CO_TAB_1"] = "出口進口";
$MESS["CO_TAB_1_TITLE"] = "一般參數";
$MESS["CO_TAB_2"] = "目錄";
$MESS["CO_TAB_2_TITLE"] = "商業目錄";
$MESS["CO_TAB_3"] = "許可銷售";
$MESS["CO_TAB_5"] = "設定";
$MESS["CO_TAB_5_TITLE"] = "通用設置";
$MESS["CO_TAB_RIGHTS"] = "使用權";
$MESS["CO_TAB_RIGHTS_TITLE"] = "模塊訪問權限";
$MESS["CO_USER_GROUP_ALT"] = "查看用戶組詳細信息";
$MESS["SMALL_BUSINESS_RECURRING_ERR"] = "內容銷售功能不包含在您的版本中。";
$MESS["SMALL_BUSINESS_RECURRING_ERR_LIST"] = "以下信息塊啟用了內容銷售：";
$MESS["SMALL_BUSINESS_RECURRING_ERR_LIST_CLEAR"] = "一旦保存模塊設置，將停用此選項。";
$MESS["USE_OFFER_MARKING_CODE_GROUP_HINT"] = "此選項可以為產品或SKUS設置特殊的QR碼。";
