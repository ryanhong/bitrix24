<?php
$MESS["CRM_BUTTON_EDIT_OPENLINE_MULTI_POPUP_LIMITED_TEXT"] = "僅在商業計劃上提供額外的開放渠道。";
$MESS["CRM_RESTR_MGR_CONDITIONALLY_REQUIRED_FIELD_POPUP_CONTENT_2"] = "<div class = \“ CRM條件上需要的場tab-content \”>
<div class = \“ CRM條件上需要的場tab-text \”>
特定於階段的必需字段僅在<a href= \"/settings/license_all.php \" target= \"_blank \">選定的商業計劃</a>中可用。
</div>
</div>";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_CONTENT_2"] = "<div class = \“ crm-deal類別 -  tab-content \”>
<div class = \“ crm-deal類別 -  tab-text \”>
您當前的計劃限制了您可以使用的交易管道數量。請升級到<a href= \"/settings/license_all.php \" target= \"_blank \">選定的商業計劃</a>創建涉及多種產品和銷售策略的管道，以分析各種業務途徑。
</div>
</div>";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_TITLE"] = "CRM多個管道";
$MESS["CRM_RESTR_MGR_DEAL_RECURRING_POPUP_CONTENT"] = "讓Bitrix24為您創建交易！
經常性交易將在多次場合節省您的時間和資源。例如，如果您的計劃是在即將到來的一年中唱每週的合同，則一定是您的交易。
創建一個經常性交易，並設置時間表和管道。新交易將在指定的時間創建，您將不必舉起手指。
<ul class = \“ hide-features-list \”>
<li class = \“ hide-features-list-item \”>自動創建交易</li>
<li class = \“ hide-features-list-item \”>提高員工的績效和效率</li>
<li class = \“ hide-features-list-item \”>快速訪問當前重複交易-  <a href = \'https：//www.bitrix24.com/pro/pro/crm.php \'target = \ \“ target = \” _blank \“ class = \” hide-features-more \“>了解更多</a> </li>
</ul>
<strong>重複交易可在選定的商業計劃中獲得</strong>";
$MESS["CRM_RESTR_MGR_DEAL_RECURRING_POPUP_TITLE"] = "經常出現的交易可在商業計劃中獲得";
$MESS["CRM_RESTR_MGR_DUP_CTRL_MSG_CONTENT_2"] = "<div class = \“ crm-duplate-tab-content \”>
<h3 class = \“ crm-duplate-tab-title \”>高級重複搜索</h3>
<div class = \“ crm-duplate-tab-text \”>
當創建新的聯繫人（或Lead或Company）時，Bitrix24發現並顯示了潛在的重複＆mdash;先發製人阻止重複項的創建。
</div>
<div class = \“ crm-duplate-tab-text \”>
在高級重複搜索中，CRM還可以在導入的數據和已經在數據庫中輸入的數據中找到重複項。這些重複項可以合併在一起。
</div>
<div class = \“ crm-duplate-tab-text \”>
將這些和其他出色的功能添加到您的Bitrix24中！高級電話 +高級CRM和其他有用的功能可在選定的商業計劃中獲得。
<a target= \"_blank \" href= \"https://www.bitrix24.com/pro/crm.php \ \">了解更多</a>
</div>
<div class = \“ ui-btn-container ui-btn-container-center \”>
<span class = \“ ui-btn ui-btn-lg ui-btn-success \” onclick = \“＃
<span class = \“ ui-btn ui-btn-lg ui-btn-light-border \” onclick = \“＃demo_license_script＃\”>獲得免費的30天試驗</span>
</div>
</div>
";
$MESS["CRM_RESTR_MGR_HX_VIEW_MSG_CONTENT_2"] = "<div class = \“ crm-history-tab-content \”>
<h3 class = \“ CRM-History-tab-title \”> CRM歷史變化可在Advanced CRM中使用</h3>
<div class = \“ crm-history-tab-text \”>
Bitrix24保留所有CRM更改的詳細日誌。當您升級到高級CRM時，您將看到更改的歷史記錄（即訪問或更改CRM條目），並在必要時可以還原以前的值。
</div>
<div class = \“ crm-History-tab-text \”>這就是它的外觀：</div>
<img alt = \“ tab歷史\” class = \“ crm-history-tab-img \” src = \ \“/bitrix/js/crm/images/himages/history-en.png \”/>
<div class = \“ crm-history-tab-text \”>
將這些和其他出色的功能添加到您的Bitrix24中！高級電話 +高級CRM和其他有用的功能可在選定的商業計劃中獲得。
<a target= \"_blank \" href= \"https://www.bitrix24.com/pro/crm.php \ \">了解更多</a>
</div>
<div class = \“ ui-btn-container ui-btn-container-center \”>
<span class = \“ ui-btn ui-btn-lg ui-btn-success \” onclick = \“＃
<span class = \“ ui-btn ui-btn-lg ui-btn-light-border \” onclick = \“＃demo_license_script＃\”>免費30天</span>
</div>
</div>";
$MESS["CRM_RESTR_MGR_INVOICE_RECURRING_POPUP_CONTENT"] = "使用自動發票功能定期向客戶收費並節省您的時間。
創建發票並指定您希望將來發送的頻率。該系統將根據您的時機偏好創建並向客戶發送新的發票。
<ul class = \“ hide-features-list \”>
<li class = \“ hide-features-list-item \”>通過自動發票節省時間</li>
<li class = \“ hide-features-list-item \”>快速訪問所有自動發票模板</li>
<li class = \“ hide-features-list-item \”>將發票發送給客戶的電子郵件<a href = \'https：//www.bitrix24.com/pro/pro/crm.php \'target = \ \“ _blank \“ class = \” hide-features-more \“>閱讀更多</a> </li>
</ul>
<strong>自動發票可在選定的商業計劃中獲得。</strong>";
$MESS["CRM_RESTR_MGR_INVOICE_RECURRING_POPUP_TITLE"] = "商業計劃中可以使用自動發票。";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_CONTENT"] = "<div class = \“ crm- permission-control-tab-content \”>
<div class = \“ crm- permission-control-tab-text \”>
免費計劃將相同的訪問權限分配給所有員工。考慮升級到為各種用戶分配不同角色，動作和數據的商業計劃之一。
要了解有關不同計劃的更多信息，請繼續前往<a target= \"_blank \" href= \"https://www.bitrix24.com/prices/prices/vrices/xprices/xprices/frices/frices/ frices/frices/frices/frices/frices/frices/frices/frices/frices/frices/frices/flice Plan比較頁面</a>。
</div>
</div>";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_CONTENT_2"] = "<div class = \“ crm- permission-control-tab-content \”>
<div class = \“ crm- permission-control-tab-text \”>
在您當前的計劃中，所有員工都共享統一的權限。升級到一個或主要計劃，以定義角色，分配權限並指定員工可以訪問的數據。
<a target = \"_blank \" href= \"https://www.bitrix24.com/prices/ \ \">詳細了解包含的計劃和比較功能</a> </a>。
</div>
</div>";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_TITLE"] = "要分配您的員工不同的訪問權限，請升級到商業計劃之一。";
$MESS["CRM_RESTR_MGR_POPUP_CONTENT_2"] = "將以下內容添加到您的CRM中：
<ul class = \“ hide-features-list \”>
<li class = \“ hide-features-list-item>交易，發票和報價之間的轉換</li>
<li class = \“ hide-features-list-item \”>擴展的重複搜索</li>
<li class = \“ hide-features-list-item \”>更改CRM回滾的歷史記錄</li>
<li class = \“ hide-features-list-item \”> crm訪問日誌</li>
<li class = \“ hide-features-list-item \”>查看CRM中的5000多個記錄</li>
<li class = \“ hide-features-list-item \”>列表驅動的呼叫<sup class = \“ hide-features-soon \”>即將推出</sup> </li>
<li class = \“ hide-features-list-item \”>給客戶的批量電子郵件<sup class = \“ hide-features-soon \”>即將推出</sup> </li>
<li class = \“ hide-features-list-item \”>支持服務銷售<sup class = \“ hide-features-soon \”>即將推出</sup>
<a target= \"_blank \" class= \" hide-features-more \" href= \之https://www.bitrix24.com/pro/cr/crm.php \">了解更多</a >
</li>
</ul>
<strong>高級電話 +高級CRM和其他有用的功能可在選定的商業計劃中獲得。</strong>
";
$MESS["CRM_RESTR_MGR_POPUP_TITLE"] = "Bitrix24中的高級CRM";
$MESS["CRM_ST_ROBOTS_POPUP_TEXT"] = "
Bitrix24 CRM自動銷售<br> <br>
為CRM創建方案，以推動您的交易前進：分配任務，計劃會議，啟動目標廣告系列並發行發票。自動化規則將指導您的經理在每個交易階段完成下一步以完成工作。<br> <br>
觸發器會自動對客戶的操作做出反應（訪問站點時，填寫表格，發布社交網絡評論或撥打電話）並啟動自動化規則，幫助研究生領先。<br> <br> <br> <br> <br >
自動化規則和触發器正在進行所有常規操作，而無需對管理者進行任何培訓：配置一次並繼續工作！<br> <br> <br>
添加自動化規則並提高您的銷售！<br> <br>
此工具僅在商業Bitrix24訂閱中可用。
";
$MESS["CRM_ST_ROBOTS_POPUP_TITLE"] = "自動化規則";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TEXT"] = "簽名只能在商業計劃上刪除。";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TITLE"] = "擴展的CRM表格";
$MESS["CRM_WEBFORM_LIST_POPUP_LIMITED_TEXT"] = "您當前的計劃限制了您可以擁有的CRM表格數量。為了添加新表格，請升級您的計劃。
<br> <br>
提示：選定的商業計劃帶有無限的CRM表格。
<br> <br>
嘗試Bitrix24專業免費30天。";
