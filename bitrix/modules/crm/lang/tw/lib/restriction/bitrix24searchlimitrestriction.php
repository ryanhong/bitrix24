<?php
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_COMPANY_WARNING_TEXT1"] = "你是真正的職業！現在，您的Bitrix24中的＃Count＃公司還多。請注意：您只有在其數字超過＃limit＃＃＃時才可以在商業計劃上搜索公司。";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_COMPANY_WARNING_TEXT2"] = "搜索如此多的公司將需要大量資源，因此只能在免費計劃中使用預設過濾器。 ＃helpdesk_link＃";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_CONTACT_WARNING_TEXT1"] = "你是真正的職業！現在，您的Bitrix24中的＃＃聯繫人不僅僅是＃count＃聯繫人。請注意：當其數字超過＃limit＃時，您只能在商業計劃上搜索聯繫人。";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_CONTACT_WARNING_TEXT2"] = "搜索如此多的聯繫人將需要大量資源，因此只有預設過濾器才能在免費計劃中使用。 ＃helpdesk_link＃";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_DEAL_WARNING_TEXT1"] = "你是真正的職業！現在，您的Bitrix24中有不止＃count＃交易。請注意：當其數字超過＃limit＃時，您只能在商業計劃上搜索交易。";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_DEAL_WARNING_TEXT2"] = "搜索如此多的交易將需要大量資源，因此只能在免費計劃中使用預設過濾器。 ＃helpdesk_link＃";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_FILTER_COMPANY_CONTENT"] = "達到公司搜索限制（＃limit＃）。我們正在努力提高服務質量和績效以適合您的業務規模。隨著業務的增長，數據的數量增加：每天創建新的任務，交易和其他實體。";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_FILTER_CONTACT_CONTENT"] = "聯繫人搜索限制（＃限制＃）達到了。我們正在努力提高服務質量和績效以適合您的業務規模。隨著業務的增長，數據的數量增加：每天創建新的任務，交易和其他實體。";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_FILTER_CONTENT2"] = "您的業​​務規模越大，搜索您創建的數據就越多。選擇適合您數據以更快搜索的計劃。 ＃helpdesk_link＃";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_FILTER_DEAL_CONTENT"] = "達到交易搜索限制（＃limit＃）。我們正在努力提高服務質量和績效以適合您的業務規模。隨著業務的增長，數據的數量增加：每天創建新的任務，交易和其他實體。";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_FILTER_DYNAMIC_CONTENT"] = "您的智能過程自動化不僅僅是＃limit＃項目。請升級到搜索更多項目的主要計劃之一。";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_FILTER_INVOICE_CONTENT"] = "到達發票搜索限制（＃limit＃）。我們正在努力提高服務質量和績效以適合您的業務規模。隨著業務的增長，數據的數量增加：每天創建新的任務，交易和其他實體。";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_FILTER_LEAD_CONTENT"] = "引線搜索限制（＃限制＃）達到了。我們正在努力提高服務質量和績效以適合您的業務規模。隨著業務的增長，數據的數量增加：每天創建新的任務，交易和其他實體。";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_FILTER_ORDER_CONTENT"] = "訂購搜索限制（＃limit＃）達到了。我們正在努力提高服務質量和績效以適合您的業務規模。隨著業務的增長，數據的數量增加：每天創建新的任務，交易和其他實體。";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_FILTER_QUOTE_CONTENT"] = "引用搜索限制（＃limit＃＃）已達到。我們正在努力提高服務質量和績效以適合您的業務規模。隨著業務的增長，數據的數量增加：每天創建新的任務，交易和其他實體。";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_FILTER_TITLE"] = "達到最大搜索限制";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_HELPDESK_LINK"] = "細節";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_INVOICE_WARNING_TEXT1"] = "你是真正的職業！現在，您的Bitrix24中的＃count＃＃發票。請注意：當其數字超過＃limit＃時，您只能在商業計劃上搜索發票。";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_INVOICE_WARNING_TEXT2"] = "搜索如此多的發票將需要大量資源，因此只有預設過濾器才能在免費計劃中使用。 ＃helpdesk_link＃";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_LEAD_WARNING_TEXT1"] = "你是真正的職業！現在，您的Bitrix24中有不止＃count＃引線。請注意：當其數字超過＃limit＃時，您只能在商業計劃上搜索潛在客戶。";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_LEAD_WARNING_TEXT2"] = "搜索如此多的線索將需要大量資源，因此只有預設過濾器才能在免費計劃中使用。 ＃helpdesk_link＃";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_LIMIT_CONTENT"] = "
<p>我們正在努力提高服務質量和績效，以適應您的業務規模。隨著業務的增長，數據的數量增加：每天創建新任務，交易和其他實體。</p>
<p>您的業務規模越大，搜索您創建的數據就越多。選擇適合您數據以更快搜索的計劃。</p>
";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_ORDER_WARNING_TEXT1"] = "你是真正的職業！現在，您的Bitrix24中有＃次數＃訂單。請注意：當其數字超過＃limit＃時，您只能在商業計劃上搜索訂單。";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_ORDER_WARNING_TEXT2"] = "搜索如此多的訂單將需要大量資源，因此只有預設過濾器才能在免費計劃中使用。 ＃helpdesk_link＃";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_QUOTE_WARNING_TEXT1"] = "你是真正的職業！現在，您的Bitrix24中有不止＃count＃引號。請注意：當其數字超過＃limit＃時，您只能在商業計劃上搜索報價。";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_QUOTE_WARNING_TEXT2"] = "搜索如此多的報價將需要大量資源，因此只能在免費計劃中使用預設過濾器。 ＃helpdesk_link＃";
$MESS["CRM_B24_SEARCH_LIMIT_RESTRICTION_TITLE"] = "到達到的項目限制（＃limit＃）";
