<?php
$MESS["CRM_AGNT_DUP_VOLATILE_IDX_ERR_ALREADY_RUNNING"] = "無法開始重新索引，因為它已經在進行中。";
$MESS["CRM_AGNT_DUP_VOLATILE_IDX_ERR_NOT_RUNNING"] = "無法停止重新索引，因為它從未啟動過。";
$MESS["CRM_AGNT_DUP_VOLATILE_IDX_ERR_NOT_STOPPED"] = "無法恢復重新索引，因為它既沒有正在進行，也沒有暫停。";
