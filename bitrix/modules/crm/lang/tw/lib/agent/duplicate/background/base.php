<?php
$MESS["CRM_AGNT_DUP_BGRND_ERR_INDEX_TYPES"] = "索引類型是未定義的或具有無效的值。";
$MESS["CRM_AGNT_DUP_BGRND_ERR_SCOPE"] = "未指定國家代碼或具有無效的價值。";
$MESS["CRM_AGNT_DUP_BGRND_ERR_TYPE_INDEX"] = "當前類型索引不正確。";
