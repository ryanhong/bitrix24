<?php
$MESS["CRM_FIELD_ATTRIBUTE_MANAGER_STAGE_CAPTION_GROUP_TYPE_GENERAL2"] = "對於所有階段和管道";
$MESS["CRM_FIELD_ATTRIBUTE_MANAGER_STAGE_CAPTION_GROUP_TYPE_JUNK"] = "失敗的";
$MESS["CRM_FIELD_ATTRIBUTE_MANAGER_STAGE_CAPTION_GROUP_TYPE_PIPELINE"] = "正在進行 +成功";
$MESS["CRM_FIELD_ATTRIBUTE_MANAGER_STAGE_CAPTION_REQUIRED_FULL"] = "從舞台上需要";
$MESS["CRM_FIELD_ATTRIBUTE_MANAGER_STAGE_CAPTION_REQUIRED_FULL_1"] = "在舞台上需要";
$MESS["CRM_FIELD_ATTRIBUTE_MANAGER_STAGE_CAPTION_REQUIRED_SHORT"] = "必需的";
$MESS["CRM_FIELD_ATTRIBUTE_MANAGER_STATUS_CAPTION_GROUP_TYPE_GENERAL_MSGVER_1"] = "對於所有階段";
$MESS["CRM_FIELD_ATTRIBUTE_MANAGER_STATUS_CAPTION_GROUP_TYPE_JUNK"] = "垃圾";
$MESS["CRM_FIELD_ATTRIBUTE_MANAGER_STATUS_CAPTION_GROUP_TYPE_PIPELINE"] = "正在進行 +轉換";
$MESS["CRM_FIELD_ATTRIBUTE_MANAGER_STATUS_CAPTION_REQUIRED_FULL"] = "狀態需要";
$MESS["CRM_FIELD_ATTRIBUTE_MANAGER_STATUS_CAPTION_REQUIRED_SHORT"] = "必需的";
