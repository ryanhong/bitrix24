<?php
$MESS["CRM_STATE_CATALOG_PRODUCT_PRICE_CHANGING_BLOCKED"] = "目前，您無法在交易，發票等中更改產品價格。以較低的價格出售產品折扣。您可以在CRM設置中啟用價格編輯。";
$MESS["CRM_STATE_ERR_CATALOG_PRODUCT_LIMIT"] = "CRM目錄產品的最大數量超過了。當前有＃count＃products（超出＃limit＃max）。";
