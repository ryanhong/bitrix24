<?php
$MESS["CRM_ENTITY_PRESET_ACTIVE_FIELD"] = "積極的";
$MESS["CRM_ENTITY_PRESET_COUNTRY_ID_FIELD"] = "國家ID";
$MESS["CRM_ENTITY_PRESET_CREATED_BY_ID_FIELD"] = "由...製作";
$MESS["CRM_ENTITY_PRESET_DATE_CREATE_FIELD"] = "創建於";
$MESS["CRM_ENTITY_PRESET_DATE_MODIFY_FIELD"] = "修改";
$MESS["CRM_ENTITY_PRESET_ENTITY_TYPE_ID_FIELD"] = "實體類型ID";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_DEF_FOR_COMPANY"] = "您無法刪除此模板，因為它是默認的公司模板。";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_DEF_FOR_CONTACT"] = "您無法刪除此模板，因為它是默認的觸點模板。";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_USED"] = "由於上面有一些詳細信息，因此無法刪除模板。";
$MESS["CRM_ENTITY_PRESET_ERR_INVALID_ENTITY_TYPE"] = "無效的實體類型用於模板。";
$MESS["CRM_ENTITY_PRESET_ERR_PRESET_NOT_FOUND"] = "找不到模板。";
$MESS["CRM_ENTITY_PRESET_FIELD_NAME_FIELD"] = "姓名";
$MESS["CRM_ENTITY_PRESET_FIELD_TITLE_FIELD"] = "模板中的名稱";
$MESS["CRM_ENTITY_PRESET_ID_FIELD"] = "ID";
$MESS["CRM_ENTITY_PRESET_IN_SHORT_LIST_FIELD"] = "節目總結";
$MESS["CRM_ENTITY_PRESET_MODIFY_BY_ID_FIELD"] = "修改";
$MESS["CRM_ENTITY_PRESET_NAME_EMPTY"] = "無標題的模板";
$MESS["CRM_ENTITY_PRESET_NAME_FIELD"] = "姓名";
$MESS["CRM_ENTITY_PRESET_SORT_FIELD"] = "種類";
$MESS["CRM_ENTITY_PRESET_UF_TYPE_BOOLEAN"] = "是/否";
$MESS["CRM_ENTITY_PRESET_UF_TYPE_DATETIME"] = "日期";
$MESS["CRM_ENTITY_PRESET_UF_TYPE_DOUBLE"] = "數字";
$MESS["CRM_ENTITY_PRESET_UF_TYPE_STRING"] = "細繩";
$MESS["CRM_ENTITY_PRESET_XML_ID_FIELD"] = "外部ID";
$MESS["CRM_ENTITY_TYPE_REQUISITE"] = "細節";
$MESS["CRM_ENTITY_TYPE_REQUISITE_DESC"] = "聯繫模板或公司詳細信息";
