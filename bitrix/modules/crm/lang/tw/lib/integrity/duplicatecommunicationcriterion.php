<?php
$MESS["CRM_DUP_CRITERION_COMM_EMAIL_ENTITY_TOTAL"] = "＃QTY＃用於電子郵件\“＃descr＃\”";
$MESS["CRM_DUP_CRITERION_COMM_EMAIL_ENTITY_TOTAL_EXCEEDED"] = "通過＃QTY＃for ea-mail \“＃descr＃\”";
$MESS["CRM_DUP_CRITERION_COMM_EMAIL_SUMMARY"] = "匹配電子郵件：\“＃descr＃\”";
$MESS["CRM_DUP_CRITERION_COMM_PHONE_ENTITY_TOTAL"] = "＃QTY＃for Phone \“＃Descr＃\”";
$MESS["CRM_DUP_CRITERION_COMM_PHONE_ENTITY_TOTAL_EXCEEDED"] = "＃QTY＃for Phone \“＃descr＃\”";
$MESS["CRM_DUP_CRITERION_COMM_PHONE_SUMMARY"] = "匹配電話：\“＃descr＃\”";
