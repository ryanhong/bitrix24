<?php
$MESS["CRM_ADS_FORM_TYPE_ERR_DISABLED_FACEBOOK"] = "由於Facebook隱私政策的最新變化，Bitrix24 CRM表格和Facebook Lead Ads之間的集成不可用。 Facebook技術人員正在研究新的API，並承諾很快就會為Bitrix24用戶提供此功能。";
$MESS["CRM_ADS_FORM_TYPE_NAME_FACEBOOK"] = "Facebook廣告";
$MESS["CRM_ADS_FORM_TYPE_NAME_VKONTAKTE"] = "VK廣告";
