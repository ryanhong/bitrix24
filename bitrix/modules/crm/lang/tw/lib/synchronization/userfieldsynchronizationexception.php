<?php
$MESS["CRM_UF_SYNC_EXCEPTION_CREATE_FAILED"] = "無法創建自定義字段（字段名稱：＃名稱＃，字段類型：＃type_name＃，所有者：＃entity_type＃）。";
$MESS["CRM_UF_SYNC_EXCEPTION_GENERAL"] = "同步自定義字段時的一般錯誤。";
