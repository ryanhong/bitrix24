<?php
$MESS["CRM_RECUR_ACTIVATE_LIMIT_DATA"] = "錯誤激活訂單是因為上次重複發票日期達到了。您可能需要更改日期。";
$MESS["CRM_RECUR_ACTIVATE_LIMIT_REPEAT"] = "由於超過最大重複計數而激活順序的錯誤。您可能需要增加最大值。";
$MESS["CRM_RECUR_WRONG_ID"] = "無效的帳戶ID";
