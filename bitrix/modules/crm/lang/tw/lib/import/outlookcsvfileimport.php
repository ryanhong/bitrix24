<?php
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BIRTHDAY"] = "生日";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_CITY"] = "商業城";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_FAX"] = "業務傳真";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_PHONE"] = "商務電話";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_PHONE_2"] = "商務電話2";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_POSTAL_CODE"] = "業務郵政編碼";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_POSTAL_COUNTRY"] = "商業國家/地區";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_STATE"] = "商業狀態";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_STREET"] = "商業街";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_CAR_PHONE"] = "汽車電話";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_COMPANY"] = "公司";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_COMPANY_MAIN_PHONE"] = "公司主電話";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_E_MAIL_2_ADDRESS"] = "電子郵件2地址";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_E_MAIL_3_ADDRESS"] = "電子郵件3地址";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_E_MAIL_ADDRESS"] = "電子郵件地址";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_FIRST_NAME"] = "名";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_CITY"] = "家鄉城市";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_COUNTRY"] = "本國/地區";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_FAX"] = "家庭傳真";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_PHONE"] = "家庭電話";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_PHONE_2"] = "家庭電話2";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_POSTAL_CODE"] = "家庭郵政代碼";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_STATE"] = "老家";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_STREET"] = "家庭街";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_JOB_TITLE"] = "公司";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_LAST_NAME"] = "姓";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_MIDDLE_NAME"] = "中間名字";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_MOBILE_PHONE"] = "智慧型手機";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_NOTES"] = "筆記";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_CITY"] = "另一個城市";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_COUNTRY"] = "其他國家/地區";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_FAX"] = "其他傳真";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_PHONE"] = "其他電話";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_POSTAL_CODE"] = "其他郵政編碼";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_STATE"] = "另一個州";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_STREET"] = "另一條街";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_PAGER"] = "呼叫器";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_PRIMARY_PHONE"] = "常用電話";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_RADIO_PHONE"] = "廣播電話";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_WEB_PAGE"] = "網頁";
$MESS["CRM_IMPORT_OUTLOOK_ERROR_FIELDS_NOT_FOUND"] = "沒有找到以下兩個字段：＃field_list＃";
$MESS["CRM_IMPORT_OUTLOOK_REQUIREMENTS"] = "導入文件應滿足以下要求正確導入。文件編碼：＃file_encoding＃。列標題的語言：＃file_lang＃。場分離器：逗號。";
$MESS["CRM_IMPORT_OUTLOOK_REQUIREMENTS_NEW"] = "您的文件需要遵循這些要求，才能成功導入：編碼：UTF-8；字段名稱語言：＃file_lang＃;場分離器：逗號。";
