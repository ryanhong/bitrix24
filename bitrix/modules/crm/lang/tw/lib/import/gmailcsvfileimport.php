<?php
$MESS["CRM_IMPORT_GMAIL_ERROR_FIELDS_NOT_FOUND"] = "沒有找到以下兩個字段：＃field_list＃";
$MESS["CRM_IMPORT_GMAIL_REQUIREMENTS"] = "文件需要遵循這些要求才能成功導入：編碼：UTF-16；字段名稱語言：英語；場分離器：逗號。";
$MESS["CRM_IMPORT_GMAIL_REQUIREMENTS_NEW"] = "您的文件需要遵循這些要求，才能成功導入：編碼：UTF-8；字段名稱語言：英語；場分離器：逗號。";
