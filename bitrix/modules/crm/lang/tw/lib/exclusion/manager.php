<?php
$MESS["CRM_EXCLUSION_COMPANY_DELETION_ERROR"] = "刪除公司時發生錯誤。";
$MESS["CRM_EXCLUSION_CONTACT_DELETION_ERROR"] = "刪除聯繫人時發生錯誤。";
$MESS["CRM_EXCLUSION_DEAL_DELETION_ERROR"] = "刪除交易時發生錯誤。";
$MESS["CRM_EXCLUSION_LEAD_DELETION_ERROR"] = "錯誤刪除引線。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
