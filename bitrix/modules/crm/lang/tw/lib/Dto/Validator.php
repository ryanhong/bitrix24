<?php
$MESS["CRM_DTO_VALIDATOR_KEY_CONTAIN_WRONG_SYMBOLS"] = "＃鍵＃的值包括無效的字符。";
$MESS["CRM_DTO_VALIDATOR_TOO_MANY_ITEMS"] = "＃parent_object＃的字段＃字段＃只能包含＃count＃values。";
$MESS["CRM_DTO_VALIDATOR_WRONG_FIELD_VALUE"] = "＃parent_object＃中＃字段＃的值不正確。";
