<?php
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_ACCESS_DENIED"] = "執行操作的權限不足。";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_DELETE_DEDUCTED_ERROR"] = "\“銷售訂單## id＃\”無法刪除，因為已經處理過";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_NOT_USED_INVENTORY_MANAGEMENT"] = "您必須使庫存管理能夠處理庫存對象";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_ORDER_NOT_FOUND_ERROR"] = "找不到訂單";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_PRODUCT_NOT_FOUND"] = "至少輸入一個產品";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_SHIPMENT_NOT_FOUND_ERROR"] = "未找到發貨## ID＃";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_SHIP_DEDUCTED_ERROR"] = "錯誤處理\“銷售訂單## ID＃\”：已經處理過";
$MESS["CRM_CONTROLLER_REALIZATION_DOCUMENT_UNSHIP_UNDEDUCTED_ERROR"] = "錯誤取消\“銷售訂單## ID＃\”：尚未處理。";
