<?php
$MESS["CRM_CONTROLLER_FIELDSET_NOT_FOUND"] = "找不到類型＃entity_type＃的字段集。";
$MESS["CRM_CONTROLLER_FIELDSET_READ_CONFIG_DENIED"] = "從CRM加載配置的權限不足。請聯繫您的主管或Bitrix24管理員。";
$MESS["CRM_CONTROLLER_FIELDSET_WRITE_CONFIG_DENIED"] = "將配置保存到CRM的權限不足。請聯繫您的主管或Bitrix24管理員。";
