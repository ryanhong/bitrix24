<?php
$MESS["CRM_INVITATION_CLIENT_BIND_ERROR"] = "無法選擇客戶。刷新頁面，然後重試。";
$MESS["CRM_INVITATION_ERROR"] = "發送錯誤";
$MESS["CRM_INVITATION_WRONG_COMPANY"] = "無法執行動作。刷新頁面，然後重試。";
$MESS["CRM_INVITATION_WRONG_CONTACT"] = "無法執行動作。刷新頁面，然後重試。";
