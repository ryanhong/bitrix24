<?php
$MESS["CRM_CONTROLLER_SEARCH_ACTION_COMPANY_LIMIT_EXCEEDED"] = "要搜索所有公司，請選擇適合累積數據和業務規模的計劃。 <a onclick= \"top.bx.helper.show('Redirect=detail&code=9745327')";
$MESS["CRM_CONTROLLER_SEARCH_ACTION_CONTACT_LIMIT_EXCEEDED"] = "要搜索所有聯繫人，請選擇適合累積數據和業務規模的計劃。 <a onclick= \"top.bx.helper.show('Redirect=detail&code=9745327')";
$MESS["CRM_CONTROLLER_SEARCH_ACTION_DEAL_LIMIT_EXCEEDED"] = "要搜索所有交易，請選擇適合累積數據和業務規模的計劃。 <a onclick= \"top.bx.helper.show('Redirect=detail&code=9745327')";
$MESS["CRM_CONTROLLER_SEARCH_ACTION_INVOICE_LIMIT_EXCEEDED"] = "要搜索所有發票，請選擇適合累積數據和業務規模的計劃。 <a onclick= \"top.bx.helper.show('Redirect=detail&code=9745327')";
$MESS["CRM_CONTROLLER_SEARCH_ACTION_LEAD_LIMIT_EXCEEDED"] = "要搜索所有潛在客戶，請選擇適合累積數據和業務規模的計劃。 <a onclick= \"top.bx.helper.show('Redirect=detail&code=9745327')";
$MESS["CRM_CONTROLLER_SEARCH_ACTION_QUOTE_LIMIT_EXCEEDED_MSGVER_1"] = "要搜索所有估計，請選擇適合累積數據和業務規模的計劃。 <a onclick= \"top.bx.helper.show('Redirect=detail&code=9745327')";
