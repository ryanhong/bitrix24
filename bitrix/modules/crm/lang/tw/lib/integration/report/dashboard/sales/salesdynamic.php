<?php
$MESS["CRM_REPORT_SALES_DYNAMIC_FIRST_LEAD_TITLE"] = "最初的";
$MESS["CRM_REPORT_SALES_DYNAMIC_GRID_CONVERSION_TITLE"] = "轉換";
$MESS["CRM_REPORT_SALES_DYNAMIC_GRID_DEAL_SUM_TITLE"] = "交易總額";
$MESS["CRM_REPORT_SALES_DYNAMIC_GRID_FIRST_DEAL_WON_SUM_TITLE"] = "最初的";
$MESS["CRM_REPORT_SALES_DYNAMIC_GRID_LOSES_SUM_TITLE"] = "丟失的";
$MESS["CRM_REPORT_SALES_DYNAMIC_GRID_REPEAT_DEAL_WON_SUM_TITLE"] = "重複";
$MESS["CRM_REPORT_SALES_DYNAMIC_GRID_TITLE"] = "銷售趨勢（金錢）";
$MESS["CRM_REPORT_SALES_DYNAMIC_MANAGER_GRID_AMOUNT_TITLE"] = "全部的";
$MESS["CRM_REPORT_SALES_DYNAMIC_MANAGER_GRID_GROUPING_COLUMN_TITLE"] = "主管";
$MESS["CRM_REPORT_SALES_DYNAMIC_REPEAT_LEAD_TITLE"] = "重複";
$MESS["CRM_REPORT_SALES_DYNAMIC_TITLE"] = "銷售趨勢";
