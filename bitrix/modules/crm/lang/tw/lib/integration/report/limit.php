<?php
$MESS["CRM_ANALYTICS_COMPANY_LIMIT_SOLUTION_MASK"] = "為了規避限制，請刪除您不再需要或升級到主要計劃之一的公司";
$MESS["CRM_ANALYTICS_COMPANY_LIMIT_SOLUTION_MASK_DELETE"] = "請刪除您不再需要刪除限制的公司。<br> <a href= \"#more_info_link# \ \">詳細信息</a>";
$MESS["CRM_ANALYTICS_CONTACT_LIMIT_SOLUTION_MASK"] = "為了規避限制，請刪除您不再需要或升級到主要計劃之一的聯繫人。";
$MESS["CRM_ANALYTICS_CONTACT_LIMIT_SOLUTION_MASK_DELETE"] = "請刪除您不再需要刪除限制的聯繫人。<br> <a href= \"#more_info_link# \ \">詳細信息</a>";
$MESS["CRM_ANALYTICS_DEAL_LIMIT_SOLUTION_MASK"] = "為了避免限制，請刪除您不再需要或升級到主要計劃之一的交易。";
$MESS["CRM_ANALYTICS_DEAL_LIMIT_SOLUTION_MASK_DELETE"] = "請刪除您不再需要刪除限制的交易。<br> <a href= \"#more_info_link# \ \">詳細信息</a>";
$MESS["CRM_ANALYTICS_LEAD_LIMIT_SOLUTION_MASK"] = "為了避免限制，請刪除潛在客戶不再需要或升級到主要計劃之一。";
$MESS["CRM_ANALYTICS_LEAD_LIMIT_SOLUTION_MASK_DELETE"] = "請刪除潛在客戶不再需要刪除限制。<br> <a href= \"#more_info_link# \ \">詳細信息</a>";
$MESS["CRM_ANALYTICS_LIMIT_COMPANY_ACTUAL_COUNT_MASK"] = "當前公司計數：＃實際_count＃";
$MESS["CRM_ANALYTICS_LIMIT_COMPANY_MAX_COUNT_MASK"] = "最多可＃max_count＃公司可以包括在報告中。";
$MESS["CRM_ANALYTICS_LIMIT_CONTACT_ACTUAL_COUNT_MASK"] = "當前的聯繫人計數：＃實際_count＃";
$MESS["CRM_ANALYTICS_LIMIT_CONTACT_MAX_COUNT_MASK"] = "最多可將＃max_count＃聯繫人包含在報告中。";
$MESS["CRM_ANALYTICS_LIMIT_DEAL_ACTUAL_COUNT_MASK"] = "當前交易計數：＃實際_count＃";
$MESS["CRM_ANALYTICS_LIMIT_DEAL_MAX_COUNT_MASK"] = "最多可＃max_count＃交易可以包含在報告中。";
$MESS["CRM_ANALYTICS_LIMIT_LEAD_ACTUAL_COUNT_MASK"] = "當前的潛在客戶計數：＃實際_COUNT＃";
$MESS["CRM_ANALYTICS_LIMIT_LEAD_MAX_COUNT_MASK"] = "最多可＃max_count＃引線可以包含在報告中。";
$MESS["CRM_ANALYTICS_LIMIT_MASK_TEXT"] = "分析報告中只能包括一定數量的CRM實體。";
$MESS["CRM_ANALYTICS_LIMIT_MASK_TEXT_FOR_MORE_INFO"] = "有關更多詳細信息，請參見計劃比較圖表。";
