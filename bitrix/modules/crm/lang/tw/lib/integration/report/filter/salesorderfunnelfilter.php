<?php
$MESS["CRM_REPORT_FILTER_CURRENT_MONTH_PRESET_TITLE"] = "這個月";
$MESS["CRM_REPORT_FILTER_LAST_30_DAYS_PRESET_TITLE"] = "最近30天";
$MESS["CRM_REPORT_FILTER_LAST_MONTH_PRESET_TITLE"] = "前一個月";
$MESS["CRM_REPORT_FILTER_LAST_QUARTER_PRESET_TITLE"] = "上一季度";
