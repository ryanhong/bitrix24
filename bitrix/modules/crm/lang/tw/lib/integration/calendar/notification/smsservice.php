<?php
$MESS["CRM_CALENDAR_SHARING_EVENT_AUTO_ACCEPTED"] = "確認的會議：＃event_url＃";
$MESS["CRM_CALENDAR_SHARING_EVENT_CANCELLED"] = "開會取消：＃event_url＃";
$MESS["CRM_CALENDAR_SHARING_EVENT_EDITED"] = "會議時間更改：＃event_url＃";
$MESS["CRM_CALENDAR_SHARING_EVENT_INVITATION"] = "＃first_name＃邀請您參加會議。請選擇一個時間：＃url＃";
