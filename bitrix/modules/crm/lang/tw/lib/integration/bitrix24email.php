<?php
$MESS["CRM_B24_EMAIL_FREE_LICENSE_SIGNATURE"] = "由[url = http：//www.bitrix24.com]發送，bitrix24.com [/url]  - 世界上最受歡迎的免費CRM和客戶管理解決方案";
$MESS["CRM_B24_EMAIL_PAID_LICENSE_SIGNATURE"] = "由[url = http：//www.bitrix24.com]發送bitrix24.com [/url]";
$MESS["CRM_B24_EMAIL_SIGNATURE_DISABLED"] = "禁用";
$MESS["CRM_B24_EMAIL_SIGNATURE_ENABLED"] = "已啟用（可以在Plus，標準和專業商業計劃中禁用選項）";
$MESS["CRM_B24_EMAIL_SIGNATURE_ENABLED_2"] = "已啟用（只能在商業計劃上禁用）";
