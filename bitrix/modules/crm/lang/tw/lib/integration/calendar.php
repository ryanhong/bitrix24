<?php
$MESS["CRM_CALENDAR_HELP_LINK"] = "了解更多";
$MESS["CRM_CALENDAR_VIEW_MODE_SPOTLIGHT_DEAL"] = "選擇在日曆中使用的首選交易視圖模式：按日期，其他日期或資源可用性。";
$MESS["CRM_CALENDAR_VIEW_MODE_SPOTLIGHT_LEAD"] = "選擇在日曆中使用的首選潛在客戶視圖模式：按日期，其他日期或資源可用性。";
$MESS["CRM_CALENDAR_VIEW_SPOTLIGHT"] = "現在，您可以在日曆模式下查看潛在客戶和交易。計劃您的客戶關係，同時關注資源可用性。";
