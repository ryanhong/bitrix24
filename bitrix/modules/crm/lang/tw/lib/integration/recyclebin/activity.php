<?php
$MESS["CRM_RECYCLE_BIN_ACTIVITY_ENTITY_NAME"] = "活動";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_RECOVERY_CONFIRMATION"] = "您要恢復所選活動嗎？";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_REMOVAL_CONFIRMATION"] = "活動將不可逆轉地刪除。你想繼續嗎？";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_REMOVED"] = "活動已被刪除。";
$MESS["CRM_RECYCLE_BIN_ACTIVITY_RESTORED"] = "活動已成功恢復。";
