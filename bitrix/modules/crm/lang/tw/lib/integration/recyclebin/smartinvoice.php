<?php
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_ENTITY_NAME"] = "發票";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_RECOVERY_CONFIRMATION"] = "您想恢復所選的發票嗎？";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_REMOVAL_CONFIRMATION"] = "發票將被永久刪除。你想繼續嗎？";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_REMOVED"] = "發票已刪除。";
$MESS["CRM_RECYCLE_BIN_SMART_INVOICE_RESTORED"] = "發票已成功恢復。";
