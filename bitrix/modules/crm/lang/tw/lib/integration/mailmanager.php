<?php
$MESS["CRM_INTEGRATION_MAIL_MANAGER_EMPTY_BODY_PLACEHOLDER"] = "[無文本]";
$MESS["CRM_INTEGRATION_MAIL_MANAGER_EMPTY_SUBJECT_PLACEHOLDER"] = "新電子郵件（＃日期＃）";
$MESS["CRM_INTEGRATION_MAIL_MANAGER_ERROR_DAILY_LIMIT"] = "每日電子郵件發送配額。更改發件人設置中的限制。";
$MESS["CRM_INTEGRATION_MAIL_MANAGER_ERROR_FROM_MAILBOX_RESTRICTED"] = "由於當前的計劃限制，您試圖用於傳出電子郵件的郵箱被阻止。";
$MESS["CRM_INTEGRATION_MAIL_MANAGER_ERROR_INVALID_TO_EMAIL"] = "收件人電子郵件＃電子郵件＃有錯誤。";
$MESS["CRM_INTEGRATION_MAIL_MANAGER_ERROR_MONTHLY_LIMIT"] = "每月電子郵件發送配額。";
$MESS["CRM_INTEGRATION_MAIL_MANAGER_ERROR_NO_FROM"] = "沒有適合發出電子郵件的郵箱。";
$MESS["CRM_INTEGRATION_MAIL_MANAGER_ERROR_TOO_BIG_ATTACHMENT"] = "超過最大可能的附件尺寸（＃size＃）。";
$MESS["CRM_INTEGRATION_MAIL_MANAGER_ERROR_TO_BLACKLISTED"] = "由於收件人＃電子郵件＃被黑名單，因此無法發送您的電子郵件。";
$MESS["CRM_INTEGRATION_MAIL_MANAGER_ERROR_UNKNOWN"] = "無法發送消息。";
$MESS["CRM_INTEGRATION_MAIL_MANAGER_HISTORY_EVENT_TEXT"] = "主題：＃主題＃<br/>來自：＃從＃<br/>到：＃＃<br/> <br/> <br/>＃Message_body＃";
