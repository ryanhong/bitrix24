<?php
$MESS["CRM_ORDER_AJAX_ERROR"] = "錯誤處理請求";
$MESS["CRM_ORDER_AJAX_ERROR_AD"] = "請求未能處理。最可能的原因包括：用戶權限不足； PHP會話存儲故障； PHP或Web服務器縮短了發布請求的數據。";
