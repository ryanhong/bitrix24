<?php
$MESS["CRM_RESERVATION_INVENTORY_MANAGEMENT_ERROR"] = "無法完成交易，因為倉庫中的某些產品不可用，或者您選擇了錯誤的倉庫。這導致銷售訂單不在庫存管理中註冊。請從交易中刪除您無法交付的產品或選擇其他庫存的其他倉庫。";
