<?php
$MESS["CRM_CALL_LIST_LICENSE_POPUP_CONTENT"] = "使用呼叫列表來構成期望您的呼叫的客戶列表，並為分配的負責人創建各自的任務。";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_FOOTER"] = "呼叫列表和其他有用的功能可以從Plus Plan中獲得，僅需\ $ 39/mo。";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_FOOTER_2"] = "可以在<a href= \"/settings/license_all.php \" target= \"_blank \">選定的商業計劃</a>中獲得呼叫列表和其他有用功能。";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_HEADER"] = "商業計劃可用";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_1"] = "單擊幾下創建呼叫列表";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_2"] = "在單個窗口中一個一個一個端打電話給所有客戶";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_3"] = "控制進度並調用結果";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_SHOW_MORE"] = "了解更多";
$MESS["CRM_CALL_LIST_LIMIT_ERROR"] = "超過最大項目。將過濾器設置為選擇不超過＃limit＃項目。";
$MESS["CRM_CALL_LIST_SUBJECT"] = "通話列表";
$MESS["CRM_CALL_LIST_UPDATE"] = "添加到呼叫列表";
