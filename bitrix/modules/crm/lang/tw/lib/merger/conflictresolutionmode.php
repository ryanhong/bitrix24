<?php
$MESS["CRM_MERGE_CONFLICT_RESOLVE_ALWAYS_OVERWRITE"] = "總是覆蓋";
$MESS["CRM_MERGE_CONFLICT_RESOLVE_ASK_USER"] = "用戶自定義";
$MESS["CRM_MERGE_CONFLICT_RESOLVE_MANUAL"] = "手動合併";
$MESS["CRM_MERGE_CONFLICT_RESOLVE_NEVER_OVERWRITE"] = "永不覆蓋";
