<?php
$MESS["CRM_CONTACT_MERGER_COLLISION_READ_PERMISSION"] = "＃user_name＃已合併了一個contact \“＃seed_title＃\” [＃seed_id＃]與\“＃targ_title＃\” [＃targ_id＃]，您由於訪問權限偏好而無法查看。";
$MESS["CRM_CONTACT_MERGER_COLLISION_READ_UPDATE_PERMISSION"] = "＃user_name＃已合併了一個contact \“＃seed_title＃\” [＃seed_id＃]與\“＃targ_title＃\” [＃targ_id＃]，由於訪問權限偏好而無法查看或編輯。";
$MESS["CRM_CONTACT_MERGER_COLLISION_UPDATE_PERMISSION"] = "＃user_name＃已合併了一個contact \“＃seed_title＃\” [＃seed_id＃]與\“＃targ_title＃\” [＃targ_id＃]，您由於訪問權限偏好而無法編輯。";
