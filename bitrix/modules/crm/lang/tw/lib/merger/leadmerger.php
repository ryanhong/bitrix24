<?php
$MESS["CRM_LEAD_MERGER_COLLISION_READ_PERMISSION"] = "＃user_name＃已與\ \“ \”＃seed_title＃\“ [＃SEED_TITLE＃\“ \”＃TARG_TITLE＃\” [＃TARG_ID＃]您無法查看由於訪問權限偏好而無法查看。";
$MESS["CRM_LEAD_MERGER_COLLISION_READ_UPDATE_PERMISSION"] = "＃user_name＃已與\ \“ \”＃seed_title＃\“ [＃SEED_TITLE＃\”與“ \”＃TARG_TITLE＃\“ [＃TARG_ID＃]您無法查看或編輯由於訪問權限偏好而無法查看或編輯。";
$MESS["CRM_LEAD_MERGER_COLLISION_UPDATE_PERMISSION"] = "＃user_name＃已與\ \“ \”＃seed_title＃\“ [＃SEED_TITLE＃]合併了\”＃TARG_TITLE＃\“ [＃TARG_ID＃]，您由於訪問權限偏好而無法編輯它。";
