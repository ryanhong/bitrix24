<?php
$MESS["CRM_INV_RQ_CONV_ERROR_GENERAL"] = "一般錯誤。";
$MESS["CRM_INV_RQ_CONV_ERROR_PERSON_TYPE_NOT_FOUND"] = "找不到付款人類型。";
$MESS["CRM_INV_RQ_CONV_ERROR_PRESET_NOT_BOUND"] = "所選模板沒有使用現有詳細信息填充的字段。";
$MESS["CRM_INV_RQ_CONV_ERROR_PROPERTY_NOT_FOUND"] = "由於刪除了現有的發票詳細信息，因此無法傳輸數據。";
