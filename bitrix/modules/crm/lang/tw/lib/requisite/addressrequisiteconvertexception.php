<?php
$MESS["CRM_ADDR_CONV_EX_COMPANY_ACCESS_DENIED"] = "進入公司被拒絕";
$MESS["CRM_ADDR_CONV_EX_COMPANY_CREATION_FAILED"] = "無法創建公司詳細信息項目";
$MESS["CRM_ADDR_CONV_EX_CONTACT_ACCESS_DENIED"] = "訪問接觸被拒絕";
$MESS["CRM_ADDR_CONV_EX_CONTACT_CREATION_FAILED"] = "無法創建聯繫方式項目";
