<?php
$MESS["CRM_PS_RQ_CONV_ACC_NUM_DEF_VAL"] = "帳號";
$MESS["CRM_PS_RQ_CONV_INTRO"] = "您的公司詳細信息對許多Bitrix24實體都很重要。發票，報價，支付系統和Bitrix24的其他部分需要提供公司詳細信息。<br> <br>您不再需要每次手動輸入公司詳細信息。只需輸入一次信息，然後在任何地方使用它。 <br> <br>為了使用現有詳細信息，請將付款系統轉換為公司詳細信息。這些詳細信息將在轉換完成後自動插入現有發票中。<br> <br> <br> <a id = \"#exec_id# \“href= \ \"#exec_url# \ \"> convert </a> <a id = a id = \“＃skip_id＃\” href = \“＃skip_url＃\”> skip </a>";
$MESS["CRM_PS_RQ_CONV_PRESET_NAME"] = "賣方";
