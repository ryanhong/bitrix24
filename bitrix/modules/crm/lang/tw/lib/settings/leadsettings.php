<?php
$MESS["CRM_LEAD_BATCH_CONVERSION_COMPLETED"] = "線索已轉換。";
$MESS["CRM_LEAD_BATCH_CONVERSION_COUNT_FAILED"] = "無法轉換：＃number_leads＃。";
$MESS["CRM_LEAD_BATCH_CONVERSION_COUNT_SUCCEEDED"] = "成功轉換：＃number_leads＃。";
$MESS["CRM_LEAD_BATCH_CONVERSION_NO_NAME"] = "無名";
$MESS["CRM_LEAD_BATCH_CONVERSION_STATE"] = "＃處理的＃＃總計＃";
$MESS["CRM_LEAD_BATCH_CONVERSION_TITLE"] = "轉換線索";
$MESS["CRM_LEAD_CONVERT_TEXT"] = "簡單的CRM模式將轉換為交易。 <br/> <br/>
開放線索將在此模式下轉換為交易和客戶。 <br/> <br/>
為創建潛在客戶和交易而配置的所有自動化規則和業務流程將自動並立即為新創建的實體執行。";
$MESS["CRM_LEAD_CONVERT_TITLE"] = "轉換線索";
$MESS["CRM_LEAD_SETTINGS_CRM_TYPE_MENU_ITEM"] = "CRM模式";
$MESS["CRM_LEAD_SETTINGS_VIEW_KANBAN"] = "鉛看板";
$MESS["CRM_LEAD_SETTINGS_VIEW_LIST"] = "線索列表";
$MESS["CRM_LEAD_SETTINGS_VIEW_WIDGET"] = "鉛分析";
$MESS["CRM_ROBOTS_TEXT"] = "在簡單的CRM模式下，Bitrix24自動將新線索轉換為使用自動化規則的交易。<br/> <br/>
您已經擁有當前活動的潛在客戶自動化規則。啟用此模式將刪除這些規則。 <br/> <br/>
您確定要啟用簡單的CRM模式嗎？";
$MESS["CRM_ROBOTS_TITLE"] = "簡單的CRM模式";
$MESS["CRM_TYPE_CANCEL"] = "取消";
$MESS["CRM_TYPE_CONTINUE"] = "繼續";
$MESS["CRM_TYPE_SAVE"] = "節省";
$MESS["CRM_TYPE_TITLE"] = "選擇您想與CRM一起工作的方式";
$MESS["CRM_TYPE_TURN_ON"] = "使能夠";
