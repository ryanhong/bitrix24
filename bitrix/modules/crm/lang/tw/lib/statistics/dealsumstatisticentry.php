<?php
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD"] = "報告要求您<a id = \"#id# \" href= \"#url# \">更新交易統計</a>正確運行。";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD_DLG_SUMMARY"] = "這將更新交易統計信息。這可能需要很長時間。";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD_DLG_TITLE"] = "更新交易統計";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_SLOT_SUM_TOTAL"] = "總和";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_TITLE"] = "交易";
