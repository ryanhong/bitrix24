<?php
$MESS["CRM_STAT_MGR_REBUILD_LEAD_CONVERSION_TITLE"] = "重建鉛轉換統計";
$MESS["CRM_STAT_MGR_REBUILD_STATE_TEMPLATE"] = "＃處理的＃＃總計＃";
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS"] = "請<a id = \"#id# \" href= \"#url# \">更新統計信息</a>以顯示正確的數據。";
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS_DLG_SUMMARY"] = "這將更新統計數據。這可能需要很長時間。";
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS_DLG_TITLE"] = "更新統計";
