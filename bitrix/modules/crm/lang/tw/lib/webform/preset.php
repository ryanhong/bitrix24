<?php
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_BUTTON_CAPTION"] = "給我回電";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_CALL_TEXT"] = "已要求回電；現在將您與客戶聯繫。";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_CAPTION"] = "找不到您想要的東西嗎？";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_DESCRIPTION"] = "還有問題嗎？我們會給你回電話！";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_NAME"] = "來自\“＃call_from＃\”的回調";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_RESULT_SUCCESS_TEXT"] = "我們的代表將在幾秒鐘內與您聯繫。";
$MESS["CRM_WEBFORM_PRESET_ITEM_CD_CAPTION"] = "聯繫信息";
$MESS["CRM_WEBFORM_PRESET_ITEM_CD_DESCRIPTION"] = "請留下您的聯繫信息，我們將與您聯繫";
$MESS["CRM_WEBFORM_PRESET_ITEM_CD_NAME"] = "聯繫信息";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_COMMENTS"] = "評論";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_EMAIL"] = "電子郵件";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_LAST_NAME"] = "姓";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_NAME"] = "名";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_PHONE"] = "電話";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_RESULT_FAILURE_TEXT"] = "不幸的是，該消息尚未發送，請稍後再試。";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_RESULT_SUCCESS_TEXT"] = "謝謝，您的消息已發送。";
$MESS["CRM_WEBFORM_PRESET_ITEM_FB_CAPTION"] = "回饋";
$MESS["CRM_WEBFORM_PRESET_ITEM_FB_DESCRIPTION"] = "請在評論字段中分享您的反饋";
$MESS["CRM_WEBFORM_PRESET_ITEM_FB_NAME"] = "反饋表";
$MESS["CRM_WEBFORM_PRESET_ITEM_IMOL_REG_DESCRIPTION"] = "請介紹一下你自己";
$MESS["CRM_WEBFORM_PRESET_ITEM_IMOL_REG_NAME"] = "聯繫信息表格可與開放渠道一起使用";
$MESS["CRM_WEBFORM_PRESET_ITEM_WA_DESCRIPTION"] = "輸入您的電話號碼，我們的經理將在WhatsApp與您聯繫";
