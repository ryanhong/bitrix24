<?php
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_INVALID_INPUT_RESPONSE"] = "垃圾郵件檢查失敗了。";
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_INVALID_INPUT_SECRET"] = "recaptcha：秘密密鑰不正確。";
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_MISSING_INPUT_RESPONSE"] = "recaptcha：未提供響應。
";
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_MISSING_INPUT_SECRET"] = "recaptcha：未指定秘密密鑰。";
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_UNKNOWN"] = "未知錯誤..";
