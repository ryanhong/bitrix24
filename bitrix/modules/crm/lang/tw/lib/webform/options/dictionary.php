<?php
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_ACTION_HIDE"] = "隱藏";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_ACTION_SHOW"] = "展示";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_EVENT_CHANGE"] = "變化";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_ANY"] = "任何";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_ANY1"] = "不是空的";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_CONTAIN"] = "包含";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_EMPTY"] = "空的";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_EMPTY1"] = "空的";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_EQUAL"] = "等於";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_GREATER"] = "多於";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_GREATEROREQUAL"] = "超過或等於";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_LESS"] = "少於";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_LESSOREQUAL"] = "小於或等於";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_NOTCONTAIN"] = "不包含";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_NOTEQUAL"] = "不等於";
$MESS["CRM_WEBFORM_OPTIONS_DICT_FIELD_FILE_CONTENT_TYPE_ARCHIVE"] = "檔案";
$MESS["CRM_WEBFORM_OPTIONS_DICT_FIELD_FILE_CONTENT_TYPE_ARCHIVE_HINT"] = "存檔文件：zip，tar，rar和7 zip文件。";
$MESS["CRM_WEBFORM_OPTIONS_DICT_FIELD_FILE_CONTENT_TYPE_AUDIO"] = "聲音的";
$MESS["CRM_WEBFORM_OPTIONS_DICT_FIELD_FILE_CONTENT_TYPE_DOC_HINT"] = "文檔文件：PDF文件，演示文稿（.ppt，.pptx），電子表格（.xls，xlsx，.csv），文本文件（.txt，.doc，.docx）和其他一些文件。";
$MESS["CRM_WEBFORM_OPTIONS_DICT_FIELD_FILE_CONTENT_TYPE_DOC_MSGVER_1"] = "文件";
$MESS["CRM_WEBFORM_OPTIONS_DICT_FIELD_FILE_CONTENT_TYPE_IMAGE"] = "圖片";
$MESS["CRM_WEBFORM_OPTIONS_DICT_FIELD_FILE_CONTENT_TYPE_VIDEO"] = "影片";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_COMPANY_NAME"] = "公司名稱";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_EMAIL"] = "電子郵件";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_LASTNAME"] = "姓";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_NAME"] = "名";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_PHONE"] = "電話";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_SECOND_NAME"] = "第二個名字";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_DOMAIN"] = "域名";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_DOMAIN_DESC"] = "提交表格的域名";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_FORM_ID"] = "形式ID";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_FORM_ID_DESC"] = "形式ID";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_FORM_NAME"] = "表格名稱";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_FORM_NAME_DESC"] = "表格名稱";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_PARAM"] = "範圍";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_PARAM_DESC"] = "您將用來識別表單的URL參數的名稱";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_RESULT_ID"] = "結果ID";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_RESULT_ID_DESC"] = "指定分配給表單結果的數字。";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_URL"] = "頁面地址";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_URL_DESC"] = "提交表格的頁面地址";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_BUTTON_TYPE_BUTTON"] = "按鈕";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_BUTTON_TYPE_LINK"] = "關聯";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_FONT_CLASSIC"] = "經典的";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_FONT_ELEGANT"] = "斜";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_FONT_MODERN"] = "現代的";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_CENTER"] = "中心";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_INLINE"] = "排隊";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_LEFT"] = "左邊";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_RIGHT"] = "正確的";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_TYPE_PANEL"] = "側邊欄";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_TYPE_POPUP"] = "彈出窗口";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_VERT_BOTTOM"] = "底部";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_VERT_TOP"] = "頂部";
