<?php
$MESS["CRM_DEAL_ENTITY_ACCOUNT_CURRENCY_ID_FIELD"] = "貨幣ID";
$MESS["CRM_DEAL_ENTITY_ADDITIONAL_INFO_FIELD"] = "附加信息";
$MESS["CRM_DEAL_ENTITY_ASSIGNED_BY_FIELD"] = "負責人";
$MESS["CRM_DEAL_ENTITY_ASSIGNED_BY_ID_FIELD"] = "負責人";
$MESS["CRM_DEAL_ENTITY_BEGINDATE_FIELD"] = "開始日期";
$MESS["CRM_DEAL_ENTITY_BEGINDATE_SHORT_FIELD"] = "開始日期";
$MESS["CRM_DEAL_ENTITY_CATEGORY_ID_FIELD"] = "交易管道";
$MESS["CRM_DEAL_ENTITY_CLOSEDATE_FIELD"] = "假定截止日期";
$MESS["CRM_DEAL_ENTITY_CLOSEDATE_SHORT_FIELD"] = "假定截止日期";
$MESS["CRM_DEAL_ENTITY_CLOSED_FIELD"] = "關閉";
$MESS["CRM_DEAL_ENTITY_COMMENTS_FIELD"] = "評論";
$MESS["CRM_DEAL_ENTITY_COMPANY_BY_FIELD"] = "公司";
$MESS["CRM_DEAL_ENTITY_CONTACT_BY_FIELD"] = "接觸";
$MESS["CRM_DEAL_ENTITY_CREATED_BY_FIELD"] = "由...製作";
$MESS["CRM_DEAL_ENTITY_CREATED_BY_ID_FIELD"] = "由...製作";
$MESS["CRM_DEAL_ENTITY_CURRENCY_BY_FIELD"] = "貨幣";
$MESS["CRM_DEAL_ENTITY_CURRENCY_ID_FIELD"] = "貨幣";
$MESS["CRM_DEAL_ENTITY_DATE_CREATE_FIELD"] = "創建於";
$MESS["CRM_DEAL_ENTITY_DATE_CREATE_SHORT_FIELD"] = "創建於";
$MESS["CRM_DEAL_ENTITY_DATE_MODIFY_FIELD"] = "修改";
$MESS["CRM_DEAL_ENTITY_DATE_MODIFY_SHORT_FIELD"] = "修改";
$MESS["CRM_DEAL_ENTITY_DEAL_EVENT_FIELD"] = "交易活動";
$MESS["CRM_DEAL_ENTITY_EVENT_BY_FIELD"] = "事件類型";
$MESS["CRM_DEAL_ENTITY_EVENT_DATE_FIELD"] = "活動日期";
$MESS["CRM_DEAL_ENTITY_EVENT_DATE_SHORT_FIELD"] = "活動日期";
$MESS["CRM_DEAL_ENTITY_EVENT_DESCRIPTION_FIELD"] = "事件描述";
$MESS["CRM_DEAL_ENTITY_EVENT_ID_FIELD"] = "事件類型";
$MESS["CRM_DEAL_ENTITY_EVENT_RELATION_FIELD"] = "事件";
$MESS["CRM_DEAL_ENTITY_HAS_PRODUCTS_FIELD"] = "包含產品";
$MESS["CRM_DEAL_ENTITY_ID_FIELD"] = "ID";
$MESS["CRM_DEAL_ENTITY_IS_LOSE_FIELD"] = "丟失的";
$MESS["CRM_DEAL_ENTITY_IS_NEW_FIELD"] = "新交易";
$MESS["CRM_DEAL_ENTITY_IS_RECURRING_FIELD"] = "經常性交易";
$MESS["CRM_DEAL_ENTITY_IS_REPEATED_APPROACH_FIELD"] = "重複查詢";
$MESS["CRM_DEAL_ENTITY_IS_RETURN_CUSTOMER_FIELD"] = "重複交易";
$MESS["CRM_DEAL_ENTITY_IS_WON_FIELD"] = "韓元";
$MESS["CRM_DEAL_ENTITY_IS_WORK_FIELD"] = "進行中";
$MESS["CRM_DEAL_ENTITY_LEAD_BY_FIELD"] = "帶領";
$MESS["CRM_DEAL_ENTITY_LOST_AMOUNT_FIELD"] = "損失";
$MESS["CRM_DEAL_ENTITY_MODIFY_BY_FIELD"] = "修改";
$MESS["CRM_DEAL_ENTITY_MODIFY_BY_ID_FIELD"] = "修改";
$MESS["CRM_DEAL_ENTITY_OPENED_FIELD"] = "每個人都可以使用";
$MESS["CRM_DEAL_ENTITY_OPPORTUNITY_ACCOUNT_FIELD"] = "預期金額";
$MESS["CRM_DEAL_ENTITY_OPPORTUNITY_FIELD"] = "全部的";
$MESS["CRM_DEAL_ENTITY_ORDER_STAGE_FIELD"] = "交易付款狀態";
$MESS["CRM_DEAL_ENTITY_ORIGINATOR_BY_FIELD"] = "捆綁";
$MESS["CRM_DEAL_ENTITY_PROBABILITY_FIELD"] = "概率，％";
$MESS["CRM_DEAL_ENTITY_PRODUCT_BY_FIELD"] = "產品";
$MESS["CRM_DEAL_ENTITY_PRODUCT_ID_FIELD"] = "產品";
$MESS["CRM_DEAL_ENTITY_RECEIVED_AMOUNT_FIELD"] = "收到的金額";
$MESS["CRM_DEAL_ENTITY_STAGE_BY_FIELD"] = "交易階段";
$MESS["CRM_DEAL_ENTITY_STAGE_ID_FIELD"] = "交易階段";
$MESS["CRM_DEAL_ENTITY_STAGE_SEMANTIC_ID_FIELD"] = "舞台組";
$MESS["CRM_DEAL_ENTITY_STATE_ID_FIELD"] = "狀態";
$MESS["CRM_DEAL_ENTITY_TITLE_FIELD"] = "姓名";
$MESS["CRM_DEAL_ENTITY_TYPE_BY_FIELD"] = "類型";
$MESS["CRM_DEAL_ENTITY_TYPE_ID_FIELD"] = "類型";
$MESS["CRM_DEAL_ENTITY_WEBFORM_ID_FIELD"] = "由CRM形式創建";
