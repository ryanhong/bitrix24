<?php
$MESS["CRM_COMMENT_IM_MENTION_POST"] = "在對\ \“＃entity_title＃\”的評論中提到您，註釋文本：\“＃comment＃\”";
$MESS["CRM_COMMENT_IM_MENTION_POST_F"] = "在對\ \“＃entity_title＃\”的評論中提到您，註釋文本：\“＃comment＃\”";
$MESS["CRM_COMMENT_IM_MENTION_POST_M"] = "在對\ \“＃entity_title＃\”的評論中提到您，註釋文本：\“＃comment＃\”";
$MESS["CRM_ENTITY_TITLE_COMPANY"] = "公司＃entity_name＃";
$MESS["CRM_ENTITY_TITLE_CONTACT"] = "聯繫＃entity_name＃";
$MESS["CRM_ENTITY_TITLE_DEAL"] = "交易＃entity_name＃";
$MESS["CRM_ENTITY_TITLE_DYNAMIC"] = "項目＃entity_name＃";
$MESS["CRM_ENTITY_TITLE_INVOICE"] = "發票＃entity_name＃";
$MESS["CRM_ENTITY_TITLE_LEAD"] = "線索＃entity_name＃";
$MESS["CRM_ENTITY_TITLE_ORDER"] = "訂單＃entity_name＃";
$MESS["CRM_ENTITY_TITLE_QUOTE_MSGVER_1"] = "估計＃entity_name＃";
$MESS["CRM_ENTITY_TITLE_SHIPMENT_DOCUMENT"] = "銷售訂單＃entity_name＃";
$MESS["CRM_ENTITY_TITLE_STORE_DOCUMENT"] = "庫存對象＃entity_name＃";
