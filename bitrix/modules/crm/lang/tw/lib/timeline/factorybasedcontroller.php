<?php
$MESS["CRM_TIMELINE_FACTORYBASED_TITLE_CATEGORY_CHANGE"] = "管道改變了";
$MESS["CRM_TIMELINE_FACTORYBASED_TITLE_CREATION"] = "創建的項目";
$MESS["CRM_TIMELINE_FACTORYBASED_TITLE_MODIFICATION"] = "\“＃field_name＃\”更改的值";
$MESS["CRM_TIMELINE_FACTORYBASED_TITLE_MOVE"] = "階段改變了";
$MESS["CRM_TIMELINE_FACTORYBASED_TITLE_RESTORATION"] = "從回收箱中回收的CRM項目";
