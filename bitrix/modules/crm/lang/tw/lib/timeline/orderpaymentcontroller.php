<?php
$MESS["CRM_ORDER_PAYMENT_CREATION"] = "付款創建";
$MESS["CRM_PAYMENT_BASE_CAPTION_BASED_ON_ORDER"] = "根據訂單添加";
$MESS["CRM_PAYMENT_CREATION_MESSAGE"] = "## account_number＃來自＃date_bill＃＃";
$MESS["CRM_PAYMENT_CREATION_MESSAGE_SUM"] = "（應付金額：＃sum_with_currency＃）";
$MESS["CRM_PAYMENT_PAID_TITLE"] = "＃＃帳號＃";
$MESS["CRM_PAYMENT_PAYSYSTEM_CLICK_TITLE"] = "選擇付款方式";
$MESS["CRM_PAYMENT_PAYSYSTEM_VIEWED_HTML_TITLE"] = "## account_number＃of＃date＃for＃sum＃";
$MESS["CRM_PAYMENT_PAYSYSTEM_VIEWED_TITLE"] = "客戶打開付款頁面";
