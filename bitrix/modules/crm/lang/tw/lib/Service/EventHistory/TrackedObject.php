<?php
$MESS["CRM_TRACKED_OBJECT_DEPENDANT_UPDATE_TEXT"] = "字段\“＃field_name＃\”鏈接的entity \“＃entity_name＃\”更改";
$MESS["CRM_TRACKED_OBJECT_ENTITY_ADD_EVENT_NAME"] = "添加到\ \“＃field_name＃\”的鏈接";
$MESS["CRM_TRACKED_OBJECT_ENTITY_DELETE_EVENT_NAME"] = "刪除鏈接到\ \“＃field_name＃\”";
$MESS["CRM_TRACKED_OBJECT_UPDATE_EVENT_NAME"] = "字段\“＃field_name＃\”更改";
