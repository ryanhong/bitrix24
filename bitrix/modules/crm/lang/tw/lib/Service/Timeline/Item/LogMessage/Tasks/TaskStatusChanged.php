<?php
$MESS["TASKS_ON_TASK_STATUS_CHANGED_STATUS_1"] = "新的";
$MESS["TASKS_ON_TASK_STATUS_CHANGED_STATUS_2"] = "待辦的";
$MESS["TASKS_ON_TASK_STATUS_CHANGED_STATUS_3"] = "進行中";
$MESS["TASKS_ON_TASK_STATUS_CHANGED_STATUS_4"] = "待審核";
$MESS["TASKS_ON_TASK_STATUS_CHANGED_STATUS_5"] = "完全的";
$MESS["TASKS_ON_TASK_STATUS_CHANGED_STATUS_6"] = "遞延";
$MESS["TASKS_ON_TASK_STATUS_CHANGED_STATUS_7"] = "拒絕";
$MESS["TASKS_ON_TASK_STATUS_CHANGED_TITLE"] = "任務狀態更改";
$MESS["TASKS_ON_TASK_STATUS_RETURNED_BACK_TO_WORK_TITLE"] = "返回的任務進行修訂";
