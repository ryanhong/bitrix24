<?php
$MESS["CRM_SERVICE_FORM_CATEGORY_CRM"] = "客戶通信";
$MESS["CRM_SERVICE_FORM_CATEGORY_CRM_AUTOMATION"] = "預填充表單字段";
$MESS["CRM_SERVICE_FORM_CATEGORY_CRM_FILLING"] = "將數據添加到CRM";
$MESS["CRM_SERVICE_FORM_CATEGORY_DELIVERY"] = "訂購交付";
$MESS["CRM_SERVICE_FORM_CATEGORY_DEPENDENCY_FIELD"] = "帶有條件字段的形式";
$MESS["CRM_SERVICE_FORM_CATEGORY_EVENTS"] = "註冊活動";
$MESS["CRM_SERVICE_FORM_CATEGORY_OTHER"] = "其他";
$MESS["CRM_SERVICE_FORM_CATEGORY_PREPARE_FORM"] = "嵌入形式";
$MESS["CRM_SERVICE_FORM_CATEGORY_PRODUCTS"] = "銷售量";
$MESS["CRM_SERVICE_FORM_CATEGORY_SOCIAL"] = "社交媒體命令";
$MESS["CRM_SERVICE_FORM_SCENARIO_CODE_ON_SITE_TEXT"] = "單擊此按鈕以獲取網站的HTML，或將表格添加到小部件";
$MESS["CRM_SERVICE_FORM_SCENARIO_CODE_ON_SITE_TITLE"] = "獲取HTML";
$MESS["CRM_SERVICE_FORM_SCENARIO_PARTICIPATION_FORMAT"] = "事件格式";
$MESS["CRM_SERVICE_FORM_SCENARIO_PARTICIPATION_FORMAT_LIVE"] = "親自";
$MESS["CRM_SERVICE_FORM_SCENARIO_PARTICIPATION_FORMAT_ONLINE"] = "虛擬（在線）";
$MESS["CRM_SERVICE_FORM_SCENARIO_PARTICIPATION_FORMAT_RECORD"] = "我會看錄製的流";
$MESS["CRM_SERVICE_FORM_SCENARIO_PRODUCT2"] = "即時付款的產品選擇器";
$MESS["CRM_WEBFORM_COMMENTS_DELIVERY_DATE"] = "所需的交貨日期和時間";
