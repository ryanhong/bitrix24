<?php
$MESS["CRM_FIELD_CRATED_TIME_VALUE_IN_FUTURE_ERROR"] = "\“＃字段＃\”的值不可能在以後";
$MESS["CRM_FIELD_CRATED_TIME_VALUE_NOT_MONOTONE_ERROR"] = "\“＃字段＃\”的值不能比任何其他項目的值少";
$MESS["CRM_FIELD_VALUE_CAN_NOT_BE_GREATER_ERROR"] = "\“＃field1＃\”的值不能大於\“＃field2＃\”";
$MESS["CRM_FIELD_VALUE_ONLY_ADMIN_CAN_SET_ERROR"] = "只有管​​理員才能更改\“＃字段＃\”的值";
