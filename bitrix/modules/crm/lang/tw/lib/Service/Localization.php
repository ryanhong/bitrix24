<?php
$MESS["CRM_COMMON_ACTION_ADD"] = "添加";
$MESS["CRM_COMMON_ACTION_ADD_OBSERVER"] = "添加觀察者";
$MESS["CRM_COMMON_ACTION_CANCEL"] = "取消";
$MESS["CRM_COMMON_ACTION_CONFIG"] = "編輯項目";
$MESS["CRM_COMMON_ACTION_COPY"] = "複製";
$MESS["CRM_COMMON_ACTION_COPY_LINK"] = "複製鏈接";
$MESS["CRM_COMMON_ACTION_CREATE"] = "創造";
$MESS["CRM_COMMON_ACTION_DELETE"] = "刪除";
$MESS["CRM_COMMON_ACTION_DOWNLOAD"] = "下載";
$MESS["CRM_COMMON_ACTION_DOWNLOAD_FORMAT"] = "下載＃格式＃";
$MESS["CRM_COMMON_ACTION_EDIT"] = "編輯";
$MESS["CRM_COMMON_ACTION_OPEN"] = "打開";
$MESS["CRM_COMMON_ACTION_PRINT"] = "列印";
$MESS["CRM_COMMON_ACTION_SAVE"] = "節省";
$MESS["CRM_COMMON_ACTION_SHOW"] = "看法";
$MESS["CRM_COMMON_ACTION_SIGN"] = "符號";
$MESS["CRM_COMMON_ACTIVITY"] = "活動";
$MESS["CRM_COMMON_ADDRESS"] = "地址";
$MESS["CRM_COMMON_ADD_ACCESS_DENIED"] = "您無權查看此項目＃entity_description＃";
$MESS["CRM_COMMON_ASSIGNED_NOT_SET"] = "未分配的負責人";
$MESS["CRM_COMMON_CALENDAR"] = "日曆";
$MESS["CRM_COMMON_CANCEL"] = "取消";
$MESS["CRM_COMMON_CATEGORY"] = "管道";
$MESS["CRM_COMMON_CLIENT"] = "顧客";
$MESS["CRM_COMMON_CLIENT_CATEGORY"] = "類別";
$MESS["CRM_COMMON_CODE"] = "符號代碼";
$MESS["CRM_COMMON_COLOR"] = "顏色";
$MESS["CRM_COMMON_COMPANIES"] = "公司";
$MESS["CRM_COMMON_COMPANY"] = "公司";
$MESS["CRM_COMMON_CONTACT"] = "接觸";
$MESS["CRM_COMMON_CONTACTS"] = "聯繫人";
$MESS["CRM_COMMON_CONTINUE"] = "繼續";
$MESS["CRM_COMMON_CONVERSION"] = "轉變";
$MESS["CRM_COMMON_COPILOT"] = "副駕駛";
$MESS["CRM_COMMON_CREATED_TIME"] = "創建於";
$MESS["CRM_COMMON_DEADLINES"] = "截止日期";
$MESS["CRM_COMMON_DEAL"] = "交易";
$MESS["CRM_COMMON_DEALS"] = "交易";
$MESS["CRM_COMMON_DETAIL"] = "細節";
$MESS["CRM_COMMON_DOCUMENT"] = "文件";
$MESS["CRM_COMMON_EMPTY"] = "空的";
$MESS["CRM_COMMON_EMPTY_VALUE"] = "空的";
$MESS["CRM_COMMON_ERROR_ACCESS_DENIED"] = "拒絕訪問";
$MESS["CRM_COMMON_ERROR_DYNAMIC_DISABLED_MSGVER_1"] = "智能過程自動化不可用";
$MESS["CRM_COMMON_ERROR_GENERAL"] = "通用錯誤";
$MESS["CRM_COMMON_GRID_NO"] = "不";
$MESS["CRM_COMMON_GRID_YES"] = "是的";
$MESS["CRM_COMMON_HELP"] = "幫助";
$MESS["CRM_COMMON_HIDDEN_ITEM"] = "隱藏的項目";
$MESS["CRM_COMMON_HIDDEN_VALUE"] = "隱";
$MESS["CRM_COMMON_INVOICE"] = "發票";
$MESS["CRM_COMMON_INVOICES"] = "發票";
$MESS["CRM_COMMON_IS_MANUAL_OPPORTUNITY_FALSE"] = "使用產品價格自動計算";
$MESS["CRM_COMMON_IS_MANUAL_OPPORTUNITY_TRUE"] = "手動的";
$MESS["CRM_COMMON_KANBAN"] = "看板";
$MESS["CRM_COMMON_LEAD"] = "帶領";
$MESS["CRM_COMMON_LEADS"] = "鉛";
$MESS["CRM_COMMON_LIST"] = "列表";
$MESS["CRM_COMMON_MODIFY_DATE"] = "修改";
$MESS["CRM_COMMON_NO"] = "不";
$MESS["CRM_COMMON_NOT_SELECTED"] = "未選中的";
$MESS["CRM_COMMON_PIPELINE"] = "管道";
$MESS["CRM_COMMON_PRODUCTS"] = "產品";
$MESS["CRM_COMMON_QUOTES"] = "引號";
$MESS["CRM_COMMON_QUOTE_MSGVER_1"] = "估計";
$MESS["CRM_COMMON_READ_ACCESS_DENIED"] = "您無權查看此項目。";
$MESS["CRM_COMMON_ROBOTS"] = "自動化規則";
$MESS["CRM_COMMON_SETTINGS"] = "設定";
$MESS["CRM_COMMON_TITLE"] = "姓名";
$MESS["CRM_COMMON_UNTITLED"] = "無標題";
$MESS["CRM_COMMON_UPDATED_BY"] = "修改";
$MESS["CRM_COMMON_UTM"] = "UTM參數";
$MESS["CRM_COMMON_YES"] = "是的";
$MESS["CRM_COMPANY_ADD_HINT"] = "您無權創建公司。請聯繫您的BitRix24管理員或負責CRM的人員。";
$MESS["CRM_CONTACT_ADD_HINT"] = "您無權創建聯繫人。請聯繫您的BitRix24管理員或負責CRM的人員。";
$MESS["CRM_DEAL_ADD_HINT"] = "您無權創建交易。請聯繫您的BitRix24管理員或負責CRM的人員。";
$MESS["CRM_DELETE_ERROR"] = "刪除對象時發生錯誤：＃錯誤＃。";
$MESS["CRM_FEATURE_RESTRICTION_ERROR"] = "您當前的計劃無法使用此操作";
$MESS["CRM_FEATURE_RESTRICTION_GRID_TEXT"] = "您無法訪問此數據，因為您已降級到次要計劃之一。請更新您以前的計劃以訪問。";
$MESS["CRM_FEATURE_RESTRICTION_GRID_TITLE"] = "無法訪問數據";
$MESS["CRM_LEAD_ADD_HINT"] = "您無權創建潛在客戶。請聯繫您的BitRix24管理員或負責CRM的人員。";
$MESS["CRM_LOCALIZATION_FIELD_VALUE_WITH_TITLE"] = "＃標題＃值＃";
$MESS["CRM_OLD_VERSION_SUFFIX"] = "＃標題＃（舊版本）";
$MESS["CRM_ORDER_TITLE"] = "訂單## order_id＃of＃order_date＃＃";
$MESS["CRM_QUOTE_ADD_HINT_MSGVER_1"] = "您無權創建估計。請聯繫您的BitRix24管理員或負責CRM的人員。";
$MESS["CRM_QUOTE_TITLE_MSGVER_1"] = "估計## quote_number＃of＃begindate＃";
$MESS["CRM_QUOTE_TITLE_PLACEHOLDER_MSGVER_1"] = "估計## ID＃";
$MESS["CRM_SMART_INVOICE_ADD_HINT"] = "您無權創建發票。請聯繫您的BitRix24管理員或負責CRM的人員。";
$MESS["CRM_SMART_INVOICE_TITLE"] = "發票##編號＃＃begindate＃of＃";
$MESS["CRM_SMART_INVOICE_TITLE_PLACEHOLDER"] = "發票## ID＃";
$MESS["CRM_TYPE_CATEGORY_ACCESS_DENIED"] = "拒絕使用管道";
$MESS["CRM_TYPE_CATEGORY_ADD_ERROR_SYSTEM"] = "無法添加系統管道";
$MESS["CRM_TYPE_CATEGORY_DEFAULT_NAME"] = "新管道";
$MESS["CRM_TYPE_CATEGORY_DELETE_ERROR_SYSTEM"] = "無法刪除系統管道";
$MESS["CRM_TYPE_CATEGORY_NOT_FOUND_ERROR"] = "沒有發現管道";
$MESS["CRM_TYPE_CATEGORY_SETTINGS"] = "管道";
$MESS["CRM_TYPE_CATEGORY_SORT_TITLE"] = "種類";
$MESS["CRM_TYPE_ENTITY_TYPE_ID_LIMIT_ERROR_MSGVER_1"] = "智能過程自動化最大限制超出了。請聯繫Helpdesk。";
$MESS["CRM_TYPE_ENTITY_TYPE_ID_TITLE_MSGVER_1"] = "類型ID";
$MESS["CRM_TYPE_FILTER_PRESET_IN_WORK"] = "進行中";
$MESS["CRM_TYPE_FILTER_PRESET_MY"] = "我的物品";
$MESS["CRM_TYPE_FILTER_PRESET_SUCCESS"] = "完成的";
$MESS["CRM_TYPE_ITEM_ADD_HINT"] = "您無權創建水療項目。請聯繫您的BitRix24管理員或負責CRM的人員。";
$MESS["CRM_TYPE_ITEM_DELETE"] = "刪除項目";
$MESS["CRM_TYPE_ITEM_DELETE_CONFIRMATION_MESSAGE"] = "你確定要刪除這個項目嗎？";
$MESS["CRM_TYPE_ITEM_DELETE_CONFIRMATION_TITLE"] = "刪除項目";
$MESS["CRM_TYPE_ITEM_DELETE_NOTIFICATION"] = "項目已刪除";
$MESS["CRM_TYPE_ITEM_DETAILS_TAB_AUTOMATION"] = "自動化規則";
$MESS["CRM_TYPE_ITEM_DETAILS_TAB_BIZPROC"] = "工作流程";
$MESS["CRM_TYPE_ITEM_DETAILS_TAB_HISTORY"] = "歷史";
$MESS["CRM_TYPE_ITEM_DETAILS_TAB_TREE"] = "依賴性";
$MESS["CRM_TYPE_ITEM_EDITOR_SECTION_ADDITIONAL"] = "更多的";
$MESS["CRM_TYPE_ITEM_EXPORT_CSV"] = "CSV導出";
$MESS["CRM_TYPE_ITEM_EXPORT_EXCEL"] = "Microsoft Excel導出";
$MESS["CRM_TYPE_ITEM_FIELD_ACCOUNT_CURRENCY_ID"] = "會計貨幣";
$MESS["CRM_TYPE_ITEM_FIELD_ACTUAL_DATE"] = "有效直到為止";
$MESS["CRM_TYPE_ITEM_FIELD_ASSIGNED_BY_ID"] = "負責人";
$MESS["CRM_TYPE_ITEM_FIELD_BEGINDATE"] = "開始";
$MESS["CRM_TYPE_ITEM_FIELD_BIRTHDATE"] = "出生日期";
$MESS["CRM_TYPE_ITEM_FIELD_CLOSEDATE"] = "完成";
$MESS["CRM_TYPE_ITEM_FIELD_COMMENTS"] = "評論";
$MESS["CRM_TYPE_ITEM_FIELD_COMPANY_ID"] = "公司";
$MESS["CRM_TYPE_ITEM_FIELD_CONTACT_ID"] = "接觸";
$MESS["CRM_TYPE_ITEM_FIELD_CONTENT"] = "內容";
$MESS["CRM_TYPE_ITEM_FIELD_CREATED_BY"] = "由...製作";
$MESS["CRM_TYPE_ITEM_FIELD_CREATED_BY_FEMININE"] = "由...製作";
$MESS["CRM_TYPE_ITEM_FIELD_CREATED_TIME"] = "創建於";
$MESS["CRM_TYPE_ITEM_FIELD_CREATED_TIME_FEMININE"] = "創建日期";
$MESS["CRM_TYPE_ITEM_FIELD_CURRENCY_ID"] = "貨幣";
$MESS["CRM_TYPE_ITEM_FIELD_HAS_EMAIL"] = "有電子郵件";
$MESS["CRM_TYPE_ITEM_FIELD_HAS_IMOL"] = "有開放的頻道";
$MESS["CRM_TYPE_ITEM_FIELD_HAS_PHONE"] = "有電話";
$MESS["CRM_TYPE_ITEM_FIELD_HONORIFIC"] = "致敬";
$MESS["CRM_TYPE_ITEM_FIELD_IS_MANUAL_OPPORTUNITY"] = "數量計算模式";
$MESS["CRM_TYPE_ITEM_FIELD_LAST_ACTIVITY_BY"] = "最後的時間表活動";
$MESS["CRM_TYPE_ITEM_FIELD_LAST_ACTIVITY_TIME"] = "最後時間線活動時間";
$MESS["CRM_TYPE_ITEM_FIELD_LAST_ACTIVITY_TIME_2"] = "最後更新";
$MESS["CRM_TYPE_ITEM_FIELD_LAST_NAME"] = "姓";
$MESS["CRM_TYPE_ITEM_FIELD_LOCATION"] = "地點";
$MESS["CRM_TYPE_ITEM_FIELD_MOVED_BY"] = "移動";
$MESS["CRM_TYPE_ITEM_FIELD_MOVED_BY_FEMININE"] = "移動";
$MESS["CRM_TYPE_ITEM_FIELD_MOVED_TIME"] = "繼續前進";
$MESS["CRM_TYPE_ITEM_FIELD_MOVED_TIME_FEMININE"] = "日期移動";
$MESS["CRM_TYPE_ITEM_FIELD_MYCOMPANY_ID"] = "您的公司詳細信息";
$MESS["CRM_TYPE_ITEM_FIELD_NAME"] = "名";
$MESS["CRM_TYPE_ITEM_FIELD_NAME_OPPORTUNITY_WITH_CURRENCY"] = "數量和貨幣";
$MESS["CRM_TYPE_ITEM_FIELD_OBSERVERS"] = "觀察者";
$MESS["CRM_TYPE_ITEM_FIELD_OPENED"] = "每個人都可以使用";
$MESS["CRM_TYPE_ITEM_FIELD_OPPORTUNITY"] = "數量";
$MESS["CRM_TYPE_ITEM_FIELD_OPPORTUNITY_ACCOUNT"] = "金額（以會計貨幣為單位）";
$MESS["CRM_TYPE_ITEM_FIELD_ORIGINATOR_ID"] = "外部源";
$MESS["CRM_TYPE_ITEM_FIELD_ORIGIN_ID"] = "數據源中的項目ID";
$MESS["CRM_TYPE_ITEM_FIELD_ORIGIN_VERSION"] = "原始版本";
$MESS["CRM_TYPE_ITEM_FIELD_POST"] = "位置";
$MESS["CRM_TYPE_ITEM_FIELD_PREVIOUS_STAGE_ID"] = "上一個階段";
$MESS["CRM_TYPE_ITEM_FIELD_SECOND_NAME"] = "中間名字";
$MESS["CRM_TYPE_ITEM_FIELD_SOURCE_DESCRIPTION"] = "來源信息";
$MESS["CRM_TYPE_ITEM_FIELD_SOURCE_ID"] = "來源";
$MESS["CRM_TYPE_ITEM_FIELD_STAGE_ID"] = "階段";
$MESS["CRM_TYPE_ITEM_FIELD_STAGE_SEMANTIC_ID"] = "舞台組";
$MESS["CRM_TYPE_ITEM_FIELD_TAX_VALUE"] = "稅收總計";
$MESS["CRM_TYPE_ITEM_FIELD_TAX_VALUE_ACCOUNT"] = "會計貨幣的稅率";
$MESS["CRM_TYPE_ITEM_FIELD_TERMS"] = "狀況";
$MESS["CRM_TYPE_ITEM_FIELD_UPDATED_BY"] = "更新";
$MESS["CRM_TYPE_ITEM_FIELD_UPDATED_BY_FEMININE"] = "更新";
$MESS["CRM_TYPE_ITEM_FIELD_UPDATED_TIME"] = "更新";
$MESS["CRM_TYPE_ITEM_FIELD_UPDATED_TIME_FEMININE"] = "日期更新";
$MESS["CRM_TYPE_ITEM_FIELD_WEBFORM_ID"] = "由CRM形式創建";
$MESS["CRM_TYPE_ITEM_FIELD_XML_ID"] = "外部ID";
$MESS["CRM_TYPE_ITEM_NOT_FOUND"] = "找不到項目。";
$MESS["CRM_TYPE_ITEM_PARTIAL_EDITOR_TITLE"] = "請完成所需的字段以更改舞台";
$MESS["CRM_TYPE_ITEM_PERMISSIONS_ADD_DENIED"] = "您無權添加新項目";
$MESS["CRM_TYPE_ITEM_PERMISSIONS_DELETE_DENIED"] = "您無權刪除此項目";
$MESS["CRM_TYPE_ITEM_PERMISSIONS_IMPORT_DENIED"] = "您無權導入此項目";
$MESS["CRM_TYPE_ITEM_PERMISSIONS_UPDATE_DENIED"] = "您無權編輯此項目";
$MESS["CRM_TYPE_ITEM_SAVE_EDITOR_AND_RELOAD"] = "請保存數據並刷新頁面";
$MESS["CRM_TYPE_ITEM_TITLE"] = "物品";
$MESS["CRM_TYPE_QUOTE_FIELD_FILES"] = "文件";
$MESS["CRM_TYPE_QUOTE_FIELD_STATUS_MSGVER_2"] = "估計階段";
$MESS["CRM_TYPE_SMART_DOCUMENT_FIELD_NUMBER"] = "文件 #";
$MESS["CRM_TYPE_SMART_INVOICE_FIELD_ACCOUNT_NUMBER"] = "發票 ＃";
$MESS["CRM_TYPE_SMART_INVOICE_FIELD_BEGIN_DATE"] = "發票日期";
$MESS["CRM_TYPE_SMART_INVOICE_FIELD_CLOSE_DATE"] = "付款之前";
$MESS["CRM_TYPE_TOOLBAR_ALL_ITEMS"] = "所有項目";
$MESS["CRM_TYPE_TYPE_ACCESS_DENIED"] = "拒絕獲得智能過程自動化";
$MESS["CRM_TYPE_TYPE_DELETE_CONFIRMATION_MESSAGE"] = "您確定要刪除此智能過程自動化嗎？";
$MESS["CRM_TYPE_TYPE_DELETE_CONFIRMATION_TITLE"] = "刪除智能過程自動化";
$MESS["CRM_TYPE_TYPE_EDIT_TITLE"] = "＃標題＃溫泉設置";
$MESS["CRM_TYPE_TYPE_FIELDS_SETTINGS"] = "現場設置";
$MESS["CRM_TYPE_TYPE_IS_AUTOMATION_ENABLED_TITLE"] = "在水療中使用自動化規則和触發器";
$MESS["CRM_TYPE_TYPE_IS_BEGIN_CLOSE_DATES_ENABLED_TITLE"] = "\“ start date \”和\“結束日期\”字段";
$MESS["CRM_TYPE_TYPE_IS_BIZ_PROC_ENABLED_TITLE"] = "在水療中使用工作流設計師";
$MESS["CRM_TYPE_TYPE_IS_CATEGORIES_ENABLED_TITLE"] = "在水療中使用自定義管道和銷售渠道";
$MESS["CRM_TYPE_TYPE_IS_CLIENT_ENABLED_TITLE"] = "\“客戶\”字段";
$MESS["CRM_TYPE_TYPE_IS_COUNTERS_ENABLED_TITLE"] = "使用計數器";
$MESS["CRM_TYPE_TYPE_IS_CRM_TRACKING_ENABLED_TITLE"] = "使用銷售情報";
$MESS["CRM_TYPE_TYPE_IS_DOCUMENTS_ENABLED_TITLE"] = "打印文檔";
$MESS["CRM_TYPE_TYPE_IS_LINK_WITH_PRODUCTS_ENABLED_TITLE"] = "與目錄產品結合";
$MESS["CRM_TYPE_TYPE_IS_MYCOMPANY_ENABLED_TITLE"] = "\“您的公司詳細信息\”字段";
$MESS["CRM_TYPE_TYPE_IS_OBSERVERS_ENABLED_TITLE"] = "\“觀察者\”字段";
$MESS["CRM_TYPE_TYPE_IS_PAYMENTS_ENABLED_TITLE"] = "接受付款";
$MESS["CRM_TYPE_TYPE_IS_RECYCLEBIN_ENABLED_TITLE"] = "使用回收箱";
$MESS["CRM_TYPE_TYPE_IS_SET_OPEN_PERMISSIONS_TITLE"] = "公開新管道";
$MESS["CRM_TYPE_TYPE_IS_SOURCE_ENABLED_TITLE"] = "\“ source \”和\“源信息\”字段";
$MESS["CRM_TYPE_TYPE_IS_STAGES_ENABLED_TITLE"] = "在水療中使用自定義階段和看板";
$MESS["CRM_TYPE_TYPE_IS_USE_IN_USERFIELD_ENABLED_TITLE"] = "在自定義字段中使用";
$MESS["CRM_TYPE_TYPE_NOT_FOUND"] = "找不到智能過程自動化";
$MESS["CRM_TYPE_TYPE_SETTINGS"] = "水療設置";
