<?php
$MESS["CRM_PROCESS_ENTITY_COMPANY_ADD"] = "公司添加了";
$MESS["CRM_PROCESS_ENTITY_CONTACT_ADD"] = "創建的聯繫人";
$MESS["CRM_PROCESS_ENTITY_DEAL_ADD"] = "交易添加了";
$MESS["CRM_PROCESS_ENTITY_DEFAULT_ADD"] = "\“＃entity_type_caption＃\”類型項目創建";
$MESS["CRM_PROCESS_ENTITY_DYNAMIC_ADD"] = "添加到spa \“＃entity_type_caption＃\”的項目";
$MESS["CRM_PROCESS_ENTITY_LEAD_ADD"] = "鉛添加了";
$MESS["CRM_PROCESS_ENTITY_ORDER_ADD"] = "創建的順序";
$MESS["CRM_PROCESS_ENTITY_ORDER_PAYMENT_ADD"] = "付款創建";
$MESS["CRM_PROCESS_ENTITY_ORDER_SHIPMENT_ADD"] = "創建了裝運";
$MESS["CRM_PROCESS_ENTITY_QUOTE_ADD_MSGVER_1"] = "創建的估計";
$MESS["CRM_PROCESS_ENTITY_RECURRING_DEAL_ADD"] = "創建的交易模板";
$MESS["CRM_PROCESS_ENTITY_SMART_INVOICE_ADD"] = "創建了發票";
