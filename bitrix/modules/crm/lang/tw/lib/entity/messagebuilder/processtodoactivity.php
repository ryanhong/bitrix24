<?php
$MESS["CRM_PROCESS_TODO_ACTIVITY_COMPANY_PING"] = "這是為了提醒您有關活動\“＃主題＃\”公司中的<a href= \"#url# \“class = \"bx-notifier-item-actim-action \ \">＃＃標題＃</ a>，截止日期＃最後期限＃";
$MESS["CRM_PROCESS_TODO_ACTIVITY_CONTACT_PING"] = "這是為了提醒您有關活動的信息\“＃主題＃\”聯繫人<a href= \"#url# \" class= \之bx-notifier-item-action \ \">＃＃標題＃</a >，截止日期＃最後期限＃";
$MESS["CRM_PROCESS_TODO_ACTIVITY_DEAL_PING"] = "這是為了提醒您活動\“＃主題＃\” <a href= \"#url# \“class = \"bx-notifier-item-actim-action \ \">＃＃標題＃</a>，截止日期＃最後期限＃";
$MESS["CRM_PROCESS_TODO_ACTIVITY_DEFAULT_PING"] = "這是為了提醒您活動\“＃主題＃\” in \“ \”＃entity_type_caption＃\“ item <a href= \“#url# ＃</a>，截止日期＃截止日期＃";
$MESS["CRM_PROCESS_TODO_ACTIVITY_DYNAMIC_PING"] = "這是為了提醒您有關活動\“＃主題＃\”中的活動\“＃”標題＃</a>，截止日期＃截止日期＃";
$MESS["CRM_PROCESS_TODO_ACTIVITY_LEAD_PING"] = "這是為了提醒您活動\“＃主題＃\”中的<a href= \"#url# \“class = \"bx-notifier-item-action \ \">＃＃標題＃</a>，截止日期＃最後期限＃";
$MESS["CRM_PROCESS_TODO_ACTIVITY_ORDER_PING"] = "這是為了提醒您活動\“＃主題＃” ＃最後期限＃";
$MESS["CRM_PROCESS_TODO_ACTIVITY_QUOTE_PING"] = "這是為了提醒您有關活動\“＃主題＃\”估計中的<a href= \"#url# \" class = \"bx-notifier-item-action \ \">＃＃標題＃</a> ，截止日期＃最後期限＃";
$MESS["CRM_PROCESS_TODO_ACTIVITY_RECURRING_DEAL_PING"] = "這是為了提醒您有關活動\“＃主題＃\”中的活動<a href= \"#url# \“class = \"bx-notifier-item-action \ \">＃＃標題＃</a> ，截止日期＃截止日期＃";
$MESS["CRM_PROCESS_TODO_ACTIVITY_SMART_INVOICE_PING"] = "這是為了提醒您有關活動的信息\“＃主題＃\”在發票<a href= \"#url# \“class = \"bx-notifier-item-action \ \">＃＃標題＃</a >，截止日期截止日期＃最後期限＃";
