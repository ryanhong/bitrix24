<?php
$MESS["CRM_DEAL_CATEGORY_DEFAULT"] = "一般的";
$MESS["CRM_DEAL_CATEGORY_FIELD_CREATED_DATE"] = "創建於";
$MESS["CRM_DEAL_CATEGORY_FIELD_ID"] = "ID";
$MESS["CRM_DEAL_CATEGORY_FIELD_IS_LOCKED"] = "阻止";
$MESS["CRM_DEAL_CATEGORY_FIELD_NAME"] = "姓名";
$MESS["CRM_DEAL_CATEGORY_FIELD_SORT"] = "種類";
$MESS["CRM_DEAL_CATEGORY_PERMISSION_ENTITY"] = "交易＃類別＃";
$MESS["CRM_DEAL_CATEGORY_STAGE_FILTER"] = "交易階段";
$MESS["CRM_DEAL_CATEGORY_STATUS_ENTITY"] = "＃類別＃交易階段";
