<?php
$MESS["CRM_COMPANY_INVOICE_SUM_STAT_PRESET_OVERALL_COUNT"] = "主動發票";
$MESS["CRM_COMPANY_INVOICE_SUM_STAT_PRESET_OVERALL_SUM"] = "主動發票總計";
$MESS["CRM_COMPANY_INVOICE_SUM_STAT_PRESET_PAID_INTIME_COUNT"] = "開票按時付款";
$MESS["CRM_COMPANY_INVOICE_SUM_STAT_PRESET_PROCESS_COUNT"] = "正在進行的發票";
$MESS["CRM_COMPANY_INVOICE_SUM_STAT_PRESET_PROCESS_SUM"] = "總共正在進行的發票";
$MESS["CRM_COMPANY_INVOICE_SUM_STAT_PRESET_SUCCESS_COUNT"] = "付費發票";
$MESS["CRM_COMPANY_INVOICE_SUM_STAT_PRESET_SUCCESS_SUM"] = "總付費發票";
