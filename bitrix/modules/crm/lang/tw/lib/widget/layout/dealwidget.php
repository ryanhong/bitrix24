<?php
$MESS["CRM_DEAL_WGT_DEAL_IN_WORK"] = "交易正在進行";
$MESS["CRM_DEAL_WGT_DEMO_CONTENT"] = "如果您沒有任何交易，請立即添加您的<a href= \"#url# \" class = \"#class_name# \ \">第一次交易</a>現在！";
$MESS["CRM_DEAL_WGT_DEMO_TITLE"] = "這是一個演示儀表板，您可以隱藏演示數據並使用交易數據。";
$MESS["CRM_DEAL_WGT_EMPLOYEE_DEAL_IN_WORK"] = "正在進行的交易（員工）";
$MESS["CRM_DEAL_WGT_FUNNEL"] = "交易的銷售渠道";
$MESS["CRM_DEAL_WGT_PAYMENT_CONTROL"] = "贏的交易付款控制";
$MESS["CRM_DEAL_WGT_QTY_ACTIVITY"] = "活動數量";
$MESS["CRM_DEAL_WGT_QTY_CALL"] = "電話數量";
$MESS["CRM_DEAL_WGT_QTY_DEAL_IDLE"] = "擱置的交易數量";
$MESS["CRM_DEAL_WGT_QTY_DEAL_IN_WORK"] = "正在進行的交易數量";
$MESS["CRM_DEAL_WGT_QTY_DEAL_WON"] = "贏的交易數量";
$MESS["CRM_DEAL_WGT_QTY_EMAIL"] = "電子郵件數量";
$MESS["CRM_DEAL_WGT_QTY_MEETING"] = "會議數量";
$MESS["CRM_DEAL_WGT_RATING"] = "交易排名獲勝";
$MESS["CRM_DEAL_WGT_SUM_DEAL_IN_WORK"] = "總共正在進行的交易";
$MESS["CRM_DEAL_WGT_SUM_DEAL_OVERALL"] = "交易的總價值";
$MESS["CRM_DEAL_WGT_SUM_DEAL_WON"] = "贏得的交易總數";
$MESS["CRM_DEAL_WGT_SUM_INVOICE_OVERALL"] = "發票的總銷售額";
$MESS["CRM_DEAL_WGT_SUM_INVOICE_OWED"] = "銷售總額";
