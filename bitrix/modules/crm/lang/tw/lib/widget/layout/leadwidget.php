<?php
$MESS["CRM_LEAD_WGT_CONVERSION_FAIL"] = "丟失的";
$MESS["CRM_LEAD_WGT_CONVERSION_SUCCESS"] = "轉換";
$MESS["CRM_LEAD_WGT_DATE_NEW_LEAD"] = "新的鉛處理動力學";
$MESS["CRM_LEAD_WGT_DEMO_CONTENT"] = "如果您仍然沒有線索，<a href= \"#url# \" class= \"#class_name# \ \">立即創建一個</a>！";
$MESS["CRM_LEAD_WGT_DEMO_TITLE"] = "這是一個演示視圖。將其隱藏以訪問潛在客戶的分析。";
$MESS["CRM_LEAD_WGT_EMPLOYEE_LEAD_PROC"] = "員工的潛在客戶處理效率";
$MESS["CRM_LEAD_WGT_FUNNEL"] = "鉛漏斗";
$MESS["CRM_LEAD_WGT_QTY_ACTIVITY"] = "活動數量";
$MESS["CRM_LEAD_WGT_QTY_CALL"] = "電話數量";
$MESS["CRM_LEAD_WGT_QTY_LEAD_FAILED"] = "垃圾鉛數";
$MESS["CRM_LEAD_WGT_QTY_LEAD_IDLE"] = "閒置線的數量";
$MESS["CRM_LEAD_WGT_QTY_LEAD_IN_WORK"] = "活躍線索數量";
$MESS["CRM_LEAD_WGT_QTY_LEAD_NEW"] = "新線索的數量";
$MESS["CRM_LEAD_WGT_QTY_LEAD_SUCCESSFUL"] = "轉換線的數量";
$MESS["CRM_LEAD_WGT_RATING"] = "領導轉換後的排行榜";
$MESS["CRM_LEAD_WGT_SOURCE"] = "鉛源";
