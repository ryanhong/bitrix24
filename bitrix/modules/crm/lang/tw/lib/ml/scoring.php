<?php
$MESS["CRM_SCORING_LICENSE_TEXT_P1"] = "處理交易時，您的銷售經理使用的標準是什麼？隊列？直覺？您需要的只是捕獲並將您最有前途的客戶列入您的名單。 AI的預測將分析現有交易，以顯示其成功的可能性。";
$MESS["CRM_SCORING_LICENSE_TEXT_P2"] = "該系統將幫助您的員工確定需要大多數關注的領域。";
$MESS["CRM_SCORING_LICENSE_TEXT_P3"] = "AI預測可在主要計劃中獲得。現在升級以提高銷售部門的效率並降低廣告成本。";
$MESS["CRM_SCORING_LICENSE_TITLE"] = "AI得分";
