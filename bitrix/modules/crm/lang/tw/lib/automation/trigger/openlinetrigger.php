<?php
$MESS["CRM_AUTOMATION_TRIGGER_OPENLINE_DEFAULT_CONFIG"] = "[任何]";
$MESS["CRM_AUTOMATION_TRIGGER_OPENLINE_DESCRIPTION"] = "在收到客戶的初始消息時更改階段。";
$MESS["CRM_AUTOMATION_TRIGGER_OPENLINE_NAME"] = "傳入的消息打開頻道";
$MESS["CRM_AUTOMATION_TRIGGER_OPENLINE_NAME1"] = "客戶發布的初始消息進行實時聊天";
$MESS["CRM_AUTOMATION_TRIGGER_OPENLINE_NAME_2"] = "跟踪客戶的初始消息";
$MESS["CRM_AUTOMATION_TRIGGER_OPENLINE_PROPERTY_CONFIG"] = "選擇“打開通道”";
