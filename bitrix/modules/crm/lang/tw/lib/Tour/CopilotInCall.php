<?php
$MESS["CRM_TOUR_COPILOT_IN_CALL_BODY_DEAL"] = "AI將轉錄您的客戶電話，創建摘要，並填寫交易字段，並使用客戶在對話中共享的信息。";
$MESS["CRM_TOUR_COPILOT_IN_CALL_BODY_LEAD"] = "AI將轉錄您的客戶電話，創建摘要，並填寫客戶在對話中共享的信息。";
$MESS["CRM_TOUR_COPILOT_IN_CALL_BODY_MAIN"] = "AI，您的必不可少的CRM助手。";
$MESS["CRM_TOUR_COPILOT_IN_CALL_TITLE"] = "介紹副駕駛";
