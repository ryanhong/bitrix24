<?php
$MESS["CRM_FILTER_ITEMDATAPROVIDER_ACTIVITY_COUNTER"] = "活動";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_ACTIVITY_RESPONSIBLE_IDS"] = "活動負責人";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_COMPANY_TITLE"] = "公司名稱";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_CONTACTS_FULL_NAME"] = "聯繫人姓名";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_MYCOMPANY_TITLE"] = "您的公司名稱";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_OPPORTUNITY_WITH_CURRENCY"] = "金額/貨幣";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_STAGE_SEMANTIC_FAIL"] = "失敗的";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_STAGE_SEMANTIC_FILTER_NAME"] = "舞台組";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_STAGE_SEMANTIC_IN_WORK"] = "進行中";
$MESS["CRM_FILTER_ITEMDATAPROVIDER_STAGE_SEMANTIC_SUCCESS"] = "成功";
