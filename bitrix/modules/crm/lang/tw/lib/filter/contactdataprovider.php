<?php
$MESS["CRM_CONTACT_FILTER_ACTIVITY_COUNTER"] = "活動";
$MESS["CRM_CONTACT_FILTER_ACTIVITY_RESPONSIBLE_IDS"] = "活動負責人";
$MESS["CRM_CONTACT_FILTER_ALL"] = "（全部）";
$MESS["CRM_CONTACT_FILTER_COMMUNICATION_TYPE"] = "溝通";
$MESS["CRM_CONTACT_FILTER_COMPANY_ID"] = "公司";
$MESS["CRM_CONTACT_FILTER_COMPANY_TITLE"] = "公司名稱";
$MESS["CRM_CONTACT_FILTER_EMAIL"] = "電子郵件";
$MESS["CRM_CONTACT_FILTER_IM"] = "信使";
$MESS["CRM_CONTACT_FILTER_LAST_ACTIVITY_TIME"] = "最近更新時間";
$MESS["CRM_CONTACT_FILTER_PHONE"] = "電話";
$MESS["CRM_CONTACT_FILTER_WEB"] = "網站";
