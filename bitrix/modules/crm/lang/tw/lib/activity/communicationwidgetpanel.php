<?php
$MESS["CRM_COMM_WGT_COMMUNICATIONS"] = "首選通信渠道";
$MESS["CRM_COMM_WGT_COMPANY_GROWTH_COUNT"] = "公司數量";
$MESS["CRM_COMM_WGT_CONTACT_GROWTH_COUNT"] = "聯繫人數量";
$MESS["CRM_COMM_WGT_CONVERSION"] = "轉換";
$MESS["CRM_COMM_WGT_DEALS"] = "交易";
$MESS["CRM_COMM_WGT_DEAL_FAILED_SUM"] = "損失交易的價值";
$MESS["CRM_COMM_WGT_DEAL_PROCESS_SUM"] = "正在進行的交易價值";
$MESS["CRM_COMM_WGT_DEAL_SUCCESS_SUM"] = "WON交易的價值";
$MESS["CRM_COMM_WGT_DEAL_SUM_PERIOD"] = "總交易金額";
$MESS["CRM_COMM_WGT_FINANCE_INDEX"] = "按時支付";
$MESS["CRM_COMM_WGT_GROWTH"] = "客戶群增長";
$MESS["CRM_COMM_WGT_INVOICES_ACTIVE_SUM"] = "主動發票總量";
$MESS["CRM_COMM_WGT_INVOICES_PAID_SUM"] = "付費發票的總金額";
$MESS["CRM_COMM_WGT_INVOICES_WAITING"] = "仍然沒有薪水";
$MESS["CRM_COMM_WGT_MARKS"] = "客戶滿意度";
$MESS["CRM_COMM_WGT_MARK_ALL"] = "全部";
$MESS["CRM_COMM_WGT_MARK_NEGATIVE"] = "消極的";
$MESS["CRM_COMM_WGT_MARK_NONE"] = "沒有評分";
$MESS["CRM_COMM_WGT_MARK_POSITIVE"] = "積極的";
$MESS["CRM_COMM_WGT_MARK_SOURCE_FB"] = "Facebook";
$MESS["CRM_COMM_WGT_MARK_SOURCE_SK"] = "Skype";
$MESS["CRM_COMM_WGT_MARK_SOURCE_TG"] = "電報";
$MESS["CRM_COMM_WGT_MARK_SOURCE_VK"] = "VK";
$MESS["CRM_COMM_WGT_MARK_SOURCE_WHATSAPP"] = "WhatsApp";
$MESS["CRM_COMM_WGT_OVERALL_INVOICES"] = "創建的發票";
$MESS["CRM_COMM_WGT_PAID_INTIME_COUNT"] = "開票按時付款";
$MESS["CRM_COMM_WGT_PAID_INVOICES"] = "付費發票";
$MESS["CRM_COMM_WGT_PAYMENTS"] = "付款";
$MESS["CRM_COMM_WGT_PROVIDER_STATUS"] = "＃Provider_name＃：狀態";
$MESS["CRM_COMM_WGT_PROVIDER_SUM"] = "＃provider_name＃：訂單總量";
$MESS["CRM_COMM_WGT_PROVIDER_TOTAL_QTY"] = "＃Provider_name＃：總負載";
$MESS["CRM_COMM_WGT_SOURCES"] = "連接器負載";
$MESS["CRM_COMM_WGT_STATUSES"] = "請求處理";
$MESS["CRM_COMM_WGT_STREAMS"] = "請求流";
$MESS["CRM_COMM_WGT_SUCCESS_DEALS"] = "贏了";
