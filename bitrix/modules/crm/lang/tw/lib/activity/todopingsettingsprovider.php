<?php
$MESS["CRM_ACTIVITY_TODO_PING_OFFSET_0_MIN"] = "事件開始時";
$MESS["CRM_ACTIVITY_TODO_PING_OFFSET_1_DAY"] = "1天前";
$MESS["CRM_ACTIVITY_TODO_PING_OFFSET_1_HOUR"] = "1小時前";
$MESS["CRM_ACTIVITY_TODO_PING_OFFSET_2_HOURS"] = "2小時前";
$MESS["CRM_ACTIVITY_TODO_PING_OFFSET_15_MIN"] = "15分鐘前";
$MESS["CRM_ACTIVITY_TODO_PING_OFFSET_30_MIN"] = "30分鐘前";
