<?php
$MESS["CRM_ACTIVITY_PROVIDER_SMS_CHANNEL_NOT_FOUND"] = "沒有找到通信渠道。";
$MESS["CRM_ACTIVITY_PROVIDER_SMS_WRONG_CHANNEL"] = "未知的通信渠道。";
$MESS["CRM_ACTIVITY_PROVIDER_SMS_WRONG_FROM"] = "發送者不正確。";
$MESS["CRM_ACTIVITY_PROVIDER_SMS_WRONG_TO"] = "不正確的收件人。";
