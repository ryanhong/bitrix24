<?php
$MESS["CRM_ACTIVITY_PROVIDER_CALL_LIST_TITLE"] = "通話列表";
$MESS["CRM_CALL_LIST_NOT_CREATED_ERROR"] = "不存在通話列表。要創建列表，請單擊\“ Call List \”區域中的鏈接。";
$MESS["CRM_CALL_LIST_RESPONSIBLE_IM_NOTIFY"] = "您現在負責呼叫列表\“＃標題＃\”";
$MESS["CRM_CALL_LIST_SUBJECT_EMPTY"] = "沒有指定的活動主題";
$MESS["CRM_CALL_LIST_USE_WEBFORM"] = "使用表格";
