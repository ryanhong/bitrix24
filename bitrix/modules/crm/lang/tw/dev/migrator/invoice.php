<?php
$MESS["CRM_ORDER_SHIPMENT_STATUS_DD"] = "取消";
$MESS["CRM_ORDER_SHIPMENT_STATUS_DF"] = "發貨";
$MESS["CRM_ORDER_SHIPMENT_STATUS_DN"] = "等待處理";
$MESS["CRM_ORDER_STATUS_D"] = "取消";
$MESS["CRM_ORDER_STATUS_F"] = "實現";
$MESS["CRM_ORDER_STATUS_N"] = "接受訂單，等待付款";
$MESS["CRM_ORDER_STATUS_P"] = "付費，準備貨物";
$MESS["SALE_CHECK_PRINT_ERROR_HTML_SUB_TITLE"] = "歡迎！";
$MESS["SALE_CHECK_PRINT_ERROR_HTML_TEXT"] = "無法打印收據## check_id＃for Order ## order_account_number＃日期＃order_date＃。

單擊此處解決問題：
＃link_url＃
";
$MESS["SALE_CHECK_PRINT_ERROR_HTML_TITLE"] = "錯誤打印收據";
$MESS["SALE_CHECK_PRINT_ERROR_SUBJECT"] = "錯誤打印收據";
$MESS["SALE_CHECK_PRINT_ERROR_TYPE_DESC"] = "＃order_account_number＃ - 訂購ID
＃order_date＃ - 訂購日期
＃order_id＃ - 訂購ID
＃check_id＃ - 收據ID";
$MESS["SALE_CHECK_PRINT_ERROR_TYPE_NAME"] = "收據打印輸出錯誤通知";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_DESC"] = "用戶組允許編輯在線商店首選項";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_NAME"] = "在線商店管理員";
$MESS["SALE_USER_GROUP_SHOP_BUYER_DESC"] = "包含所有商店客戶的客戶群";
$MESS["SALE_USER_GROUP_SHOP_BUYER_NAME"] = "所有客戶";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_DESC"] = "允許用戶組使用在線商店功能";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_NAME"] = "在線商店經理";
$MESS["SMAIL_FOOTER_BR"] = "善良的問候，<br />支持人員。";
$MESS["SMAIL_FOOTER_SHOP"] = "網上商店";
$MESS["UP_TYPE_SUBJECT"] = "返回庫存通知";
$MESS["UP_TYPE_SUBJECT_DESC"] = "＃user_name＃ - 用戶名
＃電子郵件＃-用戶電子郵件
＃名稱＃-產品名稱
＃page_url＃ - 產品詳細信息頁面";
