<?php
$MESS["CRM_COMPANY"] = "公司";
$MESS["CRM_CONTACT"] = "接觸";
$MESS["CRM_DEAL"] = "交易";
$MESS["CRM_EMAILS"] = "電子郵件";
$MESS["CRM_INVOICE"] = "發票";
$MESS["CRM_LEAD"] = "帶領";
$MESS["CRM_PHONES"] = "電話";
$MESS["CRM_QUOTE"] = "引用";
