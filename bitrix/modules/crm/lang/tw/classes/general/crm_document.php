<?php
$MESS["BPVDX_ADD"] = "添加";
$MESS["BPVDX_CALENDAR"] = "日曆";
$MESS["BPVDX_DATETIME"] = "約會時間";
$MESS["BPVDX_DOCUMENT_AUTHOR"] = "由...製作";
$MESS["BPVDX_EMAIL"] = "電子郵件";
$MESS["BPVDX_FILE"] = "文件";
$MESS["BPVDX_INTERNALSELECT"] = "綁定到文檔列表";
$MESS["BPVDX_LIST"] = "列表";
$MESS["BPVDX_MESSANGER"] = "信使";
$MESS["BPVDX_NO"] = "不";
$MESS["BPVDX_NOT_SET"] = "沒有設置";
$MESS["BPVDX_NUM"] = "數字";
$MESS["BPVDX_NUMINT"] = "整數";
$MESS["BPVDX_PHONE"] = "電話";
$MESS["BPVDX_STRING"] = "細繩";
$MESS["BPVDX_TEXT"] = "文字";
$MESS["BPVDX_USER"] = "綁定到用戶";
$MESS["BPVDX_WEB"] = "網站";
$MESS["BPVDX_YES"] = "是的";
$MESS["BPVDX_YN"] = "是/否";
$MESS["CRM_DOCUMENT_AUTHOR"] = "作者";
$MESS["CRM_DOCUMENT_AUTOMATION_DEBUG_MESSAGE_FINISHED"] = "測試模式已經完成。您可以繼續使用該項目。";
$MESS["CRM_DOCUMENT_AUTOMATION_DEBUG_MESSAGE_INTERCEPTED"] = "該項目處於測試模式。在測試模式結束之前，不要使用它。";
$MESS["CRM_DOCUMENT_AUTOMATION_DEBUG_MESSAGE_IN_DEBUG"] = "此項目已被選擇用於測試模式。請停止使用它，直到測試模式結束。";
$MESS["CRM_DOCUMENT_AUTOMATION_DEBUG_MESSAGE_REMOVED"] = "該項目返回正常模式。您可以繼續使用它。";
$MESS["CRM_DOCUMENT_CRM_ENTITY"] = "實體";
$MESS["CRM_DOCUMENT_CRM_ENTITY_OK"] = "好的";
$MESS["CRM_DOCUMENT_CRM_ENTITY_TYPE_COMPANY"] = "公司";
$MESS["CRM_DOCUMENT_CRM_ENTITY_TYPE_CONTACT"] = "接觸";
$MESS["CRM_DOCUMENT_CRM_ENTITY_TYPE_DEAL"] = "交易";
$MESS["CRM_DOCUMENT_CRM_ENTITY_TYPE_LEAD"] = "帶領";
$MESS["CRM_DOCUMENT_CRM_STATUS"] = "地位";
$MESS["CRM_DOCUMENT_DEAL_CATEGORY_CHANGE_ERROR"] = "僅在創建交易時才可以選擇管道。";
$MESS["CRM_DOCUMENT_DEAL_IS_REPEATED_APPROACH"] = "重複查詢";
$MESS["CRM_DOCUMENT_DEAL_IS_RETURN_CUSTOMER"] = "重複交易";
$MESS["CRM_DOCUMENT_DEAL_STAGE_MISMATCH_ERROR"] = "在這裡只能使用屬於\“＃類別＃\”管道的交易階段。 stage \“＃targ_stage＃\”是\ \“＃targ_category＃\” pipeline。";
$MESS["CRM_DOCUMENT_DEAL_TYPE_ID"] = "類型";
$MESS["CRM_DOCUMENT_ELEMENT_IS_NOT_FOUND"] = "找不到元素。";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_ACTIVE"] = "負責人：活躍";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_EMAIL"] = "負責人（電子郵件）";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_FIELD"] = "負責人";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_LAST_NAME"] = "負責人：姓氏";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_LOGIN"] = "負責人：登錄";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_NAME"] = "負責人：姓名";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_PERSONAL_CITY"] = "負責人：城市";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_PERSONAL_MOBILE"] = "負責人（移動）";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_PERSONAL_WWW"] = "負責人：網站";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_PRINTABLE"] = "負責人（文字）";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_SECOND_NAME"] = "負責人：中間名";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_UF_FACEBOOK"] = "負責人：Facebook";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_UF_LINKEDIN"] = "負責人：LinkedIn";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_UF_PHONE_INNER"] = "負責人：延長號碼";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_UF_SKYPE"] = "負責人：Skype";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_UF_TWITTER"] = "負責人：Twitter";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_UF_WEB_SITES"] = "負責人：其他網站";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_UF_XING"] = "負責人：Xing";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_WORK_PHONE"] = "負責人（工作電話）";
$MESS["CRM_DOCUMENT_FIELD_ASSIGNED_BY_WORK_POSITION"] = "負責人：職位";
$MESS["CRM_DOCUMENT_FIELD_CONTACT_IDS"] = "聯繫人";
$MESS["CRM_DOCUMENT_FIELD_CREATED_BY_ID_COMPANY"] = "創建的公司";
$MESS["CRM_DOCUMENT_FIELD_CREATED_BY_ID_CONTACT"] = "創建的聯繫人";
$MESS["CRM_DOCUMENT_FIELD_CREATED_BY_ID_DEAL"] = "創建的交易";
$MESS["CRM_DOCUMENT_FIELD_CRM_ID"] = "CRM項目ID";
$MESS["CRM_DOCUMENT_FIELD_LAST_COMMUNICATION_DATE"] = "最後的通信日期";
$MESS["CRM_DOCUMENT_FIELD_MODIFY_BY_ID"] = "修改";
$MESS["CRM_DOCUMENT_FIELD_PRODUCT_IDS"] = "產品";
$MESS["CRM_DOCUMENT_FIELD_PRODUCT_NAME"] = "產品";
$MESS["CRM_DOCUMENT_FIELD_PRODUCT_SUM"] = "數量";
$MESS["CRM_DOCUMENT_FIELD_SOURCE_DESCRIPTION"] = "來源信息";
$MESS["CRM_DOCUMENT_FIELD_SOURCE_ID"] = "來源";
$MESS["CRM_DOCUMENT_FIELD_TIME_CREATE"] = "創建的時間";
$MESS["CRM_DOCUMENT_FIELD_TRACKING_SOURCE_ID"] = "銷售情報來源";
$MESS["CRM_DOCUMENT_FIELD_URL"] = "項目鏈接";
$MESS["CRM_DOCUMENT_FIELD_URL_BB"] = "項目鏈接（BBCODE）";
$MESS["CRM_DOCUMENT_IBLOCK"] = "信息塊";
$MESS["CRM_DOCUMENT_LEAD_IS_RETURN_CUSTOMER"] = "重複引線";
$MESS["CRM_DOCUMENT_OPERATION_ADD"] = "添加";
$MESS["CRM_DOCUMENT_OPERATION_DELETE"] = "刪除";
$MESS["CRM_DOCUMENT_OPERATION_READ"] = "讀";
$MESS["CRM_DOCUMENT_OPERATION_WRITE"] = "寫";
$MESS["CRM_DOCUMENT_RESPONSIBLE_HEAD"] = "導師";
$MESS["CRM_DOCUMENT_RESUME_RESTRICTED"] = "由於當前的計劃限製而中斷";
$MESS["CRM_DOCUMENT_UNGROUPED_USERS"] = "[未分組]";
$MESS["CRM_DOCUMENT_WEBFORM_ID"] = "CRM形式";
$MESS["CRM_FIELD_ACCOUNT_CURRENCY_ID"] = "會計貨幣";
$MESS["CRM_FIELD_BP_EMAIL"] = "電子郵件";
$MESS["CRM_FIELD_BP_PERSONAL_MOBILE"] = "智慧型手機";
$MESS["CRM_FIELD_BP_TEXT"] = "文字";
$MESS["CRM_FIELD_BP_WORK_PHONE"] = "工作電話";
$MESS["CRM_FIELD_MULTI_EMAIL"] = "電子郵件";
$MESS["CRM_FIELD_MULTI_IM"] = "信使";
$MESS["CRM_FIELD_MULTI_PHONE"] = "電話";
$MESS["CRM_FIELD_MULTI_WEB"] = "網站";
$MESS["CRM_FIELD_OBSERVER_IDS"] = "觀察者";
$MESS["CRM_FIELD_OPPORTUNITY_ACCOUNT"] = "金額（以會計貨幣為單位）";
$MESS["CRM_FIELD_ORDER_IDS"] = "命令";
$MESS["CRM_FIELD_ORIGINATOR_ID"] = "Web Store ID";
$MESS["IBD_DOCUMENT_XFORMOPTIONS1"] = "在新行上指定每個變體。如果變體值和變體名稱不同，則將名稱帶有方括號中的值。例如：[v1]變體1";
$MESS["IBD_DOCUMENT_XFORMOPTIONS2"] = "完成後單擊\“設置\”。";
$MESS["IBD_DOCUMENT_XFORMOPTIONS3"] = "放";
