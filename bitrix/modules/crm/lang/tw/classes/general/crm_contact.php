<?php
$MESS["CRM_CONTACT_CREATION_CANCELED"] = "沒有創建聯繫人，因為事件處理程序取消了操作：\“＃名稱＃\”";
$MESS["CRM_CONTACT_DEFAULT_TITLE_TEMPLATE"] = "聯繫電話％";
$MESS["CRM_CONTACT_EVENT_ADD"] = "創建的聯繫人";
$MESS["CRM_CONTACT_EVENT_UPDATE_ASSIGNED_BY"] = "負責人更新";
$MESS["CRM_CONTACT_EVENT_UPDATE_COMPANY"] = "公司更新";
$MESS["CRM_CONTACT_FIELD_ADDRESS"] = "地址";
$MESS["CRM_CONTACT_FIELD_ADDRESS_2"] = "地址（第2行）";
$MESS["CRM_CONTACT_FIELD_ADDRESS_CITY"] = "城市";
$MESS["CRM_CONTACT_FIELD_ADDRESS_COUNTRY"] = "國家";
$MESS["CRM_CONTACT_FIELD_ADDRESS_COUNTRY_CODE"] = "國家代碼";
$MESS["CRM_CONTACT_FIELD_ADDRESS_LOC_ADDR_ID"] = "位置地址ID";
$MESS["CRM_CONTACT_FIELD_ADDRESS_POSTAL_CODE"] = "壓縮";
$MESS["CRM_CONTACT_FIELD_ADDRESS_PROVINCE"] = "州 /省";
$MESS["CRM_CONTACT_FIELD_ADDRESS_REGION"] = "地區";
$MESS["CRM_CONTACT_FIELD_ASSIGNED_BY_ID"] = "負責人";
$MESS["CRM_CONTACT_FIELD_BIRTHDATE"] = "出生日期";
$MESS["CRM_CONTACT_FIELD_COMMENTS"] = "評論";
$MESS["CRM_CONTACT_FIELD_COMMUNICATION_TYPE"] = "交流手段";
$MESS["CRM_CONTACT_FIELD_COMPANY"] = "公司";
$MESS["CRM_CONTACT_FIELD_COMPANY_ID"] = "公司";
$MESS["CRM_CONTACT_FIELD_CREATED_BY_ID"] = "由...製作";
$MESS["CRM_CONTACT_FIELD_DATE_CREATE"] = "創建於";
$MESS["CRM_CONTACT_FIELD_DATE_MODIFY"] = "修改";
$MESS["CRM_CONTACT_FIELD_EXPORT_NEW"] = "包括在出口中";
$MESS["CRM_CONTACT_FIELD_FACE_ID"] = "faceid連接";
$MESS["CRM_CONTACT_FIELD_FULL_NAME"] = "全名";
$MESS["CRM_CONTACT_FIELD_HAS_EMAIL"] = "有電子郵件";
$MESS["CRM_CONTACT_FIELD_HAS_IMOL"] = "有開放的頻道";
$MESS["CRM_CONTACT_FIELD_HAS_PHONE"] = "有電話";
$MESS["CRM_CONTACT_FIELD_HONORIFIC"] = "致敬";
$MESS["CRM_CONTACT_FIELD_ID"] = "ID";
$MESS["CRM_CONTACT_FIELD_LAST_NAME"] = "姓";
$MESS["CRM_CONTACT_FIELD_LEAD_ID"] = "帶領";
$MESS["CRM_CONTACT_FIELD_MODIFY_BY_ID"] = "修改";
$MESS["CRM_CONTACT_FIELD_NAME"] = "名";
$MESS["CRM_CONTACT_FIELD_OPENED"] = "每個人都可以使用";
$MESS["CRM_CONTACT_FIELD_ORIGINATOR_ID"] = "外部源";
$MESS["CRM_CONTACT_FIELD_ORIGIN_ID"] = "數據源中的項目ID";
$MESS["CRM_CONTACT_FIELD_ORIGIN_VERSION"] = "原始版本";
$MESS["CRM_CONTACT_FIELD_PHOTO"] = "照片";
$MESS["CRM_CONTACT_FIELD_POST"] = "位置";
$MESS["CRM_CONTACT_FIELD_SECOND_NAME"] = "第二個名字";
$MESS["CRM_CONTACT_FIELD_SOURCE_DESCRIPTION"] = "描述";
$MESS["CRM_CONTACT_FIELD_SOURCE_DESCRIPTION_NEW"] = "來源信息";
$MESS["CRM_CONTACT_FIELD_SOURCE_ID"] = "來源";
$MESS["CRM_CONTACT_FIELD_TYPE_ID"] = "聯繫人類型";
$MESS["CRM_CONTACT_FIELD_UTM"] = "UTM參數";
$MESS["CRM_CONTACT_FIELD_UTM_CAMPAIGN"] = "廣告活動UTM";
$MESS["CRM_CONTACT_FIELD_UTM_CONTENT"] = "廣告系列內容";
$MESS["CRM_CONTACT_FIELD_UTM_MEDIUM"] = "中等的";
$MESS["CRM_CONTACT_FIELD_UTM_SOURCE"] = "廣告系統";
$MESS["CRM_CONTACT_FIELD_UTM_TERM"] = "廣告系列搜索詞";
$MESS["CRM_CONTACT_FIELD_WEBFORM_ID"] = "由CRM形式創建";
$MESS["CRM_CONTACT_NOT_RESPONSIBLE_IM_NOTIFY"] = "您不再對聯繫人負責\“＃標題＃\”";
$MESS["CRM_CONTACT_PROVIDER"] = "接觸";
$MESS["CRM_CONTACT_RESPONSIBLE_IM_NOTIFY"] = "您現在負責聯繫\“＃標題＃\”";
$MESS["CRM_CONTACT_UNNAMED"] = "無名";
$MESS["CRM_CONTACT_UPDATE_CANCELED"] = "聯繫人尚未更新，因為事件處理程序取消了操作：\“＃名稱＃\”";
$MESS["CRM_ERROR_FIELD_INCORRECT"] = "字段\“％field_name％\”是不正確的。";
$MESS["CRM_ERROR_PHOTO"] = "照片不正確。";
$MESS["CRM_ERROR_REQUIRED_FIELDS"] = "需要第一個和姓氏。";
$MESS["CRM_FIELD_COMPARE_ADDRESS"] = "更改了字段\“地址\”";
$MESS["CRM_FIELD_COMPARE_ASSIGNED_BY_ID"] = "\“負責人\”字段進行了修改。";
$MESS["CRM_FIELD_COMPARE_BIRTHDATE"] = "\“出生日期\”領域改變了";
$MESS["CRM_FIELD_COMPARE_COMMENTS"] = "更改了字段\“評論\”";
$MESS["CRM_FIELD_COMPARE_COMPANIES_ADDED"] = "鏈接到創建的公司";
$MESS["CRM_FIELD_COMPARE_COMPANIES_REMOVED"] = "鏈接到已刪除的公司";
$MESS["CRM_FIELD_COMPARE_COMPANY_ID"] = "更改了字段\“公司\”";
$MESS["CRM_FIELD_COMPARE_EMPTY"] = "-空的-";
$MESS["CRM_FIELD_COMPARE_HONORIFIC"] = "\“ Saluart \”字段改變了";
$MESS["CRM_FIELD_COMPARE_LAST_NAME"] = "更改了字段\“姓氏\”";
$MESS["CRM_FIELD_COMPARE_NAME"] = "更改了字段\“名字\”";
$MESS["CRM_FIELD_COMPARE_POST"] = "更改了字段\“位置\”";
$MESS["CRM_FIELD_COMPARE_SECOND_NAME"] = "更改字段\“中間名\”";
$MESS["CRM_FIELD_COMPARE_SOURCE_DESCRIPTION"] = "更改了字段\“源描述\”";
$MESS["CRM_FIELD_COMPARE_SOURCE_ID"] = "更改了字段\“ source \”";
$MESS["CRM_FIELD_COMPARE_TYPE_ID"] = "更改了字段\“聯繫人類型\”";
$MESS["CRM_FIELD_EMAIL"] = "電子郵件";
$MESS["CRM_FIELD_PHONE"] = "電話";
$MESS["CRM_FIELD_WEB"] = "地點";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
$MESS["CRM_PERMISSION_USER_NOT_DEFINED"] = "由於沒有指定用戶，因此無法驗證權限。";
$MESS["CRM_PS_CONTACT_FIELD_EMAIL"] = "電子郵件";
$MESS["CRM_PS_CONTACT_FIELD_FULL_NAME"] = "全名";
$MESS["CRM_PS_CONTACT_FIELD_PHONE"] = "電話";
