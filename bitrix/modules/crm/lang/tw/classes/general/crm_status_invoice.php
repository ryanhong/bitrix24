<?php
$MESS["CRM_INVOICE_STATUSN_D"] = "未付";
$MESS["CRM_INVOICE_STATUSN_F"] = "有薪酬的";
$MESS["CRM_INVOICE_STATUSN_N"] = "新的";
$MESS["CRM_INVOICE_STATUSN_P"] = "有薪酬的";
$MESS["CRM_INVOICE_STATUS_D"] = "拒絕";
$MESS["CRM_INVOICE_STATUS_F"] = "有薪酬的";
$MESS["CRM_INVOICE_STATUS_N"] = "草稿";
$MESS["CRM_INVOICE_STATUS_P"] = "完全的";
$MESS["CRM_STATUS_TYPE_INVOICE_STATUS"] = "發票狀態";
