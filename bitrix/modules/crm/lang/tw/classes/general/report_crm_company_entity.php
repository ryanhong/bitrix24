<?php
$MESS["CRM_COMPANY_ENTITY_ADDRESS"] = "地址";
$MESS["CRM_COMPANY_ENTITY_ADDRESS_LEGAL"] = "合法地址";
$MESS["CRM_COMPANY_ENTITY_BANKING_DETAILS"] = "付款詳情";
$MESS["CRM_COMPANY_ENTITY_COMMENTS"] = "評論";
$MESS["CRM_COMPANY_ENTITY_COMPANY_TYPE_BY"] = "公司類型";
$MESS["CRM_COMPANY_ENTITY_CREATED_BY"] = "由...製作";
$MESS["CRM_COMPANY_ENTITY_CREATED_BY_ID"] = "由...製作";
$MESS["CRM_COMPANY_ENTITY_CURRENCY_BY"] = "貨幣";
$MESS["CRM_COMPANY_ENTITY_CURRENCY_ID"] = "貨幣";
$MESS["CRM_COMPANY_ENTITY_DATE_CREATE"] = "創建於";
$MESS["CRM_COMPANY_ENTITY_DATE_MODIFY"] = "修改";
$MESS["CRM_COMPANY_ENTITY_EMAIL"] = "電子郵件";
$MESS["CRM_COMPANY_ENTITY_EMPLOYEES_BY"] = "僱員";
$MESS["CRM_COMPANY_ENTITY_EVENT_RELATION"] = "事件";
$MESS["CRM_COMPANY_ENTITY_ID"] = "ID";
$MESS["CRM_COMPANY_ENTITY_INDUSTRY_BY"] = "行業";
$MESS["CRM_COMPANY_ENTITY_MESSENGER"] = "信使";
$MESS["CRM_COMPANY_ENTITY_MODIFY_BY"] = "修改";
$MESS["CRM_COMPANY_ENTITY_MODIFY_BY_ID"] = "修改";
$MESS["CRM_COMPANY_ENTITY_PHONE"] = "電話";
$MESS["CRM_COMPANY_ENTITY_REVENUE"] = "年收入";
$MESS["CRM_COMPANY_ENTITY_REVENUE_BY"] = "年收入";
$MESS["CRM_COMPANY_ENTITY_TITLE"] = "公司名稱";
$MESS["CRM_COMPANY_ENTITY_WEB"] = "網站";
