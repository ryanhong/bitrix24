<?php
$MESS["CRM_EXT_SALE_IMPORT_EMPTY_ANSW"] = "服務器返回一個空響應。";
$MESS["CRM_EXT_SALE_IMPORT_ERROR_XML"] = "錯誤解析服務器響應。";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW"] = "身份不明的服務器響應。前100個字符：";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_F"] = "網站數據交換錯誤。";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_PERMS"] = "權限不足。請檢查用戶有訂單導出訪問權限。";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_PERMS1"] = "權限不足。請檢查用戶有訂單導出訪問權限。";
$MESS["CRM_EXT_SALE_IM_GROUP"] = "團體";
$MESS["CRM_EXT_SALE_TITLE_ERROR_SETTINGS"] = "導入錯誤";
$MESS["CRM_EXT_SALE_TITLE_SETTINGS"] = "導入結果";
$MESS["CRM_GCES_NOTIFY_ERROR_MESSAGE"] = "同步錯誤：<br />連接嘗試：10，最後一個：＃date＃<br​​ />連接被停用。<br /> <br />有關更多信息，請打開網絡商店連接表格（< a href =' #url＃'> crm＆gt; web Store連接</a>）。";
$MESS["CRM_GCES_NOTIFY_ERROR_TITLE"] = "與\“＃名稱＃\”同步";
$MESS["CRM_GCES_NOTIFY_MESSAGE"] = "同步成功，接收到：<br />＃totalDeals＃<a href= \"#deal_url# \ \"> deals </a>（＃createDeals＃new）<br />＃totalContacts＃<a href = \ a href = \' ＃\“> contacts </a>（＃createContacts＃new）<br />＃totalCompanies＃<a href= \“#company_url# \ \"> compisions </a>（＃createCompanies＃new）";
$MESS["CRM_GCES_NOTIFY_TITLE"] = "與\“＃名稱＃\”同步";
