<?php
$MESS["CRM_CATALOG_MODULE_IS_NOT_INSTALLED"] = "找不到商業目錄模塊。請安裝此模塊。";
$MESS["CRM_MODULE_IS_NOT_INSTALLED"] = "找不到CRM模塊。請安裝此模塊。";
$MESS["CRM_SALE_MODULE_IS_NOT_INSTALLED"] = "找不到電子商店模塊。請安裝此模塊。";
$MESS["CRM_VAT_EMPTY_VALUE"] = "稅收不包括";
$MESS["CRM_VAT_NOT_SELECTED"] = "[未選中的]";
