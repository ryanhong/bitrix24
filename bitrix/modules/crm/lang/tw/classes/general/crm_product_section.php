<?php
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_INCLUDES_USED_PRODUCTS"] = "無法刪除該部分，因為它包含了交易，潛在客戶，要約或發票的產品。";
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_NAME_EMPTY"] = "未指定截面名稱。";
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_NOT_FOUND"] = "找不到部分。";
$MESS["CRM_PRODUCT_SECTION_FIELD_CATALOG_ID"] = "目錄";
$MESS["CRM_PRODUCT_SECTION_FIELD_CODE"] = "瘋子代碼";
$MESS["CRM_PRODUCT_SECTION_FIELD_ID"] = "ID";
$MESS["CRM_PRODUCT_SECTION_FIELD_NAME"] = "姓名";
$MESS["CRM_PRODUCT_SECTION_FIELD_SECTION_ID"] = "部分";
$MESS["CRM_PRODUCT_SECTION_FIELD_XML_ID"] = "外部ID";
