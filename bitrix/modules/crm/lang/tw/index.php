<?php
$MESS["CRM_INSTALL_DESCRIPTION"] = "提供CRM支持。";
$MESS["CRM_INSTALL_NAME"] = "CRM";
$MESS["CRM_INSTALL_TITLE"] = "CRM模塊安裝";
$MESS["CRM_SALE_MODULE_NOT_INSTALL"] = "在安裝CRM之前，請安裝E商店模塊。";
$MESS["CRM_UNINSTALL_TITLE"] = "CRM模塊卸載";
