<?php
$MESS["CRM_CATALOG_NOT_INSTALLED"] = "未安裝商業目錄模塊。";
$MESS["CRM_CATALOG_STORE_ADR"] = "第五大道944號";
$MESS["CRM_CATALOG_STORE_DESCR"] = "在這裡，您可以從全球領先的供應商那裡找到商品";
$MESS["CRM_CATALOG_STORE_GPS_N"] = "40.7681635";
$MESS["CRM_CATALOG_STORE_GPS_S"] = "-73.95932";
$MESS["CRM_CATALOG_STORE_NAME"] = "倉庫";
$MESS["CRM_CATALOG_STORE_PHONE"] = "(212)123-4567";
$MESS["CRM_CATALOG_STORE_SCHEDULE"] = "星期一至週五，上午9點至下午6點";
$MESS["CRM_DELIVERY_COURIER"] = "送貨服務";
$MESS["CRM_DELIVERY_COURIER_DESCR"] = "白天在指定的時間訂購船舶。";
$MESS["CRM_DELIVERY_PICKUP"] = "當地取貨";
$MESS["CRM_DELIVERY_PICKUP_DESCR"] = "您可以在我們的商店接訂單。";
$MESS["CRM_ORDER_PAY_SYSTEM_CASH_DESC"] = "貨到付款";
$MESS["CRM_ORDER_PAY_SYSTEM_CASH_NAME"] = "現金";
$MESS["CRM_ORDER_PAY_SYSTEM_ORDERDOCUMENT_DESC"] = "可打印發票（應在新窗口中打開）。";
$MESS["CRM_ORDER_PAY_SYSTEM_ORDERDOCUMENT_NAME_V2"] = "發票";
$MESS["CRM_ORD_PROP_2"] = "地點";
$MESS["CRM_ORD_PROP_4"] = "郵遞區號";
$MESS["CRM_ORD_PROP_5"] = "收件地址";
$MESS["CRM_ORD_PROP_6_2"] = "全名";
$MESS["CRM_ORD_PROP_7"] = "帳單地址";
$MESS["CRM_ORD_PROP_8"] = "公司名稱";
$MESS["CRM_ORD_PROP_9"] = "電話";
$MESS["CRM_ORD_PROP_10"] = "聯絡人";
$MESS["CRM_ORD_PROP_11"] = "傳真";
$MESS["CRM_ORD_PROP_12"] = "收件地址";
$MESS["CRM_ORD_PROP_13"] = "稅號";
$MESS["CRM_ORD_PROP_14"] = "#VALUE!";
$MESS["CRM_ORD_PROP_21"] = "城市";
$MESS["CRM_ORD_PROP_40"] = "公司名稱";
$MESS["CRM_ORD_PROP_41"] = "聯絡人";
$MESS["CRM_ORD_PROP_42"] = "收件地址";
$MESS["CRM_ORD_PROP_43"] = "城市";
$MESS["CRM_ORD_PROP_44"] = "郵遞區號";
$MESS["CRM_ORD_PROP_45"] = "電話";
$MESS["CRM_ORD_PROP_46"] = "#VALUE!";
$MESS["CRM_ORD_PROP_47"] = "帳單地址";
$MESS["CRM_ORD_PROP_48"] = "#VALUE!";
$MESS["CRM_ORD_PROP_49"] = "稅號";
$MESS["CRM_ORD_PROP_BIC_SWIFT"] = "BIC/SWIFT";
$MESS["CRM_ORD_PROP_BLZ"] = "#VALUE!";
$MESS["CRM_ORD_PROP_CRN"] = "公司Reg。不。";
$MESS["CRM_ORD_PROP_GROUP_FIZ1"] = "個人資料";
$MESS["CRM_ORD_PROP_GROUP_FIZ2"] = "運輸數據";
$MESS["CRM_ORD_PROP_GROUP_UR1"] = "公司信息";
$MESS["CRM_ORD_PROP_GROUP_UR2"] = "聯繫信息";
$MESS["CRM_ORD_PROP_IBAN"] = "伊班";
$MESS["CRM_ORD_PROP_SORT_CODE"] = "排序代碼";
$MESS["CRM_ORD_PROP_STEU"] = "#VALUE!";
$MESS["CRM_ORD_PROP_TRN"] = "稅收條。不。";
$MESS["CRM_ORD_PROP_UST_IDNR"] = "#VALUE!";
$MESS["CRM_SALE_NOT_INSTALLED"] = "沒有安裝電子商店模塊。";
$MESS["CRM_SALE_PERSON_TYPE_E"] = "法人實體";
$MESS["CRM_SALE_PERSON_TYPE_I"] = "個人";
$MESS["CRM_SALE_USER_GROUP_SHOP_BUYER_DESC"] = "包含所有在線商店客戶的用戶組";
$MESS["CRM_SALE_USER_GROUP_SHOP_BUYER_NAME"] = "所有客戶";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_DESC"] = "用戶組允許編輯在線商店首選項";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_NAME"] = "在線商店管理員";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_DESC"] = "允許用戶組使用在線商店功能";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_NAME"] = "在線商店員工";
