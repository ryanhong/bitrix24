<?php
$MESS["CRM_BP"] = "業務流程";
$MESS["CRM_CONFIG"] = "其他設置";
$MESS["CRM_CURRENCIES"] = "貨幣";
$MESS["CRM_EXTERNAL_SALE"] = "網絡商店";
$MESS["CRM_FIELDS"] = "自定義字段";
$MESS["CRM_GUIDES"] = "選擇列表";
$MESS["CRM_PERMS"] = "訪問權限";
$MESS["CRM_SENDSAVE"] = "電子郵件集成";
