<?php
$MESS["CRM_ACTIVITY_VISIT_BUTTON_FINISH"] = "關閉";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_AGREED"] = "我接受條款";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_BODY_1"] = "重要的！這將記錄與您的訪客的對話。";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_BODY_2"] = "確保您遵循所有規範您所在國家對話記錄的法律。";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_CLOSE"] = "關閉";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_TITLE"] = "使用條款";
$MESS["CRM_ACTIVITY_VISIT_DEFAULT_CAMERA"] = "默認相機";
$MESS["CRM_ACTIVITY_VISIT_ERROR_MIC_FAILURE"] = "無法訪問麥克風。錯誤描述：";
$MESS["CRM_ACTIVITY_VISIT_FACEID_AGREEMENT"] = "<div class = \“ tracker-agreement-popup-content \”>
   <ol class = \“ tracker-agreement-popup-list \”>
    <li class = \“ tracker-agreement-popup-list-item \”>
 N-Tech.lab Ltd提供了Findface Service（以下簡稱為\“ Findface Service \”）。
 Findface服務的用戶必須與Findface Service達成協議，並且必須同意<a target = \“ _ blank \” href = \'https://saas.findface.pro/files/files/docs/docs/license_us.pdf \ “ class = \” tracker-link \“”>以下條款和條件</a>通過選擇\“我同意\”。
 <br>
 通過選擇\“我同意”，用戶同意並保證他們將遵守以下要求：
     <ul class = \“ tracker-agreement-popup-inner-list \”>
      <li class = \“ tracker-agreement-popup-inner-list-item \”>
       嚴格遵守所有當地法律法規，這些法規管理用戶居住國的隱私和個人數據使用情況。
      </li>
      <li class = \“ tracker-agreement-popup-inner-list-item \”>
       嚴格遵守所有地方法律法規。在利用FINDFACE服務的整個過程中，此要求將保持生效。
      </li>
      <li class = \“ tracker-agreement-popup-inner-list-item \”>
       獲得書面授權，或者按照用戶正在運營的每個國家 /地區的當地法律規定。
      </li>
      <li class = \“ tracker-agreement-popup-inner-list-item \”>
       要求用戶勤奮地通知每個人數據的人，包括用戶打算通過使用Findface服務來處理的個人照片和圖像。
      </li>
      <li class = \“ tracker-agreement-popup-inner-list-item \”>
       在利用Findface服務之前尋求法律顧問。
      </li>
      <li class = \“ tracker-agreement-popup-inner-list-item \”>
    確保用戶上傳的任何信息已由個人授權，並且符合用戶管轄權的法律和法規。
   </li>
      <li class = \“ tracker-agreement-popup-inner-list-item \”>
    為了確認用戶使用自己的風險，並且沒有任何形式的保修。
   </li>
     </ul>
    </li>
    <li class = \“ tracker-agreement-popup-list-item \”>
     Bitrix24對服務不承擔任何責任，並且對服務準確性或可用性沒有任何索賠或保證，也沒有提供有關服務的任何技術支持或諮詢。
    </li>
   </ol>
   <div class = \“ tracker-agreement-popup-description \”>
    Bitrix24對Findface服務無負責，也不為FINDFACE服務提供索賠或保證。
 通過接受本服務協議的條款和條件，用戶同意並確認FINDFACE服務，也沒有Bitrix24收集或處理用戶上傳的任何數據。
 用戶確認Findface服務和Bitrix24對用戶收集數據，照片或圖像的情況有任何了解。
 用戶同意對用戶的任何和所有動作和不進行任何操作和所有動作保留Findface服務和Bitrix無害。
 <br> <br>
 爭議解決；約束仲裁：任何爭議，爭議，解釋或索賠，包括但不限於違反合同的索賠，任何形式的疏忽，欺詐或虛假陳述，或與本協議產生或與本協議產生或相關或與之相關或相關或與之相關。
   </div>
  </div>";
$MESS["CRM_ACTIVITY_VISIT_FACEID_SOCIAL_SEACH_IN_PROCESS"] = "搜索數據...";
$MESS["CRM_ACTIVITY_VISIT_FACEID_SOCIAL_SEACH_TITLE"] = "搜索VK配置文件";
$MESS["CRM_ACTIVITY_VISIT_OWNER_CHANGE"] = "改變";
$MESS["CRM_ACTIVITY_VISIT_OWNER_RETAKE_PICTURE"] = "再次接受";
$MESS["CRM_ACTIVITY_VISIT_OWNER_SELECT"] = "選擇";
$MESS["CRM_ACTIVITY_VISIT_OWNER_TAKE_PICTURE"] = "拍個照";
$MESS["CRM_ACTIVITY_VISIT_TITLE"] = "訪問";
