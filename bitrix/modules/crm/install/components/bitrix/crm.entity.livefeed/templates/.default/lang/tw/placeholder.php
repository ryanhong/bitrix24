<?php
$MESS["CRM_ENTITY_LF_PLACEHOLDER_IMAGE_HINT"] = "CRM活動流";
$MESS["CRM_ENTITY_LF_PLACEHOLDER_PARAGRAPH_1"] = "該提要顯示了您的CRM：最近電話中的所有內容；交易，潛在客戶和發票更新。右側的列表顯示緊急任務。";
$MESS["CRM_ENTITY_LF_PLACEHOLDER_PARAGRAPH_2"] = "您可以直接從CRM Feed向客戶發送電子郵件。您會在提要中看到他們的答复。";
