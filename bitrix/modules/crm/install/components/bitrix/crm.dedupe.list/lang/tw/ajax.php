<?php
$MESS["CRM_DEDUPE_LIST_JUNK"] = "信息已過時。請更新列表。";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_DELETE_DENIED"] = "無刪除訪問\ \“＃標題＃\” [＃id＃]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_DELETE_FAILED"] = "無法刪除\“＃標題＃\” [＃id＃]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_NOT_FOUND"] = "找不到項目[＃id＃]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_READ_DENIED"] = "無需閱讀訪問\ \“＃標題＃\” [＃id＃]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_UPDATE_DENIED"] = "無法更新訪問\ \“＃標題＃\” [＃id＃]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_UPDATE_FAILED"] = "無法保存\“＃標題＃\” [＃id＃]";
$MESS["CRM_DEDUPE_LIST_MERGE_GENERAL_ERROR"] = "合併時發生錯誤";
$MESS["CRM_DEDUPE_LIST_REBUILD_INDEX_COMPLETED_SUMMARY"] = "已建立了重複的清單。找到匹配：＃processed_items＃。";
$MESS["CRM_DEDUPE_LIST_REBUILD_INDEX_PROGRESS_SUMMARY"] = "找到匹配：＃processed_items＃";
