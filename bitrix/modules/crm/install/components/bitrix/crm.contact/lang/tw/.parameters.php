<?php
$MESS["CRM_CONTACT_VAR"] = "聯繫ID變量名稱";
$MESS["CRM_ELEMENT_ID"] = "聯繫人ID";
$MESS["CRM_NAME_TEMPLATE"] = "名稱格式";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "聯繫編輯器頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_EXPORT"] = "導出頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "導入頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "索引頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_LIST"] = "聯繫人頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_SERVICE"] = "Web服務頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "聯繫人查看頁面路徑模板";
