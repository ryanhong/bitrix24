<?php
$MESS["CRM_CONTACT_MERGE_HEADER_TEMPLATE"] = "在＃date_create＃上創建";
$MESS["CRM_CONTACT_MERGE_PAGE_TITLE"] = "合併聯繫人";
$MESS["CRM_CONTACT_MERGE_RESULT_LEGEND"] = "從列表中選擇優先聯繫人。它將用作聯繫人配置文件的基礎。您可以從其他聯繫人中添加更多數據。";
