<?php
$MESS["CRM_CALL_DESCR"] = "聯繫＃名稱＃＃電話＃";
$MESS["CRM_EVENT_ADD_BUTTON"] = "添加";
$MESS["CRM_EVENT_ADD_FILE"] = "附加文件";
$MESS["CRM_EVENT_ADD_TITLE"] = "註冊電話";
$MESS["CRM_EVENT_DATE"] = "電話的日期";
$MESS["CRM_EVENT_DESC_TITLE"] = "對話的描述";
$MESS["CRM_EVENT_QUOTE_STATUS_ID_MSGVER_2"] = "估計階段";
$MESS["CRM_EVENT_STAGE_ID"] = "交易階段";
$MESS["CRM_EVENT_STATUS_ID_MSGVER_1"] = "鉛階段";
$MESS["CRM_EVENT_TITLE_COMPANY"] = "公司";
$MESS["CRM_EVENT_TITLE_CONTACT"] = "接觸";
$MESS["CRM_EVENT_TITLE_DEAL"] = "交易";
$MESS["CRM_EVENT_TITLE_LEAD"] = "帶領";
$MESS["CRM_EVENT_TITLE_QUOTE"] = "引用";
$MESS["CRM_NO_PHONES"] = "找不到電話";
$MESS["CRM_PHONE_LIST"] = "電話";
