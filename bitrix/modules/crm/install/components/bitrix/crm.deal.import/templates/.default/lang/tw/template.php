<?php
$MESS["CRM_IMPORT_AGAIN"] = "導入另一個文件";
$MESS["CRM_IMPORT_AGAIN_TITLE"] = "單擊以導入另一個文件";
$MESS["CRM_IMPORT_CANCEL"] = "取消";
$MESS["CRM_IMPORT_CANCEL_TITLE"] = "中止並返回交易清單";
$MESS["CRM_IMPORT_DONE"] = "準備好";
$MESS["CRM_IMPORT_DONE_TITLE"] = "查看交易";
$MESS["CRM_IMPORT_NEXT_STEP"] = "下一個>>";
$MESS["CRM_IMPORT_NEXT_STEP_TITLE"] = "轉到下一步";
$MESS["CRM_IMPORT_PREVIOUS_STEP"] = "<<回";
$MESS["CRM_IMPORT_PREVIOUS_STEP_TITLE"] = "轉到上一個步驟";
$MESS["CRM_TAB_1"] = "導入設置";
$MESS["CRM_TAB_1_TITLE"] = "編輯導入設置";
$MESS["CRM_TAB_2"] = "字段";
$MESS["CRM_TAB_2_TITLE"] = "配置字段映射";
$MESS["CRM_TAB_3"] = "進口";
$MESS["CRM_TAB_3_TITLE"] = "導入結果";
