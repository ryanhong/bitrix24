<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "沒有安裝電子商店模塊。";
$MESS["CRM_TAX_ADD"] = "增加稅";
$MESS["CRM_TAX_ADD_TITLE"] = "創造新稅";
$MESS["CRM_TAX_DELETE"] = "刪除稅";
$MESS["CRM_TAX_DELETE_DLG_BTNTITLE"] = "刪除稅";
$MESS["CRM_TAX_DELETE_DLG_MESSAGE"] = "您確定要刪除此稅嗎？";
$MESS["CRM_TAX_DELETE_DLG_TITLE"] = "刪除稅";
$MESS["CRM_TAX_DELETE_TITLE"] = "刪除稅";
$MESS["CRM_TAX_EDIT"] = "編輯";
$MESS["CRM_TAX_EDIT_TITLE"] = "編輯稅";
$MESS["CRM_TAX_LIST"] = "稅";
$MESS["CRM_TAX_LIST_TITLE"] = "查看所有稅收";
$MESS["CRM_TAX_SETTINGS"] = "設定";
$MESS["CRM_TAX_SETTINGS_TITLE"] = "配置稅收系統";
$MESS["CRM_TAX_SHOW"] = "看法";
$MESS["CRM_TAX_SHOW_TITLE"] = "查看稅";
