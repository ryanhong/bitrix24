<?php
$MESS["CRM_ITEM_EXPORT_CSV_SUMMARY"] = "這將創建一個水療項目導出文件。
導出大量數據可能需要一些時間。";
$MESS["CRM_ITEM_EXPORT_CSV_TITLE"] = "CSV導出";
$MESS["CRM_ITEM_EXPORT_EXCEL_SUMMARY"] = "這將創建一個水療項目導出文件。
導出大量數據可能需要一些時間。";
$MESS["CRM_ITEM_EXPORT_EXCEL_TITLE"] = "導出到Microsoft Excel";
$MESS["CRM_ITEM_LIST_ADD_CHILDREN_ELEMENT"] = "添加＃childror_element＃";
