<?php
$MESS["IMCONNECTOR_IS_NOT_INSTALLED_MSGVER_1"] = "\“外部Messenger Connectors \”模塊未安裝。請安裝模塊。";
$MESS["IMOPENLINES_IS_NOT_INSTALLED"] = "未安裝開放通道模塊（iMopenlines）。請安裝模塊。";
$MESS["STEP_IMCONNECTOR_DESCRIPTION"] = "將Facebook，Instagram和其他社交媒體連接到BITRIX24 OPEN頻道";
$MESS["STEP_IMCONNECTOR_ERROR1"] = "注意：使用的連接器未配置。";
$MESS["STEP_IMCONNECTOR_TITLE"] = "通過社交媒體進行交流和銷售";
