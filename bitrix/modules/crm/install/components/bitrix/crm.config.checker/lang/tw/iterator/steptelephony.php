<?php
$MESS["STEP_TELEPHONY_DESCRIPTION"] = "啟用電話集成可以接收並在您的Bitrix24中撥打電話";
$MESS["STEP_TELEPHONY_ERROR1"] = "找不到配置的電話號碼。";
$MESS["STEP_TELEPHONY_TITLE"] = "致電您的客戶並處理入站電話";
$MESS["VOXIMPLANT_IS_NOT_INSTALLED"] = "未安裝電話模塊（voximplant）。請安裝模塊。";
