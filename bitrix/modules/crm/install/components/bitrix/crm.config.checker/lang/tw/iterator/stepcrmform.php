<?php
$MESS["STEP_CRMFORM_DESCRIPTION"] = "啟用電話集成並配置回調表單";
$MESS["STEP_CRMFORM_ERROR1"] = "電話中沒有電話。";
$MESS["STEP_CRMFORM_ERROR2"] = "有回調表格不會列出活動電話號碼。請選擇這些表格的電話號碼。";
$MESS["STEP_CRMFORM_TITLE"] = "回電您的客戶";
$MESS["VOXIMPLANT_IS_NOT_INSTALLED"] = "電話模塊未安裝。";
