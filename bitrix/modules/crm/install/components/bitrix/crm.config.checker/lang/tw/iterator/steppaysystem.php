<?php
$MESS["SALE_IS_NOT_INSTALLED"] = "E商店模塊（銷售）未安裝。請安裝模塊。";
$MESS["STEP_PAYSYSTEM_DESCRIPTION"] = "連接付款系統並在線接收付款";
$MESS["STEP_PAYSYSTEM_ERROR1"] = "找不到正確配置的付款系統。";
$MESS["STEP_PAYSYSTEM_TITLE"] = "接收在線付款";
