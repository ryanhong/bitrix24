<?php
$MESS["CRM_ACTIVITY_TODO_CLOSE"] = "關閉";
$MESS["CRM_ACTIVITY_TODO_CONTACT"] = "和";
$MESS["CRM_ACTIVITY_TODO_DEADLINE"] = "最後期限";
$MESS["CRM_ACTIVITY_TODO_HOT"] = "（高優先級）";
$MESS["CRM_ACTIVITY_TODO_OPENLINE_COMPLETE_CONF"] = "如果您完成活動，則客戶對話將自動關閉。";
$MESS["CRM_ACTIVITY_TODO_OPENLINE_COMPLETE_CONF_OK_TEXT"] = "完整的活動";
$MESS["CRM_ACTIVITY_TODO_OPENLINE_COMPLETE_CONF_TITLE"] = "您確定要完成活動嗎？";
$MESS["CRM_ACTIVITY_TODO_VIEW_TITLE"] = "活動視圖";
