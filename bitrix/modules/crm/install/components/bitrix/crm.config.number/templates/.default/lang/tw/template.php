<?php
$MESS["CRM_NUMBER_DATE"] = "時期：";
$MESS["CRM_NUMBER_DATE_1"] = "日常的";
$MESS["CRM_NUMBER_DATE_2"] = "每月";
$MESS["CRM_NUMBER_DATE_3"] = "每年";
$MESS["CRM_NUMBER_NUMBER"] = "初始數字：";
$MESS["CRM_NUMBER_NUMBER_DESC"] = "1至7個字符。新值必須大於前一個值。";
$MESS["CRM_NUMBER_PREFIX"] = "字首：";
$MESS["CRM_NUMBER_PREFIX_DESC"] = "1至7個字符（使用拉丁字母，數字，減號和下劃線）。前綴：test_1234";
$MESS["CRM_NUMBER_RANDOM"] = "字符數：";
$MESS["CRM_NUMBER_TEMPL"] = "模板：";
$MESS["CRM_NUMBER_TEMPLATE_EXAMPLE"] = "例子：";
$MESS["CRM_NUMBER_WARNING"] = "字首：";
