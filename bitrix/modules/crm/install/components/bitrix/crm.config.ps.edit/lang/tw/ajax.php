<?php
$MESS["CRM_PS_ACCESS_DENIED"] = "拒絕訪問";
$MESS["CRM_PS_ALREADY_CONFIGURED"] = "私鑰已經存在。";
$MESS["CRM_PS_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PS_NOT_CONFIGURED"] = "在嘗試生成私鑰之前，請在嘗試生成私鑰之前配置處理程序。";
$MESS["CRM_PS_NOT_SUPPORTED"] = "證書請求只能在基於雲的版本中執行。";
$MESS["CRM_PS_PS_MODE_DOCUMENT_TITLE"] = "文檔模板";
$MESS["CRM_PS_PS_MODE_TITLE"] = "支付系統類型";
