<?php
$MESS["CRM_CONFIG_PERMS_REBUILD_ATTR_COMPLETED_SUMMARY"] = "訪問屬性已更新。處理的項目：＃processed_items＃。";
$MESS["CRM_CONFIG_PERMS_REBUILD_ATTR_NOT_REQUIRED_SUMMARY"] = "訪問屬性無需更新。";
$MESS["CRM_CONFIG_PERMS_REBUILD_ATTR_PROGRESS_SUMMARY"] = "處理的項目：＃processed_items＃of＃total_items＃。";
