<?php
$MESS["CRM_ELEMENT_ID"] = "訂購ID";
$MESS["CRM_NAME_TEMPLATE"] = "名稱格式";
$MESS["CRM_ORDER_VAR"] = "訂單ID變量名稱";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "訂單編輯頁面路徑";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "導入頁面路徑";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "索引頁路徑";
$MESS["CRM_SEF_PATH_TO_LIST"] = "訂單頁面路徑";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "訂單查看頁面路徑";
