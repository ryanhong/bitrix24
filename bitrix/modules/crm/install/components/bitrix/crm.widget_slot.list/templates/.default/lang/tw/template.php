<?php
$MESS["CRM_WGT_SLST_ADD"] = "添加";
$MESS["CRM_WGT_SLST_ADD_PRODUCT_SUM"] = "添加產品金額";
$MESS["CRM_WGT_SLST_BY_DEFALT"] = "預設";
$MESS["CRM_WGT_SLST_CALCEL"] = "取消";
$MESS["CRM_WGT_SLST_DLG_WAIT"] = "請稍等...";
$MESS["CRM_WGT_SLST_EDIT"] = "編輯";
$MESS["CRM_WGT_SLST_ERR_FIELD_ALREADY_EXISTS"] = "請選擇另一個字段。 \“＃字段＃\”字段已經在使用中。";
$MESS["CRM_WGT_SLST_ERR_FIELD_LIMT_EXCEEDED"] = "無法添加新字段，因為添加了最大可能的字段。";
$MESS["CRM_WGT_SLST_ERR_NO_FREE_SLOTS"] = "由於沒有免費插槽，因此無法添加新字段。";
$MESS["CRM_WGT_SLST_ERR_SELECT_FIELD"] = "請選擇一個要處理的字段。";
$MESS["CRM_WGT_SLST_GENERAL_INTRO"] = "在此頁面上選擇您的自定義字段，以在CRM分析報告中包含。";
$MESS["CRM_WGT_SLST_LIMITS_INTRO"] = "最多可以選擇十個字段；最多五種相同類型。";
$MESS["CRM_WGT_SLST_LIMITS_TOTALS"] = "選擇：＃總＃總＃＃";
$MESS["CRM_WGT_SLST_LRP_DLG_BTN_CLOSE"] = "關閉";
$MESS["CRM_WGT_SLST_LRP_DLG_BTN_START"] = "跑步";
$MESS["CRM_WGT_SLST_LRP_DLG_BTN_STOP"] = "停止";
$MESS["CRM_WGT_SLST_LRP_DLG_REQUEST_ERR"] = "錯誤處理請求。";
$MESS["CRM_WGT_SLST_NODE_TOOLTIP"] = "如果您將自定義字段用於交易總額，請在此處指定。交易產品總計可以添加到該領域。默認情況下，使用交易的\“ Total \”字段創建報告。";
$MESS["CRM_WGT_SLST_NOT_SELECTED"] = "未選中的";
$MESS["CRM_WGT_SLST_REMOVE"] = "刪除";
$MESS["CRM_WGT_SLST_RESET"] = "重置";
$MESS["CRM_WGT_SLST_SAVE"] = "節省";
$MESS["CRM_WGT_SLST_TOTAL_SUM"] = "總和";
$MESS["CRM_WGT_SLST_USER_FIELDS"] = "自定義字段";
