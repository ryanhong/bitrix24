<?php
$MESS["CRM_BUTTON_LIST_ADD_CAPTION"] = "創建小部件";
$MESS["CRM_BUTTON_LIST_ADD_DESC1"] = "單擊此處創建表單";
$MESS["CRM_BUTTON_LIST_APPLY"] = "申請";
$MESS["CRM_BUTTON_LIST_CANCEL"] = "取消";
$MESS["CRM_BUTTON_LIST_CLOSE"] = "關閉";
$MESS["CRM_BUTTON_LIST_COPY_TO_CLIPBOARD"] = "複製到剪貼板";
$MESS["CRM_BUTTON_LIST_DELETE_CONFIRM"] = "您想刪除這個小部件嗎？";
$MESS["CRM_BUTTON_LIST_ERROR_ACTION"] = "由於發生了錯誤，因此已取消操作。";
$MESS["CRM_BUTTON_LIST_HIDE_DESC"] = "隱藏描述";
$MESS["CRM_BUTTON_LIST_INFO_DESC"] = "一個小部件中的所有溝通方式。";
$MESS["CRM_BUTTON_LIST_INFO_DESC_ITEM_1"] = "實時聊天，回調，CRM表格 - 與客戶保持聯繫所需的任何東西。";
$MESS["CRM_BUTTON_LIST_INFO_DESC_ITEM_2"] = "小部件將改善整體網站轉換並增加銷售額。";
$MESS["CRM_BUTTON_LIST_INFO_DESC_ITEM_3"] = "將小部件代碼添加到您的網站。";
$MESS["CRM_BUTTON_LIST_INFO_DESC_ITEM_4"] = "該站點將顯示“小部件”按鈕。";
$MESS["CRM_BUTTON_LIST_INFO_DESC_ITEM_5"] = "訪問者將單擊按鈕，然後選擇首選的通信方式：實時聊天，電話或網絡表格。";
$MESS["CRM_BUTTON_LIST_INFO_TITLE"] = "這個小部件將與您的Bitrix24帳戶中的客戶統一所有可能的數字通信渠道。";
$MESS["CRM_BUTTON_LIST_ITEM_ACTIVE_ACTIVATED"] = "活性";
$MESS["CRM_BUTTON_LIST_ITEM_ACTIVE_ACT_ON"] = "在";
$MESS["CRM_BUTTON_LIST_ITEM_ACTIVE_BTN_OFF"] = "禁用";
$MESS["CRM_BUTTON_LIST_ITEM_ACTIVE_BTN_ON"] = "使能夠";
$MESS["CRM_BUTTON_LIST_ITEM_ACTIVE_DEACTIVATED"] = "停用";
$MESS["CRM_BUTTON_LIST_ITEM_ACTIVE_OFF"] = "離開";
$MESS["CRM_BUTTON_LIST_ITEM_ACTIVE_OFF_NOW"] = "剛才停用";
$MESS["CRM_BUTTON_LIST_ITEM_ACTIVE_OFF_STATUS"] = "窗口小部件禁用";
$MESS["CRM_BUTTON_LIST_ITEM_ACTIVE_ON"] = "在";
$MESS["CRM_BUTTON_LIST_ITEM_ACTIVE_ON_NOW"] = "剛剛激活";
$MESS["CRM_BUTTON_LIST_ITEM_ACTIVE_ON_STATUS"] = "啟用窗口小部件";
$MESS["CRM_BUTTON_LIST_ITEM_BTN_GET_SCRIPT"] = "嵌入代碼";
$MESS["CRM_BUTTON_LIST_ITEM_DATE_CREATE"] = "創建於";
$MESS["CRM_BUTTON_LIST_ITEM_PUBLIC_LINK_COPY"] = "複製到剪貼板";
$MESS["CRM_BUTTON_LIST_LOCATION"] = "位置";
$MESS["CRM_BUTTON_LIST_NOT_SELECTED"] = "未選中的";
$MESS["CRM_BUTTON_LIST_PLUGIN_BTN_MORE"] = "更多的";
$MESS["CRM_BUTTON_LIST_PLUGIN_DESC"] = "將小部件安裝到您的網站中，並在Bitrix24中與客戶通信。您將獲得一個即時通信工具箱：實時聊天，回調和CRM表單。安裝小部件非常簡單：只需選擇網站的平台，我們將告訴您下一步該怎麼做。";
$MESS["CRM_BUTTON_LIST_PLUGIN_TITLE"] = "CMS插件";
$MESS["CRM_BUTTON_LIST_SITE_SCRIPT_TIP"] = "複製代碼並將其粘貼到網站的模板代碼中，然後再;/lt;/body＆gt;關閉標籤。";
$MESS["CRM_BUTTON_LIST_TITLE"] = "網站小部件";
$MESS["CRM_BUTTON_LIST_VIEW"] = "小部件顯示設置";
