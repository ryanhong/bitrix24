<?php
$MESS["CRM_CATALOG_ID"] = "商業目錄";
$MESS["CRM_CATALOG_NOT_SELECTED"] = "[未選中的]";
$MESS["CRM_PATH_TO_PRODUCT_EDIT"] = "產品編輯頁面路徑模板";
$MESS["CRM_PATH_TO_PRODUCT_LIST"] = "產品頁路徑模板";
$MESS["CRM_PATH_TO_PRODUCT_SHOW"] = "產品視圖頁面路徑模板";
$MESS["CRM_PRODUCT_ID"] = "產品ID";
$MESS["CRM_PRODUCT_ID_PARAM"] = "產品ID變量名稱";
