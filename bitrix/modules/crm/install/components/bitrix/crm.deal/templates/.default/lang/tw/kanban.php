<?php
$MESS["CRM_DEAL_LIST_SWITCHER_LIST"] = "標準";
$MESS["CRM_DEAL_LIST_SWITCHER_RECUR"] = "再次發生的";
$MESS["CRM_KANBAN_SUPERVISOR_N"] = "禁用主管模式";
$MESS["CRM_KANBAN_SUPERVISOR_TITLE"] = "使用主管模式實時查看其他員工的工作。當您不在時，主管模式就會關閉。";
$MESS["CRM_KANBAN_SUPERVISOR_Y"] = "啟用主管模式";
