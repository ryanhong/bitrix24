<?php
$MESS["CRM_DEAL_VAR"] = "交易ID變量名稱";
$MESS["CRM_ELEMENT_ID"] = "交易ID";
$MESS["CRM_NAME_TEMPLATE"] = "名稱格式";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "交易編輯器頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_FUNNEL"] = "銷售渠道頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "導入頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "索引頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_LIST"] = "交易頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "交易查看頁面路徑模板";
