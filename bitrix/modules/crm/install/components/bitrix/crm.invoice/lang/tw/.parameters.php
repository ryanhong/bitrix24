<?php
$MESS["CRM_ELEMENT_ID"] = "發票ID";
$MESS["CRM_INVOICE_VAR"] = "發票ID變量名稱";
$MESS["CRM_NAME_TEMPLATE"] = "名稱格式";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "發票編輯頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "索引頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_LIST"] = "發票列表頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_PAYMENT"] = "發票付款頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "發票查看頁面路徑模板";
