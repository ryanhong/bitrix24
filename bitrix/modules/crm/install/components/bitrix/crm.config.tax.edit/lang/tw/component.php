<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_MODULE_SALE_NOT_INSTALLED"] = "沒有安裝電子商店模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
$MESS["CRM_TAX_ADD_UNKNOWN_ERROR"] = "錯誤創建稅。";
$MESS["CRM_TAX_DELETE_UNKNOWN_ERROR"] = "錯誤刪除稅收。";
$MESS["CRM_TAX_FIELD_CODE"] = "瘋子代碼";
$MESS["CRM_TAX_FIELD_DESCRIPTION"] = "描述";
$MESS["CRM_TAX_FIELD_ID"] = "ID";
$MESS["CRM_TAX_FIELD_LID"] = "網站";
$MESS["CRM_TAX_FIELD_NAME"] = "姓名";
$MESS["CRM_TAX_FIELD_TIMESTAMP_X"] = "修改";
$MESS["CRM_TAX_NOT_FOUND"] = "找不到稅收。";
$MESS["CRM_TAX_RATE_ADD"] = "增加稅率";
$MESS["CRM_TAX_RATE_ADD_TITLE"] = "增加稅率";
$MESS["CRM_TAX_RATE_LIST"] = "稅率";
$MESS["CRM_TAX_SECTION_MAIN"] = "稅";
$MESS["CRM_TAX_UPDATE_UNKNOWN_ERROR"] = "錯誤更新稅款。";
