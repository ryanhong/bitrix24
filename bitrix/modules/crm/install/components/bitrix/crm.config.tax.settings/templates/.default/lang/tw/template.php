<?php
$MESS["CRM_TAX_SETTINGS_CHOOSE"] = "選擇稅型類型";
$MESS["CRM_TAX_SETTINGS_SAVE_BUTTON"] = "節省";
$MESS["CRM_TAX_SETTINGS_TITLE"] = "稅收設置";
$MESS["CRM_TAX_TAX"] = "位置依賴稅（營業稅）";
$MESS["CRM_TAX_TAX1"] = "位置依賴稅（營業稅）";
$MESS["CRM_TAX_TAX_HINT"] = "此稅將總訂單金額適用。稅率取決於客戶位置。";
$MESS["CRM_TAX_TAX_HINT1"] = "此稅將總訂單金額適用。稅率取決於客戶位置。";
$MESS["CRM_TAX_VAT"] = "項目依賴稅（增值稅）";
$MESS["CRM_TAX_VAT_HINT"] = "該稅是指定並分別應用於每個項目的。";
$MESS["CRM_TAX_VAT_HINT1"] = "該稅是指定並分別應用於每個項目的。";
