<?php
$MESS["CRM_BUTTON_CANCEL"] = "取消";
$MESS["CRM_BUTTON_SAVE"] = "節省";
$MESS["CRM_DEAL_CATEGORY_SELECT_DLG_FIELD"] = "管道";
$MESS["CRM_DEAL_CATEGORY_SELECT_DLG_TITLE"] = "交易偏好";
$MESS["CRM_DEAL_CONV_ACCESS_DENIED"] = "您需要許可才能創建聯繫人，公司和交易以完成操作。";
$MESS["CRM_DEAL_CONV_DIALOG_CANCEL_BTN"] = "取消";
$MESS["CRM_DEAL_CONV_DIALOG_CONTINUE_BTN"] = "繼續";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "選擇將創建缺失字段的實體";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "這些字段將被創建";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_LEGEND"] = "選定的實體沒有可以存儲交易數據的字段。請選擇將創建缺少字段以適應所有可用信息的實體。";
$MESS["CRM_DEAL_CONV_DIALOG_TITLE"] = "基於交易生成實體";
$MESS["CRM_DEAL_CONV_GENERAL_ERROR"] = "通用轉換錯誤。";
$MESS["CRM_DEAL_MOVE_TO_CATEGORY_DLG_SUMMARY"] = "目前正在進行的交易將移至新管道的初始階段； Won交易將進入\“交易完成\”階段；丟失的交易 -  \ \“ Deal Lost \”階段。請注意，屬於源管道的階段更新記錄將從歷史記錄中清除。用於計算報告編號的自定義字段將重置。一旦交易移至新管道，他們將立即從頭開始收集數據。";
$MESS["CRM_DEAL_MOVE_TO_CATEGORY_DLG_TITLE"] = "移至新管道";
