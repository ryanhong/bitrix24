<?php
$MESS["CRM_BUTTON_CANCEL"] = "取消";
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "不要保存當前參數";
$MESS["CRM_BUTTON_DELETE"] = "刪除";
$MESS["CRM_BUTTON_DELETE_TITLE"] = "刪除當前集成參數";
$MESS["CRM_BUTTON_SAVE"] = "節省";
$MESS["CRM_BUTTON_SAVE_TITLE"] = "保存集成參數";
$MESS["CRM_TAB_CONFIG"] = "發送並保存集成";
$MESS["CRM_TAB_CONFIG_TITLE"] = "發送並保存集成參數";
$MESS["CRM_TAB_CONFIG_TITLE_CREATE"] = "新的發送和保存電子郵件集成";
$MESS["CRM_TAB_CONFIG_TITLE_EDIT"] = "發送並保存電子郵件集成參數";
