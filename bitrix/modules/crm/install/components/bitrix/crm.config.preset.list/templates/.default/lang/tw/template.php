<?php
$MESS["CRM_ADD_PRESET_DIALOG_ACTIVE_TITLE"] = "積極的";
$MESS["CRM_ADD_PRESET_DIALOG_BTN_ADD_TEXT"] = "添加";
$MESS["CRM_ADD_PRESET_DIALOG_BTN_CANCEL_TEXT"] = "取消";
$MESS["CRM_ADD_PRESET_DIALOG_BTN_EDIT_TEXT"] = "申請";
$MESS["CRM_ADD_PRESET_DIALOG_CREATE_NEW_TITLE"] = "創建新的";
$MESS["CRM_ADD_PRESET_DIALOG_CREATE_SELECTED_TITLE"] = "從列表中選擇";
$MESS["CRM_ADD_PRESET_DIALOG_ERR_EMPTY_NAME"] = "請輸入模板名稱";
$MESS["CRM_ADD_PRESET_DIALOG_ERR_LONG_NAME"] = "模板名稱太長。";
$MESS["CRM_ADD_PRESET_DIALOG_FIXED_PRESET_TITLE"] = "選擇樣本";
$MESS["CRM_ADD_PRESET_DIALOG_NAME_TITLE"] = "姓名";
$MESS["CRM_ADD_PRESET_DIALOG_SORT_TITLE"] = "種類";
$MESS["CRM_ALL"] = "全部的";
$MESS["CRM_PRESET_LIST_ACTION_MENU_DELETE"] = "刪除";
$MESS["CRM_PRESET_LIST_ACTION_MENU_DELETE_CONF"] = "您確定要刪除此模板嗎？";
$MESS["CRM_PRESET_LIST_ACTION_MENU_EDIT"] = "編輯";
$MESS["CRM_PRESET_LIST_ACTION_MENU_FIELD_LIST"] = "字段";
$MESS["CRM_PRESET_LIST_ACTION_MENU_SET_DEF_FOR_COMPANY"] = "為公司製作主要";
$MESS["CRM_PRESET_LIST_ACTION_MENU_SET_DEF_FOR_CONTACT"] = "進行接觸";
$MESS["CRM_PRESET_LIST_CHANGE_CURRENT_COUNTRY_MSG_HIDE_TEXT"] = "隱藏通知";
$MESS["CRM_PRESET_LIST_CHANGE_CURRENT_COUNTRY_MSG_TEXT"] = "您可以選擇該國家作為詳細信息的默認值。模板將自動創建。
當前詳細信息國家 /地區：＃src_country_name＃
默認情況下，新模板將用於聯繫和公司詳細信息。

如果您遷移到新的詳細信息模板（＃dst_country_name＃）：
1.鏈接到詳細信息字段的訂單屬性將重新分配到新的國家詳細信息。
2.如果使用使用詳細信息字段的付款方式，則必須重新配置這些付款方式以手動使用新模板。
3.現有公司和聯繫方式不會改變；他們將繼續使用舊模板。";
$MESS["CRM_PRESET_LIST_CHANGE_CURRENT_COUNTRY_MSG_TEXT_CANCEL"] = "離開，不要再問";
$MESS["CRM_PRESET_LIST_CHANGE_CURRENT_COUNTRY_MSG_TEXT_OK"] = "改變國家";
$MESS["CRM_PRESET_LIST_CHANGE_CURRENT_COUNTRY_MSG_TITLE"] = "詳細信息模板現在可用於您的國家 /地區（＃dst_country_name＃）";
$MESS["CRM_PRESET_TOOLBAR_ADD"] = "添加模板";
$MESS["CRM_PRESET_TOOLBAR_ADD_TITLE"] = "添加新模板";
$MESS["CRM_PRESET_TOOLBAR_EDIT"] = "編輯模板";
$MESS["CRM_PRESET_TOOLBAR_UFIELDS"] = "未使用的自定義字段";
$MESS["CRM_PRESET_TOOLBAR_UFIELDS_TITLE"] = "查看未使用的自定義字段";
