<?php
$MESS["CRM_ENTITY_ID"] = "實體ID";
$MESS["CRM_ENTITY_TYPE"] = "實體類型";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "公司";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "接觸";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "交易";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "帶領";
$MESS["CRM_ENTITY_TYPE_QUOTE_MSGVER_1"] = "估計";
$MESS["CRM_EVENT_COUNT"] = "每頁事件";
$MESS["CRM_EVENT_ENTITY_LINK"] = "顯示實體標題";
$MESS["CRM_NAME_TEMPLATE"] = "名稱格式";
