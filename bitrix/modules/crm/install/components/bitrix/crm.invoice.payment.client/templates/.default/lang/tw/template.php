<?php
$MESS["CIPC_TPL_BANK_PROPS"] = "付款詳情";
$MESS["CIPC_TPL_BITRIX_SIGN"] = "由＃徽標＃提供動力，免費CRM";
$MESS["CIPC_TPL_BUTTON_LOAD"] = "下載";
$MESS["CIPC_TPL_BUTTON_PRINT"] = "列印";
$MESS["CIPC_TPL_PAID"] = "有薪酬的";
$MESS["CIPC_TPL_PAID_DATE"] = "付款日期";
$MESS["CIPC_TPL_PAID_SUM"] = "支付的金額";
$MESS["CIPC_TPL_PAID_SYSTEM"] = "付款";
$MESS["CIPC_TPL_PAID_TITLE"] = "發票## Invoice_id＃of＃date_bill＃";
$MESS["CIPC_TPL_PAY_FOR"] = "使用";
$MESS["CIPC_TPL_PAY_SYSTEM_BLOCK_HEADER"] = "顯示支付系統";
$MESS["CIPC_TPL_RETURN_LIST"] = "返回付款方式";
$MESS["CIPC_TPL_SUM_PAYMENT"] = "應付金額";
