<?php
$MESS["CRM_ALL"] = "全部的";
$MESS["CRM_PS_DELETE"] = "刪除";
$MESS["CRM_PS_DELETE_CONFIRM_TITLE"] = "您確定要刪除此付款系統嗎？";
$MESS["CRM_PS_DELETE_TITLE"] = "刪除付款系統";
$MESS["CRM_PS_EDIT"] = "編輯";
$MESS["CRM_PS_EDIT_TITLE"] = "開放付款系統進行編輯";
$MESS["CRM_PS_LIST_PS_ACTIVE_BTN_OFF"] = "離開";
$MESS["CRM_PS_LIST_PS_ACTIVE_BTN_ON"] = "在";
$MESS["CRM_PS_LIST_PS_ACTIVE_OFF"] = "支付系統不活動";
$MESS["CRM_PS_LIST_PS_ACTIVE_ON"] = "付款系統活躍";
$MESS["CRM_PS_LIST_PS_CREATE"] = "創建付款系統";
$MESS["CRM_PS_LIST_PS_CREATE_DESC"] = "單擊框以創建新的付款系統";
$MESS["CRM_PS_LIST_TITLE"] = "支付系統";
