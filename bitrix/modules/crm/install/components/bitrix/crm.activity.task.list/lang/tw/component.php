<?php
$MESS["CRM_COLUMN_CHANGED_BY"] = "修改";
$MESS["CRM_COLUMN_CLOSED_DATE"] = "關閉";
$MESS["CRM_COLUMN_DATE_CREATE"] = "創建於";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "修改";
$MESS["CRM_COLUMN_DATE_START"] = "開始日期";
$MESS["CRM_COLUMN_ENTITY_TITLE"] = "標題";
$MESS["CRM_COLUMN_ENTITY_TYPE"] = "類型";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_PRIORITY"] = "優先事項";
$MESS["CRM_COLUMN_REAL_STATUS"] = "地位";
$MESS["CRM_COLUMN_RESPONSIBLE_BY"] = "負責人";
$MESS["CRM_COLUMN_TITLE"] = "姓名";
$MESS["CRM_COLUMN_UF_CRM_TASK"] = "CRM項目";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "公司";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "接觸";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "交易";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "帶領";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
$MESS["CRM_PRESET_CHANGE_MY"] = "由我修改";
$MESS["CRM_PRESET_CHANGE_TODAY"] = "今天修改了";
$MESS["CRM_PRESET_CHANGE_YESTERDAY"] = "昨天修改了";
$MESS["CRM_PRESET_MY"] = "我的任務";
$MESS["CRM_PRESET_NEW"] = "新任務";
$MESS["TASKS_MODULE_NOT_INSTALLED"] = "任務模塊未安裝。";
$MESS["TASKS_PRIORITY_0"] = "低的";
$MESS["TASKS_PRIORITY_1"] = "普通的";
$MESS["TASKS_PRIORITY_2"] = "高的";
$MESS["TASKS_STATUS_1"] = "新的";
$MESS["TASKS_STATUS_2"] = "待辦的";
$MESS["TASKS_STATUS_3"] = "進行中";
$MESS["TASKS_STATUS_4"] = "據說已經完成";
$MESS["TASKS_STATUS_5"] = "完全的";
$MESS["TASKS_STATUS_6"] = "遞延";
$MESS["TASKS_STATUS_7"] = "拒絕";
