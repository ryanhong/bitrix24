<?php
$MESS["CRM_ALL"] = "全部的";
$MESS["CRM_CALENDAR_DELETE"] = "刪除電話/會議";
$MESS["CRM_CALENDAR_DELETE_CONFIRM"] = "您確定要刪除該項目嗎？";
$MESS["CRM_CALENDAR_DELETE_TITLE"] = "刪除電話/會議";
$MESS["CRM_CALENDAR_EDIT"] = "編輯通話/會議";
$MESS["CRM_CALENDAR_EDIT_TITLE"] = "編輯通話/會議";
$MESS["CRM_CALENDAR_SHOW"] = "查看通話/會議";
$MESS["CRM_CALENDAR_SHOW_TITLE"] = "查看通話/會議";
