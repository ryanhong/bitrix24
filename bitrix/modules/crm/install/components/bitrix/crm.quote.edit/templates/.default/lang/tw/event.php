<?php
$MESS["CRM_QUOTE_EDIT_EVENT_CANCELED"] = "動作已取消。現在，您被重定向到上一頁。如果當前頁面仍在顯示，請手動關閉它。";
$MESS["CRM_QUOTE_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "引用<a href='#url#'>＃標題＃</a>已創建。現在重定向到上一頁。如果此頁面仍在顯示，請手動關閉它。";
