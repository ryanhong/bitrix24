<?php
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW"] = "歸因窗口";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_DAYS"] = "天";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_OFFLINE"] = "使用歸因窗口進行潛在客戶和交易";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_OFFLINE_HINT"] = "如果註冊日期未先於歸因窗口，則不是從網站流量中創建的潛在客戶和交易將從聯繫人或與之關聯的公司繼承。";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_SITE"] = "歸因窗口";
$MESS["CRM_TRACKING_SETTINGS_ATTR_WINDOW_SITE_HINT"] = "當一個人第一次與您的廣告系列互動，然後採取Bitrix24跟踪的動作之間的天數稱為歸因窗口。您可以根據需要更改歸因期的持續時間，以提高報告準確性。";
$MESS["CRM_TRACKING_SETTINGS_REF_DOMAIN"] = "參考域";
$MESS["CRM_TRACKING_SETTINGS_REF_DOMAIN_USE"] = "添加沒有UTM_Source標籤的社交網絡流量的來源";
