<?php
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_1"] = "您可以通過單擊此圖標再次觀看視頻。";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_2"] = "將更多的通信渠道連接到您的Bitrix24 CRM：電話，電子郵件，打開頻道，CRM表單。";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_3_EMPLOYEE"] = "使用您的個人計數器來評估您的表現：CRM跟踪您今天完成的剩餘任務。您的目標是在一天結束時沒有完成未完成的任務。";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_3_SUPERVISOR"] = "使用計數器查看員工的當前進度：今天完成和剩下的任務。如果一天結束時沒有剩餘的任務，您的員工表現良好。";
$MESS["CRM_CH_TRACKER_WGT_ANIMATION_4"] = "您可以隨時啟用演示視圖。";
$MESS["CRM_CH_TRACKER_WGT_DEMO_TITLE"] = "這是一個演示視圖。隱藏它以分析您的數據。";
$MESS["CRM_CH_TRACKER_WGT_SALE_TARGET"] = "銷售目標";
