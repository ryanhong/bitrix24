<?php
$MESS["CRM_ACTION_VAR"] = "動作ID變量";
$MESS["CRM_REPORT_ID"] = "報告ID";
$MESS["CRM_REPORT_VAR"] = "報告ID變量";
$MESS["CRM_SEF_PATH_TO_CONSTRUCT"] = "報告編輯頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "索引頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_REPORT"] = "報告頁面路徑模板";
$MESS["CRM_SEF_PATH_TO_VIEW"] = "報告查看頁面路徑模板";
