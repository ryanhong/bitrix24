<?php
$MESS["CRM_CONTACT_EDIT_EVENT_CANCELED"] = "動作已取消。現在，您被重定向到上一頁。如果您仍在此頁面上，請手動關閉它。";
$MESS["CRM_CONTACT_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "聯繫人<a href='#url#'>＃標題＃</a>已成功創建。現在，您被重定向到上一頁。如果您仍在此頁面上，請手動關閉它。";
