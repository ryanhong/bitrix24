<?php
$MESS["CRM_1C_START_BACKOFFICE_NAME"] = "1C：backoffice- 2.0";
$MESS["CRM_1C_START_BACKOFFICE_NAME_MSGVER_1"] = "1C的連接器";
$MESS["CRM_1C_START_DOC_ADV_1"] = "易於連接";
$MESS["CRM_1C_START_DOC_ADV_2"] = "簡單配置";
$MESS["CRM_1C_START_DOC_ADV_TITLE"] = "1C的集成數據交換解決方案";
$MESS["CRM_1C_START_DOC_ADV_TITLE_MSGVER_1"] = "1C的集成數據交換解決方案";
$MESS["CRM_1C_START_DOC_DO_START"] = "連接";
$MESS["CRM_1C_START_DOC_INFO_TEXT"] = "新！<br>
<br>
1C的集成數據交換解決方案。<br>
忘記設置。發展業務。<br>
所有數據1C交換在一個整潔的軟件包中<br>
 -  1C和Bitrix24之間的自動或計劃的交易和訂單同步<br>
 - 使用1C而不離開Bitrix24 <br>
 - 在Bitrix24中創建客戶，並自動將其與Bitrix24同步<br>
 - 從您的Bitrix24中搜索1C客戶<br>
 - 通過Bitrix24從1C撥打電話<br>
 - 確保您的伴侶使用1C合法：Spark <br>
 -  1C和BITRIX24之間的實時數據交換<br>
 - 使用1C和自動化規則或觸發器自動化平凡的任務";
