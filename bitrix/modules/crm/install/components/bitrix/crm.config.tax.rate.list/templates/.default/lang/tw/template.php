<?php
$MESS["CRM_ALL"] = "全部的";
$MESS["CRM_TAXRATE_DELETE"] = "刪除稅率";
$MESS["CRM_TAXRATE_DELETE_CONFIRM"] = "您確定要刪除“％s”？";
$MESS["CRM_TAXRATE_DELETE_TITLE"] = "刪除此稅率";
$MESS["CRM_TAXRATE_EDIT"] = "編輯稅率";
$MESS["CRM_TAXRATE_EDIT_TITLE"] = "編輯此稅率";
