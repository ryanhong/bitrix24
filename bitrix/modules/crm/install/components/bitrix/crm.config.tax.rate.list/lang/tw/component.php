<?php
$MESS["CRM_COLUMN_ACTIVE"] = "積極的";
$MESS["CRM_COLUMN_APPLY_ORDER"] = "申請順序";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_IS_IN_PRICE"] = "包括價格稅";
$MESS["CRM_COLUMN_NAME"] = "姓名";
$MESS["CRM_COLUMN_PERSON_TYPE_ID"] = "客戶類型";
$MESS["CRM_COLUMN_TIMESTAMP_X"] = "修改";
$MESS["CRM_COLUMN_VALUE"] = "速度";
$MESS["CRM_COMPANY_PT"] = "公司";
$MESS["CRM_CONTACT_PT"] = "接觸";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "沒有安裝電子商店模塊。";
$MESS["CRM_TAXRATE_DELETION_GENERAL_ERROR"] = "錯誤刪除稅率。";
$MESS["CRM_TAXRATE_UPDATE_GENERAL_ERROR"] = "錯誤更新稅率。";
