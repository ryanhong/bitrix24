<?php
$MESS["CRM_ALL"] = "全部的";
$MESS["CRM_CHECK_LIST_ADD"] = "添加收據";
$MESS["CRM_CHECK_LIST_ADD_SHORT"] = "收據";
$MESS["CRM_ORDER_CHECK_CHECK_STATUS"] = "更新狀態";
$MESS["CRM_ORDER_CHECK_CHECK_STATUS_TITLE"] = "更新收據狀態";
$MESS["CRM_ORDER_CHECK_DELETE"] = "刪除";
$MESS["CRM_ORDER_CHECK_DELETE_CONFIRM"] = "你確定要刪除這個項目嗎？";
$MESS["CRM_ORDER_CHECK_DELETE_TITLE"] = "刪除收據";
$MESS["CRM_ORDER_CHECK_SHOW"] = "看法";
$MESS["CRM_ORDER_CHECK_SHOW_TITLE"] = "查看收據";
$MESS["CRM_ORDER_CHECK_TITLE"] = "收據## ID＃of＃date_create＃";
$MESS["CRM_ORDER_CHECK_URL"] = "收據鏈接";
$MESS["CRM_ORDER_PAYMENT_TITLE"] = "付款## account_number＃of＃date_bill＃";
$MESS["CRM_ORDER_SHIPMENT_TITLE"] = "發貨## Account_number＃of＃date_insert＃";
$MESS["CRM_SHOW_ROW_COUNT"] = "顯示數量";
