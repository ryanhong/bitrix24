<?php
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問。";
$MESS["CRM_SL_EVENT_EDIT_ENTITY_ID_NOT_DEFINED"] = "未指定CRM實體ID。";
$MESS["CRM_SL_EVENT_EDIT_ENTITY_TYPE_NOT_DEFINED"] = "CRM實體類型未指定。";
$MESS["CRM_SL_EVENT_EDIT_HIDDEN_GROUP"] = "隱藏組";
$MESS["CRM_SL_EVENT_NOT_AVAIBLE"] = "當前用戶無法使用事件。";
$MESS["SONET_MODULE_NOT_INSTALLED"] = "沒有安裝社交網絡模塊。";
