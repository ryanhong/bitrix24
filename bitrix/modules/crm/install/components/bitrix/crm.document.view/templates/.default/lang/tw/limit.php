<?php
$MESS["CRM_DOCUMENT_LIMIT_TEXT_EXTENDED"] = "您可以在免費版本中創建最多可創建<span class = \“ document-limit-desc-number \”>＃max＃documents </span>。請升級到選定的商業計劃之一，以獲取不受限制地訪問CRM文檔。";
$MESS["CRM_DOCUMENT_LIMIT_TITLE"] = "僅在擴展計劃上可用";
