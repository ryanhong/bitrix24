<?php
$MESS["CRM_DEAL_CONV_ACCESS_DENIED"] = "創建發票和報價的許可不足。";
$MESS["CRM_DEAL_CONV_DIALOG_CANCEL_BTN"] = "取消";
$MESS["CRM_DEAL_CONV_DIALOG_CONTINUE_BTN"] = "繼續";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "選擇將創建其他字段的實體";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "將創建哪些字段";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_LEGEND"] = "所選的實體沒有可以傳遞交易數據的字段。請選擇將創建其他字段以保存所有可用信息的實體。";
$MESS["CRM_DEAL_CONV_DIALOG_TITLE"] = "創建交易實體";
$MESS["CRM_DEAL_CONV_GENERAL_ERROR"] = "通用錯誤。";
$MESS["CRM_DEAL_CREATE_TITLE"] = "新交易";
$MESS["CRM_DEAL_EDIT_TITLE"] = "交易## id＃＆mdash; ＃標題＃";
$MESS["CRM_DEAL_RECUR_SHOW_TITLE"] = "交易模板## id＃＆mdash; ＃標題＃";
$MESS["CRM_TAB_1"] = "交易";
$MESS["CRM_TAB_1_TITLE"] = "交易屬性";
$MESS["CRM_TAB_2"] = "紀錄";
$MESS["CRM_TAB_2_TITLE"] = "交易日誌";
$MESS["CRM_TAB_3"] = "業務流程";
$MESS["CRM_TAB_3_TITLE"] = "處理業務流程";
