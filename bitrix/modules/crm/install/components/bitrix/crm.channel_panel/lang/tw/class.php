<?php
$MESS["CRM_CHANNEL_1C_CAPTION"] = "1C連接器";
$MESS["CRM_CHANNEL_1C_LEGEND"] = "將所有銷售和客戶歷史記錄出口到CRM。將離線銷售鏈接到在線CRM。";
$MESS["CRM_CHANNEL_CALLBACK_CAPTION"] = "打回來";
$MESS["CRM_CHANNEL_CALLBACK_LEGEND"] = "獲取一個免費的回調小部件。客戶請求回調，新呼叫由Bitrix24進行，並由CRM錄製。";
$MESS["CRM_CHANNEL_CHAT_CAPTION"] = "在線聊天";
$MESS["CRM_CHANNEL_CHAT_LEGEND"] = "獲取一個免費的聊天小部件。所有客戶端請求均發送到BITRIX24；所有對話總是保存給CRM。";
$MESS["CRM_CHANNEL_EMAIL_CAPTION"] = "CRM電子郵件帳戶";
$MESS["CRM_CHANNEL_EMAIL_LEGEND"] = "一條新電子郵件創建了新的線索。電子郵件將添加到聯繫人中，並創建新的活動。";
$MESS["CRM_CHANNEL_OPEN_LINE_CAPTION"] = "開放頻道";
$MESS["CRM_CHANNEL_OPEN_LINE_LEGEND"] = "將客戶對話從社會服務遷移到Bitrix24聊天。所有對話都保存給CRM。";
$MESS["CRM_CHANNEL_TELEPHONY_CAPTION"] = "電話";
$MESS["CRM_CHANNEL_TELEPHONY_LEGEND"] = "所有電話都保存在CRM中。即使錯過了電話，也總是會創建新的線索。";
$MESS["CRM_CHANNEL_WEB_FORM_CAPTION"] = "CRM形式";
$MESS["CRM_CHANNEL_WEB_FORM_LEGEND"] = "申請，表格，註冊表將直接添加到CRM。 CRM表單很容易創建 - 即使您沒有網站。";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
