<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "未安裝業務流程模塊。";
$MESS["CRM_CONTACT_ADD_INVOICE_TITLE"] = "創建聯繫發票";
$MESS["CRM_CONTACT_EVENT_INFO"] = "＃event_name＃＆rarr; ＃new_value＃";
$MESS["CRM_CONTACT_NAV_TITLE_ADD"] = "增加聯繫人";
$MESS["CRM_CONTACT_NAV_TITLE_EDIT"] = "聯繫人姓名＃";
$MESS["CRM_CONTACT_NAV_TITLE_LIST"] = "聯繫人";
$MESS["CRM_CONTACT_NAV_TITLE_LIST_SHORT"] = "聯繫人";
$MESS["CRM_CONTACT_SHOW_FIELD_BIRTHDATE"] = "出生日期";
$MESS["CRM_FIELD_ACTIVITY_LIST"] = "接觸活動";
$MESS["CRM_FIELD_ADDRESS"] = "地址";
$MESS["CRM_FIELD_ADDRESS_2"] = "地址（第2行）";
$MESS["CRM_FIELD_ADDRESS_CITY"] = "城市";
$MESS["CRM_FIELD_ADDRESS_COUNTRY"] = "國家";
$MESS["CRM_FIELD_ADDRESS_POSTAL_CODE"] = "壓縮";
$MESS["CRM_FIELD_ADDRESS_PROVINCE"] = "州 /省";
$MESS["CRM_FIELD_ADDRESS_REGION"] = "地區";
$MESS["CRM_FIELD_ASSIGNED_BY_ID"] = "負責任的";
$MESS["CRM_FIELD_BIRTHDATE"] = "生日";
$MESS["CRM_FIELD_COMMENTS"] = "評論";
$MESS["CRM_FIELD_COMPANY_ID"] = "公司";
$MESS["CRM_FIELD_COMPANY_TITLE"] = "公司";
$MESS["CRM_FIELD_CONTACT_ACTIVITY"] = "任務";
$MESS["CRM_FIELD_CONTACT_BIZPROC"] = "業務流程";
$MESS["CRM_FIELD_CONTACT_CALENDAR"] = "日曆記錄";
$MESS["CRM_FIELD_CONTACT_COMPANY"] = "聯繫公司";
$MESS["CRM_FIELD_CONTACT_DEAL"] = "聯繫交易";
$MESS["CRM_FIELD_CONTACT_EVENT"] = "聯繫交易";
$MESS["CRM_FIELD_CONTACT_INVOICE"] = "聯繫發票";
$MESS["CRM_FIELD_CONTACT_LEAD"] = "聯繫人的線索";
$MESS["CRM_FIELD_CONTACT_QUOTE_MSGVER_1"] = "聯繫估計";
$MESS["CRM_FIELD_CONTACT_REQUISITE"] = "聯繫方式";
$MESS["CRM_FIELD_CREATED_BY_ID"] = "由...製作";
$MESS["CRM_FIELD_DATE_CREATE"] = "創建";
$MESS["CRM_FIELD_DATE_MODIFY"] = "修改的";
$MESS["CRM_FIELD_DEALS"] = "聯繫人交易";
$MESS["CRM_FIELD_EMAIL"] = "電子郵件";
$MESS["CRM_FIELD_ENTITY_TREE"] = "依賴性";
$MESS["CRM_FIELD_EXPORT"] = "在觸點出口中使用";
$MESS["CRM_FIELD_FIND"] = "搜尋";
$MESS["CRM_FIELD_FULL_NAME"] = "全名";
$MESS["CRM_FIELD_HONORIFIC"] = "致敬";
$MESS["CRM_FIELD_LAST_NAME"] = "姓";
$MESS["CRM_FIELD_LEADS"] = "接觸線";
$MESS["CRM_FIELD_LEAD_ACTIVITY"] = "接觸活動";
$MESS["CRM_FIELD_MESSENGER"] = "信使";
$MESS["CRM_FIELD_MODIFY_BY_ID"] = "修改";
$MESS["CRM_FIELD_NAME"] = "名";
$MESS["CRM_FIELD_OPENED"] = "每個人都可以使用";
$MESS["CRM_FIELD_OPPORTUNITY"] = "機會";
$MESS["CRM_FIELD_PHONE"] = "電話";
$MESS["CRM_FIELD_PHOTO"] = "照片";
$MESS["CRM_FIELD_POST"] = "位置";
$MESS["CRM_FIELD_SECOND_NAME"] = "中間名字";
$MESS["CRM_FIELD_SOURCE_DESCRIPTION"] = "描述";
$MESS["CRM_FIELD_SOURCE_ID"] = "來源";
$MESS["CRM_FIELD_STATUS_DESCRIPTION"] = "描述";
$MESS["CRM_FIELD_STATUS_ID"] = "地位";
$MESS["CRM_FIELD_TYPE_ID"] = "聯繫人類型";
$MESS["CRM_FIELD_UTM"] = "UTM參數";
$MESS["CRM_FIELD_WEB"] = "地點";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
$MESS["CRM_SECTION_ACTIVITIES"] = "活動";
$MESS["CRM_SECTION_ACTIVITY_CALENDAR"] = "電話和事件";
$MESS["CRM_SECTION_ACTIVITY_GRID"] = "接觸活動";
$MESS["CRM_SECTION_ACTIVITY_TASK"] = "任務";
$MESS["CRM_SECTION_ADDITIONAL"] = "更多的";
$MESS["CRM_SECTION_BIZPROC"] = "業務流程";
$MESS["CRM_SECTION_CONTACT_INFO"] = "聯繫信息";
$MESS["CRM_SECTION_CONTACT_INFO_2"] = "關於聯繫";
$MESS["CRM_SECTION_DEALS"] = "交易";
$MESS["CRM_SECTION_DETAILS"] = "細節";
$MESS["CRM_SECTION_EVENT"] = "接觸更改日誌";
$MESS["CRM_SECTION_LEADS"] = "鉛";
$MESS["CRM_SECTION_LIVE_FEED"] = "溪流";
$MESS["RESPONSIBLE_NOT_ASSIGNED"] = "[未分配]";
