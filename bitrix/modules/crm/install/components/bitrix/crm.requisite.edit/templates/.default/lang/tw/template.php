<?php
$MESS["CRM_REQUISITE_CUSTOM_SAVE_BUTTON_TITLE"] = "保存並返回";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_BANK_DETAIL_SUMMARY_TITLE"] = "通過銀行詳細信息";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_REQUISITE_SUMMARY_TITLE"] = "通過聯繫/公司詳細信息";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_SHORT_SUMMARY_TITLE"] = "成立";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_WARNING_ACCEPT_BTN_TITLE"] = "忽略並保存";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_WARNING_CANCEL_BTN_TITLE"] = "取消";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_WARNING_DLG_TITLE"] = "可能的克隆";
$MESS["CRM_REQUISITE_SERCH_RESULT_NOT_FOUND"] = "您的搜索請求返回沒有結果。";
$MESS["CRM_REQUISITE_SHOW_NEW_TITLE_COMPANY"] = "新公司的詳細信息";
$MESS["CRM_REQUISITE_SHOW_NEW_TITLE_CONTACT"] = "新的聯繫方式";
$MESS["CRM_REQUISITE_SHOW_TITLE_COMPANY"] = "公司詳細信息";
$MESS["CRM_REQUISITE_SHOW_TITLE_CONTACT"] = "聯繫方式";
$MESS["CRM_TAB_1_COMPANY"] = "公司詳細信息";
$MESS["CRM_TAB_1_CONTACT"] = "聯繫方式";
$MESS["CRM_TAB_1_TITLE_COMPANY"] = "公司詳細信息信息";
$MESS["CRM_TAB_1_TITLE_CONTACT"] = "聯繫方式信息";
