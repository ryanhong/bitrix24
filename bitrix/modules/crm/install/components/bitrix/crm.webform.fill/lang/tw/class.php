<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "未安裝業務流程模塊。";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "未安裝商業目錄模塊。";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "貨幣模塊未安裝。";
$MESS["CRM_MODULE_NOT_INSTALLED_REST"] = "未安裝其餘模塊。";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "沒有安裝電子商店模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
$MESS["CRM_WEBFORM_ERROR_DEACTIVATED"] = "形式不活躍。";
$MESS["CRM_WEBFORM_ERROR_NOT_FOUND"] = "找不到形式。";
$MESS["CRM_WEBFORM_ERROR_SECURITY"] = "形式URL可能發生了變化。";
$MESS["CRM_WEBFORM_PREVIEW_AD_AUTH_SUBTITLE"] = "有人向您發送了表單預覽鏈接。請登錄Bitrix24或向所有者索取公共鏈接。";
$MESS["CRM_WEBFORM_PREVIEW_AD_AUTH_TITLE"] = "你沒有登錄";
$MESS["CRM_WEBFORM_PREVIEW_AD_NSD_SUBTITLE"] = "有人向您發送了表單預覽鏈接。您需要獲得各自的訪問權限；否則，請所有者提供公共鏈接。";
$MESS["CRM_WEBFORM_PREVIEW_AD_NSD_TITLE"] = "拒絕訪問";
