<?php
$MESS["CRM_COLUMN_CODE"] = "瘋子代碼";
$MESS["CRM_COLUMN_DATE"] = "修改";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_NAME"] = "名稱 /描述";
$MESS["CRM_COLUMN_RATES"] = "稅率";
$MESS["CRM_COLUMN_SITE"] = "網站";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "未安裝CRM模塊。";
$MESS["CRM_PERMISSION_DENIED"] = "拒絕訪問";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "沒有安裝電子商店模塊。";
$MESS["CRM_TAX_DELETION_GENERAL_ERROR"] = "錯誤刪除稅收。";
$MESS["CRM_TAX_UPDATE_GENERAL_ERROR"] = "錯誤更新稅款。";
