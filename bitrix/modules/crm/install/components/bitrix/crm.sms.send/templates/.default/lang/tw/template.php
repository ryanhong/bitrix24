<?php
$MESS["CRM_SMS_CANCEL"] = "取消";
$MESS["CRM_SMS_ERROR_NO_COMMUNICATIONS"] = "請在詳細信息中提供客戶電話號碼以發送SMS消息。";
$MESS["CRM_SMS_FROM"] = "從數字";
$MESS["CRM_SMS_MANAGE_TEXT_1"] = "探索SMS營銷的可能性";
$MESS["CRM_SMS_MANAGE_TEXT_2"] = "Bitrix24提供了簡單且無憂的短信消息傳遞。";
$MESS["CRM_SMS_MANAGE_TEXT_3_MSGVER_1"] = "直接從潛在客戶，交易，聯繫或估計中發送消息。";
$MESS["CRM_SMS_MANAGE_URL"] = "配置";
$MESS["CRM_SMS_REST_MARKETPLACE"] = "選擇其他服務";
$MESS["CRM_SMS_SEND"] = "發送";
$MESS["CRM_SMS_SENDER"] = "使用";
$MESS["CRM_SMS_SEND_MESSAGE"] = "發送SMS消息";
$MESS["CRM_SMS_SEND_SENDER_SUBTITLE"] = "使用＃發件人＃";
$MESS["CRM_SMS_SYMBOLS"] = "人物";
$MESS["CRM_SMS_SYMBOLS_FROM"] = "的";
$MESS["CRM_SMS_TO"] = "到";
