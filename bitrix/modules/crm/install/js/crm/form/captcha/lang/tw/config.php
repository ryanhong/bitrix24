<?php
$MESS["CRM_FORM_CAPTCHA_JS_ACCESS_DENIED"] = "拒絕訪問。請聯繫您的BitRix24管理員。";
$MESS["CRM_FORM_CAPTCHA_JS_CUSTOM_HOWTO"] = "如何獲取鑰匙？";
$MESS["CRM_FORM_CAPTCHA_JS_CUSTOM_TEXT"] = "您可以選擇使用自己的垃圾郵件保護鍵。它們將與所有CRM表格一起使用。";
$MESS["CRM_FORM_CAPTCHA_JS_CUSTOM_TITLE"] = "使用自定義recaptcha v2鍵";
$MESS["CRM_FORM_CAPTCHA_JS_STD_TEXT"] = "配置完成。您現在可以使用垃圾郵件保護。";
$MESS["CRM_FORM_CAPTCHA_JS_STD_TITLE"] = "自動配置";
$MESS["CRM_FORM_CAPTCHA_JS_TITLE"] = "垃圾郵件保護設置";
