<?php
$MESS["CRM_AI_FEEDBACK_POPUP_BUTTON_ANOTHER_TIME"] = "之後";
$MESS["CRM_AI_FEEDBACK_POPUP_BUTTON_SHARE"] = "分享";
$MESS["CRM_AI_FEEDBACK_POPUP_TEXT"] = "這將有助於我們培訓Copilot，以獲得更精確的語音識別和現場完成。您的錄音只能用於提高副本性能。";
$MESS["CRM_AI_FEEDBACK_POPUP_TITLE"] = "向我們發送您的電話錄音";
