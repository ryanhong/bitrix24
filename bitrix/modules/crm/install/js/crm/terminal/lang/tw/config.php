<?php
$MESS["TERMINAL_QR_AUTH_CONTENT_MSGVER_1"] = "使用您的個人QR碼進行更快的身份驗證。 <br> <br> <b>切勿向任何人展示此代碼。</b>它包含您的個人登錄信息。";
$MESS["TERMINAL_QR_AUTH_TITLE"] = "移動應用中的開放終端";
