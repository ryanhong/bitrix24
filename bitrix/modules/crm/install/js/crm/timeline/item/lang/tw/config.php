<?php
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_COPY_PUBLIC_LINK_ERROR"] = "無法將鏈接複製到剪貼板。請再試一次。";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_CREATE_PUBLIC_LINK_ERROR"] = "錯誤創建文檔公共鏈接";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_PDF_NOT_READY"] = "PDF文件仍在創建。請稍後再試。您可以立即下載相應的DOCX文件。";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_PRINT_NOT_READY"] = "無法打印文檔，因為PDF文件仍在創建。請稍後再試。";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DOCUMENT_UPDATE_DOCUMENT_ERROR"] = "錯誤更新文檔";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DO_USE_PREVIOUS_MSGVER_1"] = "該模板已在負責人\“％啟動器％\”的DEAL \“％title％\”的％創建_AT％上使用。您想再次使用它嗎？";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DO_USE_PREVIOUS_MSGVER_2"] = "以前與Deal \“％title％\”一起使用了帶有文本和字段的完整配置模板。負責人是％啟動者％。您可以繼續使用此模板，或者在合適的情況下對其進行編輯。";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_DO_USE_PREVIOUS_MSGVER_3"] = "帶有文本和配置字段的模板已與Deal \“％title％\”一起使用，負責人是\“％啟動器％\”。您可以重複使用此模板或創建一個新模板。";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_NEW_BUTTON_MSGVER_1"] = "使用其他模板";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_NEW_BUTTON_MSGVER_2"] = "查看和編輯";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_NEW_BUTTON_MSGVER_3"] = "創建新的";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_OLD_BUTTON_MSGVER_1"] = "無論如何使用此模板";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_OLD_BUTTON_MSGVER_2"] = "無論如何使用現有";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_OPENLINE_COMPLETE_CONF"] = "如果您完成活動，則客戶對話將自動關閉。";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_OPENLINE_COMPLETE_CONF_OK_TEXT"] = "完整的活動";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_OPENLINE_COMPLETE_CONF_TITLE"] = "您確定要完成活動嗎？";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_POPUP_CLOSE"] = "關閉";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_POPUP_TITLE_MSGVER_1"] = "簽名";
$MESS["CRM_TIMELINE_ITEM_ACTIVITY_POPUP_TITLE_MSGVER_2"] = "找到了以前的模板";
$MESS["CRM_TIMELINE_ITEM_AI_PROVIDER_POPUP_OK_TEXT"] = "公開市場";
$MESS["CRM_TIMELINE_ITEM_AI_PROVIDER_POPUP_TEXT"] = "Copilot將轉錄您的客戶電話，創建摘要，並填寫交易字段，並使用客戶在對話中共享的信息。 [helpdesklink]了解更多[/helpdesklink]";
$MESS["CRM_TIMELINE_ITEM_AI_PROVIDER_POPUP_TITLE"] = "從市場上安裝一個應用程序以在CRM中使用Copilot";
$MESS["CRM_TIMELINE_ITEM_CALENDAR_SHARING_SLOTS_HOUR"] = "小時";
$MESS["CRM_TIMELINE_ITEM_CALENDAR_SHARING_SLOTS_LENGTH"] = "對於％slot_length％";
$MESS["CRM_TIMELINE_ITEM_CALENDAR_SHARING_SLOTS_MINUTES"] = "分鐘";
$MESS["CRM_TIMELINE_ITEM_CALENDAR_SHARING_SLOTS_RANGE"] = "＃工作日＃;可用性：＃from_time＃to＃to_time＃;持續時間：＃持續時間＃";
$MESS["CRM_TIMELINE_ITEM_CALENDAR_SHARING_SLOTS_RANGE_V2"] = "＃工作日＃，可用性：＃from_time＃to＃to_time＃，持續時間：＃持續時間＃";
$MESS["CRM_TIMELINE_ITEM_CALENDAR_SHARING_SLOTS_WORK_DAYS"] = "工作日";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_CANCEL"] = "取消";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_HIDE_MSGVER_1"] = "坍塌";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_PLACEHOLDER"] = "要做的事情";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_SAVE"] = "節省";
$MESS["CRM_TIMELINE_ITEM_EDITABLE_DESCRIPTION_SHOW_MSGVER_1"] = "擴張";
$MESS["CRM_TIMELINE_ITEM_FILE_LIST_COLLAPSE"] = "坍塌";
$MESS["CRM_TIMELINE_ITEM_FILE_LIST_EXPAND"] = "顯示所有";
$MESS["CRM_TIMELINE_ITEM_FILE_LIST_SHOW_MORE"] = "顯示＃計數＃更多";
$MESS["CRM_TIMELINE_ITEM_LINK_IS_COPIED"] = "鏈接複製到剪貼板";
$MESS["CRM_TIMELINE_ITEM_NOTE_HIDE"] = "坍塌";
$MESS["CRM_TIMELINE_ITEM_NOTE_SHOW"] = "擴張";
$MESS["CRM_TIMELINE_ITEM_NO_AI_PROVIDER_POPUP_OK_TEXT"] = "需要副駕駛。";
$MESS["CRM_TIMELINE_ITEM_NO_AI_PROVIDER_POPUP_TEXT"] = "您所在地區沒有AI提供者。如果您想在CRM中使用Copilot，請給我們留言。";
$MESS["CRM_TIMELINE_ITEM_NO_AI_PROVIDER_POPUP_TITLE"] = "找不到任何Copilot提供商";
$MESS["CRM_TIMELINE_ITEM_SIGN_DOCUMENT_RESEND_SUCCESS"] = "發送";
$MESS["CRM_TIMELINE_ITEM_TASK_CONFIRM_DELETE"] = "您確定要刪除此任務嗎？";
$MESS["CRM_TIMELINE_ITEM_TASK_DELETE"] = "任務已刪除";
$MESS["CRM_TIMELINE_ITEM_TASK_PING_SENT"] = "發送通知發送";
