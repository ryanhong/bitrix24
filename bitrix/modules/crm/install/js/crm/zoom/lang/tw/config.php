<?php
$MESS["CRM_ZOOM_CREATE_MEETING_SERVER_RETURNS_ERROR"] = "由於服務器錯誤而無法創建會議：";
$MESS["CRM_ZOOM_ERROR_EMPTY_TITLE"] = "會議名稱不能為空。";
$MESS["CRM_ZOOM_ERROR_INCORRECT_DATETIME"] = "開始時間不能比現在少。";
$MESS["CRM_ZOOM_ERROR_INCORRECT_DURATION"] = "會議期限不正確。";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DATE_CAPTION"] = "開始日期和時間";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DURATION_CAPTION"] = "期間";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DURATION_HOURS"] = "小時";
$MESS["CRM_ZOOM_NEW_CONFERENCE_DURATION_MINUTES"] = "分鐘";
$MESS["CRM_ZOOM_NEW_CONFERENCE_TITLE"] = "姓名";
$MESS["CRM_ZOOM_NEW_CONFERENCE_TITLE_PLACEHOLDER"] = "縮放會議";
