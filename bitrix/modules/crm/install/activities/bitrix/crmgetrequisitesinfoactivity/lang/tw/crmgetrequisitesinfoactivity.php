<?php
$MESS["CRM_GRI_ADDRESS_TYPE"] = "地址類型";
$MESS["CRM_GRI_COMPANY_NOT_EXISTS"] = "沒有發現公司";
$MESS["CRM_GRI_CONTACT_NOT_EXISTS"] = "沒有找到聯繫";
$MESS["CRM_GRI_COUNTRY"] = "國家";
$MESS["CRM_GRI_EMPTY_PROP"] = "所需的參數為空：＃屬性＃";
$MESS["CRM_GRI_ENTITY_COMPANY"] = "公司";
$MESS["CRM_GRI_ENTITY_CONTACT"] = "接觸";
$MESS["CRM_GRI_ENTITY_ID"] = "實體ID";
$MESS["CRM_GRI_ENTITY_TYPE"] = "實體類型";
$MESS["CRM_GRI_ENTITY_TYPE_ERROR"] = "選擇不正確的實體類型";
$MESS["CRM_GRI_READ_PERMISSION_ERROR"] = "閱讀許可不足";
$MESS["CRM_GRI_REQUISITE_PRESET_NOT_EXIST"] = "template \“＃template＃\”用於實體類型\“＃類型＃\” ID \“＃id＃\”未找到";
$MESS["CRM_GRI_REQUISITE_TEMPLATES"] = "模板";
