<?php
$MESS["CRM_CDA_CYCLING_ERROR"] = "無法移動智能過程自動化，因為可能有無限的循環";
$MESS["CRM_CDCA_ENABLING_CATEGORIES_ERROR"] = "該水療中心禁用管道";
$MESS["CRM_CDCA_MOVE_ERROR_CATEGORY_NOT_CHANGED"] = "智能過程自動化已經是該管道的一部分";
$MESS["CRM_CDCA_MOVE_TERMINATION_TITLE"] = "管道更改時自動完成";
