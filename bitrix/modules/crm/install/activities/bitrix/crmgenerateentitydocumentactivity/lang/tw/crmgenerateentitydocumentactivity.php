<?php
$MESS["CRM_GEDA_EMPTY_TEMPLATE_ID"] = "未指定文檔模板";
$MESS["CRM_GEDA_MODULE_DOCGEN_ERROR"] = "未安裝文檔生成器模塊。";
$MESS["CRM_GEDA_NAME_CREATE_ACTIVITY"] = "添加活動";
$MESS["CRM_GEDA_NAME_DELETE"] = "刪除";
$MESS["CRM_GEDA_NAME_MY_COMPANY_BANK_DETAIL_ID"] = "我的公司銀行詳細信息";
$MESS["CRM_GEDA_NAME_MY_COMPANY_ID"] = "我的公司";
$MESS["CRM_GEDA_NAME_MY_COMPANY_REQUISITE_ID"] = "我的公司詳細信息";
$MESS["CRM_GEDA_NAME_PUBLIC_URL"] = "創建公共鏈接";
$MESS["CRM_GEDA_NAME_TEMPLATE_ID"] = "模板";
$MESS["CRM_GEDA_NAME_USE_SUBSCRIPTION"] = "等待PDF轉換完成";
$MESS["CRM_GEDA_NAME_WAIT_FOR_EVENT_LOG"] = "工作流正在等待文檔完成轉換";
$MESS["CRM_GEDA_NAME_WAIT_FOR_EVENT_LOG_COMPLETE"] = "轉換文檔";
$MESS["CRM_GEDA_NAME_WITH_STAMPS"] = "帶有簽名和郵票";
$MESS["CRM_GEDA_PROVIDER_NOT_FOUND"] = "找不到此實體類型的處理程序";
$MESS["CRM_GEDA_TRANSFORMATION_ERROR"] = "錯誤轉換文檔：";
