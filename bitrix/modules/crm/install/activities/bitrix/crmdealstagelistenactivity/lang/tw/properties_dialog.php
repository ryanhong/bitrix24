<?php
$MESS["BPCDSA_PD_DEAL"] = "交易ID";
$MESS["BPCDSA_PD_STAGE"] = "階段";
$MESS["BPCDSA_PD_STAGE_DESCR"] = "選擇一個或多個階段以等待。工作流將等到交易轉移到選定或最後階段。";
