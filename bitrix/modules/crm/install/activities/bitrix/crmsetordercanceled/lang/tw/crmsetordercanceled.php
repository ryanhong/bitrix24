<?php
$MESS["CRM_SOCCL_COMMENT_NAME"] = "取消的原因";
$MESS["CRM_SOCCL_ORDER_ERROR"] = "當前實體不是\“ order \”類型實體";
$MESS["CRM_SOCCL_ORDER_IS_CANCELED"] = "訂單已經被取消";
$MESS["CRM_SOCCL_ORDER_NOT_FOUND"] = "無法獲取訂單數據";
$MESS["CRM_SOCCL_STATUS_ERROR"] = "未指定取消狀態";
$MESS["CRM_SOCCL_STATUS_NAME"] = "取消狀態";
$MESS["CRM_SOCCL_TERMINATE"] = "由於狀態已更改而完成";
