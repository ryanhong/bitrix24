<?php
$MESS["CRM_DDA_DESC"] = "刪除智能過程自動化";
$MESS["CRM_DDA_DESC_1"] = "刪除CRM項目";
$MESS["CRM_DDA_DESC_2"] = "當不再需要時，刪除CRM項目。";
$MESS["CRM_DDA_NAME"] = "刪除水療中心";
$MESS["CRM_DDA_NAME_1"] = "刪除CRM項目";
$MESS["CRM_DDA_ROBOT_DESCRIPTION_DIGITAL_WORKPLACE"] = "當不再需要時，將刪除水療項目。";
