<?php
$MESS["CRM_GRIA_DESC"] = "鏈接的項目信息";
$MESS["CRM_GRIA_DESC_1"] = "將鏈接項目字段的值發送到其他自動化規則（助手）。";
$MESS["CRM_GRIA_NAME"] = "鏈接的項目信息";
$MESS["CRM_GRIA_NAME_1"] = "獲取鏈接項目的數據";
