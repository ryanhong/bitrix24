<?php
$MESS["CRM_WAIT_ACTIVITY_DESC"] = "等待客戶或指定的時間";
$MESS["CRM_WAIT_ACTIVITY_DESC_1"] = "暫停指定天數的項目，或直到客戶回來為止。";
$MESS["CRM_WAIT_ACTIVITY_NAME"] = "等待";
$MESS["CRM_WAIT_ACTIVITY_NAME_1"] = "暫停";
