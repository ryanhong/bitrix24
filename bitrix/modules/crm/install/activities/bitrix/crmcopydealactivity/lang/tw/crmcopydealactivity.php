<?php
$MESS["CRM_CDA_CHANGE_RESPONSIBLE"] = "負責人";
$MESS["CRM_CDA_CHANGE_STAGE"] = "初期";
$MESS["CRM_CDA_COPY_PRODUCTS_ERROR"] = "無法從源交易中復制產品項目";
$MESS["CRM_CDA_CYCLING_ERROR"] = "由於可能的無盡循環，無法複製交易";
$MESS["CRM_CDA_CYCLING_EXCEPTION_MESSAGE"] = "自動化規則未啟動";
$MESS["CRM_CDA_DEAL_TITLE"] = "交易名稱";
$MESS["CRM_CDA_MOVE_TO_CATEGORY"] = "移至管道";
$MESS["CRM_CDA_NEW_DEAL_TITLE"] = "＃source_title＃（複製）";
$MESS["CRM_CDA_NO_SOURCE_FIELDS"] = "無法獲得源交易數據";
$MESS["CRM_CDA_STAGE_SELECTION_ERROR"] = "選定的初始階段屬於其他管道";
