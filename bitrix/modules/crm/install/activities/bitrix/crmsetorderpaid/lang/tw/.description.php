<?php
$MESS["CRM_SOP_DESCR_DESCR"] = "將所有訂單付款標記為已完成";
$MESS["CRM_SOP_DESCR_DESCR_1"] = "在當前訂單中更改所有付款的狀態。";
$MESS["CRM_SOP_DESCR_NAME"] = "付款訂單";
