<?php
$MESS["CRM_CMPR_DESC"] = "複製/移動產品項目";
$MESS["CRM_CMPR_DESC_1"] = "複製或將產品從當前項目移至指定項目。";
$MESS["CRM_CMPR_DESC_2"] = "複製或將產品項目從當前項目移至指定項目。";
$MESS["CRM_CMPR_NAME"] = "複製/移動產品";
$MESS["CRM_CMPR_NAME_1"] = "複製或移動產品";
$MESS["CRM_CMPR_NAME_2"] = "複製或移動產品項目";
