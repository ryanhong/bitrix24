<?php
$MESS["CRM_AUTOMATION_DEMO_DEAL_1_1_MESSAGE"] = "新交易＃url_1＃{= document：title}＃url_2＃創建

請檢查您是否有針對客戶和訂單的所有必要信息。添加所有丟失的數據，計劃活動和設置此交易的任務。

不要忘記將交易轉移到下一階段。";
$MESS["CRM_AUTOMATION_DEMO_DEAL_1_2_MESSAGE"] = "交易＃url_1＃{= document：title}＃url_2＃仍處於原始階段。

如果需要，請檢查是否進行此交易並將其移至下一階段。";
$MESS["CRM_AUTOMATION_DEMO_DEAL_1_NOTIFY_TITLE"] = "通知";
