<?php
$MESS["CRM_AUTOMATION_DEMO_LEAD_1_1_MESSAGE_MSGVER_1"] = "創建了新的引線：＃url_1＃{= document：title}＃url_2＃。

注意鉛的起源。有關電話，請收聽錄音，填寫所需的表格和計劃活動。有關電子郵件，請閱讀該消息並繪製答复。對於其他來源，請根據需要處理數據。最初的處理完成後，將引線移至下一階段或將其轉換為交易。";
$MESS["CRM_AUTOMATION_DEMO_LEAD_1_2_MESSAGE_MSGVER_1"] = "＃url_1＃線索＃url_2＃已經處於初始階段兩天。如果您不盡快處理客戶，您可能會失去客戶。";
$MESS["CRM_AUTOMATION_DEMO_LEAD_1_3_MESSAGE_MSGVER_1"] = "潛在的挫折。
引線＃url_1＃{= document：title}＃url_2＃已經在舞台上{= document：status_id}持續了三天。
建議負責人將領導者轉移到下一階段或將其轉換。

負責人：{= document：apanded_by_printable}";
$MESS["CRM_AUTOMATION_DEMO_LEAD_1_CONTROL_TITLE"] = "控制";
$MESS["CRM_AUTOMATION_DEMO_LEAD_1_NOTIFY_TITLE"] = "通知";
