<?php
$MESS["CRM_AUTOMATION_DEMO_LEAD_2_CONTROL_TITLE"] = "控制";
$MESS["CRM_AUTOMATION_DEMO_LEAD_2_MESSAGE_1_MSGVER_1"] = "創建新的引線：{= document：id} {= document：title}
處理新線索。填寫表格併計劃您的活動。將線索移至下一階段，或將其轉換為交易。";
$MESS["CRM_AUTOMATION_DEMO_LEAD_2_MESSAGE_2"] = "恭喜！您現在正在研究引線{= document：id} {= document：title}

請記住：您的目標是轉換鉛。通過完成計劃的活動，將領先者向前推進渠道。";
$MESS["CRM_AUTOMATION_DEMO_LEAD_2_MESSAGE_3_MSGVER_1"] = "潛在的挫折。
引線＃url_1＃{= document：title}＃url_2＃已經在舞台上{= document：status_id}持續了三天。
已建議負責人開始處理領導，但沒有採取任何行動。

負責人：{= document：apanded_by_printable}";
$MESS["CRM_AUTOMATION_DEMO_LEAD_2_NOTIFY_TITLE"] = "通知";
