<?php
$MESS["CRM_AUTOMATION_DEMO_ORDER_1_ALLOW_DELIVERY_TITLE"] = "允許交貨";
$MESS["CRM_AUTOMATION_DEMO_ORDER_1_EMAIL_TITLE"] = "發信息";
$MESS["CRM_AUTOMATION_DEMO_ORDER_1_FOOTER"] = "最好的問候，<br>＃A1＃在線商店＃A2＃";
$MESS["CRM_AUTOMATION_DEMO_ORDER_1_MAIL_1_BODY"] = "<p style = \“保證 -  30px; margin-bottom：28px; font-jight：bold; font; font; font-size：19px; \'>
親愛的{= document：contact.name} {= document：contact.second_name} {= document：contact.last_name}，
</p>
<p style = \“保證金 -  top：0; margin-top：20px; line-height：20px; \'>
您的訂單＃{= document：account_number}來自{= document：document：date_insert}已確認。<br>
<br>
訂單總計：{= document：price_formatted}。<br>
<br>
您可以在{= document：shop_title}的帳戶中跟踪訂單進度。<br>
<br>
請注意，您需要在{= document：shop_title}上註冊的登錄和密碼。<br>
<br>
如果您想取消訂單，則可以在您的個人帳戶中進行。<br>
<br>
每當您聯繫{= document：shop_title}管理時，請確保您提及您的訂單＃{= coundment：councel_number}。<br>
<br>
謝謝您的訂單！<br>
</p>";
$MESS["CRM_AUTOMATION_DEMO_ORDER_1_MAIL_1_TITLE"] = "您已經下訂單{= document：shop_title}";
$MESS["CRM_AUTOMATION_DEMO_ORDER_1_MAIL_2_BODY"] = "<p style = \“保證 -  30px; margin-bottom：28px; font-jight：bold; font; font; font-size：19px; \'>
親愛的{= document：contact.name} {= document：contact.second_name} {= document：contact.last_name}，
</p>
<p style = \“保證金 -  top：0; margin-top：20px; line-height：20px; \'>
您在{= document：document_insert}上放置了一個訂單＃{= coundut_number} for {= document：document：price_formatted}。<br>
<br>
不幸的是您還沒有付錢。<br>
<br>
您可以在{= document：shop_title}的帳戶中跟踪訂單進度。<br>
<br>
請注意，您需要在{= document：shop_title}上註冊的登錄和密碼。<br>
<br>
如果您想取消訂單，則可以在您的個人帳戶中進行。<br>
<br>
每當您聯繫{= document：shop_title}管理時，請確保您提及您的訂單＃{= coundment：councel_number}。<br>
<br>
謝謝您的訂單！<br>
</p>";
$MESS["CRM_AUTOMATION_DEMO_ORDER_1_MAIL_2_TITLE"] = "不要忘記用{= document：shop_title}支付訂單";
$MESS["CRM_AUTOMATION_DEMO_ORDER_1_MAIL_3_BODY"] = "<p style = \“保證 -  30px; margin-bottom：28px; font-jight：bold; font; font; font-size：19px; \'>
{= document：document：document_insert}的訂單＃{= counduct_number}已付款。
</p>
<p style = \“保證金 -  top：0; margin-top：20px; line-height：20px; \'>
使用此鏈接以獲取更多信息：{= document：shop_public_url}
</p>";
$MESS["CRM_AUTOMATION_DEMO_ORDER_1_MAIL_3_TITLE"] = "您使用{= document：shop_title}的訂單付款";
$MESS["CRM_AUTOMATION_DEMO_ORDER_1_MAIL_4_BODY"] = "<p style = \“保證 -  30px; margin-bottom：28px; font-jight：bold; font; font; font-size：19px; \'>
{= document：document：date_insert}的訂單＃{= coundut_number}已取消。
</p>
<p style = \“保證金 -  top：0; margin-top：20px; line-height：20px; \'>
{= document：conical_canceled} <br>
<br>
使用此鏈接以獲取更多信息：{= document：shop_public_url}
</p>";
$MESS["CRM_AUTOMATION_DEMO_ORDER_1_MAIL_4_TITLE"] = "{= document：shop_title}：取消訂單＃{= document：account_number}";
$MESS["CRM_AUTOMATION_DEMO_ORDER_1_NEW_ORDER_PAYMENT_TITLE"] = "{= document：shop_title}：訂單付款提醒＃{= document：account_number}";
$MESS["CRM_AUTOMATION_DEMO_ORDER_1_NEW_ORDER_TITLE"] = "{= document：shop_title}：新訂單＃{= document：account_number}";
$MESS["CRM_AUTOMATION_DEMO_ORDER_1_ORDER_CANCELED_TITLE"] = "{= document：shop_title}：取消訂單＃{= document：account_number}";
$MESS["CRM_AUTOMATION_DEMO_ORDER_1_ORDER_PAYED_TITLE"] = "{= document：shop_title}：訂單＃{= document：account_number}已付款。";
