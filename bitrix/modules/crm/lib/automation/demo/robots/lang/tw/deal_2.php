<?php
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_1_MESSAGE"] = "創建的新交易：{= document：id} {= document：title}

閱讀有關新交易的事實：訂購的項目；最後期限;預定的開始日期和其他重要信息。

相應地計劃您的活動，以使交易取得成功。";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_2_MESSAGE"] = "恭喜！您現在正在處理交易{= document：id} {= document：title}

請記住：您的目標是使交易取得成功。通過完成計劃的活動並更新交易階段，將交易推進了整個渠道。";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_3_MESSAGE"] = "注意力！交易{= document：id} {= document：title}已經進入階段{= document：stage_id_printable}已經三天了

負責人已得到相應的通知，但是沒有採取任何行動。

負責人：{= document：apanded_by_printable}";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_4_MESSAGE"] = "交易{= document：id} {= document：title} {= document：opporter_account}失敗了。

負責人：{= document：apanded_by_printable}";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_5_MESSAGE"] = "恭喜！您正在努力交易{= document：id} {= document：title}

請記住：您的目標是使交易取得成功。通過完成計劃的活動並更新交易階段，將交易推進了整個渠道。";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_6_MESSAGE"] = "請記住，您必須向前推進這筆交易，以使其成功。

交易{= document：id} {= document：title}已經進入階段{= document：stage_id_printable}已經兩天了。您最好完成此階段並繼續前進。";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_7_MESSAGE"] = "注意力！交易{= document：id} {= document：title}已經進入階段{= document：stage_id_printable}已經三天了

負責人已得到相應的通知，但是沒有採取任何行動。

負責人：{= document：apanded_by_printable}";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_8_MESSAGE"] = "恭喜！您正在努力工作{= document：id} {= document：title}。

在此階段，您需要選擇一種可能的方案：

如果交易需要提前付款，請創建並將發票發送給客戶。

如果不需要事先付款，請將交易轉移到下一階段。";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_9_MESSAGE"] = "提醒客戶應支付發票以將交易{= document：id} {= document：title}進一步。

如果已經支付發票，請將其標記為付款，並將交易推向下一階段。";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_10_MESSAGE"] = "注意力！交易{= document：id} {= document：title}已經進入階段{= document：stage_id_printable}已經五天了

負責人已得到相應的通知，但是沒有採取任何行動。

負責人：{= document：apanded_by_printable}";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_11_MESSAGE"] = "恭喜！您正在努力交易{= document：id} {= document：title}

請記住：您的目標是使交易取得成功。通過完成計劃的活動並更新交易階段，將交易推進了整個渠道。";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_12_MESSAGE"] = "注意力！交易{= document：id} {= document：title}已經進入階段四天{= document：stage_id_printable}

負責人已得到相應的通知，但是沒有採取任何行動。

負責人：{= document：apanded_by_printable}";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_13_MESSAGE"] = "恭喜！您正在努力交易{= document：id} {= document：title}

現在，您必須收到付款才能完成交易。

創建並將發票發送給客戶。";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_14_MESSAGE"] = "提醒客戶為交易支付發票{= document：id} {= document：title}。

如果已經支付發票，請標記為已付款並完成交易。";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_15_MESSAGE"] = "恭喜！您正在努力交易{= document：id} {= document：title}

所有進行的活動，發票支付了？將交易轉移到最後階段，以標記成功。";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_CALL_SUBJECT"] = "提醒發票";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_CALL_TITLE"] = "安排電話";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_CONTROL_TITLE"] = "控制";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_NOTIFY_TITLE"] = "通知";
