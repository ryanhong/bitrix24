<?php
$MESS["SEO_DIR_LOGICAL_NO_NAME"] = "<untitled>";
$MESS["SEO_ICON_ALT"] = "SEO優化";
$MESS["SEO_ICON_HINT"] = "運行SEO工具以將您的網站移動到搜索引擎結果中";
$MESS["SEO_ICON_TEXT"] = "SEO";
$MESS["SEO_UNIQUE_TEXT_YANDEX"] = "向Yandex提交獨特的文字";
$MESS["SEO_UNIQUE_TEXT_YANDEX_SUBMIT"] = "提交Yandex";
$MESS["SEO_YANDEX_DOMAIN"] = "領域";
$MESS["SEO_YANDEX_ERROR"] = "未指定網站關係。 <a href= \"/bitrix/admin/seo_search_yandex.php?lang=ru \">現在設置</a>";
$MESS["SEO_YANDEX_ORIGINAL_TEXT_OK"] = "行動成功了";
$MESS["SEO_YANDEX_ORIGINAL_TEXT_OK_DETAILS"] = "添加了原始文本。 <a href= \"/bitrix/admin/seo_search_yandex.php?lang=#language_id# \ \">管理原始文本</a>";
$MESS["SEO_YANDEX_ORIGINAL_TEXT_TOO_LONG"] = "文字太長了。您的文本最多必須是＃num＃字符或更短的字符。";
$MESS["SEO_YANDEX_ORIGINAL_TEXT_TOO_SHORT"] = "文字太短。您的文本必須是＃num＃字符或更長的字符。";
$MESS["YANDEX_ERROR__HOST_NOT_VERIFIED"] = "用戶站點列表中缺少該網站或未確認權限";
$MESS["YANDEX_ERROR__INVALID_USER_ID"] = "令牌發行者用戶ID與請求中指定的用戶ID不同。";
$MESS["YANDEX_ERROR__ORIGINALS_TEXT_ALREADY_ADDED"] = "提交的文字已經添加";
$MESS["YANDEX_ERROR__ORIGINAL_TEXT_ALREADY_ADDED"] = "提交的文字已經添加";
$MESS["YANDEX_ERROR__QUOTA_EXCEEDED"] = "文本配額超出了";
$MESS["YANDEX_ERROR__TEXT_LENGTH_CONSTRAINTS_VIOLATION"] = "提交的文本太短或太長。";
