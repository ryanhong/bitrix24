<?php
$MESS["SEO_MENU_ADV_AUTOLOG"] = "AutoContext日誌";
$MESS["SEO_MENU_ADV_AUTOLOG_ALT"] = "廣告機器人日誌";
$MESS["SEO_MENU_ADV_ENGINES"] = "Autocontext";
$MESS["SEO_MENU_ADV_ENGINES_ALT"] = "廣告平台集成";
$MESS["SEO_MENU_GOOGLE"] = "Google";
$MESS["SEO_MENU_MAIN"] = "搜索引擎優化";
$MESS["SEO_MENU_MAIN_TITLE"] = "網站SEO工具";
$MESS["SEO_MENU_ROBOTS"] = "robots.txt";
$MESS["SEO_MENU_ROBOTS_ALT"] = "robots.txt的參數";
$MESS["SEO_MENU_SEARCH_ENGINES"] = "搜尋引擎";
$MESS["SEO_MENU_SEARCH_ENGINES_ALT"] = "搜索引擎特定工具";
$MESS["SEO_MENU_SITEMAP"] = "sitemap.xml";
$MESS["SEO_MENU_SITEMAP_ALT"] = "SiteMap.xml的參數";
$MESS["SEO_MENU_YANDEX"] = "yandex";
$MESS["SEO_MENU_YANDEX_DIRECT"] = "yandex.Direct";
