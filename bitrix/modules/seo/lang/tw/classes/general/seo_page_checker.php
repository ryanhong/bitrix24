<?php
$MESS["SEO_ERROR_NO_SEARCH"] = "搜索模塊缺少或未安裝。關鍵字統計信息不可用。";
$MESS["SEO_ERROR_NO_SERVER_NAME"] = "網站URL丟失了！請檢查<a href= \"/bitrix/admin/site_edit.php?lid=#site_id# \ \">當前站點設置</a>或<a href = \ \ \“/bitrix/admin/settings.php？ MID = MAIN \“>內核設置</a>。";
$MESS["SEO_H1_ABSENT_ERROR"] = "該頁面不包含＆lt; h1＆gt;標題。";
$MESS["SEO_H1_UNIQUE_ERROR"] = "該頁麵包含多個＆lt; h1＆gt;標題（＃count＃）。找到標題：＃值＃。";
$MESS["SEO_IMG_NO_ALT_ERROR"] = "該頁麵包含＃count＃＃圖像，沒有alt/title屬性。";
$MESS["SEO_LINKS_COUNT_ERROR"] = "該頁麵包含太多鏈接：＃count＃（＃count_external＃是外部的）。不建議在頁面中擁有超過＃預選賽＃鏈接。";
$MESS["SEO_META_NO_DESCRIPTION_ERROR"] = "元/描述標籤是空的或缺少的。";
$MESS["SEO_META_NO_KEYWORDS_ERROR"] = "元/關鍵字標籤是空的或缺少的。";
$MESS["SEO_PCRE_ERROR"] = "PCRE.BACKTRACK_LIMIT（＃PCRE_BACKTRACK_LIMIT＃）的值比文本長度（＃text_len＃）的值更少，並且無法使用ini_set更改。該形式會產生不正確的結果。";
