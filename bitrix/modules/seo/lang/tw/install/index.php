<?php
$MESS["SEO_DENIED"] = "拒絕訪問";
$MESS["SEO_FULL"] = "完全訪問";
$MESS["SEO_INSTALL_TITLE"] = "SEO模塊安裝";
$MESS["SEO_MODULE_DESCRIPTION"] = "為您的網站提供各種SEO工具。";
$MESS["SEO_MODULE_NAME"] = "搜索引擎優化";
$MESS["SEO_OPENED"] = "使用權";
$MESS["SEO_UNINSTALL_TITLE"] = "SEO模塊去除";
