<?php
$MESS["SEO_RETARGETING_SERVICE_RESPONSE_GOOGLE_CANT_ADD_AUDIENCE"] = "無法創建受眾，因為您的帳戶當前不符合使用客戶列表的要求。 <a href='#link#'target='_blank'>詳細信息</a>。";
$MESS["SEO_RETARGETING_SERVICE_RESPONSE_GOOGLE_ERROR"] = "Google返回錯誤：＃錯誤＃";
$MESS["SEO_RETARGETING_SERVICE_RESPONSE_GOOGLE_NAME_ALREADY_USED"] = "這個名字的觀眾已經存在";
