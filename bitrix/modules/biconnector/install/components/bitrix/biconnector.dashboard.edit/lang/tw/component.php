<?php
$MESS["CC_BBDE_ERROR_EMPTY_FIELD"] = "必需字段是空的";
$MESS["CC_BBDE_ERROR_EMPTY_NAME"] = "需要字段\“名稱\”。";
$MESS["CC_BBDE_ERROR_EMPTY_URL"] = "需要字段\“嵌入鏈接\”。";
$MESS["CC_BBDE_ERROR_INCLUDE_MODULE"] = "未安裝雙方驅動器模塊。";
$MESS["CC_BBDE_ERROR_INVALID_URL"] = "不正確的嵌入鏈接格式。";
$MESS["CC_BBDE_ERROR_LIMIT_EXCEEDED"] = "超過數據傳輸限製到外部BI系統。";
$MESS["CC_BBDE_TITLE_ADD"] = "添加報告";
$MESS["CC_BBDE_TITLE_EDIT"] = "編輯報告";
$MESS["CC_BBDE_TITLE_NEW"] = "新報告";
