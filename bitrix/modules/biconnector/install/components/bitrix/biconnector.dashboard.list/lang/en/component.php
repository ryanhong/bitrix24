<?php
$MESS["CC_BBDL_ACTION_MENU_DELETE"] = "Delete";
$MESS["CC_BBDL_ACTION_MENU_DELETE_CONF"] = "Are you sure you want to delete report link? This action cannot be undone.";
$MESS["CC_BBDL_ACTION_MENU_EDIT"] = "Edit";
$MESS["CC_BBDL_ACTION_MENU_VIEW"] = "View";
$MESS["CC_BBDL_ERROR_INCLUDE_MODULE"] = "The biconnector module is not installed.";
$MESS["CC_BBDL_TITLE"] = "Manage reports";
