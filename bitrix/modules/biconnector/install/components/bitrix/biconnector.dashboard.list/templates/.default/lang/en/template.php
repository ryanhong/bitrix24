<?php
$MESS["CC_BBDL_ACTION_MENU_DELETE_CONF"] = "Are you sure you want to delete the report link? This action cannot be undone.";
$MESS["CC_BBDL_ONBOARDING_DESCRIPTION"] = "Add BI system reports to Bitrix24 and grant user access.";
$MESS["CC_BBDL_ONBOARDING_TITLE"] = "Keep reports handy ";
$MESS["CT_BBDL_COLUMN_CREATED_BY"] = "Administrator";
$MESS["CT_BBDL_COLUMN_DATE_CREATE"] = "Created on";
$MESS["CT_BBDL_COLUMN_ID"] = "ID";
$MESS["CT_BBDL_COLUMN_NAME"] = "Name";
$MESS["CT_BBDL_COLUMN_URL"] = "BI system link";
$MESS["CT_BBDL_TITLE"] = "My BI reports";
$MESS["CT_BBDL_TOOLBAR_ADD"] = "Add report";
