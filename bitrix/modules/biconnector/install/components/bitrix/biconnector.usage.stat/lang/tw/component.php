<?php
$MESS["CC_BBUS_EMPTYSTATE_DESCRIPTION"] = "獲取有關導出線的數據源和數字<br>的詳細信息，並管理Overlimits。";
$MESS["CC_BBUS_EMPTYSTATE_TITLE"] = "此屏幕將顯示<br> BI連接器的用法統計信息";
$MESS["CC_BBUS_ERROR_INCLUDE_MODULE"] = "未安裝雙方驅動器模塊。";
$MESS["CC_BBUS_SERVICE_DATALENS"] = "yandex datalens";
$MESS["CC_BBUS_SERVICE_GDS"] = "Looker Studio";
$MESS["CC_BBUS_SERVICE_PBI"] = "Microsoft Power BI";
$MESS["CC_BBUS_SHOW_MORE"] = "更多＃n＃";
$MESS["CC_BBUS_TITLE"] = "用法統計";
