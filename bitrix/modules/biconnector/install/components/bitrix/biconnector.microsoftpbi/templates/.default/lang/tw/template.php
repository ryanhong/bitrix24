<?php
$MESS["CONNECT_DATA_KEY"] = "您的秘密密鑰：";
$MESS["CONNECT_DATA_SERVER"] = "您的服務器地址：";
$MESS["CONNECT_DATA_TITLE"] = "您的連接信息";
$MESS["DESCRIPTION_REPORT_TEMPLATES"] = "創建令人印象深刻的互動報告並與您的同事分享。使用Microsoft Power BI將您的BITRIX24數據作為儀表板和圖表顯示。";
$MESS["LINK_MANUAL_REPORT_TEMPLATES"] = "連接說明";
$MESS["TITLE_REPORT_TEMPLATES"] = "連接<span> Microsoft Power BI </span>";
