<?php
$MESS["BTN_TEMPLATES"] = "連接Google Looker Studio";
$MESS["DESCRIPTION_REPORT_TEMPLATES"] = "創建令人印象深刻的互動報告並與您的同事分享。使用Google Looker Studio將您的Bitrix24數據顯示為儀表板和圖表。";
$MESS["LINK_MANUAL_REPORT_TEMPLATES"] = "連接說明";
$MESS["TITLE_REPORT_TEMPLATES"] = "連接<span> Google Looker Studio </span>";
