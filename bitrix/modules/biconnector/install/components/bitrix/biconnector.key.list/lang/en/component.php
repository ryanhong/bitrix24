<?php
$MESS["CC_BBKL_ACTION_MENU_DELETE"] = "Delete";
$MESS["CC_BBKL_ACTION_MENU_DELETE_CONF"] = "Are you sure you want to delete the key? This action cannot be undone.";
$MESS["CC_BBKL_ACTION_MENU_EDIT"] = "Edit";
$MESS["CC_BBKL_ERROR_INCLUDE_MODULE"] = "The biconnector module is not installed.";
$MESS["CC_BBKL_TITLE"] = "Manage keys";
$MESS["CT_BBKL_KEY_COPIED"] = "Key copied";
$MESS["CT_BBKL_KEY_COPY"] = "Copy";
$MESS["CT_BBKL_KEY_HIDE"] = "Hide";
$MESS["CT_BBKL_KEY_SHOW"] = "Show";
