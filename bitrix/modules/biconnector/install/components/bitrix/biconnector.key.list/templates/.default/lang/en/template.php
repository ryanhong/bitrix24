<?php
$MESS["CC_BBKL_ACTION_MENU_DELETE_CONF"] = "Are you sure you want to delete this key? This action cannot be undone.";
$MESS["CC_BBKL_ACTIVATE_KEY_ERROR"] = "Error updating the activity. Please try again later.";
$MESS["CC_BBKL_KEY_COPIED"] = "The key has been copied successfully.";
$MESS["CC_BBKL_KEY_COPY_ERROR"] = "Error copying the key.";
$MESS["CC_BBKL_KEY_ONBOARDING_DESCRIPTION"] = "Use keys to conveniently manage data sent to your BI systems from Bitrix24.";
$MESS["CC_BBKL_KEY_ONBOARDING_TITLE"] = "Manage report details";
$MESS["CT_BBKL_COLUMN_ACCESS_KEY"] = "Key";
$MESS["CT_BBKL_COLUMN_ACTIVE"] = "Active";
$MESS["CT_BBKL_COLUMN_APPLICATION"] = "Application";
$MESS["CT_BBKL_COLUMN_CONNECTION"] = "Connection";
$MESS["CT_BBKL_COLUMN_CREATED_BY"] = "Administrator";
$MESS["CT_BBKL_COLUMN_DATE_CREATE"] = "Created on";
$MESS["CT_BBKL_COLUMN_ID"] = "ID";
$MESS["CT_BBKL_COLUMN_LAST_ACTIVITY_DATE"] = "Last active on";
$MESS["CT_BBKL_TITLE"] = "My keys";
$MESS["CT_BBKL_TOOLBAR_ADD"] = "Add key";
