<?php
$MESS["SUPERSET_DASHBOARD_DETAIL_ERROR_DESC"] = "無法創建報告。<br>請重試。";
$MESS["SUPERSET_DASHBOARD_DETAIL_ERROR_RELOAD_BTN"] = "創造";
$MESS["SUPERSET_DASHBOARD_DETAIL_HEADER_EDIT"] = "編輯";
$MESS["SUPERSET_DASHBOARD_DETAIL_HINT_DESC"] = "可能還要等一下。您可以關閉窗格並重新完成工作。創建儀表板後，您會在此處找到儀表板。";
$MESS["SUPERSET_DASHBOARD_DETAIL_HINT_LINK"] = "了解更多";
$MESS["SUPERSET_DASHBOARD_DETAIL_HINT_TITLE"] = "加載模板...";
