<?php
$MESS["CT_BBKE_ACCESS_KEY"] = "鑰匙";
$MESS["CT_BBKE_ACTIVE"] = "積極的";
$MESS["CT_BBKE_CONNECTION"] = "聯繫";
$MESS["CT_BBKE_KEY_COPIED"] = "鑰匙複製";
$MESS["CT_BBKE_KEY_COPY"] = "複製";
$MESS["CT_BBKE_KEY_HIDE"] = "隱藏";
$MESS["CT_BBKE_KEY_SHOW"] = "展示";
$MESS["CT_BBKE_ONBOARDING"] = "複製並輸入您要與BitRix24一起使用的BI系統中的密鑰";
$MESS["CT_BBKE_ONBOARDING_MORE"] = "了解更多";
$MESS["CT_BBKE_USERS"] = "用戶";
