<?php
$MESS["CC_BBKE_ERROR_INCLUDE_MODULE"] = "未安裝雙方驅動器模塊。";
$MESS["CC_BBKE_ERROR_LIMIT_EXCEEDED"] = "超過數據傳輸限製到外部BI系統。";
$MESS["CC_BBKE_ERROR_NO_CONNECTION"] = "找不到BI Analytics兼容連接。";
$MESS["CC_BBKE_TITLE_CREATE"] = "創建一個密鑰";
$MESS["CC_BBKE_TITLE_EDIT"] = "編輯密鑰";
$MESS["CC_BBKE_TITLE_NEW"] = "新密鑰";
