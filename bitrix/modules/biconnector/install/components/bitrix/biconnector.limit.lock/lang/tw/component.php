<?php
$MESS["CC_BLL_CONTENT_BLOCKED"] = "<p>您要求報告的記錄數量超過限制。 BI Analytics不能將＃限制＃數據行發送到您當前計劃的外部系統。</p>
<p>請升級到獲得精確分析報告的主要計劃之一。</p>
<p> <b>截至＃short_date＃，數據不再發送到BI系統。您無法訪問先前創建的報告。</b> </p>
態";
$MESS["CC_BLL_CONTENT_BLOCKED_BOX"] = "<p>您的許可已過期。您必須續訂執照才能繼續使用BI Analytics。</p>
<p>截至＃short_date＃（您的許可證到期日）數據不再從BITRIX24轉到外部BI系統。您無法訪問現有報告。</b> </p>
<p> <a href= \"#about_limits_href# \“target= \"blank \">詳細信息</a> </p>";
$MESS["CC_BLL_CONTENT_WARNING"] = "
<p>您要求報告的記錄數量超過限制。 BI Analytics不能將＃限制＃數據行發送到您當前計劃的外部系統。</p>
<p>請升級到獲得精確分析報告的主要計劃之一。</p>
<p> <b>注意！從＃short_date＃開始，數據將不再發送到BI系統，對先前創建的報告的訪問將被關閉。</b> </p>
態
";
$MESS["CC_BLL_CONTENT_WARNING_BOX"] = "<p>您的許可將很快到期。您必須續訂執照才能繼續使用BI Analytics。</p>
<p>截至＃short_date＃（您的許可證到期日）數據將不再從BITRIX24轉到外部BI系統。您將無法訪問現有報告。</b> </p>
<p> <a href= \"#about_limits_href# \“target= \"blank \">詳細信息</a> </p>";
$MESS["CC_BLL_LATER_BUTTON"] = "我稍後再決定";
$MESS["CC_BLL_LICENSE_BUTTON"] = "選擇一個計劃";
$MESS["CC_BLL_LICENSE_BUTTON_BOX"] = "續訂許可證";
$MESS["CC_BLL_TITLE"] = "BI分析限制";
