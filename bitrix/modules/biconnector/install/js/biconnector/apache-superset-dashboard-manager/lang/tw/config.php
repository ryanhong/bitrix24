<?php
$MESS["SUPERSET_DASHBOARD_DETAIL_COPY_ERROR"] = "錯誤複製儀表板。請再試一次。";
$MESS["SUPERSET_DASHBOARD_DETAIL_GET_CREDENTIALS_ERROR"] = "錯誤獲取身份驗證數據。請再試一次。";
$MESS["SUPERSET_DASHBOARD_DETAIL_LOGIN_POPUP_AUTH_BTN"] = "登入";
$MESS["SUPERSET_DASHBOARD_DETAIL_LOGIN_POPUP_CANCEL_BTN"] = "取消";
$MESS["SUPERSET_DASHBOARD_DETAIL_LOGIN_POPUP_CONTINUE_BTN"] = "繼續";
$MESS["SUPERSET_DASHBOARD_DETAIL_LOGIN_POPUP_COPY_MARKET_DASHBOARD_ATTENTION"] = "市場儀表板無法修改。將創建該儀表板的副本，您可以根據需要進行編輯。";
$MESS["SUPERSET_DASHBOARD_DETAIL_LOGIN_POPUP_COPY_SYSTEM_DASHBOARD_ATTENTION"] = "內置儀表板無法修改。將創建該儀表板的副本，您可以根據需要進行編輯。";
$MESS["SUPERSET_DASHBOARD_DETAIL_LOGIN_POPUP_DATA_LOGIN"] = "登入";
$MESS["SUPERSET_DASHBOARD_DETAIL_LOGIN_POPUP_DATA_PASSWORD"] = "密碼";
$MESS["SUPERSET_DASHBOARD_DETAIL_LOGIN_POPUP_DATA_TITLE"] = "您的身份驗證數據";
$MESS["SUPERSET_DASHBOARD_DETAIL_LOGIN_POPUP_DESCRIPTION"] = "登錄到BI Builder編輯儀表板";
$MESS["SUPERSET_DASHBOARD_DETAIL_LOGIN_POPUP_TITLE"] = "編輯儀表板";
