<?php
$MESS["BICONNECTOR_CONNECTION_NOTE"] = "
<p>在文件bitrix/.settings.php。</p>中找到與此模塊兼容的數據庫連接
<p>將使用默認連接可能會影響性能。<p>
<p>您必須編輯文件bitrix/.settings.php才能達到最佳性能。</p>
<ul>
<li>在“連接”鍵下，將“默認”鍵複製到新密鑰（例如：“ Biconnector”）。
<li>將“ className”鍵的值設置為“ \ bitrix \ biconnecter \ db \ mysqliconnnection”。
<li>如果需要，請包括一個文件，以在“ include_after_connected”中提供其他配置。
</ul>
<p>您的結果看起來像這樣：</p>
<pre>
  '連接'=>
  大批 （
    'value'=>
    大批 （
      “默認” =>
      大批 （
        'className'=>'\ bitrix \ main \ db \ mysqliconnection'，
        '主機'=>'localhost'，
        '數據庫'=>'sitemanager'，
        '登錄'=>'用戶'，
        '密碼'=>'密碼'，
        '選項'=> 2，
        'include_after_connected'=> \ $ _ server ['document_root']。 '/bitrix/'。 'php_interface/after_connect.php'，
      ），
      'Biconnector'=>
      大批 （
        'className'=>'\ bitrix \ biconnnector \ db \ mysqliconnection'，
        '主機'=>'localhost'，
        '數據庫'=>'sitemanager'，
        '登錄'=>'用戶'，
        '密碼'=>'密碼'，
        '選項'=> 2，
        'include_after_connected'=> \ $ _ server ['document_root']。 '/bitrix/'。 'php_interface/after_connect_bi.php'，
      ），
    ），
    'readonly'=> true，
  ），
</pre>
<p>這是其他配置文件After_connect_bi.php的樣子看起來像：</p>
<pre>
\ $ this-> queryExecute（\“設置名稱'utf8'\”）;
\ $ this-> queryExecute（\“ set sql_mode =''\”）;
</pre>
<p>請注意使用“ \ $ this”來微調數據庫連接。</p>
<p>編輯此文件時，請注意所有註意力。即使是一個小錯誤也可能使您的網站完全沒有反應。<p>
<p>我們建議您使用SSH控制台或SFTP編輯此文件。<p>";
$MESS["BICONNECTOR_INSTALL"] = "BI連接器模塊安裝";
