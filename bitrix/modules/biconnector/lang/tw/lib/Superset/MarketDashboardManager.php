<?php
$MESS["BI_CONNECTOR_SUPERSET_DELETE_ERROR_APP_NOT_FOUND"] = "此應用不存在。";
$MESS["BI_CONNECTOR_SUPERSET_DELETE_ERROR_DATASETS"] = "無法刪除BI Builder數據源。";
$MESS["BI_CONNECTOR_SUPERSET_DELETE_ERROR_DATASET_IMPORT"] = "錯誤的數據源應用程序ID";
$MESS["BI_CONNECTOR_SUPERSET_DELETE_ERROR_HAS_COPIES"] = "刪除此儀表板之前，請刪除此儀表板的所有副本。";
$MESS["BI_CONNECTOR_SUPERSET_DELETE_ERROR_SYSTEM_DASHBOARD"] = "無法刪除內置儀表板。";
