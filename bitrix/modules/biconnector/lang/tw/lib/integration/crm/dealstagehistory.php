<?php
$MESS["CRM_BIC_DSH_FIELD_ASSIGNED_BY"] = "負責人";
$MESS["CRM_BIC_DSH_FIELD_ASSIGNED_BY_DEPARTMENT"] = "負責人的部門";
$MESS["CRM_BIC_DSH_FIELD_ASSIGNED_BY_DEPARTMENT_FULL"] = "公司結構的路徑";
$MESS["CRM_BIC_DSH_FIELD_ASSIGNED_BY_FULL"] = "被分配為交易的用戶的ID和名稱";
$MESS["CRM_BIC_DSH_FIELD_ASSIGNED_BY_ID"] = "負責人ID";
$MESS["CRM_BIC_DSH_FIELD_ASSIGNED_BY_ID_FULL"] = "被分配為負責交易的用戶的ID";
$MESS["CRM_BIC_DSH_FIELD_ASSIGNED_BY_NAME"] = "負責人";
$MESS["CRM_BIC_DSH_FIELD_DATE_CREATE"] = "創建於";
$MESS["CRM_BIC_DSH_FIELD_DEAL_ID"] = "交易ID";
$MESS["CRM_BIC_DSH_FIELD_END_DATE"] = "結束日期";
$MESS["CRM_BIC_DSH_FIELD_ID"] = "唯一的鑰匙";
$MESS["CRM_BIC_DSH_FIELD_STAGE"] = "階段";
$MESS["CRM_BIC_DSH_FIELD_STAGE_ID"] = "舞台ID";
$MESS["CRM_BIC_DSH_FIELD_STAGE_NAME"] = "藝名";
$MESS["CRM_BIC_DSH_FIELD_STAGE_SEMANTIC"] = "舞台類型";
$MESS["CRM_BIC_DSH_FIELD_STAGE_SEMANTIC_ID"] = "階段類型ID";
$MESS["CRM_BIC_DSH_FIELD_START_DATE"] = "開始日期";
$MESS["CRM_BIC_DSH_FIELD_TYPE_ID"] = "記錄類型";
$MESS["CRM_BIC_DSH_TABLE"] = "交易：舞台歷史";
