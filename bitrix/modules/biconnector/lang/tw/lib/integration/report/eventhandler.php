<?php
$MESS["BIC_CRM_BUTTON_ORDER"] = "實施請求";
$MESS["BIC_CRM_MENU_CONNECT"] = "聯繫";
$MESS["BIC_CRM_MENU_DASHBOARD_MANAGE"] = "管理報告";
$MESS["BIC_CRM_MENU_GOOGLE"] = "Google Looker Studio";
$MESS["BIC_CRM_MENU_ITEM"] = "BI分析";
$MESS["BIC_CRM_MENU_ITEM_SETTINGS"] = "BI分析設置";
$MESS["BIC_CRM_MENU_KEY_MANAGE"] = "管理密鑰";
$MESS["BIC_CRM_MENU_MICROSOFT"] = "Microsoft Power BI";
$MESS["BIC_CRM_MENU_REPORT_TEMPLATES"] = "現成的報告模板";
$MESS["BIC_CRM_MENU_USAGE_STAT"] = "用法";
$MESS["BIC_CRM_MENU_YANDEX"] = "yandex datalens";
