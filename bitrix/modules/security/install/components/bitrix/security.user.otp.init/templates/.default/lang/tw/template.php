<?php
$MESS["SECURITY_OTP_APP_EXECUTE"] = "運行應用程序";
$MESS["SECURITY_OTP_APP_EXECUTE2"] = "然後單擊<strong>配置</strong>按鈕";
$MESS["SECURITY_OTP_APP_EXECUTE_TMP"] = "然後單擊圖標添加新帳戶";
$MESS["SECURITY_OTP_CHOOSE_TYPE"] = "選擇首選方法以接收驗證代碼";
$MESS["SECURITY_OTP_CODE_DESCR"] = "一旦代碼成功掃描或手動輸入，您的手機將顯示您必須在下面輸入的代碼。";
$MESS["SECURITY_OTP_CODE_DESCR2"] = "要完成初始化，請在移動應用程序中單擊\“獲取新代碼\”，然後輸入您在移動屏幕上看到的另一個代碼。";
$MESS["SECURITY_OTP_CODE_INFO_HOTP"] = "基於計數器";
$MESS["SECURITY_OTP_CODE_INFO_TOTP"] = "基於時間";
$MESS["SECURITY_OTP_CONNECT"] = "立即啟用兩步身份驗證";
$MESS["SECURITY_OTP_DESCR"] = "該軟件受數據加密技術的保護，每個用戶都有一對登錄和密碼。但是，惡意人員可以使用一些工具來進入您的計算機並竊取這些數據。
<br /> <br />
我們強烈建議您遷移到兩步身份驗證策略。<br /> <br />
兩步身份驗證意味著您必須通過兩個級別
登錄時驗證。首先，您將輸入密碼。然後，你必須
輸入發送到您的移動設備的一次性安全代碼。 <br /> <br />
這將使您的業務數據更加安全。";
$MESS["SECURITY_OTP_DONE"] = "完畢";
$MESS["SECURITY_OTP_ENTER_CODE"] = "輸入驗證碼";
$MESS["SECURITY_OTP_ENTER_CODE_PL"] = "輸入代碼";
$MESS["SECURITY_OTP_ENTER_CODE_PL1"] = "第一個代碼";
$MESS["SECURITY_OTP_ENTER_CODE_PL2"] = "第二個代碼";
$MESS["SECURITY_OTP_ERROR_TITLE"] = "由於發生錯誤而無法保存。";
$MESS["SECURITY_OTP_HAND_DESCR"] = "如果您無法掃描代碼，請手動輸入它。
<br />您必須指定網站（或Bitrix24）地址，您的電子郵件，驗證字，然後選擇鍵類型。";
$MESS["SECURITY_OTP_HAND_TYPE"] = "手動輸入代碼";
$MESS["SECURITY_OTP_MOBILE"] = "下載Bitrix24 OTP移動應用";
$MESS["SECURITY_OTP_MOBILE2"] = "為您的手機或GooglePlay的手機";
$MESS["SECURITY_OTP_MOBILE2_TMP"] = "從AppStore或GooglePlay的電話中";
$MESS["SECURITY_OTP_MOBILE_TMP"] = "下載FreeOTP或Google Authenticator移動應用程序";
$MESS["SECURITY_OTP_SCAN_CODE"] = "掃描二維碼";
$MESS["SECURITY_OTP_SCAN_DESCR"] = "要掃描代碼，請將手機的相機帶到屏幕上，然後等到應用程序掃描代碼。";
$MESS["SECURITY_OTP_UNKNOWN_ERROR"] = "意外的錯誤。請稍後再試。";
