<?php
$MESS["SEC_SESSION_ADMIN_DB_BUTTON_OFF"] = "不要將會話數據存儲在安全模塊數據庫中";
$MESS["SEC_SESSION_ADMIN_DB_BUTTON_ON"] = "將會話數據存儲在安全模塊數據庫中";
$MESS["SEC_SESSION_ADMIN_DB_NOTE"] = "<p>大多數Web攻擊都會竊取授權的用戶會話數據。啟用<b>會話保護</b>使會話劫持毫無意義。</p>
<p>除了可以在用戶組首選項中設置的標準會話保護選項外，<b>主動會話保護</b>：
<ul style ='字體大小：100％'>
<li>定期更改會話ID，並且可以設置頻率; </li>
<li>將會話數據存儲在模塊表中。</li>
</ul>
<p>將會話數據存儲在模塊數據庫中可以通過在其他虛擬服務器上運行腳本來防止數據被盜，從而消除了虛擬託管配置錯誤，不良的臨時文件夾權限設置和其他與操作系統相關的問題。它還通過將操作卸載到數據庫服務器來減少文件系統壓力。</p>
<p> <i>推薦用於高級。</i> </p>";
$MESS["SEC_SESSION_ADMIN_DB_NOTE_V2"] = "
<p>將會話存儲在數據庫，redis或memcache而不是文件中，而不是通過在其他虛擬服務器上託管的腳本訪問此數據，有助於避免服務器配置和訪問權限許可沖突和其他環境配置問題。此外，它通過將請求卸載到數據庫服務器，redis或memcache來減少文件系統加載。</p>

<p>要更改會話存儲參數，您將必須編輯文件<b> .settings.php </b>如所述<a href ='https：//training.bitrix24.com/support/training/course/ index .php？course_id = 68＆contract_id = 05962＆threns_path = 5936.5959.5962'>在這裡</a>。</p>

<p>除了在組設置中指定的常規會話安全措施外，您還可以設置會話ID每隔幾分鐘更改。</p>

<p> <i>推薦用於高安全級別。</i> </p>";
$MESS["SEC_SESSION_ADMIN_DB_OFF"] = "會話數據未存儲在安全模塊數據庫中。";
$MESS["SEC_SESSION_ADMIN_DB_ON"] = "會話數據存儲在安全模塊數據庫中。";
$MESS["SEC_SESSION_ADMIN_DB_WARNING"] = "注意力！打開或關閉會話模式將導致當前授權的用戶丟失授權（會話數據將被銷毀）。";
$MESS["SEC_SESSION_ADMIN_SAVEDB_TAB"] = "數據庫中的會話";
$MESS["SEC_SESSION_ADMIN_SAVEDB_TAB_TITLE_V2"] = "用戶會話存儲設置";
$MESS["SEC_SESSION_ADMIN_SAVEDB_TAB_V2"] = "會話存儲設置";
$MESS["SEC_SESSION_ADMIN_SESSID_BUTTON_OFF"] = "禁用ID更改";
$MESS["SEC_SESSION_ADMIN_SESSID_BUTTON_ON"] = "啟用ID更改";
$MESS["SEC_SESSION_ADMIN_SESSID_NOTE"] = "<p>如果啟用了此功能，則會話ID將在指定的時間段之後更改。這增加了服務器的負載，但顯然可以使ID劫持，而無需瞬間使用絕對毫無意義。</p>
<p> <i>推薦用於高級。</i> </p>";
$MESS["SEC_SESSION_ADMIN_SESSID_OFF"] = "會話ID更改被禁用。";
$MESS["SEC_SESSION_ADMIN_SESSID_ON"] = "會話ID更改已啟用。";
$MESS["SEC_SESSION_ADMIN_SESSID_TAB"] = "ID更改";
$MESS["SEC_SESSION_ADMIN_SESSID_TAB_TITLE"] = "配置定期更改會話ID";
$MESS["SEC_SESSION_ADMIN_SESSID_TTL"] = "會話ID壽命，秒。";
$MESS["SEC_SESSION_ADMIN_SESSID_WARNING"] = "會話ID與主動保護模塊不兼容。帶有session_id（）函數返回的標識符不得超過32個字符，並且僅包含拉丁字母或數字。";
$MESS["SEC_SESSION_ADMIN_STORAGE_IN_FILES"] = "會話數據存儲在文件中。";
$MESS["SEC_SESSION_ADMIN_STORAGE_NAME_TYPE_DATABASE"] = "資料庫";
$MESS["SEC_SESSION_ADMIN_STORAGE_NAME_TYPE_FILE"] = "文件";
$MESS["SEC_SESSION_ADMIN_STORAGE_NAME_TYPE_MEMCACHE"] = "memcache";
$MESS["SEC_SESSION_ADMIN_STORAGE_NAME_TYPE_REDIS"] = "Redis";
$MESS["SEC_SESSION_ADMIN_STORAGE_WITH_SESSION_DATA"] = "會話數據存儲在＃名稱＃中。";
$MESS["SEC_SESSION_ADMIN_TITLE"] = "會話保護";
