<?php
$MESS["SEC_OTP_RECOVERY_CREATED"] = "在：＃日期＃上創建";
$MESS["SEC_OTP_RECOVERY_ISSUER"] = "發行者：＃發行人＃";
$MESS["SEC_OTP_RECOVERY_LOGIN"] = "登錄：＃登錄＃";
$MESS["SEC_OTP_RECOVERY_NOTE"] = "代碼只能使用一次。提示：罷工二手代碼在列表中。";
$MESS["SEC_OTP_RECOVERY_TITLE"] = "恢復驗證代碼";
