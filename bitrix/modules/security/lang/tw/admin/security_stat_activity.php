<?php
$MESS["SEC_STATACT_503_TEMPLATE"] = "要顯示給禁止訪問者的頁面模板";
$MESS["SEC_STATACT_BUTTON_OFF"] = "禁用活動控制";
$MESS["SEC_STATACT_BUTTON_ON"] = "啟用活動控制";
$MESS["SEC_STATACT_DEFENCE_DELAY"] = "禁止";
$MESS["SEC_STATACT_DEFENCE_DELAY_MEAS"] = "（秒）";
$MESS["SEC_STATACT_DEFENCE_LOG"] = "將條目添加到事件日誌：";
$MESS["SEC_STATACT_DEFENCE_MAX_HITS"] = "客戶重新結束";
$MESS["SEC_STATACT_DEFENCE_MAX_HITS_MEAS"] = "命中";
$MESS["SEC_STATACT_DEFENCE_STACK_TIME"] = "如果期間";
$MESS["SEC_STATACT_DEFENCE_STACK_TIME_MEAS"] = "（秒）";
$MESS["SEC_STATACT_GRABBER_EDIT_503_TEMPLATE_LINK"] = "編輯模板";
$MESS["SEC_STATACT_MAIN_TAB"] = "活動控制";
$MESS["SEC_STATACT_MAIN_TAB_TITLE"] = "啟用或禁用活動控制";
$MESS["SEC_STATACT_NOTE"] = "<p>活動控制允許保護系統免受大量活躍用戶的保護，引人注目的機器人，一些DDOS攻擊並防止密碼蠻力嘗試。</p>
<p>您可以為您的網站設置最大允許的活動（例如，用戶可以執行的每秒請求數）。</p>
<p> <i>推薦用於正常水平。</i> </p>";
$MESS["SEC_STATACT_OFF"] = "活動控制被禁用。";
$MESS["SEC_STATACT_ON"] = "啟用了活動控制。";
$MESS["SEC_STATACT_PARAMS_TAB"] = "參數";
$MESS["SEC_STATACT_PARAMS_TAB_TITLE"] = "編輯活動控制參數";
$MESS["SEC_STATACT_TITLE"] = "活動控制";
