<?php
$MESS["SEC_FILTER_ACTION"] = "對入侵的反應";
$MESS["SEC_FILTER_ACTION_CLEAR"] = "刪除危險數據";
$MESS["SEC_FILTER_ACTION_FILTER"] = "使數據安全";
$MESS["SEC_FILTER_ACTION_NONE"] = "跳過危險數據";
$MESS["SEC_FILTER_ACTION_NOTE_1"] = "傳入數據將被修改。例如：“選擇”將用“ sel sel” ect; quord; lt; - 帶有“＆lt; sc＆nbsp; ript; gt;＆quot。";
$MESS["SEC_FILTER_ACTION_NOTE_2"] = "不過濾潛在的攻擊使任何惡意人都無法使您的網站無法使用。";
$MESS["SEC_FILTER_ADD"] = "添加";
$MESS["SEC_FILTER_BUTTON_OFF"] = "禁用主動保護";
$MESS["SEC_FILTER_BUTTON_ON"] = "啟用主動保護";
$MESS["SEC_FILTER_DURATION"] = "添加到（最小）的停止列表";
$MESS["SEC_FILTER_EXCEPTIONS_TAB"] = "例外";
$MESS["SEC_FILTER_EXCEPTIONS_TAB_TITLE"] = "為URL指定不會被拒絕的口罩";
$MESS["SEC_FILTER_EXCL_FOUND"] = "存在過濾器排除。";
$MESS["SEC_FILTER_LOG"] = "添加入侵嘗試記錄";
$MESS["SEC_FILTER_MAIN_TAB"] = "主動過濾器";
$MESS["SEC_FILTER_MAIN_TAB_TITLE"] = "啟用或禁用主動過濾器";
$MESS["SEC_FILTER_MASKS"] = "異常：<br>（例如：/bitrix/*或*/news/*）";
$MESS["SEC_FILTER_NOTE"] = "<p> <b>主動過濾器</b>保護站點免受最著名的Web攻擊。該過濾器認識到傳入請求中的危險威脅並阻止入侵。</p>
<p>主動過濾器是防止Web項目實施中可能的安全缺陷的最有效方法（XSS，SQL注入，PHP等）。</p>
<p>您將始終使用SiteUpdate Technology獲取主動過濾器的最新更新。</p>
<p>請注意，用戶執行的某些無害動作可能被認為可疑並導致過濾器做出反應。</p>
<p> <i>推薦用於標準級別。</i> </p>";
$MESS["SEC_FILTER_OFF"] = "主動保護被禁用。";
$MESS["SEC_FILTER_ON"] = "啟用了主動保護。";
$MESS["SEC_FILTER_PARAMETERS_TAB"] = "主動反應";
$MESS["SEC_FILTER_PARAMETERS_TAB_TITLE"] = "配置系統對入侵的反應";
$MESS["SEC_FILTER_SITE"] = "對於網站：";
$MESS["SEC_FILTER_STOP"] = "將攻擊者的IP地址添加到停止列表";
$MESS["SEC_FILTER_TITLE"] = "主動過濾器";
