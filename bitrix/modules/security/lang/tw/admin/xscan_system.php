<?php
$MESS["BITRIX_XSCAN_SYSTEM"] = "系統狀況";
$MESS["BITRIX_XSCAN_SYSTEM_INFO"] = "
<div class = \“ adm-info-message-wrap adm-info-message-red \”>
    <div class = \“ adm-info-message \”>
        <div class = \“ adm-info-message-title \”>除了掃描漏洞，我們建議您：</div>
        <ul>
            <li>檢查服務器上運行的可疑過程。</li>
            <li>在crontab上檢查未經授權的任務。</li>
            <li>檢查未知的SSH鍵。</li>
        </ul>
        <div class = \“ adm-info-message-icon \”> </div>
    </div>
</div>
";
