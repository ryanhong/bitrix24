<?php
$MESS["SEC_REDIRECT_ACTIONS"] = "網絡釣魚保護行動：";
$MESS["SEC_REDIRECT_ACTIONS_HEADER"] = "動作";
$MESS["SEC_REDIRECT_ACTION_REDIRECT"] = "重定向到指定的URL。";
$MESS["SEC_REDIRECT_ACTION_REDIRECT_URL"] = "URL：";
$MESS["SEC_REDIRECT_ADD"] = "添加";
$MESS["SEC_REDIRECT_BUTTON_OFF"] = "禁用重定向防止網絡釣魚攻擊";
$MESS["SEC_REDIRECT_BUTTON_ON"] = "啟用重定向防止網絡釣魚攻擊";
$MESS["SEC_REDIRECT_HREF_SIGN"] = "在下面的URL中添加數字簽名：";
$MESS["SEC_REDIRECT_LOG"] = "將網絡釣魚嘗試添加到日誌中";
$MESS["SEC_REDIRECT_MAIN_TAB"] = "重定向保護";
$MESS["SEC_REDIRECT_MAIN_TAB_TITLE"] = "實現重定向防止網絡釣魚攻擊。";
$MESS["SEC_REDIRECT_MESSAGE"] = "訊息";
$MESS["SEC_REDIRECT_METHODS"] = "網絡釣魚保護方法：";
$MESS["SEC_REDIRECT_METHODS_HEADER"] = "方法";
$MESS["SEC_REDIRECT_NOTE"] = "<p> <a href= \"http://en.wikipedia.org/wiki/phishish/phishing \“target= \"_blank \">網絡釣魚</a>  - 是試圖獲取敏感信息的犯罪欺詐過程作為用戶名，密碼和信用卡的詳細信息，通過將電子通信中的一個值得信賴的實體偽裝成詳細信息。 </p>

<p>存在兩種防止重定向網絡釣魚的方法：</p>
<ul style = \“ font-size：100％\”>
<li>通過篩選HTTP標題中缺少引用頁面來檢測惡意重定向。</li>
<li>標誌與數字簽名鏈接並在重定向嘗試時對其進行驗證。</li>
</ul>
<p>以下可以用作保護：
<ul style = \“ font-size：100％\”>
<li>向訪客顯示重定向警告。</li>
<li>無條件地將訪客重定向到已知安全的網站。</li>
</ul>
<p> <i>推薦用於高安全級別</i> </p>";
$MESS["SEC_REDIRECT_OFF"] = "重定向防止網絡釣魚攻擊的保護是禁用的";
$MESS["SEC_REDIRECT_ON"] = "重定向防止網絡釣魚攻擊啟用";
$MESS["SEC_REDIRECT_PARAMETERS_TAB"] = "參數";
$MESS["SEC_REDIRECT_PARAMETERS_TAB_TITLE"] = "重定向保護參數設置";
$MESS["SEC_REDIRECT_PARAMETER_NAME"] = "參數名稱：";
$MESS["SEC_REDIRECT_REFERER_CHECK"] = "檢查描述參考頁面的HTTP標頭的存在。";
$MESS["SEC_REDIRECT_REFERER_SITE_CHECK"] = "\“轉介\” HTTP標頭必須包含當前站點的域名。";
$MESS["SEC_REDIRECT_SHOW_MESSAGE_AND_STAY"] = "顯示重定向到另一個URL通知消息。";
$MESS["SEC_REDIRECT_SYSTEM"] = "系統";
$MESS["SEC_REDIRECT_TITLE"] = "重定向防止網絡釣魚攻擊";
$MESS["SEC_REDIRECT_URL"] = "URL：";
$MESS["SEC_REDIRECT_URLS"] = "簽名的URL";
$MESS["SEC_REDIRECT_USER"] = "用戶";
