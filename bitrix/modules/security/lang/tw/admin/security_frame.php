<?php
$MESS["SEC_FRAME_ADD"] = "添加";
$MESS["SEC_FRAME_BUTTON_OFF"] = "禁用反框架保護";
$MESS["SEC_FRAME_BUTTON_ON"] = "啟用反框架保護";
$MESS["SEC_FRAME_EXCEPTIONS_TAB"] = "例外";
$MESS["SEC_FRAME_EXCEPTIONS_TAB_TITLE"] = "保護不會應用於與過濾器匹配的頁面。";
$MESS["SEC_FRAME_EXCL_FOUND"] = "存在排除。";
$MESS["SEC_FRAME_HTML_CACHE"] = "框架使用限制現在可以在復合模式下或啟用HTML頁面緩存時工作";
$MESS["SEC_FRAME_MAIN_TAB"] = "反框架保護";
$MESS["SEC_FRAME_MAIN_TAB_TITLE"] = "反框架保護";
$MESS["SEC_FRAME_MASKS"] = "排除面具：<br>（例如：/bitrix/*或*/news/*）";
$MESS["SEC_FRAME_NOTE"] = "<p>通過在框架中禁用或限製網站頁面的顯示，保護您的網站和用戶免受UI的糾正，點擊夾克和框架的範圍。這還將大大降低跨站點腳本攻擊的風險。</p>";
$MESS["SEC_FRAME_OFF"] = "反框架保護是禁用的";
$MESS["SEC_FRAME_ON"] = "啟用了反框架保護";
$MESS["SEC_FRAME_SITE"] = "對於網站：";
$MESS["SEC_FRAME_TITLE"] = "反框架保護";
