<?php
$MESS["SEC_IPRULE_ADMIN_ADD"] = "添加";
$MESS["SEC_IPRULE_ADMIN_BUTTON_OFF"] = "禁用保護";
$MESS["SEC_IPRULE_ADMIN_BUTTON_ON"] = "啟用保護";
$MESS["SEC_IPRULE_ADMIN_EXCL_FILES_ACTIVE"] = "以下路徑在異常列表中提供外部服務兼容性：";
$MESS["SEC_IPRULE_ADMIN_EXCL_FILES_INACTIVE"] = "以下路徑將添加到異常列表中，以提供外部服務兼容性：";
$MESS["SEC_IPRULE_ADMIN_EXCL_IPS"] = "IP地址和IP地址範圍可以訪問後端";
$MESS["SEC_IPRULE_ADMIN_EXCL_IPS_SAMPLE"] = "示例：192.168.0.7; 192.168.0.1-192.168.0.100";
$MESS["SEC_IPRULE_ADMIN_MAIN_TAB"] = "控制面板保護";
$MESS["SEC_IPRULE_ADMIN_MAIN_TAB_TITLE"] = "拒絕除指定的IP地址以外的任何對控制面板的訪問";
$MESS["SEC_IPRULE_ADMIN_NOTE"] = "您的IP地址已被認為是＃IP＃。如果是這樣，請將其複制並粘貼到下面的輸入字段中。";
$MESS["SEC_IPRULE_ADMIN_NO_IP"] = "未指定IP地址或IP地址範圍。";
$MESS["SEC_IPRULE_ADMIN_OFF"] = "保護禁用";
$MESS["SEC_IPRULE_ADMIN_ON"] = "啟用保護";
$MESS["SEC_IPRULE_ADMIN_RULE_NAME"] = "控制面板自動保護規則";
$MESS["SEC_IPRULE_ADMIN_SAVE_ERROR"] = "保存規則時發生錯誤。";
$MESS["SEC_IPRULE_ADMIN_TITLE"] = "控制面板保護";
