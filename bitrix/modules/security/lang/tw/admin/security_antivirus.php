<?php
$MESS["SEC_ANTIVIRUS_ACTION"] = "病毒檢測到作用";
$MESS["SEC_ANTIVIRUS_ACTION_NOTIFY_ONLY"] = "記錄日誌並通知管理員";
$MESS["SEC_ANTIVIRUS_ACTION_REPLACE"] = "從站點代碼剪切對象";
$MESS["SEC_ANTIVIRUS_ADD"] = "添加";
$MESS["SEC_ANTIVIRUS_BUTTON_OFF"] = "關閉網絡防病毒軟件";
$MESS["SEC_ANTIVIRUS_BUTTON_ON"] = "激活Web防病毒軟件";
$MESS["SEC_ANTIVIRUS_LEVEL"] = "建議對最高安全性進行激活。";
$MESS["SEC_ANTIVIRUS_MAIN_TAB"] = "Web防病毒軟件";
$MESS["SEC_ANTIVIRUS_MAIN_TAB_TITLE"] = "激活Web防病毒軟件。";
$MESS["SEC_ANTIVIRUS_NOTE"] = "<p> <b> Web防病毒</b> </p>
<p> <a href= \"http://www.bitrixsoft.com/products/cms/features/proactive.php \"> web antivirus </a>  - 一種抵消感染的系統。 Web防病毒軟件識別潛在危險的HTML代碼，\ \“ cuts \”站點代碼中的可疑對象，阻止了用戶計算機的感染。</p>
<p>注意！網絡防病毒軟件不是替代傳統防病毒計劃。</p>";
$MESS["SEC_ANTIVIRUS_OFF"] = "網絡防病毒停用";
$MESS["SEC_ANTIVIRUS_ON"] = "Web防病毒激活";
$MESS["SEC_ANTIVIRUS_PARAMETERS_TAB"] = "參數";
$MESS["SEC_ANTIVIRUS_PARAMETERS_TAB_TITLE"] = "病毒感染通知參數。";
$MESS["SEC_ANTIVIRUS_PREBODY_NOTFOUND"] = "要檢測在輸出緩沖之前輸入的病毒，請在php.ini中分配以下參數：
<br> auto_prepend_file =＃路徑＃<br>或.htaccess：<br> php_value auto_prepend_file \“＃路徑＃\”";
$MESS["SEC_ANTIVIRUS_PREBODY_NOTFOUND_CGI"] = "要檢測在輸出緩沖之前輸入的病毒，請在php.ini中分配以下參數：
<br> auto_prepend_file =＃路徑＃";
$MESS["SEC_ANTIVIRUS_TIMEOUT"] = "通知間隔（分鐘）";
$MESS["SEC_ANTIVIRUS_TITLE"] = "Web防病毒軟件";
$MESS["SEC_ANTIVIRUS_WARNING"] = "添加了例外或僅通知制度。";
$MESS["SEC_ANTIVIRUS_WHITE_LIST"] = "例外：";
$MESS["SEC_ANTIVIRUS_WHITE_LIST_SET_TAB"] = "例外（列出）";
$MESS["SEC_ANTIVIRUS_WHITE_LIST_TAB"] = "例外";
$MESS["SEC_ANTIVIRUS_WHITE_LIST_TAB_TITLE"] = "過濾將不會在包含以下代碼摘錄的HTML塊上執行。";
