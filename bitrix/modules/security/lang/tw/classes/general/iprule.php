<?php
$MESS["SECURITY_IPRULE_ERROR_SELF_BLOCK"] = "您的IP（＃IP＃）與範圍匹配。如果您確定自己是正確的，請再次保存表單。";
$MESS["SECURITY_IPRULE_ERROR_SELF_BLOCK_2"] = "您的IP（＃IP＃）與範圍匹配。";
$MESS["SECURITY_IPRULE_ERROR_WONG_IP"] = "錯誤的IP地址（＃IP＃）。";
$MESS["SECURITY_IPRULE_ERROR_WONG_IP_RANGE"] = "最後一個IP地址（＃END_IP＃）小於範圍內的第一個IP地址（＃start_ip＃）。";
$MESS["SECURITY_IPRULE_IPCHECK_DISABLE_FILE_WARNING"] = "發現IP塊禁用標誌文件。將其刪除以啟用IP封鎖。";
