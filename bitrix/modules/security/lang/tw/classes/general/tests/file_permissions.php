<?php
$MESS["SECURITY_SITE_CHECKER_FILE_PERM_ADDITIONAL"] = "最後＃計數＃文件/目錄：";
$MESS["SECURITY_SITE_CHECKER_FILE_PERM_DETAIL"] = "授予所有系統用戶的完整寫入許可將完全損害您的項目，因為您的代碼可能會由第三方更改。";
$MESS["SECURITY_SITE_CHECKER_FILE_PERM_RECOMMENDATION"] = "刪除冗餘權限。";
$MESS["SECURITY_SITE_CHECKER_FILE_PERM_SMALL_MAX_EXEC"] = "PHP腳本執行時間（max_execution_time）太短。建議值30秒或更多。";
$MESS["SECURITY_SITE_CHECKER_FILE_PERM_TIMEOUT"] = "抱歉，文件系統性能太低。";
$MESS["SECURITY_SITE_CHECKER_FILE_PERM_TITLE"] = "至少有＃count＃文件或目錄，並在當前環境中為任何人提供完整的寫入權限（用戶對Bitrix框架的用戶）";
$MESS["SECURITY_SITE_CHECKER_FilePermissionsTest_NAME"] = "文件可訪問性檢查";
