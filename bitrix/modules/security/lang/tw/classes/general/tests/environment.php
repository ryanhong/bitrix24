<?php
$MESS["SECURITY_SITE_CHECKER_BITRIX_TMP_DIR"] = "臨時文件保存在項目根目錄中";
$MESS["SECURITY_SITE_CHECKER_BITRIX_TMP_DIR_ADDITIONAL"] = "當前目錄：＃dir＃";
$MESS["SECURITY_SITE_CHECKER_BITRIX_TMP_DIR_DETAIL"] = "不建議將CTEMPFILE創建的臨時文件保存到根文件夾。";
$MESS["SECURITY_SITE_CHECKER_BITRIX_TMP_DIR_RECOMMENDATION"] = "定義常數\“ bx_temporary_files_directory \“ in \” bitrix/php_interface/dbconn.php \“並指定必要的路徑。<br>
按照以下步驟：<br>
1.為您的溫度目錄選擇一個名稱並創建它。例如，\“/home/bitrix/tmp/www \”：
<pre>
mkdir -p -m 700/home/bitrix/tmp/www
</pre>
2.定義常數，讓系統知道您要將臨時文件保存到該文件夾​​：
<pre>
define（\“ bx_temporary_files_directory \”，\“/home/bitrix/tmp/www \”）;
</pre>";
$MESS["SECURITY_SITE_CHECKER_COLLECTIVE_SESSION"] = "會話存儲目錄可能包含不同項目的會話。";
$MESS["SECURITY_SITE_CHECKER_COLLECTIVE_SESSION_ADDITIONAL_OWNER"] = "原因：文件所有者不是當前用戶<br>
文件：＃文件＃<br>
文件所有者uid：＃file_onwer＃<br>
當前用戶uid：＃current_owner＃<br>";
$MESS["SECURITY_SITE_CHECKER_COLLECTIVE_SESSION_ADDITIONAL_SIGN"] = "原因：會話文件未使用當前網站的簽名<br>簽名
文件：＃文件＃<br>
當前網站的簽名：＃標誌＃<br>
文件內容：<pre> #file_content＃</pre>";
$MESS["SECURITY_SITE_CHECKER_COLLECTIVE_SESSION_DETAIL"] = "這可能有助於使用其他虛擬服務器上的腳本讀取和編寫會話數據。";
$MESS["SECURITY_SITE_CHECKER_COLLECTIVE_SESSION_RECOMMENDATION"] = "更改數據庫中的目錄或存儲會話：<a href= \"/bitrix/admin/security_session.php \">會話保護</a>。";
$MESS["SECURITY_SITE_CHECKER_EnvironmentTest_NAME"] = "環境檢查";
$MESS["SECURITY_SITE_CHECKER_PHP_PRIVILEGED_USER"] = "PHP作為特權用戶運行";
$MESS["SECURITY_SITE_CHECKER_PHP_PRIVILEGED_USER_ADDITIONAL"] = "#uid＃/＃gid＃";
$MESS["SECURITY_SITE_CHECKER_PHP_PRIVILEGED_USER_DETAIL"] = "作為特權用戶（例如root）運行PHP可能會損害您項目的安全性";
$MESS["SECURITY_SITE_CHECKER_PHP_PRIVILEGED_USER_RECOMMENDATION"] = "以PHP為無私人用戶運行的方式配置服務器";
$MESS["SECURITY_SITE_CHECKER_SESSION_DIR"] = "所有系統用戶都可以訪問會話文件存儲目錄";
$MESS["SECURITY_SITE_CHECKER_SESSION_DIR_ADDITIONAL"] = "會話存儲目錄：＃dir＃<br>
許可：＃PERMS＃";
$MESS["SECURITY_SITE_CHECKER_SESSION_DIR_DETAIL"] = "此漏洞可用於讀取或更改其他虛擬服務器上運行腳本的會話數據。";
$MESS["SECURITY_SITE_CHECKER_SESSION_DIR_RECOMMENDATION"] = "正確配置訪問權限或更改目錄。另一個選項是將會話存儲在數據庫中：<a href= \"/bitrix/admin/security_session.php \">會話保護</a>。";
$MESS["SECURITY_SITE_CHECKER_UPLOAD_EXECUTABLE_PHP"] = "PHP腳本在上傳文件目錄中執行。";
$MESS["SECURITY_SITE_CHECKER_UPLOAD_EXECUTABLE_PHP_DETAIL"] = "有時，開發人員對適當的文件名過濾器的關注不足。攻擊者可能會利用這種脆弱性來完全控制您的項目。";
$MESS["SECURITY_SITE_CHECKER_UPLOAD_EXECUTABLE_PHP_DOUBLE"] = "具有雙擴展名（例如php.lala）的PHP腳本在上傳的文件目錄中執行。";
$MESS["SECURITY_SITE_CHECKER_UPLOAD_EXECUTABLE_PHP_DOUBLE_DETAIL"] = "有時，開發人員對適當的文件名過濾器的關注不足。攻擊者可能會利用這種脆弱性來完全控制您的項目。";
$MESS["SECURITY_SITE_CHECKER_UPLOAD_EXECUTABLE_PHP_DOUBLE_RECOMMENDATION"] = "正確配置您的Web服務器。";
$MESS["SECURITY_SITE_CHECKER_UPLOAD_EXECUTABLE_PHP_RECOMMENDATION"] = "正確配置您的Web服務器。";
$MESS["SECURITY_SITE_CHECKER_UPLOAD_EXECUTABLE_PY"] = "Python腳本在上傳的文件目錄中執行。";
$MESS["SECURITY_SITE_CHECKER_UPLOAD_EXECUTABLE_PY_DETAIL"] = "有時，開發人員對適當的文件名過濾器的關注不足。攻擊者可能會利用這種脆弱性來完全控制您的項目。";
$MESS["SECURITY_SITE_CHECKER_UPLOAD_EXECUTABLE_PY_RECOMMENDATION"] = "正確配置您的Web服務器。";
$MESS["SECURITY_SITE_CHECKER_UPLOAD_HTACCESS"] = "Apache不得處理上傳文件目錄中的.htaccess文件";
$MESS["SECURITY_SITE_CHECKER_UPLOAD_HTACCESS_DETAIL"] = "有時，開發人員對適當的文件名過濾器的關注不足。攻擊者可能會利用這種脆弱性來完全控制您的項目。";
$MESS["SECURITY_SITE_CHECKER_UPLOAD_HTACCESS_RECOMMENDATION"] = "正確配置您的Web服務器。";
$MESS["SECURITY_SITE_CHECKER_UPLOAD_NEGOTIATION"] = "Apache內容協商在文件上傳目錄中啟用。";
$MESS["SECURITY_SITE_CHECKER_UPLOAD_NEGOTIATION_DETAIL"] = "不建議進行Apache內容協商，因為它可能會引起XSS攻擊。";
$MESS["SECURITY_SITE_CHECKER_UPLOAD_NEGOTIATION_RECOMMENDATION"] = "正確配置您的Web服務器。";
