<?php
$MESS["VIRUS_DETECTED_DESC"] = "＃電子郵件＃-網站管理員的電子郵件地址（來自內核模塊設置）";
$MESS["VIRUS_DETECTED_MESSAGE"] = "來自＃site_name＃的信息消息

-------------------------------------------------- --------

由於＃server_name＃的主動保護系統檢測到了潛在的危險代碼，因此您已經收到了此消息。

1.潛在的危險代碼已從HTML刪除。
2.檢查事件日誌並確保代碼確實有害，而不僅僅是計數器或框架。
（鏈接：http：//#server_name#/bitrix/admin/admin/event_log.php？lang = en＆set_filter = y＆find_type = audit_type_ide_id＆find_audit_audit_type []
3.如果代碼不有害，請將其添加到“防病毒設置”頁面上的“異常”列表中。
（鏈接：http：//#server_name#/bitrix/admin/security_antivirus.php？lang = en＆tabcontrol_active_tab =例外）
4.如果代碼是病毒，請完成以下步驟：

a）將管理員和其他負責用戶的登錄密碼更改為網站。
b）更改SSH和FTP的登錄密碼。
c）從可以通過SSH或FTP訪問站點的管理員的計算機中測試並刪除病毒。
d）關閉通過SSH或FTP提供對網站訪問的程序中保存密碼。
e）從受感染的文件中刪除有害代碼。例如，使用最新備份重新安裝受感染的文件。

-------------------------------------------------- --------------------------- ----------------------- ----
此消息已自動生成。";
$MESS["VIRUS_DETECTED_NAME"] = "檢測到病毒";
$MESS["VIRUS_DETECTED_SUBJECT"] = "＃site_name＃：檢測到病毒";
