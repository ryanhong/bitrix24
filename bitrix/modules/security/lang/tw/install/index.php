<?php
$MESS["SEC_ADMIN"] = "完整的管理訪問";
$MESS["SEC_DENIED"] = "拒絕訪問";
$MESS["SEC_FILTER"] = "旁路主動過濾器";
$MESS["SEC_INSTALL_TITLE"] = "主動保護模塊安裝";
$MESS["SEC_MODULE_DESCRIPTION"] = "主動保護您的網站的模塊。";
$MESS["SEC_MODULE_NAME"] = "主動保護";
$MESS["SEC_PASSWORD"] = "管理一次性密碼";
$MESS["SEC_UNINSTALL_TITLE"] = "主動保護模塊卸載";
$MESS["SEC_VIEW"] = "查看所有參數";
