<?php
$MESS["SEC_OTP_CONNECTED"] = "連接的";
$MESS["SEC_OTP_CONNECT_DEVICE"] = "連接加密狗";
$MESS["SEC_OTP_CONNECT_DEVICE_TITLE"] = "連接加密狗";
$MESS["SEC_OTP_CONNECT_DONE"] = "準備好";
$MESS["SEC_OTP_CONNECT_MOBILE"] = "連接移動設備";
$MESS["SEC_OTP_CONNECT_MOBILE_ENTER_CODE"] = "輸入代碼";
$MESS["SEC_OTP_CONNECT_MOBILE_ENTER_NEXT_CODE"] = "輸入下一個代碼";
$MESS["SEC_OTP_CONNECT_MOBILE_INPUT_DESCRIPTION"] = "一旦代碼成功掃描或手動輸入，您的手機將顯示您必須在下面輸入的代碼。";
$MESS["SEC_OTP_CONNECT_MOBILE_INPUT_NEXT_DESCRIPTION"] = "OTP算法需要兩個用於身份驗證的代碼。請生成下一個代碼並在下面輸入。";
$MESS["SEC_OTP_CONNECT_MOBILE_MANUAL_INPUT"] = "要手動輸入數據，請指定網站地址，您的電子郵件或登錄，圖片上的秘密代碼，然後選擇密鑰類型。";
$MESS["SEC_OTP_CONNECT_MOBILE_MANUAL_INPUT_HOTP"] = "基於計數器";
$MESS["SEC_OTP_CONNECT_MOBILE_MANUAL_INPUT_TOTP"] = "基於時間";
$MESS["SEC_OTP_CONNECT_MOBILE_SCAN_QR"] = "將您的移動設備帶到監視器上，並在應用程序掃描代碼時等待。";
$MESS["SEC_OTP_CONNECT_MOBILE_STEP_1"] = "在<a href= \"https://itunes.apple.com/en/app/bitrix24-otp/id929604673?l= en \“target= \"_new \"> AppStore上，下載<a href= \" https://itunes.apple.com/en/app/bitrix24-opp/bitrix24-opp/bitrix24-opplecom/en/bitrix24-opplecom/en/bitrix24-opple.com/en/bitrix24-opple com.com/en /bitrix24-opplecom.com/en/bitrix24-enew \"> AppStore上，下載Bitrix OTP移動應用程序</a> on <a href= \"https://play.google.com/store/apps/details ?id=com.bitrixsoft.otp \" target= \"_new \ \"> googleplay </a>";
$MESS["SEC_OTP_CONNECT_MOBILE_STEP_2"] = "運行應用程序，然後單擊<b>配置</b>";
$MESS["SEC_OTP_CONNECT_MOBILE_STEP_3"] = "選擇要輸入數據的方式：使用QR碼或手動";
$MESS["SEC_OTP_CONNECT_MOBILE_TITLE"] = "連接移動設備";
$MESS["SEC_OTP_CONNECT_NEW_DEVICE"] = "連接新加密狗";
$MESS["SEC_OTP_CONNECT_NEW_MOBILE"] = "連接新的移動設備";
$MESS["SEC_OTP_DEACTIVATE_UNTIL"] = "禁用直到＃日期＃";
$MESS["SEC_OTP_DESCRIPTION_ABOUT"] = "一次性密碼（OTP）是作為宣誓計劃的一部分而開發的。<br>
OTP基於HMAC和SHA-1/SHA-256/SHA-512。目前，支持兩種算法生成代碼：
<ul> <li>基於計數器（基於HMAC的一次性密碼，HOTP），如<a href = \'https：//tools.ietf.org/html/rfc4226 \'https：//tools.ietf.org . > RFC4226 </a> </li>
<li>基於時間的（基於時間的一次性密碼，TOTP），如<a href= \"https://tools.ietf.org/html/rfc6238 \“target= \”_blank \"> rfc6238 < / a> </li> </ul>
為了計算OTP值，算法採用兩個輸入參數：一個秘密密鑰（初始值）和當前的計數器值（根據算法的不同，生成周期的數量或當前時間）。初始化設備後，將初始值保存在設備以及網站上。如果使用HOTP，則每個OTP生成的設備計數器會增加，而服務器計數器則在每個成功的OTP身份驗證上更改。如果使用TOTP，則沒有保存在設備中，並且服務器跟踪設備在每個成功的OTP身份驗證上的可能更改。<br>
批處理中的每個OTP設備都包含一個包含批處理中每個設備的初始值（秘密鍵）的加密文件，該文件綁定到設備上可以在設備上找到的設備序列號。<br>
如果設備和服務器計數器從同步中生長出來，則可以通過將服務器值帶入設備的值來輕鬆同步。為此，管理員（或具有適當許可的用戶）必須生成兩個連續的OTP並在網站上輸入。<br>
您可以在AppStore和GooglePlay上找到移動應用程序。";
$MESS["SEC_OTP_DESCRIPTION_ABOUT_TITLE"] = "描述";
$MESS["SEC_OTP_DESCRIPTION_ACTIVATION"] = "可以使用特殊設備（加密狗）或每個用戶需要安裝在其移動設備上的免費移動應用程序（Bitrix OTP）獲得一次兩步身份驗證的一次性代碼。<br>
要啟用加密狗，管理員將必須打開用戶的配置文件，並輸入由。<br>生成的兩個密碼
要在移動設備上獲取一次性代碼，用戶可以下載並運行該應用程序，並在其用戶配置文件中的設置頁面上掃描QR碼，或者手動輸入帳戶數據。";
$MESS["SEC_OTP_DESCRIPTION_ACTIVATION_TITLE"] = "啟用設定";
$MESS["SEC_OTP_DESCRIPTION_INTRO_INTRANET"] = "今天，用戶正在使用一對登錄和密碼在您的Bitrix24上進行身份驗證。但是，有一個惡意人的工具
可以僱用進入計算機並竊取這些數據，例如，如果用戶保存密碼。<br>
<b>兩步身份驗證</b>是保護您的Bitrix24免受黑客軟件的推薦選項。每次用戶登錄到系統時，他們都必須通過兩個級別的驗證。首先，輸入登錄和密碼。然後，輸入發送到其移動設備的一次性安全代碼。最重要的是攻擊者無法使用被盜數據，因為他們不知道安全碼。";
$MESS["SEC_OTP_DESCRIPTION_INTRO_SITE"] = "今天，用戶正在使用一對登錄和密碼在您的網站上進行身份驗證。但是，有一個惡意人的工具
可以僱用進入計算機並竊取這些數據，例如，如果用戶保存密碼。<br>
<b>兩步身份驗證</b>是防止黑客軟件的推薦選項。每次用戶登錄到系統時，他們都必須通過兩個級別的驗證。首先，輸入登錄和密碼。然後，輸入發送到其移動設備的一次性安全代碼。最重要的是攻擊者無法使用被盜數據，因為他們不知道安全碼。";
$MESS["SEC_OTP_DESCRIPTION_INTRO_TITLE"] = "一次性密碼";
$MESS["SEC_OTP_DESCRIPTION_USING"] = "啟用兩步身份驗證後，用戶在登錄時必須通過兩個級別的驗證。 <br>
首先，像往常一樣輸入其電子郵件和密碼。 <br>
然後，輸入發送到其移動設備或使用專用加密狗獲得的一次性安全代碼。";
$MESS["SEC_OTP_DESCRIPTION_USING_STEP_0"] = "步驟1";
$MESS["SEC_OTP_DESCRIPTION_USING_STEP_1"] = "第2步";
$MESS["SEC_OTP_DESCRIPTION_USING_TITLE"] = "使用一次密碼";
$MESS["SEC_OTP_DISABLE"] = "禁用";
$MESS["SEC_OTP_ENABLE"] = "使能夠";
$MESS["SEC_OTP_ERROR_TITLE"] = "由於發生錯誤而無法保存。";
$MESS["SEC_OTP_INIT"] = "初始化";
$MESS["SEC_OTP_MANDATORY_ALMOST_EXPIRED"] = "用戶必須設置兩步身份驗證的時間將在＃日期＃到期。";
$MESS["SEC_OTP_MANDATORY_DEFFER"] = "延長";
$MESS["SEC_OTP_MANDATORY_DISABLED"] = "強制性的兩步身份驗證禁用。";
$MESS["SEC_OTP_MANDATORY_ENABLE"] = "需要在內部激活兩步身份驗證";
$MESS["SEC_OTP_MANDATORY_ENABLE_DEFAULT"] = "需要激活兩步身份驗證";
$MESS["SEC_OTP_MANDATORY_EXPIRED"] = "用戶必須設置兩步身份驗證的時間現已到期。";
$MESS["SEC_OTP_MOBILE_INPUT_METHODS_SEPARATOR"] = "或者";
$MESS["SEC_OTP_MOBILE_MANUAL_INPUT"] = "手動輸入代碼";
$MESS["SEC_OTP_MOBILE_SCAN_QR"] = "掃描二維碼";
$MESS["SEC_OTP_NEW_ACCESS_DENIED"] = "拒絕訪問兩步身份驗證控制。";
$MESS["SEC_OTP_NEW_SWITCH_ON"] = "啟用兩步身份驗證";
$MESS["SEC_OTP_NO_DAYS"] = "永遠";
$MESS["SEC_OTP_PASS1"] = "第一個設備密碼（單擊並寫下）";
$MESS["SEC_OTP_PASS2"] = "第二個設備密碼（再次單擊並寫下）";
$MESS["SEC_OTP_RECOVERY_CODES_BUTTON"] = "恢復代碼";
$MESS["SEC_OTP_RECOVERY_CODES_DESCRIPTION"] = "複製您可能需要的恢復代碼，如果您丟失了移動設備或由於任何其他原因無法通過該應用程序獲得代碼。";
$MESS["SEC_OTP_RECOVERY_CODES_NOTE"] = "代碼只能使用一次。提示：罷工二手代碼在列表中。";
$MESS["SEC_OTP_RECOVERY_CODES_PRINT"] = "列印";
$MESS["SEC_OTP_RECOVERY_CODES_REGENERATE"] = "生成新代碼";
$MESS["SEC_OTP_RECOVERY_CODES_REGENERATE_DESCRIPTION"] = "恢復代碼的縮寫？<br/>
創建一些新的。 <br/> <br/>
創建新的恢復代碼無效<br/>先前生成的代碼。";
$MESS["SEC_OTP_RECOVERY_CODES_SAVE_FILE"] = "保存到文本文件";
$MESS["SEC_OTP_RECOVERY_CODES_TITLE"] = "恢復代碼";
$MESS["SEC_OTP_RECOVERY_CODES_WARNING"] = "在您的錢包或錢包裡說，請放手。每個代碼只能使用一次。";
$MESS["SEC_OTP_SECRET_KEY"] = "秘密鑰匙（用設備提供）";
$MESS["SEC_OTP_START_TIMESTAMP"] = "初始時間值（UNIX時間，例如01.01.2000的946684800；默認值為0）";
$MESS["SEC_OTP_STATUS"] = "當前狀態";
$MESS["SEC_OTP_STATUS_ON"] = "啟用";
$MESS["SEC_OTP_SYNC_NOW"] = "同步";
$MESS["SEC_OTP_TYPE"] = "密碼生成算法";
$MESS["SEC_OTP_UNKNOWN_ERROR"] = "意外的錯誤。請稍後再試。";
$MESS["SEC_OTP_WARNING_RECOVERY_CODES"] = "啟用了兩步身份驗證，但您沒有創建恢復代碼。如果您丟失了移動設備或由於任何其他原因無法通過該應用程序獲得代碼，則可能需要它們。";
$MESS["security_TAB"] = "一次性密碼";
$MESS["security_TAB_TITLE"] = "一次性密碼身份驗證設置";
