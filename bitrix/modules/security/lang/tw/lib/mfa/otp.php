<?php
$MESS["SECURITY_HOTP_TITLE"] = "基於計數器（HOTP）";
$MESS["SECURITY_OTP_ERROR_PASS1_EMPTY"] = "第一個密碼值為空。";
$MESS["SECURITY_OTP_ERROR_PASS1_INVALID"] = "第一個密碼值必須至少具有6個字符，僅包含數字。";
$MESS["SECURITY_OTP_ERROR_PASS2_EMPTY"] = "第二個密碼值為空。";
$MESS["SECURITY_OTP_ERROR_PASS2_INVALID"] = "第二個密碼值必須至少具有6個字符，僅包含數字。";
$MESS["SECURITY_OTP_ERROR_SYNC_ERROR"] = "無法將此秘密密鑰與提供的密碼值同步。";
$MESS["SECURITY_TOTP_TITLE"] = "基於時間（TOTP）";
