<?php
$MESS["BT_CUR_LANG_CURRENCY_RUBLE"] = "擦";
$MESS["BT_CUR_LANG_ERR_DEC_POINT_EQUAL_THOUSANDS_SEP"] = "小數點與為＃lang＃指定的千分隔符相同";
$MESS["BT_CUR_LANG_ERR_FORMAT_STRING_IS_EMPTY"] = "語言＃lang＃的字段\“格式\”為空。";
$MESS["BT_CUR_LANG_ERR_THOUSANDS_SEP_IS_EMPTY"] = "用戶定義的數千個分隔符未針對＃lang＃指定。";
$MESS["BT_CUR_LANG_ERR_THOUSANDS_SEP_IS_NOT_VALID"] = "無效的用戶定義了為＃lang＃指定的數千個分隔符。只有Unicode字符和HTML實體才能用作分離器。";
$MESS["BT_CUR_LANG_SEP_VARIANT_COMMA"] = "逗號";
$MESS["BT_CUR_LANG_SEP_VARIANT_DOT"] = "觀點";
$MESS["BT_CUR_LANG_SEP_VARIANT_EMPTY"] = "沒有分離器";
$MESS["BT_CUR_LANG_SEP_VARIANT_NBSPACE"] = "非破壞空間";
$MESS["BT_CUR_LANG_SEP_VARIANT_SPACE"] = "空間";
