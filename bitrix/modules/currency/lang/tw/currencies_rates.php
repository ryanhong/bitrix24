<?php
$MESS["BX_CURRENCY_GET_RATE_ERR_UNKNOWN"] = "獲取貨幣率的未知錯誤";
$MESS["BX_CURRENCY_RATE_BASE_BASE_CURRENCY_FIELD_ABSENT"] = "未指定";
$MESS["BX_CURRENCY_RATE_BASE_CURRENCY"] = "基本貨幣";
$MESS["BX_CURRENCY_RATE_EDIT_ERR_ADD"] = "添加貨幣利率的錯誤";
$MESS["BX_CURRENCY_RATE_EDIT_ERR_UPDATE"] = "錯誤編輯貨幣匯率ID＃ID＃";
$MESS["BX_CURRENCY_RATE_EDIT_MESS_AMOUNT"] = "貨幣模塊使用<b>直接報價</b>對基本貨幣的貨幣。在基本貨幣中提供貨幣單位（如果是貨幣面值，則需要100、100、1000）的價格。貨幣匯率清單中的基本貨幣不得有記錄。";
$MESS["CONFIRM_DEL_MESSAGE"] = "您一定會刪除此費率嗎？";
$MESS["CURRENCY_EDIT_TITLE"] = "編輯貨幣利率";
$MESS["CURRENCY_F_DEL"] = "卸下過濾器";
$MESS["CURRENCY_NEW_TITLE"] = "新的貨幣利率";
$MESS["CURRENCY_RATES_A_EDIT"] = "變更貨幣率";
$MESS["CURRENCY_RATES_A_EDIT_TITLE"] = "變更貨幣率";
$MESS["CURRENCY_TITLE"] = "貨幣利率";
$MESS["ERROR_ADD_REC"] = "添加新記錄時出錯";
$MESS["ERROR_ADD_REC2"] = "該日期可能已經分配了費率。";
$MESS["ERROR_BASE_CURRENCY_RATE"] = "基本貨幣缺少";
$MESS["ERROR_CURRENCY"] = "未指定貨幣";
$MESS["ERROR_DATE_RATE"] = "費率日期未設置";
$MESS["ERROR_EMPTY_ANSWER"] = "服務器返回一個空響應。";
$MESS["ERROR_QUERY_RATE"] = "quat速時錯誤";
$MESS["ERROR_SAVING_RATE"] = "保存費率時出錯";
$MESS["ERROR_SAVING_RATE1"] = "費率未設置。";
$MESS["ERROR_SAVING_RATE2"] = "金額未設置。";
$MESS["ERROR_SESSID"] = "您的會議已經過期。請重新加載您的頁面。";
$MESS["ERROR_UPDATE_REC"] = "更新記錄時錯誤";
$MESS["SAVE_ERROR"] = "錯誤更新率＃";
$MESS["curr_rates_all"] = "（全部）";
$MESS["curr_rates_curr"] = "貨幣：";
$MESS["curr_rates_curr1"] = "貨幣";
$MESS["curr_rates_date1"] = "日期";
$MESS["curr_rates_del"] = "刪除";
$MESS["curr_rates_err1"] = "錯誤的開始日期格式。";
$MESS["curr_rates_err2"] = "錯誤的結束日期格式。";
$MESS["curr_rates_flt_btn"] = "設置過濾器";
$MESS["curr_rates_flt_date"] = "日期：";
$MESS["curr_rates_list"] = "貨幣";
$MESS["curr_rates_nav"] = "費率";
$MESS["curr_rates_query"] = "詢問";
$MESS["curr_rates_query_ex"] = "查詢遠程服務器";
$MESS["curr_rates_rate"] = "速度";
$MESS["curr_rates_rate_cnt"] = "數量";
$MESS["curr_rates_rate_ex"] = "費率設置";
$MESS["curr_rates_reset"] = "重置";
$MESS["curr_rates_title"] = "貨幣利率";
$MESS["curr_rates_upd"] = "節省";
$MESS["currency_error"] = "錯誤";
