<?php
$MESS["USER_TYPE_MONEY_DEFAULT_VALUE"] = "預設值";
$MESS["USER_TYPE_MONEY_DESCRIPTION"] = "錢";
$MESS["USER_TYPE_MONEY_ERR_BAD_ROW_FORMAT"] = "＃field_name＃的每個值必須是一個格式為價格|貨幣的字符串。";
$MESS["USER_TYPE_MONEY_ERR_BAD_SINGLE_FORMAT"] = "＃field_name＃的值必須是格式為價格|貨幣的字符串。";
