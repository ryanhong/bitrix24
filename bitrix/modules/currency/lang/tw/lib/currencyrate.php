<?php
$MESS["CURRENCY_RATE_ENTITY_BASE_CURRENCY_FIELD"] = "基本貨幣";
$MESS["CURRENCY_RATE_ENTITY_CREATED_BY_FIELD"] = "由...製作";
$MESS["CURRENCY_RATE_ENTITY_CURRENCY_FIELD"] = "貨幣";
$MESS["CURRENCY_RATE_ENTITY_DATE_CREATE_FIELD"] = "創建於";
$MESS["CURRENCY_RATE_ENTITY_DATE_RATE_FIELD"] = "日期";
$MESS["CURRENCY_RATE_ENTITY_ID_FIELD"] = "課程ID";
$MESS["CURRENCY_RATE_ENTITY_MODIFIED_BY_FIELD"] = "修改";
$MESS["CURRENCY_RATE_ENTITY_RATE_CNT_FIELD"] = "面值";
$MESS["CURRENCY_RATE_ENTITY_RATE_FIELD"] = "速度";
$MESS["CURRENCY_RATE_ENTITY_TIMESTAMP_X_FIELD"] = "修改";
