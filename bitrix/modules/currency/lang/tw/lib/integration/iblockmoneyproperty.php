<?php
$MESS["CIMP_FORMAT_ERROR"] = "貨幣領域的格式無效。";
$MESS["CIMP_INPUT_DESCRIPTION"] = "錢";
$MESS["CIMP_INPUT_FORMAT_ERROR"] = "無效的貨幣字段格式。示例：＃示例＃";
$MESS["CIMP_INPUT_FORMAT_ERROR_1"] = "只有數字和分隔符字符：[＃saparators＃]（＃示例＃）";
$MESS["CIMP_INPUT_FORMAT_ERROR_2"] = "無效的格式。示例：（＃示例＃）";
