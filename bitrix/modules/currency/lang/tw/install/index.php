<?php
$MESS["CURRENCY_BASE_EMPTY"] = "未指定基本貨幣。該模塊無法按設計工作。請在<a href= \"#link# \ \">模塊設置頁面上指定基本貨幣</a>。";
$MESS["CURRENCY_BASE_ERROR"] = "沒有指定的基本貨幣；某些貨幣的匯率為1。模塊無法按設計。請在<a href= \"#link# \ \">模塊設置頁面上指定基本貨幣</a>。";
$MESS["CURRENCY_INSTALL_BACK"] = "返回模塊管理部分";
$MESS["CURRENCY_INSTALL_COMPLETE_ERROR"] = "安裝完成了錯誤";
$MESS["CURRENCY_INSTALL_COMPLETE_OK"] = "安裝完成。請參閱“幫助”部分以獲取更多信息。";
$MESS["CURRENCY_INSTALL_DESCRIPTION"] = "這些貨幣模塊可輕鬆管理現場的貨幣和貨幣利率。";
$MESS["CURRENCY_INSTALL_ERROR"] = "安裝錯誤";
$MESS["CURRENCY_INSTALL_NAME"] = "貨幣";
$MESS["CURRENCY_INSTALL_PUBLIC_DIR"] = "公共文件夾";
$MESS["CURRENCY_INSTALL_PUBLIC_SETUP"] = "安裝";
$MESS["CURRENCY_INSTALL_SETUP"] = "安裝";
$MESS["CURRENCY_INSTALL_TITLE"] = "貨幣模塊安裝";
$MESS["CURRENCY_INSTALL_UNPOSSIBLE"] = "該模塊無法卸載。";
$MESS["CURRENCY_UNINSTALL_COMPLETE"] = "卸載完成。";
$MESS["CURRENCY_UNINSTALL_DEL"] = "解除安裝";
$MESS["CURRENCY_UNINSTALL_ERROR"] = "卸載錯誤：";
$MESS["CURRENCY_UNINSTALL_SAVEDATA"] = "要保存存儲在數據庫表中的數據，請檢查“保存表”。複選框。";
$MESS["CURRENCY_UNINSTALL_SAVETABLE"] = "保存表";
$MESS["CURRENCY_UNINSTALL_WARNING"] = "警告！該模塊將從系統中卸載。";
