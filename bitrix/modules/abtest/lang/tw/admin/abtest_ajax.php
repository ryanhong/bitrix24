<?php
$MESS["ABTEST_AJAX_ERROR"] = "錯誤發送請求。";
$MESS["ABTEST_CSRF_ERROR"] = "提交表單時的安全錯誤。請刷新頁面，然後重試。";
$MESS["ABTEST_UNKNOWN_PAGE"] = "該頁面不存在。";
