<?php
$MESS["abtest_entity_active_field"] = "積極的";
$MESS["abtest_entity_descr_field"] = "描述";
$MESS["abtest_entity_duration_field"] = "期間";
$MESS["abtest_entity_enabled_field"] = "啟用";
$MESS["abtest_entity_min_amount_field"] = "最小樣本量";
$MESS["abtest_entity_name_field"] = "姓名";
$MESS["abtest_entity_portion_field"] = "帶寬";
$MESS["abtest_entity_site_field"] = "地點";
$MESS["abtest_entity_sort_field"] = "種類";
$MESS["abtest_entity_start_date_field"] = "開始";
$MESS["abtest_entity_stop_date_field"] = "結束日期";
$MESS["abtest_entity_test_data_field"] = "測試數據";
$MESS["abtest_entity_userid_field"] = "用戶";
