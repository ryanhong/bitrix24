<?php
$MESS["LOCATION_OPT_DEFAULT_SOURCE"] = "默認來源";
$MESS["LOCATION_OPT_FORMAT"] = "地址格式";
$MESS["LOCATION_OPT_LOG_LEVEL"] = "記錄級別";
$MESS["LOCATION_OPT_LOG_LEVEL_DEBUG"] = "偵錯";
$MESS["LOCATION_OPT_LOG_LEVEL_ERROR"] = "錯誤";
$MESS["LOCATION_OPT_LOG_LEVEL_INFO"] = "資訊";
$MESS["LOCATION_OPT_LOG_LEVEL_NONE"] = "沒有任何";
$MESS["LOCATION_OPT_SOURCE_GOOGLE_API_KEY_BACKEND"] = "Google位置API和地理編碼API服務器密鑰";
$MESS["LOCATION_OPT_SOURCE_GOOGLE_API_KEY_BACKEND_NOTE"] = "如何獲取鑰匙：<a href= \"https://develovelers.google.com/maps/documentation/javascript/javascript/get-api-key \ \"> https://develovelers.google.com/maps/maps/ documentation/documentation/ javascript/get-api-key </a>";
$MESS["LOCATION_OPT_SOURCE_GOOGLE_API_KEY_FRONTEND"] = "Google Maps JavaScript API，位置API和地理編碼API瀏覽器密鑰";
$MESS["LOCATION_OPT_SOURCE_GOOGLE_SHOW_PHOTOS_ON_MAP"] = "顯示Google照片的位置地圖查看";
$MESS["LOCATION_OPT_SOURCE_GOOGLE_SHOW_PHOTOS_ON_MAP_NOTE"] = "注意力！ Google可能會為此選項收取添加費。";
$MESS["LOCATION_OPT_SOURCE_GOOGLE_USE_GEOCODING_SERVICE"] = "將地理編碼服務用於未知地址";
$MESS["LOCATION_OPT_SOURCE_GOOGLE_USE_GEOCODING_SERVICE_NOTE"] = "注意力！ Google可能會為此選項收取添加費。";
$MESS["LOCATION_OPT_SOURCE_OSM_SERVICE_URL"] = "服務網址";
$MESS["LOCATION_OPT_TAB_OPTIONS"] = "設定";
$MESS["LOCATION_OPT_TAB_SOURCES_OPTIONS"] = "來源";
