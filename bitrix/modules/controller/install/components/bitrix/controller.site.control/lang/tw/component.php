<?php
$MESS["CC_BCSC_DELETE_ERROR"] = "刪除網站＃id＃的錯誤。";
$MESS["CC_BCSC_EMAIL_ERROR"] = "錯誤更改＃ID＃站點管理員電子郵件：＃消息＃";
$MESS["CC_BCSC_ERROR_MODULE"] = "沒有安裝控制器模塊。";
$MESS["CC_BCSC_PASSWORD_ERROR"] = "錯誤更改＃ID＃網站密碼：＃消息＃";
$MESS["CC_BCSC_RESERVE_ERROR"] = "保留網站的錯誤：＃消息＃。";
$MESS["CC_BCSC_RESERVE_ERROR_ALREADY"] = "這個地址的網站已經存在。";
$MESS["CC_BCSC_RESERVE_ERROR_SITE_URL"] = "網站URL缺失或不正確。";
$MESS["CC_BCSC_TITLE"] = "站點管理組件。";
$MESS["CC_BCSC_UNKNOWN_GROUP"] = "未知組。";
$MESS["CC_BCSC_UPDATE_ERROR"] = "錯誤更新網站＃id＃：＃消息＃";
