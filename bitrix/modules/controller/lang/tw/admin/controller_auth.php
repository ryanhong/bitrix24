<?php
$MESS["CTRLR_AUTH_CS_LABEL"] = "允許授權控制器用戶";
$MESS["CTRLR_AUTH_CS_TAB_TITLE"] = "在客戶端網站上配置控制器用戶身份驗證";
$MESS["CTRLR_AUTH_LOC_GROUPS"] = "地圖組";
$MESS["CTRLR_AUTH_NOTE"] = "控制器管理員始終授權在最大權限的客戶網站上";
$MESS["CTRLR_AUTH_SC_LABEL"] = "允許控制器側身份驗證";
$MESS["CTRLR_AUTH_SC_TAB_TITLE"] = "在控制器上配置客戶端網站用戶身份驗證";
$MESS["CTRLR_AUTH_SETUP"] = "配置";
$MESS["CTRLR_AUTH_SS_LABEL"] = "允許透明授權";
$MESS["CTRLR_AUTH_SS_TAB_TITLE"] = "在客戶網站上配置通過身份驗證";
$MESS["CTRLR_AUTH_TAB"] = "配置用戶身份驗證";
$MESS["CTRLR_AUTH_TITLE"] = "授權";
$MESS["CTRL_AUTH_LOG"] = "身份驗證日誌";
