<?php
$MESS["CTRLR_RUN_ADD_TASK"] = "通過任務調度程序執行命令";
$MESS["CTRLR_RUN_ADD_TASK_LABEL"] = "將命令添加到任務隊列";
$MESS["CTRLR_RUN_BUTT_RUN"] = "跑步";
$MESS["CTRLR_RUN_COMMAND_FIELD"] = "PHP腳本";
$MESS["CTRLR_RUN_COMMAND_TAB_TITLE"] = "遠程執行的PHP腳本";
$MESS["CTRLR_RUN_CONFIRM"] = "輸入的命令將以PHP腳本在遠程服務器上執行。繼續？";
$MESS["CTRLR_RUN_ERR_NSELECTED"] = "選擇一個客戶進行執行";
$MESS["CTRLR_RUN_ERR_TOO_MANY_SELECTED"] = "該命令將在大量連接的站點處執行。如果您確定知道自己在做什麼，請檢查各自的框。";
$MESS["CTRLR_RUN_FILTER_GROUP"] = "團體";
$MESS["CTRLR_RUN_FILTER_GROUP_ANY"] = "（任何）";
$MESS["CTRLR_RUN_FILTER_SITE"] = "地點";
$MESS["CTRLR_RUN_FILTER_SITE_ALL"] = "（全部）";
$MESS["CTRLR_RUN_FORCE_RUN"] = "在任意數量的站點上執行命令";
$MESS["CTRLR_RUN_SUCCESS"] = "＃cont＃cnt＃cnt＃remote命令已成功添加到<a href= \"controller_task.php?lang=#lang# \ \">任務隊列</a>。";
$MESS["CTRLR_RUN_TITLE"] = "遠程PHP命令行";
