<?php
$MESS["CTRLR_WS_ERR_BAD_COMMAND"] = "未找到或過期所需的命令";
$MESS["CTRLR_WS_ERR_BAD_LEVEL"] = "添加新客戶的權限不足";
$MESS["CTRLR_WS_ERR_BAD_OPERID"] = "未知操作ID：";
$MESS["CTRLR_WS_ERR_BAD_PASSW"] = "密碼錯誤。";
$MESS["CTRLR_WS_ERR_FILE_NOT_FOUND"] = "文件未找到";
$MESS["CTRLR_WS_ERR_MEMB_DISCN"] = "斷開客戶的權限不足";
$MESS["CTRLR_WS_ERR_MEMB_NFOUND"] = "找不到客戶";
$MESS["CTRLR_WS_ERR_RUN"] = "執行錯誤：";
$MESS["CTRLR_WS_ERR_RUN_BACK"] = "後退";
$MESS["CTRLR_WS_ERR_RUN_TRY"] = "請再試一次。";
