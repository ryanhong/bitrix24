<?php
$MESS["SUPC_HE_C_UPDATED"] = "幫助部分更新";
$MESS["SUPC_HE_UPD"] = "幫助部分更新失敗";
$MESS["SUPC_LE_C_UPDATED"] = "語言文件更新";
$MESS["SUPC_LE_UPD"] = "語言文件更新失敗";
$MESS["SUPC_ME_CHECK"] = "文件寫驗證錯誤";
$MESS["SUPC_ME_C_UPDATED"] = "常見的內核更新了";
$MESS["SUPC_ME_LOAD"] = "無法下載更新";
$MESS["SUPC_ME_PACK"] = "無法從存檔中提取文件";
$MESS["SUPC_ME_UPDATE"] = "模塊更新失敗";
