<?php
$MESS["MEMBER_ENTITY_ACTIVE_FIELD"] = "積極的";
$MESS["MEMBER_ENTITY_CONTACT_PERSON_FIELD"] = "聯絡人";
$MESS["MEMBER_ENTITY_CONTROLLER_GROUP_ID_FIELD"] = "組ID";
$MESS["MEMBER_ENTITY_COUNTERS_UPDATED_FIELD"] = "櫃檯上次更新";
$MESS["MEMBER_ENTITY_COUNTER_FREE_SPACE_FIELD"] = "磁盤空間計數器";
$MESS["MEMBER_ENTITY_COUNTER_LAST_AUTH_FIELD"] = "最後一個身份驗證計數器";
$MESS["MEMBER_ENTITY_COUNTER_SITES_FIELD"] = "站點計數器";
$MESS["MEMBER_ENTITY_COUNTER_USERS_FIELD"] = "用戶計數器";
$MESS["MEMBER_ENTITY_CREATED_BY_FIELD"] = "由...製作";
$MESS["MEMBER_ENTITY_DATE_ACTIVE_FROM_FIELD"] = "活躍";
$MESS["MEMBER_ENTITY_DATE_ACTIVE_TO_FIELD"] = "活躍直到";
$MESS["MEMBER_ENTITY_DATE_CREATE_FIELD"] = "創建於";
$MESS["MEMBER_ENTITY_DISCONNECTED_FIELD"] = "停用";
$MESS["MEMBER_ENTITY_EMAIL_FIELD"] = "電子郵件地址";
$MESS["MEMBER_ENTITY_HOSTNAME_FIELD"] = "主機名";
$MESS["MEMBER_ENTITY_ID_FIELD"] = "站點ID";
$MESS["MEMBER_ENTITY_MEMBER_ID_FIELD"] = "唯一身份";
$MESS["MEMBER_ENTITY_MODIFIED_BY_FIELD"] = "修改";
$MESS["MEMBER_ENTITY_NAME_FIELD"] = "姓名";
$MESS["MEMBER_ENTITY_NOTES_FIELD"] = "描述";
$MESS["MEMBER_ENTITY_SECRET_ID_FIELD"] = "共享的秘密ID";
$MESS["MEMBER_ENTITY_SHARED_KERNEL_FIELD"] = "常見的內核國旗";
$MESS["MEMBER_ENTITY_SITE_ACTIVE_FIELD"] = "公共訪問標誌";
$MESS["MEMBER_ENTITY_TIMESTAMP_X_FIELD"] = "修改";
$MESS["MEMBER_ENTITY_URL_FIELD"] = "URL";
