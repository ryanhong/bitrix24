<?php
$MESS["CTRLR_OPTIONS_AUTH_LOG_DAYS"] = "保持身份驗證日誌的日子（0-永不刪除）：";
$MESS["CTRLR_OPTIONS_DEF_GROUP"] = "新客戶網站的默認組：";
$MESS["CTRLR_OPTIONS_SHARED_KERNEL_PATH"] = "通往常見內核文件夾的路徑：";
$MESS["CTRLR_OPTIONS_SHOW_HOSTNAME"] = "顯示\“主機名\”字段：";
$MESS["CTRLR_OPTIONS_TASK_RETRY_COUNT"] = "任務執行的數量重試：";
$MESS["CTRLR_OPTIONS_TASK_RETRY_TIMEOUT"] = "任務執行重試間隔，秒：";
$MESS["CTRLR_OPTIONS_TIME_AUTOUPDATE"] = "每次自動將組參數應用於客戶網站（分鐘； 0-不應用）：";
