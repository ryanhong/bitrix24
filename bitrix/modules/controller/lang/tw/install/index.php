<?php
$MESS["CTRL_INST_DESC"] = "遠程站點管理";
$MESS["CTRL_INST_NAME"] = "站點控制器";
$MESS["CTRL_INST_STEP1"] = "控制器模塊安裝：步驟1";
$MESS["CTRL_INST_STEP1_UN"] = "控制器模塊卸載：步驟1";
$MESS["CTRL_PERM_D"] = "拒絕訪問";
$MESS["CTRL_PERM_L"] = "授權網站";
$MESS["CTRL_PERM_R"] = "只讀";
$MESS["CTRL_PERM_T"] = "添加新網站";
$MESS["CTRL_PERM_V"] = "管理站點";
$MESS["CTRL_PERM_W"] = "完全訪問";
