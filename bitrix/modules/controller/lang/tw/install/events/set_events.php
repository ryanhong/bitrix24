<?php
$MESS["CONTROLLER_MEMBER_CLOSED_DESC"] = "＃id＃ - 數字控制器成員ID，
＃成員_ID＃ - 全局控制器成員ID，
＃名稱＃-控制器成員名稱，
＃電子郵件＃ -Controller成員電子郵件地址，
＃Contact_person＃ - 聯繫人，
＃URL＃ - 控制器成員站點的URL，
＃controller_group_id＃ - 控制器成員組的ID，
＃shared_kernel＃ - 共享文件系統內核（y | n），
＃活動＃ -活動狀態，
＃斷開連接＃ - 斷開連接狀態，
＃date_active_from＃ - 激活日期，
＃date_active_to＃ - 到期日期，
＃date_create＃ -創建日期，
＃筆記＃ - 文字說明
";
$MESS["CONTROLLER_MEMBER_CLOSED_NAME"] = "控制器成員網站關閉";
$MESS["CONTROLLER_MEMBER_REGISTER_DESC"] = "＃id＃ - 數字控制器成員ID，
＃成員_ID＃ - 全局控制器成員ID，
＃名稱＃-控制器成員名稱，
＃電子郵件＃ -Controller成員電子郵件地址，
＃Contact_person＃ - 聯繫人，
＃URL＃ - 控制器成員站點的URL，
＃controller_group_id＃ - 控制器成員組的ID，
＃shared_kernel＃ - 共享文件系統內核（y | n），
＃活動＃ -活動狀態，
＃斷開連接＃ - 斷開連接狀態，
＃date_active_from＃ - 激活日期，
＃date_active_to＃ - 到期日期，
＃date_create＃ -創建日期，
＃筆記＃ - 文字說明
";
$MESS["CONTROLLER_MEMBER_REGISTER_NAME"] = "新的控制器成員";
