<?php
$MESS["CONVERSION_DESCRIPTION"] = "轉換是唯一訪問者到網站的百分比
行動，向所有訪客。
<br> <br>
例如，上個月您的網站吸引了10000個唯一訪問者
其中已完成訂單。轉化率為1％。競爭對手的網站
吸引了相同的10000名遊客，其中兩百個已經完成了
訂單 - 這裡的轉換率為2％，這意味著他們的網站更有利可圖。
<br> <br>
此比率僅適用於網絡商店，其他的轉換率
項目可能會更好。";
$MESS["CONVERSION_DESCRIPTION_TITLE"] = "解釋";
$MESS["CONVERSION_FILTER_APPLY"] = "申請";
$MESS["CONVERSION_FILTER_PERIOD"] = "日期範圍";
$MESS["CONVERSION_SCALE_BAD"] = "壞的";
$MESS["CONVERSION_SCALE_EXCELLENT"] = "出色的";
$MESS["CONVERSION_SCALE_GOOD"] = "好的";
$MESS["CONVERSION_SCALE_OK"] = "好的";
$MESS["CONVERSION_SCALE_PASSABLE"] = "可通過";
$MESS["CONVERSION_SITE"] = "轉換";
