<?php
$MESS["BX_SALE_TOOLS_GEN_CPN_ERR_AUTH"] = "你沒有登錄。";
$MESS["BX_SALE_TOOLS_GEN_CPN_ERR_MODULE"] = "不能包括電子商店模塊。";
$MESS["BX_SALE_TOOLS_GEN_CPN_ERR_SESSION"] = "您的會議已經過期。請再次授權。";
$MESS["BX_SALE_TOOLS_GEN_CPN_ERR_USER"] = "無法定義用戶。";
