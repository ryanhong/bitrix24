<?php
$MESS["SALE_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["SALE_ALL"] = "全部";
$MESS["SALE_CASHBOX_ACTIVE"] = "積極的";
$MESS["SALE_CASHBOX_ADD_NEW"] = "連接收銀機";
$MESS["SALE_CASHBOX_COPY"] = "複製URL";
$MESS["SALE_CASHBOX_DATE_CREATE"] = "創建於";
$MESS["SALE_CASHBOX_DATE_LAST_CHECK"] = "最後收據日期";
$MESS["SALE_CASHBOX_DELETE"] = "刪除";
$MESS["SALE_CASHBOX_DELETE_CONFIRM"] = "您確定要刪除收銀機## cashbox_id＃嗎？";
$MESS["SALE_CASHBOX_DELETE_DESCR"] = "刪除";
$MESS["SALE_CASHBOX_EDIT"] = "編輯";
$MESS["SALE_CASHBOX_EDIT_DESCR"] = "編輯現金寄存器參數";
$MESS["SALE_CASHBOX_GENERATE_LINK"] = "創建鏈接";
$MESS["SALE_CASHBOX_ID"] = "ID";
$MESS["SALE_CASHBOX_LAST_CHECK_STATUS"] = "最後的請求狀態";
$MESS["SALE_CASHBOX_LAST_CHECK_STATUS_N"] = "錯誤";
$MESS["SALE_CASHBOX_LAST_CHECK_STATUS_Y"] = "好的";
$MESS["SALE_CASHBOX_MODE_CONFLICT"] = "注意力！一些活動現金寄存器僅支持自動收據打印：<strong> #cashbox_paysystem＃</strong> <br>
自動化模式現在將允許手動添加收據。<br>
當前不支持混合模式。如果要手動添加收據，則必須禁用自動收據。<br>
";
$MESS["SALE_CASHBOX_NAME"] = "姓名";
$MESS["SALE_CASHBOX_NUMBER_KKM"] = "收銀機 ＃";
$MESS["SALE_CASHBOX_POPUP_TITLE"] = "收銀機連接鏈接";
$MESS["SALE_CASHBOX_SORT"] = "種類";
$MESS["SALE_CASHBOX_TITLE"] = "收銀機";
$MESS["SALE_CASHBOX_VERSION_CONFLICT"] = "注意力！一些活躍的現金寄存器支持FFD 1.05：<strong>＃cashbox_ffd105＃</strong> <br>
當前不支持混合模式。您將必須停用以下項目以遷移到FFD 1.05：<strong>＃cashbox_no_ffd105＃</strong>
";
$MESS["SALE_CASHBOX_WINDOW_STEP_1"] = "在應用程序設置中指定此URL";
$MESS["SALE_CASHBOX_WINDOW_STEP_2"] = "啟動我們的收銀機桌面申請";
$MESS["SALE_CASHBOX_WINDOW_TITLE"] = "連接收銀機";
$MESS["SALE_CASHBOX_ZONE_CONFLICT"] = "注意力！一些活躍的現金寄存器支持多個國家。<br>
我們不支持混合模式。請停用支持多個國家的現金記錄。<br>
";
$MESS["SALE_CASHBOX_ZONE_CONFLICT_RU_LIST"] = "俄羅斯收銀機：<strong> #cashboxes＃</strong> <br>";
$MESS["SALE_CASHBOX_ZONE_CONFLICT_UA_LIST"] = "烏克蘭收銀機：<strong> #cashboxes＃</strong> <br>";
$MESS["SALE_F_ACTIVE"] = "積極的";
$MESS["SALE_MARKETPLACE_ADD_NEW"] = "從市場安裝";
$MESS["SALE_MARKETPLACE_ADD_NEW_ALT"] = "點擊從市場安裝";
$MESS["SALE_NO"] = "不";
$MESS["SALE_YES"] = "是的";
$MESS["SPSAN_ERROR_ACTIVE_CASHBOX_PAYSYSTEM"] = "收銀機\“＃cashbox_name＃\”是系統實體。它無法手動激活或停用。";
$MESS["SPSAN_ERROR_DELETE"] = "錯誤刪除收銀機";
$MESS["SPSAN_ERROR_DELETE_1C"] = "由於屬於系統，因此無法刪除1C現金寄存器。";
$MESS["SPSAN_ERROR_DELETE_CASHBOX_PAYSYSTEM"] = "收銀機\“＃cashbox_name＃\”是系統實體。它無法刪除。";
$MESS["SPSAN_ERROR_UPDATE"] = "錯誤更新收銀機";
