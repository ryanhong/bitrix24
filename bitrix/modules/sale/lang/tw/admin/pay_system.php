<?php
$MESS["SALE_ACTION"] = "動作";
$MESS["SALE_ACTIVE"] = "積極的";
$MESS["SALE_ADD"] = "添加";
$MESS["SALE_ALL"] = "全部";
$MESS["SALE_ALLOW_EDIT_PAYMENT"] = "啟用自動重新計算付款";
$MESS["SALE_CONFIRM_DEL_MESSAGE"] = "您確定要刪除付款系統嗎？帶有主動訂單的支付系統無法刪除。";
$MESS["SALE_DELETE"] = "刪除";
$MESS["SALE_DELETE_DESCR"] = "刪除付款系統";
$MESS["SALE_DELETE_ERROR"] = "刪除付款系統的錯誤。一些訂單可能是使用此付款系統下達的。<br>";
$MESS["SALE_EDIT"] = "調整";
$MESS["SALE_EDIT_DESCR"] = "修改付款系統設置";
$MESS["SALE_F_ACTIVE"] = "積極的";
$MESS["SALE_F_CURRENCY"] = "貨幣";
$MESS["SALE_F_DEL"] = "卸下過濾器";
$MESS["SALE_F_FILTER"] = "篩選";
$MESS["SALE_F_LANG"] = "地點";
$MESS["SALE_F_PERSON_TYPE"] = "付款人類型";
$MESS["SALE_F_SUBMIT"] = "設置過濾器";
$MESS["SALE_H_ACTION_FILES"] = "處理者";
$MESS["SALE_H_CURRENCY"] = "貨幣";
$MESS["SALE_H_DESCRIPTION"] = "描述";
$MESS["SALE_H_PERSON_TYPES"] = "付款人類型";
$MESS["SALE_LID"] = "地點";
$MESS["SALE_LOGOTIP"] = "標識";
$MESS["SALE_NAME"] = "姓名";
$MESS["SALE_NO"] = "不";
$MESS["SALE_PRLIST"] = "支付系統";
$MESS["SALE_SECTION_TITLE"] = "支付系統";
$MESS["SALE_SORT"] = "種類。";
$MESS["SALE_YES"] = "是的";
$MESS["SPSAN_ADD_NEW"] = "新的支付系統";
$MESS["SPSAN_ADD_NEW_ALT"] = "點擊添加新的付款系統";
$MESS["SPSAN_ERROR_DELETE"] = "刪除記錄的錯誤";
$MESS["SPSAN_ERROR_UPDATE"] = "錯誤修改記錄";
$MESS["SPSAN_MARKETPLACE_ADD_NEW"] = "從市場安裝";
$MESS["SPSAN_MARKETPLACE_ADD_NEW_ALT"] = "點擊從市場安裝";
$MESS["SPS_ADD_NEW"] = "添加＆gt;＆gt;";
$MESS["SPS_NO"] = "不";
$MESS["SPS_YES"] = "是的";
