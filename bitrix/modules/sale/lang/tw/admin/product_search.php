<?php
$MESS["SOPS_ACTIVE"] = "積極的";
$MESS["SOPS_APPLY"] = "申請";
$MESS["SOPS_BALANCE"] = "平衡";
$MESS["SOPS_COUPON"] = "客戶優惠券";
$MESS["SOPS_PRICE"] = "價格";
$MESS["SOPS_PRICE1"] = "如果訂購＃cnt＃pcs，則為1個項目。";
$MESS["SOPS_QUANTITY"] = "數量";
$MESS["SPS_ACT"] = "積極的";
$MESS["SPS_ACTIVE"] = "積極的";
$MESS["SPS_ANY"] = "任何";
$MESS["SPS_A_PROP_NOT_SET"] = "（沒有設置）";
$MESS["SPS_CAN_BUY_NOT"] = "不幸的是，該產品缺貨。";
$MESS["SPS_CAN_BUY_NOT_PRODUCT"] = "缺貨";
$MESS["SPS_CATALOG"] = "目錄";
$MESS["SPS_CHANGER"] = "修改";
$MESS["SPS_CLOSE"] = "關閉";
$MESS["SPS_CODE"] = "符號代碼";
$MESS["SPS_DESCR"] = "描述";
$MESS["SPS_ID_FROM_TO"] = "開始和結束";
$MESS["SPS_INCLUDING_SUBS"] = "包括小節";
$MESS["SPS_NAME"] = "姓名";
$MESS["SPS_NO"] = "不";
$MESS["SPS_NO_PERMS"] = "您沒有足夠的權限來查看此目錄";
$MESS["SPS_PROCE_FROM"] = "從";
$MESS["SPS_PRODUCT_ACTIVE"] = "是的";
$MESS["SPS_PRODUCT_NO_ACTIVE"] = "不";
$MESS["SPS_PRODUCT_SELECTED"] = "選定";
$MESS["SPS_SEARCH_TITLE"] = "產品";
$MESS["SPS_SECTION"] = "部分";
$MESS["SPS_SELECT"] = "選擇";
$MESS["SPS_SET"] = "放";
$MESS["SPS_SKU_HIDE"] = "隱藏";
$MESS["SPS_SKU_SHOW"] = "展示";
$MESS["SPS_STATUS"] = "地位";
$MESS["SPS_TIMESTAMP"] = "修改的";
$MESS["SPS_TOP_LEVEL"] = "頂層";
$MESS["SPS_UNSET"] = "重置";
$MESS["SPS_VALUE_ANY"] = "任何";
$MESS["SPS_XML_ID"] = "外部ID";
$MESS["SPS_YES"] = "是的";
$MESS["prod_search_cancel"] = "取消";
$MESS["prod_search_cancel_title"] = "顯示所有記錄";
$MESS["prod_search_find"] = "設置過濾器";
$MESS["prod_search_find_title"] = "選擇匹配記錄";
$MESS["sale_prod_search_nav"] = "商品";
