<?php
$MESS["SALE_ALLOW_DELIVERY"] = "允許交貨";
$MESS["SALE_CANCELED"] = "取消";
$MESS["SALE_COUNT"] = "全部的";
$MESS["SALE_DATE"] = "日期";
$MESS["SALE_PAYED"] = "有薪酬的";
$MESS["SALE_SECTION_TITLE"] = "訂單統計";
$MESS["SALE_SHOW"] = "展示";
$MESS["SALE_S_BY"] = "通過...分組";
$MESS["SALE_S_DATE"] = "訂購日期";
$MESS["SALE_S_DAY"] = "天";
$MESS["SALE_S_MONTH"] = "月";
$MESS["SALE_S_SITE"] = "地點";
$MESS["SALE_S_WEEK"] = "星期";
$MESS["SALE_S_WEEKDAY"] = "工作日";
$MESS["SALE_S_YEAR"] = "年";
