<?php
$MESS["SALE_DELIVERY_REQUEST_ACCESS_DENIED"] = "拒絕訪問";
$MESS["SALE_DELIVERY_REQUEST_BACK"] = "後退";
$MESS["SALE_DELIVERY_REQUEST_LIST"] = "運輸命令";
$MESS["SALE_DELIVERY_REQUEST_SEND"] = "發送運輸訂單";
$MESS["SALE_DELIVERY_REQUEST_SHIPMENT_IDS_EMPTY"] = "裝運ID列表為空";
