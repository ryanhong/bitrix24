<?php
$MESS["BUYER_PE_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["BUYER_PE_EMPTY_PROPS"] = "需要字段\“＃名稱＃\”。";
$MESS["BUYER_PE_FIELD"] = "定制";
$MESS["BUYER_PE_FIELD_TITLE"] = "自定義表單字段";
$MESS["BUYER_PE_LIST_PROFILE"] = "配置文件列表";
$MESS["BUYER_PE_NO_PROFILE"] = "找不到輪廓。";
$MESS["BUYER_PE_NO_USER"] = "找不到用戶。";
$MESS["BUYER_PE_PROFILE_NAME"] = "輪廓";
$MESS["BUYER_PE_TAB_PROFILE"] = "輪廓";
$MESS["BUYER_PE_TAB_PROFILE_TITLE"] = "編輯個人資料";
$MESS["BUYER_PE_TAB_TITLE"] = "客戶資料";
$MESS["BUYER_PE_TITLE"] = "編輯配置文件＃名稱＃";
$MESS["BUYER_PE_USER"] = "用戶";
