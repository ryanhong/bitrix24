<?php
$MESS["SOG_ACCOUNT_NUMBER"] = "帳戶";
$MESS["SOG_ADDRESS_FULL"] = "完整地址";
$MESS["SOG_AGENT_NAME"] = "姓名";
$MESS["SOG_BIRTHDAY"] = "出生日期";
$MESS["SOG_BUILDING"] = "建築";
$MESS["SOG_B_ADDRESS_FULL"] = "完整的地址";
$MESS["SOG_B_BIK"] = "垃圾桶";
$MESS["SOG_B_BUILDING"] = "建築";
$MESS["SOG_B_CITY"] = "城市";
$MESS["SOG_B_COUNTRY"] = "國家";
$MESS["SOG_B_HOUSE"] = "＃";
$MESS["SOG_B_INDEX"] = "壓縮";
$MESS["SOG_B_NAME"] = "銀行";
$MESS["SOG_B_REGION"] = "地區";
$MESS["SOG_B_STATE"] = "狀態";
$MESS["SOG_B_STREET"] = "街道";
$MESS["SOG_B_TOWN"] = "鎮";
$MESS["SOG_CITY"] = "城市";
$MESS["SOG_CONTACT_PERSON"] = "聯絡人";
$MESS["SOG_COUNTRY"] = "國家";
$MESS["SOG_EGRPO"] = "Egrpo";
$MESS["SOG_EMAIL"] = "電子郵件";
$MESS["SOG_FLAT"] = "易於。";
$MESS["SOG_FULL_NAME"] = "全名";
$MESS["SOG_F_ADDRESS_FULL"] = "完整的街道地址";
$MESS["SOG_F_BUILDING"] = "建築";
$MESS["SOG_F_CITY"] = "城市";
$MESS["SOG_F_COUNTRY"] = "國家";
$MESS["SOG_F_FLAT"] = "易於。";
$MESS["SOG_F_HOUSE"] = "＃";
$MESS["SOG_F_INDEX"] = "壓縮";
$MESS["SOG_F_REGION"] = "地區";
$MESS["SOG_F_STATE"] = "狀態";
$MESS["SOG_F_STREET"] = "街道";
$MESS["SOG_F_TOWN"] = "鎮";
$MESS["SOG_HOUSE"] = "＃";
$MESS["SOG_INDEX"] = "壓縮";
$MESS["SOG_INN"] = "itin";
$MESS["SOG_KPP"] = "KPP（稅收註冊原因代碼）";
$MESS["SOG_MALE"] = "性別";
$MESS["SOG_NAME"] = "姓名";
$MESS["SOG_OKDP"] = "OKDP";
$MESS["SOG_OKFC"] = "好的";
$MESS["SOG_OKOPF"] = "OKOPF";
$MESS["SOG_OKPO"] = "OKPO";
$MESS["SOG_OKVED"] = "好的";
$MESS["SOG_PARAM"] = "參數名稱";
$MESS["SOG_PHONE"] = "電話";
$MESS["SOG_REGION"] = "地區";
$MESS["SOG_SECOND_NAME"] = "中間名字";
$MESS["SOG_STATE"] = "狀態";
$MESS["SOG_STREET"] = "街道";
$MESS["SOG_SURNAME"] = "姓";
$MESS["SOG_TOWN"] = "鎮";
$MESS["SO_ADIT_1C_PARAMS"] = "其他參數（作為訂單屬性傳遞至1C）";
$MESS["SPSG_ACT_PROP"] = "處理程序屬性";
$MESS["SPSG_ADD"] = "更多的";
$MESS["SPSG_FROM_ORDER"] = "訂單參數";
$MESS["SPSG_FROM_PROPS"] = "訂購屬性";
$MESS["SPSG_FROM_USER"] = "用戶參數";
$MESS["SPSG_OTHER"] = "其他值";
$MESS["SPSG_TYPE"] = "類型";
$MESS["SPSG_VALUE"] = "價值";
