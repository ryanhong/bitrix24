<?php
$MESS["BIGDATA_ANALYZE"] = "分析";
$MESS["BIGDATA_CONNECT"] = "在簡化的用戶界面中將偏離和在線銷售帶入在一起。";
$MESS["BIGDATA_CONVERT"] = "出售更多並擊敗您的競爭對手！";
$MESS["BIGDATA_DESC_1"] = "<strong> bitrix bigdata </strong>個性化服務流程從Bitrix CMS供電的所有Web商店收集的用戶數據。";
$MESS["BIGDATA_DESC_2"] = "該服務分析用戶的偏好（興趣，訂購項目，比較用戶查找類似特徵），同時確保用戶安全和匿名。";
$MESS["BIGDATA_DESC_3"] = "個性化組件幾乎可以在任何地方添加：到主頁，產品列表或產品查看頁面，購物車或訂購頁面。";
$MESS["BIGDATA_DISABLED"] = "Bitrix Bigdata離線。使其能夠使用其服務。";
$MESS["BIGDATA_ENABLED"] = "Bitrix BigData可用";
$MESS["BIGDATA_GO"] = "去";
$MESS["BIGDATA_HOWTO_ENABLE"] = "您如何啟用Bitrix BigData？";
$MESS["BIGDATA_INSTALLED"] = "將小部件目錄安裝到主頁，到產品列表或產品查看頁面，購物車或訂購頁面。";
$MESS["BIGDATA_NUM_ONE"] = "Bitrix bigdata <br>一流的網絡商店聚合器。";
$MESS["BIGDATA_OBSERVE"] = "突出顯示在訂單頁面上推薦的產品銷售";
$MESS["BIGDATA_PERSONALIZATION"] = "個性化";
$MESS["BIGDATA_PLATFORM"] = "集成在Bitrix CMS中。";
$MESS["BIGDATA_UNINSTALLED"] = "小部件未安裝";
