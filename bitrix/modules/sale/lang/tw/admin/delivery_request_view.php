<?php
$MESS["SALE_DELIVERY_REQ_VIEW_ACTIONS"] = "動作";
$MESS["SALE_DELIVERY_REQ_VIEW_CONTENT"] = "內容";
$MESS["SALE_DELIVERY_REQ_VIEW_CONTENT_T"] = "運輸訂單項目";
$MESS["SALE_DELIVERY_REQ_VIEW_DELETE"] = "刪除運輸訂單";
$MESS["SALE_DELIVERY_REQ_VIEW_DEL_CONFIRM"] = "您確定要刪除運輸訂單嗎？";
$MESS["SALE_DELIVERY_REQ_VIEW_ERROR"] = "錯誤";
$MESS["SALE_DELIVERY_REQ_VIEW_F_DATE_INSERT"] = "創建於";
$MESS["SALE_DELIVERY_REQ_VIEW_F_DELIVERY_IDT"] = "送貨服務";
$MESS["SALE_DELIVERY_REQ_VIEW_F_EXTERNAL_ID"] = "外部ID";
$MESS["SALE_DELIVERY_REQ_VIEW_F_ID"] = "ID";
$MESS["SALE_DELIVERY_REQ_VIEW_F_STATUS"] = "地位";
$MESS["SALE_DELIVERY_REQ_VIEW_F_STATUS_P"] = "處理（已完成）";
$MESS["SALE_DELIVERY_REQ_VIEW_F_STATUS_R"] = "準備好";
$MESS["SALE_DELIVERY_REQ_VIEW_F_STATUS_S"] = "發送";
$MESS["SALE_DELIVERY_REQ_VIEW_MAIN"] = "主要的";
$MESS["SALE_DELIVERY_REQ_VIEW_MAIN_T"] = "交通命令的一般信息";
$MESS["SALE_DELIVERY_REQ_VIEW_SHP_LIST"] = "貨物";
$MESS["SALE_DELIVERY_REQ_VIEW_SHP_LIST_T"] = "運輸順序的運輸";
$MESS["SALE_DELIVERY_REQ_VIEW_TITLE"] = "查看運輸訂單";
$MESS["SALE_DELIVERY_REQ_VIEW_TO_LIST"] = "返回目錄";
