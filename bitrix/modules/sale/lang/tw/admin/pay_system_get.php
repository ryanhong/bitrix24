<?php
$MESS["SPSG_ACT_PROP"] = "處理程序屬性";
$MESS["SPSG_DEL"] = "刪除文件";
$MESS["SPSG_FROM_ORDER"] = "訂購特定參數";
$MESS["SPSG_FROM_PROPS"] = "訂購屬性";
$MESS["SPSG_FROM_USER"] = "用戶參數";
$MESS["SPSG_OTHER"] = "其他";
$MESS["SPSG_TARIFS"] = "關稅";
$MESS["SPSG_TYPE"] = "類型";
$MESS["SPSG_VALUE"] = "價值";
