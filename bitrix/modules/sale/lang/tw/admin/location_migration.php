<?php
$MESS["SALE_LOCATION_MIGRATION_ALREADY_DONE"] = "遷移到新位置已經進行了。無需再次遷移。";
$MESS["SALE_LOCATION_MIGRATION_ERROR"] = "錯誤";
$MESS["SALE_LOCATION_MIGRATION_NEUTRAL_ERROR"] = "有一個錯誤";
$MESS["SALE_LOCATION_MIGRATION_STAGE_COMPLETE"] = "遷移完成，現在重定向...";
$MESS["SALE_LOCATION_MIGRATION_STAGE_CONVERT_LINKS"] = "轉換實體綁定";
$MESS["SALE_LOCATION_MIGRATION_STAGE_CONVERT_TREE"] = "轉換位置樹";
$MESS["SALE_LOCATION_MIGRATION_STAGE_CONVERT_ZONES"] = "轉換商店服務區域";
$MESS["SALE_LOCATION_MIGRATION_STAGE_COPY_DEFAULT_LOCATIONS"] = "複製選定的位置";
$MESS["SALE_LOCATION_MIGRATION_STAGE_COPY_ZIP_CODES"] = "複製郵政編碼";
$MESS["SALE_LOCATION_MIGRATION_STAGE_CREATE_TYPES"] = "創建位置類型";
$MESS["SALE_LOCATION_MIGRATION_START"] = "開始遷移";
$MESS["SALE_LOCATION_MIGRATION_STATUS"] = "地位";
$MESS["SALE_LOCATION_MIGRATION_TAB_MIGRATION_TITLE"] = "遷移到位置2.0";
$MESS["SALE_LOCATION_MIGRATION_TITLE"] = "遷移到位置2.0";
