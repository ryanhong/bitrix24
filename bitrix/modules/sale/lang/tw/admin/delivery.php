<?php
$MESS["SALE_ACTION"] = "動作";
$MESS["SALE_ACTIVE"] = "積極的";
$MESS["SALE_ADD"] = "添加";
$MESS["SALE_ALL"] = "（全部）";
$MESS["SALE_CONFIRM_DEL_MESSAGE"] = "您確定要刪除此交付服務嗎？如果此交貨服務包含訂單，則該服務將不會刪除。";
$MESS["SALE_DELETE"] = "刪除";
$MESS["SALE_DELETE_DESCR"] = "刪除送貨服務";
$MESS["SALE_DELETE_ERROR"] = "錯誤刪除交貨服務。該服務可能包含訂單。<br>";
$MESS["SALE_EDIT"] = "調整";
$MESS["SALE_EDIT_DESCR"] = "編輯送貨服務首選項";
$MESS["SALE_FROM"] = "從";
$MESS["SALE_F_ACTIVE"] = "積極的";
$MESS["SALE_F_DEL"] = "卸下過濾器";
$MESS["SALE_F_FILTER"] = "篩選";
$MESS["SALE_F_FROM"] = "從";
$MESS["SALE_F_LANG"] = "地點";
$MESS["SALE_F_LOCATION"] = "地點";
$MESS["SALE_F_ORDER_PRICE"] = "訂單價格";
$MESS["SALE_F_ORDER_PRICE_DESC"] = "此過濾器需要站點過濾器";
$MESS["SALE_F_SUBMIT"] = "設置過濾器";
$MESS["SALE_F_TO"] = "到";
$MESS["SALE_F_WEIGHT"] = "重量";
$MESS["SALE_LID"] = "地點";
$MESS["SALE_NAME"] = "姓名";
$MESS["SALE_NO"] = "不";
$MESS["SALE_ORDER_PRICE"] = "訂單價格";
$MESS["SALE_PRICE"] = "價格";
$MESS["SALE_PRLIST"] = "送貨服務";
$MESS["SALE_SECTION_TITLE"] = "可配置的交付服務";
$MESS["SALE_SORT"] = "種類。";
$MESS["SALE_TO"] = "到";
$MESS["SALE_WEIGHT"] = "重量";
$MESS["SALE_YES"] = "是的";
$MESS["SDAN_ADD_NEW"] = "新的送貨服務";
$MESS["SDAN_ADD_NEW_ALT"] = "點擊添加新的送貨服務";
$MESS["SDAN_ERROR_DELETE"] = "錯誤刪除送貨服務";
$MESS["SDAN_ERROR_UPDATE"] = "修改送貨服務的錯誤";
$MESS["SDAN_HANDLERS"] = "自動交付";
$MESS["SDAN_HANDLERS_ALT"] = "編輯自動交付服務";
$MESS["SD_ADD_NEW"] = "添加";
$MESS["SD_NO"] = "不";
$MESS["SD_YSE"] = "是的";
