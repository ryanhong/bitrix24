<?php
$MESS["SOPAN_4NEW_PROMT"] = "付款人類型：";
$MESS["SOPAN_ADD_NEW"] = "新的";
$MESS["SOPAN_ADD_NEW_ALT"] = "單擊創建新屬性";
$MESS["USERTYPE_ADD_TITLE"] = "新的自定義字段";
$MESS["USERTYPE_DELETE_CONF"] = "您確定要繼續嗎？";
$MESS["USERTYPE_DEL_ERROR"] = "錯誤刪除字段";
$MESS["USERTYPE_EDIT_IN_LIST"] = "編輯";
$MESS["USERTYPE_ENTITY_ID"] = "實體";
$MESS["USERTYPE_FIELD_NAME"] = "字段名稱";
$MESS["USERTYPE_F_FIND"] = "搜尋";
$MESS["USERTYPE_IS_SEARCHABLE"] = "搜尋";
$MESS["USERTYPE_MANDATORY"] = "必需的";
$MESS["USERTYPE_MULTIPLE"] = "多種的";
$MESS["USERTYPE_NAV"] = "特性";
$MESS["USERTYPE_PERSON"] = "付款人類型";
$MESS["USERTYPE_SHOW_FILTER"] = "篩選";
$MESS["USERTYPE_SHOW_IN_LIST"] = "可見的";
$MESS["USERTYPE_SORT"] = "種類。";
$MESS["USERTYPE_TITLE"] = "自定義屬性";
$MESS["USERTYPE_UPDATE_ERROR"] = "錯誤更新字段。";
$MESS["USERTYPE_USER_TYPE_ID"] = "數據類型";
$MESS["USERTYPE_XML_ID"] = "xml_id";
$MESS["USER_TYPE_FILTER_E"] = "通配符搜索";
$MESS["USER_TYPE_FILTER_I"] = "完全符合";
$MESS["USER_TYPE_FILTER_N"] = "隱藏";
$MESS["USER_TYPE_FILTER_S"] = "部分比賽";
