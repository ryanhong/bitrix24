<?php
$MESS["SATE1_ADD"] = "新層";
$MESS["SATE1_DELETE"] = "刪除層";
$MESS["SATE1_DELETE_CONF"] = "您確定要刪除層嗎？";
$MESS["SATE1_ERROR_SAVE"] = "保存層的錯誤";
$MESS["SATE1_EXISTS"] = "網站＃site_id＃已經有層";
$MESS["SATE1_LIST"] = "層";
$MESS["SATE1_NO_SITE"] = "沒有網站分配給層";
$MESS["SATE1_RATE1"] = "1級關稅：";
$MESS["SATE1_RATE2"] = "2級關稅：";
$MESS["SATE1_RATE3"] = "3級關稅：";
$MESS["SATE1_RATE4"] = "4級關稅：";
$MESS["SATE1_RATE5"] = "5級關稅：";
$MESS["SATE1_SITE"] = "地點：";
$MESS["SATE1_TIER"] = "層";
$MESS["SATE1_TIER_ALT"] = "層參數";
$MESS["SATE1_TITLE_ADD"] = "創建並添加新層";
$MESS["SATE1_TITLE_UPDATE"] = "編輯層＃ID＃";
