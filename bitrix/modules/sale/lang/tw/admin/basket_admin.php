<?php
$MESS["BASKET_AVAIBLE"] = "可用性";
$MESS["SB_ADD_NEW"] = "添加";
$MESS["SB_ALL"] = "（全部）";
$MESS["SB_BASKET"] = "產品";
$MESS["SB_BASKET_NAME"] = "產品名稱";
$MESS["SB_BASKET_PRICE"] = "產品價格";
$MESS["SB_BASKET_QUANTITY"] = "數量";
$MESS["SB_BASKET_TYPE"] = "有存貨";
$MESS["SB_CNT"] = "獨特的產品";
$MESS["SB_CREATE_ORDER"] = "查看";
$MESS["SB_CURRENCY"] = "貨幣";
$MESS["SB_DATE_INSERT"] = "創建於";
$MESS["SB_DATE_UPDATE"] = "修改";
$MESS["SB_FILTER_ALL"] = "所有時間最昂貴的推車";
$MESS["SB_FILTER_PRD"] = "最大的推車";
$MESS["SB_FILTER_WEEK"] = "本週最昂貴的車";
$MESS["SB_FUSER_ID"] = "客戶ID";
$MESS["SB_FUSER_INFO"] = "客戶資料";
$MESS["SB_F_FROM"] = "從";
$MESS["SB_F_TO"] = "直到";
$MESS["SB_ITOG"] = "全部的";
$MESS["SB_LID"] = "網站";
$MESS["SB_MAILTO"] = "發信息";
$MESS["SB_NAV"] = "購物車";
$MESS["SB_NO"] = "不";
$MESS["SB_NOT_AUTH"] = "未登錄";
$MESS["SB_PRICE_ALL"] = "價格";
$MESS["SB_PRODUCT_ID"] = "產品";
$MESS["SB_QUANTITY_ALL"] = "總產品";
$MESS["SB_SET"] = "安裝";
$MESS["SB_SHT"] = "件。";
$MESS["SB_TITLE"] = "廢棄的購物車";
$MESS["SB_TYPE_AV"] = "有存貨";
$MESS["SB_TYPE_CAN_BUY"] = "可立即購買";
$MESS["SB_TYPE_DEL"] = "留以後用";
$MESS["SB_TYPE_DELAY"] = "留以後用";
$MESS["SB_TYPE_NA"] = "N/A。";
$MESS["SB_TYPE_SUB"] = "船長";
$MESS["SB_TYPE_SUBCRIBE"] = "儲備通知訂閱";
$MESS["SB_UNIVERSAL"] = "顧客";
$MESS["SB_UNSET"] = "重置";
$MESS["SB_USER"] = "顧客";
$MESS["SB_USER_EMAIL"] = "電子郵件";
$MESS["SB_USER_GROUP_ID"] = "用戶組";
$MESS["SB_USER_ID"] = "用戶身份";
$MESS["SB_USER_INFO"] = "用戶資料";
$MESS["SB_USER_LOGIN"] = "客戶登錄";
$MESS["SB_YES"] = "是的";
