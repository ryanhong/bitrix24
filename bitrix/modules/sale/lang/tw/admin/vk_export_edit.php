<?php
$MESS["SALE_VK_ACCESS_DENIED"] = "拒絕訪問";
$MESS["SALE_VK_MODULE_NOT_INSTALLED"] = "沒有安裝電子商店模塊。";
$MESS["SALE_VK_ONLY_RUSSIAN"] = "出口到VK僅在俄羅斯可用。";
$MESS["SALE_VK_ONLY_RUSSIAN_2"] = "如果要配置Exchange，請使用頂部菜單更改站點語言。";
$MESS["SALE_VK_TITLE"] = "VK導出 - ＃E1＃";
$MESS["SALE_VK_TITLE_NEW"] = "VK導出 - 新個人資料";
