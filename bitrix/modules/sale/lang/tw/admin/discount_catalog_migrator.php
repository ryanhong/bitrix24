<?php
$MESS["DISCOUNT_CATALOG_MIGRATOR_CONFIRM_MESSAGE"] = "您已經合併了折扣。您確定要重複合併嗎？";
$MESS["DISCOUNT_CATALOG_MIGRATOR_CONVERT_COMPLETE"] = "遷移已經完成";
$MESS["DISCOUNT_CATALOG_MIGRATOR_CONVERT_FAILED"] = "錯誤遷移折扣";
$MESS["DISCOUNT_CATALOG_MIGRATOR_CONVERT_IN_PROGRESS"] = "遷移仍在進行中";
$MESS["DISCOUNT_CATALOG_MIGRATOR_CONVERT_START_BUTTON"] = "開始遷移";
$MESS["DISCOUNT_CATALOG_MIGRATOR_CONVERT_STOP_BUTTON"] = "中止";
$MESS["DISCOUNT_CATALOG_MIGRATOR_CONVERT_TAB"] = "數據傳輸";
$MESS["DISCOUNT_CATALOG_MIGRATOR_CONVERT_TAB_TITLE"] = "數據傳輸";
$MESS["DISCOUNT_CATALOG_MIGRATOR_CONVERT_TITLE"] = "遷移商業目錄折扣";
$MESS["DISCOUNT_CATALOG_MIGRATOR_ERROR_REPORT"] = "無法處理<a href \"#url# \ \">＃標題＃</a>：＃錯誤＃";
$MESS["DISCOUNT_CATALOG_MIGRATOR_HELLO_TEXT"] = "巫師將將商業目錄折扣與電子商店折扣合併，以便依次應用它們。<br> <br> <br>
這將有助於管理折扣依賴項，在需要時暫停折扣或指定折扣優先級。<br> <br>
合併可能需要一段時間，具體取決於您的項目的折扣。建議您在服務器負載和網站流量較低時繼續進行。<br>";
$MESS["DISCOUNT_CATALOG_MIGRATOR_HELLO_TEXT_CUMULATIVE_PART"] = "您正在使用漸進式折扣！<br> <br>
<b>很重要！</b> <br> <br>
折扣處理邏輯已得到改善；現在可以應用兩個選項：\“不要應用進一步的規則\”和\“設置折扣優先\”。
漸進式折扣也將遷移。然後，您必須檢查並設置折扣優先級，並在需要時配置到期。<br>
默認情況下，漸進式折扣優先級設置為最低的值，因此它們將在上次使用。請注意，如果有更高優先級的折扣，其選項\“不應用進一步的規則\”已啟用，則不會適用漸進式折扣。<br> <br> <br> <br>
";
$MESS["DISCOUNT_CATALOG_MIGRATOR_HELLO_TEXT_FINAL"] = "合併折扣後，您必須檢查\“不應用進一步折扣\”折扣選項是否在每個折扣中正確選擇。商業目錄的折扣和電子商店模塊的折扣現在處於同一隊列，因此可能會互相影響。<br> <br> <br>
建議在繼續之前備份您的項目和數據庫。<br> <br>
合併時您網站的公共區域將被禁用。
";
$MESS["DISCOUNT_CATALOG_MIGRATOR_HELLO_TEXT_NEW"] = "此更新將在統一的處理隊列中合併商業目錄折扣和電子商店模塊的折扣。<br> <br> <br>
合併後，折扣將更容易管理。這還將幫助您控制折扣依賴項，隨時暫停折​​扣並指定折扣申請優先級。<br> <br> <br>
如果您的項目指定很多折扣，則合併可能需要一段時間。建議當您的網站的網站體驗最低負載時執行此任務。<br> <br>
＃cumulative_part＃
<br>
";
$MESS["DISCOUNT_CATALOG_MIGRATOR_NON_SUPPORTED_FEATURE_DISC_CURRENCY_SALE_SITE"] = "為該站點選擇的折扣貨幣和電子儲物貨幣不同。";
$MESS["DISCOUNT_CATALOG_MIGRATOR_NON_SUPPORTED_FEATURE_DISC_SAVE"] = "漸進式折扣";
$MESS["DISCOUNT_CATALOG_MIGRATOR_NON_SUPPORTED_FEATURE_RELATIVE_ACTIVE_PERIOD"] = "漸進折扣壽命設置為\“折扣開始日期\”的時間段";
$MESS["DISCOUNT_CATALOG_MIGRATOR_NON_SUPPORTED_TEXT"] = "當前無法轉換或合併一些折扣。請注意這個問題。<br>
已經發現以下未支撐的折扣：<br> <br>
";
$MESS["DISCOUNT_CATALOG_MIGRATOR_PAGE_HELLO_TEXT"] = "<p>折扣合併嚮導將幫助您遷移到更好，改進的管理折扣的方法。</p>
<p>嚮導完成後，您將準備好使用營銷工具的軍械庫。您可以開始使用折扣，而不必學習所有復雜的配置選項。</p>
<p>提供靈活的折扣，營銷您的產品，增加銷售！系統將為您做家務。</p>
";
$MESS["DISCOUNT_CATALOG_MIGRATOR_PAGE_REPEAT_HELLO_TEXT"] = "<p>您已經合併了折扣。不建議在沒有嚴重原因的情況下重複合併。</p>";
$MESS["DISCOUNT_CATALOG_MIGRATOR_PROCESSED_SUMMARY"] = "折扣遷移（累積）：";
$MESS["DISCOUNT_CATALOG_MIGRATOR_UNKNOWN_ERROR"] = "未知錯誤遷移折扣";
