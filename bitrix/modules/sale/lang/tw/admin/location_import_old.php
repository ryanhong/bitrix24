<?php
$MESS["ERROR_LOC_FORMAT"] = "錯誤的位置文件格式：未設置主語言";
$MESS["LOCATION_CLEAR"] = "清除";
$MESS["LOCATION_CLEAR_BTN"] = "清除";
$MESS["LOCATION_CLEAR_CONFIRM"] = "您確定要刪除所有位置嗎？";
$MESS["LOCATION_CLEAR_DESC"] = "刪除所有位置";
$MESS["LOCATION_CLEAR_OK"] = "位置已刪除。";
$MESS["LOCA_DEL_OLD"] = "加載前清除站點位置";
$MESS["LOCA_DO_LOAD"] = "載入";
$MESS["LOCA_FILE"] = "文件";
$MESS["LOCA_HINT"] = "您可以從Bitrix網站下載位置文件：<ul style = \“ font-size：100％\”>
<li> <a href= \"http://www.1c-bitrix.ru/download/files/locations/loc_cntr.csv \">世界國家</a> </li>
<li> <a href= \"http://www.1c-bitrix.ru/download/files/locations/loc_usa.csv \"> usia </a> </a> </li>
</ul>";
$MESS["LOCA_LOADING"] = "位置導入嚮導";
$MESS["LOCA_LOADING_OLD"] = "從文件加載位置";
$MESS["LOCA_LOADING_WIZARD"] = "運行位置導入嚮導";
$MESS["LOCA_LOCATIONS_CITY_STATS"] = "城市";
$MESS["LOCA_LOCATIONS_COUNTRY_STATS"] = "國家";
$MESS["LOCA_LOCATIONS_GROUP_STATS"] = "位置組";
$MESS["LOCA_LOCATIONS_LOC_STATS"] = "總位置";
$MESS["LOCA_LOCATIONS_REGION_STATS"] = "地區";
$MESS["LOCA_LOCATIONS_STATS"] = "位置統計";
$MESS["LOCA_SAVE"] = "載入";
$MESS["NO_LOC_FILE"] = "位置數據文件未加載";
$MESS["NO_MAIN_LANG"] = "網站上未設置位置文件的主要語言";
$MESS["OMLOADED1"] = "載入";
$MESS["OMLOADED2"] = "國家";
$MESS["OMLOADED3"] = "城市";
$MESS["OMLOADED4"] = "創建的位置";
$MESS["location_admin_import"] = "進口位置";
$MESS["location_admin_import_tab"] = "進口";
$MESS["location_admin_import_tab_old"] = "從文件中導入";
