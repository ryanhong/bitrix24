<?php
$MESS["SOE_EXPORT_ERROR"] = "導出錯誤";
$MESS["SOE_NO_FORMAT"] = "未指定導出格式";
$MESS["SOE_NO_SALE"] = "E商店模塊未安裝";
$MESS["SOE_NO_SCRIPT"] = "數據導出腳本＃文件＃找不到";
$MESS["SOE_WRONG_FORMAT"] = "未知的導出格式\“＃export_format＃\”";
