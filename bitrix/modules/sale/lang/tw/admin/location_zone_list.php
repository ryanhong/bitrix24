<?php
$MESS["SALE_LOCATION_L_EDIT_ITEM"] = "編輯位置列表";
$MESS["SALE_LOCATION_L_EDIT_PAGE_TITLE"] = "站點的位置";
$MESS["SALE_LOCATION_L_ITEM"] = "綁定";
$MESS["SALE_LOCATION_L_ITEM_NOT_FOUND"] = "沒有發現此ID的約束";
$MESS["SALE_LOCATION_L_PAGES"] = "網站綁定";
$MESS["SALE_MODULE_ACCES_DENIED"] = "進入模塊被拒絕";
