<?php
$MESS["SAA_0_SUM"] = " - 清除保留金額（資金已轉移）。";
$MESS["SAA_ACTIVE"] = "積極的";
$MESS["SAA_ACTIVE1"] = "積極的：";
$MESS["SAA_ADD_AFFILIATE"] = "添加會員";
$MESS["SAA_ADD_AFFILIATE_DESCR"] = "添加了一個新的會員";
$MESS["SAA_AFFILIATES"] = "會員";
$MESS["SAA_ALL"] = "（全部）";
$MESS["SAA_CALCULATE_AFF"] = "計算會員利潤";
$MESS["SAA_CALCULATE_AFF_EXT"] = "計算會員金額（擴展）";
$MESS["SAA_DATE_CREATE"] = "創建";
$MESS["SAA_DELETE"] = "刪除";
$MESS["SAA_DELETE_CONF"] = "您確定要刪除此會員嗎？";
$MESS["SAA_EDIT"] = "編輯";
$MESS["SAA_ERROR_ACTIVATE"] = "錯誤更改會員活動狀態";
$MESS["SAA_ERROR_CALCULATE"] = "錯誤計算會員利潤的錯誤";
$MESS["SAA_ERROR_CLEAR"] = "清除時發生了錯誤";
$MESS["SAA_ERROR_DELETE"] = "錯誤刪除會員";
$MESS["SAA_ERROR_PAY"] = "錯誤處理會員盈利付款";
$MESS["SAA_ERROR_UPDATE"] = "更新會員的錯誤";
$MESS["SAA_GOTO_USER"] = "編輯用戶參數";
$MESS["SAA_INNER_PAY_AFF_EXT"] = "將未決金額發送給會員內部帳戶";
$MESS["SAA_LAST_CALCULATE"] = "最後計算";
$MESS["SAA_LAST_CALCULATE1"] = "上次計算：";
$MESS["SAA_NO"] = "不";
$MESS["SAA_NOTE_NOTE1"] = "行動：";
$MESS["SAA_NOTE_NOTE2"] = " - 從上次計算到現在，計算到期的會員金額。銷售決定會員計劃。";
$MESS["SAA_NOTE_NOTE3"] = "計算會員金額（擴展）";
$MESS["SAA_NOTE_NOTE4"] = " - 設置計算期以及確定會員計劃的銷售期。";
$MESS["SAA_NOTE_NOTE5"] = " - 將未決的金額轉換為保留金額。";
$MESS["SAA_NOTE_NOTE6"] = " - 將未決金額轉換為保留金額，並將其轉移到客戶內部帳戶。";
$MESS["SAA_NO_AFFILIATE"] = "找不到會員";
$MESS["SAA_PAR_AFFILIATE"] = "父母相關";
$MESS["SAA_PAYED_SUM"] = "支付的金額";
$MESS["SAA_PAY_AFF_EXT"] = "將未決金額發送給會員";
$MESS["SAA_PENDING_SUM"] = "等待金額";
$MESS["SAA_PLAN"] = "計劃";
$MESS["SAA_PLAN1"] = "計劃：";
$MESS["SAA_REG_DATE"] = "註冊日期";
$MESS["SAA_REG_DATE1"] = "註冊日期：";
$MESS["SAA_SITE"] = "地點";
$MESS["SAA_SITE1"] = "地點：";
$MESS["SAA_UPDATE_AFFILIATE"] = "編輯會員參數";
$MESS["SAA_USER"] = "用戶";
$MESS["SAA_USER1"] = "用戶：";
$MESS["SAA_USER_EMAIL"] = "用戶電子郵件地址";
$MESS["SAA_WAIT"] = "等待";
$MESS["SAA_YES"] = "是的";
