<?php
$MESS["MAIN_1CE_CATALOG_TAB"] = "出口目錄";
$MESS["MAIN_1CE_CATALOG_TAB_TITLE"] = "目錄導出參數";
$MESS["MAIN_1C_CATALOG_NOTE"] = "設置1C導出配置文件配置時，請使用以下URL作為加載數據的站點地址：http：//site/bitrix/admin/1c_exchange.php";
$MESS["MAIN_1C_CATALOG_TAB"] = "目錄";
$MESS["MAIN_1C_CATALOG_TAB_TITLE"] = "目錄導入設置";
$MESS["MAIN_1C_SALE_NOTE"] = "設置1C交換配置文件配置時，請使用以下URL作為交換訂單信息的站點地址：http：//site/bitrix/admin/1c_exchange.php";
$MESS["MAIN_1C_SALE_PROFILE_TAB"] = "交換配置文件";
$MESS["MAIN_1C_SALE_PROFILE_TITLE"] = "導出字段設置";
$MESS["MAIN_1C_SALE_TAB"] = "命令";
$MESS["MAIN_1C_SALE_TAB_TITLE"] = "訂購交換設置";
$MESS["MAIN_1C_TAB"] = "1C集成";
$MESS["MAIN_1C_TAB_TITLE"] = "1C集成設置";
$MESS["MAIN_1C_TITLE"] = "1C集成";
