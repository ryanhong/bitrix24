<?php
$MESS["ERROR_NO_HANDLER"] = "現金寄存器處理程序未指定";
$MESS["ERROR_NO_NAME"] = "公司名稱未指定";
$MESS["ERROR_NO_OFD"] = "未指定的OFD";
$MESS["SALE_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["SALE_CASHBOX_2FLIST"] = "返回目錄";
$MESS["SALE_CASHBOX_ACTIVE"] = "積極的";
$MESS["SALE_CASHBOX_CASHBOXCHECKBOX_HINT"] = "注意：此處理程序僅在烏克蘭工作。";
$MESS["SALE_CASHBOX_EDIT_RECORD"] = "編輯收銀機";
$MESS["SALE_CASHBOX_EMAIL"] = "電子郵件";
$MESS["SALE_CASHBOX_ERROR"] = "刪除對象時發生錯誤。";
$MESS["SALE_CASHBOX_EXTERNAL_UUID"] = "外現金寄存器ID";
$MESS["SALE_CASHBOX_FOR_UA"] = "（烏克蘭）";
$MESS["SALE_CASHBOX_HANDLER"] = "處理程序";
$MESS["SALE_CASHBOX_KKM_ID"] = "收銀機品牌";
$MESS["SALE_CASHBOX_KKM_NO_CHOOSE"] = "未選中的";
$MESS["SALE_CASHBOX_NAME"] = "姓名";
$MESS["SALE_CASHBOX_NEW_RECORD"] = "創建收銀機";
$MESS["SALE_CASHBOX_NO_HANDLER"] = "選擇處理程序";
$MESS["SALE_CASHBOX_OFD"] = "OFD";
$MESS["SALE_CASHBOX_OTHER_HANDLER"] = "其他";
$MESS["SALE_CASHBOX_RDL_RESTRICTION"] = "限制";
$MESS["SALE_CASHBOX_RDL_SAVE"] = "節省";
$MESS["SALE_CASHBOX_RDL_SORT"] = "種類";
$MESS["SALE_CASHBOX_RESTRICTION"] = "限制";
$MESS["SALE_CASHBOX_RESTRICTION_DESC"] = "收銀機限制";
$MESS["SALE_CASHBOX_SORT"] = "種類";
$MESS["SALE_CASHBOX_TAB_TITLE_OFD_SETTINGS"] = "OFD設置";
$MESS["SALE_CASHBOX_TAB_TITLE_OFD_SETTINGS_DESC"] = "OFD設置";
$MESS["SALE_CASHBOX_TAB_TITLE_SETTINGS"] = "設定";
$MESS["SALE_CASHBOX_TAB_TITLE_SETTINGS_DESC"] = "現金寄存器參數";
$MESS["SALE_CASHBOX_USE_OFFLINE"] = "離線模式";
$MESS["SALE_DELETE_CASHBOX"] = "刪除收銀機";
$MESS["SALE_NEW_CASHBOX"] = "添加收銀機";
$MESS["SALE_TAB_CASHBOX"] = "收銀機";
$MESS["SALE_TAB_CASHBOX_DESCR"] = "現金寄存器參數";
$MESS["SPSN_DELETE_CASHBOX_CONFIRM"] = "您確定要刪除收銀機嗎？";
