<?php
$MESS["ERROR_ADD_PROP"] = "添加屬性的錯誤。";
$MESS["ERROR_ADD_VARIANT"] = "添加屬性的錯誤";
$MESS["ERROR_EDIT_PROP"] = "錯誤修改屬性。";
$MESS["ERROR_EDIT_VARIANT"] = "錯誤修改值";
$MESS["F_ACTIVE"] = "積極的";
$MESS["F_ANOTHER_LOCATION"] = "替代位置輸入字段";
$MESS["F_CODE"] = "瘋子代碼";
$MESS["F_DEFAULT_VALUE"] = "預設值";
$MESS["F_DESCRIPTION"] = "屬性說明";
$MESS["F_INPUT_FIELD_DESCR"] = "該屬性將用作輔助位置輸入字段。";
$MESS["F_IS_ADDRESS"] = "是地址";
$MESS["F_IS_EMAIL"] = "用作電子郵件";
$MESS["F_IS_EMAIL_DESCR"] = "註冊新客戶時，屬性價值將用作電子郵件";
$MESS["F_IS_FILTERED"] = "屬性可在訂單過濾器中使用：";
$MESS["F_IS_LOCATION"] = "用作位置";
$MESS["F_IS_LOCATION4TAX"] = "用作稅收位置";
$MESS["F_IS_LOCATION4TAX_DESCR"] = "屬性價值將用作計算稅收的客戶位置（僅適用於位置類型的屬性）";
$MESS["F_IS_LOCATION_DESCR"] = "計算運輸成本時（僅適用於位置類型屬性），屬性價值將用作客戶位置";
$MESS["F_IS_PAYER"] = "用作付款人名稱";
$MESS["F_IS_PAYER_DESCR"] = "物業價值將用作付款人名稱";
$MESS["F_IS_PHONE"] = "是電話";
$MESS["F_IS_PROFILE_NAME"] = "用作個人資料名稱";
$MESS["F_IS_PROFILE_NAME_DESCR"] = "屬性值將用作用戶配置文件名稱";
$MESS["F_IS_ZIP"] = "用作拉鍊";
$MESS["F_IS_ZIP_DESCR"] = "該值將用作客戶的郵政編碼來計算交貨成本。";
$MESS["F_NAME"] = "姓名";
$MESS["F_PROPS_GROUP_ID"] = "財產組";
$MESS["F_SORT"] = "排序索引";
$MESS["F_USER_PROPS"] = "包括在個人資料中";
$MESS["F_UTIL"] = "服務";
$MESS["F_XML_ID"] = "外部ID";
$MESS["MULTIPLE_DESCRIPTION"] = "過濾器中不可用多個值";
$MESS["NO_DEFAULT_VALUE"] = "不明確的";
$MESS["NULL_ANOTHER_LOCATION"] = "選擇";
$MESS["PROPERTY_TITLE"] = "財產";
$MESS["SALE_ADD"] = "添加";
$MESS["SALE_APPLY"] = "申請";
$MESS["SALE_EDIT_RECORD"] = "編輯屬性＃＃ID＃";
$MESS["SALE_NEW_RECORD"] = "新物業";
$MESS["SALE_PERS_TYPE"] = "付款人類型";
$MESS["SALE_PROPERTIES"] = "參數";
$MESS["SALE_PROPERTY_DELIVERY"] = "送貨服務";
$MESS["SALE_PROPERTY_LINKING"] = "屬性鏈接";
$MESS["SALE_PROPERTY_LINKING_DESC"] = "將物業鏈接到支付系統和交付服務";
$MESS["SALE_PROPERTY_PAYSYSTEM"] = "支付系統";
$MESS["SALE_PROPERTY_SELECT_ALL"] = "全部";
$MESS["SALE_PROPERTY_TP"] = "訂單源";
$MESS["SALE_PROPERTY_TP_LANDING"] = "著陸";
$MESS["SALE_PROPS_GROUP"] = "財產組";
$MESS["SALE_RECORDS_LIST"] = "到屬性清單";
$MESS["SALE_RESET"] = "重置";
$MESS["SALE_SAVE"] = "節省";
$MESS["SALE_VARIANTS_CODE"] = "代碼";
$MESS["SALE_VARIANTS_DEL"] = "刪除";
$MESS["SALE_VARIANTS_DESCR"] = "描述";
$MESS["SALE_VARIANTS_MORE"] = "更多的...";
$MESS["SALE_VARIANTS_NAME"] = "姓名";
$MESS["SALE_VARIANTS_SORT"] = "種類。";
$MESS["SALE_VARIANTS_XML_ID"] = "外部ID";
$MESS["SOPEN_2FLIST"] = "訂單屬性列表";
$MESS["SOPEN_4NEW_PROMT"] = "選擇付款人類型：";
$MESS["SOPEN_DELETE_PROPS"] = "刪除屬性";
$MESS["SOPEN_DELETE_PROPS_CONFIRM"] = "您確定要刪除此屬性嗎？";
$MESS["SOPEN_NEW_PROPS"] = "創建一個新屬性";
$MESS["SOPEN_TAB_PROPS"] = "訂單屬性";
$MESS["SOPEN_TAB_PROPS_DESCR"] = "訂購“＃ptype＃＆quot”的訂單屬性付款人類型";
$MESS["SOPE_TO_LIST"] = "特性";
$MESS["TYPE_TITLE"] = "數據類型";
