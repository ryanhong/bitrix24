<?php
$MESS["SALE_EBAY_AUTH_FAIL"] = "授權錯誤";
$MESS["SALE_EBAY_CLOSE"] = "關閉";
$MESS["SALE_EBAY_GET_TOKEN_FAIL"] = "接收令牌的錯誤";
$MESS["SALE_EBAY_TITLE"] = "電子灣設置";
$MESS["SALE_EBAY_YOUR_TOKEN"] = "您的令牌（在\“身份驗證令牌\”字段中復制和粘貼）";
$MESS["SALE_EBAY_YOUR_TOKEN_EXP"] = "有效直到為止";
