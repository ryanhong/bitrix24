<?php
$MESS["SALE_CASHBOX_ORANGE_DATA_ERROR_CHECK_CHECK"] = "錯誤檢查收據狀態";
$MESS["SALE_CASHBOX_ORANGE_DATA_ERROR_CHECK_PRINT"] = "錯誤打印收據";
$MESS["SALE_CASHBOX_ORANGE_DATA_ERROR_CURL_ERROR"] = "收銀機需要捲曲庫。請安裝此庫";
$MESS["SALE_CASHBOX_ORANGE_DATA_ERROR_RESPONSE_400"] = "提交的數據具有驗證錯誤或簽名驗證失敗";
$MESS["SALE_CASHBOX_ORANGE_DATA_ERROR_RESPONSE_401"] = "客戶證書驗證失敗";
$MESS["SALE_CASHBOX_ORANGE_DATA_ERROR_RESPONSE_409"] = "此ID的收據已經存在";
$MESS["SALE_CASHBOX_ORANGE_DATA_ERROR_RESPONSE_503"] = "文檔隊列溢出";
$MESS["SALE_CASHBOX_ORANGE_DATA_ERROR_SEND_QUERY"] = "eror發送請求。請檢查連接設置";
$MESS["SALE_CASHBOX_ORANGE_DATA_ERROR_SIGN"] = "數據簽名錯誤";
$MESS["SALE_CASHBOX_ORANGE_DATA_MODE_ACTIVE"] = "真實的";
$MESS["SALE_CASHBOX_ORANGE_DATA_MODE_TEST"] = "測試";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_CLIENT"] = "客戶信息";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_CLIENT_DEFAULT"] = "預設";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_CLIENT_EMAIL"] = "電子郵件";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_CLIENT_INFO"] = "客戶信息傳遞到橙色數據";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_CLIENT_PHONE"] = "電話";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_INTERACTION"] = "Interop設置";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_MODE_HANDLER_LABEL"] = "收銀機模式";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_SECURITY"] = "安全設定";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_SECURITY_KEY_SIGN"] = "簽名驗證鍵";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_SECURITY_PKEY"] = "私人簽名密鑰（.pem）";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_SECURITY_SSL_CERT"] = "客戶證書";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_SECURITY_SSL_KEY"] = "SSL私鑰";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_SECURITY_SSL_KEY_PASS"] = "SSL私鑰密碼";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_SERVICE"] = "公司信息";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_SERVICE_INN_LABEL"] = "納稅人ID";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_SNO"] = "稅收";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_SNO_LABEL"] = "稅制";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_VAT"] = "稅率參數";
$MESS["SALE_CASHBOX_ORANGE_DATA_SETTINGS_VAT_LABEL_NOT_VAT"] = "無稅[默認]";
$MESS["SALE_CASHBOX_ORANGE_DATA_SNO_ENVD"] = "對估計收入的單一稅";
$MESS["SALE_CASHBOX_ORANGE_DATA_SNO_ESN"] = "統一農業稅";
$MESS["SALE_CASHBOX_ORANGE_DATA_SNO_OSN"] = "標準";
$MESS["SALE_CASHBOX_ORANGE_DATA_SNO_PATENT"] = "專利";
$MESS["SALE_CASHBOX_ORANGE_DATA_SNO_UI"] = "簡化";
$MESS["SALE_CASHBOX_ORANGE_DATA_SNO_UIO"] = "簡化（收入減少費用）";
$MESS["SALE_CASHBOX_ORANGE_DATA_TITLE"] = "橙色數據";
