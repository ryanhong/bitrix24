<?php
$MESS["SALE_CASHBOX_CHECK_DELIVERY"] = "送貨";
$MESS["SALE_CASHBOX_CHECK_ERROR_CHECK_SUM"] = "錯誤創建收據：收據金額與產品總數不匹配";
$MESS["SALE_CASHBOX_CHECK_ERROR_NO_BUYER_INFO"] = "錯誤創建收據：丟失客戶聯繫信息";
$MESS["SALE_CASHBOX_CHECK_ERROR_NO_NOMENCLATURE_CODE"] = "沒有為＃product_name＃指定的標籤";
$MESS["SALE_CASHBOX_CHECK_ERROR_NO_PRODUCTS"] = "錯誤創建收據：空運產品列表";
