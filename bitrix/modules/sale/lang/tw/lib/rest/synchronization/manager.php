<?php
$MESS["MAN_ERROR_CATALOGS"] = "系統中沒有商業目錄";
$MESS["MAN_ERROR_EMPTY_FIELD_DELIVERY_SERVICES"] = "未指定默認交貨服務";
$MESS["MAN_ERROR_EMPTY_FIELD_DELIVERY_STATUS"] = "未指定默認交貨狀態";
$MESS["MAN_ERROR_EMPTY_FIELD_ORDER_STATUS"] = "未指定默認訂單狀態";
$MESS["MAN_ERROR_EMPTY_FIELD_PAY_SYSTEM"] = "未指定默認支付系統";
$MESS["MAN_ERROR_EMPTY_FIELD_PERSON_TYPE"] = "未指定默認付款人類型";
$MESS["MAN_ERROR_EMPTY_FIELD_SITE"] = "未指定默認網站";
