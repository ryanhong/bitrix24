<?php
$MESS["TRADING_PLATFORM_SFTP_ERROR_CONNECT"] = "無法使用\“＃主機＃\”：\“＃port＃建立SSH連接";
$MESS["TRADING_PLATFORM_SFTP_ERROR_FINGERPRINT"] = "無效＃主機＃指紋！主機指紋可能是合法變化的，也可能是由於“中間人”攻擊的結果。當前主機指紋：\“＃fingerprint1＃\”，預期：\“＃fingerprint2＃\”。";
$MESS["TRADING_PLATFORM_SFTP_ERROR_INIT"] = "無法初始化SFTP子系統";
$MESS["TRADING_PLATFORM_SFTP_ERROR_OPEN_FILE"] = "無法打開文件\“＃文件＃\”";
$MESS["TRADING_PLATFORM_SFTP_ERROR_OPEN_PATH"] = "無法打開路徑\“＃路徑＃\”";
$MESS["TRADING_PLATFORM_SFTP_ERROR_PASS"] = "您的登錄或密碼不正確。";
$MESS["TRADING_PLATFORM_SFTP_ERROR_READ_FILE"] = "無法讀取文件\“＃文件＃\”";
$MESS["TRADING_PLATFORM_SFTP_ERROR_SSH2_EXT"] = "未安裝PHP SSH2擴展";
$MESS["TRADING_PLATFORM_SFTP_ERROR_WRITE_FILE"] = "無法寫入文件\“＃文件＃\”";
