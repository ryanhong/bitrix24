<?php
$MESS["EBAY_POLICY_REQUEST_HTTP_ERROR"] = "獲得策略數據時HTTP錯誤";
$MESS["SALE_EBAY_AT_AGENT_ADDING_RESULT"] = "數據交換代理安裝結果";
$MESS["SALE_EBAY_AT_AGENT_FEED_STARTED"] = "代理商發起的數據交換";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_ORDER_ALREADY_EXIST"] = "訂單已經存在";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_ORDER_CANCELING_ERROR"] = "錯誤取消訂單";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_ORDER_CHANGE_STATUS_ERROR"] = "錯誤更新訂單狀態";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_ORDER_CORR_SAVE_ERROR"] = "保存錯誤訂單文檔";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_ORDER_CREATED"] = "成功創建的順序";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_ORDER_CREATE_ERROR_SET_BASKET"] = "創建購物車的錯誤";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_ORDER_DEDUCTIOING_ERROR"] = "錯誤運輸訂單";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_ORDER_ERROR"] = "錯誤處理新訂單";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_ORDER_PROCESSED"] = "新訂單成功添加";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_ORDER_PROCESSING"] = "訂單處理開始";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_ORDER_PROCESSING_TRANSACTION_ITEM_SKU_NOT_FOUND"] = "沒有發現產品sku";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_ORDER_PROCESSING_TR_NOT_FOUND"] = "沒有新訂單中的交易數據";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_ORDER_SAVE_ERROR"] = "保存錯誤順序";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_ORDER_SKIPPED"] = "新訂單沒有處理";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_ORDER_TRANSACTION_ITEM_CREATE_ERROR"] = "錯誤添加產品";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_SFTPQUEUE_FLUSHING"] = "準備交換的數據";
$MESS["SALE_EBAY_AT_DATA_PROCESSOR_SFTPQUEUE_SEND"] = "文件已成功發送";
$MESS["SALE_EBAY_AT_DATA_SOURCE_ORDERFILE_RECEIVED"] = "收到的訂單文件";
$MESS["SALE_EBAY_AT_DATA_SOURCE_RESULTS_ERROR"] = "接收交換結果文件的錯誤";
$MESS["SALE_EBAY_AT_DATA_SOURCE_RESULTS_RECEIVED"] = "接收到的交換結果文件";
$MESS["SALE_EBAY_AT_FEED_CREATED"] = "數據交換開始";
$MESS["SALE_EBAY_AT_FEED_ERROR"] = "數據交換錯誤";
$MESS["SALE_EBAY_AT_FEED_RESULTS_ERROR"] = "錯誤處理數據交換結果文件";
$MESS["SALE_EBAY_AT_POLICY_REQUEST_ERROR"] = "錯誤獲取策略數據";
$MESS["SALE_EBAY_AT_SFTP_TOKEN_EXP"] = "SFTP令牌已過期";
$MESS["SALE_EBAY_AT_SFTP_TOKEN_EXP_MESSAGE"] = "SFTP令牌已過期。您必須在eBay.com上生成新的SFTP令牌。";
$MESS["SALE_EBAY_HLP_CHECK_ERROR_SIMPLEXML"] = "Simplexml不可用！";
$MESS["SALE_EBAY_HLP_CHECK_ERROR_SSH2"] = "SSH2不可用！";
$MESS["SALE_EBAY_HLP_EVNT_MSG"] = "與電子灣交換數據時發生了錯誤";
$MESS["SALE_EBAY_HLP_EVNT_MSG_INFO_SITE"] = "消息來自";
$MESS["SALE_EBAY_HLP_EVNT_MSG_SBUJ"] = "與電子灣交換數據的錯誤";
$MESS["SALE_EBAY_HLP_EVNT_TYPE_BCC"] = "BCC電子郵件";
$MESS["SALE_EBAY_HLP_EVNT_TYPE_ERROR"] = "e-bay錯誤";
$MESS["SALE_EBAY_HLP_EVNT_TYPE_ERROR_DETAIL"] = "錯誤描述";
$MESS["SALE_EBAY_HLP_EVNT_TYPE_ERROR_TYPE"] = "錯誤類型";
$MESS["SALE_EBAY_HLP_EVNT_TYPE_FROM"] = "發件人電子郵件";
$MESS["SALE_EBAY_HLP_EVNT_TYPE_TO"] = "收件人電子郵件";
