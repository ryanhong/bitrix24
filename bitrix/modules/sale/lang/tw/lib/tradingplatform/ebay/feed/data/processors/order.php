<?php
$MESS["SALE_TP_EBAY_FDPO_NOT_MAPPED_SHIPPING"] = "無法為訂單## order_id＃創建發貨，因為不存在用於交付類型\“＃ebay_shipping＃\”的映射。";
$MESS["SALE_TP_EBAY_FDPO_ORDER_ALLOW_DELIVERY_ERROR"] = "無法清除訂單## order_id＃用於交付";
$MESS["SALE_TP_EBAY_FDPO_ORDER_CANCEL_ERROR"] = "無法取消訂單## order_id＃";
$MESS["SALE_TP_EBAY_FDPO_ORDER_DEDUCT_ERROR"] = "無法將訂單## order_id＃標記為發貨";
$MESS["SALE_TP_EBAY_FDPO_ORDER_ERROR"] = "錯誤處理順序## order_id＃＃";
$MESS["SALE_TP_EBAY_FDPO_ORDER_PROCESSED"] = "訂單## order_id＃已成功處理";
$MESS["SALE_TP_EBAY_FDPO_ORDER_SAVED"] = "訂單## order_id＃已成功保存。";
$MESS["SALE_TP_EBAY_FDPO_ORDER_SET_STATUS_ERROR"] = "無法將順序## order_id＃的狀態更改為\“＃status \”。";
$MESS["SALE_TP_EBAY_FDPO_ORDER_SKIPPED"] = "訂單## order_id＃未經處理，因為它是未付的";
$MESS["SALE_TP_EBAY_FDPO_ORDER_SKIPPED_EXIST"] = "訂單## order_id＃未經處理，因為已經存在具有相同ID的訂單";
