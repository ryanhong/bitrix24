<?php
$MESS["SALE_VK_DESCRIPTION"] = "VK商店集成";
$MESS["SALE_VK_NAME"] = "VK產品";
$MESS["SALE_VK_ONLY_RUSSIAN"] = "出口到VK僅在俄羅斯可用。";
$MESS["SALE_VK_ONLY_RUSSIAN_2"] = "如果要配置Exchange，請使用頂部菜單更改站點語言。";
