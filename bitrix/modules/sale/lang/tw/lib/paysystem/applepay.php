<?php
$MESS["SALE_APPLE_PAY_HTTP_STATUS_CODE"] = "錯誤獲得響應。狀態：＃http_status＃";
$MESS["SALE_APPLE_PAY_HTTP_STATUS_MESSAGE"] = "錯誤獲得響應。狀態：＃http_status＃，消息：＃status_message＃";
$MESS["SALE_APPLE_PAY_LINE_ITEM_DISCOUNT"] = "折扣";
$MESS["SALE_APPLE_PAY_LINE_ITEM_ORDER"] = "訂單＃order_id＃";
$MESS["SALE_APPLE_PAY_LINE_ITEM_SHIPPING"] = "送貨";
$MESS["SALE_APPLE_PAY_LINE_ITEM_TAX"] = "稅";
$MESS["SALE_APPLE_PAY_LINE_ITEM_TOTAL"] = "全部的";
$MESS["SALE_APPLE_PAY_ORDER_SUBTITLE"] = "訂單＃order_id＃for＃sum＃";
