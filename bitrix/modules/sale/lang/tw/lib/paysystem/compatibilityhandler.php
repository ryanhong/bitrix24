<?php
$MESS["SALE_COMPATIBILITY_ACCOUNTANT_NAME"] = "威廉·史密斯";
$MESS["SALE_COMPATIBILITY_ACCOUNTANT_POSITION"] = "首席財務官";
$MESS["SALE_COMPATIBILITY_BANK_ADDRESS"] = "美國";
$MESS["SALE_COMPATIBILITY_BANK_CITY"] = "紐約";
$MESS["SALE_COMPATIBILITY_BANK_NAME"] = "Bitrix24";
$MESS["SALE_COMPATIBILITY_BASKET_ITEM_MEASURE"] = "件。";
$MESS["SALE_COMPATIBILITY_BASKET_ITEM_NAME"] = "測試";
$MESS["SALE_COMPATIBILITY_BUYER_COMPANY_ADDRESS"] = "紐約";
$MESS["SALE_COMPATIBILITY_BUYER_COMPANY_NAME"] = "公司";
$MESS["SALE_COMPATIBILITY_BUYER_NAME_CONTACT"] = "公司";
$MESS["SALE_COMPATIBILITY_COMMENT1"] = "在這裡評論";
$MESS["SALE_COMPATIBILITY_COMMENT2"] = "在這裡添加可選評論";
$MESS["SALE_COMPATIBILITY_COMPANY_NAME"] = "Bitrix";
$MESS["SALE_COMPATIBILITY_DIRECTOR_NAME"] = "約翰·多伊";
$MESS["SALE_COMPATIBILITY_DIRECTOR_POSITION"] = "CEO";
$MESS["SALE_COMPATIBILITY_TAX"] = "稅";
