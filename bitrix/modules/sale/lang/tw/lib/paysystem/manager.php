<?php
$MESS["SALE_PS_MANAGER_GROUP_COLUMN"] = "列";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_ADYEN"] = "Adyen連接參數";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_ALFABANK"] = "阿爾法銀行連接參數";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_APPLE_PAY"] = "Apple Pay連接參數";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_ASSIST"] = "協助連接參數";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_AUTHORIZE"] = "授權.net連接參數";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_BEPAID"] = "Bepaid連接設置";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_BILL"] = "發票首選項";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_BILLDE"] = "發票偏好（德語）";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_BILLEN"] = "發票偏好（英語）";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_BILLLA"] = "發票偏好（西班牙語）";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_BILLUA"] = "發票偏好（烏克蘭人）";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_LIQPAY"] = "LIQPAY連接參數";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_PAYMASTER"] = "Paymaster連接參數";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_PAYPAL"] = "貝寶連接參數";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_PLATON"] = "柏拉圖連接參數";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_QIWI"] = "QIWI連接參數";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_ROBOXCHANGE"] = "RoboxChange連接參數";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_SBERBANK"] = "Sberbank連接設置";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_SINARA"] = "Sinara銀行連接參數";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_SKB"] = "SKB銀行連接設置";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_UAPAY"] = "UAPAY連接設置";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_WEBMONEY"] = "Yandex連接參數";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_WOOPPAY"] = "WoopPay連接參數";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_YANDEX"] = "Yoomoney連接參數";
$MESS["SALE_PS_MANAGER_GROUP_CONNECT_SETTINGS_YANDEX_INVOICE"] = "Yoomoney連接參數";
$MESS["SALE_PS_MANAGER_GROUP_FOOTER_SETTINGS"] = "文檔頁腳參數";
$MESS["SALE_PS_MANAGER_GROUP_GENERAL_SETTINGS"] = "一般的";
$MESS["SALE_PS_MANAGER_GROUP_HEADER_SETTINGS"] = "文檔標題參數";
$MESS["SALE_PS_MANAGER_GROUP_PAYMENT"] = "付款";
$MESS["SALE_PS_MANAGER_GROUP_PAYSYSTEM"] = "支付系統";
$MESS["SALE_PS_MANAGER_GROUP_PS_OTHER"] = "其他屬性";
$MESS["SALE_PS_MANAGER_GROUP_VISUAL"] = "樣式";
$MESS["SALE_PS_MANAGER_INNER_NAME"] = "內部帳戶";
