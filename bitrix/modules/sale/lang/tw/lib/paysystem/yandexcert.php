<?php
$MESS["YANDEX_CERT_ERR_ACCORDING_PKEY_TO_CERT"] = "證書與私鑰不匹配。";
$MESS["YANDEX_CERT_ERR_CN"] = "無法上傳證書，因為它是為其他商店發行的。";
$MESS["YANDEX_CERT_ERR_EXT"] = "無效的擴展。證書必須是.cer文件。";
$MESS["YANDEX_CERT_ERR_LOAD"] = "無法上傳證書。請再試一次。";
$MESS["YANDEX_CERT_ERR_NULL"] = "無效的擴展。證書必須是.cer文件。";
$MESS["YANDEX_CERT_ERR_SIZE"] = "由於文件太大而無法上傳證書。最大文件大小為2 kb。";
