<?php
$MESS["SALE_DLVR_REQ_BASE_ACTIONS_NOT_SUPPORT"] = "此交付服務處理程序不支持執行運輸訂單命令。";
$MESS["SALE_DLVR_REQ_BASE_CANCEL_REQUEST"] = "取消請求";
$MESS["SALE_DLVR_REQ_BASE_CREATE_NOT_SUPPORT"] = "該交付服務處理程序不支持運輸訂單。";
$MESS["SALE_DLVR_REQ_BASE_DELETE_NOT_SUPPORT"] = "該交付服務處理程序不支持刪除運輸訂單。";
$MESS["SALE_DLVR_REQ_BASE_SHIPMENT_ACTIONS_NOT_SUPPORT"] = "該交付服務處理程序不支持命令實際運輸運輸訂單。";
$MESS["SALE_DLVR_REQ_BASE_SHIPMENT_ADD_NOT_SUPPORT"] = "該交付服務處理程序不支持將運輸貨物添加到運輸訂單中。";
$MESS["SALE_DLVR_REQ_BASE_SHIPMENT_DELETE_NOT_SUPPORT"] = "該交付服務處理程序不支持運輸訂單中的刪除貨物。";
$MESS["SALE_DLVR_REQ_BASE_SHIPMENT_UPDATE_NOT_SUPPORTED"] = "該交付服務處理程序不支持運輸訂單中的更新貨物。";
$MESS["SALE_DLVR_REQ_BASE_SHIPMENT_VIEW_NOT_SUPPORT"] = "該交付服務處理程序不支持查看發貨內容。";
$MESS["SALE_DLVR_REQ_BASE_VIEW_NOT_SUPPORT"] = "此交付服務處理程序不支持查看運輸訂單內容。";
