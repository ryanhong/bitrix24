<?php
$MESS["SALE_DLVR_RSTR_BY_WEIGHT_DESCRIPT"] = "限制交貨服務到訂單運輸重量";
$MESS["SALE_DLVR_RSTR_BY_WEIGHT_MAX_WEIGHT"] = "最大重量（g）";
$MESS["SALE_DLVR_RSTR_BY_WEIGHT_MIN_WEIGHT"] = "最小重量（G）";
$MESS["SALE_DLVR_RSTR_BY_WEIGHT_NAME"] = "按重量";
