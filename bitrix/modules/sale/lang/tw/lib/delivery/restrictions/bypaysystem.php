<?php
$MESS["SALE_DLVR_RSTR_BY_PAYSYSTEM_DESCRIPT"] = "將交付服務限制為指定的支付系統。<br> <br> <br>僅在使用指定的付款系統時才可用。<br> <br>以防止其他交貨系統的行為可能發生變化保存此限制時，可以創建或刪除付款系統依賴關係。";
$MESS["SALE_DLVR_RSTR_BY_PAYSYSTEM_NAME"] = "按付款系統";
$MESS["SALE_DLVR_RSTR_BY_PAYSYSTEM_PRM_PS"] = "支付系統";
