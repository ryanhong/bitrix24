<?php
$MESS["SALE_DLVR_MNGR_DLV_SRVS"] = "送貨服務";
$MESS["SALE_DLVR_MNGR_ERR_DEL_IN_SHPMNTS_EXIST"] = "由於有訂單使用它，因此無法刪除此交付服務";
$MESS["SALE_DLVR_MNGR_ERR_DEL_IN_SHPMNTS_EXIST_CHLD"] = "由於兒童服務與一個或多個訂單一起使用，因此無法刪除此送貨服務";
