<?php
$MESS["SALE_EVENT_ON_BEFORE_SALESHIPMENTITEM_SET_FIELD_ERROR"] = "發貨現場價值設置的錯誤事件";
$MESS["SALE_SHIPMENT_ITEM_BARCODE_MORE_ITEM_QUANTITY"] = "條形碼的數量超過產品數量";
$MESS["SALE_SHIPMENT_ITEM_BASKET_ITEM_NOT_FOUND"] = "購物車項目## basket_item_id＃鏈接到發貨##貨運＃＃。";
$MESS["SALE_SHIPMENT_ITEM_BASKET_WRONG_BASKET_ITEM"] = "不正確的購物車對象";
$MESS["SALE_SHIPMENT_ITEM_ERR_QUANTITY_EMPTY"] = "＃basket_item_name＃的數量不能為零或小於零";
$MESS["SALE_SHIPMENT_ITEM_LESS_AVAILABLE_QUANTITY_2"] = "\“＃product_name＃\”的可用數量小於購物車中指定的數量。檢查該產品是否已添加到訂單的其他貨物中。";
$MESS["SALE_SHIPMENT_ITEM_MARKING_CODE_LESS_ITEM_QUANTITY"] = "並非所有針對\“＃product_name＃\”指定的條形碼。";
$MESS["SALE_SHIPMENT_ITEM_MARKING_CODE_LESS_ITEM_QUANTITY_LONG_2"] = "無法實現發貨，因為產品\“＃product_name＃\”不存在條形碼。請打開貨物編輯表格，添加條形碼，然後重試。";
$MESS["SALE_SHIPMENT_ITEM_SHIPMENT_ALREADY_SHIPPED_CANNOT_EDIT"] = "裝運已經完成。不可能改變。";
