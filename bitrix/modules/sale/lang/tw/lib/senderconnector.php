<?php
$MESS["sender_connector_buyer_all"] = "任何";
$MESS["sender_connector_buyer_from"] = "從";
$MESS["sender_connector_buyer_lastorderdate"] = "最後訂單日期：";
$MESS["sender_connector_buyer_n"] = "不";
$MESS["sender_connector_buyer_name"] = "電子商店 - 客戶";
$MESS["sender_connector_buyer_ordercnt"] = "總付費訂單：";
$MESS["sender_connector_buyer_ordersum"] = "總訂單數量：";
$MESS["sender_connector_buyer_site"] = "地點：";
$MESS["sender_connector_buyer_to"] = "到";
$MESS["sender_connector_buyer_y"] = "是的";
