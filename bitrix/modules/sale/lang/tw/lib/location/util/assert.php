<?php
$MESS["SALE_LOCATION_ASSERT_ARRAY_EXPECTED"] = "＃arg_name＃的值不是數組。";
$MESS["SALE_LOCATION_ASSERT_ARRAY_NOT_EMPTY_EXPECTED"] = "＃arg_name＃的值必須是一個非空數數組。";
$MESS["SALE_LOCATION_ASSERT_ARRAY_OF_INTEGER_NOT_NULL_EXPECTED"] = "＃arg_name＃的值不是積極整數的數組。";
$MESS["SALE_LOCATION_ASSERT_ARRAY_OF_STRING_NOT_NULL_EXPECTED"] = "＃arg_name＃的值不是一系列非空字符串。";
$MESS["SALE_LOCATION_ASSERT_EMPTY_ARGUMENT"] = "空的參數傳遞";
$MESS["SALE_LOCATION_ASSERT_EMPTY_ENUMERATION"] = "空的枚舉傳遞給了檢查方法。";
$MESS["SALE_LOCATION_ASSERT_INTEGER_EXPECTED"] = "＃arg_name＃的值必須是整數。";
$MESS["SALE_LOCATION_ASSERT_INTEGER_NONNEGATIVE_EXPECTED"] = "參數＃arg_name＃必須是一個非負整數。";
$MESS["SALE_LOCATION_ASSERT_INTEGER_NOTNULL_EXPECTED"] = "＃arg_name＃的值必須是一個正整數。";
$MESS["SALE_LOCATION_ASSERT_ITEM_NOT_IN_ENUMERATION"] = "＃arg_name＃的值不是枚舉值之一。";
$MESS["SALE_LOCATION_ASSERT_STRING_NOTNULL_EXPECTED"] = "＃arg_name＃的值必須是一個非零長度字符串。";
