<?php
$MESS["SALE_LOCATION_ADMIN_SEARCH_HELPER_ENTITY_INVALID_DBINDEX"] = "與位置相關的數據庫表中缺少一個或多個索引。這可能會降低位置的性能。建議您＃anchor_index_restore＃還原數據庫索引手動＃anchor_end＃。";
$MESS["SALE_LOCATION_ADMIN_SEARCH_HELPER_ENTITY_INVALID_SINDEX"] = "位置搜索索引已過時。如果您在輸入框中使用增量搜索，則結果可能很慢，結果無關。建議您＃anchor_index_restore＃手動還原索引＃anchor_end＃。";
