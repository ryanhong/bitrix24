<?php
$MESS["SALE_LOCATION_ADMIN_LOCATION_HELPER_DATABASE_FAILURE"] = "位置數據庫中存在錯誤。請＃anchor_import_url＃再次導入位置＃anchor_end＃或聯繫技術支持。";
$MESS["SALE_LOCATION_ADMIN_LOCATION_HELPER_ENTITY_NAME_EMPTY_ERROR"] = "需要位置名稱";
$MESS["SALE_LOCATION_ADMIN_LOCATION_HELPER_ENTITY_TYPE_ID_UNKNOWN_ERROR"] = "未知位置類型";
$MESS["SALE_LOCATION_ADMIN_LOCATION_HELPER_ENTITY_UNKNOWN_EXTERNAL_SERVICE_ID_ERROR"] = "未知的外部服務ID";
$MESS["SALE_MENU_LOCATION_THE_REST_OF"] = "其他項目 ...";
