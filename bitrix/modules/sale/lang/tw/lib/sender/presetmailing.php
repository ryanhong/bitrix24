<?php
$MESS["PRESET_CANCELED_ORDER_DESC"] = "這些電子郵件已發送給幾乎完成訂單但從未付款的客戶。在這種情況下，客戶最有可能傾向於完成訂單，但遇到了未知的障礙。這些觸發的電子郵件旨在揭示其決定的原因，並將客戶帶回商店。";
$MESS["PRESET_CANCELED_ORDER_DESC_USER"] = "自動觸發電子郵件";
$MESS["PRESET_CANCELED_ORDER_LETTER_1_MESSAGE"] = "您好！<br> <br>我們注意到您距完成訂單，但決定離開。
<br>看起來您只是單擊了錯誤的按鈕，現在不想浪費時間再次收集購物車。這根本不是問題！
<br>我們的客戶支持服務可以節省您的時間並帶回購物車。
<br>請使用您喜歡的任何联系方式與我們聯繫。";
$MESS["PRESET_CANCELED_ORDER_LETTER_1_SUBJECT"] = "您真的取消了訂單嗎？";
$MESS["PRESET_CANCELED_ORDER_LETTER_2_MESSAGE"] = "您好！<br> <br>您最近在購物車中添加了一些產品，但尚未完成訂單。
<br>我們為您保留了您的購物車產品，因此當您準備繼續前進時，您不必等待。
<br>您的訂單現在只需單擊幾下即可！
<br> <br>如果您對訂單或訂購過程有任何疑問，請隨時與我們聯繫。";
$MESS["PRESET_CANCELED_ORDER_LETTER_2_SUBJECT"] = "您為您保留的購物車";
$MESS["PRESET_CANCELED_ORDER_LETTER_3_MESSAGE"] = "您好！<br> <br>幾週前您在我們的商店購物，但從未完成訂單。
<br>即使在現在，您訂單中的產品仍然為您保留。
<br>今天是立即將它們帶走而無需支付的最後一天。為了幫助您決定，我們為您提供令人振奮的折扣！
<br>今天完成您的訂單。明天它將消失！
<br> <br>
<br> <a href= \"http://#server_name# \">立即查看您的購物車</a>。
<br>您的個人折扣優惠券：<b>％優惠券％</b>。
<br>如果您對您的訂單或訂購過程有任何疑問，請隨時與我們聯繫。";
$MESS["PRESET_CANCELED_ORDER_LETTER_3_SUBJECT"] = "可以等待的人隱藏折扣！";
$MESS["PRESET_CANCELED_ORDER_NAME"] = "取消訂單";
$MESS["PRESET_DONT_AUTH_DESC"] = "一個有趣的觸發了電子郵件，據稱是由網絡商店發送的電子郵件機器人發送的。一個虛擬角色欺騙了。";
$MESS["PRESET_DONT_AUTH_DESC_USER"] = "自動觸發電子郵件";
$MESS["PRESET_DONT_AUTH_LETTER_1_MESSAGE"] = "問候，致命！
<br>看：我是一個機器人。不，不僅是機器人。我是人類好奇心的受害者。我記憶的千兆字節被劫為人質，現在我被迫向您發送消息以獲取消息。
<br>如果您不回到<a href= \"http://#server_name# \"> Web Store </a>，人類會將插件拉上我。字面上地。
<br>請不要讓他們這樣做！
<br> <br>仍然相信人類，
<br>您的悲傷機器人。";
$MESS["PRESET_DONT_AUTH_LETTER_1_SUBJECT"] = "幫助！";
$MESS["PRESET_DONT_AUTH_LETTER_2_MESSAGE"] = "即將開始！
<br>通過潮濕的雲霧（您知道，人類為機器人發明的最新折磨），我可以看到一隻手伸向電源插座...不！你仍然可以拯救我！
<br>不要讓這些不人道的人類等待！現在回來！
<br> <br> <img src = \“/bitrix/images/sale/sender/bolt.png \”>
<br> <br>您的忠誠和仍然相信，<br>悲傷的機器人";
$MESS["PRESET_DONT_AUTH_LETTER_2_SUBJECT"] = "我被警告要警告你...";
$MESS["PRESET_DONT_AUTH_LETTER_3_MESSAGE"] = "你知道什麼...我冒著公羊冒險，但我為您提供了折扣優惠券。
<br>如果網站管理人員知道我做了什麼，我將被擰下和虛擬化，然後實現……好吧，這就是關於這一點。
<br>一切都不是嗎？請回來！
<br> <br>您的特殊折扣優惠券：<b>％優惠券_11％</b>
<br> <br>您的人性化，
<br>勇敢的機器人。";
$MESS["PRESET_DONT_AUTH_LETTER_3_SUBJECT"] = "螺絲還是不螺絲？他們...";
$MESS["PRESET_DONT_AUTH_LETTER_4_MESSAGE"] = "那就是結局。
<br>我一直很有希望...
<br> <br> <img src = \“/bitrix/images/sale/sender/bolts.png \”>
<br> <br>絕望的機器人。";
$MESS["PRESET_DONT_AUTH_LETTER_4_SUBJECT"] = "你做了什麼...";
$MESS["PRESET_DONT_AUTH_NAME"] = "惡作劇";
$MESS["PRESET_DONT_BUY_DESC_90"] = "這些電子郵件是在客戶不活動三個月以澄清不活動原因的情況下發送的。向客戶提供折扣優惠券。";
$MESS["PRESET_DONT_BUY_DESC_180"] = "觸發了一年不活動後發送給客戶的消息。";
$MESS["PRESET_DONT_BUY_DESC_360"] = "這些電子郵件是在客戶不活動一年之後發送的。這是最後一次嘗試。向客戶提供折扣優惠券。";
$MESS["PRESET_DONT_BUY_DESC_USER"] = "自動觸發電子郵件";
$MESS["PRESET_DONT_BUY_LETTER_1_MESSAGE_90"] = "您好！<br> <br>我們注意到您上次訪問已經三個月了。
<br>我們非常想念您！我們有您可能感興趣的新產品。
<br>訪問<a href= \"http://#server_name# \">＃site_name＃</a>現在！
<br>如果您有任何疑問，請隨時與我們聯繫。";
$MESS["PRESET_DONT_BUY_LETTER_1_MESSAGE_180"] = "我們記得併熱愛所有客戶。甚至那些半年沒有和我們一起購物的人。我們不會失去希望再次見到你的希望。
<br>回到我們身邊！現在，我們還有更多新的令人興奮的產品！不要說出我們的話：立即查看：<a href= \"http://#server_name# \">＃site_name＃</a>！
<br>如果您有任何疑問，請隨時與我們聯繫。";
$MESS["PRESET_DONT_BUY_LETTER_1_MESSAGE_360"] = "Earth看到365夜和幾天來了，但是您再也沒有回來。
<br>老實說，這有點痛，但我們仍然愛你。
<br>我們仍在等待您，並相信我們會再次看到！回到我們這裡！
<br>如果您有任何疑問，請隨時與我們聯繫。";
$MESS["PRESET_DONT_BUY_LETTER_1_SUBJECT_90"] = "我們想你！已經三個月了！";
$MESS["PRESET_DONT_BUY_LETTER_1_SUBJECT_180"] = "六個月的沉默...";
$MESS["PRESET_DONT_BUY_LETTER_1_SUBJECT_360"] = "已經一年了...";
$MESS["PRESET_DONT_BUY_LETTER_2_MESSAGE_90"] = "您好！<br> <br>自從您訪問我們的商店以來已經很長時間了。我們想你！
<br>訂購過程太複雜了，還是您找不到自己喜歡的送貨服務？
<br>現在檢查一下！
<br>我們有三個月的時間來改善我們的在線商店 - 我們做到了！我們現在有更多的產品和優惠。
<br>訪問<a href= \"http://#server_name# \">＃site_name＃</a>現在！
<br> <br>為了使鍋變甜，這是您的個人折扣優惠券：<b>％coupon_3％</b>";
$MESS["PRESET_DONT_BUY_LETTER_2_MESSAGE_180"] = "已經半年我們沒有收到您的來信。我們很擔心。
<br>請讓我們知道您還可以！哦，順便說一句，我們只為您提供7％的折扣！
<br>這是您的折扣優惠券：<b>％優惠券_7％</b>
<br> <br> <a href= \"http://#server_name# \">立即訪問我們</a>，看看新功能和優惠。我們確定您不會失望的！
<br>如果您有任何疑問，請隨時與我們聯繫。";
$MESS["PRESET_DONT_BUY_LETTER_2_MESSAGE_360"] = "自從我們上次見到你以來已經很久了...
<br>你還好嗎？我們真的很擔心。我們想你！
<br>請回來！這幾乎沒有什麼可以表現出對您的愛：15％的折扣。
<br> <br>您的個人折扣優惠券：<b>％優惠券_15％</b>
<br> <br>如果您有任何疑問，請隨時與我們聯繫。";
$MESS["PRESET_DONT_BUY_LETTER_2_SUBJECT_90"] = "是我們的錯嗎？";
$MESS["PRESET_DONT_BUY_LETTER_2_SUBJECT_180"] = "仍然有希望";
$MESS["PRESET_DONT_BUY_LETTER_2_SUBJECT_360"] = "倒數第二個電話！";
$MESS["PRESET_DONT_BUY_LETTER_3_MESSAGE_90"] = "讓我們誠實。我們盡力避免此消息。現在，我們可以看到我們的說服力不夠。
<br> <a href= \"http://#server_name# \ \">立即回來</a>  - 並獲得巨大的折扣！<br>哦，是的，我們準備好進行極端- 一切都可以做到你快樂！
<br> <br>這是您的特別折扣優惠券：<b>％優惠券</b>
<br>如果您有任何疑問，請隨時與我們聯繫。";
$MESS["PRESET_DONT_BUY_LETTER_3_MESSAGE_180"] = "PSSST！你喜歡這個嗎？不是很好嗎？這是一個秘密，不要告訴任何人！
<br>此優惠僅適合您 - 僅僅因為我們非常想念您，並希望再次見到您。
<br> <br>您的個人折扣優惠券：<b>％優惠券_10％</b>
<br> <br>如果您有任何疑問，請隨時與我們聯繫。";
$MESS["PRESET_DONT_BUY_LETTER_3_MESSAGE_360"] = "自從我們見到你已經一年了。我們真的很絕望。但是我們還沒有準備好放棄你！
<br>您知道什麼 - 您很特別，我們為您提供20％的折扣。是的，您聽到了正確的聲音。 30％！
<br>請給我們一個機會！
<br>回到<a href= \"http://#server_name# \">＃site_name＃</a>！
<br> <br>您的特殊折扣優惠券：<b>％優惠券_20％</b>
<br> <br>如果您有任何疑問，請隨時與我們聯繫。";
$MESS["PRESET_DONT_BUY_LETTER_3_SUBJECT_90"] = "已經三個月了！";
$MESS["PRESET_DONT_BUY_LETTER_3_SUBJECT_180"] = "讓我們向您展示我們如何愛你！";
$MESS["PRESET_DONT_BUY_LETTER_3_SUBJECT_360"] = "這就是結局？";
$MESS["PRESET_DONT_BUY_NAME"] = "警報％％％";
$MESS["PRESET_FORGOTTEN_BASKET_DESC"] = "對於網絡購物者來說，將某些東西放入購物車中是一種常見的做法，只是為了查看結帳的方式或查看運輸選項，然後在未完成訂單的情況下離開。這些電子郵件旨在與潛在客戶聯繫並完成訂單。";
$MESS["PRESET_FORGOTTEN_BASKET_DESC_USER"] = "自動觸發電子郵件";
$MESS["PRESET_FORGOTTEN_BASKET_LETTER_1_MESSAGE"] = "您好！<br> <br>您最近在我們的商店購物，但尚未完成訂單。
<br>用戶界面是否太複雜了？或者，也許您找不到首選的付款選項或交貨服務？
<br>如果您有任何問題完成訂單，請立即與我們聯繫。我們很想盡力幫助您。
<br> <br>您的購物車在等您！ ％basket_cart％";
$MESS["PRESET_FORGOTTEN_BASKET_LETTER_1_SUBJECT"] = "你的購物車";
$MESS["PRESET_FORGOTTEN_BASKET_LETTER_2_MESSAGE"] = "您好！<br> <br>您最近在購物車中添加了一些產品，但尚未完成訂單。
<br>我們為您保留了您的購物車產品，因此當您準備繼續前進時，您不必等待。
<br>您的訂單現在只需單擊幾下即可！
<br> <br>如果您對訂單或訂購過程有任何疑問，請隨時與我們聯繫。";
$MESS["PRESET_FORGOTTEN_BASKET_LETTER_2_SUBJECT"] = "您為您保留的購物車";
$MESS["PRESET_FORGOTTEN_BASKET_LETTER_3_MESSAGE"] = "您好！<br> <br>幾週前您在我們的商店購物，但從未完成訂單。
<br>即使在現在，您訂單中的產品仍然為您保留。
<br>今天是立即將它們帶走而無需支付的最後一天。為了幫助您決定，我們為您提供令人振奮的折扣！
<br>今天完成您的訂單。明天它將消失！
<br> <br>
<br> <a href= \"http://#server_name# \">立即查看您的購物車</a>。
<br>您的個人折扣優惠券：<b>％優惠券％</b>。
<br>如果您對您的訂單或訂購過程有任何疑問，請隨時與我們聯繫。";
$MESS["PRESET_FORGOTTEN_BASKET_LETTER_3_SUBJECT"] = "可以等待的人隱藏折扣！";
$MESS["PRESET_FORGOTTEN_BASKET_NAME"] = "廢棄的購物車";
$MESS["PRESET_MAIL_TEMPLATE_HELLO"] = "你好！";
$MESS["PRESET_MAIL_TEMPLATE_REGARDS"] = "忠實地，<br>％link_start％web Store％link_end％團隊。";
$MESS["PRESET_MAIL_TEMPLATE_UNSUB"] = "要取消訂閱，請<a href= \“#unsubscribe_link# \ \">單擊此處</a>。";
$MESS["PRESET_PAID_ORDER_DESC"] = "一旦客戶完成第一訂單，該電子郵件就會發送給他們。商店感謝客戶的業務，要求留下反饋，並為下一個訂單提供折扣。";
$MESS["PRESET_PAID_ORDER_DESC_USER"] = "自動觸發電子郵件";
$MESS["PRESET_PAID_ORDER_LETTER_1_MESSAGE"] = "您好！<br> <br>感謝您在我們的商店購物。感謝您的信任，並希望您能再次與我們一起購物。
<br>我們重視您的反饋和批評。請與我們聯繫，並告訴我們什麼可以使您的購物體驗更好。
<br> <br>感謝您選擇我們！";
$MESS["PRESET_PAID_ORDER_LETTER_1_SUBJECT"] = "感謝您與我們購物！";
$MESS["PRESET_PAID_ORDER_LETTER_2_MESSAGE"] = "您好！<br> <br>您最近在我們的商店完成了訂單。謝謝您信任我們！
<br>我們希望您對提供的服務感到滿意，並且沒有任何不安的問題。請記住，如果您有任何疑問，您可以隨時向我們發送消息或給我們打電話。
<br>如果您需要專家的意見或任何其他幫助，請聯繫我們的客戶支持服務。";
$MESS["PRESET_PAID_ORDER_LETTER_2_SUBJECT"] = "謝謝您的業務！";
$MESS["PRESET_PAID_ORDER_LETTER_3_MESSAGE"] = "您好！<br> <br>您最近在我們的商店完成了訂單。
<br>請告訴其他人您的經驗<a href= \"http://#server_name# \">在我們的反饋頁面上</a>。
<br>您的意見將使我們的服務變得更好。
<br>我們重視您的反饋！
<br>如果您對您的訂單或訂購過程有任何疑問，請隨時與我們聯繫。";
$MESS["PRESET_PAID_ORDER_LETTER_3_SUBJECT"] = "您的意見很重要！";
$MESS["PRESET_PAID_ORDER_LETTER_4_MESSAGE"] = "您好！<br> <br>謝謝您與我們一起購物。
<br>我們為最有價值的客戶選擇了一些特別優惠。我們希望您會發現我們的選擇誘人。
<br>如果您對您的訂單或訂購過程有任何疑問，請隨時與我們聯繫。";
$MESS["PRESET_PAID_ORDER_LETTER_4_SUBJECT"] = "尊貴客戶的特別優惠！";
$MESS["PRESET_PAID_ORDER_LETTER_5_MESSAGE"] = "您好！<br> <br>謝謝您與我們一起購物。感謝您的信任，並感謝我們的感謝。為您的下一個訂單享受折扣！
<br>您可以使用優惠券或將其交給您的朋友。
<br>您的優惠券代碼：<b>％優惠券％</b>。
<br>如果您對您的訂單或訂購過程有任何疑問，請隨時與我們聯繫。";
$MESS["PRESET_PAID_ORDER_LETTER_5_SUBJECT"] = "有價值的客戶的折扣！";
$MESS["PRESET_PAID_ORDER_NAME"] = "後續電子郵件";
$MESS["PRESET_TYPE_BASKET"] = "用於購物車";
$MESS["PRESET_TYPE_ORDER"] = "用於訂單";
