<?php
$MESS["SALE_SRV_BASE_REST_ACCESS_SETTINGS_ERROR"] = "無法獲取配置文件數據。請確保系統配置正確，並且此URL可用：ssl：//saleservices.bitrix.info：443。";
$MESS["SALE_SRV_BASE_REST_ANSWER_ERROR"] = "無效的服務器響應";
$MESS["SALE_SRV_BASE_REST_CONNECT_ERROR"] = "無法連接到服務器";
