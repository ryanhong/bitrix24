<?php
$MESS["ORDERDELIVERY_ENTITY_DATE_REQUEST_FIELD"] = "提交的請求";
$MESS["ORDERDELIVERY_ENTITY_HID_FIELD"] = "送貨服務ID";
$MESS["ORDERDELIVERY_ENTITY_ID_FIELD"] = "ID";
$MESS["ORDERDELIVERY_ENTITY_LID_FIELD"] = "網站";
$MESS["ORDERDELIVERY_ENTITY_ORDER_ID_FIELD"] = "訂購ID";
$MESS["ORDERDELIVERY_ENTITY_PARAMS_FIELD"] = "其他參數";
$MESS["ORDERDELIVERY_ENTITY_SHIPMENT_ID_FIELD"] = "發貨ID";
