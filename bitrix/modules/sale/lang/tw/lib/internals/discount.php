<?php
$MESS["DISCOUNT_ENTITY_ACTIONS_LIST_FIELD"] = "動作";
$MESS["DISCOUNT_ENTITY_ACTIVE_FIELD"] = "積極的";
$MESS["DISCOUNT_ENTITY_ACTIVE_FROM_FIELD"] = "活躍";
$MESS["DISCOUNT_ENTITY_ACTIVE_TO_FIELD"] = "活躍直到";
$MESS["DISCOUNT_ENTITY_CONDITIONS_LIST_FIELD"] = "附加條件";
$MESS["DISCOUNT_ENTITY_CREATED_BY_FIELD"] = "由...製作";
$MESS["DISCOUNT_ENTITY_CURRENCY_FIELD"] = "貨幣";
$MESS["DISCOUNT_ENTITY_DATE_CREATE_FIELD"] = "創建於";
$MESS["DISCOUNT_ENTITY_EXECUTE_MODULE_FIELD"] = "執行模塊";
$MESS["DISCOUNT_ENTITY_ID_FIELD"] = "購物車規則ID";
$MESS["DISCOUNT_ENTITY_LAST_DISCOUNT_FIELD"] = "停止申請進一步規則";
$MESS["DISCOUNT_ENTITY_LID_FIELD"] = "網站ID";
$MESS["DISCOUNT_ENTITY_MODIFIED_BY_FIELD"] = "修改";
$MESS["DISCOUNT_ENTITY_NAME_FIELD"] = "姓名";
$MESS["DISCOUNT_ENTITY_PRESET_ID_FIELD"] = "規則預設";
$MESS["DISCOUNT_ENTITY_PRIORITY_FIELD"] = "申請的優先級";
$MESS["DISCOUNT_ENTITY_SITE_ID_FIELD"] = "網站ID";
$MESS["DISCOUNT_ENTITY_SORT_FIELD"] = "排序索引";
$MESS["DISCOUNT_ENTITY_TIMESTAMP_X_FIELD"] = "修改";
$MESS["DISCOUNT_ENTITY_USE_COUPONS_FIELD"] = "優惠券";
$MESS["DISCOUNT_ENTITY_VERSION_FIELD"] = "格式版本";
$MESS["DISCOUNT_ENTITY_XML_ID_FIELD"] = "購物車規則外部ID";
