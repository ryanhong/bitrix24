<?php
$MESS["DISCOUNT_ENTITIES_ENTITY_DISCOUNT_ID_FIELD"] = "購物車規則ID";
$MESS["DISCOUNT_ENTITIES_ENTITY_ENTITY_FIELD"] = "實體";
$MESS["DISCOUNT_ENTITIES_ENTITY_FIELD_ENTITY_FIELD"] = "實體字段";
$MESS["DISCOUNT_ENTITIES_ENTITY_FIELD_TABLE_FIELD"] = "表字段";
$MESS["DISCOUNT_ENTITIES_ENTITY_ID_FIELD"] = "ID";
$MESS["DISCOUNT_ENTITIES_ENTITY_MODULE_ID_FIELD"] = "模塊ID";
