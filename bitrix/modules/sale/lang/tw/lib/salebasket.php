<?php
$MESS["SALE_BASKET_ENTITY_BASKET"] = "購物車";
$MESS["SALE_BASKET_ENTITY_DATE_INS"] = "添加的項目";
$MESS["SALE_BASKET_ENTITY_DATE_INSERT"] = "添加的項目";
$MESS["SALE_BASKET_ENTITY_DATE_UPD"] = "項目更改";
$MESS["SALE_BASKET_ENTITY_DATE_UPDATE"] = "項目更改";
$MESS["SALE_BASKET_ENTITY_FUSER_ID"] = "訪客ID";
$MESS["SALE_BASKET_ENTITY_ID"] = "記錄ID";
$MESS["SALE_BASKET_ENTITY_NAME"] = "產品名稱";
$MESS["SALE_BASKET_ENTITY_N_SUBSCRIBE"] = "訂閱";
$MESS["SALE_BASKET_ENTITY_ORDER_ID"] = "訂購ID";
$MESS["SALE_BASKET_ENTITY_PRICE"] = "價格";
$MESS["SALE_BASKET_ENTITY_PRODUCT_ID"] = "產品ID";
$MESS["SALE_BASKET_ENTITY_QUANTITY"] = "數量";
$MESS["SALE_BASKET_ENTITY_SUBSCRIBE"] = "該產品有訂閱";
$MESS["SALE_BASKET_ENTITY_SUMMARY_PRICE"] = "產品總計";
