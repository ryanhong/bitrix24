<?php
$MESS["SALE_BASKET_AVAILABLE_FOR_ADDING_QUANTITY_IS_LESS"] = "＃數量＃\“＃product_name＃\”的PC已經添加到購物車中。您可以添加＃添加＃更多。";
$MESS["SALE_BASKET_AVAILABLE_FOR_ADDING_QUANTITY_IS_ZERO"] = "\“＃product_name＃\”的最大可用數量（＃數量＃）已經添加到購物車中。";
$MESS["SALE_BASKET_AVAILABLE_FOR_DECREASE_QUANTITY"] = "\“＃product_name＃\”的數量可用於扣除：＃可用_Quantity＃";
$MESS["SALE_BASKET_AVAILABLE_FOR_PURCHASE_QUANTITY"] = "可供購買的數量\“＃product_name＃\”";
$MESS["SALE_BASKET_ITEM_ERR_CURRENCY_EMPTY"] = "產品貨幣缺少";
$MESS["SALE_BASKET_ITEM_ERR_QUANTITY_ZERO"] = "product \“＃product_name＃\”數量不能為零或小於零";
$MESS["SALE_BASKET_ITEM_NOT_UPDATED_BECAUSE_NOT_EXISTS"] = "產品\“＃product_name＃\”沒有更新，因為它已較早刪除。";
$MESS["SALE_BASKET_ITEM_WRONG_AVAILABLE_QUANTITY_2"] = "\“＃product_name＃\”的庫存不足";
$MESS["SALE_BASKET_ITEM_WRONG_PRICE"] = "錯誤獲得\“＃product_name＃\”的價格";
$MESS["SALE_BASKET_PRODUCT_NOT_AVAILABLE"] = "\“＃product_name＃\”無庫存";
$MESS["SALE_BASKET_SERVICE_NOT_AVAILABLE"] = "服務\“＃product_name＃\”不可用";
