<?php
$MESS["SALE_HLP_ORDERBUILDER_PRICE_ERROR"] = "\“＃product_name＃\”價格不能為負";
$MESS["SALE_HLP_ORDERBUILDER_PRODUCT_NOT_AVILABLE"] = "\“＃名稱＃\”是不可用的。";
$MESS["SALE_HLP_ORDERBUILDER_QUANTITY_ERROR"] = "\“＃product_name＃\”的數量必須大於零";
$MESS["SALE_HLP_ORDERBUILDER_SHIPMENT_ITEM_ERROR"] = "錯誤將項目添加到發貨（＃id＃）";
