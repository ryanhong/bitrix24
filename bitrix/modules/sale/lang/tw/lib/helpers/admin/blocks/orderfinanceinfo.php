<?php
$MESS["SALE_ORDER_FINANCEINFO_BALANCE_INNER_BUDGET"] = "內部帳戶餘額";
$MESS["SALE_ORDER_FINANCEINFO_FOR_PAYMENT"] = "應付金額";
$MESS["SALE_ORDER_FINANCEINFO_PAYABLE"] = "應付金額";
$MESS["SALE_ORDER_FINANCEINFO_SUM_PAID"] = "有薪酬的";
$MESS["SALE_ORDER_FINANCEINFO_USE_INNER_BUDGET"] = "使用內部帳戶";
