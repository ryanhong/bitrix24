<?php
$MESS["SALE_ORDER_PAYMENT_NO_DELIVERY_SERVICE"] = "（選擇送貨服務）";
$MESS["SALE_ORDER_SHIPMENT_ADD_COMPANY"] = "沒有活躍的公司。態";
$MESS["SALE_ORDER_SHIPMENT_ADD_SHIPMENT"] = "添加貨物";
$MESS["SALE_ORDER_SHIPMENT_ALLOW_DELIVERY"] = "批准裝運";
$MESS["SALE_ORDER_SHIPMENT_ALLOW_DELIVERY_NO"] = "未批准的裝運";
$MESS["SALE_ORDER_SHIPMENT_ALLOW_DELIVERY_YES"] = "批准裝運";
$MESS["SALE_ORDER_SHIPMENT_APPLY"] = "申請";
$MESS["SALE_ORDER_SHIPMENT_BASKET"] = "貨單";
$MESS["SALE_ORDER_SHIPMENT_BLOCK_DELIVERY_ADDITIONAL"] = "送貨服務信息";
$MESS["SALE_ORDER_SHIPMENT_BLOCK_DELIVERY_INFO"] = "裝運信息";
$MESS["SALE_ORDER_SHIPMENT_BLOCK_EDIT_SHIPMENT_TITLE"] = "發貨## ID＃來自＃date_insert＃";
$MESS["SALE_ORDER_SHIPMENT_BLOCK_NEW_SHIPMENT_TITLE"] = "新裝運";
$MESS["SALE_ORDER_SHIPMENT_BLOCK_SERVICE"] = "服務";
$MESS["SALE_ORDER_SHIPMENT_BLOCK_SHIPMENT"] = "運輸";
$MESS["SALE_ORDER_SHIPMENT_BLOCK_SHIPMENT_DELETE"] = "刪除";
$MESS["SALE_ORDER_SHIPMENT_BLOCK_SHIPMENT_EDIT"] = "編輯";
$MESS["SALE_ORDER_SHIPMENT_BLOCK_SHIPMENT_TOGGLE"] = "坍塌";
$MESS["SALE_ORDER_SHIPMENT_BLOCK_SHIPMENT_TOGGLE_UP"] = "擴張";
$MESS["SALE_ORDER_SHIPMENT_BLOCK_STATUS"] = "狀態";
$MESS["SALE_ORDER_SHIPMENT_CHECK_ADD"] = "添加收據";
$MESS["SALE_ORDER_SHIPMENT_CHECK_ADD_WINDOW_TITLE"] = "添加新收據";
$MESS["SALE_ORDER_SHIPMENT_CHECK_CHECK_STATUS"] = "更新";
$MESS["SALE_ORDER_SHIPMENT_CHECK_LINK"] = "收據## check_id＃";
$MESS["SALE_ORDER_SHIPMENT_CHECK_LINK_TITLE"] = "收據";
$MESS["SALE_ORDER_SHIPMENT_CONFIRM_DELETE_SHIPMENT"] = "您確定要刪除貨物嗎？";
$MESS["SALE_ORDER_SHIPMENT_CONFIRM_SET_NEW_PRICE"] = "您確定要分配新的交付價格嗎？";
$MESS["SALE_ORDER_SHIPMENT_CONFIRM_SET_NEW_WEIGHT"] = "您確定要設置新的交付重量嗎？";
$MESS["SALE_ORDER_SHIPMENT_DEDUCTED"] = "運輸";
$MESS["SALE_ORDER_SHIPMENT_DEDUCTED_NO"] = "沒有發貨";
$MESS["SALE_ORDER_SHIPMENT_DEDUCTED_YES"] = "發貨";
$MESS["SALE_ORDER_SHIPMENT_DELIVERY_BLOCK_PRICE"] = "價格";
$MESS["SALE_ORDER_SHIPMENT_DELIVERY_BLOCK_WEIGHT"] = "重量";
$MESS["SALE_ORDER_SHIPMENT_DELIVERY_CHOOSE_DATE"] = "點擊選擇日期";
$MESS["SALE_ORDER_SHIPMENT_DELIVERY_DOC_DATE"] = "裝運文件日期";
$MESS["SALE_ORDER_SHIPMENT_DELIVERY_DOC_NUM"] = "裝運文件＃";
$MESS["SALE_ORDER_SHIPMENT_DELIVERY_DOC_STATUS"] = "文件狀態";
$MESS["SALE_ORDER_SHIPMENT_DELIVERY_SERVICE"] = "送貨服務";
$MESS["SALE_ORDER_SHIPMENT_DELIVERY_SERVICE_PROFILE"] = "輪廓";
$MESS["SALE_ORDER_SHIPMENT_DELIVERY_STATUS"] = "郵寄狀態";
$MESS["SALE_ORDER_SHIPMENT_DELIVERY_SUM_DISCOUNT_PRICE"] = "交貨較少的折扣";
$MESS["SALE_ORDER_SHIPMENT_DELIVERY_SUM_PRICE"] = "運輸成本";
$MESS["SALE_ORDER_SHIPMENT_DELIVERY_WEIGHT"] = "裝運重量";
$MESS["SALE_ORDER_SHIPMENT_DEL_REQ"] = "運輸令";
$MESS["SALE_ORDER_SHIPMENT_DEL_REQ_ERROR"] = "錯誤將裝運添加到運輸訂單";
$MESS["SALE_ORDER_SHIPMENT_DEL_REQ_INFO"] = "運輸訂單信息";
$MESS["SALE_ORDER_SHIPMENT_DEL_REQ_NAME"] = "運輸訂單\“＃request_id＃\”來自\“＃request_date＃\”";
$MESS["SALE_ORDER_SHIPMENT_DISCOUNT"] = "折扣";
$MESS["SALE_ORDER_SHIPMENT_ERROR_NO_DELIVERY_SERVICE"] = "未選擇送貨服務";
$MESS["SALE_ORDER_SHIPMENT_ERROR_ORDER_NOT_FOUND"] = "訂單不存在";
$MESS["SALE_ORDER_SHIPMENT_ERROR_SHIPMENT_NOT_FOUND"] = "沒有找到此ID的裝運";
$MESS["SALE_ORDER_SHIPMENT_ERROR_UNCORRECT_FORM_DATE"] = "不正確的日期格式";
$MESS["SALE_ORDER_SHIPMENT_HIDDEN"] = "[隱]";
$MESS["SALE_ORDER_SHIPMENT_MODIFY_BY"] = "修改";
$MESS["SALE_ORDER_SHIPMENT_NEW_PRICE_DELIVERY"] = "計算的交貨成本";
$MESS["SALE_ORDER_SHIPMENT_NEW_WEIGHT_DELIVERY"] = "估計的運輸重量";
$MESS["SALE_ORDER_SHIPMENT_NO_COMPANY"] = "未選中的";
$MESS["SALE_ORDER_SHIPMENT_OFFICE"] = "運輸辦公室";
$MESS["SALE_ORDER_SHIPMENT_PROFILE"] = "概況";
$MESS["SALE_ORDER_SHIPMENT_RECALCULATE_DELIVERY_PRICE"] = "重新計價的價格";
$MESS["SALE_ORDER_SHIPMENT_STORE_SELF_DELIVERY"] = "接送點";
$MESS["SALE_ORDER_SHIPMENT_TITLE"] = "運輸";
$MESS["SALE_ORDER_SHIPMENT_TRACKING_DESCRIPTION"] = "狀態描述";
$MESS["SALE_ORDER_SHIPMENT_TRACKING_LAST_CHANGE"] = "最後更新";
$MESS["SALE_ORDER_SHIPMENT_TRACKING_NUMBER"] = "追踪號碼";
$MESS["SALE_ORDER_SHIPMENT_TRACKING_STATUS"] = "運輸狀態";
$MESS["SALE_ORDER_SHIPMENT_TRACKING_STATUS_REFRESH"] = "更新";
$MESS["SALE_ORDER_SHIPMENT_TRACKING_S_EMPTY"] = "需要跟踪號碼";
$MESS["SALE_ORDER_SHIPMENT_TRACKING_URL"] = "包裝跟踪詳細信息";
