<?php
$MESS["SALE_EXCHANGE_EXTERNAL_ID_NOT_FOUND"] = "未提供文檔ID";
$MESS["SALE_EXCHANGE_PACKAGE_ERROR_ORDER_CANNOT_UPDATE"] = "錯誤將訂單與1C同步。該訂單無法更新。";
$MESS["SALE_EXCHANGE_PACKAGE_ERROR_ORDER_IS_NOT_LOADED"] = "錯誤加載文檔## document_id＃。找不到父母訂單以獲取依賴文件。";
$MESS["SALE_EXCHANGE_PACKAGE_ERROR_PAYMENT_IS_NOT_RELATED_TO_ORDER_OR_DELETED"] = "付款＃document_id＃未鏈接到任何現有訂單";
$MESS["SALE_EXCHANGE_PACKAGE_ERROR_PERSONAL_TYPE_IS_EMPTY"] = "無法通過配置文件＃document_id＃識別付款人類型";
$MESS["SALE_EXCHANGE_PACKAGE_ERROR_SHIPMENT_IS_NOT_RELATED_TO_ORDER_OR_DELETED"] = "貨運＃document_id＃未鏈接到任何現有訂單";
$MESS["SALE_EXCHANGE_PACKAGE_ERROR_USER_IS_EMPTY"] = "無法通過配置文件＃document_id＃識別用戶";
