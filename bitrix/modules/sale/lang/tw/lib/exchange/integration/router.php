<?php
$MESS["SALE_ROUTER_INTERNAL_ERROR"] = "內部應用程序錯誤";
$MESS["SALE_ROUTER_INTERNAL_ERROR_TITLE"] = "警告！出了些問題。";
$MESS["SALE_ROUTER_INTERNAL_SERVER_ERROR"] = "應用錯誤：＃錯誤＃";
$MESS["SALE_ROUTER_INTERNAL_SERVER_ERROR_AUHORIZATION"] = "身份驗證服務器錯誤：＃描述＃（＃錯誤＃）";
$MESS["SALE_ROUTER_INTERNAL_SERVER_ERROR_TOKEN_IS_COMPROMISED"] = "應用錯誤：令牌已被妥協。請重新安裝該應用程序。";
$MESS["SALE_ROUTER_INTERNAL_SERVER_ERROR_TOKEN_IS_NULL"] = "應用錯誤：缺少令牌";
$MESS["SALE_ROUTER_INTERNAL_SERVER_ERROR_WRONG_RESPONSE"] = "應用服務器錯誤";
$MESS["SALE_ROUTER_ORDER_NOT_FOUND"] = "<p>此對訂單綁定不再相關。該訂單可能已被刪除或綁定到另一筆交易。</p> <p>要與該訂單同步，刪除交易＆nbsp; <b> #placement_entity_id＃</b>，然後運行Deal incort Import Import import import import import嚮導嚮導。從交易中。</p>";
$MESS["SALE_ROUTER_PAGE_NOT_FOUND"] = "未知請求類型";
