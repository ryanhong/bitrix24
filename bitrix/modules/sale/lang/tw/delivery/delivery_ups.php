<?php
$MESS["SALE_DH_UPS_CONFIG_export_csv"] = "CSV文件帶有交付關稅";
$MESS["SALE_DH_UPS_CONFIG_zones_csv"] = "帶有送貨區域的CSV文件";
$MESS["SALE_DH_UPS_DESCRIPTION"] = "國際郵件";
$MESS["SALE_DH_UPS_DESCRIPTION_INNER"] = "UPS交付計算。使用UPS，Inc。提供的CSV文件中的交付價格報價。您可以從<a href= \"http://www.ups.com \" target= \"_blank \"> UPS網站下載任何國家/地區的CSV文件</a>並在交付服務屬性中指定其位置。必須在<a href= \"/bitrix/admin/settings.php?mid = sale＆lang=en \ \">模塊設置</a>中指定e-商店地址。";
$MESS["SALE_DH_UPS_EXPRESS_DESCRIPTION"] = "將下一個辦公室日至10.30或12.00提供到大多數歐洲地址。";
$MESS["SALE_DH_UPS_EXPRESS_SAVER_DESCRIPTION"] = "將下一個辦公時間交給大多數歐洲地址。";
$MESS["SALE_DH_UPS_EXPRESS_SAVER_TITLE"] = "UPS快速節省";
$MESS["SALE_DH_UPS_EXPRESS_TITLE"] = "UPS表達";
$MESS["SALE_DH_UPS_NAME"] = "UPS";
$MESS["SALE_DH_UPS_TARIFF_TITLE"] = "關稅";
