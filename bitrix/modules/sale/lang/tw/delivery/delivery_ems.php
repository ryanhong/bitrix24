<?php
$MESS["SALE_DH_EMS_CONFIG_CATEGORY"] = "裝運類型（全球交付）";
$MESS["SALE_DH_EMS_CONFIG_CATEGORY_att"] = "產品";
$MESS["SALE_DH_EMS_CONFIG_CATEGORY_doc"] = "文件（最多2公斤）";
$MESS["SALE_DH_EMS_CONFIG_TITLE"] = "參數";
$MESS["SALE_DH_EMS_CORRECT_CITIES"] = "城市更新。";
$MESS["SALE_DH_EMS_CORRECT_COUNTRIES"] = "國家更新。";
$MESS["SALE_DH_EMS_CORRECT_REGIONS"] = "更新區域。";
$MESS["SALE_DH_EMS_DAYS"] = "天";
$MESS["SALE_DH_EMS_DELIVERY_TITLE"] = "快速傳送";
$MESS["SALE_DH_EMS_DESCRIPTION"] = "快速傳送";
$MESS["SALE_DH_EMS_DESCRIPTION_INNER"] = "俄羅斯EMS職位的處理程序。僅從俄羅斯船隻。基於<a href= \之http://www.emspost.ru/corp_clients/dogovor_docements/api.php \" target = \"_blank \"> public ems ems api </a>。<br />要求商店地址將在<a href= \"/bitrix/admin/settings.php?mid = sale＆lang=ru \">模塊設置</a>中指定。";
$MESS["SALE_DH_EMS_ERROR_CONNECT"] = "無法計算交貨成本：連接錯誤。";
$MESS["SALE_DH_EMS_ERROR_NO_CITY_FROM"] = "送貨服務不會從＃City＃提供。";
$MESS["SALE_DH_EMS_ERROR_NO_CITY_TO"] = "送貨服務未交付到＃City＃。";
$MESS["SALE_DH_EMS_ERROR_NO_COUNTRY_TO"] = "送貨服務未交付給＃Country＃。";
$MESS["SALE_DH_EMS_ERROR_NO_LOCATION_TO"] = "送貨服務無法運送到這個位置";
$MESS["SALE_DH_EMS_ERROR_RESPONSE"] = "無法計算交付成本：錯誤的服務器響應。";
$MESS["SALE_DH_EMS_NAME"] = "俄羅斯的EMS職位";
