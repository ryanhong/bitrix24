<?php
$MESS["SALE_DH_RPF_DCL_VAL"] = "#VALUE!";
$MESS["SALE_DH_RPF_DEPRECATED_MESSAGE"] = "該交付服務處理程序已過時，不再支持。請使用＃A1＃新處理程序＃A2＃。";
$MESS["SALE_DH_RPF_DESCR"] = "#VALUE!";
$MESS["SALE_DH_RPF_FEATURE_ENABLED"] = "#VALUE!";
$MESS["SALE_DH_RPF_FEATURE_VALUE"] = "#VALUE!";
$MESS["SALE_DH_RPF_NAME"] = "#VALUE!";
$MESS["SALE_DH_RPF_OVERLOAD"] = "#VALUE!";
$MESS["SALE_DH_RPF_RGST_NTF"] = "#VALUE!";
$MESS["SALE_DH_RPF_SET_DEFAULT_TARIF"] = "默認關稅";
$MESS["SALE_DH_RPF_SET_DEFAULT_TARIF_SET"] = "放";
$MESS["SALE_DH_RPF_SMPL_NTF"] = "#VALUE!";
$MESS["SALE_DH_RPF_SRV_ALLOW"] = "#VALUE!";
$MESS["SALE_DH_RPF_SRV_PRICE"] = "#VALUE!";
$MESS["SALE_DH_RPF_STNRD_BOX"] = "#VALUE!";
$MESS["SALE_DH_RPF_TARIFS"] = "#VALUE!";
$MESS["SALE_DH_RPF_WRP_DESCR"] = "#VALUE!";
$MESS["SALE_DH_RPF_WRP_LESS_100"] = "#VALUE!";
$MESS["SALE_DH_RPF_WRP_LESS_100_DECLARED_VALUE"] = "小數據包高達100克，已聲明為價值。";
$MESS["SALE_DH_RPF_WRP_MORE_100"] = "#VALUE!";
$MESS["SALE_DH_RPF_WRP_TITLE"] = "#VALUE!";
