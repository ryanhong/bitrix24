<?php
$MESS["SALE_DH_SIMPLE_CONFIG_TITLE"] = "交貨價格";
$MESS["SALE_DH_SIMPLE_DESCRIPTION_INNER"] = "簡單的快遞服務。此服務要求至少存在一個位置組。配置服務時，請記住為每個位置組設置固定交付價格。要將組排除在處理之外，請將價格字段留為空。<br /> <a href= \"/bitrix/admin/sale_location_group_admin.php?lang= en \“target= \”_blank \">編輯位置組</ a>。";
$MESS["SALE_DH_SIMPLE_GROUP_PRICE"] = "分組";
$MESS["SALE_DH_SIMPLE_NAME"] = "快遞交付";
$MESS["SALE_DH_SIMPLE_SIMPLE_DESCRIPTION"] = "3天內";
$MESS["SALE_DH_SIMPLE_SIMPLE_TITLE"] = "送貨";
