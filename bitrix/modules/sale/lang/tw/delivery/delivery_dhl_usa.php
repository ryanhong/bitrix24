<?php
$MESS["SALE_DH_DHL_USA_CONFIG_DELIVERY_TITLE"] = "運輸信息";
$MESS["SALE_DH_DHL_USA_CONFIG_PACKAGE_TYPE"] = "包裝信息";
$MESS["SALE_DH_DHL_USA_CONFIG_PACKAGE_TYPE_CP"] = "您的包裝";
$MESS["SALE_DH_DHL_USA_CONFIG_PACKAGE_TYPE_EE"] = "DHL Express信";
$MESS["SALE_DH_DHL_USA_CONFIG_PACKAGE_TYPE_OD"] = "DHL其他包裝";
$MESS["SALE_DH_DHL_USA_DESCRIPTION"] = "DHL送貨服務（美國）";
$MESS["SALE_DH_DHL_USA_DESCRIPTION_INNER"] = "<a href= \"http://www.dhl-usa.com \“target= \"_blank \"> dhl（美國）的送貨服務服務</a>。基於在線計算器。 DHL（美國）從美國提供到世界各地的任何國家。需要在E商店模塊設置中指定郵政編碼。";
$MESS["SALE_DH_DHL_USA_ERROR_RESPONSE"] = "無法識別的服務器響應";
$MESS["SALE_DH_DHL_USA_NAME"] = "DHL（美國）";
$MESS["SALE_DH_DHL_USA_PROFILE_DESCRIPTION"] = "在1至4個辦公室內提供";
$MESS["SALE_DH_DHL_USA_PROFILE_TITLE"] = "送貨";
