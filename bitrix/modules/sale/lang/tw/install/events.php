<?php
$MESS["SALE_CHECK_PRINT_ERROR_HTML_SUB_TITLE"] = "歡迎！";
$MESS["SALE_CHECK_PRINT_ERROR_HTML_TEXT"] = "無法打印收據## check_id＃for Order ## order_account_number＃日期＃order_date＃。

單擊此處解決問題：
http：//#server_name#/bitrix/admin/sale_order_view.php？id =＃order_id＃＃
";
$MESS["SALE_CHECK_PRINT_ERROR_HTML_TITLE"] = "錯誤打印收據";
$MESS["SALE_CHECK_PRINT_ERROR_SUBJECT"] = "錯誤打印收據";
$MESS["SALE_CHECK_PRINT_ERROR_TYPE_DESC"] = "＃order_account_number＃ - 訂購ID
＃order_date＃ - 訂購日期
＃order_id＃ - 訂購ID
＃check_id＃ - 收據ID";
$MESS["SALE_CHECK_PRINT_ERROR_TYPE_NAME"] = "收據打印輸出錯誤通知";
$MESS["SALE_CHECK_PRINT_HTML_SUB_TITLE"] = "親愛的＃order_user＃，";
$MESS["SALE_CHECK_PRINT_HTML_TEXT"] = "您的付款已經處理過，並創建了各自的收據。要查看收據，請使用鏈接：

＃check_link＃

要獲取有關您訂單##訂單＃＃或＃order_date＃的更多詳細信息，請單擊以下鏈接：http：//＃server_name＃/personal/serment/order/derdy/derdy/＃order_account_number_encode＃/
";
$MESS["SALE_CHECK_PRINT_HTML_TITLE"] = "您使用＃site_name＃的訂單付款";
$MESS["SALE_CHECK_PRINT_SUBJECT"] = "收據鏈接";
$MESS["SALE_CHECK_PRINT_TYPE_DESC"] = "＃order_id＃ - 訂購ID
＃order_date＃ - 訂購日期
＃order_user＃ - 客戶
＃order_account_number_encode＃ - 訂購ID用於鏈接中
＃order_public_url＃ - 未經授權的用戶的訂單查看鏈接（需要在e -商店模塊設置中配置）
＃check_link＃ - 收據鏈接";
$MESS["SALE_CHECK_PRINT_TYPE_NAME"] = "收據打印輸出通知";
$MESS["SALE_CHECK_VALIDATION_ERROR_HTML_SUB_TITLE"] = "你好！";
$MESS["SALE_CHECK_VALIDATION_ERROR_HTML_TEXT"] = "
從＃order_date＃＃！

單擊此鏈接解決問題：
＃link_url＃
";
$MESS["SALE_CHECK_VALIDATION_ERROR_HTML_TITLE"] = "錯誤創建收據";
$MESS["SALE_CHECK_VALIDATION_ERROR_SUBJECT"] = "錯誤創建收據";
$MESS["SALE_CHECK_VALIDATION_ERROR_TYPE_DESC"] = "＃order_account_number＃ - 訂購＃
＃order_date＃ - 訂購日期
＃order_id＃ - 訂購ID";
$MESS["SALE_CHECK_VALIDATION_ERROR_TYPE_NAME"] = "收據創建錯誤通知";
$MESS["SALE_MAIL_EVENT_TEMPLATE"] = "<！doctype html public \“  -  // w3c // dtd html 4.01 transitional // en \” \“ http：//www.w.org/tr/tr/html4/loose.dtd \”>
<html xmlns = \“ http：//www.w3.org/1999/xhtml \” xml \“ xml：lang = \”
<頭>
<meta http-equiv = \“ content-type \” content = \“ text/html; charset =＃site_charset＃\”/>
<樣式>
身體
{
字體家庭：“ Helvetica Neue”，Helvetica，Arial，sans-serif;
字體大小：14px;
顏色：＃000;
}
</style>
</head>
<身體>
<table cellpadding = \“ 0 \” cellspacing = \“ 0 \” width = \“ 850 \” style = \“ backick-color：＃d1d1d1; border-d1d1; border-dius：2px; border：1px solid＃d1d1d1d1d1d1 ; margin：0 auto; \ \“ border = \” 1 \“ bordercolor = \”＃d1d1d1 \“>
<tr>
<td高度= \“ 83 \” width = \“ 850 \” bgcolor = \“＃eaf3f5 \” style = \“ border：none; padding-top：23px; padding-right：17px; padding; padding-bottom： 24px;郵政：17px; \“>
<table cellpadding = \“ 0 \” cellSpacing = \“ 0 \” border = \“ 0 \” width = \“ 100％\”>
<tr>
<td bgcolor = \“＃ffffff \” height = \“ 75 \” style = \“ font-weight：bold; bold; text-align：center; ftext-align; font-size：26px; color：＃0B3961; \ \ \ \ \'>＃＃＃＃＃＃＃ </td>
</tr>
<tr>
<td bgcolor = \“＃bad3df \” height = \“ 11 \”> </td>
</tr>
</table>
</td>
</tr>
<tr>
<td width = \“ 850 \” bgcolor = \“＃f7f7f7 \” valign = \“ top \” top \“ style = \” border：none; padding-top：0; padding-right：padding-right：44px ; padding-bottom：16px;郵政左：44px; \“>”>
<p style = \“保證金 -  top：30px; margin-bottom：28px; font-rate：bold; font; font; font-size：19px; \'>＃sub_title＃</p>
<p style = \“保證 -  top：0; margin-top：20px; line-height：20px; \ \“>＃text＃</p>
</td>
</tr>
<tr>
<td height = \“ 40px \” width = \“ 850 \” bgcolor = \“＃f7f7f7 \” valign = \“ top = \” top = \“ style = \” border; ;填充底：30px; Padding- Left：44px; \“>
<p style = \“邊界-  top：1px實心＃d1d1d1; margin-topom：5px; margin-top：0; padding-top：20px; line-height：21px; \ \'>＃footer_br＃<a href＃ <a href = \ \ \ \ \ \ \ \ \ \ \ \ “ http：//＃server_name＃\” style = \“顏色：＃2e6eb6; \”>＃footer_shop＃</a> <br />
電子郵件：<a href= \"mailto:# sale_email# \“style = \"color:#2e6eb6; \ \">＃sale_email＃</a>
</p>
</td>
</tr>
</table>
</body>
</html>";
$MESS["SALE_NEW_ORDER_DESC"] = "＃order_id＃ - 訂購ID
＃order_account_number_encode＃ - 訂購ID（用於URL）
＃order_real_id＃ - 真實訂單ID
＃order_date＃ - 訂購日期
＃order_user＃ - 客戶
＃價格＃ - 訂單金額
＃電子郵件＃-客戶電子郵件
＃BCC＃ -BCC電子郵件
＃order_list＃ - 訂購內容
＃order_public_url＃ - 未經授權的用戶的訂單查看鏈接（需要在e -商店模塊設置中配置）
＃sale_email＃ - 銷售部。電子郵件";
$MESS["SALE_NEW_ORDER_HTML_SUB_TITLE"] = "親愛的＃order_user＃，";
$MESS["SALE_NEW_ORDER_HTML_TEXT"] = "我們已經收到了您的訂單## of＃order_date＃＃。

訂單總數：＃價格＃。

訂購項目：
＃訂單＃

您可以通過＃site_name＃登錄帳戶來跟踪訂單。在註冊＃site_name＃時，您必須提供使用的登錄和密碼。

如果由於某種原因要取消訂單，請在您的帳戶中使用＃site_name＃中的適當命令。

與我們聯繫時，請參考您的訂單號（## worder_id＃）。

謝謝您的訂單！
";
$MESS["SALE_NEW_ORDER_HTML_TITLE"] = "您已經下訂單＃site_name＃";
$MESS["SALE_NEW_ORDER_MESSAGE"] = "從＃site_name＃訂購確認
-------------------------------------------------- --------

親愛的＃order_user＃，

您的訂單＃order_id＃來自＃order_date＃已被接受。

訂單值：＃價格＃。

訂購的項目：
＃訂單＃

您可以監視訂單處理（查看當前狀態
訂單）通過＃site_name＃輸入您的個人網站部分。
請注意，您需要登錄和密碼來輸入此
網站部分，＃site_name＃。

要取消您的訂單，請使用您的特殊選項
＃site_name＃的個人部分。

請注意，您應該指定訂單ID：＃order_id＃
在＃site_name＃中請求網站管理中的任何信息時。

感謝您的訂購！
";
$MESS["SALE_NEW_ORDER_NAME"] = "新命令";
$MESS["SALE_NEW_ORDER_RECURRING_DESC"] = "＃order_id＃ - 訂購ID
＃order_account_number_encode＃ - 訂購ID（用於URL）
＃order_real_id＃ - 真實訂單ID
＃order_date＃ - 訂購日期
＃order_user＃ - 客戶
＃價格＃ - 訂單金額
＃電子郵件＃-客戶電子郵件
＃BCC＃ -BCC電子郵件
＃order_list＃ - 訂購內容
＃sale_email＃ - 銷售部。電子郵件";
$MESS["SALE_NEW_ORDER_RECURRING_MESSAGE"] = "信息來自#Site_name＃\\ r \\ n --------------------------------------- ---------------------------------------- --- \\ r \\ n \ \ r \\ r \\ ndear #order_user＃，\\ r \\ n \\ r \\ r \\ n yyour order order訂單## prode_id＃of＃order_date＃for subscription reenewal已被接受。\\ r \ \ n \\ r \\ norder量：#price＃。\\ r \\ n \\ r \\ norder項目：\\ r \\ n＃order_list＃可以在＃site_name＃的私人區域中跟踪您的訂單狀態。請注意，您將必須輸入通常使用的登錄和密碼，以登錄#Site_Name＃。\\ r \\ n \\ r \\ n yyou可以在#Site_Name＃。\\ r中取消您的訂單。\\ r \\ n \\ r \\ n yyou被要求將您的訂單編號＃order_id＃包含在您發送到＃site_name＃管理的所有消息中。";
$MESS["SALE_NEW_ORDER_RECURRING_NAME"] = "訂閱續訂的新訂單";
$MESS["SALE_NEW_ORDER_RECURRING_SUBJECT"] = "＃site_name＃：新訂單## order_id＃用於訂閱續訂";
$MESS["SALE_NEW_ORDER_SUBJECT"] = "＃site_name＃：新訂單n＃order_id＃＃";
$MESS["SALE_ORDER_CANCEL_DESC"] = "＃order_id＃ - 訂購ID
＃order_account_number_encode＃ - 訂購ID（用於URL）
＃order_real_id＃ - 真實訂單ID
＃order_date＃ - 訂購日期
＃電子郵件＃-客戶電子郵件
＃order_list＃ - 訂購內容
＃order_cancel_description＃ - 取消原因
＃order_public_url＃ - 未經授權的用戶的訂單查看鏈接（需要在e -商店模塊設置中配置）
＃sale_email＃ - 銷售部。電子郵件
";
$MESS["SALE_ORDER_CANCEL_HTML_SUB_TITLE"] = "訂單## word_id＃of＃order_date＃已取消。";
$MESS["SALE_ORDER_CANCEL_HTML_TEXT"] = "＃order_cancel_description＃

要查看訂單詳細信息，請單擊此處：http：//＃server_name＃/personal/order/worder/＃order_id＃/＃/＃
";
$MESS["SALE_ORDER_CANCEL_HTML_TITLE"] = "＃site_name＃：取消訂單## order_id＃＃";
$MESS["SALE_ORDER_CANCEL_MESSAGE"] = "來自＃site_name＃的信息消息
-------------------------------------------------- --------

訂單## rode_id＃來自＃order_date＃已取消。

＃order_cancel_description＃

＃site_name＃
";
$MESS["SALE_ORDER_CANCEL_NAME"] = "取消訂單";
$MESS["SALE_ORDER_CANCEL_SUBJECT"] = "＃site_name＃：訂單n＃order_id＃被取消";
$MESS["SALE_ORDER_DELIVERY_DESC"] = "＃order_id＃ - 訂購ID
＃order_account_number_encode＃ - 訂購ID（用於URL）
＃order_real_id＃ - 真實訂單ID
＃order_date＃ - 訂購日期
＃電子郵件＃-客戶電子郵件
＃order_public_url＃ - 未經授權的用戶的訂單查看鏈接（需要在e -商店模塊設置中配置）
＃sale_email＃ - 銷售部。電子郵件";
$MESS["SALE_ORDER_DELIVERY_HTML_SUB_TITLE"] = "訂單## word_id＃of＃order_date＃已發貨。";
$MESS["SALE_ORDER_DELIVERY_HTML_TEXT"] = "要查看訂單詳細信息，請單擊此處：http：//＃server_name＃/personal/order/worder/＃order_id＃/＃/＃
";
$MESS["SALE_ORDER_DELIVERY_HTML_TITLE"] = "您使用＃site_name＃已發貨。";
$MESS["SALE_ORDER_DELIVERY_MESSAGE"] = "來自＃site_name＃的信息消息
-------------------------------------------------- --------

允許從＃order_date＃的訂單##訂單＃＃交付。

＃site_name＃
";
$MESS["SALE_ORDER_DELIVERY_NAME"] = "允許訂單交貨";
$MESS["SALE_ORDER_DELIVERY_SUBJECT"] = "＃site_name＃：允許訂單n＃order_id＃的交付";
$MESS["SALE_ORDER_PAID_DESC"] = "＃order_id＃ - 訂購ID
＃order_account_number_encode＃ - 訂購ID（用於URL）
＃order_real_id＃ - 真實訂單ID
＃order_date＃ - 訂購日期
＃電子郵件＃-客戶電子郵件
＃order_public_url＃ - 未經授權的用戶的訂單查看鏈接（需要在e -商店模塊設置中配置）
＃sale_email＃ - 銷售部。電子郵件";
$MESS["SALE_ORDER_PAID_HTML_SUB_TITLE"] = "您的訂單## word_id＃of＃order_date＃已付款。";
$MESS["SALE_ORDER_PAID_HTML_TEXT"] = "要查看訂單詳細信息，請單擊此處：http：//＃server_name＃/personal/order/worder/＃order_id＃/＃/＃";
$MESS["SALE_ORDER_PAID_HTML_TITLE"] = "您使用＃site_name＃的訂單付款";
$MESS["SALE_ORDER_PAID_MESSAGE"] = "來自＃site_name＃的信息消息
-------------------------------------------------- --------

訂單##訂購＃＃dord_date＃已付款。

＃site_name＃
";
$MESS["SALE_ORDER_PAID_NAME"] = "付費訂單";
$MESS["SALE_ORDER_PAID_SUBJECT"] = "＃site_name＃：訂單n＃order_id＃已付款";
$MESS["SALE_ORDER_REMIND_PAYMENT_DESC"] = "＃order_id＃ - 訂購ID
＃order_account_number_encode＃ - 訂購ID（用於URL）
＃order_real_id＃ - 真實訂單ID
＃order_date＃ - 訂購日期
＃order_user＃ - 客戶
＃價格＃ - 訂單金額
＃電子郵件＃-客戶電子郵件
＃BCC＃ -BCC電子郵件
＃order_list＃ - 訂購內容
＃order_public_url＃ - 未經授權的用戶的訂單查看鏈接（需要在e -商店模塊設置中配置）
＃sale_email＃ - 銷售部。電子郵件";
$MESS["SALE_ORDER_REMIND_PAYMENT_HTML_SUB_TITLE"] = "親愛的＃order_user＃，";
$MESS["SALE_ORDER_REMIND_PAYMENT_HTML_TEXT"] = "您已經在＃order_date＃上放置了一個##訂單＃＃price＃。

不幸的是，我們尚未收到您的付款。

您可以通過＃site_name＃登錄帳戶來跟踪訂單。在註冊＃site_name＃時，您必須提供使用的登錄和密碼。

如果由於某種原因要取消訂單，請在您的帳戶中使用＃site_name＃中的適當命令。

與我們聯繫時，請參考您的訂單號（## worder_id＃）。

謝謝您的訂單！";
$MESS["SALE_ORDER_REMIND_PAYMENT_HTML_TITLE"] = "不要忘記用＃site_name＃付款";
$MESS["SALE_ORDER_REMIND_PAYMENT_MESSAGE"] = "來自＃site_name＃的信息
-------------------------------------------------- --------

親愛的＃order_user＃，

您已經放置了## of＃order_date＃的訂單## order_id＃，金額：＃price＃。

不幸的是，看來您的付款尚未完成。沒有資金轉移到我們的帳戶。

您可以在私人區域跟踪訂單狀態
在＃site_name＃。請注意，您必須輸入登錄
和通常用於登錄到＃site_name＃的密碼。

您可以在＃site_name＃的私人區域中取消訂單。

請您在發送到＃site_name＃管理的所有消息中包括您的訂單號＃order_id＃。

感謝您的購買！
";
$MESS["SALE_ORDER_REMIND_PAYMENT_NAME"] = "訂單付款提醒";
$MESS["SALE_ORDER_REMIND_PAYMENT_SUBJECT"] = "＃site_name＃：訂單的付款提醒## order_id＃＃";
$MESS["SALE_ORDER_SHIPMENT_STATUS_CHANGED_HTML_SUB_TITLE"] = "親愛的＃order_user＃，";
$MESS["SALE_ORDER_SHIPMENT_STATUS_CHANGED_HTML_TEXT"] = "訂單## order_no＃of＃order_date＃的運輸狀態已更新為

\“＃status_name＃\”（＃status_description＃）。

跟踪號：＃Tracking_number＃。

發貨：＃veliver_name＃。

＃delivery_tracking_url ## order_detail_url＃
";
$MESS["SALE_ORDER_SHIPMENT_STATUS_CHANGED_HTML_TITLE"] = "從＃site_name＃跟踪您的發貨信息已更新";
$MESS["SALE_ORDER_SHIPMENT_STATUS_CHANGED_SUBJECT"] = "＃site_name＃的貨物狀態已更新";
$MESS["SALE_ORDER_SHIPMENT_STATUS_CHANGED_TYPE_DESC"] = "＃shipment_no＃ - 發貨ID
＃shipment_date＃ - 發貨
＃order_no＃ - 訂單＃
＃order_date＃ - 訂購日期
＃status_name＃ - 狀態名稱
＃status_description＃ - 狀態描述
＃tracking_number＃ - 跟踪號碼
＃電子郵件＃ - 通知電子郵件地址
＃bcc＃ - 將副本發送到地址
＃order_user＃ - 客戶
＃delivery_name＃ - 交付服務名稱
＃delivery_tracking_url＃ - 交付服務網站以獲取更多跟踪詳細信息
＃order_account_number_encode＃ - 訂購ID（用於鏈接）
＃order_public_url＃ - 未經授權的用戶的訂單查看鏈接（需要在e -商店模塊設置中配置）
＃order_detail_url＃ - 訂購詳細信息URL";
$MESS["SALE_ORDER_SHIPMENT_STATUS_CHANGED_TYPE_NAME"] = "軟件包狀態更新";
$MESS["SALE_ORDER_TRACKING_NUMBER_HTML_SUB_TITLE"] = "親愛的＃order_user＃，";
$MESS["SALE_ORDER_TRACKING_NUMBER_HTML_TEXT"] = "您的訂單＃order_id＃來自＃order_date＃已發貨。

跟踪號為：＃order_tracking_number＃。

有關該訂單的詳細信息

電子郵件：＃sale_email＃
";
$MESS["SALE_ORDER_TRACKING_NUMBER_HTML_TITLE"] = "您從＃site_name＃的訂單的裝運號";
$MESS["SALE_ORDER_TRACKING_NUMBER_MESSAGE"] = "訂單n＃order_id＃來自＃order_date＃已通過郵件發貨。

跟踪號為：＃order_tracking_number＃。

有關該訂單的信息，請參見http：//＃server_name＃/hersepher/order/order/lode/＃order_id＃/＃

電子郵件：＃sale_email＃
";
$MESS["SALE_ORDER_TRACKING_NUMBER_SUBJECT"] = "從＃site_name＃跟踪您的訂單號碼";
$MESS["SALE_ORDER_TRACKING_NUMBER_TYPE_DESC"] = "＃order_id＃ - 訂購ID
＃order_account_number_encode＃ - 訂購ID（用於URL）
＃order_real_id＃ - 真實訂單ID
＃order_date＃ - 訂購日期
＃order_user＃ - 客戶
＃order_tracking_number＃ - 跟踪號碼
＃order_public_url＃ - 未經授權的用戶的訂單查看鏈接（需要在e -商店模塊設置中配置）
＃電子郵件＃-客戶電子郵件
＃BCC＃ -BCC電子郵件
＃sale_email＃ - 銷售部。電子郵件";
$MESS["SALE_ORDER_TRACKING_NUMBER_TYPE_NAME"] = "跟踪編號變更通知";
$MESS["SALE_RECURRING_CANCEL_DESC"] = "＃order_id＃ - 訂購ID
＃order_account_number_encode＃ - 訂購ID（用於URL）
＃order_real_id＃ - 真實訂單ID
＃order_date＃ - 訂購日期
＃電子郵件＃-客戶電子郵件
＃取消_REASON＃ - 取消原因
＃sale_email＃ - 銷售部。電子郵件";
$MESS["SALE_RECURRING_CANCEL_MESSAGE"] = "來自＃site_name＃的信息消息
-------------------------------------------------- --------

經常付款被取消

＃取消_REASAN＃
＃site_name＃
";
$MESS["SALE_RECURRING_CANCEL_NAME"] = "反复付款被取消";
$MESS["SALE_RECURRING_CANCEL_SUBJECT"] = "＃site_name＃：取消重複付款";
$MESS["SALE_SUBSCRIBE_PRODUCT_HTML_SUB_TITLE"] = "親愛的＃user_name＃！";
$MESS["SALE_SUBSCRIBE_PRODUCT_HTML_TEXT"] = "\“＃名稱＃\”（＃page_url＃）現在又回來了。

單擊此處立即訂購：http：//＃server_name＃/personal/cart/

請記住在訂購前請記錄自己。

您正在收到此消息，因為您要求我們及時通知您。
此消息是由機器人發送的。不要回复。

感謝您與我們購物！
";
$MESS["SALE_SUBSCRIBE_PRODUCT_HTML_TITLE"] = "產品回到＃site_name＃的庫存庫存";
$MESS["SALE_SUBSCRIBE_PRODUCT_SUBJECT"] = "＃site_name＃：產品又回來了";
$MESS["SKGS_STATUS_MAIL_HTML_TITLE"] = "訂單在＃site_name＃上更新";
$MESS["SMAIL_FOOTER_BR"] = "善良的問候，<br />支持人員。";
$MESS["SMAIL_FOOTER_SHOP"] = "網上商店";
$MESS["UP_MESSAGE"] = "來自＃site_name＃的消息
-------------------------------------------------- --------

親愛的＃user_name＃，

您感興趣的產品，\“＃名稱＃\”（＃page_url＃）現在又回來了。
我們建議您盡快下訂單（http：//＃server_name＃/personal/cart/）。

您正在收到此消息，因為您要求在此產品何時通知此消息。

真摯地，

＃site_name＃客戶服務";
$MESS["UP_SUBJECT"] = "＃site_name＃：產品又回來了";
$MESS["UP_TYPE_SUBJECT"] = "返回庫存通知";
$MESS["UP_TYPE_SUBJECT_DESC"] = "＃user_name＃ - 用戶名
＃電子郵件＃-用戶電子郵件
＃名稱＃-產品名稱
＃page_url＃ - 產品詳細信息頁面";
