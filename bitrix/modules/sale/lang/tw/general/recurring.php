<?php
$MESS["SKGR_DUB_CANCEL"] = "訂閱## ID＃已經分配了取消標誌";
$MESS["SKGR_EMPTY_NEXT_DATE"] = "未指定下一個付款的日期";
$MESS["SKGR_EMPTY_ORDER_ID"] = "訂單ID未指定";
$MESS["SKGR_EMPTY_SUBSCR"] = "未指定訂閱ID";
$MESS["SKGR_EMPTY_USER_ID"] = "未指定用戶ID";
$MESS["SKGR_NO_DELIVERY"] = "無法找到適當的送貨服務";
$MESS["SKGR_NO_ORDER"] = "找不到訂單## ID＃";
$MESS["SKGR_NO_ORDER1"] = "找不到訂單## ID＃";
$MESS["SKGR_NO_RECID"] = "記錄ID未指定";
$MESS["SKGR_NO_RECID1"] = "找不到記錄## ID＃";
$MESS["SKGR_NO_SUBSCR"] = "找不到訂閱## ID＃";
$MESS["SKGR_NO_USER"] = "找不到用戶## ID＃";
$MESS["SKGR_VAT"] = "增值稅";
