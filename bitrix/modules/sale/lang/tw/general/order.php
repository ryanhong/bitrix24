<?php
$MESS["SALE_ORDER_GENERATE_ACCOUNT_NUMBER_ORDER_NUMBER_IS_NOT_SET"] = "無法設置訂單ID";
$MESS["SALE_ORDER_GENERATE_ACCOUNT_NUMBER_ORDER_NUMBER_IS_NOT_SET_AS_ID"] = "無法將ID設置為訂單號";
$MESS["SALE_ORDER_GENERATE_ACCOUNT_NUMBER_ORDER_NUMBER_WRONG_ID"] = "無效訂單ID";
$MESS["SKGO_CALC_PARAM_ERROR"] = "錯誤調用DocalculateOrder：未指定網站ID。";
$MESS["SKGO_CANCEL_ERROR"] = "錯誤取消訂單。 #訊息#";
$MESS["SKGO_DELETE_ERROR"] = "錯誤刪除訂單。 #訊息#";
$MESS["SKGO_DUB_CANCEL"] = "訂單## ID＃已經分配了取消標誌";
$MESS["SKGO_DUB_DEDUCTION"] = "訂單## ID＃已經具有所需的\“已運送\”值。";
$MESS["SKGO_DUB_DELIVERY"] = "訂單## ID＃已經分配了一個交付標誌";
$MESS["SKGO_DUB_PAY"] = "訂單## ID＃已經分配了付費標誌";
$MESS["SKGO_DUB_RESERVATION"] = "訂單## ID＃已經具有所需的\“ Reserved \”值。";
$MESS["SKGO_DUB_STATUS"] = "訂單## ID＃已經需要狀態";
$MESS["SKGO_EMPTY_ACCOUNT_NUMBER"] = "訂單號不能空白";
$MESS["SKGO_EMPTY_CURRENCY"] = "未指定貨幣";
$MESS["SKGO_EMPTY_PERS_TYPE"] = "未指定付款人類型";
$MESS["SKGO_EMPTY_SITE"] = "未指定訂購網站";
$MESS["SKGO_EMPTY_USER_ID"] = "未指定客戶ID";
$MESS["SKGO_ERROR_ORDERS"] = "用戶## user_id＃已經在電子商店中有一些訂單，無法刪除。";
$MESS["SKGO_ERROR_ORDERS_ARCHIVE"] = "用戶## user_id＃已在電子商店模塊中存檔訂單，無法刪除。";
$MESS["SKGO_ERROR_ORDERS_ARCHIVE_CURRENCY"] = "系統中有存檔訂單使用貨幣\“＃貨幣＃\”。";
$MESS["SKGO_ERROR_ORDERS_CURRENCY"] = "該系統包含＃貨幣＃的訂單。";
$MESS["SKGO_EXISTING_ACCOUNT_NUMBER"] = "輸入的訂單編號已經用於不同的順序";
$MESS["SKGO_NO_ORDER"] = "訂單## ID＃找不到";
$MESS["SKGO_NO_ORDER_ID"] = "訂單ID未指定";
$MESS["SKGO_NO_ORDER_ID1"] = "訂單ID未指定";
$MESS["SKGO_SHOPPING_CART_EMPTY"] = "購物車是空的";
$MESS["SKGO_WRONG_CURRENCY"] = "找不到貨幣## ID";
$MESS["SKGO_WRONG_DELIVERY"] = "找不到送貨服務## ID＃";
$MESS["SKGO_WRONG_PERSON_TYPE"] = "找不到付款人類型## ID＃";
$MESS["SKGO_WRONG_PS"] = "找不到付費系統## ID＃";
$MESS["SKGO_WRONG_SITE"] = "找不到網站## ID＃";
$MESS["SKGO_WRONG_STATUS"] = "找不到狀態## ID＃";
$MESS["SKGO_WRONG_USER"] = "找不到用戶## ID＃";
