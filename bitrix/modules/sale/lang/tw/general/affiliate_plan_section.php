<?php
$MESS["SCGAPS1_BAD_FUNC"] = "錯誤調用功能";
$MESS["SCGAPS1_NO_CURRENCY"] = "未指定佣金貨幣";
$MESS["SCGAPS1_NO_MODULE"] = "未指定模塊名稱";
$MESS["SCGAPS1_NO_PLAN"] = "計劃ID未指定";
$MESS["SCGAPS1_NO_RECORD"] = "沒有找到ID＃ID＃的記錄";
$MESS["SCGAPS1_NO_SECTION"] = "組ID未指定";
