<?php
$MESS["SL_IMPORT_ALL_DONE"] = "完畢。";
$MESS["SL_IMPORT_ERROR_FILES"] = "錯誤！文件名不正確。";
$MESS["SL_IMPORT_ERROR_NO_LANG"] = "沒有找到語言。";
$MESS["SL_IMPORT_ERROR_NO_LOC_FILE"] = "沒有位置文件上傳。";
$MESS["SL_IMPORT_ERROR_NO_ZIP_FILE"] = "找不到郵政編碼文件。";
$MESS["SL_IMPORT_ERROR_WRONG_LOC_FILE"] = "位置文件格式不正確。";
$MESS["SL_IMPORT_LOC_STATS"] = "位置：<ul>
<li>進口國家：#NumCountries＃</li>
<li>導入的區域：#numregiones＃</li>
<li>導入的城市：#numcities＃</li>
<li>總位置：#numlocations＃</li>
</ul>";
$MESS["SL_IMPORT_ZIP_STATS"] = "郵政編碼：<ul>
<li>導入的郵政代碼：#NUMZIP＃</li>
<li>對於城市：#numcities＃</li>
</ul>";
$MESS["SL_LOADER_FILE_ERROR"] = "無法加載文件。";
$MESS["SL_LOADER_FILE_LOADED"] = "加載文件";
$MESS["SL_MODULE_SALE_NOT_INSTALLED"] = "沒有安裝電子商店模塊。";
