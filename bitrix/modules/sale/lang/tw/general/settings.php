<?php
$MESS["SKGO_ERROR_CURRENCY"] = "貨幣＃貨幣＃被綁定到電子商店設置中的站點，因此無法刪除。";
$MESS["SKGO_ERROR_DEFAULT_CURRENCY"] = "＃貨幣＃是在線商店的默認貨幣。只要相應的模塊設置保留在原位，就無法刪除它。";
$MESS["SKGS_NO_DEL_FLAG"] = "未指定要刪除的記錄的標誌的ID";
$MESS["SKGS_NO_DEL_GROUP"] = "未指定要刪除記錄的組的ID";
$MESS["SKGS_NO_DEL_ID"] = "未指定要刪除的記錄的ID";
$MESS["SKGS_NO_DEL_SITE"] = "未指定要刪除其記錄的網站的ID";
$MESS["SKGS_NO_ID"] = "未指定要更新記錄的ID";
