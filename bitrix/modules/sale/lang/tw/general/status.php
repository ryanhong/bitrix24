<?php
$MESS["SIM_ACCEPTED"] = "已收到";
$MESS["SIM_ACCEPTED_DESCR"] = "訂單已被接受，但尚未處理（例如：可能剛剛創建訂單或等待付款）";
$MESS["SIM_FINISHED"] = "完全的";
$MESS["SIM_FINISHED_DESCR"] = "訂單已交付並支付";
$MESS["SKGS_CHANGING_SHIPMENT_STATUS_TO"] = "將裝運狀態更改為";
$MESS["SKGS_CHANGING_STATUS_TO"] = "將訂單狀態更改為";
$MESS["SKGS_ERROR_ARCHIVED_DELETE"] = "無法刪除狀態，因為此狀態中有存檔訂單。";
$MESS["SKGS_ERROR_DELETE"] = "由於此狀態中有訂單，因此無法刪除狀態。";
$MESS["SKGS_ID_NOT_SYMBOL"] = "狀態代碼應該是字母";
$MESS["SKGS_ORDER_ALLOW_PAY_STATUS"] = "清算付款的訂單";
$MESS["SKGS_ORDER_ALLOW_PAY_STATUS_MAIL_BODY1"] = "此消息是從＃site_name＃發送的
";
$MESS["SKGS_ORDER_ALLOW_PAY_STATUS_MAIL_BODY2"] = "訂單## order_id＃現在可以付款。

";
$MESS["SKGS_ORDER_ALLOW_PAY_STATUS_MAIL_BODY3"] = "請按照此鏈接進行付款：＃server_name＃/hersemy/orders/＃order_id＃＃
";
$MESS["SKGS_ORDER_ALLOW_PAY_STATUS_MAIL_BODY4"] = "要查看訂單詳細信息，請單擊以下鏈接：＃server_name＃/personal/orders/＃order_id＃＃

謝謝你！
";
$MESS["SKGS_ORDER_ALLOW_PAY_STATUS_MAIL_SUBJ"] = "＃server_name＃：訂單## order_id＃清算付款";
$MESS["SKGS_ORDER_DATE"] = "訂購日期";
$MESS["SKGS_ORDER_EMAIL"] = "客戶電子郵件";
$MESS["SKGS_ORDER_ID"] = "訂購ID";
$MESS["SKGS_ORDER_PUBLIC_LINK"] = "未經授權用戶的訂單查看鏈接（需要在E商店模塊設置中進行配置）";
$MESS["SKGS_ORDER_STATUS"] = "訂單狀態";
$MESS["SKGS_SALE_EMAIL"] = "銷售部門電子郵件";
$MESS["SKGS_SALE_STATUS_ALREADY_EXISTS"] = "此狀態ID已經存在。";
$MESS["SKGS_SHIPMENT_DATE"] = "創建的裝運";
$MESS["SKGS_SHIPMENT_ID"] = "發貨ID";
$MESS["SKGS_SHIPMENT_STATUS"] = "運輸狀態";
$MESS["SKGS_SHIPMENT_STATUS_MAIL_BODY1"] = "此消息是從＃site_name＃發送的
";
$MESS["SKGS_SHIPMENT_STATUS_MAIL_BODY2"] = "裝運的狀態## Shipment_ID＃已更改。
";
$MESS["SKGS_SHIPMENT_STATUS_MAIL_BODY3"] = "新裝運狀態：
";
$MESS["SKGS_SHIPMENT_STATUS_MAIL_BODY4"] = "要查看訂單詳細信息，請單擊此處：＃server_name＃/personal/order/worde/＃order_id＃/

感謝您的購物！";
$MESS["SKGS_SHIPMENT_STATUS_MAIL_SUBJ"] = "＃server_name＃：更改運輸狀態## Shipment_ID＃";
$MESS["SKGS_STATUS_DESCR"] = "訂單狀態描述";
$MESS["SKGS_STATUS_MAIL_BODY1"] = "來自＃site_name＃的消息
";
$MESS["SKGS_STATUS_MAIL_BODY2"] = "訂單號的狀態。 ＃order_id＃of＃order_date＃已更改。

";
$MESS["SKGS_STATUS_MAIL_BODY3"] = "新訂單狀態：
";
$MESS["SKGS_STATUS_MAIL_BODY4"] = "要查看訂單詳細信息，請單擊此處：＃server_name＃/personal/order/worde/＃order_id＃/

感謝您的購物！";
$MESS["SKGS_STATUS_MAIL_SUBJ"] = "＃server_name＃：更改訂單編號的狀態。 ＃order_id＃＃";
$MESS["SKGS_STATUS_TEXT"] = "文字";
