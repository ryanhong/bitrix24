<?php
$MESS["SKGU_ACCOUNT_LOCKED"] = "本地用戶帳戶已經鎖定";
$MESS["SKGU_ACCOUNT_NOT_LOCKED"] = "帳戶無法鎖定";
$MESS["SKGU_ACCOUNT_NOT_WORK"] = "內部帳戶暫時不可用。";
$MESS["SKGU_EMPTYID"] = "空用戶ID";
$MESS["SKGU_EMPTY_CUR"] = "空貨幣";
$MESS["SKGU_EMPTY_CURRENCY"] = "空貨幣字段";
$MESS["SKGU_EMPTY_SUM"] = "空總和";
$MESS["SKGU_EMPTY_USER_ID"] = "空用戶字段";
$MESS["SKGU_ERROR_LOCK"] = "錯誤鎖定內部帳戶";
$MESS["SKGU_NO_ACCOUNT"] = "找不到內部用戶帳戶";
$MESS["SKGU_NO_ENOUGH"] = "您的信用不足以完成交易";
$MESS["SKGU_NO_USER"] = "找不到用戶## ID＃";
$MESS["UA_ERROR_USER"] = "用戶## user_id＃在電子商店模塊中具有正平衡，因此無法刪除。";
