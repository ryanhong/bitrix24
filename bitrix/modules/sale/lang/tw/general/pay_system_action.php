<?php
$MESS["SKGPSA_ALARM_EVENT_LOG"] = "支付系統問題";
$MESS["SKGPSA_ALARM_MESSAGE"] = "有付款系統問題。請參閱<a href= \"/bitrix/admin/event_log.php？";
$MESS["SKGPSA_ERROR_NO_KEY"] = "鍵\“＃鍵＃\”在數組Globals [sale_corrvesence]中找不到，未指定默認值。訂單ID：\“＃order_id＃\”，付款系統ID：\“＃PS_ID＃\”";
$MESS["SKGPSA_NO_CODE"] = "未指定付款系統代碼";
$MESS["SKGPSA_NO_ID_TYPE"] = "未指定付款人類型ID";
$MESS["SKGPSA_NO_NAME"] = "付款系統服務名稱未指定";
$MESS["SKGPSA_NO_PERS_TYPE"] = "找不到付款人類型## ID＃";
$MESS["SKGPSA_NO_PS"] = "找不到付款系統## ID＃";
