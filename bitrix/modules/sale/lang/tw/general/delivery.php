<?php
$MESS["SALE_DELIV_PERIOD"] = "交貨時間";
$MESS["SKGD_DELIVERY_NOT_FOUND"] = "未找到送貨服務。";
$MESS["SKGD_EMPTY_CURRENCY"] = "未指定送貨服務貨幣";
$MESS["SKGD_EMPTY_DELIVERY"] = "未指定送貨服務名稱";
$MESS["SKGD_EMPTY_LOCATION"] = "未指定交貨位置";
$MESS["SKGD_EMPTY_SITE"] = "未指定送貨服務URL";
$MESS["SKGD_NO_CURRENCY"] = "找不到貨幣## ID";
$MESS["SKGD_NO_SITE"] = "找不到網站## ID＃";
$MESS["SKGPS_ORDERS_TO_DELIVERY"] = "無法刪除記錄，因為使用此交付服務有一些訂單。";
$MESS["SOA_DAY"] = "天";
$MESS["SOA_FROM"] = "從";
$MESS["SOA_HOUR"] = "小時";
$MESS["SOA_MONTH"] = "月份";
$MESS["SOA_TO"] = "到";
