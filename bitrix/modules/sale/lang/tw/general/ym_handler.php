<?php
$MESS["SALE_YMARKET_ORDER_PRICE_CHANGED"] = "注意力！訂單總計在客戶下訂單時發生了變化。";
$MESS["SALE_YMH_ADDRESS_APARTMENT"] = "#VALUE!";
$MESS["SALE_YMH_ADDRESS_BLOCK"] = "#VALUE!";
$MESS["SALE_YMH_ADDRESS_CITY"] = "#VALUE!";
$MESS["SALE_YMH_ADDRESS_COUNTRY"] = "#VALUE!";
$MESS["SALE_YMH_ADDRESS_ENTRANCE"] = "#VALUE!";
$MESS["SALE_YMH_ADDRESS_ENTRYPHONE"] = "#VALUE!";
$MESS["SALE_YMH_ADDRESS_FLOOR"] = "#VALUE!";
$MESS["SALE_YMH_ADDRESS_HOUSE"] = "#VALUE!";
$MESS["SALE_YMH_ADDRESS_PHONE"] = "#VALUE!";
$MESS["SALE_YMH_ADDRESS_RECIPIENT"] = "#VALUE!";
$MESS["SALE_YMH_ADDRESS_STREET"] = "#VALUE!";
$MESS["SALE_YMH_ADDRESS_SUBWAY"] = "#VALUE!";
$MESS["SALE_YMH_ADMIN_NOTIFY_SEND_STATUS_ERROR_403"] = "#VALUE!";
$MESS["SALE_YMH_CITY_MOSCOW"] = "#VALUE!";
$MESS["SALE_YMH_CITY_SPB"] = "#VALUE!";
$MESS["SALE_YMH_DESCRIPTION"] = "#VALUE!";
$MESS["SALE_YMH_ERROR_BAD_STRUCTURE"] = "#VALUE!";
$MESS["SALE_YMH_ERROR_CATALOG_NOT_INSTALLED"] = "#VALUE!";
$MESS["SALE_YMH_ERROR_FORBIDDEN"] = "#VALUE!";
$MESS["SALE_YMH_ERROR_OFF"] = "#VALUE!";
$MESS["SALE_YMH_ERROR_UNAUTH"] = "#VALUE!";
$MESS["SALE_YMH_ERROR_UNKNOWN_REQ_OBJ"] = "#VALUE!";
$MESS["SALE_YMH_ERROR_UNKNOWN_STATUS"] = "#VALUE!";
$MESS["SALE_YMH_INCOMING_ORDER_STATUS_CANCELED"] = "#VALUE!";
$MESS["SALE_YMH_INCOMING_ORDER_STATUS_PROCESSING"] = "#VALUE!";
$MESS["SALE_YMH_LOCATION_NOT_FOUND"] = "#VALUE!";
$MESS["SALE_YMH_LOG_TYPE_INCOMING_ORDER_STATUS"] = "#VALUE!";
$MESS["SALE_YMH_LOG_TYPE_INCOMING_REQUEST"] = "#VALUE!";
$MESS["SALE_YMH_LOG_TYPE_INCOMING_REQUEST_RESULT"] = "#VALUE!";
$MESS["SALE_YMH_LOG_TYPE_ORDER_CREATE"] = "#VALUE!";
$MESS["SALE_YMH_LOG_TYPE_ORDER_CREATE_ERROR"] = "錯誤創建訂單。";
$MESS["SALE_YMH_LOG_TYPE_ORDER_STATUS_CHANGE"] = "#VALUE!";
$MESS["SALE_YMH_LOG_TYPE_ORDER_STATUS_CHANGE_ERROR"] = "#VALUE!";
$MESS["SALE_YMH_LOG_TYPE_ORDER_STATUS_CHANGE_OK"] = "#VALUE!";
$MESS["SALE_YMH_LOG_TYPE_REQUEST_ERROR"] = "#VALUE!";
$MESS["SALE_YMH_LOG_TYPE_STATUS_CHANGE"] = "#VALUE!";
$MESS["SALE_YMH_LOG_TYPE_USER_CREATE"] = "#VALUE!";
$MESS["SALE_YMH_LOG_TYPE_YMARKET_LOCATION_MAPPING"] = "#VALUE!";
$MESS["SALE_YMH_MARK_BUYER_WAITING"] = "等待客戶的信息";
$MESS["SALE_YMH_NAME"] = "#VALUE!";
$MESS["SALE_YMH_ORDER_CREATE"] = "#VALUE!";
$MESS["SALE_YMH_SHT"] = "#VALUE!";
$MESS["SALE_YMH_STATUS"] = "#VALUE!";
$MESS["SALE_YMH_SUBSTATUS_PROCESSING_EXPIRED"] = "#VALUE!";
$MESS["SALE_YMH_SUBSTATUS_REPLACING_ORDER"] = "#VALUE!";
$MESS["SALE_YMH_SUBSTATUS_RESERVATION_EXPIRED"] = "#VALUE!";
$MESS["SALE_YMH_SUBSTATUS_SHOP_FAILED"] = "#VALUE!";
$MESS["SALE_YMH_SUBSTATUS_USER_CHANGED_MIND"] = "#VALUE!";
$MESS["SALE_YMH_SUBSTATUS_USER_NOT_PAID"] = "#VALUE!";
$MESS["SALE_YMH_SUBSTATUS_USER_REFUSED_DELIVERY"] = "#VALUE!";
$MESS["SALE_YMH_SUBSTATUS_USER_REFUSED_PRODUCT"] = "#VALUE!";
$MESS["SALE_YMH_SUBSTATUS_USER_REFUSED_QUALITY"] = "#VALUE!";
$MESS["SALE_YMH_SUBSTATUS_USER_UNREACHABLE"] = "#VALUE!";
$MESS["SALE_YMH_USER_PROFILE_CREATED"] = "#VALUE!";
