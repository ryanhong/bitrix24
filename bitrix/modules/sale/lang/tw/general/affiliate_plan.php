<?php
$MESS["SCGAP1_AFF_EXISTS"] = "計劃＃id＃包含會員";
$MESS["SCGAP1_ERROR_FUNC"] = "錯誤調用功能";
$MESS["SCGAP1_NO_CURRENCY"] = "未指定佣金貨幣";
$MESS["SCGAP1_NO_NAME"] = "計劃名稱未指定";
$MESS["SCGAP1_NO_PLAN"] = "沒有找到ID＃ID＃的計劃";
$MESS["SCGAP1_NO_SITE"] = "未指定計劃網站";
