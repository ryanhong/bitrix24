<?php
$MESS["NEWO_ERR_STORE_BARCODES"] = "交付錯誤：在倉庫## store_id＃上指定了針對\“＃product_name＃\”的不存在的條形碼。";
$MESS["NEWO_ERR_STORE_EMPTY_BARCODES"] = "交付錯誤：為\“＃product_name＃\”指定的空條形碼在倉庫## store_id＃。";
$MESS["NEWO_ERR_STORE_NO_BARCODES"] = "交付錯誤：在倉庫## store_id＃上沒有針對\“＃product_name＃\”指定的條形碼。";
$MESS["NEWO_ERR_STORE_QUANTITY_BARCODE"] = "交付錯誤：\“＃product_name＃\”的欄編碼數量## store_id＃與產品總量不匹配。";
$MESS["NEWO_ERR_STORE_QUANTITY_NOT_EQUAL_TOTAL_QUANTITY"] = "交付錯誤：\“＃product_name＃\”的庫存在倉庫中與產品總數不符。";
$MESS["NEWO_ERR_STORE_WRONG_INFO"] = "滿足順序的錯誤：關於\“＃product_name＃\”的信息無效。";
