<?php
$MESS["SKGOT_EMPTY_IN_PRICE"] = "未分配稅款\“包含在價格\”選項";
$MESS["SKGOT_EMPTY_ORDER_ID"] = "訂單ID未指定";
$MESS["SKGOT_EMPTY_SUM"] = "未指定稅額";
$MESS["SKGOT_EMPTY_SUM_MONEY"] = "未指定稅金";
$MESS["SKGOT_EMPTY_TAX_NAME"] = "稅名未指定";
$MESS["SKGOT_EMPTY_TAX_VALUE"] = "未指定稅率價值";
$MESS["SKGOT_NO_ORDER"] = "找不到訂單## ID＃";
