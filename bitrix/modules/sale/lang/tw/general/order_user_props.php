<?php
$MESS["SKGOUP_EMPTY_PERS_TYPE"] = "未指定付款人類型";
$MESS["SKGOUP_NO_USER_ID"] = "未指定用戶ID";
$MESS["SKGOUP_PARRAMS_ERROR"] = "無效方法調用參數。";
$MESS["SKGOUP_PROFILE_CREATE_ERROR"] = "無法創建客戶資料";
$MESS["SKGOUP_PROFILE_NOT_FOUND"] = "找不到輪廓。";
$MESS["SKGOUP_UNAUTH"] = "您沒有授權";
$MESS["SOA_PROFILE"] = "輪廓";
