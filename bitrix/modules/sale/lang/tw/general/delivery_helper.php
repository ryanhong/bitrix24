<?php
$MESS["SALE_DHLP_AVIABLE_BOXES"] = "計算的包裝大小（長度x寬度x高）";
$MESS["SALE_DHLP_CONTAIN"] = "可能只包括數字，十進制分離器";
$MESS["SALE_DHLP_FIELD"] = "場地";
$MESS["SALE_DHLP_HANDLER_HAS_NO_ACTION"] = "服務不支持此操作。";
$MESS["SALE_DHLP_HANDLER_REQUEST_ALREADY_SENT"] = "此請求已經發送。";
$MESS["SALE_DHLP_SEPARATOR"] = "和一千個分離器";
