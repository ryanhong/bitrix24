<?php
$MESS["BT_MOD_SALE_DSC_FORMAT_INVITE"] = "重要的！折扣將不活動，直到將其轉換為新格式為止。您必須轉換所有折扣才能激活它們。請在此處使用轉換器<a href= \“#link# \ \"> </a>。";
$MESS["BT_MOD_SALE_DSC_FORMAT_NAME"] = "＃值＃訂單的折扣總數＃從＃＃到＃";
$MESS["BT_MOD_SALE_DSC_FORMAT_NAME_FROM"] = "從＃值＃";
$MESS["BT_MOD_SALE_DSC_FORMAT_NAME_TO"] = "到＃值＃";
$MESS["BT_MOD_SALE_DSC_FORMAT_SHORT_NAME"] = "＃價值＃折扣";
