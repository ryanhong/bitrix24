<?php
$MESS["SALE_GOPE_FIELD_EMPTY"] = "需要字段“＃名＃”。";
$MESS["SALE_GOPE_WRONG_EMAIL"] = "電子郵件地址“＃電子郵件＃”在“＃名稱＃”中指定的是不正確的。";
$MESS["SKGOP_EMPTY_PERS_TYPE"] = "未指定付款人類型";
$MESS["SKGOP_EMPTY_PROP_GROUP"] = "未指定財產組";
$MESS["SKGOP_EMPTY_PROP_NAME"] = "屬性名稱未指定";
$MESS["SKGOP_EMPTY_PROP_TYPE"] = "未指定屬性類型";
$MESS["SKGOP_NO_PERS_TYPE"] = "找不到付款人類型## ID＃";
$MESS["SKGOP_WRONG_PROP_TYPE"] = "屬性類型不正確";
