<?php
$MESS["SKGP_BAD_SITE_NA"] = "沒有指定網站。";
$MESS["SKGP_ERROR_PERSON_HAS_ARCHIVE"] = "存檔訂單使用付款人類型ID ## ID＃。";
$MESS["SKGP_ERROR_PERSON_HAS_ORDER"] = "錯誤刪除記錄。有一些使用此人類型的訂單。";
$MESS["SKGP_NO_NAME_TP"] = "未指定付款人類型名稱";
$MESS["SKGP_NO_SITE"] = "找不到網站## ID＃";
$MESS["SKGP_PERSON_TYPE_EMPTY"] = "沒有付款人類型。";
$MESS["SKGP_PERSON_TYPE_NOT_FOUND"] = "付款人類型不正確。";
