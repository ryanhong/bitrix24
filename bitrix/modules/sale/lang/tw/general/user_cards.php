<?php
$MESS["SKGUC_CROSS_BOUND"] = "此卡的付款限額為＃sum1＃，而您嘗試支付＃sum2＃";
$MESS["SKGUC_EMPTY_CURRENCY"] = "未指定支付貨幣";
$MESS["SKGUC_EMPTY_ID"] = "記錄ID未指定";
$MESS["SKGUC_EMPTY_SUM"] = "未指定付款金額";
$MESS["SKGUC_NO_ACTION"] = "找不到付款系統的服務## ID＃";
$MESS["SKGUC_NO_CURRENCY"] = "未指定付款限額的貨幣";
$MESS["SKGUC_NO_ID"] = "記錄的ID未指定";
$MESS["SKGUC_NO_PARAMS"] = "信用卡參數陣列未指定";
$MESS["SKGUC_NO_PATH"] = "付款系統處理器的路徑\“＃文件＃\”無效";
$MESS["SKGUC_NO_PS"] = "找不到付款系統的處理器## ID＃";
$MESS["SKGUC_NO_RECID"] = "找不到記錄## ID＃";
$MESS["SKGUC_NO_SCRIPT"] = "找不到付款系統集成腳本\“＃文件＃\”";
$MESS["SKGUC_NO_USER"] = "用戶## ID＃找不到";
