<?php
$MESS["ACGA1_BAD_DATE"] = "會員創建日期未指定或不正確";
$MESS["ACGA1_BAD_FUNC1"] = "函數調用不正確";
$MESS["ACGA1_ERROR_FUNC"] = "錯誤調用功能";
$MESS["ACGA1_ERROR_TRANSF_MONEY"] = "將資金轉移到內部帳戶時發生了錯誤。";
$MESS["ACGA1_ERROR_UPDATE_SUM"] = "錯誤更新待處理金額記錄";
$MESS["ACGA1_NO_AFFILIATE"] = "沒有找到與ID＃ID＃的關聯";
$MESS["ACGA1_NO_PLAN"] = "計劃ID未指定";
$MESS["ACGA1_NO_PLAN_DEACT"] = "找不到適合的計劃＃id＃。會員停用。";
$MESS["ACGA1_NO_SITE"] = "未指定計劃網站";
$MESS["ACGA1_NO_USER"] = "未指定用戶ID";
$MESS["ACGA1_TRANSF_MONEY"] = "。資金已轉移到內部帳戶。";
$MESS["AF_ERROR_USER"] = "用戶## user_id＃在電子店模塊中具有關聯符，因此無法刪除。";
$MESS["SKGU_NO_USER"] = "用id =＃id＃的用戶找不到";
