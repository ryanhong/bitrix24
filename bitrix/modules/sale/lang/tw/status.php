<?php
$MESS["SIM_ACCEPTED"] = "公認";
$MESS["SIM_ACCEPTED_DESCR"] = "訂單已被接受，但尚未處理（例如：可能剛剛創建訂單或等待付款）";
$MESS["SIM_FINISHED"] = "完全的";
$MESS["SIM_FINISHED_DESCR"] = "訂單已交付並支付";
