<?php
$MESS["SALE_HANDLERS_DISCOUNTPRESET_CP_DISCOUNT_PH_DISCOUNT_VALUE"] = "折扣";
$MESS["SALE_HANDLERS_DISCOUNTPRESET_CP_DISCOUNT_PH_LINK"] = "關聯";
$MESS["SALE_HANDLERS_DISCOUNTPRESET_CP_DISCOUNT_PH_NAME"] = "姓名";
$MESS["SALE_HANDLERS_DISCOUNTPRESET_CP_DISCOUNT_PREDICTION_TEXT"] = "預測文本";
$MESS["SALE_HANDLERS_DISCOUNTPRESET_CP_DISCOUNT_PREDICTION_TEXT_DEFAULT_ACT"] = "<span class = \“ catalog-element-popup-info \”>獲取<span class = \“ catalog-element-popup-element \”>折扣＃discount_value＃</span>當您購買<a href ='時＃鏈接＃'target ='_空白'>＃名稱＃</a> </span>";
$MESS["SALE_HANDLERS_DISCOUNTPRESET_CP_DISCOUNT_PREDICTION_TEXT_DEFAULT_COND"] = "<span class = \“ catalog-element-popup-info \”>購買此項目以獲取<span class = \“ catalog-element-popup-element \”>折扣#discount_value＃</span> for <a href = '＃鏈接＃'target ='_空白'>＃名稱＃</a> </span>";
$MESS["SALE_HANDLERS_DISCOUNTPRESET_CP_DISCOUNT_VALUE"] = "折扣";
$MESS["SALE_HANDLERS_DISCOUNTPRESET_CP_NAME"] = "產品套裝折扣";
$MESS["SALE_HANDLERS_DISCOUNTPRESET_CP_STEP_DESCR_FOR_DISCOUNT"] = "選擇系統將提供折扣的產品或部分。您將選擇將在下一步激活這些折扣的觸發產品。";
$MESS["SALE_HANDLERS_DISCOUNTPRESET_CP_STEP_DESCR_INPUT_NAME"] = "當產品被添加到購物車中時，可以將系統設置為為其他產品或部分創建折扣，這可以激發客戶進行進一步購買，並最終增加平均訂單總額。";
$MESS["SALE_HANDLERS_DISCOUNTPRESET_CP_STEP_DESCR_WHEN_DISCOUNT"] = "選擇將激活上一步中指定的折扣的產品。";
$MESS["SALE_HANDLERS_DISCOUNTPRESET_CP_STEP_TITLE_DISCOUNT_FOR"] = "產品/部分折扣";
$MESS["SALE_HANDLERS_DISCOUNTPRESET_CP_STEP_TITLE_DISCOUNT_WHEN"] = "產品/部分的折扣";
$MESS["SALE_HANDLERS_DISCOUNTPRESET_ERROR_PRODUCT_NON_ARRAY"] = "選擇的產品具有無效的值";
$MESS["SALE_HANDLERS_DISCOUNTPRESET_ERROR_SECTION_NON_ARRAY"] = "選擇的部分具有無效的值";
