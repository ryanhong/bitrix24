<?php
$MESS["SALE_HPS_WEBMONEY_INVS_NO"] = "帳戶號 - ＃invs_no＃";
$MESS["SALE_HPS_WEBMONEY_PAYEE_PURSE"] = "商人錢包 - ＃payee_purse＃";
$MESS["SALE_HPS_WEBMONEY_PAYER_PURSE"] = "客戶錢包 - ＃Payer_purse＃";
$MESS["SALE_HPS_WEBMONEY_PAYER_WM"] = "客戶wmid-＃payer_wm＃";
$MESS["SALE_HPS_WEBMONEY_PAYMER_EMAIL"] = "Paymer.com客戶電子郵件 - ＃Paymer_email＃";
$MESS["SALE_HPS_WEBMONEY_PAYMER_NUMBER"] = "VM卡＃ - ＃Paymer_number＃";
$MESS["SALE_HPS_WEBMONEY_TELEPAT_ORDERID"] = "Telepath付款 - ＃Telepat_orderID＃";
$MESS["SALE_HPS_WEBMONEY_TELEPAT_PHONENUMBER"] = "客戶電話 - ＃telepat_phonenumber＃";
$MESS["SALE_HPS_WEBMONEY_TEST"] = "測試模式，沒有實際的基金轉移；";
$MESS["SALE_HPS_WEBMONEY_TRANS_DATE"] = "付款日期 - ＃trans_date＃";
$MESS["SALE_HPS_WEBMONEY_TRANS_NO"] = "付款號 - ＃trans_no＃";
