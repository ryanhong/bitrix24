<?php
$MESS["SALE_HPS_UAPAY"] = "UAPAY";
$MESS["SALE_HPS_UAPAY_CALLBACK_URL"] = "發送後回复";
$MESS["SALE_HPS_UAPAY_CALLBACK_URL_DESC"] = "UAPAY將向此地址發送回复。需要一個完全合格的地址，包括協議。";
$MESS["SALE_HPS_UAPAY_CHANGE_STATUS_PAY"] = "當收到付款成功狀態時，自動更改訂單狀態為已付款";
$MESS["SALE_HPS_UAPAY_CLIENT_ID"] = "客戶ID";
$MESS["SALE_HPS_UAPAY_INVOICE_DESCRIPTION"] = "付款說明";
$MESS["SALE_HPS_UAPAY_INVOICE_DESCRIPTION_DESC"] = "文本可能包括：＃payment_id＃ - 付款ID，＃order_id＃ - 訂購ID，＃payment_number＃ - 付款參考，＃order_number＃ - 訂購參考，＃user_email＃ - 客戶電子郵件";
$MESS["SALE_HPS_UAPAY_INVOICE_DESCRIPTION_TEMPLATE"] = "付款## payment_number＃for Order ## order_number＃for＃user_email＃";
$MESS["SALE_HPS_UAPAY_PUBLIC_DESCRIPTION"] = "使用任何烏克蘭銀行簽發的簽證或萬事達卡卡。該卡必須被激活以進行在線付款（請聯繫您的銀行客戶支持）。";
$MESS["SALE_HPS_UAPAY_REDIRECT_URL"] = "成功付款後，將客戶重定向到此頁面";
$MESS["SALE_HPS_UAPAY_REDIRECT_URL_DESC"] = "需要一個完全合格的地址，包括協議。將此字段空白地將客戶重定向到啟動付款的頁面";
$MESS["SALE_HPS_UAPAY_SIGN_KEY"] = "簽名密鑰";
$MESS["SALE_HPS_UAPAY_SIGN_KEY_DESC"] = "所有對UAPAY API的請求都必須使用您可以在UAPAY帳戶中獲得的密鑰簽名。";
$MESS["SALE_HPS_UAPAY_TEST_MODE"] = "測試模式";
$MESS["SALE_HPS_UAPAY_TEST_MODE_DESC"] = "選擇此選項啟用測試模式。";
