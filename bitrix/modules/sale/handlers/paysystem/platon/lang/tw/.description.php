<?php
$MESS["SALE_HPS_PLATON"] = "柏拉圖";
$MESS["SALE_HPS_PLATON_API_KEY"] = "API鍵";
$MESS["SALE_HPS_PLATON_API_KEY_DESCRIPTION"] = "此鍵已發送到商人的電子郵件地址";
$MESS["SALE_HPS_PLATON_DESCRIPTION"] = "通過Platon接收在線付款。接受的付款方式：信用卡和借記卡，Apple Pay，Google Pay，Privat24";
$MESS["SALE_HPS_PLATON_INVOICE_DESCRIPTION_DEFAULT_TEMPLATE"] = "付款## payment_number＃for Order ## order_number＃for＃user_email＃";
$MESS["SALE_HPS_PLATON_MODE_APPLE_PAY"] = "通過Apple Pay付款";
$MESS["SALE_HPS_PLATON_MODE_CARD"] = "信用卡/借記卡付款";
$MESS["SALE_HPS_PLATON_MODE_GOOGLE_PAY"] = "通過Google付款";
$MESS["SALE_HPS_PLATON_MODE_PRIVAT24"] = "通過Privat24付款";
$MESS["SALE_HPS_PLATON_PASSWORD"] = "API密碼";
$MESS["SALE_HPS_PLATON_PAYMENT_DESCRIPTION"] = "付款說明";
$MESS["SALE_HPS_PLATON_PAYMENT_DESCRIPTION_DESCRIPTION"] = "文本可能包括：＃payment_id＃ - 付款ID，＃order_id＃ - 訂購ID，＃payment_number＃ - 付款參考，＃order_number＃ - 訂購參考，＃user_email＃ - 客戶電子郵件";
$MESS["SALE_HPS_PLATON_SUCCESS_URL"] = "成功付款後，將付款人重定向到此頁面";
$MESS["SALE_HPS_PLATON_SUCCESS_URL_DESCRIPTION"] = "需要一個完全合格的地址，包括協議。將此字段空白地將付款人重定向到啟動付款的頁面。";
