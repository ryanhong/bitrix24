<?php
$MESS["SALE_HANDLERS_PAY_SYSTEM_SBERBANK_BUTTON_PAID"] = "支付";
$MESS["SALE_HANDLERS_PAY_SYSTEM_SBERBANK_DESCRIPTION"] = "發票金額：";
$MESS["SALE_HANDLERS_PAY_SYSTEM_SBERBANK_REDIRECT"] = "現在，您將被重定向到付款頁面";
$MESS["SALE_HANDLERS_PAY_SYSTEM_SBERBANK_WARNING_RETURN"] = "<b>注意：</b>如果要退還訂單，則必須與賣方聯繫。";
