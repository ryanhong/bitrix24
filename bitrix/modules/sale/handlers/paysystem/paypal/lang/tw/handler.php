<?php
$MESS["SALE_HPS_PAYPAL_I1"] = "付款不成功。";
$MESS["SALE_HPS_PAYPAL_I3"] = "您的交易已經完成，您的購買收據已通過電子郵件發送給您。<br>您可以通過<a href= \"https://www.paypal.com \"> www.paypal登錄到您的帳戶。 com </a>查看此交易的詳細信息。";
$MESS["SALE_HPS_PAYPAL_I4"] = "您可以在<a href= \"/personal/xhip的個人部分中查看訂單狀態</a>。";
$MESS["SALE_HPS_PAYPAL_T1"] = "感謝您的購買！";
$MESS["SALE_HPS_PAYPAL_T2"] = "付款詳情";
$MESS["SALE_HPS_PAYPAL_T3"] = "姓名";
$MESS["SALE_HPS_PAYPAL_T4"] = "物品";
$MESS["SALE_HPS_PAYPAL_T5"] = "數量";
