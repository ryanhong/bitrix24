<?php
$MESS["SALE_HPS_WOOPPAY"] = "Woopkassa";
$MESS["SALE_HPS_WOOPPAY_BACK_URL"] = "付款時將客戶重定向到此頁面";
$MESS["SALE_HPS_WOOPPAY_BACK_URL_DESC"] = "頁面地址必須完全合格，包括協議。將字段留空自動重定向到上一頁。";
$MESS["SALE_HPS_WOOPPAY_CHANGE_STATUS_PAY"] = "當收到付款成功狀態時，自動更改訂單狀態為已付款";
$MESS["SALE_HPS_WOOPPAY_CHECKOUT_MODE"] = "付款頁";
$MESS["SALE_HPS_WOOPPAY_CHECKOUT_MODE_DESCRIPTION"] = "接受銀行卡，手機或Wooppay錢包中的在線付款。 Woopkassa通過哈薩克斯坦的第一份註冊付款服務Wooppay支持付款。";
$MESS["SALE_HPS_WOOPPAY_DESCRIPTION_MAIN"] = "在將其集成到解決方案時，您將獲得正確的信息，以填寫Woopkassa工程師之一的字段。";
$MESS["SALE_HPS_WOOPPAY_LOGIN"] = "登入";
$MESS["SALE_HPS_WOOPPAY_PASSWORD"] = "密碼";
$MESS["SALE_HPS_WOOPPAY_PAYMENT_DESCRIPTION"] = "付款說明";
$MESS["SALE_HPS_WOOPPAY_PAYMENT_DESCRIPTION_DESC"] = "付款說明客戶將在付款頁面上看到。以下宏是可能的：＃payment_id＃ - 付款ID； ＃order_id＃ - 訂單ID; ＃payment_number＃ - 付款＃; ＃order_number＃ - 訂單＃; ＃USER_EMAIL＃ - 客戶電子郵件";
$MESS["SALE_HPS_WOOPPAY_PAYMENT_DESCRIPTION_TEMPLATE"] = "付款## payment_number＃for Order ## order_number＃for＃user_email＃";
$MESS["SALE_HPS_WOOPPAY_REQUEST_URL"] = "Woopkassa通知地址";
$MESS["SALE_HPS_WOOPPAY_REQUEST_URL_DESC"] = "該地址必須完全合格，包括協議。";
$MESS["SALE_HPS_WOOPPAY_SERVICE_NAME"] = "系統服務名稱";
$MESS["SALE_HPS_WOOPPAY_TEST_MODE"] = "測試模式";
$MESS["SALE_HPS_WOOPPAY_TEST_MODE_DESC"] = "選擇此選項啟用測試模式。";
