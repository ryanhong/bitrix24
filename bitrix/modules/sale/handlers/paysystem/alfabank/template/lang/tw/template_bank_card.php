<?php
$MESS["SALE_HANDLERS_PAY_SYSTEM_ALFABANK_BUTTON_PAID"] = "支付";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ALFABANK_DESCRIPTION"] = "發票金額：＃sum＃";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ALFABANK_REDIRECT"] = "現在，您將被重定向到付款頁面";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ALFABANK_WARNING_RETURN"] = "<b>注意：</b>如果要退還訂單，則必須與賣方聯繫。";
