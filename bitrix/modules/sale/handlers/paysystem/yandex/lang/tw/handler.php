<?php
$MESS["SALE_HPS_YANDEX_AlphaClick"] = "Alfaclick";
$MESS["SALE_HPS_YANDEX_CUSTOMER_REJECTION"] = "客戶拒絕了";
$MESS["SALE_HPS_YANDEX_Cards"] = "金融卡";
$MESS["SALE_HPS_YANDEX_DATE_PAYED"] = "付款日期";
$MESS["SALE_HPS_YANDEX_MasterPass"] = "MasterPass";
$MESS["SALE_HPS_YANDEX_Mobile"] = "移動付款";
$MESS["SALE_HPS_YANDEX_Promsvyazbank"] = "PSB";
$MESS["SALE_HPS_YANDEX_Qiwi"] = "Qiwi錢包";
$MESS["SALE_HPS_YANDEX_REFUND_CONNECTION_ERROR"] = "連接錯誤。 URL：＃URL＃。 HTTP代碼：＃代碼＃。錯誤：＃錯誤＃";
$MESS["SALE_HPS_YANDEX_REFUND_ERROR"] = "處理倒款時錯誤。";
$MESS["SALE_HPS_YANDEX_REFUND_ERROR_INFO"] = "狀態：＃狀態＃。錯誤代碼：＃錯誤＃";
$MESS["SALE_HPS_YANDEX_Sberbank"] = "Sberbank在線";
$MESS["SALE_HPS_YANDEX_Smart"] = "明智的付款";
$MESS["SALE_HPS_YANDEX_TRANSACTION"] = "交易";
$MESS["SALE_HPS_YANDEX_Terminals"] = "終端";
$MESS["SALE_HPS_YANDEX_TinkoffBank"] = "Kupivkredit";
$MESS["SALE_HPS_YANDEX_YKuppiRu"] = "值得信賴的付款";
$MESS["SALE_HPS_YANDEX_YMoney"] = "yoomoney";
$MESS["SALE_HPS_YANDEX_mPOS"] = "移動終端（MPO）";
