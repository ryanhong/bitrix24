<?php
$MESS["SALE_HPS_PAYMASTER_DESC_MERCHANT_ID"] = "商品錢包 - ＃merchant_id＃;";
$MESS["SALE_HPS_PAYMASTER_DESC_PAYER_EMAIL"] = "paymer.com的客戶電子郵件 - ＃paymer_email＃;";
$MESS["SALE_HPS_PAYMASTER_DESC_PAYER_NUMBER"] = "VM卡＃ - ＃Paymer_number＃;";
$MESS["SALE_HPS_PAYMASTER_DESC_PAYER_PURSE"] = "客戶錢包 - ＃payer_purse＃;";
$MESS["SALE_HPS_PAYMASTER_DESC_PAYER_WM"] = "客戶wmid-＃payer_wm＃;";
$MESS["SALE_HPS_PAYMASTER_DESC_PAY_SYSTEM"] = "付款系統 - ＃pay_system＃;";
$MESS["SALE_HPS_PAYMASTER_DESC_SYS_INVS_NO"] = "發票編號 - ＃sys_invs_no＃;";
$MESS["SALE_HPS_PAYMASTER_DESC_SYS_TRANS_DATE"] = "付款日期 - ＃sys_trans_date＃;";
$MESS["SALE_HPS_PAYMASTER_DESC_SYS_TRANS_NO"] = "付款號 - ＃sys_trans_no＃;";
$MESS["SALE_HPS_PAYMASTER_DESC_TELEPAT_ORDERID"] = "Telepath付款 - ＃telepat_orderid＃;";
$MESS["SALE_HPS_PAYMASTER_DESC_TELEPAT_PHONENUMBER"] = "客戶電話 - ＃telepat_phonenumber＃;";
$MESS["SALE_HPS_PAYMASTER_ERROR_PARAMS_VALUE"] = "付款參數不匹配；";
$MESS["SALE_HPS_PAYMASTER_SIM_MODE_TEST"] = "測試模式，沒有實際的基金轉移；";
