<?php
$MESS["SALE_HPS_ADYEN_ERROR_ACTION"] = "沒有找到動作\“＃動作＃\”";
$MESS["SALE_HPS_ADYEN_ERROR_CHECK_SUM"] = "校驗和不正確。";
$MESS["SALE_HPS_ADYEN_ERROR_QUERY"] = "服務器響應不正確或空";
$MESS["SALE_HPS_ADYEN_ERROR_SUM"] = "付款金額不符合應付金額";
$MESS["SALE_HPS_ADYEN_ERROR_URL"] = "無法獲取網址：＃動作＃";
$MESS["SALE_HPS_ADYEN_HTTP_STATUS"] = "錯誤獲得響應。狀態：＃http_status＃";
$MESS["SALE_HPS_ADYEN_SEND_REFUSAL_REASON_ERROR"] = "錯誤創建付款。錯誤代碼：＃result_code＃，原因：＃reclusal_reason＃";
$MESS["SALE_HPS_ADYEN_SEND_RESULT_CODE_ERROR"] = "錯誤創建付款。錯誤代碼：＃result_code＃";
$MESS["SALE_HPS_ADYEN_TRANSACTION"] = "交易";
