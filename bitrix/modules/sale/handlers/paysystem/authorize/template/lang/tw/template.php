<?php
$MESS["AN_CC"] = "信用卡號碼：";
$MESS["AN_CC_BUTTON"] = "繼續";
$MESS["AN_CC_CVV2"] = "CVV2（信用卡背面的三位數或四位數號）：";
$MESS["AN_CC_DATE"] = "信用卡到期的日期：";
$MESS["SALE_HPS_AUTHORIZE_ERROR_MESSAGE_FOOTER"] = "請選擇其他付款選項或聯繫銷售代表。";
$MESS["SALE_HPS_AUTHORIZE_ERROR_MESSAGE_HEADER"] = "不幸的是有一個錯誤。";
