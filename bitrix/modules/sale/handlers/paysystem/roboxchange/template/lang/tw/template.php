<?php
$MESS["SALE_HANDLERS_PAY_SYSTEM_TEMPLATE_ROBOXCHANGE_CHECKOUT_BUTTON_PAID"] = "支付";
$MESS["SALE_HANDLERS_PAY_SYSTEM_TEMPLATE_ROBOXCHANGE_CHECKOUT_DESCRIPTION"] = "由<b>提供的服務。";
$MESS["SALE_HANDLERS_PAY_SYSTEM_TEMPLATE_ROBOXCHANGE_CHECKOUT_SUM"] = "訂單總計：＃sum＃";
$MESS["SALE_HANDLERS_PAY_SYSTEM_TEMPLATE_ROBOXCHANGE_CHECKOUT_WARNING_RETURN"] = "<b>注意：</b>如果要退還訂單，則必須與賣方聯繫。";
