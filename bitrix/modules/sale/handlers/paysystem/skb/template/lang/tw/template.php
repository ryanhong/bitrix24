<?php
$MESS["SALE_HANDLERS_PAY_SYSTEM_SKB_BUTTON_PAID"] = "支付";
$MESS["SALE_HANDLERS_PAY_SYSTEM_SKB_DESCRIPTION_MSGVER_1"] = "服務由<span class ='widget-paysystem-name'> sinara bank </span>提供。";
$MESS["SALE_HANDLERS_PAY_SYSTEM_SKB_DESCRIPTION_SUM"] = "發票總計：<span class ='widget-paysystem-checkout-sum'>＃sum＃</span>";
$MESS["SALE_HANDLERS_PAY_SYSTEM_SKB_QR_CODE_HINT"] = "使用銀行的移動應用程序掃描QR碼並支付訂單。";
$MESS["SALE_HANDLERS_PAY_SYSTEM_SKB_REDIRECT"] = "現在，您將被重定向到付款頁面";
$MESS["SALE_HANDLERS_PAY_SYSTEM_SKB_WARNING_RETURN"] = "<b>注意：</b>如果要退還訂單，則必須與賣方聯繫。";
