<?php
$MESS["SALE_HPS_SKB"] = "Sinara銀行";
$MESS["SALE_HPS_SKB_ADDITIONAL_INFO"] = "附加信息";
$MESS["SALE_HPS_SKB_ADDITIONAL_INFO_DESC"] = "額外信息（最多140個字符）。可能的宏是可能的：＃payment_id＃ - 付款ID，＃order_id＃ - 訂購ID，＃payment_number＃ - 付款no。，＃order_number＃ - 訂購no。，＃USER_EMAIL＃ - 客戶電子郵件";
$MESS["SALE_HPS_SKB_ADDITIONAL_INFO_TEMPLATE"] = "付款## payment_number＃for Order ## order_number＃for＃user_email＃";
$MESS["SALE_HPS_SKB_CHANGE_STATUS_PAY"] = "當收到付款成功狀態時，自動更改訂單狀態為已付款";
$MESS["SALE_HPS_SKB_DESCRIPTION_MAIN"] = "使用QR碼接受付款。";
$MESS["SALE_HPS_SKB_LOGIN"] = "登入";
$MESS["SALE_HPS_SKB_LOGIN_DESC"] = "TSP密碼";
$MESS["SALE_HPS_SKB_MERCHANT_ID"] = "TSP ID";
$MESS["SALE_HPS_SKB_MODE_DELOBANK"] = "Delobank";
$MESS["SALE_HPS_SKB_MODE_GAZENERGOBANK"] = "Gazenergobank";
$MESS["SALE_HPS_SKB_MODE_SKB"] = "快速支付系統";
$MESS["SALE_HPS_SKB_MODE_SKB_PUBLIC_DESCRIPTION"] = "使用QR碼付款使用任何支持快速支付系統的銀行申請。";
$MESS["SALE_HPS_SKB_PASSWORD"] = "密碼";
$MESS["SALE_HPS_SKB_PASSWORD_DESC"] = "TSP密碼";
$MESS["SALE_HPS_SKB_SECRET_KEY"] = "密鑰";
$MESS["SALE_HPS_SKB_TEST_MODE"] = "測試模式";
