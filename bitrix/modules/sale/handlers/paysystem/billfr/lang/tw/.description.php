<?php
$MESS["SALE_HPS_BILL_FR_ACC_POS_SUPPLI"] = "會計師職位";
$MESS["SALE_HPS_BILL_FR_ACC_POS_SUPPLI_DESC"] = "會計師職位（銷售）";
$MESS["SALE_HPS_BILL_FR_ACC_POS_SUPPLI_VAL"] = "會計師";
$MESS["SALE_HPS_BILL_FR_ACC_SIGN_SUPPLI"] = "會計師的簽名（建議尺寸200x50 PX）";
$MESS["SALE_HPS_BILL_FR_ACC_SIGN_SUPPLI_DESC"] = "會計師的電子簽名";
$MESS["SALE_HPS_BILL_FR_ACC_SUPPLI"] = "會計全名";
$MESS["SALE_HPS_BILL_FR_ACC_SUPPLI_DESC"] = "會計（銷售）全名";
$MESS["SALE_HPS_BILL_FR_ADRESS_SUPPLI"] = "賣方的地址";
$MESS["SALE_HPS_BILL_FR_ADRESS_SUPPLI_DESC"] = "賣方的物理地址";
$MESS["SALE_HPS_BILL_FR_BACKGROUND"] = "背景（建議尺寸800x1120 PX）";
$MESS["SALE_HPS_BILL_FR_BACKGROUND_DESC"] = "發票的背景圖像";
$MESS["SALE_HPS_BILL_FR_BACKGROUND_STYLE"] = "背景圖像樣式";
$MESS["SALE_HPS_BILL_FR_BACKGROUND_STYLE_NONE"] = "沒有任何";
$MESS["SALE_HPS_BILL_FR_BACKGROUND_STYLE_STRETCH"] = "全螢幕";
$MESS["SALE_HPS_BILL_FR_BACKGROUND_STYLE_TILE"] = "瓦";
$MESS["SALE_HPS_BILL_FR_BANK_ACCNO_SUPPLI"] = "賣方的銀行帳號";
$MESS["SALE_HPS_BILL_FR_BANK_ACCNO_SUPPLI_DESC"] = "賣方的銀行帳號";
$MESS["SALE_HPS_BILL_FR_BANK_ACCNO_SUPPLI_VAL"] = "賣方的銀行帳號";
$MESS["SALE_HPS_BILL_FR_BANK_ADDR_SUPPLI"] = "賣方銀行的地址";
$MESS["SALE_HPS_BILL_FR_BANK_ADDR_SUPPLI_DESC"] = "賣方銀行的地址";
$MESS["SALE_HPS_BILL_FR_BANK_ADDR_SUPPLI_VAL"] = "賣方銀行的地址";
$MESS["SALE_HPS_BILL_FR_BANK_PHONE_SUPPLI"] = "賣方銀行的電話";
$MESS["SALE_HPS_BILL_FR_BANK_PHONE_SUPPLI_DESC"] = "賣方銀行的電話";
$MESS["SALE_HPS_BILL_FR_BANK_PHONE_SUPPLI_VAL"] = "賣方銀行的電話";
$MESS["SALE_HPS_BILL_FR_BANK_ROUTENO_SUPPLI"] = "賣方的相應帳戶";
$MESS["SALE_HPS_BILL_FR_BANK_ROUTENO_SUPPLI_DESC"] = "賣方的相應帳戶";
$MESS["SALE_HPS_BILL_FR_BANK_ROUTENO_SUPPLI_VAL"] = "賣方的相應帳戶";
$MESS["SALE_HPS_BILL_FR_BANK_SUPPLI"] = "賣方銀行的名稱";
$MESS["SALE_HPS_BILL_FR_BANK_SUPPLI_DESC"] = "賣方帳戶所在的銀行名稱";
$MESS["SALE_HPS_BILL_FR_BANK_SUPPLI_VAL"] = "賣方銀行的名稱";
$MESS["SALE_HPS_BILL_FR_BANK_SWIFT_SUPPLI"] = "快速賣方銀行的守則";
$MESS["SALE_HPS_BILL_FR_BANK_SWIFT_SUPPLI_DESC"] = "快速賣方銀行的守則";
$MESS["SALE_HPS_BILL_FR_BANK_SWIFT_SUPPLI_VAL"] = "快速賣方銀行的守則";
$MESS["SALE_HPS_BILL_FR_COLUMN_MEASURE_VALUE"] = "單位";
$MESS["SALE_HPS_BILL_FR_COLUMN_NAME_VALUE"] = "商品描述";
$MESS["SALE_HPS_BILL_FR_COLUMN_NUMBER_VALUE"] = "＃";
$MESS["SALE_HPS_BILL_FR_COLUMN_PRICE_VALUE"] = "單價";
$MESS["SALE_HPS_BILL_FR_COLUMN_QUANTITY_VALUE"] = "數量";
$MESS["SALE_HPS_BILL_FR_COLUMN_SHOW"] = "積極的";
$MESS["SALE_HPS_BILL_FR_COLUMN_SORT"] = "種類";
$MESS["SALE_HPS_BILL_FR_COLUMN_SUM_VALUE"] = "全部的";
$MESS["SALE_HPS_BILL_FR_COLUMN_TITLE"] = "列標題";
$MESS["SALE_HPS_BILL_FR_COLUMN_VAT_RATE_VALUE"] = "稅率";
$MESS["SALE_HPS_BILL_FR_COMMENT1"] = "評論1發票";
$MESS["SALE_HPS_BILL_FR_COMMENT2"] = "評論2發票";
$MESS["SALE_HPS_BILL_FR_CUSTOMER"] = "買方";
$MESS["SALE_HPS_BILL_FR_CUSTOMER_ADRES"] = "買方的地址";
$MESS["SALE_HPS_BILL_FR_CUSTOMER_ADRES_DESC"] = "收款人的地址";
$MESS["SALE_HPS_BILL_FR_CUSTOMER_DESC"] = "付款公司 /付款人的名稱";
$MESS["SALE_HPS_BILL_FR_CUSTOMER_PERSON"] = "聯絡人";
$MESS["SALE_HPS_BILL_FR_CUSTOMER_PERSON_DESC"] = "與買家聯繫";
$MESS["SALE_HPS_BILL_FR_DATE"] = "訂購日期";
$MESS["SALE_HPS_BILL_FR_DATE_DESC"] = "創建訂單的日期";
$MESS["SALE_HPS_BILL_FR_DIR_POS_SUPPLI"] = "主管工作職位";
$MESS["SALE_HPS_BILL_FR_DIR_POS_SUPPLI_DESC"] = "主管工作職位（銷售）";
$MESS["SALE_HPS_BILL_FR_DIR_POS_SUPPLI_VAL"] = "導演";
$MESS["SALE_HPS_BILL_FR_DIR_SIGN_SUPPLI"] = "董事的簽名（建議尺寸200x50 PX）";
$MESS["SALE_HPS_BILL_FR_DIR_SIGN_SUPPLI_DESC"] = "董事的電子簽名";
$MESS["SALE_HPS_BILL_FR_DIR_SUPPLI"] = "主管全名";
$MESS["SALE_HPS_BILL_FR_DIR_SUPPLI_DESC"] = "主管（銷售）全名";
$MESS["SALE_HPS_BILL_FR_HEADER"] = "發票標題";
$MESS["SALE_HPS_BILL_FR_HEADER_VALUE"] = "發票";
$MESS["SALE_HPS_BILL_FR_LOGO"] = "供應商徽標（建議尺寸80x80 PX）";
$MESS["SALE_HPS_BILL_FR_LOGO_DESC"] = "供應商徽標";
$MESS["SALE_HPS_BILL_FR_LOGO_DPI"] = "徽標刻度";
$MESS["SALE_HPS_BILL_FR_LOGO_DPI_72"] = "133％（72 DPI）";
$MESS["SALE_HPS_BILL_FR_LOGO_DPI_96"] = "100％（96 DPI）";
$MESS["SALE_HPS_BILL_FR_LOGO_DPI_150"] = "64％（150 dpi）";
$MESS["SALE_HPS_BILL_FR_LOGO_DPI_300"] = "32％（300 dpi）";
$MESS["SALE_HPS_BILL_FR_LOGO_DPI_600"] = "16％（600 dpi）";
$MESS["SALE_HPS_BILL_FR_MARGIN_BOTTOM"] = "底部邊距（mm）";
$MESS["SALE_HPS_BILL_FR_MARGIN_LEFT"] = "左邊（mm）";
$MESS["SALE_HPS_BILL_FR_MARGIN_RIGHT"] = "右邊邊距（mm）";
$MESS["SALE_HPS_BILL_FR_MARGIN_TOP"] = "最高邊距（mm）";
$MESS["SALE_HPS_BILL_FR_PAYER_SHOW"] = "顯示付款人信息";
$MESS["SALE_HPS_BILL_FR_PAY_BEFORE"] = "付款";
$MESS["SALE_HPS_BILL_FR_PAY_BEFORE_DESC"] = "付款截止日期";
$MESS["SALE_HPS_BILL_FR_PHONE_SUPPLI"] = "賣方電話";
$MESS["SALE_HPS_BILL_FR_PHONE_SUPPLI_DESC"] = "賣方的電話號碼";
$MESS["SALE_HPS_BILL_FR_PRINT"] = "郵票（建議尺寸150x150 PX）";
$MESS["SALE_HPS_BILL_FR_PRINT_DESC"] = "供應商郵票圖像";
$MESS["SALE_HPS_BILL_FR_SUPPLI"] = "賣方";
$MESS["SALE_HPS_BILL_FR_SUPPLI_DESC"] = "收款人或賣方的名稱";
$MESS["SALE_HPS_BILL_FR_TITLE"] = "發票（法語）";
$MESS["SALE_HPS_BILL_FR_TOTAL_SHOW"] = "顯示摘要";
