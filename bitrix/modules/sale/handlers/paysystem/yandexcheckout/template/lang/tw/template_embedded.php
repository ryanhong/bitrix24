<?php
$MESS["SALE_HANDLERS_PAY_SYSTEM_YANDEX_CHECKOUT_DESCRIPTION_MSGVER_1"] = "服務由<span class ='widge-paysystem-name'> yoomoney </span>提供。";
$MESS["SALE_HANDLERS_PAY_SYSTEM_YANDEX_CHECKOUT_DESCRIPTION_SUM"] = "發票總計：<span class ='widget-paysystem-checkout-sum'>＃sum＃</span>";
$MESS["SALE_HANDLERS_PAY_SYSTEM_YANDEX_CHECKOUT_ERROR_MESSAGE"] = "不幸的是有一個錯誤。請選擇其他付款選項或聯繫銷售代表。";
$MESS["SALE_HANDLERS_PAY_SYSTEM_YANDEX_CHECKOUT_WARNING_RETURN"] = "<b>注意：</b>如果要退還訂單，則必須與賣方聯繫。";
