<?php
$MESS["SALE_DOCUMENT_HANDLER_DOWNLOAD_DOCUMENT"] = "<a href= \"#link# \" target= \"_blank \">單擊此處</a>以將發票作為PDF下載。";
$MESS["SALE_DOCUMENT_HANDLER_ERROR_MESSAGE_FOOTER"] = "選擇其他付款方式或聯繫我們的一位銷售代表。";
$MESS["SALE_DOCUMENT_HANDLER_ERROR_MESSAGE_HEADER"] = "不幸的是有一個錯誤。";
$MESS["SALE_DOCUMENT_HANDLER_WAIT_TRANSFORMATION"] = "您的發票現在正在打印。請稍等。";
