<?php
$MESS["SALE_HANDLERS_PAY_SYSTEM_TEMPLATE_BEPAID_ERIP_CHECKOUT_DESCRIPTION"] = "服務由<b>'bepaid＆raquo; </b>。";
$MESS["SALE_HANDLERS_PAY_SYSTEM_TEMPLATE_BEPAID_ERIP_CHECKOUT_SUM"] = "訂單總計：＃sum＃";
$MESS["SALE_HANDLERS_PAY_SYSTEM_TEMPLATE_BEPAID_ERIP_CHECKOUT_WARNING_RETURN"] = "<b>注意：</b>如果要退還訂單，則必須與賣方聯繫。";
$MESS["SALE_HANDLERS_PAY_SYSTEM_TEMPLATE_BEPAID_ERIP_INSTRUCTION"] = "
如果您要通過銀行收銀員付款，請讓店員知道您是通過Erip付款的。</br>
<br>
使用服務ID <strong> #ERIP_SERVICE_CODE＃</strong>在ERIP樹中找到商店，或按照以下步驟：<br>
<ol>
  <li>選擇ERIP </li>
  <li>選擇以下選項，一個一個：<i> #instruction＃</i> </li>
  <li>輸入號碼：<strong> #account_number＃</strong> </li>
  <li>確保信息正確</li>
  <li>已經付款了，您會嗎？</li>
</ol>";
$MESS["SALE_HANDLERS_PAY_SYSTEM_TEMPLATE_BEPAID_ERIP_QR_INSTRUCTION"] = "如果您使用的是銀行移動應用程序，請使用此應用程序掃描QR碼。";
