<?php
$MESS["SALE_HPS_BEPAID_ERIP"] = "bepaid erip";
$MESS["SALE_HPS_BEPAID_ERIP_CHANGE_STATUS_PAY"] = "當收到付款成功狀態時，自動更改訂單狀態為已付款";
$MESS["SALE_HPS_BEPAID_ERIP_DESCRIPTION_MAIN"] = "使用ERIP小部件進行更快的付款。";
$MESS["SALE_HPS_BEPAID_ERIP_IS_TEST"] = "測試模式";
$MESS["SALE_HPS_BEPAID_ERIP_NOTIFICATION_URL"] = "通知URL";
$MESS["SALE_HPS_BEPAID_ERIP_PAYMENT_DESCRIPTION"] = "訂單說明";
$MESS["SALE_HPS_BEPAID_ERIP_PAYMENT_DESCRIPTION_DESC"] = "文本可能包括：＃payment_id＃ - 付款ID，＃order_id＃ - 訂購ID，＃payment_number＃ - 付款參考，＃order_number＃ - 訂購參考，＃user_email＃ - 客戶電子郵件";
$MESS["SALE_HPS_BEPAID_ERIP_PAYMENT_DESCRIPTION_TEMPLATE"] = "付款## payment_number＃for Order ## order_number＃for＃user_email＃";
$MESS["SALE_HPS_BEPAID_ERIP_PUBLIC_KEY"] = "存儲公鑰";
$MESS["SALE_HPS_BEPAID_ERIP_SECRET_KEY"] = "商店秘密鑰匙";
$MESS["SALE_HPS_BEPAID_ERIP_SERVICE_CODE"] = "ERIP服務ID";
$MESS["SALE_HPS_BEPAID_ERIP_SERVICE_CODE_DESC"] = "可選，請指定單個ERIP上的多個服務。";
$MESS["SALE_HPS_BEPAID_ERIP_SHOP_ID"] = "存儲ID";
