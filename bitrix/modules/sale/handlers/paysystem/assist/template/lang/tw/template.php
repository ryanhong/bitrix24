<?php
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_ACCOUNT_NO"] = "發票 ＃";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_ACTION"] = "支付";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_BUTTON_PAID"] = "支付";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_DESCRIPTION"] = "<b> yoomoney提供的付款處理< /b>。<br /> <br />訂單總計：";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_NOTES"] = "賣方可能需要通過電話授權付款。從10:00到18:00（莫斯科時間），在工作時間內手動處理所有訂單。<br> <br>由於欺詐控制措施的增加，Visa和Eurocard/MasterCard處理Cernters將拒絕任何未指定的交易cvv2 /cvc2。<br> <br> <a href= \"http://www.assist.ru/ \ \"> Assist </a>付款處理器使用SSL來確保從客戶發送的涉及機密信息的交易安全性<br> <br>從客戶端獲得的信息通過私人網絡進一步發送，其被黑客入侵的機會幾乎沒有。<br> <br>在<a上執行了客戶機密信息的處理處理。 href = \ “ http：//www.alfabank.ru/ \”> alfa bank </a>處理中心。實際上，這意味著沒有人在內的任何人都無法訪問客戶的個人和銀行數據。<br> <br>";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_NOTES1"] = "為了防止未經授權訪問客戶端信息，使用SSL 3.0從客戶端到處理服務器傳輸數據。該服務器使用<a href= \"https://www.thawte.com \"> Thawte </a>發行的128位證書</a>。您可以<br> <br> <a href= \"https://sealinfo.thawte.com/thawtesplash?form_file=fdf/thawtesplash.fdf&dn = www.assist.ru＆lu＆lu＆lu＆luang = en \ \">驗證輔助證書</a >任何時間。";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_NOTES_TITLE"] = "訂單處理";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_NOTES_TITLE1"] = "信息安全";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_ORDER_FROM"] = "從";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_ORDER_SUM"] = "合計訂單：";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_PROMT"] = "您將使用<b> www.assist.ru </b>付款。";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_WARNING_RETURN"] = "<b>注意：</b>如果要退還訂單，則必須聯繫賣方。";
