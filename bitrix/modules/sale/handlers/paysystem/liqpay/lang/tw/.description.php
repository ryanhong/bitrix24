<?php
$MESS["SALE_HPS_LIQPAY_CURRENCY"] = "訂單貨幣";
$MESS["SALE_HPS_LIQPAY_DESCRIPTION"] = "LIQPAY付款";
$MESS["SALE_HPS_LIQPAY_MERCHANT_ID"] = "商人ID";
$MESS["SALE_HPS_LIQPAY_NAME"] = "liqpay";
$MESS["SALE_HPS_LIQPAY_ORDER_ID"] = "命令 ＃";
$MESS["SALE_HPS_LIQPAY_PATH_TO_RESULT_URL"] = "回到商店路徑";
$MESS["SALE_HPS_LIQPAY_PATH_TO_RESULT_URL_DESC"] = "客戶將重定向到的頁面。";
$MESS["SALE_HPS_LIQPAY_PATH_TO_SERVER_URL"] = "處理程序路徑";
$MESS["SALE_HPS_LIQPAY_PAYMENT_DESCRIPTION"] = "付款詳情";
$MESS["SALE_HPS_LIQPAY_PAYMENT_DESCRIPTION_DESC"] = "文本可能包括：＃payment_id＃ - 付款ID，＃order_id＃ - 訂購ID，＃payment_number＃ - 付款參考，＃order_number＃ - 訂購參考，＃user_email＃ - 客戶電子郵件";
$MESS["SALE_HPS_LIQPAY_PAYMENT_DESCRIPTION_TEMPLATE"] = "付款## payment_number＃for Order ## order_number＃for＃user_email＃";
$MESS["SALE_HPS_LIQPAY_PAYMENT_PM"] = "付款方式";
$MESS["SALE_HPS_LIQPAY_PHONE"] = "客戶電話";
$MESS["SALE_HPS_LIQPAY_SHOULD_PAY"] = "總金額";
$MESS["SALE_HPS_LIQPAY_SIGN"] = "簽名";
