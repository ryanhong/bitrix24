<?php
$MESS["ADDITIONAL_PROPS_DEFAULT"] = "其他屬性";
$MESS["ADD_DEFAULT"] = "添加";
$MESS["AUTH_BLOCK_NAME_DEFAULT"] = "登入";
$MESS["AUTH_REFERENCE_1_DEFAULT"] = "標有*為必填項。";
$MESS["AUTH_REFERENCE_2_DEFAULT"] = "完成註冊後，您將收到通知消息。";
$MESS["AUTH_REFERENCE_3_DEFAULT"] = "除非法律當局或法院裁決要求，否則您在註冊，下達命令或任何其他方式獲得的個人信息將永遠不會出租，出售或轉讓給第三方。";
$MESS["BACK_DEFAULT"] = "後退";
$MESS["BASKET_BLOCK_NAME_DEFAULT"] = "購物車";
$MESS["BUYER_BLOCK_NAME_DEFAULT"] = "顧客";
$MESS["CAPTCHA_REGF_PROMT"] = "輸入您在圖片上看到的字符";
$MESS["CAPTCHA_REGF_TITLE"] = "驗證碼";
$MESS["COUPON_DEFAULT"] = "優惠券";
$MESS["DELIVERY_BLOCK_NAME_DEFAULT"] = "送貨";
$MESS["DELIVERY_CALC_ERROR_TEXT_DEFAULT"] = "繼續結帳。我們的銷售代表將與您聯繫交付詳細信息。";
$MESS["DELIVERY_CALC_ERROR_TITLE_DEFAULT"] = "無法計算交貨價格。";
$MESS["ECONOMY_DEFAULT"] = "節省";
$MESS["EDIT_DEFAULT"] = "改變";
$MESS["EMPTY_BASKET_HINT"] = "＃A1＃單擊此處＃A2＃繼續購物";
$MESS["EMPTY_BASKET_HINT_PATH_TIP"] = "如果購物車為空，則顯示“繼續購物\”鏈接。<br>如果鏈接為空，則文本將被隱藏。";
$MESS["EMPTY_BASKET_TITLE"] = "您的購物車是空的";
$MESS["FAIL_PRELOAD_TEXT_DEFAULT"] = "您以前和我們一起購物，我們記得您，所以我們藉此自由為您填補了田野。<br />
徹底檢查訂單信息，並在需要時編輯您的訂單。看到一切都很好後，單擊\“＃order_button＃\”。
";
$MESS["FURTHER_DEFAULT"] = "下一個";
$MESS["INNER_PS_BALANCE_DEFAULT"] = "您的餘額：";
$MESS["NAV_BACK_DEFAULT"] = "後退";
$MESS["NAV_FORWARD_DEFAULT"] = "下一個";
$MESS["NEAREST_PICKUP_LIST_DEFAULT"] = "最近的位置：";
$MESS["ORDER_DEFAULT"] = "查看";
$MESS["ORDER_DESC_DEFAULT"] = "訂單評論：";
$MESS["PAYMENT_BLOCK_NAME_DEFAULT"] = "支付";
$MESS["PAY_SYSTEM_PAYABLE_ERROR_DEFAULT"] = "在我們驗證您訂購的物品庫存之後，您將能夠支付訂單。完成訂單後，您將收到帶有付款說明的電子郵件。您可以在客戶帳戶中完成購買。";
$MESS["PERIOD_DEFAULT"] = "交貨時間";
$MESS["PERSON_TYPE_DEFAULT"] = "付款人類型";
$MESS["PICKUP_LIST_DEFAULT"] = "接送地點：";
$MESS["PRICE_DEFAULT"] = "價格";
$MESS["PRICE_FREE_DEFAULT"] = "自由的";
$MESS["REGION_BLOCK_NAME_DEFAULT"] = "送貨區";
$MESS["REGION_REFERENCE_DEFAULT"] = "從列表中選擇您的城市。如果找不到城市，請選擇\“其他位置\”，然後在\“ City \”字段中輸入您的城市。";
$MESS["REGISTRATION_REFERENCE_DEFAULT"] = "請註冊以獲得更好的購物體驗並保留您的訂單歷史記錄。";
$MESS["REG_BLOCK_NAME_DEFAULT"] = "登記";
$MESS["SALE_SADC_TRANSIT"] = "交貨時間";
$MESS["SELECT_FILE_DEFAULT"] = "選擇";
$MESS["SELECT_PICKUP_DEFAULT"] = "選擇";
$MESS["SELECT_PROFILE_DEFAULT"] = "選擇個人資料";
$MESS["SOA_BAD_EXTENSION"] = "文件類型無效";
$MESS["SOA_DELIVERY"] = "送貨服務";
$MESS["SOA_DELIVERY_SELECT_ERROR"] = "未選擇送貨服務";
$MESS["SOA_DISTANCE_KM"] = "公里";
$MESS["SOA_DO_SOC_SERV"] = "社交登錄";
$MESS["SOA_ERROR_ORDER"] = "錯誤創建順序。";
$MESS["SOA_ERROR_ORDER_LOST"] = "訂單號。 ＃order_id＃找不到。";
$MESS["SOA_ERROR_ORDER_LOST1"] = "請聯繫商店管理或重試。";
$MESS["SOA_FIELD"] = "場地";
$MESS["SOA_INVALID_EMAIL"] = "電子郵件是不正確的";
$MESS["SOA_INVALID_PATTERN"] = "與模式不匹配";
$MESS["SOA_LESS"] = "至少";
$MESS["SOA_LOCATION_NOT_FOUND"] = "找不到位置";
$MESS["SOA_LOCATION_NOT_FOUND_PROMPT"] = "單擊＃錨＃添加位置＃Anchor_end＃，因此我們知道您要在哪裡運送訂單";
$MESS["SOA_MAP_COORDS"] = "地圖坐標";
$MESS["SOA_MAX_LENGTH"] = "最大限度。場長";
$MESS["SOA_MAX_SIZE"] = "最大限度。文件大小超過";
$MESS["SOA_MAX_VALUE"] = "最大限度。場值";
$MESS["SOA_MIN_LENGTH"] = "最小。場長";
$MESS["SOA_MIN_VALUE"] = "最小。場值";
$MESS["SOA_MORE"] = "少於";
$MESS["SOA_NO"] = "不";
$MESS["SOA_NOT_CALCULATED"] = "未計算";
$MESS["SOA_NOT_FOUND"] = "未找到";
$MESS["SOA_NOT_NUMERIC"] = "僅數字";
$MESS["SOA_NOT_SELECTED"] = "未選中的";
$MESS["SOA_NOT_SELECTED_ALT"] = "如果需要，使位置更具體";
$MESS["SOA_NOT_SPECIFIED"] = "未指定";
$MESS["SOA_NO_JS"] = "訂購過程要求在系統上啟用JavaScript。 JavaScript被禁用或不受瀏覽器的支持。請更改瀏覽器設置，然後再試一次<a href= \ \">重試</a>。";
$MESS["SOA_NUM_STEP"] = "不匹配";
$MESS["SOA_ORDER_COMPLETE"] = "訂單已完成";
$MESS["SOA_ORDER_PROPS"] = "訂單屬性";
$MESS["SOA_ORDER_PS_ERROR"] = "選定的付款方式失敗了。請聯繫站點管理員或選擇其他方法。";
$MESS["SOA_ORDER_SUC"] = "您的訂單<b> ## rode_id＃</b>＃order_date＃已成功創建。";
$MESS["SOA_ORDER_SUC1"] = "您可以在<a href= \"#link# \">個人帳戶中跟踪訂單</a>。您將被要求輸入您的登錄名和密碼以訪問您的帳戶。";
$MESS["SOA_OTHER_LOCATION"] = "其他地方";
$MESS["SOA_PAY"] = "訂購付款";
$MESS["SOA_PAYMENT_SUC"] = "付款<b> ## payment_id＃</b>";
$MESS["SOA_PAYSYSTEM_PRICE"] = "額外的鱈魚：";
$MESS["SOA_PAY_ACCOUNT3"] = "您有足夠的信用來全額支付訂單。";
$MESS["SOA_PAY_LINK"] = "如果您沒有看到任何付款信息，請單擊此處：<a href= \"#link# \" target= \"_blank \">付款和下訂單</a>。";
$MESS["SOA_PAY_PDF"] = "單擊<a href= \“#link# \" target= \"_blank \">下載發票</a>以pdf格式獲取發票。";
$MESS["SOA_PAY_SYSTEM"] = "支付系統";
$MESS["SOA_PICKUP_ADDRESS"] = "地址";
$MESS["SOA_PICKUP_DESC"] = "描述";
$MESS["SOA_PICKUP_EMAIL"] = "電子郵件";
$MESS["SOA_PICKUP_PHONE"] = "電話";
$MESS["SOA_PICKUP_STORE"] = "倉庫";
$MESS["SOA_PICKUP_WORK"] = "營業時間";
$MESS["SOA_PROP_NEW_PROFILE"] = "新的配置文件";
$MESS["SOA_PS_SELECT_ERROR"] = "未選擇付款系統";
$MESS["SOA_REQUIRED"] = "此字段是必需的";
$MESS["SOA_SUM_DELIVERY"] = "送貨：";
$MESS["SOA_SUM_DISCOUNT"] = "折扣";
$MESS["SOA_SUM_IT"] = "全部的：";
$MESS["SOA_SUM_LEFT_TO_PAY"] = "應付金額：";
$MESS["SOA_SUM_NAME"] = "姓名";
$MESS["SOA_SUM_PAYED"] = "有薪酬的：";
$MESS["SOA_SUM_PRICE"] = "價格";
$MESS["SOA_SUM_QUANTITY"] = "數量";
$MESS["SOA_SUM_SUMMARY"] = "總價：";
$MESS["SOA_SUM_VAT"] = "稅：";
$MESS["SOA_SUM_WEIGHT"] = "重量";
$MESS["SOA_SUM_WEIGHT_SUM"] = "總重量：";
$MESS["SOA_SYMBOLS"] = "符號";
$MESS["SOA_YES"] = "是的";
$MESS["STOF_AUTH_REQUEST"] = "請登錄。";
$MESS["STOF_DO_AUTHORIZE"] = "登入";
$MESS["STOF_DO_REGISTER"] = "登記";
$MESS["STOF_EMAIL"] = "電子郵件";
$MESS["STOF_ENTER"] = "登入";
$MESS["STOF_FORGET_PASSWORD"] = "忘記密碼了嗎？";
$MESS["STOF_LASTNAME"] = "姓";
$MESS["STOF_LOGIN"] = "登入";
$MESS["STOF_MY_PASSWORD"] = "您的登錄和密碼";
$MESS["STOF_NAME"] = "名";
$MESS["STOF_NEXT_STEP"] = "繼續結帳";
$MESS["STOF_PASSWORD"] = "密碼";
$MESS["STOF_PHONE"] = "電話號碼";
$MESS["STOF_REGISTER"] = "登記";
$MESS["STOF_REG_HINT"] = "請註冊以獲得更好的購物體驗並保留您的訂單歷史記錄。";
$MESS["STOF_REG_REQUEST"] = "請註冊。";
$MESS["STOF_REG_SMS_REQUEST"] = "確認代碼已發送到您的手機";
$MESS["STOF_REMEMBER"] = "記住賬號";
$MESS["STOF_RE_PASSWORD"] = "重複輸入密碼";
$MESS["STOF_SEND"] = "發送";
$MESS["STOF_SMS_CODE"] = "SMS確認代碼";
$MESS["STOF_SYS_PASSWORD"] = "生成登錄和密碼";
$MESS["SUCCESS_PRELOAD_TEXT_DEFAULT"] = "您以前和我們一起購物，我們記得您，所以我們藉此自由為您填補了田野。<br />
如果信息正確，請單擊\“＃order_button＃\”。
";
$MESS["USE_COUPON_DEFAULT"] = "應用優惠券";
