<?php
$MESS["SMOB_BARCODE_ENTER"] = "輸入條形碼";
$MESS["SMOB_BARCODE_SCAN"] = "掃描";
$MESS["SMOB_BUTTON_BACK"] = "後退";
$MESS["SMOB_BUTTON_CHECK"] = "查看";
$MESS["SMOB_BUTTON_SAVE"] = "節省";
$MESS["SMOB_CHECK_ERROR"] = "錯誤檢查條形碼";
$MESS["SMOB_PRODUCT_NAME"] = "產品名稱";
$MESS["SMOB_SCAN_ERROR"] = "錯誤掃描條形碼";
$MESS["SMOB_STORE_NAME_TMPL"] = "倉庫：\“ ## Store_name ## \”（## Store_id ##）";
