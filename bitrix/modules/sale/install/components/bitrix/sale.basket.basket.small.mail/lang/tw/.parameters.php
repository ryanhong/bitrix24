<?php
$MESS["SBBS_PATH_TO_BASKET"] = "籃子頁面";
$MESS["SBBS_PATH_TO_ORDER"] = "訂購頁面";
$MESS["SBBS_SHOW_DELAY"] = "顯示保留商品";
$MESS["SBBS_SHOW_NOTAVAIL"] = "顯示目前不在庫存中的商品";
$MESS["SBBS_SHOW_SUBSCRIBE"] = "顯示用戶被訂閱的商品";
$MESS["SBBS_USER_ID"] = "用戶身份";
$MESS["SBB_BDELAY"] = "抓住";
$MESS["SBB_BDELETE"] = "刪除";
$MESS["SBB_BDISCOUNT"] = "折扣";
$MESS["SBB_BNAME"] = "產品名稱";
$MESS["SBB_BPRICE"] = "價格";
$MESS["SBB_BPROPS"] = "產品項屬性";
$MESS["SBB_BQUANTITY"] = "數量";
$MESS["SBB_BSUM"] = "全部的";
$MESS["SBB_BTYPE"] = "價格類型";
$MESS["SBB_BWEIGHT"] = "重量";
$MESS["SBB_COLUMNS_LIST"] = "列";
