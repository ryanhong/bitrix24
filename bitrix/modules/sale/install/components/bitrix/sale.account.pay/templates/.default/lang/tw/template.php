<?php
$MESS["SAP_BUTTON"] = "買";
$MESS["SAP_BUY_MONEY"] = "充氣帳戶";
$MESS["SAP_ERROR_INPUT"] = "輸入正值";
$MESS["SAP_FIXED_PAYMENT"] = "固定付款";
$MESS["SAP_INPUT_PLACEHOLDER"] = "輸入值";
$MESS["SAP_LINK_TITLE"] = "添加資金到帳戶";
$MESS["SAP_SUM"] = "數量";
$MESS["SAP_TYPE_PAYMENT_TITLE"] = "付款方式";
