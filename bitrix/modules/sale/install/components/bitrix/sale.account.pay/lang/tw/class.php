<?php
$MESS["SALE_ACCESS_DENIED"] = "請登錄以查看用戶帳戶。";
$MESS["SAP_BASKET_NAME"] = "將資金添加到內部帳戶＃sum＃";
$MESS["SAP_EMPTY_CURRENCY_CODE"] = "沒有選擇貨幣";
$MESS["SAP_EMPTY_PAY_SYSTEM_ID"] = "無效的支付系統ID";
$MESS["SAP_EMPTY_PAY_SYSTEM_LIST"] = "沒有付款系統";
$MESS["SAP_EMPTY_VAR_PRICE_VALUE"] = "丟失應付金額";
$MESS["SAP_ERROR_ADD_BASKET"] = "錯誤將產品添加到購物車";
$MESS["SAP_ERROR_ORDER_PAYMENT_SYSTEM"] = "錯誤創建訂單：無法獲得付款系統。請聯繫網站管理員以獲取更多信息。";
$MESS["SAP_FEATURE_NOT_ALLOW"] = "您的版本無法使用此功能。";
$MESS["SAP_INVALID_TOKEN"] = "無效的安全令牌";
$MESS["SAP_IN_NOT_ARRAY_VALUES"] = "指定的金額超出了範圍";
$MESS["SAP_MODULE_NOT_INSTALL"] = "E商店模塊未安裝";
$MESS["SAP_TITLE"] = "添加資金來帳戶";
$MESS["SAP_WRONG_ID"] = "錯誤的產品ID";
$MESS["SAP_WRONG_INPUT_VALUE"] = "金額不正確";
