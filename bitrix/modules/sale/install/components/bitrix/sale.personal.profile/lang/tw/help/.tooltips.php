<?php
$MESS["PER_PAGE_TIP"] = "指定每個頁面的配置文件數量。";
$MESS["SEF_FOLDER_TIP"] = "指定組件工作文件夾。該文件夾可以由真實的文件系統路徑表示，也可以是虛擬的。";
$MESS["SEF_MODE_TIP"] = "檢查此選項啟用SEF模式和URL配置字段。";
$MESS["SEF_URL_TEMPLATES_detail_TIP"] = "指定配置文件查看頁面的URL段。 URL段應包含配置文件ID，例如：<b>詳細信息/＃id＃/</b>";
$MESS["SEF_URL_TEMPLATES_list_TIP"] = "指定配置文件頁面的URL段。例如：<b> list/</b>";
$MESS["SET_TITLE_TIP"] = "將頁面標題設置為\“ propiles \”。";
