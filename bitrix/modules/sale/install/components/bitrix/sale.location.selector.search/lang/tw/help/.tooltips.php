<?php
$MESS["JS_CALLBACK_TIP"] = "每當選擇器值更改時，JavaScript函數都會調用。該函數必須在窗口對像中定義，例如：<br />
window.locationupdated = function（id）<br />
{<br />
＆nbsp; console.log（參數）; <br />
＆nbsp; console.log（this.getNodeByLocationId（id））; <br />
}";
$MESS["JS_CONTROL_GLOBAL_ID_TIP"] = "使用window.bx.locationselectors對象將選擇器的JavaScript對象調用的字符串ID。";
$MESS["PROVIDE_LINK_BY_TIP"] = "指定代碼或ID在選擇位置時是否將傳遞給輸入控件。";
