<?php
$MESS["SMOT_COMMENTS"] = "評論";
$MESS["SMOT_DATE"] = "日期";
$MESS["SMOT_DESCRIPTION"] = "描述";
$MESS["SMOT_HISTORY"] = "歷史";
$MESS["SMOT_ORDER"] = "命令";
$MESS["SMOT_ORDER_FROM"] = "從";
$MESS["SMOT_ORDER_N"] = "命令 ＃";
$MESS["SMOT_SUMM"] = "全部的";
$MESS["SMOT_TRANSACT"] = "交易";
$MESS["SMOT_TRANS_EMPTY"] = "找不到交易";
$MESS["SMOT_USER"] = "用戶";
