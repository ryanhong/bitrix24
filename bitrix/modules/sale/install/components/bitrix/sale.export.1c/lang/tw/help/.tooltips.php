<?php
$MESS["EXPORT_ALLOW_DELIVERY_ORDERS_TIP"] = "如果已檢查，只有允許交付的訂單將出口到1C。";
$MESS["EXPORT_FINAL_ORDERS_TIP"] = "在此處指定必須將訂單導出的最低狀態。";
$MESS["EXPORT_PAYED_ORDERS_TIP"] = "如果已檢查，僅付費訂單將出口到1C。";
$MESS["FINAL_STATUS_ON_DELIVERY_TIP"] = "如果導出到1C成功並已通知，則在此處指定為訂單申請的狀態。";
$MESS["GROUP_PERMISSIONS_TIP"] = "選擇此處的用戶組，其成員可以導出為1C。";
$MESS["REPLACE_CURRENCY_TIP"] = "可以用該字段中指定的物品貨幣替換。這不會轉換實際價格。";
$MESS["SITE_LIST_TIP"] = "在此處指定其訂單將被導出的網站。";
$MESS["USE_ZIP_TIP"] = "如果可能的話，指定使用ZIP壓縮。";
