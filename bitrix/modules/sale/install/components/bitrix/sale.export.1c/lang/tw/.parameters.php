<?php
$MESS["CP_BCI1_ALL_SITES"] = "所有站點";
$MESS["CP_BCI1_CHANGE_STATUS_FROM_1C"] = "使用1C數據設置訂單狀態";
$MESS["CP_BCI1_EXPORT_ALLOW_DELIVERY_ORDERS"] = "僅出口訂單";
$MESS["CP_BCI1_EXPORT_FINAL_ORDERS"] = "從狀態出口訂單";
$MESS["CP_BCI1_EXPORT_PAYED_ORDERS"] = "僅出口訂單";
$MESS["CP_BCI1_FINAL_STATUS_ON_DELIVERY"] = "從1C收購交貨後的訂單狀態";
$MESS["CP_BCI1_GROUP_PERMISSIONS"] = "用戶組允許導出";
$MESS["CP_BCI1_NO"] = "<未選擇>";
$MESS["CP_BCI1_REPLACE_CURRENCY"] = "出口到1C時，將貨幣更改為";
$MESS["CP_BCI1_REPLACE_CURRENCY_VALUE"] = "盧布";
$MESS["CP_BCI1_SITE_LIST"] = "該網站的出口訂單到1C";
$MESS["CP_BCI1_USE_ZIP"] = "如果可用的話，請使用ZIP壓縮";
$MESS["SALE_1C_FILE_SIZE_LIMIT"] = "導入文件塊的最大尺寸（字節）";
$MESS["SALE_1C_IMPORT_NEW_ORDERS"] = "創建1C的新訂單和承包商";
$MESS["SALE_1C_INTERVAL"] = "進口步驟持續時間，秒。 （0-一次導入）";
$MESS["SALE_1C_SITE_NEW_ORDERS"] = "將新訂單和承包商進口到";
