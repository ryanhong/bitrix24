<?php
$MESS["STPCL_ACTIONS"] = "動作";
$MESS["STPCL_ACTIV"] = "卡是<br>活動";
$MESS["STPCL_CEXP"] = "到期<br />日期（m /y）";
$MESS["STPCL_DELETE"] = "刪除";
$MESS["STPCL_DELETE_ALT"] = "刪除信用卡記錄";
$MESS["STPCL_DELETE_PROMT"] = "您確定要刪除此信用卡上的註冊信息嗎？";
$MESS["STPCL_NEW"] = "添加";
$MESS["STPCL_NO"] = "不";
$MESS["STPCL_PAY_SYS"] = "支付系統";
$MESS["STPCL_TYPE"] = "卡的種類";
$MESS["STPCL_UPDATE"] = "編輯";
$MESS["STPCL_UPDATE_ALT"] = "編輯信用卡參數";
$MESS["STPCL_YES"] = "是的";
