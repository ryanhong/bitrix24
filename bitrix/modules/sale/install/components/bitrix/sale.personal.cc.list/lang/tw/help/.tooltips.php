<?php
$MESS["PATH_TO_DETAIL_TIP"] = "指定信用卡詳細信息頁面的路徑。可以使用<b>編輯信用卡</b>組件創建此頁面。";
$MESS["PER_PAGE_TIP"] = "指定每個頁面的卡數。其他卡將在下一頁上可用。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將頁面標題設置為<b>我的信用卡</b>。";
