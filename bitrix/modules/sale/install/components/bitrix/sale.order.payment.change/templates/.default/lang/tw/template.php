<?php
$MESS["SOPC_TPL_BILL"] = "發票";
$MESS["SOPC_TPL_FROM_DATE"] = "的";
$MESS["SOPC_TPL_NOTPAID"] = "未付";
$MESS["SOPC_TPL_NUMBER_SIGN"] = "＃";
$MESS["SOPC_TPL_PAID"] = "有薪酬的";
$MESS["SOPC_TPL_RESTRICTED_PAID"] = "審查";
$MESS["SOPC_TPL_RESTRICTED_PAID_MESSAGE"] = "<b>通知：</b>訂單可以在銷售代表驗證後支付";
$MESS["SOPC_TPL_SUM_TO_PAID"] = "發票金額";
