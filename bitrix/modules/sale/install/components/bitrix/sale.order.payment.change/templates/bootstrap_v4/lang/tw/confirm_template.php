<?php
$MESS["SOPC_HANDLERS_PAY_SYSTEM_WARNING_RETURN"] = "<b>注意：</b>如果要退還訂單，則必須與賣方聯繫。";
$MESS["SOPC_INNER_BALANCE"] = "您的內部帳戶餘額";
$MESS["SOPC_LOW_BALANCE"] = "內部帳戶的資金不足";
$MESS["SOPC_ORDER_SUC"] = "您的訂單＃<b> #order_id＃</b>＃order_date＃已更新。";
$MESS["SOPC_PAYMENT_SUC"] = "您的付款ID：＃<b> #payment_id＃</b>";
$MESS["SOPC_PAYMENT_SYSTEM_NAME"] = "使用<b> #pay_system_name＃</b>付款";
$MESS["SOPC_PAY_LINK"] = "如果您沒有看到任何付款信息，請單擊此處：<a href= \"#link# \" target= \"_blank \">付款和下訂單</a>。";
$MESS["SOPC_PAY_SYSTEM_CHANGED"] = "付款方式改變了";
$MESS["SOPC_PAY_SYSTEM_NOT_ALLOW_PAY"] = "該訂單可以在銷售代表核實後支付";
$MESS["SOPC_SUM_OF_PAYMENT"] = "應付金額";
$MESS["SOPC_TPL_BILL"] = "發票";
$MESS["SOPC_TPL_FROM_DATE"] = "從";
$MESS["SOPC_TPL_NUMBER_SIGN"] = "＃";
$MESS["SOPC_TPL_PAY_BUTTON"] = "支付";
$MESS["SOPC_TPL_SUM_TO_PAID"] = "發票金額";
