<?php
$MESS["SOPC_ELIMINATED_PAY_SYSTEMS"] = "排除付款系統選擇";
$MESS["SOPC_PATH_TO_PAYMENT"] = "支付系統集成頁面";
$MESS["SOPC_SHOW_ALL"] = "（未選中的）";
$MESS["SPOC_ALLOW_INNER"] = "使用內部帳戶啟用付款";
$MESS["SPOC_ONLY_INNER_FULL"] = "僅允許使用內部帳戶支付全部訂單";
$MESS["SPOC_REFRESH_PRICE"] = "付款系統狀態發生變化時重新計算訂單";
