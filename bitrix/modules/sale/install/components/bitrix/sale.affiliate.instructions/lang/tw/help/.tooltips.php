<?php
$MESS["AFF_REG_PAGE_TIP"] = "該路徑將被添加到會員的指示中。想要成為會員的用戶可以單擊使用此路徑創建的鏈接。默認值為 /affiliate/register.php";
$MESS["REGISTER_PAGE_TIP"] = "會員註冊頁面的路徑名。默認註冊頁面是register.php。";
$MESS["SET_TITLE_TIP"] = "如果已檢查，則頁面標題將設置為\“新的會員註冊\”。";
$MESS["SHOP_NAME_TIP"] = "指定將添加到會員指令中的站點名稱。";
$MESS["SHOP_URL_TIP"] = "指定建立分支機構的網站的URL。";
