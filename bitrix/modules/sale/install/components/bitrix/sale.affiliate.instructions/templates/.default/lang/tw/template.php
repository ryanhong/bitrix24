<?php
$MESS["SPCAT3_AFF_REG"] = "會員註冊文本鏈接";
$MESS["SPCAT3_HTML"] = "HTML代碼：";
$MESS["SPCAT3_LINK_TEXT"] = "成為“＃＃名稱＃”的會員";
$MESS["SPCAT3_NOTE"] = "文本鏈接不是唯一的選擇。您可以使用圖像，徽標或橫幅，帶有或沒有評論 - 任何說服您的訪客點擊它為您賺錢的東西。";
$MESS["SPCAT3_PARTNER_ID"] = " - 您的伴侶ID。鏈接中此ID的存在告訴我們，客戶來自您的網站。";
$MESS["SPCAT3_TEXT_LINK"] = "文字鏈接到商店";
$MESS["SPCAT3_UNACTIVE_AFF"] = "您是註冊會員，但您的帳戶不活動。請聯繫政府以獲取更多信息。";
$MESS["SPCAT3_VIEW"] = "外貌：";
