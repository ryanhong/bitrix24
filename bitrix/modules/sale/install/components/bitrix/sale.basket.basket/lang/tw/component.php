<?php
$MESS["SALE_EMPTY_BASKET"] = "你的購物車是空的";
$MESS["SALE_MODULE_NOT_INSTALL"] = "E商店模塊未安裝";
$MESS["SBB_PRODUCT_NOT_AVAILABLE"] = "＃產品缺貨";
$MESS["SBB_PRODUCT_NOT_ENOUGH_QUANTITY"] = "\“＃product＃\”的當前股票不足（需要＃編號＃）";
$MESS["SBB_PRODUCT_PRICE_NOT_FOUND"] = "找不到產品價格。";
$MESS["SBB_TITLE"] = "我的購物車";
