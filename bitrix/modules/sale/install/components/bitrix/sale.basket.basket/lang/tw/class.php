<?php
$MESS["SALE_EMPTY_BASKET"] = "你的購物車是空的";
$MESS["SALE_MODULE_NOT_INSTALL"] = "沒有安裝電子商店模塊。";
$MESS["SBB_BASKET_ITEM_WRONG_AVAILABLE_QUANTITY"] = "抱歉，您選擇的產品數量當前不可用。<br>使用先前的正確值。";
$MESS["SBB_PRODUCT_NOT_AVAILABLE"] = "＃產品缺貨";
$MESS["SBB_PRODUCT_NOT_ENOUGH_QUANTITY"] = "\“＃product＃\”的當前股票不足（需要＃編號＃）";
$MESS["SBB_PRODUCT_QUANTITY_CHANGED"] = "產品數量已更改";
$MESS["SBB_TITLE"] = "我的購物車";
$MESS["SOA_TEMPL_ORDER_PS_ERROR"] = "選定的付款方式失敗了。請聯繫站點管理員或選擇其他方法。";
