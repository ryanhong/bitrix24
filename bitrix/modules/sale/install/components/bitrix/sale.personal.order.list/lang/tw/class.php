<?php
$MESS["SPOL_ACCESS_DENIED"] = "請登錄以查看訂單。";
$MESS["SPOL_CANNOT_COPY_ORDER"] = "無法複製訂單。";
$MESS["SPOL_CATALOG_MODULE_NOT_INSTALL"] = "未安裝商業目錄模塊。";
$MESS["SPOL_DEFAULT_TITLE"] = "我的訂單";
$MESS["SPOL_PAGES"] = "命令";
$MESS["SPOL_SALE_MODULE_NOT_INSTALL"] = "沒有安裝電子商店模塊。";
