<?php
$MESS["SPCA_COMMENT"] = "評論";
$MESS["SPCA_DATE"] = "日期";
$MESS["SPCA_FILTER"] = "顯示期";
$MESS["SPCA_INCOME"] = "收入";
$MESS["SPCA_NO_ACT"] = "在指定期間未發生交易";
$MESS["SPCA_ON_ACCT"] = "根據";
$MESS["SPCA_OUTCOME"] = "花費";
$MESS["SPCA_PERIOD"] = "時期：";
$MESS["SPCA_SET"] = "放";
$MESS["SPCA_UNACTIVE_AFF"] = "您是有效的會員，但您的帳戶不活動。請聯繫政府以獲取更多信息。";
$MESS["SPCA_UNSET"] = "重置";
