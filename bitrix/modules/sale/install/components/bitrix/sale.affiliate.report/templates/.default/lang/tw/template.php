<?php
$MESS["SPCAS1_FILTER"] = "顯示期";
$MESS["SPCAS1_ITOG"] = "全部的";
$MESS["SPCAS1_NAME"] = "姓名";
$MESS["SPCAS1_NO_ACT"] = "在指定期間沒有交易";
$MESS["SPCAS1_PERIOD"] = "時期：";
$MESS["SPCAS1_QUANTITY"] = "數量";
$MESS["SPCAS1_SET"] = "放";
$MESS["SPCAS1_SUM"] = "數量";
$MESS["SPCAS1_UNACTIVE_AFF"] = "您是註冊會員，但您的帳戶不活動。請聯繫政府以獲取更多信息。";
$MESS["SPCAS1_UNSET"] = "重置";
