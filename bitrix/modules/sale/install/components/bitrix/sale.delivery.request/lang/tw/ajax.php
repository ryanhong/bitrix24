<?php
$MESS["SALE_SDR_AJAX_CREATE"] = "創建運輸訂單";
$MESS["SALE_SDR_AJAX_DELIVERY_NOT_FOUND"] = "找不到交貨服務ID \“＃veliver_id＃\”";
$MESS["SALE_SDR_AJAX_NOT_SUPPORT"] = "送貨服務ID \“＃delivery_id＃\”不支持運輸訂單";
$MESS["SALE_SDR_AJAX_NO_REQUESTS"] = "找不到合適的運輸訂單";
$MESS["SALE_SDR_AJAX_REQUEST_NUMBER"] = "運輸訂單ID";
$MESS["SALE_SDR_AJAX_SHIPMENTS_ADD"] = "將貨物添加到運輸訂單";
