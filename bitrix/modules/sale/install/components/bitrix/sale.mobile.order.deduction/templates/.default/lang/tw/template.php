<?php
$MESS["SMODE_BACK"] = "後退";
$MESS["SMODE_BARCODE"] = "條碼";
$MESS["SMODE_DEDUCT"] = "實現";
$MESS["SMODE_DEDUCT_HINT"] = "單擊\“滿足\”以最終確定訂單。";
$MESS["SMODE_DEDUCT_UNDO"] = "取消";
$MESS["SMODE_DEDUCT_UNDO_REASON"] = "取消的原因";
$MESS["SMODE_ERROR"] = "履行錯誤";
$MESS["SMODE_PRODUCT"] = "產品";
$MESS["SMODE_PRODUCT_NAME"] = "產品名稱";
$MESS["SMODE_READY"] = "準備好";
$MESS["SMODE_SAVE"] = "節省";
$MESS["SMODE_STORE"] = "倉庫";
$MESS["SMODE_TITLE"] = "訂單完成度";
$MESS["SMODE_TITLE_UNDO"] = "取消履行";
