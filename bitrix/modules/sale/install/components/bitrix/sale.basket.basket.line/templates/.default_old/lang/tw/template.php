<?php
$MESS["TSB1_2ORDER"] = "查看";
$MESS["TSB1_CART"] = "大車";
$MESS["TSB1_COLLAPSE"] = "隱藏";
$MESS["TSB1_DELAY"] = "在願望清單上";
$MESS["TSB1_DELETE"] = "刪除";
$MESS["TSB1_EXPAND"] = "擴張";
$MESS["TSB1_LOGIN"] = "登入";
$MESS["TSB1_LOGOUT"] = "登出";
$MESS["TSB1_NOTAVAIL"] = "不可用";
$MESS["TSB1_PERSONAL"] = "個人部分";
$MESS["TSB1_READY"] = "有存貨";
$MESS["TSB1_REGISTER"] = "登記";
$MESS["TSB1_SUBSCRIBE"] = "觀看列表";
$MESS["TSB1_SUM"] = "全部的";
$MESS["TSB1_TOTAL_PRICE"] = "總價";
$MESS["TSB1_YOUR_CART"] = "你的購物車";
