<?php
$MESS["ID_TIP"] = "評估配置文件ID的PHP代碼。默認值是<b> = {\ $ id} </b>";
$MESS["PATH_TO_DETAIL_TIP"] = "配置文件編輯器頁面的路徑，包括配置文件作為參數。";
$MESS["PATH_TO_LIST_TIP"] = "配置文件頁面的路徑名。您可以使用<i>用戶配置文件</i>組件創建此頁面。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將頁面標題設置為<b>配置文件#id＃</b>";
