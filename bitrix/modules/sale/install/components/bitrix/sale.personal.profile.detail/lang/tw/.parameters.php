<?php
$MESS["SPPD_COMPATIBLE_LOCATION_MODE"] = "獲取傳統模板版本的位置列表";
$MESS["SPPD_ID"] = "配置文件ID";
$MESS["SPPD_PATH_TO_DETAIL"] = "個人資料編輯頁面";
$MESS["SPPD_PATH_TO_LIST"] = "個人資料列表頁面";
$MESS["SPPD_USE_AJAX_LOCATIONS"] = "使用擴展位置選擇";
