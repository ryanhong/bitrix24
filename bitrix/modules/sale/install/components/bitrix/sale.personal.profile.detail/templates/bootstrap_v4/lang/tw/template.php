<?php
$MESS["SALE_APPLY"] = "申請";
$MESS["SALE_PERS_TYPE"] = "付款人類型";
$MESS["SALE_PNAME"] = "姓名";
$MESS["SALE_RESET"] = "取消";
$MESS["SALE_SAVE"] = "節省";
$MESS["SPPD_ADD"] = "添加";
$MESS["SPPD_DELETE_FILE"] = "刪除文件";
$MESS["SPPD_DOWNLOAD_FILE"] = "下載文件＃file_name＃";
$MESS["SPPD_FILE_COUNT"] = "選擇的文件：";
$MESS["SPPD_FILE_NOT_SELECTED"] = "沒有選擇文件";
$MESS["SPPD_PROFILE_NO"] = "個人資料編號＃ID＃";
$MESS["SPPD_RECORDS_LIST"] = "到個人資料列表";
$MESS["SPPD_SELECT"] = "選擇";
