<?php
$MESS["SALE_DVF_COMPONENT_GRID_ACTION_DELETE"] = "刪除";
$MESS["SALE_DVF_COMPONENT_MANAGER_ERROR"] = "經理參數必須從bitrix \\ sale \\ domain \\ verification \\ basemanager繼承";
$MESS["SALE_DVF_COMPONENT_PARAM_REQUIRED_ERROR"] = "\“＃param_name＃\”是必需的";
$MESS["SALE_DVF_COMPONENT_SALE_MODULE_ERROR"] = "未安裝\“在線商店\”模塊。";
$MESS["SALE_DVF_COMPONENT_SAVE_DOMAIN_ERROR"] = "未指定確認域";
$MESS["SALE_DVF_COMPONENT_SAVE_FILE_ERROR"] = "域確認文件未上傳";
