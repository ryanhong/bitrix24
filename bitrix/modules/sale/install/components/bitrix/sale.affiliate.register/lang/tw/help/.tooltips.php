<?php
$MESS["AGREEMENT_TEXT_FILE_TIP"] = "聯盟協議頁面的路徑。";
$MESS["REDIRECT_PAGE_TIP"] = "指定註冊時用戶將重定向到的頁面的路徑。默認值是<b> = {\ $ _請求[\“ redirect_page \”]} </b>。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將將頁面標題設置為\“新的會員註冊\”。";
