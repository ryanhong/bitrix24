<?php
$MESS["SPCR1_CAPTCHA"] = "預防自動註冊";
$MESS["SPCR1_CAPTCHA_WRD"] = "在圖片上輸入單詞：";
$MESS["SPCR1_FORG_PASSWORD"] = "忘記密碼？";
$MESS["SPCR1_IF_NOT_REG"] = "未註冊";
$MESS["SPCR1_IF_REG"] = "已經註冊";
$MESS["SPCR1_IF_REMEMBER"] = "請輸入您的登錄和密碼：";
$MESS["SPCR1_I_AGREE"] = "我同意會員協議的條款";
$MESS["SPCR1_LASTNAME"] = "姓";
$MESS["SPCR1_LOGIN"] = "登入";
$MESS["SPCR1_NAME"] = "姓名";
$MESS["SPCR1_NEXT"] = "繼續";
$MESS["SPCR1_PASSWORD"] = "密碼";
$MESS["SPCR1_PASS_CONF"] = "確認密碼";
$MESS["SPCR1_REGISTER"] = "登記";
$MESS["SPCR1_SITE_DESCR"] = "您網站的描述：";
$MESS["SPCR1_SITE_URL"] = "您的網站URL：";
$MESS["SPCR1_UNACTIVE_AFF"] = "您是註冊會員，但您的帳戶不活動。請聯繫政府以獲取更多信息。";
