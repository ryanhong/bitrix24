<?php
$MESS["ID_TIP"] = "評估訂閱ID的PHP代碼。默認值是<b> = {\ $ id} </b>";
$MESS["PATH_TO_LIST_TIP"] = "指定訂閱頁面的路徑名。如果頁面在同一目錄中，則只能指定文件名。您可以使用<b>用戶的訂閱</b>組件創建此頁面。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將頁面標題設置為<b>取消訂閱#id＃</b>。";
