<?php
$MESS["SALE_ACCESS_DENIED"] = "您必須授權取消訂閱";
$MESS["SALE_MODULE_NOT_INSTALL"] = "E商店模塊未安裝";
$MESS["SPOC_NO_ORDER"] = "訂單號。找不到＃order_id＃。";
$MESS["SPSC_TITLE"] = "取消訂閱號。 ＃ID＃";
$MESS["STPSC_CANT_CANCEL"] = "您不能取消此訂閱";
$MESS["STPSC_CONFIRM"] = "您確定要取消訂閱號嗎？ ＃id＃for＃name＃？此動作是不可逆轉的。";
