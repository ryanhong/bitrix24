<?php
$MESS["PER_PAGE_TIP"] = "指定每個頁面的卡數。其他卡將在下一頁上可用。";
$MESS["SEF_FOLDER_TIP"] = "指定組件工作文件夾。該文件夾可以由真實的文件系統路徑表示，也可以是虛擬的。";
$MESS["SEF_MODE_TIP"] = "檢查此選項啟用SEF模式和URL配置字段。";
$MESS["SEF_URL_TEMPLATES_detail_TIP"] = "指定信用卡編輯器頁面的URL段。 URL應包含卡ID。";
$MESS["SEF_URL_TEMPLATES_list_TIP"] = "指定信用卡頁面URL的細分市場。例如：<b> list/</b>";
$MESS["SET_TITLE_TIP"] = "檢查此選項將頁面標題設置為<b>我的信用卡</b>。";
