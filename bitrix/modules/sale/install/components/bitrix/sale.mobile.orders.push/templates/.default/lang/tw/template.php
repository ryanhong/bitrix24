<?php
$MESS["SMOP_ALL_N"] = "不";
$MESS["SMOP_ALL_SUBSCRIBES"] = "訂閱一切";
$MESS["SMOP_ALL_Y"] = "是的";
$MESS["SMOP_BACK"] = "後退";
$MESS["SMOP_HEAD"] = "訂閱推送通知";
$MESS["SMOP_JS_SAVE_ERROR"] = "由於發生錯誤而無法保存。";
$MESS["SMOP_JS_SAVING"] = "保存...";
$MESS["SMOP_SAVE"] = "節省";
$MESS["SMOP_SUBSCRIBES"] = "訂閱";
$MESS["SMOP_TITLE"] = "電子商店";
