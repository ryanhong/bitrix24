<?php
$MESS["PERIOD_TIP"] = "抽樣期設置為0至180天。";
$MESS["SBP_ACTION_VARIABLE"] = "包含動作的變量";
$MESS["SBP_ACTION_VARIABLE_TIP"] = "將傳遞所需操作的變量的名稱：add_to_compare_list，add2basket等。默認值是<i> action </i>。";
$MESS["SBP_ADDITIONAL_IMAGE"] = "附加圖像";
$MESS["SBP_ADD_PROPERTIES_TO_BASKET"] = "將物品和SKU屬性傳遞到運輸購物車";
$MESS["SBP_AMOUNT"] = "銷售金額";
$MESS["SBP_BASKET"] = "購物車中的物品";
$MESS["SBP_BASKET_URL"] = "購物車URL";
$MESS["SBP_BASKET_URL_TIP"] = "在此處指定客戶購物車頁面的路徑。";
$MESS["SBP_CACHE_GROUPS"] = "尊重訪問權限";
$MESS["SBP_CONVERT_CURRENCY"] = "僅使用一種貨幣展示價格";
$MESS["SBP_CURRENCY_ID"] = "將所有價格轉換為貨幣：";
$MESS["SBP_DAYS"] = "天";
$MESS["SBP_DESC_DESCRIPTION"] = "複製頁面描述從屬性";
$MESS["SBP_DESC_DISPLAY_COMPARE"] = "顯示\“比較\”按鈕";
$MESS["SBP_DESC_KEYWORDS"] = "從屬性複制頁面關鍵字";
$MESS["SBP_DESC_PAGER_CATALOG"] = "產品";
$MESS["SBP_DETAIL_URL"] = "項目詳細信息URL";
$MESS["SBP_DETAIL_URL_TIP"] = "在此處指定信息阻止元素詳細信息頁面的路徑。";
$MESS["SBP_DISPLAY_COMPARE_TIP"] = "檢查此選項以顯示<b>比較</b>按鈕，使客戶可以在比較表中添加產品。";
$MESS["SBP_DISPLAY_PANEL_TIP"] = "如果已檢查，則在“站點編輯”模式下的管理工具欄和組件編輯區域工具欄上顯示該工具按鈕。";
$MESS["SBP_ERROR_MODULE"] = "需要商業目錄和信息塊模塊。";
$MESS["SBP_FILTER_NAME"] = "產品屬性過濾標題";
$MESS["SBP_F_CANCELED"] = "\“取消\”標誌";
$MESS["SBP_F_DELIVERY"] = "\“已交付\”標誌";
$MESS["SBP_F_OUT"] = "\“已發貨\”標誌";
$MESS["SBP_F_PAY"] = "\“付費\”標誌";
$MESS["SBP_GROUP_OFFERS_CATALOG_PARAMS"] = "顯示\“％s \” SKU的參數";
$MESS["SBP_GROUP_PRODUCT_CATALOG_PARAMS"] = "顯示\“％s \”目錄項目的參數";
$MESS["SBP_HIDE_NOT_AVAILABLE_EXT"] = "隱藏庫存的產品";
$MESS["SBP_IBLOCK"] = "信息塊";
$MESS["SBP_ITEM_COUNT"] = "顯示的項目數量";
$MESS["SBP_LINE_ELEMENT_COUNT_TIP"] = "在顯示部分的元素時指定每行元素的數量。";
$MESS["SBP_MESS_BTN_ADD_TO_BASKET"] = "\“添加到購物車\”按鈕文本";
$MESS["SBP_MESS_BTN_ADD_TO_BASKET_DEFAULT"] = "添加到購物車";
$MESS["SBP_MESS_BTN_BUY"] = "\“購買\”按鈕文字";
$MESS["SBP_MESS_BTN_BUY_DEFAULT"] = "買";
$MESS["SBP_MESS_BTN_DETAIL"] = "\“詳細信息\”按鈕文字";
$MESS["SBP_MESS_BTN_DETAIL_DEFAULT"] = "細節";
$MESS["SBP_MESS_BTN_SUBSCRIBE"] = "\“返回庫存\”按鈕文本";
$MESS["SBP_MESS_BTN_SUBSCRIBE_DEFAULT"] = "訂閱";
$MESS["SBP_MESS_NOT_AVAILABLE"] = "有關物品掉庫存的消息";
$MESS["SBP_MESS_NOT_AVAILABLE_DEFAULT"] = "缺貨";
$MESS["SBP_META_DESCRIPTION_TIP"] = "從屬性複制頁面描述。";
$MESS["SBP_META_KEYWORDS_TIP"] = "從屬性複制頁面關鍵字。";
$MESS["SBP_NEED_REQUIRED_MODULES"] = "該組件需要電子商店，信息塊和商業目錄模塊。";
$MESS["SBP_OFFERS_CART_PROPERTIES"] = "購物車中的Sku物業";
$MESS["SBP_OFFERS_FIELD_CODE"] = "SKU場";
$MESS["SBP_OFFERS_LIMIT"] = "最大SKU（0-顯示全部）";
$MESS["SBP_OFFERS_PROPERTY_CODE"] = "SKU屬性";
$MESS["SBP_ORDER_FILTER_NAME"] = "訂單參數過濾標題";
$MESS["SBP_PAGE_ELEMENT_COUNT"] = "每頁項目";
$MESS["SBP_PAGE_ELEMENT_COUNT_TIP"] = "指定每個頁面的項目數。";
$MESS["SBP_PARTIAL_PRODUCT_PROPERTIES"] = "允許部分空屬性";
$MESS["SBP_PERIOD"] = "時期";
$MESS["SBP_PRICES"] = "價格";
$MESS["SBP_PRICE_CODE"] = "價格類型";
$MESS["SBP_PRICE_CODE_TIP"] = "指定目錄中要使用的價格類型。如果您省略了價格類型，價格和<b>購買</b>和<b>添加到籃子</b>按鈕將被隱藏。";
$MESS["SBP_PRICE_VAT_INCLUDE_TIP"] = "指定包括價格稅。";
$MESS["SBP_PRODUCT_ID_VARIABLE"] = "包含可購買產品ID的變量";
$MESS["SBP_PRODUCT_ID_VARIABLE_TIP"] = "將通過產品ID的變量的名稱。";
$MESS["SBP_PRODUCT_PROPS_VARIABLE"] = "包含產品屬性的變量";
$MESS["SBP_PRODUCT_QUANTITY_VARIABLE"] = "包含產品數量的變量";
$MESS["SBP_PRODUCT_SUBSCRIPTION"] = "啟用後庫存通知";
$MESS["SBP_PROPERTY"] = "特性";
$MESS["SBP_PROPERTY_ADD_TO_BASKET"] = "添加到購物車的屬性";
$MESS["SBP_PROPERTY_CODE_TIP"] = "在此處選擇顯示項目時要顯示的信息塊屬性。";
$MESS["SBP_PROPERTY_DISPLAY"] = "顯示的屬性";
$MESS["SBP_PROPERTY_GROUP"] = "分組的屬性";
$MESS["SBP_PROPERTY_LABEL"] = "產品標籤屬性";
$MESS["SBP_QUANTITY"] = "售出的產品項目";
$MESS["SBP_QUANTITY_FLOAT_TIP"] = "指定購物車中的物品數量是否可以分數。";
$MESS["SBP_SECTION_URL"] = "部分目錄URL";
$MESS["SBP_SHOW"] = "展示暢銷書";
$MESS["SBP_SHOW_DISCOUNT_PERCENT"] = "顯示折扣百分比";
$MESS["SBP_SHOW_IMAGE"] = "顯示圖像";
$MESS["SBP_SHOW_NAME"] = "顯示名稱";
$MESS["SBP_SHOW_OLD_PRICE"] = "顯示以前的價格";
$MESS["SBP_SHOW_PRICE_COUNT"] = "顯示基於數量的價格";
$MESS["SBP_SHOW_PRICE_COUNT_TIP"] = "如果未選中“使用價格範圍”選項（這意味著僅顯示一個價格），但是有基於數量的定價產品，請指定數量以選擇正確的價格。此選項對固定價格的產品沒有影響。";
$MESS["SBP_SHOW_PRODUCTS"] = "顯示目錄產品";
$MESS["SBP_SORT_ASC"] = "上升";
$MESS["SBP_SORT_DESC"] = "下降";
$MESS["SBP_TYPE"] = "信息塊類型";
$MESS["SBP_UNDEFINED"] = "未指定";
$MESS["SBP_USE_PRICE_COUNT"] = "使用價格範圍";
$MESS["SBP_USE_PRICE_COUNT_TIP"] = "如果已檢查，將選擇所有現有的價格範圍。";
$MESS["SBP_USE_PRODUCT_QUANTITY"] = "顯示產品數量輸入字段";
$MESS["SBP_VAT_INCLUDE"] = "包括價格稅";
