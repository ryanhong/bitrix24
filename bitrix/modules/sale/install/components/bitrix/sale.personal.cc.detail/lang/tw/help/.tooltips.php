<?php
$MESS["ID_TIP"] = "評估卡ID的PHP代碼。默認值是<b> = {\ $ id} </b>";
$MESS["PATH_TO_LIST_TIP"] = "指定信用卡頁面的路徑。如果頁面在同一目錄中，則只能輸入文件名。您可以使用<b>客戶的信用卡</b>組件創建此頁面。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將頁面標題設置為<b>編輯卡#id＃</b>。";
