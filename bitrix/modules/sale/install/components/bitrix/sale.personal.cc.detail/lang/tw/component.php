<?php
$MESS["SALE_ACCESS_DENIED"] = "您必須授權查看信用卡信息";
$MESS["SALE_MODULE_NOT_INSTALL"] = "E商店模塊未安裝";
$MESS["STPC_EMPTY_BCURRENCY"] = "未指定信用卡付款限額的貨幣";
$MESS["STPC_EMPTY_CARDNUM"] = "字段\“信用卡號\”是空的";
$MESS["STPC_EMPTY_CARD_TYPE"] = "字段\“信用卡類型\”是空的";
$MESS["STPC_EMPTY_PAY_SYS"] = "字段\“付款系統\”是空的";
$MESS["STPC_ERROR_SAVING_CARD"] = "保存錯誤信用卡";
$MESS["STPC_NO_CARD"] = "找不到信用卡";
$MESS["STPC_TITLE_ADD"] = "新信用卡";
$MESS["STPC_TITLE_UPDATE"] = "信用卡的參數## ID＃";
$MESS["STPC_WRONG_CARDNUM"] = "信用卡號無效";
$MESS["STPC_WRONG_MONTH"] = "信用卡到期月無效";
$MESS["STPC_WRONG_YEAR"] = "信用卡到期年無效";
