<?php
$MESS["IVR_LICENSE_POPUP_FOOTER_2"] = "語音菜單（IVR）僅在選定的商業計劃中可用。";
$MESS["IVR_LICENSE_POPUP_HEADER_2"] = "僅在商業計劃中提供。";
$MESS["IVR_LICENSE_POPUP_ITEM_1"] = "嵌套菜單";
$MESS["IVR_LICENSE_POPUP_ITEM_2"] = "使用自定義音頻文件向客戶說消息";
$MESS["IVR_LICENSE_POPUP_ITEM_3"] = "使用語音引擎說文本（可以使用不同的聲音；設置速度和音量的選項）";
$MESS["IVR_LICENSE_POPUP_ITEM_4"] = "向僱員，隊列組或外部號碼進行遠程呼叫";
$MESS["IVR_LICENSE_POPUP_ITEM_5"] = "向前呼叫到擴展號";
$MESS["IVR_LICENSE_POPUP_MORE"] = "了解更多";
$MESS["IVR_LICENSE_POPUP_TEXT"] = "語音菜單（IVR）將在您公司的適當員工或部門分發入站電話。可以使用各種選項來設置功能齊全的語音菜單：";
