<?php
$MESS["VOX_CALLER_ID_ALREADY_EXISTS"] = "這個數字已經連接到Bitrix24";
$MESS["VOX_CALLER_ID_NOT_FOUND"] = "沒有找到數字";
$MESS["VOX_CALLER_ID_WRONG_NUMBER"] = "數字必須以國際格式輸入";
