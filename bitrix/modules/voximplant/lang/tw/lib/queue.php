<?php
$MESS["INCOMING_QUEUE_ENTITY_CONFIG_ID_FIELD"] = "配置ID";
$MESS["INCOMING_QUEUE_ENTITY_ID_FIELD"] = "ID";
$MESS["INCOMING_QUEUE_ENTITY_LAST_ACTIVITY_DATE_FIELD"] = "最後一個呼叫日期";
$MESS["INCOMING_QUEUE_ENTITY_SEARCH_ID_FIELD"] = "搜索字符串";
$MESS["INCOMING_QUEUE_ENTITY_STATUS_FIELD"] = "地位";
$MESS["INCOMING_QUEUE_ENTITY_USER_ID_FIELD"] = "用戶身份";
