<?php
$MESS["VI_ACCOUNT_BALANCE"] = "平衡";
$MESS["VI_ACCOUNT_DEBUG"] = "調試模式";
$MESS["VI_ACCOUNT_ERROR"] = "錯誤獲取數據。請稍後再試。";
$MESS["VI_ACCOUNT_ERROR_LICENSE"] = "錯誤驗證許可證密鑰。請檢查您的條目，然後重試。";
$MESS["VI_ACCOUNT_ERROR_PUBLIC"] = "您輸入了無效的公共地址。";
$MESS["VI_ACCOUNT_ERROR_PUBLIC_EXTENDED"] = "您指定的公共URL不正確。請確保可以從互聯網上獲得＃public_url＃。";
$MESS["VI_ACCOUNT_ERROR_PUBLIC_IS_LOCAL"] = "本地IP地址指定為公共URL。";
$MESS["VI_ACCOUNT_ERROR_PUBLIC_WITHOUT_PROTOCOL"] = "未指定公共URL協議。";
$MESS["VI_ACCOUNT_NAME"] = "帳戶名稱";
$MESS["VI_ACCOUNT_URL"] = "現場公共地址";
$MESS["VI_TAB_SETTINGS"] = "設定";
$MESS["VI_TAB_TITLE_SETTINGS_2"] = "連接參數";
