<?php
$MESS["VI_PUBLIC_PATH"] = "網站的公共地址：";
$MESS["VI_PUBLIC_PATH_DESC"] = "要使電話正常工作，必須輸入有效的公共站點地址。";
$MESS["VI_PUBLIC_PATH_DESC_2"] = "如果從Internet訪問該網站是有限的，則必須授予某些頁面的訪問權限。有關此內容的詳細信息可以在＃link_start＃文檔＃link_end＃中找到。";
