<?php
$MESS["VI_ERROR_COULD_NOT_CREATE_ACCOUNT"] = "無法在電話控制器上創建電話帳戶";
$MESS["VI_ERROR_EMAIL_NOT_CONFIRMED"] = "您必須確認電子郵件地址以連接電話。";
$MESS["VI_ERROR_EMAIL_NOT_CONFIRMED_2"] = "您必須驗證您的電子郵件地址";
$MESS["VI_ERROR_USER_NOT_FOUND"] = "找不到用戶。";
$MESS["VI_ERROR_USER_NOT_REGISTERED"] = "用戶未在電話控制器上註冊";
$MESS["VI_ERROR_USER_NO_EXTRANET"] = "Extranet用戶無法使用電話";
$MESS["VI_USER_PASS_ERROR"] = "密碼錯誤";
