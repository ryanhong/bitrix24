<?php
$MESS["ERROR_NUMBER"] = "電話號碼必須採用國際格式。示例：+44 20 1234 5678";
$MESS["ERROR_PERSONAL_MOBILE"] = "\“移動\”字段不正確。";
$MESS["ERROR_PERSONAL_PHONE"] = "\“ Phone \”字段不正確。";
$MESS["ERROR_PHONE_INNER_2"] = "在“擴展號”字段中輸入的不正確值。該數字必須在1到9999之間。";
$MESS["ERROR_PHONE_INNER_IN_USAGE"] = "擴展號是不正確的。其他用戶或組已經在使用此數字。";
$MESS["ERROR_WORK_PHONE"] = "\“工作電話\”字段不正確。";
$MESS["VI_EVENTS_NOTIFICATIONS"] = "電話通知";
$MESS["VI_EVENTS_SIP_STATUS_NOTIFICATIONS"] = "通知何時sip連接<br>狀態更改";
