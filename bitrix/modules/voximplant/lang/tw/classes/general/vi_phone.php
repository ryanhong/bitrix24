<?php
$MESS["VI_PHONE_ATTACH_ERROR_20"] = "購買電話號碼時提供商錯誤。請稍後再試。";
$MESS["VI_PHONE_ATTACH_ERROR_25"] = "電話號碼購買服務暫時不可用。請稍後再試。";
$MESS["VI_PHONE_ATTACH_ERROR_115"] = "錯誤將請求發送給提供商。請聯繫Helpdesk（115）";
$MESS["VI_PHONE_ATTACH_ERROR_121"] = "錯誤將請求發送給提供商。請聯繫Helpdesk（121）";
$MESS["VI_PHONE_ATTACH_ERROR_127"] = "平衡不足";
$MESS["VI_PHONE_ATTACH_ERROR_162"] = "此數字當前不可用。請稍後再試。";
$MESS["VI_PHONE_ATTACH_ERROR_182"] = "該號碼已經在另一個客戶使用中使用。";
$MESS["VI_PHONE_ATTACH_ERROR_239"] = "錯誤將請求發送給提供商。請聯繫Helpdesk（239）";
$MESS["VI_PHONE_ATTACH_ERROR_240"] = "錯誤將請求發送給提供商。請聯繫Helpdesk（240）";
$MESS["VI_PHONE_ATTACH_ERROR_241"] = "錯誤將請求發送給提供商。請聯繫Helpdesk（241）";
$MESS["VI_PHONE_ATTACH_ERROR_242"] = "錯誤將請求發送給提供商。請聯繫Helpdesk（242）";
$MESS["VI_PHONE_ATTACH_ERROR_243"] = "錯誤將請求發送給提供商。請聯繫Helpdesk（243）";
$MESS["VI_PHONE_ATTACH_ERROR_281"] = "驗證文檔需要上傳";
$MESS["VI_PHONE_ATTACH_ERROR_282"] = "文檔仍在驗證";
$MESS["VI_PHONE_ATTACH_ERROR_331"] = "該區域沒有可用的電話號碼";
$MESS["VI_PHONE_ATTACH_ERROR_360"] = "地址驗證不正確";
$MESS["VI_PHONE_ATTACH_ERROR_395"] = "您已經購買了這個號碼";
$MESS["VI_PHONE_ATTACH_ERROR_UNKNOWN_REGION"] = "未知區域";
$MESS["VI_PHONE_CATEGORY_GEOGRAPHIC"] = "當地的";
$MESS["VI_PHONE_CATEGORY_GEO_CATEGORY1"] = "本地（類別1）";
$MESS["VI_PHONE_CATEGORY_GEO_CATEGORY2"] = "本地（類別2）";
$MESS["VI_PHONE_CATEGORY_MOBILE"] = "智慧型手機";
$MESS["VI_PHONE_CATEGORY_MOSCOW495"] = "莫斯科（495）";
$MESS["VI_PHONE_CATEGORY_NATIONAL"] = "國家的";
$MESS["VI_PHONE_CATEGORY_TOLLFREE"] = "免費電話";
$MESS["VI_PHONE_CATEGORY_TOLLFREE804"] = "Tollfree（8-804）";
$MESS["VI_PHONE_CATEGORY_TOLLFREE_OTHER"] = "免費收費";
$MESS["VI_PHONE_CODE_AR"] = "阿根廷";
$MESS["VI_PHONE_CODE_AT"] = "奧地利";
$MESS["VI_PHONE_CODE_AU"] = "澳大利亞";
$MESS["VI_PHONE_CODE_BE"] = "比利時";
$MESS["VI_PHONE_CODE_BG"] = "保加利亞";
$MESS["VI_PHONE_CODE_BH"] = "巴林";
$MESS["VI_PHONE_CODE_BR"] = "巴西";
$MESS["VI_PHONE_CODE_CA"] = "加拿大";
$MESS["VI_PHONE_CODE_CH"] = "瑞士";
$MESS["VI_PHONE_CODE_CL"] = "智利";
$MESS["VI_PHONE_CODE_CO"] = "哥倫比亞";
$MESS["VI_PHONE_CODE_CY"] = "賽普勒斯";
$MESS["VI_PHONE_CODE_CZ"] = "捷克共和國";
$MESS["VI_PHONE_CODE_DE"] = "德國";
$MESS["VI_PHONE_CODE_DK"] = "丹麥";
$MESS["VI_PHONE_CODE_DO"] = "多明尼加共和國";
$MESS["VI_PHONE_CODE_EE"] = "愛沙尼亞";
$MESS["VI_PHONE_CODE_ES"] = "西班牙";
$MESS["VI_PHONE_CODE_FI"] = "芬蘭";
$MESS["VI_PHONE_CODE_FR"] = "法國";
$MESS["VI_PHONE_CODE_GB"] = "英國";
$MESS["VI_PHONE_CODE_GE"] = "喬治亞州";
$MESS["VI_PHONE_CODE_GR"] = "希臘";
$MESS["VI_PHONE_CODE_HK"] = "香港";
$MESS["VI_PHONE_CODE_HR"] = "克羅埃西亞";
$MESS["VI_PHONE_CODE_HU"] = "匈牙利";
$MESS["VI_PHONE_CODE_IE"] = "愛爾蘭";
$MESS["VI_PHONE_CODE_IL"] = "以色列";
$MESS["VI_PHONE_CODE_IT"] = "義大利";
$MESS["VI_PHONE_CODE_JP"] = "日本";
$MESS["VI_PHONE_CODE_KR"] = "韓國";
$MESS["VI_PHONE_CODE_KZ"] = "哈薩克";
$MESS["VI_PHONE_CODE_KZ_713"] = "Aktubinsk";
$MESS["VI_PHONE_CODE_KZ_717"] = "阿斯塔納";
$MESS["VI_PHONE_CODE_KZ_718"] = "Pavlodar";
$MESS["VI_PHONE_CODE_KZ_721"] = "卡拉甘達";
$MESS["VI_PHONE_CODE_KZ_725"] = "樸素";
$MESS["VI_PHONE_CODE_KZ_726"] = "塔茲";
$MESS["VI_PHONE_CODE_KZ_727"] = "almaty";
$MESS["VI_PHONE_CODE_LT"] = "立陶宛";
$MESS["VI_PHONE_CODE_LU"] = "盧森堡";
$MESS["VI_PHONE_CODE_LV"] = "拉脫維亞";
$MESS["VI_PHONE_CODE_MT"] = "馬爾他";
$MESS["VI_PHONE_CODE_MX"] = "墨西哥";
$MESS["VI_PHONE_CODE_MY"] = "馬來西亞";
$MESS["VI_PHONE_CODE_NL"] = "荷蘭";
$MESS["VI_PHONE_CODE_NO"] = "挪威";
$MESS["VI_PHONE_CODE_NZ"] = "紐西蘭";
$MESS["VI_PHONE_CODE_PA"] = "巴拿馬";
$MESS["VI_PHONE_CODE_PE"] = "秘魯";
$MESS["VI_PHONE_CODE_PH"] = "菲律賓";
$MESS["VI_PHONE_CODE_PL"] = "波蘭";
$MESS["VI_PHONE_CODE_PR"] = "波多黎各";
$MESS["VI_PHONE_CODE_PT"] = "葡萄牙";
$MESS["VI_PHONE_CODE_RO"] = "羅馬尼亞";
$MESS["VI_PHONE_CODE_RU"] = "俄羅斯";
$MESS["VI_PHONE_CODE_RU_341"] = "Izhevsk";
$MESS["VI_PHONE_CODE_RU_342"] = "珀斯";
$MESS["VI_PHONE_CODE_RU_343"] = "Ekaterineburg";
$MESS["VI_PHONE_CODE_RU_345"] = "衛星";
$MESS["VI_PHONE_CODE_RU_346"] = "Surgut";
$MESS["VI_PHONE_CODE_RU_347"] = "UFA";
$MESS["VI_PHONE_CODE_RU_351"] = "Chelyabinsk";
$MESS["VI_PHONE_CODE_RU_353"] = "奧倫堡";
$MESS["VI_PHONE_CODE_RU_381"] = "OMSK";
$MESS["VI_PHONE_CODE_RU_382"] = "湯姆斯克";
$MESS["VI_PHONE_CODE_RU_383"] = "Novosibirsk";
$MESS["VI_PHONE_CODE_RU_384"] = "凱梅羅沃";
$MESS["VI_PHONE_CODE_RU_390"] = "阿巴坎";
$MESS["VI_PHONE_CODE_RU_391"] = "克勞索爾斯克";
$MESS["VI_PHONE_CODE_RU_401"] = "Kaliningrad";
$MESS["VI_PHONE_CODE_RU_421"] = "Khabarovsk";
$MESS["VI_PHONE_CODE_RU_423"] = "弗拉基維斯托克";
$MESS["VI_PHONE_CODE_RU_472"] = "Belgorod";
$MESS["VI_PHONE_CODE_RU_473"] = "Voronezh";
$MESS["VI_PHONE_CODE_RU_474"] = "Lipetsk";
$MESS["VI_PHONE_CODE_RU_482"] = "tver";
$MESS["VI_PHONE_CODE_RU_484"] = "卡盧加";
$MESS["VI_PHONE_CODE_RU_485"] = "Yarostlavl";
$MESS["VI_PHONE_CODE_RU_486"] = "oryol";
$MESS["VI_PHONE_CODE_RU_487"] = "圖拉";
$MESS["VI_PHONE_CODE_RU_491"] = "瑞贊";
$MESS["VI_PHONE_CODE_RU_495"] = "莫斯科";
$MESS["VI_PHONE_CODE_RU_499"] = "莫斯科";
$MESS["VI_PHONE_CODE_RU_812"] = "聖彼得堡";
$MESS["VI_PHONE_CODE_RU_815"] = "穆爾曼斯克";
$MESS["VI_PHONE_CODE_RU_818"] = "Arkhangelsk";
$MESS["VI_PHONE_CODE_RU_831"] = "Nizhniy Novgorod";
$MESS["VI_PHONE_CODE_RU_842"] = "尤利亞諾夫斯克";
$MESS["VI_PHONE_CODE_RU_843"] = "喀山";
$MESS["VI_PHONE_CODE_RU_844"] = "伏時";
$MESS["VI_PHONE_CODE_RU_845"] = "薩拉托夫";
$MESS["VI_PHONE_CODE_RU_846"] = "薩馬拉";
$MESS["VI_PHONE_CODE_RU_851"] = "阿斯特拉漢";
$MESS["VI_PHONE_CODE_RU_861"] = "Krasnodar";
$MESS["VI_PHONE_CODE_RU_862"] = "索契";
$MESS["VI_PHONE_CODE_RU_863"] = "Rostov-on-Don";
$MESS["VI_PHONE_CODE_RU_872"] = "Makhachkala";
$MESS["VI_PHONE_CODE_RU_3012"] = "Ulan-ude";
$MESS["VI_PHONE_CODE_RU_3435"] = "Nizhny Tagil";
$MESS["VI_PHONE_CODE_RU_3466"] = "Nizhnevartovsk";
$MESS["VI_PHONE_CODE_RU_3473"] = "Sterlitamak";
$MESS["VI_PHONE_CODE_RU_3496"] = "noyabrsk";
$MESS["VI_PHONE_CODE_RU_3519"] = "Magnitogorsk";
$MESS["VI_PHONE_CODE_RU_3522"] = "庫爾根";
$MESS["VI_PHONE_CODE_RU_3537"] = "奧斯克";
$MESS["VI_PHONE_CODE_RU_3812"] = "OMSK";
$MESS["VI_PHONE_CODE_RU_3843"] = "Novokuznetsk";
$MESS["VI_PHONE_CODE_RU_3852"] = "Barnaul";
$MESS["VI_PHONE_CODE_RU_3952"] = "Irkutsk";
$MESS["VI_PHONE_CODE_RU_3955"] = "安加爾斯克";
$MESS["VI_PHONE_CODE_RU_4112"] = "Yakutsk";
$MESS["VI_PHONE_CODE_RU_4162"] = "Blagoveshchensk";
$MESS["VI_PHONE_CODE_RU_4242"] = "Yuzhno-Sakhalinsk";
$MESS["VI_PHONE_CODE_RU_4712"] = "庫爾斯克";
$MESS["VI_PHONE_CODE_RU_4725"] = "斯塔里·奧斯科爾（Stary Oskol）";
$MESS["VI_PHONE_CODE_RU_4752"] = "坦博夫";
$MESS["VI_PHONE_CODE_RU_4812"] = "Smolensk";
$MESS["VI_PHONE_CODE_RU_4832"] = "布萊恩斯克";
$MESS["VI_PHONE_CODE_RU_4842"] = "托利亞蒂";
$MESS["VI_PHONE_CODE_RU_4922"] = "弗拉基米爾";
$MESS["VI_PHONE_CODE_RU_4932"] = "伊万諾沃";
$MESS["VI_PHONE_CODE_RU_4942"] = "Kostroma";
$MESS["VI_PHONE_CODE_RU_8172"] = "Vologda";
$MESS["VI_PHONE_CODE_RU_8202"] = "Cherepovets";
$MESS["VI_PHONE_CODE_RU_8212"] = "Syktyvkar";
$MESS["VI_PHONE_CODE_RU_8332"] = "基洛夫";
$MESS["VI_PHONE_CODE_RU_8342"] = "薩蘭斯克";
$MESS["VI_PHONE_CODE_RU_8352"] = "Cheboksary";
$MESS["VI_PHONE_CODE_RU_8362"] = "Yoshkar-Ola";
$MESS["VI_PHONE_CODE_RU_8412"] = "Penza";
$MESS["VI_PHONE_CODE_RU_8453"] = "恩格斯";
$MESS["VI_PHONE_CODE_RU_8552"] = "Naberezhnye Chelny";
$MESS["VI_PHONE_CODE_RU_8555"] = "Nizhnekamsk";
$MESS["VI_PHONE_CODE_RU_8612"] = "Krasnodar";
$MESS["VI_PHONE_CODE_RU_8617"] = "Novorossiysk";
$MESS["VI_PHONE_CODE_RU_8634"] = "taganrog";
$MESS["VI_PHONE_CODE_RU_8652"] = "Stavropol";
$MESS["VI_PHONE_CODE_RU_8793"] = "Pyatigorsk";
$MESS["VI_PHONE_CODE_RU_35132"] = "MIAS";
$MESS["VI_PHONE_CODE_RU_41656"] = "Tynda";
$MESS["VI_PHONE_CODE_RU_48762"] = "Novomoskovsk";
$MESS["VI_PHONE_CODE_RU_49232"] = "科夫羅夫";
$MESS["VI_PHONE_CODE_RU_87922"] = "Mineralnye Vody";
$MESS["VI_PHONE_CODE_RU_87934"] = "Yessentuki";
$MESS["VI_PHONE_CODE_SE"] = "瑞典";
$MESS["VI_PHONE_CODE_SG"] = "新加坡";
$MESS["VI_PHONE_CODE_SI"] = "斯洛維尼亞";
$MESS["VI_PHONE_CODE_SK"] = "斯洛伐克";
$MESS["VI_PHONE_CODE_SV"] = "薩爾瓦多";
$MESS["VI_PHONE_CODE_TR"] = "火雞";
$MESS["VI_PHONE_CODE_US"] = "美國";
$MESS["VI_PHONE_CODE_VN"] = "越南";
$MESS["VI_PHONE_CODE_ZA"] = "南非共和國";
$MESS["VI_PHONE_DESCRIPTION_LINK"] = "連接直到＃verified_until＃";
$MESS["VI_PHONE_DESCRIPTION_LINK_UNVERIFIED"] = "數字尚未確認";
$MESS["VI_PHONE_DESCRIPTION_RENT"] = "付款直到＃pay_until＃，每月付款＃價格＃";
$MESS["VI_PHONE_DESCRIPTION_RENT_DESCRIPTION"] = "每月費＃價格＃";
$MESS["VI_PHONE_DESCRIPTION_RENT_STATUS"] = "活躍直到＃pay_until＃";
$MESS["VI_PHONE_DESCRIPTION_RENT_TO_DELETE"] = "在＃disconnect_date＃上斷開連接。自動更新禁用";
$MESS["VI_PHONE_DESCRIPTION_RENT_TO_DELETE_DESCRIPTION"] = "自動更新禁用";
$MESS["VI_PHONE_DESCRIPTION_RENT_TO_DELETE_STATUS"] = "在＃disconnect_date＃上斷開連接";
$MESS["VI_PHONE_GROUP"] = "數字池";
$MESS["VI_PHONE_PACKAGE"] = "＃計數＃號碼包";
