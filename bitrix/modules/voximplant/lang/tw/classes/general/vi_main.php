<?php
$MESS["VI_CALL_CHAT_UNIFY"] = "電話";
$MESS["VI_DEMO_TOPUP_WARNING"] = "請注意，任何商業計劃都可以使用電話。當您的試用期到期時，即使您的帳戶中仍然有足夠的資金，您也無法撥打電話。但是，所有租用的電話號碼和資金都將保留。升級到商業計劃之一後，您將能夠繼續使用電話。 <a href= \"#link# \">詳細信息</a>";
$MESS["VI_DEMO_TOPUP_WARNING_TITLE"] = "演示模式僅用於測試BitRix24功能。";
$MESS["VI_ERROR_EMAIL_NOT_CONFIRMED"] = "您必須確認您的電子郵件地址才能撥打電話。";
$MESS["VI_ERROR_NEW_CHAT"] = "錯誤創建聊天。";
$MESS["VI_ERROR_NUMBER"] = "電話號碼不正確。";
$MESS["VI_GET_ACCOUNT_INFO"] = "錯誤獲取帳戶信息。";
$MESS["VI_GET_USER_INFO"] = "錯誤獲取用戶信息。";
$MESS["VI_TOS_EN"] = "您將離開Bitrix24網站，並將轉發到Voximplant網站。通過繼續使用此服務，您可以接受voximplant數據處理協議<a href= \"https://voximplant.com/assets/uploads/data-processing-addendum.pdf \" target= \“_blank \"_blank \" > https：// https：// https：// https：// voximplant.com/assets/uploads/data-processing-addendum.pdf </a>";
$MESS["VI_TOS_EN_2"] = "您將離開Bitrix24網站，並將轉發到Voximplant網站。通過繼續使用此服務，您可以接受voximplant＃link1start＃數據處理附錄＃link1end＃and voximplant＃link2start＃隱私策略＃link2end＃。";
$MESS["VI_TOS_RU"] = "<p>您將被重定向到<a href= \之https://billing.voximplant.com/xtrom tart稱為提供商）以補充您的餘額。</p>
<p>單擊\“ poppup \”：</p>
<ol>
<li>允許Bitrix24使提供商訪問您的個人數據（電子郵件，IP地址等），提供商可能需要全部提供服務。 <strong> bitrix24將對提供商可能採取的數據採取任何行動不承擔任何責任。</strong>提供商對您的數據承擔全部責任：<a href = \ a https：// voximplant 。
<li>您不會使用提供商服務：
<ul>
<li>執行當地法律禁止的任何行動； </li>
<li>進行自動散裝電話（垃圾郵件）或其他不使用口頭通信的自動電話。</li>
</ul>
</li>
</ol>";
$MESS["VI_TRIAL_CALL_INTERCEPT_TEXT"] = "使用呼叫攔截進行其他員工無法提供的入站呼叫。涉及的員工必須是負責入站電話的同一隊列組的成員。";
$MESS["VI_TRIAL_CALL_INTERCEPT_TITLE"] = "選定的商業計劃中可以使用呼叫攔截";
$MESS["VI_TRIAL_FEATURES_1"] = "記錄所有電話（每月超過100個）";
$MESS["VI_TRIAL_FEATURES_2"] = "同時致電所有可用員工";
$MESS["VI_TRIAL_FEATURES_3"] = "客戶評估質量";
$MESS["VI_TRIAL_FEATURES_4"] = "從電話號碼（CRM）得出呼叫源";
$MESS["VI_TRIAL_FEATURES_5"] = "致電分析和報告";
$MESS["VI_TRIAL_FEATURES_6"] = "語音菜單（IVR）";
$MESS["VI_TRIAL_FEATURES_6_3"] = "語音菜單（IVR）（選定的商業計劃）";
$MESS["VI_TRIAL_FEATURES_7"] = "自動呼叫轉錄";
$MESS["VI_TRIAL_G_F1"] = "將三個以上的員工添加到隊列中";
$MESS["VI_TRIAL_G_F2"] = "創建多個隊列組並指定自動分發呼叫";
$MESS["VI_TRIAL_G_P1"] = "如果入站呼叫的數量大大增加，則可以在不同的員工或不同隊列組之間分配它們：";
$MESS["VI_TRIAL_G_P2"] = "要在隊列組中增加三個以上的員工或創建多個隊列組，請立即升級到選定的商業計劃！";
$MESS["VI_TRIAL_LINE_SELECT_TEXT"] = "此功能使員工可以在撥打電話之前選擇合適的外站電話號碼。呼叫員工必須對所選行有適當的訪問權限。";
$MESS["VI_TRIAL_LINE_SELECT_TITLE"] = "選定的商業計劃中可以選擇出站電話號碼";
$MESS["VI_TRIAL_LINK"] = "https://www.bitrix24.com/pro/call.php";
$MESS["VI_TRIAL_LINK_TEXT"] = "了解更多";
$MESS["VI_TRIAL_N_P1"] = "免費計劃僅限於一個租用的電話號碼。";
$MESS["VI_TRIAL_N_P2"] = "如果您需要更多租用的電話號碼，請升級到選定的商業計劃。";
$MESS["VI_TRIAL_SOON"] = "很快";
$MESS["VI_TRIAL_S_F1"] = "可以撥打外部電話號碼的員工";
$MESS["VI_TRIAL_S_F2"] = "允許的目的地（例如，只有CRM註冊客戶）";
$MESS["VI_TRIAL_S_F3"] = "用戶允許播放通話記錄";
$MESS["VI_TRIAL_S_F4"] = "用戶允許租用電話號碼；配置呼叫路由和其他首選項。";
$MESS["VI_TRIAL_S_P1"] = "選定的商業計劃中提供了每個用戶電話和電話設置訪問權限。";
$MESS["VI_TRIAL_S_P2"] = "您可以指定：";
$MESS["VI_TRIAL_S_P3"] = "對於擁有大量員工的企業，限制電話訪問的企業是有意義的，可以根據員工的工作要求向員工提供外部電話。";
$MESS["VI_TRIAL_S_TITLE"] = "分配訪問權限";
$MESS["VI_TRIAL_TARIFF"] = "高級電話 +高級CRM和其他功能在選定的商業計劃中提供。";
$MESS["VI_TRIAL_TEXT_TITLE"] = "添加到您的電話中：";
$MESS["VI_TRIAL_TITLE"] = "Bitrix24中的高級電話";
