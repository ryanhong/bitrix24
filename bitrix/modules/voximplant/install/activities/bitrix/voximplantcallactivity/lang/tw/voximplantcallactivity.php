<?php
$MESS["BPVICA_ERROR_AUDIO_FILE"] = "音頻文件未指定。";
$MESS["BPVICA_ERROR_NUMBER"] = "未指定訂戶的編號。";
$MESS["BPVICA_ERROR_OUTPUT_NUMBER"] = "電話號碼未指定。";
$MESS["BPVICA_ERROR_TEXT"] = "沒有提供要說的文字。";
$MESS["BPVICA_INCLUDE_MODULE"] = "電話模塊未安裝。";
$MESS["BPVICA_PROPERTY_TEXT"] = "將文字轉換為語音";
$MESS["BPVICA_RESULT_FALSE"] = "失敗的";
$MESS["BPVICA_RESULT_TRUE"] = "成功";
$MESS["BPVICA_TRACK_SUBSCR"] = "等待Robocall結果";
