<?php
$MESS["BPVICA_DESCR_DESCR"] = "預先錄製或基於文本的Robocall";
$MESS["BPVICA_DESCR_DESCR_1"] = "語音機器人在特定時間或使用指定的規則來調用客戶。";
$MESS["BPVICA_DESCR_NAME"] = "Robocall";
$MESS["BPVICA_DESCR_NAME_1"] = "打個電話";
$MESS["BPVICA_DESCR_RESULT"] = "通話結果";
$MESS["BPVICA_DESCR_RESULT_CODE"] = "呼叫結束代碼";
$MESS["BPVICA_DESCR_RESULT_TEXT"] = "呼叫結果（文字）";
