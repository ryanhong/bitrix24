<?php
$MESS["TELEPHONY_REPORT_LOST_CALLS"] = "未接來電";
$MESS["TELEPHONY_REPORT_LOST_CALLS_COUNT"] = "未接聽電話";
$MESS["TELEPHONY_REPORT_LOST_CALLS_DATE"] = "日期";
$MESS["TELEPHONY_REPORT_LOST_CALLS_DYNAMICS"] = "更改與以前的時期";
$MESS["TELEPHONY_REPORT_LOST_CALLS_HELP"] = "百分比差異是當前選擇的報告期的問題；它顯示了當前報告期的值增加或減少到上一時期註冊的值。例如，如果選擇了\“上個月\”，並且上個月是5月，則前期是四月。";
