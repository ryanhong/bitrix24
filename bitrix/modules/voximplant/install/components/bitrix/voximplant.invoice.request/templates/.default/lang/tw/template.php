<?php
$MESS["VOX_CLOSING_DOCS_CANCEL"] = "取消";
$MESS["VOX_CLOSING_DOCS_EMAIL"] = "聯繫電子郵件地址：";
$MESS["VOX_CLOSING_DOCS_MONTH_01"] = "一月";
$MESS["VOX_CLOSING_DOCS_MONTH_02"] = "二月";
$MESS["VOX_CLOSING_DOCS_MONTH_03"] = "3月";
$MESS["VOX_CLOSING_DOCS_MONTH_04"] = "四月";
$MESS["VOX_CLOSING_DOCS_MONTH_05"] = "可能";
$MESS["VOX_CLOSING_DOCS_MONTH_06"] = "六月";
$MESS["VOX_CLOSING_DOCS_MONTH_07"] = "七月";
$MESS["VOX_CLOSING_DOCS_MONTH_08"] = "八月";
$MESS["VOX_CLOSING_DOCS_MONTH_09"] = "九月";
$MESS["VOX_CLOSING_DOCS_MONTH_10"] = "十月";
$MESS["VOX_CLOSING_DOCS_MONTH_11"] = "十一月";
$MESS["VOX_CLOSING_DOCS_MONTH_12"] = "十二月";
$MESS["VOX_CLOSING_DOCS_PERIOD"] = "請求發票以：";
$MESS["VOX_CLOSING_DOCS_PLACEHOLDER_ADDRESS"] = "地址";
$MESS["VOX_CLOSING_DOCS_PLACEHOLDER_INDEX"] = "郵遞區號";
$MESS["VOX_CLOSING_DOCS_REQUEST"] = "要求";
$MESS["VOX_CLOSING_DOCS_REQUEST_SENT"] = "請求已成功發送";
$MESS["VOX_CLOSING_DOCS_REQUEST_TITLE"] = "請求電話發票";
$MESS["VOX_CLOSING_DOCS_YOUR_ADDRESS"] = "你的地址：";
