<?php
$MESS["VI_REGULAR_CONFIG_RENT"] = "管理租用的數字";
$MESS["VI_REGULAR_FEE_EUR"] = "EUR＃錢＃";
$MESS["VI_REGULAR_FEE_RUR"] = "擦＃錢＃";
$MESS["VI_REGULAR_FEE_UAH"] = "嗯＃錢＃。";
$MESS["VI_REGULAR_FEE_USD"] = "\ \$＃錢＃";
$MESS["VI_REGULAR_NOTICE"] = "注意：自動延長電話號碼。";
$MESS["VI_REGULAR_NO_MONEY"] = "您沒有足夠的信用來自動重複付款。您需要充值餘額，直到＃日期＃。";
$MESS["VI_REGULAR_NO_VERIFY"] = "法律要求您提供法律文檔以使用出租數字。<br> <br>您必須上傳文檔，直到＃日期＃＃，否則您的電話號碼將被斷開連接。 <br> <br>＃url_start＃上傳文檔現在＃url_end＃＃";
$MESS["VI_REGULAR_TABLE_FEE"] = "月租費";
$MESS["VI_REGULAR_TABLE_NUMBER"] = "數字";
$MESS["VI_REGULAR_TABLE_PAID_BEFORE"] = "付款直到";
$MESS["VI_REGULAR_TABLE_STATUS_N"] = "殘疾，需要付款";
$MESS["VI_REGULAR_TABLE_STATUS_Y"] = "啟用";
$MESS["VI_REGULAR_TITLE"] = "經常付款";
