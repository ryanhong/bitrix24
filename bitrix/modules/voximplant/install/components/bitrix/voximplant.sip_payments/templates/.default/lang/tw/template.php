<?php
$MESS["VI_NOTICE_BUTTON_DONE"] = "完畢";
$MESS["VI_NOTICE_OLD_CONFIG_OFFICE_PBX"] = "為了加快SIP連接器的工作，您需要從<b> incoming。 > <br>在PBX連接參數中。";
$MESS["VI_SIP_BUTTON"] = "延長";
$MESS["VI_SIP_BUTTON_BUY"] = "連接";
$MESS["VI_SIP_CONFIG"] = "管理SIP號碼";
$MESS["VI_SIP_PAID_BEFORE"] = "模塊將在＃日期＃到期";
$MESS["VI_SIP_PAID_FREE"] = "您有＃Count＃免費分鐘來配置和測試設備。";
$MESS["VI_SIP_PAID_NOTICE"] = "在模塊訂閱到期後，SIP電話號碼將斷開連接。";
$MESS["VI_SIP_PAID_NOTICE_2"] = "免費會議分鐘過期後，SIP編號將被斷開連接，直到您支付SIP連接器每月訂閱為止。";
$MESS["VI_SIP_TITLE"] = "SIP連接器";
