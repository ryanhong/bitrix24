<?php
$MESS["TELEPHONY_CALLERID_NUMBER"] = "您已連接的號碼＃Caller_id＃";
$MESS["TELEPHONY_CONFIRM_DATE"] = "該數字已被確認，它將活躍到＃date＃，之後將自動停用，直到再次確認為止。您可以隨時重新徵集下一個確認的日期。";
$MESS["TELEPHONY_EMPTY_PHONE"] = "沒有提供電話號碼";
$MESS["TELEPHONY_EMPTY_PHONE_DESC"] = "該電話將看到一個中繼電話號碼";
$MESS["TELEPHONY_NOT_CONFIRMED"] = "電話號碼未驗證。您必須驗證此電話號碼，以使您的呼叫者ID可見其他人。";
$MESS["TELEPHONY_NUMBER_CONFIG"] = "配置電話號碼";
$MESS["TELEPHONY_PUT_PHONE"] = "輸入公司電話號碼";
