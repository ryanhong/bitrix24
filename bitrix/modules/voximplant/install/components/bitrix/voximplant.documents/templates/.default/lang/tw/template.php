<?php
$MESS["VI_DOCS_BODY"] = "對於某些國家 /地區，法律要求電話提供商驗證您的住宅狀態以授予租用的電話號碼。<br>您可能需要提交相應的文書工作，以便能夠在整個期限內租用電話號碼租。";
$MESS["VI_DOCS_BODY_2"] = "法律在某些國家 /地區要求電話公司必須向您索取法律文件，然後才能使用租用的號碼。<br>如果您想在租賃期內保留和使用這些數字，則這是一項要求。您將無法使用數字，直到您上傳所需的文檔並已批准。";
$MESS["VI_DOCS_COUNTRY_RU"] = "俄羅斯";
$MESS["VI_DOCS_SERVICE_ERROR"] = "錯誤發送請求到文檔上傳服務";
$MESS["VI_DOCS_SERVICE_UPLOAD"] = "上傳";
$MESS["VI_DOCS_STATUS"] = "地位：";
$MESS["VI_DOCS_TABLE_COMMENT"] = "評論";
$MESS["VI_DOCS_TABLE_LINK"] = "文檔上傳歷史記錄";
$MESS["VI_DOCS_TABLE_OWNER"] = "所有者";
$MESS["VI_DOCS_TABLE_STATUS"] = "檢查狀態";
$MESS["VI_DOCS_TABLE_TYPE"] = "法人實體";
$MESS["VI_DOCS_TABLE_UPLOAD"] = "上傳";
$MESS["VI_DOCS_TITLE"] = "將文檔上傳到出租電話號碼";
$MESS["VI_DOCS_UNTIL_DATE"] = "您必須上傳文檔，直到＃日期＃";
$MESS["VI_DOCS_UNTIL_DATE_NOTICE"] = "在指定的日期之後，保留數字將被斷開，資金返回您的帳戶。<br> <br>租用的數字將活躍到租賃期限結束。";
$MESS["VI_DOCS_UPDATE_BTN"] = "上傳新文檔";
$MESS["VI_DOCS_UPLOAD_BTN"] = "上傳文檔";
$MESS["VI_DOCS_UPLOAD_NOTICE"] = "請注意，您要提交的文書工作將直接上傳到Voximplant，Inc，並根據相應國家的立法對待。 Bitrix24不會收集，存儲或處理與這些文檔關聯的任何數據。";
$MESS["VI_DOCS_UPLOAD_WHILE_RENT"] = "如果您需要驗證您的文檔以租用電話號碼，則租金用戶界面的號碼將顯示文檔上傳表格";
$MESS["VI_DOCS_WAIT"] = "上傳...請等待";
