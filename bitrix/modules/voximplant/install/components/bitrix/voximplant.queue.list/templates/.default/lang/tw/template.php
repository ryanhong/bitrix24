<?php
$MESS["VOX_QUEUE_CLOSE"] = "關閉";
$MESS["VOX_QUEUE_DELETE_ERROR"] = "刪除隊列組的錯誤。";
$MESS["VOX_QUEUE_IS_USED"] = "該隊列組目前正在使用：";
$MESS["VOX_QUEUE_IVR"] = "IVR";
$MESS["VOX_QUEUE_LIST_ADD"] = "添加隊列組";
$MESS["VOX_QUEUE_LIST_SELECTED"] = "總隊列組";
$MESS["VOX_QUEUE_NUMBER"] = "數字";
