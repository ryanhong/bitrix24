<?
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('NOT_CHECK_FILE_PERMISSIONS', true);
define('PUBLIC_AJAX_MODE', true);
define('NO_KEEP_STATISTIC', 'Y');
define('STOP_STATISTICS', true);
define('BX_SECURITY_SHOW_MESSAGE', true);

require __DIR__ . '/../../vendor/autoload.php';
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/socialnetwork/classes/general/messages.php");

use GuzzleHttp\Client as GuzzleClient;
use \Bitrix\Main\Engine\Controller;
use Bitrix\Main\Loader;
use Bitrix\Im\Message\Uuid;
// use Maknz\Slack\Client as SlackClient;

CModule::IncludeModule("im");

class ChatController extends Controller
{
    public function __construct() 
    {        
		$data = json_decode(file_get_contents('php://input'), true);
        // $slackClient = new SlackClient('https://hooks.slack.com/services/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', [
        //     'username' => 'linebot',
        //     'channel' => '#linebot',
        //     'link_names' => true
        // ]);

        // $slackClient->send(json_encode($data));

        $user_id = $data['events'][0]['source']['userId'];
        $MESSAGE =  $data['events'][0]['message']['text'];

        $fields = [
        	'MESSAGE' => $MESSAGE,
        	'MESSAGE_TYPE' => "P",
        	"DIALOG_ID" => '1',
        	"FROM_USER_ID" => 9,
        ];

		$messageId = CIMMessenger::Add($fields);
		// var_dump($messageId);
    }
}

$ChatController = new ChatController;