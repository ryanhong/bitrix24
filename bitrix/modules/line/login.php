<?
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('NOT_CHECK_FILE_PERMISSIONS', true);
define('PUBLIC_AJAX_MODE', true);
define('NO_KEEP_STATISTIC', 'Y');
define('STOP_STATISTICS', true);
define('BX_SECURITY_SHOW_MESSAGE', true);

require __DIR__ . '/../../vendor/autoload.php';
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use GuzzleHttp\Client as GuzzleClient;
use \Bitrix\Main\Engine\Controller;
use Bitrix\Main\Loader;

class LineController extends Controller
{
	public function __construct()
	{
		global $DB, $APPLICATION;

		$data = $_GET;
		$client = new GuzzleClient();
		$response = $client->post('https://api.line.me/oauth2/v2.1/token', [
		    'form_params' => [
		        'grant_type' => 'authorization_code',
		        'code' =>$data['code'],
		        'redirect_uri' => 'http://49.158.45.98:81/bitrix/modules/line/login.php',
		        'client_id' => '2002446601',
		        'client_secret' => 'ad166d6ddd0371137dadbc4e53621db9'
		    ]
		]);


		$body = json_decode($response->getBody()->getContents(), true);

		$id_token = $body['id_token'];

		$client = new GuzzleClient([
		    'headers' => ["Authorization" => "Bearer ". $body['access_token']]
		]);

		$response = $client->get('https://api.line.me/v2/profile');
		$body = json_decode($response->getBody()->getContents(), true);
		$userId = $data['userId'];

		$client = new GuzzleClient([
		    'headers' => ["Authorization" => "Bearer ". $body['access_token']]
		]);
		$response = $client->post('https://api.line.me/oauth2/v2.1/verify', [
		    'form_params' => [
		        'id_token' => $id_token,
		        'code' => $data['code'],
		        'user_id' => $userId,
		        'client_id' => '2002446601'
		    ]
		]);


		$body = json_decode($response->getBody()->getContents(), true);
		$strError = '';		

		if (!$arUser) {

			$userData = [
				// 'DEPARTMENT_ID' => ,
				'ADD_EMAIL' => $body['email'],
				'ADD_NAME' => $body['name'],
				'ADD_LAST_NAME' => ' ',
				'ADD_POSITION' => ' ',
				'EXTERNAL_AUTH_ID' => $userId
			];

			$userData["DEPARTMENT_ID"] = Bitrix\Intranet\DepartmentStructure::getInstance(SITE_ID)->getBaseDepartmentId();
			$idAdded = CIntranetInviteDialog::AddNewUser(SITE_ID, $userData, $strError);
		}

		$strSql =
			"SELECT U.ID, U.LOGIN, U.ACTIVE, U.BLOCKED, U.PASSWORD, U.PASSWORD_EXPIRED, U.LOGIN_ATTEMPTS, U.CONFIRM_CODE, U.EMAIL " .
			"FROM b_user U  " .
			"WHERE U.LOGIN='" . $DB->ForSQL($body['email']) . "' ";
		$result = $DB->Query($strSql);
		$arUser = $result->Fetch();


		$CAllUser = new CAllUser;
		$CAllUser->Authorize($arUser["ID"], 'Y');
		\localRedirect('/');
	}
}

$LineController = new LineController;