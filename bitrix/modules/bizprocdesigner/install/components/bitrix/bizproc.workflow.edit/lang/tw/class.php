<?php
$MESS["BIZPROC_USER_PARAMS_SAVE_ERROR"] = "\“我的活動\”酒吧中的一個或多個活動太大。更改將無法保存。";
$MESS["BIZPROC_WFEDIT_CATEGORY_CONSTR_1"] = "流量控制";
$MESS["BIZPROC_WFEDIT_CATEGORY_DOC_1"] = "元素處理";
$MESS["BIZPROC_WFEDIT_CATEGORY_INTER"] = "交互式設置";
$MESS["BIZPROC_WFEDIT_CATEGORY_MAIN"] = "主要的";
$MESS["BIZPROC_WFEDIT_CATEGORY_OTHER"] = "其他";
$MESS["BIZPROC_WFEDIT_CATEGORY_REST_1"] = "申請活動";
$MESS["BIZPROC_WFEDIT_CATEGORY_TASKS_1"] = "作業";
$MESS["BIZPROC_WFEDIT_CONSTANTS_SAVE_ERROR"] = "由於模板常數太大而無法保存模板。";
$MESS["BIZPROC_WFEDIT_DEFAULT_TITLE"] = "業務過程模板";
$MESS["BIZPROC_WFEDIT_ERROR_TYPE"] = "需要文檔類型。";
$MESS["BIZPROC_WFEDIT_IMPORT_ERROR"] = "導入業務過程模板的錯誤。";
$MESS["BIZPROC_WFEDIT_PARAMETERS_SAVE_ERROR"] = "由於模板參數太大而無法保存模板。";
$MESS["BIZPROC_WFEDIT_SAVE_ERROR"] = "保存對象時發生錯誤：";
$MESS["BIZPROC_WFEDIT_TITLE_ADD"] = "新的業務流程模板";
$MESS["BIZPROC_WFEDIT_TITLE_EDIT"] = "編輯業務流程模板";
$MESS["BIZPROC_WFEDIT_VARIABLES_SAVE_ERROR"] = "由於模板變量太大而無法保存模板。";
