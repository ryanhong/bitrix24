<?php
$MESS["STATWIZ_CANCELSTEP_BUTTONTITLE"] = "關閉";
$MESS["STATWIZ_CANCELSTEP_CONTENT"] = "嚮導已被取消。";
$MESS["STATWIZ_CANCELSTEP_TITLE"] = "嚮導已被取消";
$MESS["STATWIZ_FILES_NOT_FOUND"] = "找不到文件。從www.maxmind.com或ip-to-country.webhosting.info站點加載文件到指定的文件夾，然後再次運行嚮導。";
$MESS["STATWIZ_FINALSTEP_BUTTONTITLE"] = "結束";
$MESS["STATWIZ_FINALSTEP_CITIES"] = "城市：＃計數＃。";
$MESS["STATWIZ_FINALSTEP_CITY_IPS"] = "IP範圍：＃count＃。";
$MESS["STATWIZ_FINALSTEP_COUNTRIES"] = "國家：＃count＃。";
$MESS["STATWIZ_FINALSTEP_TITLE"] = "嚮導完成了";
$MESS["STATWIZ_NO_MODULE_ERROR"] = "Web分析模塊未安裝。嚮導無法繼續。";
$MESS["STATWIZ_STEP1_CITY"] = "<b> City </b>和<b> Country </b>查找的IP地址索引。";
$MESS["STATWIZ_STEP1_CITY_NOTE"] = "支持格式：
<ul>
<li> <a target= \"_blank \" href= \"#geoip_href# \“> geoip City </a>。</li>
<li> <a target= \"_blank \" href= \"#geoiplite_href# \“> geolite City </a>。</li>
<li> <a target = \"_blank \" href= \"#ipgeobase_href# \“> ipgeobase </a>。</li>
</ul>";
$MESS["STATWIZ_STEP1_COMMON_NOTE"] = "將檔案和上傳文件上傳到＃路徑＃目錄。然後，您可以進入下一個嚮導步驟。";
$MESS["STATWIZ_STEP1_CONTENT"] = "歡迎來到IP地址索引創建嚮導！此嚮導將幫助您為城市和國家查找的IP地址索引創建IP地址索引。<br />選擇操作：";
$MESS["STATWIZ_STEP1_COUNTRY"] = "<b> City </b>查找的IP地址索引。";
$MESS["STATWIZ_STEP1_COUNTRY_NOTE_V2"] = "支持格式：
<ul>
<li> <a target= \"_blank \" href= \"#geoip_href# \“> geoip country </a>。</li>
<li> <a target= \"_blank \" href= \"#geoiplite_href# \“> geolite country </a>。</li>
</ul>";
$MESS["STATWIZ_STEP1_TITLE"] = "IP地址索引嚮導";
$MESS["STATWIZ_STEP2_CITY_CHOOSEN"] = "您已經選擇為<b> country </b>和<b> City </b>查找創建IP地址索引。";
$MESS["STATWIZ_STEP2_CONTENT"] = "搜索已在\“/bitrix/模塊/統計/ip2country \”文件夾中執行。";
$MESS["STATWIZ_STEP2_COUNTRY_CHOOSEN"] = "您已經選擇為<b> country </b>查找創建IP地址索引。";
$MESS["STATWIZ_STEP2_DESCRIPTION"] = "描述";
$MESS["STATWIZ_STEP2_FILE_ERROR"] = "沒有加載文件。";
$MESS["STATWIZ_STEP2_FILE_NAME"] = "文件名";
$MESS["STATWIZ_STEP2_FILE_SIZE"] = "尺寸";
$MESS["STATWIZ_STEP2_FILE_TYPE_IPGEOBASE"] = "IP範圍數據庫IPGEOBASE（僅俄羅斯）。國家指數應首先加載。";
$MESS["STATWIZ_STEP2_FILE_TYPE_IPGEOBASE2"] = "ipgeobase數據庫的第二部分；將IP地址與位置匹配。這必須在第一部分之後加載。";
$MESS["STATWIZ_STEP2_FILE_TYPE_IPGEOBASE2_CITY"] = "ipgeobase數據庫的第二部分；包含位置。首先加載國家指數以定義國家。";
$MESS["STATWIZ_STEP2_FILE_TYPE_IP_TO_COUNTRY"] = "IP到國家數據庫。";
$MESS["STATWIZ_STEP2_FILE_TYPE_MAXMIND_CITY_LOCATION"] = "地理城市或Geolite City數據庫的一部分。應首先加載。";
$MESS["STATWIZ_STEP2_FILE_TYPE_MAXMIND_IP_COUNTRY"] = "Geoip國家或Geolite國家數據庫。";
$MESS["STATWIZ_STEP2_FILE_TYPE_MAXMIND_IP_LOCATION"] = "IP範圍是Geoip City或Geolite City數據庫的一部分。應在位置部分後加載。";
$MESS["STATWIZ_STEP2_FILE_TYPE_UNKNOWN"] = "未知格式。";
$MESS["STATWIZ_STEP2_TITLE"] = "CSV文件選擇";
$MESS["STATWIZ_STEP3_LOADING"] = "加工...";
$MESS["STATWIZ_STEP3_TITLE"] = "正在創建索引。";
