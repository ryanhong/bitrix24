<?php
$MESS["GD_STAT_P_EVENT"] = "事件";
$MESS["GD_STAT_P_GRAPH_DAYS"] = "繪製圖表的日子";
$MESS["GD_STAT_P_GRAPH_HEIGHT"] = "高度";
$MESS["GD_STAT_P_GRAPH_PARAMS"] = "圖表參數";
$MESS["GD_STAT_P_GRAPH_WIDTH"] = "寬度";
$MESS["GD_STAT_P_GUEST"] = "訪客";
$MESS["GD_STAT_P_HIDE_GRAPH"] = "隱藏圖";
$MESS["GD_STAT_P_HIT"] = "命中";
$MESS["GD_STAT_P_HOST"] = "主持人";
$MESS["GD_STAT_P_SESSION"] = "會議";
$MESS["GD_STAT_P_SITE_ID"] = "網站";
$MESS["GD_STAT_P_SITE_ID_ALL"] = "全部";
