<?php
$MESS["GD_STAT_B_YESTERDAY"] = "兩天前";
$MESS["GD_STAT_EVENTS"] = "事件";
$MESS["GD_STAT_HITS"] = "命中";
$MESS["GD_STAT_HOSTS"] = "主持人";
$MESS["GD_STAT_NOT_ENOUGH_DATA"] = "數據不足以構建圖形。";
$MESS["GD_STAT_SESSIONS"] = "會議";
$MESS["GD_STAT_TAB_ADV"] = "ADV。活動";
$MESS["GD_STAT_TAB_COMMON"] = "受歡迎程度";
$MESS["GD_STAT_TAB_EVENT"] = "事件";
$MESS["GD_STAT_TAB_PHRASE"] = "短語";
$MESS["GD_STAT_TAB_REF"] = "網站";
$MESS["GD_STAT_TODAY"] = "今天";
$MESS["GD_STAT_TOTAL"] = "全部的";
$MESS["GD_STAT_VISITORS"] = "訪客";
$MESS["GD_STAT_YESTERDAY"] = "昨天";
