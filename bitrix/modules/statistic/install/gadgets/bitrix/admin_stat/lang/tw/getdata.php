<?php
$MESS["GD_STAT_ADV_NAME"] = "廣告攻勢";
$MESS["GD_STAT_BEFORE_YESTERDAY"] = "兩天前";
$MESS["GD_STAT_EVENT"] = "事件";
$MESS["GD_STAT_EVENT_GRAPH"] = "每日事件類型圖";
$MESS["GD_STAT_NO_DATA"] = "沒有數據。";
$MESS["GD_STAT_PHRASE"] = "關鍵詞";
$MESS["GD_STAT_SERVER"] = "網站";
$MESS["GD_STAT_TODAY"] = "今天";
$MESS["GD_STAT_TOTAL_1"] = "全部的";
$MESS["GD_STAT_YESTERDAY"] = "昨天";
