<?php
$MESS["STAT_CLICKS"] = "點擊";
$MESS["STAT_DEFENCE_LOG_EVENT"] = "活動限制超出了。";
$MESS["STAT_DEFENCE_LOG_MESSAGE"] = "訪問者活動：＃activity_hits＃pers caster percor_time_time_limit＃sec執行的命中。 ＃Activity_exceeding＃命中超過的活動限制。";
$MESS["STAT_LINK"] = "關聯";
$MESS["STAT_LINK_STAT"] = "鏈接點擊頁面上";
$MESS["STAT_LINK_STAT_HIDE_PANEL_BUTTON"] = "隱藏鏈接點擊此頁面的統計信息";
$MESS["STAT_LINK_STAT_PANEL_BUTTON"] = "顯示此頁面的鏈接點擊圖";
$MESS["STAT_LINK_STAT_PANEL_BUTTON_ALERT"] = "無鏈接單擊此頁面的統計信息。";
$MESS["STAT_LINK_STAT_SHOW_PANEL_BUTTON"] = "顯示鏈接點擊此頁面的統計信息";
$MESS["STAT_LINK_STAT_TITLE"] = "鏈接點擊圖";
$MESS["STAT_PAGE_GRAPH_PANEL_BUTTON"] = "頁面流量";
$MESS["STAT_PANEL_BUTTON"] = "統計數據";
$MESS["STAT_PANEL_BUTTON_HINT"] = "查看頁面流量或點擊統計信息";
