<?php
$MESS["STAT_CITY_GEOIP_PHP_DESCR"] = "用於GEOIP數據訪問的PHP模塊。 <a href= \"http://www.maxmind.com/app/php \" target= \"_blank \">確保安裝了最新的模塊版本。檢查geoip_database_file常數是否在dbconn.php中定義，並包含通往IP數據庫的完整路徑。";
$MESS["STAT_CITY_GEOIP_PHP_LATITUDE"] = "經度";
$MESS["STAT_CITY_GEOIP_PHP_LONGITUDE"] = "緯度";
$MESS["STAT_CITY_GEOIP_PHP_POSTAL_CODE"] = "郵遞區號";
