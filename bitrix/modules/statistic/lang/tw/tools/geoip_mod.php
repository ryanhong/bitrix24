<?php
$MESS["STAT_CITY_GEOIP_MOD_CONTINENT"] = "大陸";
$MESS["STAT_CITY_GEOIP_MOD_DESCR"] = "Apache Module <a href= \"http://www.maxmind.com/app/mod_geoip \“target = \"_blank \"> mod_geoip </a>。所需的模塊版本：1.2.5或更高版本。";
$MESS["STAT_CITY_GEOIP_MOD_LATITUDE"] = "經度";
$MESS["STAT_CITY_GEOIP_MOD_LONGITUDE"] = "緯度";
