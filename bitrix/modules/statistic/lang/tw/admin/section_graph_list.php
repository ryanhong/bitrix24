<?php
$MESS["STAT_ADV_BACK"] = "返回";
$MESS["STAT_ADV_LIST"] = "以下廣告活動涵蓋了：";
$MESS["STAT_ADV_NO_BACK"] = "直接命中";
$MESS["STAT_ADV_SUMMA"] = "直接命中和返回總數";
$MESS["STAT_CREATE_GRAPH"] = "構建圖";
$MESS["STAT_ENTER_POINTS"] = "輸入點";
$MESS["STAT_EXIT_POINTS"] = "出口點";
$MESS["STAT_GO_LINK"] = "轉到此頁面";
$MESS["STAT_HITS"] = "命中";
$MESS["STAT_TITLE_PAGE"] = "頁面流量";
$MESS["STAT_TITLE_SECTION"] = "部分流量";
