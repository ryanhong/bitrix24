<?php
$MESS["STAT_FL_SEACHERS"] = "搜尋引擎";
$MESS["STAT_F_PERIOD"] = "時期";
$MESS["STAT_F_SEACHERS"] = "搜尋引擎:";
$MESS["STAT_NO_DATA"] = "沒有足夠的數據來創建圖形";
$MESS["STAT_RECORDS_LIST"] = "站點索引餅圖";
$MESS["STAT_VIEW_SEARCHER_GRAPH"] = "搜索引擎：站點索引圖";
$MESS["STAT_VIEW_SEARCHER_HITS"] = "搜索引擎命中";
