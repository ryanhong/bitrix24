<?php
$MESS["STAT_CHOOSE"] = "選擇";
$MESS["STAT_CHOOSE_TITLE"] = "選擇事件類型並關閉窗口。";
$MESS["STAT_DESCRIPTION"] = "描述";
$MESS["STAT_EVENT_TYPE_PAGES"] = "事件類型";
$MESS["STAT_NAME"] = "姓名";
$MESS["STAT_SELECT"] = "選擇";
$MESS["STAT_SELECT_TITLE"] = "選擇標記類型";
$MESS["STAT_TITLE"] = "事件類型選擇";
