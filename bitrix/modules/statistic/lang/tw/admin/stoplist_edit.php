<?php
$MESS["STAT_ACTIONS"] = "動作";
$MESS["STAT_ACTIVE"] = "積極的：";
$MESS["STAT_ACTIVITY"] = "記錄活動";
$MESS["STAT_COMMENT"] = "管理員評論";
$MESS["STAT_COMMENT_S"] = "評論";
$MESS["STAT_CONDITIONS"] = "過濾訪問者的功能（\“和\”邏輯）";
$MESS["STAT_DEFAULT_MESSAGE"] = "網站管理員禁止訪問";
$MESS["STAT_DELETE_STOPLIST"] = "刪除停止列表記錄";
$MESS["STAT_DELETE_STOPLIST_CONFIRM"] = "您確定要刪除此停止列表記錄嗎？";
$MESS["STAT_EDIT_RECORD"] = "停止列表記錄＃＃id＃的設置";
$MESS["STAT_EMPTY"] = "空的";
$MESS["STAT_END_DATE"] = "結束日期";
$MESS["STAT_ERROR"] = "錯誤保存記錄";
$MESS["STAT_GREEN_LAMP"] = "積極的";
$MESS["STAT_IP"] = "淨IP地址：";
$MESS["STAT_MASK"] = "淨面膜：";
$MESS["STAT_MESSAGE"] = "顯示消息：";
$MESS["STAT_MESSAGE_LID"] = "編碼：";
$MESS["STAT_NEW_RECORD"] = "新停止列表記錄";
$MESS["STAT_NEW_STOPLIST"] = "新停止列表記錄";
$MESS["STAT_PARAMS"] = "設定";
$MESS["STAT_PARAMS_S"] = "記錄設置";
$MESS["STAT_RECORDS_LIST"] = "停止列表";
$MESS["STAT_REDIRECT"] = "重定向到URL：";
$MESS["STAT_RED_LAMP"] = "不活躍";
$MESS["STAT_SAVE_STATISTIC"] = "保存統計信息：";
$MESS["STAT_START_DATE"] = "開始日期";
$MESS["STAT_TIMESTAMP"] = "修改的：";
$MESS["STAT_URL_FROM"] = "從：";
$MESS["STAT_URL_TO"] = "到：";
$MESS["STAT_USER_AGENT"] = "用戶代理：";
$MESS["STAT_WHAT_TO_DO"] = "設定採取什麼行動";
