<?php
$MESS["STAT_ATTENTION_1"] = "注意力";
$MESS["STAT_CALENDAR"] = "日曆";
$MESS["STAT_CHARGEBACK"] = "退款";
$MESS["STAT_CONVERT_TO_DEFAULT_CUR"] = "在加載所有金額之前，它們將轉換為基本貨幣。";
$MESS["STAT_COPY"] = "複製到底線";
$MESS["STAT_CURRENCY"] = "貨幣";
$MESS["STAT_DATE"] = "日期<br>";
$MESS["STAT_DUPLICATE"] = "重複：";
$MESS["STAT_EVENTS_LOADING"] = "活動加載正在進行中。請稍等....";
$MESS["STAT_EVENT_TYPE"] = "事件類型<br> ID";
$MESS["STAT_FILE"] = "CSV文件：";
$MESS["STAT_FROM_FILE"] = "從文件";
$MESS["STAT_FROM_FORM"] = "手動進入";
$MESS["STAT_HANDLER"] = "CSV文件處理程序：";
$MESS["STAT_LOAD"] = "載入";
$MESS["STAT_LOADED"] = "載入:";
$MESS["STAT_LOADING_FROM_CSV"] = "從CSV文件加載事件";
$MESS["STAT_LOADING_FROM_TABLE"] = "手動事件加載";
$MESS["STAT_MONEY"] = "金錢數額";
$MESS["STAT_NEXT_STEP"] = "下一步";
$MESS["STAT_ON_STEP"] = "步驟";
$MESS["STAT_PARAM"] = "附加參數<br>（＃group_site_id＃.session。";
$MESS["STAT_PREVIEW"] = "與預覽：";
$MESS["STAT_PROCESSED"] = "處理：";
$MESS["STAT_SELECT_EVENT"] = "選擇事件類型";
$MESS["STAT_STEP"] = "加載步驟（秒）：";
$MESS["STAT_TITLE"] = "事件加載";
$MESS["STAT_TOTAL_CSV"] = "CSV中的總行：";
$MESS["STAT_UNIQUE"] = "僅加載獨特的事件";
$MESS["STAT_UNIQUE_ALT"] = "通過所有加載字段的組合檢查了獨特性（金額除外）";
