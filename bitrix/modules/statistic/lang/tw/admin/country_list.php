<?php
$MESS["STAT_DYNAMIC_GRAPH"] = "在給定的時間";
$MESS["STAT_EVENTS"] = "事件";
$MESS["STAT_F_COUNTRY_ID"] = "國家";
$MESS["STAT_F_DATA_TYPE"] = "數據類型：";
$MESS["STAT_F_PERIOD"] = "時期";
$MESS["STAT_HITS"] = "命中";
$MESS["STAT_NEW_GUESTS"] = "新訪客";
$MESS["STAT_NO_DATA"] = "沒有足夠的數據來構建圖表";
$MESS["STAT_RECORDS_LIST"] = "基於國家的交通";
$MESS["STAT_SESSIONS"] = "會議";
$MESS["STAT_STATIC_GRAPH"] = "一直保持統計數據";
