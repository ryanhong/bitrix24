<?php
$MESS["STAT_DATE"] = "日期";
$MESS["STAT_F_PERIOD"] = "時期";
$MESS["STAT_F_SEARCHER_ID"] = "搜尋引擎:";
$MESS["STAT_GRAPH"] = "搜索引擎：站點索引圖";
$MESS["STAT_GRAPH_TITLE"] = "搜索引擎的站點索引圖";
$MESS["STAT_HITS"] = "命中";
$MESS["STAT_HITS_LIST_OPEN"] = "查看搜索引擎命中";
$MESS["STAT_RECORDS_LIST"] = "搜索引擎：站點索引動態";
$MESS["STAT_SEARCHER_PAGES"] = "記錄";
$MESS["STAT_TOTAL_HITS"] = "總命中：";
