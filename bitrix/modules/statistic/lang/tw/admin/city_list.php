<?php
$MESS["STAT_DYNAMIC_GRAPH2"] = "一段時間";
$MESS["STAT_EVENTS"] = "事件";
$MESS["STAT_F_COUNTRY_ID"] = "國家";
$MESS["STAT_F_PERIOD"] = "時期";
$MESS["STAT_HITS"] = "命中";
$MESS["STAT_NEW_GUESTS"] = "新訪客";
$MESS["STAT_NO_COUNTRY_ID"] = "必須指定國家過濾器。";
$MESS["STAT_NO_DATA"] = "沒有足夠的數據來構建圖表";
$MESS["STAT_RECORDS_LIST"] = "基於城市的交通動態";
$MESS["STAT_SESSIONS"] = "會議";
$MESS["STAT_STATIC_GRAPH"] = "一直保持統計數據";
