<?php
$MESS["STAT_ADDED_BROWSERS"] = "添加了瀏覽器面具：";
$MESS["STAT_ADDED_SEARCHERS"] = "添加了搜索引擎面具：";
$MESS["STAT_ADD_AS_BROWSER"] = "添加為瀏覽器";
$MESS["STAT_ADD_AS_SEARCHER"] = "添加為搜索引擎";
$MESS["STAT_BROWSER"] = "瀏覽器";
$MESS["STAT_FL_DAY"] = "查詢時間範圍";
$MESS["STAT_FL_SESS"] = "會議數量";
$MESS["STAT_F_COUNTER"] = "會議：";
$MESS["STAT_F_LAST_DAY"] = "查詢時間範圍";
$MESS["STAT_F_USER_AGENT"] = "用戶代理：";
$MESS["STAT_MASK"] = "蒙版添加[％_]";
$MESS["STAT_RECORDS_LIST"] = "未知的Useragent Autodect";
$MESS["STAT_SEARCHER"] = "搜尋引擎";
$MESS["STAT_SESSIONS"] = "會議";
$MESS["STAT_SESS_LIST"] = "會議列表";
$MESS["STAT_USER_AGENT"] = "用戶代理";
$MESS["STAT_USER_AGENT_PAGES"] = "用戶";
