<?php
$MESS["STAT_COUNT"] = "數量";
$MESS["STAT_EVENT_DYNAMIC"] = "事件類型動力學";
$MESS["STAT_F_EVENTS"] = "事件類型：";
$MESS["STAT_F_PERIOD"] = "時期";
$MESS["STAT_MONEY"] = "錢";
$MESS["STAT_MULTI_GRAPH"] = "單獨的圖";
$MESS["STAT_RECORDS_LIST"] = "事件類型圖";
$MESS["STAT_SHOW_COUNT"] = "顯示數量";
$MESS["STAT_SHOW_MONEY"] = "顯示金額";
$MESS["STAT_SUMMARIZED"] = "選定事件類型的摘要圖";
$MESS["STAT_SUMMARIZED_GRAPH"] = "摘要圖";
