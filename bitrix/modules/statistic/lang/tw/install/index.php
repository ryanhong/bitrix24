<?php
$MESS["STAT_ADMIN"] = "完整的管理訪問";
$MESS["STAT_CREATE_I2C_DB"] = "iP到國家搜索的翻拍索引？";
$MESS["STAT_DENIED"] = "拒絕訪問";
$MESS["STAT_INSTALL_DATABASE"] = "將模塊安裝到數據庫：";
$MESS["STAT_INSTALL_TITLE"] = "統計模塊安裝";
$MESS["STAT_MAIN_DATABASE"] = "主要的";
$MESS["STAT_MODULE_DESCRIPTION"] = "該模塊收集並顯示站點統計信息。";
$MESS["STAT_MODULE_NAME"] = "網絡分析";
$MESS["STAT_START_GUESTS"] = "初始訪問者的價值：";
$MESS["STAT_START_HITS"] = "初始點擊值：";
$MESS["STAT_START_HOSTS"] = "初始主機值：";
$MESS["STAT_UNINSTALL_TITLE"] = "統計模塊卸載";
$MESS["STAT_VIEW"] = "查看所有統計數據";
$MESS["STAT_VIEW_WITHOUT_MONEY"] = "查看沒有任何財務參數的統計數據";
