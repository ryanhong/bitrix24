<?php
$MESS["STATISTIC_ACTIVITY_EXCEEDING_DESC"] = "＃activity_time_limit＃ - 測試時間（秒）
＃activity_hits＃ - 測試期間的命中次數
＃activity_hits_limit＃ - 測試期間的最大命中率
＃activity_exceeding＃ - 活動超出
＃current_time＃ - 阻止時間（服務器時間）
＃delay_time＃ - 阻止期限
＃USER_AGENT＃ -USERAGENT
＃session_id＃ - 會話ID
＃session_link＃ - 鏈接到會話報告
＃seracher_id＃ - 搜索引擎ID
＃searcher_name＃ - 搜索引擎名稱
＃searcher_link＃ - 鏈接到搜索引擎命中列表
＃visitor_id＃ - 訪客ID
＃visitor_link＃ - 鏈接到訪問者資料
＃stoplist_link＃ - 將訪問者添加到停止列表的鏈接";
$MESS["STATISTIC_ACTIVITY_EXCEEDING_MESSAGE"] = "訪問者在網站＃server_name＃上超過了活動限制。

從＃current_time＃開始，訪問者被阻止以＃delay_time＃sec。

活動 - ＃activity_hits＃命中per＃activity_time_limit＃sec。 （限制 - ＃activity_hits_limit＃）
訪客 - ＃visitor_id＃＃
會話 - ＃session_id＃＃
搜索器 -  [＃seracher_id＃]＃searcher_name＃
用戶 - ＃user_agent＃

>================================================= =========== ===============
使用以下鏈接添加到停止列表：
http：//＃server_name ## stoplist_link＃
使用以下鏈接查看會話：
http：//＃server_name ## session_link＃
使用以下鏈接查看訪問者資料：
http：//＃server_name ## visitor_link＃
使用以下鏈接查看搜索器命中：
http：//＃server_name ## searcher_link＃";
$MESS["STATISTIC_ACTIVITY_EXCEEDING_NAME"] = "訪客活動限制超過";
$MESS["STATISTIC_ACTIVITY_EXCEEDING_SUBJECT"] = "＃server_name＃：超出活動限制";
$MESS["STATISTIC_DAILY_REPORT_DESC"] = "＃email_to＃ - 網站管理員電子郵件
＃server_time＃ - 報告發送期間的服務器時間發送
＃html_header＃-html標題
＃HTML_COMMON＃ - 網站流量表（命中，會話，主機，訪客，活動）（HTML）
＃HTML_ADV＃ - 廣告表活動表（前10）（HTML）
＃HTML_EVENTS＃ - 事件表鍵入（前10）（HTML）
＃HTML_REFERER＃ - 參考站點表（前10）（HTML）
＃HTML_PHRASE＃ - 搜索短語表（前10）（HTML）
＃html_searchers＃ - 網站索引表（前10）（HTML）
＃HTML_FOOTER＃ -HTML頁腳
";
$MESS["STATISTIC_DAILY_REPORT_MESSAGE"] = "＃html_header＃
<font class ='h2'> <font color ='＃a52929'>＃site_name＃</font> site <br>
<font color ='＃0d716f'>＃server_time＃</font> </font> </font>上的數據
<br> <br>
<a class=''class ='' =＃語言＃</a>
<br>
<hr> <br>
＃HTML_COMMON＃
<br>
＃HTML_ADV＃
<br>
＃html_referer＃
<br>
＃HTML_PHRASE＃
<br>
＃html_searchers＃
<br>
＃html_events＃
<br>
<hr>
<a class=''class ='' =＃語言＃</a>
＃html_footer＃
";
$MESS["STATISTIC_DAILY_REPORT_NAME"] = "現場統計每日報告";
$MESS["STATISTIC_DAILY_REPORT_SUBJECT"] = "＃server_name＃：站點的統計信息（＃server_time＃）";
