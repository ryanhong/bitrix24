<?php
$MESS["STAT_OPT_ADV_AUTO_CREATE"] = "自動創建廣告系列使用Reforer1和referer2如果不存在";
$MESS["STAT_OPT_ADV_DETAIL_TOP_SIZE"] = "廣告活動報告中的最大項目數量：";
$MESS["STAT_OPT_ADV_EVENTS_DEFAULT"] = "顯示廣告活動事件為：";
$MESS["STAT_OPT_ADV_EVENTS_GROUP_BY_EVENT1"] = "by Event1組";
$MESS["STAT_OPT_ADV_EVENTS_GROUP_BY_EVENT2"] = "小組按Event2";
$MESS["STAT_OPT_ADV_EVENTS_SHOW_LINK"] = "事件摘要";
$MESS["STAT_OPT_ADV_EVENTS_SHOW_LIST"] = "事件類型詳細信息";
$MESS["STAT_OPT_ADV_OPENSTAT_ACTIVE"] = "跟踪<a href= \"http://openstat.ru/ \ \"> openstat </a>鏈接";
$MESS["STAT_OPT_ADV_OPENSTAT_R1_TEMPLATE"] = "<i> referer1 < /i> <br />的模板（可能的宏：＃service-name＃，＃競爭-ID＃，＃ad-id＃，＃source-id＃）：";
$MESS["STAT_OPT_ADV_OPENSTAT_R2_TEMPLATE"] = "<i> referer2 < /i> <br />的模板（可能的宏：＃service-name＃，＃競爭-ID＃，＃ad-id＃，＃source-id＃）：";
$MESS["STAT_OPT_ADV_OPENSTAT_SECTION"] = "OpenStat支持";
$MESS["STAT_OPT_ADV_REFERER1_SYN"] = "\“ Referer1 \”的別名：";
$MESS["STAT_OPT_ADV_REFERER2_SYN"] = "\“ Referer2 \”的別名：";
$MESS["STAT_OPT_ADV_REFERER3_SYN"] = "\“ Referer3 \”的別名：";
$MESS["STAT_OPT_ADV_USE_DEFAULT_ADV"] = "跟踪未識別的訪問者為<i> referer1 = na，referer2 = na </i>";
$MESS["STAT_OPT_BASE_CURRENCY"] = "財務信息的默認貨幣：";
$MESS["STAT_OPT_CITY_AVAILABLE"] = "可用的";
$MESS["STAT_OPT_CITY_CITY_LOOKUP"] = "城市查找";
$MESS["STAT_OPT_CITY_COUNTRY_LOOKUP"] = "國家查找";
$MESS["STAT_OPT_CITY_HEADER"] = "當前支持的IP查找數據源：";
$MESS["STAT_OPT_CITY_IS_IN_USE"] = "正在使用";
$MESS["STAT_OPT_CITY_SOURCE"] = "數據源";
$MESS["STAT_OPT_CLEANUP_BUTTON"] = "刪除數據";
$MESS["STAT_OPT_CLEANUP_CONFIRMATION"] = "您確定要刪除統計信息嗎？";
$MESS["STAT_OPT_CLEANUP_DATE2"] = "刪除所有數據槽（留空以刪除所有數據）：";
$MESS["STAT_OPT_CLEAN_UP_ERRORS"] = "發生錯誤的錯誤刪除了統計信息：";
$MESS["STAT_OPT_CLEAN_UP_OK"] = "統計數據成功刪除了。";
$MESS["STAT_OPT_DAILY_REPORT_TIME2"] = "即將到來的統計報告將發送：";
$MESS["STAT_OPT_DEFENCE_DELAY"] = "為了";
$MESS["STAT_OPT_DEFENCE_DELAY_MEASURE_SEC"] = "秒";
$MESS["STAT_OPT_DEFENCE_LOG"] = "將記錄添加到<a href= \"#href# \ \">事件日誌</a>：";
$MESS["STAT_OPT_DEFENCE_MAX_STACK_HITS"] = "客戶的生成不僅僅是";
$MESS["STAT_OPT_DEFENCE_MAX_STACK_HITS_MEASURE"] = "命中";
$MESS["STAT_OPT_DEFENCE_ON"] = "禁止過度活躍的客戶";
$MESS["STAT_OPT_DEFENCE_STACK_TIME"] = "如果期間";
$MESS["STAT_OPT_DEFENCE_STACK_TIME_MEASURE_SEC"] = "秒";
$MESS["STAT_OPT_DIAGRAM_DIAMETER"] = "餅圖直徑：";
$MESS["STAT_OPT_DO_RECOUNT"] = "立即重新計算所有收集的財務數據";
$MESS["STAT_OPT_ERR_NO_GROUPS"] = "沒有選擇用戶組排除。";
$MESS["STAT_OPT_ERR_NO_RANGES"] = "未指定用於排除的IP地址範圍。";
$MESS["STAT_OPT_GRABBER_DEFENCE_SECTION"] = "活動限制";
$MESS["STAT_OPT_GRABBER_EDIT_503_TEMPLATE_LINK"] = "編輯模板";
$MESS["STAT_OPT_GRAPH_HEIGHT"] = "報告圖表高度：";
$MESS["STAT_OPT_GRAPH_WEIGHT"] = "報告圖表寬度：";
$MESS["STAT_OPT_INDEXED"] = "索引已成功創建。";
$MESS["STAT_OPT_INDEX_ATTENTION"] = "注意力";
$MESS["STAT_OPT_INDEX_ATTENTION_DETAIL"] = "此操作可能需要很長時間才能完成。";
$MESS["STAT_OPT_INDEX_CREATE_BUTTON"] = "創造";
$MESS["STAT_OPT_ONLINE_INTERVAL"] = "更新每個（秒）的在線訪問者：";
$MESS["STAT_OPT_OPTIMIZED"] = "優化成功完成";
$MESS["STAT_OPT_OPTIMIZE_BUTTON"] = "現在優化";
$MESS["STAT_OPT_OPTIMIZE_CONFIRMATION"] = "您確定現在要優化統計模塊表嗎？";
$MESS["STAT_OPT_OPTIMIZE_ERRORS"] = "優化期間發生以下錯誤：";
$MESS["STAT_OPT_RECORDS_LIMIT"] = "表格顯示的最大記錄數：";
$MESS["STAT_OPT_REFERER_CHECK2"] = "僅當引用器<i> n </i>包含拉丁字符，數字和<b> \“ _：;。， -  \” </b>時，創建廣告系列";
$MESS["STAT_OPT_SAVE_SESSION_DATA"] = "使用替代算法跟踪<br>拒絕餅乾的訪客";
$MESS["STAT_OPT_SEARCHER_EVENTS"] = "跟踪搜索引擎廣告活動活動";
$MESS["STAT_OPT_SKIP_BOTH"] = "從指定的IP地址中排除選定組的用戶";
$MESS["STAT_OPT_SKIP_GROUPS"] = "排除選定組的用戶";
$MESS["STAT_OPT_SKIP_GROUPS_LABEL"] = "排除用戶組";
$MESS["STAT_OPT_SKIP_NONE"] = "沒有例外;跟踪所有訪客";
$MESS["STAT_OPT_SKIP_RANGES"] = "從指定的IP地址排除訪問";
$MESS["STAT_OPT_SKIP_RANGES_LABEL"] = "排除IP範圍的訪問<br>（每行一個範圍）";
$MESS["STAT_OPT_SKIP_RULES"] = "應用例外";
$MESS["STAT_OPT_SKIP_SAMPLE"] = "例子";
$MESS["STAT_OPT_STAT_LIST_TOP_SIZE"] = "統計數據中最大項目數：";
$MESS["STAT_OPT_STORAGE2_EVENT_GID_SITE_ID"] = "簡短的網站ID：<br>（用作<a href= \"#href# \">手動條目的額外參數</a>；此處沒有期間或空格）";
$MESS["STAT_OPT_STORAGE2_USER_EVENTS_LOAD_HANDLERS_PATH"] = "通往CSV文件處理器的路徑：";
$MESS["STAT_OPT_STORAGE_BROWSERS"] = "排除用戶：<br>（\“％\”  - 零或更多字符; \ \“ _ \”  - 任何一個字符）";
$MESS["STAT_OPT_STORAGE_DIRECTORY_INDEX"] = "部分索引頁的可能名稱：";
$MESS["STAT_OPT_STORAGE_EVENTS_SECTION"] = "事件";
$MESS["STAT_OPT_STORAGE_EVENT_GID_BASE64_ENCODE"] = "使用base64編碼＃event_gid＃參數";
$MESS["STAT_OPT_STORAGE_HIT_SECTION"] = "命中";
$MESS["STAT_OPT_STORAGE_IMPORTANT_PAGE_PARAMS"] = "重要的URL參數：";
$MESS["STAT_OPT_STORAGE_MAX_PATH_STEPS"] = "路徑中的最大頁面數：";
$MESS["STAT_OPT_STORAGE_REFERER_SECTION"] = "引用站點和搜索短語";
$MESS["STAT_OPT_STORAGE_REFERER_TOP"] = "不要清除大多數活躍的參考域：";
$MESS["STAT_OPT_STORAGE_SAVE_HITS"] = "保存命中";
$MESS["STAT_OPT_STORAGE_SAVE_PATH_DATA"] = "收集網站路徑報告所需的數據";
$MESS["STAT_OPT_STORAGE_SAVE_REFERERS"] = "跟踪參考站點和搜索關鍵字";
$MESS["STAT_OPT_STORAGE_SAVE_VISITS"] = "跟踪部分和頁面命中";
$MESS["STAT_OPT_STORAGE_SEARCHER_SECTION"] = "搜尋引擎";
$MESS["STAT_OPT_STORAGE_TRAFFIC_SECTION"] = "頁面流量";
$MESS["STAT_OPT_SYSTEM_PROC"] = "服務工具";
$MESS["STAT_OPT_TAB_ADV"] = "廣告活動";
$MESS["STAT_OPT_TAB_ADV_TITLE"] = "廣告活動處理和跟踪";
$MESS["STAT_OPT_TAB_CITY"] = "城市和國家";
$MESS["STAT_OPT_TAB_CITY_TITLE"] = "國家和城市IP查找參數";
$MESS["STAT_OPT_TAB_CLEANUP"] = "明確的統計數據";
$MESS["STAT_OPT_TAB_CLEANUP_TITLE"] = "清除統計";
$MESS["STAT_OPT_TAB_INDEX"] = "新索引";
$MESS["STAT_OPT_TAB_INDEX_TITLE"] = "Web分析模塊的最新更新需要創建新索引。";
$MESS["STAT_OPT_TAB_OPTIMIZE"] = "服務工具";
$MESS["STAT_OPT_TAB_OPTIMIZE_TITLE"] = "優化統計表";
$MESS["STAT_OPT_TAB_SKIP"] = "例外";
$MESS["STAT_OPT_TAB_SKIP_TITLE"] = "將訪客排除在統計上";
$MESS["STAT_OPT_TAB_STORAGE"] = "存儲的數據";
$MESS["STAT_OPT_TAB_STORAGE_TITLE"] = "可跟踪的事件和信息";
$MESS["STAT_OPT_TAB_TIME"] = "保留期";
$MESS["STAT_OPT_TAB_TIME_TITLE"] = "信息存儲時間";
$MESS["STAT_OPT_TIME_ADV_DAYS"] = "保持廣告系列動態的天數：";
$MESS["STAT_OPT_TIME_ADV_GUEST_DAYS"] = "保持廣告活動訪問的天數：";
$MESS["STAT_OPT_TIME_ADV_SECTION"] = "廣告活動";
$MESS["STAT_OPT_TIME_CITY_DAYS"] = "保留城市數據的天數：";
$MESS["STAT_OPT_TIME_CLEAR"] = "清除";
$MESS["STAT_OPT_TIME_COUNTRY_DAYS"] = "保留國家數據的天數：";
$MESS["STAT_OPT_TIME_EVENTS_DAYS"] = "保留事件的天數：";
$MESS["STAT_OPT_TIME_EVENTS_DYNAMIC_DAYS"] = "保持事件類型動態的天數：";
$MESS["STAT_OPT_TIME_EVENTS_SECTION"] = "事件";
$MESS["STAT_OPT_TIME_GEO_SECTION"] = "城市和國家";
$MESS["STAT_OPT_TIME_GUEST_DAYS"] = "保持遊客的天數：";
$MESS["STAT_OPT_TIME_GUEST_SECTION"] = "訪客";
$MESS["STAT_OPT_TIME_HIT_DAYS"] = "保持點擊的天數：";
$MESS["STAT_OPT_TIME_HIT_TITLE"] = "命中";
$MESS["STAT_OPT_TIME_PATH_DAYS"] = "保持站點路徑的天數：";
$MESS["STAT_OPT_TIME_PHRASES_DAYS"] = "保持搜索關鍵字命中的天數：";
$MESS["STAT_OPT_TIME_RECORDS"] = "記錄：";
$MESS["STAT_OPT_TIME_REFERER_DAYS"] = "保持參考域的天數：";
$MESS["STAT_OPT_TIME_REFERER_LIST_DAYS"] = "繼續參考頁面命中的天數：";
$MESS["STAT_OPT_TIME_REFERER_SECTION"] = "引用站點和搜索短語";
$MESS["STAT_OPT_TIME_SEARCHER_DAYS"] = "保持搜索引擎動態的天數：";
$MESS["STAT_OPT_TIME_SEARCHER_HIT_DAYS"] = "保持搜索引擎命中的天數：";
$MESS["STAT_OPT_TIME_SEARCHER_SECTION"] = "搜尋引擎";
$MESS["STAT_OPT_TIME_SESSION_DAYS"] = "保留會議的天數：";
$MESS["STAT_OPT_TIME_SESSION_SECTION"] = "會議";
$MESS["STAT_OPT_TIME_TRAFFIC_SECTION"] = "頁面流量";
$MESS["STAT_OPT_TIME_VISIT_DAYS"] = "保留節目和歸檔命中的天數：";
$MESS["STAT_OPT_USE_AUTO_OPTIMIZE"] = "在清除上優化數據庫表";
$MESS["STAT_OPT_WRONG_CLEANUP_DATE"] = "要刪除統計信息，請以正確格式提供日期";
$MESS["STAT_OPT_WRONG_NEXT_EXEC"] = "請以適當格式指定發送統計報告的時間";
