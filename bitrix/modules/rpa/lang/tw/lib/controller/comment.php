<?php
$MESS["RPA_ADD_COMMENT_ACCESS_DENIED"] = "您無權向此項目添加評論。";
$MESS["RPA_COMMENT_MENTION_NOTIFY"] = "在\ \ \“＃item＃\”的評論中提到了您，在工作流＃類型＃中，註釋文本：\“＃comment＃\”";
$MESS["RPA_COMMENT_MENTION_NOTIFY_F"] = "在\ \ \“＃item＃\”的評論中提到了您，在工作流＃類型＃中，註釋文本：\“＃comment＃\”";
$MESS["RPA_COMMENT_MENTION_NOTIFY_M"] = "在\ \ \“＃item＃\”的評論中提到了您，在工作流＃類型＃中，註釋文本：\“＃comment＃\”";
$MESS["RPA_COMMENT_NOT_FOUND_ERROR"] = "沒有找到評論。";
$MESS["RPA_DELETE_COMMENT_ACCESS_DENIED"] = "您無權刪除此評論。";
$MESS["RPA_MODIFY_COMMENT_ACCESS_DENIED"] = "您無權編輯此評論。";
