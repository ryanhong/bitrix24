<?php
$MESS["RPA_BP_DOCUMENT_ITEM_CREATED_BY"] = "由...製作";
$MESS["RPA_BP_DOCUMENT_ITEM_CREATED_TIME"] = "創建於";
$MESS["RPA_BP_DOCUMENT_ITEM_MOVED_BY"] = "移動";
$MESS["RPA_BP_DOCUMENT_ITEM_MOVED_TIME"] = "繼續前進";
$MESS["RPA_BP_DOCUMENT_ITEM_NAME"] = "姓名";
$MESS["RPA_BP_DOCUMENT_ITEM_PREVIOUS_STAGE_ID"] = "上一個階段";
$MESS["RPA_BP_DOCUMENT_ITEM_STAGE_ID"] = "階段";
$MESS["RPA_BP_DOCUMENT_ITEM_UPDATED_BY"] = "修改";
$MESS["RPA_BP_DOCUMENT_ITEM_UPDATED_TIME"] = "最後更新";
$MESS["RPA_BP_DOCUMENT_ITEM_USER_GROUP_HEAD"] = "工作流創建者的主管";
$MESS["RPA_BP_DOCUMENT_ITEM_XML_ID"] = "外部ID";
$MESS["RPA_BP_ITEM_ENTITY_NAME"] = "工作流程";
