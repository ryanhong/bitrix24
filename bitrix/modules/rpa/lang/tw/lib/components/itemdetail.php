<?php
$MESS["RPA_FIRST_STAGE_NOT_FOUND_ERROR"] = "找不到初始階段";
$MESS["RPA_FIRST_STAGE_PERMISSION_DENIED"] = "您無權將項目添加到初始階段。";
$MESS["RPA_ITEM_CREATE_TITLE"] = "創建＃類型＃項目";
$MESS["RPA_ITEM_EDITOR_MAIN_SECTION_TITLE"] = "項目字段";
$MESS["RPA_ITEM_EDIT_TITLE"] = "項目＃類型＃＃ID＃";
$MESS["RPA_ITEM_NOT_FOUND"] = "找不到項目。";
$MESS["RPA_STAGE_MOVE_PERMISSION_DENIED"] = "無法將項目從階段＃stage_from＃移至＃stage_to＃";
$MESS["RPA_STAGE_NOT_FOUND_ERROR"] = "找不到階段";
$MESS["RPA_STAGE_PERMISSION_DENIED"] = "您無權在此階段編輯項目。";
$MESS["RPA_TYPE_PERMISSION_ERROR"] = "您無權查看此工作流中的項目。";
