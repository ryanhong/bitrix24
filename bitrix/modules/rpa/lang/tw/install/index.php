<?php
$MESS["RPA_INSTALL_TITLE"] = "機器人過程自動化（RPA）模塊安裝";
$MESS["RPA_MODULE_DESCRIPTION"] = "該模塊實現機器人過程自動化（RPA）模塊";
$MESS["RPA_MODULE_NAME"] = "機器人過程自動化（RPA）";
$MESS["RPA_UNINSTALL_QUESTION"] = "您確定要刪除模塊嗎？";
$MESS["RPA_UNINSTALL_TITLE"] = "機器人過程自動化（RPA）模塊卸載";
