<?php
$MESS["RPA_BP_APR_ACT_NO_ACTION"] = "沒有指定行動。";
$MESS["RPA_BP_APR_ACT_PROP_EMPTY1"] = "沒有選擇任務的負責人。";
$MESS["RPA_BP_APR_ACT_PROP_EMPTY2"] = "未指定批准類型。";
$MESS["RPA_BP_APR_ACT_PROP_EMPTY3"] = "錯誤類型的批准類型";
$MESS["RPA_BP_APR_ACT_PROP_EMPTY4"] = "分配名稱未指定。";
$MESS["RPA_BP_APR_ERROR_STAGE_ID"] = "目標階段不正確";
$MESS["RPA_BP_APR_FIELD_ACTIONS"] = "鈕扣";
$MESS["RPA_BP_APR_FIELD_ALTER_RESPONSIBLE"] = "備份用戶";
$MESS["RPA_BP_APR_FIELD_APPROVE_ACTION_NO"] = "衰退";
$MESS["RPA_BP_APR_FIELD_APPROVE_ACTION_YES"] = "批准";
$MESS["RPA_BP_APR_FIELD_APPROVE_FIXED_COUNT"] = "批准的人";
$MESS["RPA_BP_APR_FIELD_APPROVE_TYPE"] = "批准類型";
$MESS["RPA_BP_APR_FIELD_APPROVE_TYPE_ALL"] = "全部";
$MESS["RPA_BP_APR_FIELD_APPROVE_TYPE_ANY"] = "任何列出的";
$MESS["RPA_BP_APR_FIELD_APPROVE_TYPE_FIXED"] = "任何";
$MESS["RPA_BP_APR_FIELD_APPROVE_TYPE_QUEUE"] = "全部在隊列中";
$MESS["RPA_BP_APR_FIELD_DESCRIPTION"] = "文字";
$MESS["RPA_BP_APR_FIELD_EXECUTIVE_RESPONSIBLE"] = "最終批准者";
$MESS["RPA_BP_APR_FIELD_FIELDS_TO_SHOW"] = "展示場";
$MESS["RPA_BP_APR_FIELD_NAME"] = "姓名";
$MESS["RPA_BP_APR_FIELD_RESPONSIBLE_TYPE"] = "負責類型";
$MESS["RPA_BP_APR_FIELD_RESPONSIBLE_TYPE_HEADS"] = "員工主管";
$MESS["RPA_BP_APR_FIELD_RESPONSIBLE_TYPE_PLAIN"] = "用戶隊列";
$MESS["RPA_BP_APR_FIELD_SKIP_ABSENT"] = "跳過缺席的用戶";
$MESS["RPA_BP_APR_FIELD_USERS"] = "負責人";
$MESS["RPA_BP_APR_RUNTIME_TERMINATED"] = "階段改變了";
