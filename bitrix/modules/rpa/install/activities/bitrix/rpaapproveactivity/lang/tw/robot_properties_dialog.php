<?php
$MESS["RPA_BP_APR_SPD_BTN_ACTION"] = "移至舞台";
$MESS["RPA_BP_APR_SPD_BTN_ADD_BTN"] = "添加按鈕";
$MESS["RPA_BP_APR_SPD_BTN_COLOR"] = "顏色";
$MESS["RPA_BP_APR_SPD_BTN_NAME"] = "姓名";
$MESS["RPA_BP_APR_SPD_HELP_ABSENCE"] = "如果當前階段的所有用戶都不可用，則備份用戶將參加。";
$MESS["RPA_BP_APR_SPD_HELP_EXECUTIVE"] = "選擇將在批准中擁有最終決定權的用戶。如果在批准主管鏈中至少存在一個\“最終批准者”，則批准任務在該人做出決定後停止。";
$MESS["RPA_BP_APR_SPD_HELP_HEADS"] = "批准過程將遵循員工的主管鏈，直到鏈的頂部或指定為\“最終批准者\”的人員。";
$MESS["RPA_BP_APR_SPD_RESPONSIBLE"] = "負責人";
$MESS["RPA_BP_APR_SPD_TASK"] = "任務";
