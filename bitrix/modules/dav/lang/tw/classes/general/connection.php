<?php
$MESS["DAV_EXP_ACCOUNT_TYPE"] = "連接類型";
$MESS["DAV_EXP_ACCOUNT_TYPE_OOR"] = "以下連接類型值是可能的：caldav，ical，caldav_google_oauth。";
$MESS["DAV_EXP_ENTITY_ID"] = "實體ID";
$MESS["DAV_EXP_ENTITY_ID_TYPE"] = "實體ID必須是整數。";
$MESS["DAV_EXP_ENTITY_TYPE"] = "實體類型";
$MESS["DAV_EXP_ENTITY_TYPE_OOR"] = "以下實體類型值是可能的：用戶，組。";
$MESS["DAV_EXP_NAME"] = "姓名";
$MESS["DAV_EXP_SERVER_HOST"] = "地址";
$MESS["DAV_EXP_SERVER_PORT"] = "港口";
$MESS["DAV_EXP_SERVER_SCHEME"] = "模式";
$MESS["DAV_EXP_SERVER_SCHEME_OOR"] = "以下連接模式值是可能的：HTTP，HTTP。";
