<?php
$MESS["DAV_HELP_NAME"] = "DAV模塊";
$MESS["DAV_HELP_TEXT"] = "DAV模塊使網站上的日曆和聯繫人與任何
支持CardDav和CardDav協議的軟件和設備。 iPhone和iPad支持
這些協議。軟件支持由Mozilla Sunbird，EM客戶端和其他一些軟件應用程序提供。<br>
<ul>
<li> <b> <a href= \"#carddav \">使用carddav </a> </b> </li>連接
<li> <b> <a href= \“#caldav \">使用Caldav </a> </b>連接
<ul>
<li> <a href= \"#caldavipad \"> connect </a> <a href= \“#caldavipad \"> iPhone/ipad </a> </ipad </a> </li>
<li> <a href= \“#carddavsunbird \">連接mozilla sunbird </a> </li>
</ul>
</li>
</ul>

<br>

<h3> <a name= \"carddav \"> </a>使用carddav </h3>連接

設置您的Apple設備以支持CardDav：
<ol>
<li>單擊<b>設置</b>，然後選擇<b>密碼和帳戶</b>。</li>
<li>單擊<b>添加帳戶</b>。</li>
<li>選擇<b>其他</b>＆gt; <b>添加CardDav帳戶</b>。</li>
<li>將此網站地址指定為服務器（＃服務器＃）。使用您的登錄和密碼。</li>
<li>如果您使用的是兩步身份驗證，請使用在此處指定的密碼
  用戶資料：<b>應用程序密碼</b>  -  <b>聯繫人</b>。</li>
<li>要指定端口號，保存帳戶，然後將其打開以進行編輯
  並繼續進行<b>更多</b>區域。</li>
</ol>

您所有的聯繫人都將出現在Contacts應用程序中。<br>
要啟用或禁用選定聯繫人的同步，請打開您的個人資料
並在菜單中選擇<b>同步</b>。

<br> <br>

<h3> <a name= \"caldav \"> </a>使用Caldav </h3>連接

<H4> <a name= \"caldavipad \"> </a>連接iPhone/ipad </h4>

設置您的Apple設備以支持Caldav：
<ol>
<li>在您的Apple設備上，打開菜單<b>設置</b>＆gt; <b>密碼和帳戶</b>。</li>
<li>選擇<b>在帳戶列表下添加帳戶</b>。</li>
<li>選擇Caldav作為帳戶類型（<b>其他</b>＆gt; <b> caldav帳戶</b>）。</li>
<li>將此網站地址指定為服務器（＃服務器＃）。使用您的登錄和密碼。</li>
<li>如果您使用的是兩步身份驗證，請使用用戶配置文件中指定的密碼：<b>應用程序密碼</b>
  ＆gt; <b>日曆</b>。</li>
<li>要指定端口號，保存帳戶，然後將其打開以再次進行編輯，然後進行<b> More </b>區域。</li>
</ol>

您的日曆將出現在日曆應用中。＆nbsp; <br>
要連接其他用戶的日曆，請在<b> More </b>中使用這些鏈接； <b>帳戶
url </b>：<br>
<i>＃服務器＃/bitrix/groupdav.php/site ID/用戶名/日曆/</i> <br>
和<br>
<i>＃服務器＃/bitrix/groupdav.php/</i> <i>站點ID </i> <i>/group-group ID/calendar/>

<br> <br>

<h4> <a name= \"carddavsunbird \ \"> </a>連接mozilla sunbird </h4>

配置Mozilla Sunbird與Caldav一起使用：
<ol>
<li>運行Sunbird並選擇<b> file＆gt;新日曆</b>。</li>
<li>在網絡上選擇<b> </b>，然後單擊<b>下一個</b>。</li>
<li>選擇<b> caldav </b>格式。</li>
<li>在<b>位置</b>字段中，輸入：<br>
  <i>＃服務器＃/bitrix/groupdav.php/site ID/用戶名/日曆/日曆ID/</i> <br>
  或<br>
  <i>＃服務器＃/bitrix/groupdav.php/i> <i>站點ID </i> <i>/group- </i> <i>組/日曆
  id/</i> <br>
然後單擊<b>下一個</b>。</li>
<li>給您的日曆一個名字，然後為其選擇顏色。</li>
<li>輸入您的用戶名和密碼。</li>
<li>如果您使用的是兩步身份驗證，請使用用戶配置文件中指定的密碼：<b>應用程序密碼</b>
  ＆gt; <b>日曆</b>。</li>
</ol>

您的日曆現已在Mozilla Sunbird中找到。";
