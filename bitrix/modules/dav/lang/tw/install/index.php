<?php
$MESS["DAV_ERROR_EDITABLE"] = "您的版本不提供此模塊。";
$MESS["DAV_INSTALL_DESCRIPTION"] = "對象和收集訪問模塊";
$MESS["DAV_INSTALL_NAME"] = "dav";
$MESS["DAV_INSTALL_TITLE"] = "模塊安裝";
$MESS["DAV_PERM_D"] = "拒絕訪問";
$MESS["DAV_PERM_R"] = "讀";
$MESS["DAV_PERM_W"] = "寫";
