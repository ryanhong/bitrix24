<?php
$MESS["DAV_APP_COMMENT"] = "自動創建";
$MESS["DAV_APP_SYSCOMMENT"] = "DAV同步";
$MESS["DAV_APP_SYSCOMMENT_TYPE"] = "DAV同步：＃類型＃";
$MESS["DAV_APP_TYPE_caldav"] = "日曆";
$MESS["DAV_APP_TYPE_carddav"] = "聯繫人";
$MESS["dav_app_calendar"] = "日曆";
$MESS["dav_app_calendar_desc"] = "同步日曆的事件以查看計劃的事件和活動。選擇要配置的同步目標，然後單擊“獲取密碼\”。";
$MESS["dav_app_calendar_phone"] = "智慧型手機";
$MESS["dav_app_card"] = "聯繫人";
$MESS["dav_app_card_desc"] = "將您的聯繫人列表與手機聯繫人或其他應用程序同步。";
$MESS["dav_app_doc"] = "文件";
$MESS["dav_app_doc_desc"] = "MS Office的某些版本需要登錄和密碼來編輯文檔。使用您的登錄；要獲取正確的密碼，請單擊\“獲取密碼\”。";
$MESS["dav_app_doc_office"] = "微軟Office";
