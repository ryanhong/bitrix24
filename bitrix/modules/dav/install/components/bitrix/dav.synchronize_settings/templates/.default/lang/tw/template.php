<?php
$MESS["DAV_ACCOUNTS_EXPORT_DEPARTMENT"] = "部門";
$MESS["DAV_ACCOUNTS_SECTION"] = "系統用戶";
$MESS["DAV_BUTTON_SAVE"] = "節省";
$MESS["DAV_CARDDAV_SETTINGS_HELP"] = "DAV模塊使網站上的聯繫人與任何
支持CardDav協議的軟件和設備。 iPhone和iPad支持
卡達夫。
<br> <br>

<H3>啟用CardDav連接</h3>

按照以下說明在Apple設備上配置CardDav。
<ol>
<li>在您的Apple設備上，打開菜單<b>設置</b>＆gt; <b>密碼和帳戶</b>。</li>
<li>選擇<b>在帳戶列表下添加帳戶</b>。</li>
<li>選擇cardDav作為帳戶類型（<b>其他</b>＆gt; <b> carddav帳戶</b>）。</li>
<li>將此網站地址指定為服務器（＃服務器＃）。使用您的登錄和密碼.. </li>
<li>如果您使用的是兩步身份驗證，請使用在此處指定的密碼
  用戶資料：<b>應用程序密碼</b>  -  <b>聯繫人</b>。</li>
<li>要指定端口號，保存帳戶，然後將其打開以進行編輯
  並繼續進行<b>更多</b>區域。</li>
</ol>

您所有的聯繫人都將出現在Contacts應用程序中。要啟用或禁用選定聯繫人的同步，請使用以下設置。";
$MESS["DAV_COMMON_SECTION"] = "常規設置";
$MESS["DAV_COMPANIES"] = "CRM公司";
$MESS["DAV_CONTACTS"] = "CRM聯繫人";
$MESS["DAV_DEFAULT_COLLECTION_TO_SYNC"] = "默認情況下同步";
$MESS["DAV_ENABLE"] = "使能夠";
$MESS["DAV_EXPORT_FILTER"] = "篩選";
$MESS["DAV_EXTRANET_ACCOUNTS"] = "Extranet用戶";
$MESS["DAV_MAX_COUNT"] = "限制";
$MESS["main_app_pass_comment"] = "評論";
$MESS["main_app_pass_comment_ph"] = "添加描述";
$MESS["main_app_pass_create_pass"] = "創建密碼";
$MESS["main_app_pass_create_pass_close"] = "關閉";
$MESS["main_app_pass_create_pass_text"] = "使用此密碼與所選應用程序同步。
密碼不會以開放的形式保存；因此，只有在獲得創建時才可用。
在復製或輸入密碼之前，不要關閉窗口。";
$MESS["main_app_pass_created"] = "創建於";
$MESS["main_app_pass_del"] = "刪除";
$MESS["main_app_pass_del_pass"] = "您確定要刪除密碼嗎？";
$MESS["main_app_pass_del_pass_text"] = "由於身份驗證錯誤，該應用程序將無法訪問數據，因此同步將被中止。";
$MESS["main_app_pass_get_pass"] = "獲取密碼";
$MESS["main_app_pass_last"] = "最後登錄";
$MESS["main_app_pass_last_ip"] = "最後一個IP";
$MESS["main_app_pass_link"] = "連接";
$MESS["main_app_pass_manage"] = "管理";
$MESS["main_app_pass_other"] = "其他";
$MESS["main_app_pass_text1"] = "要將數據與外部應用程序安全地同步，請使用可以在此頁面上獲得的特殊密碼。";
$MESS["main_app_pass_text2"] = "這些工具中的每一個都支持一系列支持同步的應用程序。選擇要配置數據傳輸的應用程序。";
$MESS["main_app_pass_title"] = "這是什麼？";
