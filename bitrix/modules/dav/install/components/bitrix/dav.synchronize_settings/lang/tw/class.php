<?php
$MESS["DAV_ACCOUNTS"] = "系統用戶";
$MESS["DAV_COMPANIES"] = "CRM公司";
$MESS["DAV_CONTACTS"] = "CRM聯繫人";
$MESS["DAV_EXTRANET_ACCOUNTS"] = "Extranet用戶";
$MESS["DAV_SYNCHRONIZE_TITLE"] = "同步參數";
$MESS["dav_app_synchronize_auth"] = "在使用同步參數之前，必須登錄您。";
