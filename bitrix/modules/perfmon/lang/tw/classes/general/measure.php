<?php
$MESS["PERFMON_MEASURE_CACHE_REC"] = "增加緩存尺寸。";
$MESS["PERFMON_MEASURE_CURRENT_VALUE"] = "當前值：\“＃值＃\”";
$MESS["PERFMON_MEASURE_DISABLED"] = "禁用";
$MESS["PERFMON_MEASURE_EQUAL_OR_GREATER_THAN_REC"] = "設置為\“＃值＃\”或更多。";
$MESS["PERFMON_MEASURE_GREATER_THAN_ZERO_REC"] = "設置為大於零。";
$MESS["PERFMON_MEASURE_MEMORY_USAGE"] = "＃百分比＃％二手內存";
$MESS["PERFMON_MEASURE_OPCODE_CACHING"] = "PHP緩存（加速度）";
$MESS["PERFMON_MEASURE_SET_REC"] = "設置為\“＃值＃\”。";
$MESS["PERFMON_MEASURE_UP_AND_RUNNING"] = "啟動和運行";
$MESS["PERFMON_MEASURE_ZERO_OR_GREATER_THAN_REC"] = "設置為零或超過\“＃value＃\”。";
