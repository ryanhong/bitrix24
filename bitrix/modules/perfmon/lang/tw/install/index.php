<?php
$MESS["PERF_ADMIN"] = "完整的管理員訪問";
$MESS["PERF_DENIED"] = "拒絕訪問";
$MESS["PERF_INSTALL_TITLE"] = "安裝性能監視器模塊";
$MESS["PERF_MODULE_DESCRIPTION"] = "監視站點性能參數的模塊";
$MESS["PERF_MODULE_NAME"] = "性能監視器";
$MESS["PERF_UNINSTALL_TITLE"] = "卸載性能監視器模塊";
$MESS["PERF_VIEW"] = "查看所有統計數據";
