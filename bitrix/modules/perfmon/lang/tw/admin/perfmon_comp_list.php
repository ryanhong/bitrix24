<?php
$MESS["PERFMON_COMP_CACHE_COUNT"] = "緩存操作";
$MESS["PERFMON_COMP_CACHE_COUNT_C"] = "緩存重置";
$MESS["PERFMON_COMP_CACHE_COUNT_R"] = "緩存讀取";
$MESS["PERFMON_COMP_CACHE_COUNT_W"] = "緩存寫道";
$MESS["PERFMON_COMP_CACHE_SIZE"] = "快取";
$MESS["PERFMON_COMP_CACHE_TYPE"] = "快取";
$MESS["PERFMON_COMP_CACHE_TYPE_AUTO"] = "汽車";
$MESS["PERFMON_COMP_CACHE_TYPE_NO"] = "不";
$MESS["PERFMON_COMP_CACHE_TYPE_YES"] = "是的";
$MESS["PERFMON_COMP_COMPONENT_NAME"] = "成分";
$MESS["PERFMON_COMP_COMPONENT_TIME"] = "時間";
$MESS["PERFMON_COMP_COUNT"] = "數數";
$MESS["PERFMON_COMP_FIND"] = "搜尋";
$MESS["PERFMON_COMP_GROUP"] = "團體";
$MESS["PERFMON_COMP_GROUP_OFF"] = "未分組";
$MESS["PERFMON_COMP_GROUP_ON"] = "分組";
$MESS["PERFMON_COMP_HIT_ID"] = "打";
$MESS["PERFMON_COMP_HIT_SCRIPT_NAME"] = "腳本";
$MESS["PERFMON_COMP_ID"] = "ID";
$MESS["PERFMON_COMP_NN"] = "＃";
$MESS["PERFMON_COMP_PAGE"] = "成分";
$MESS["PERFMON_COMP_QUERIES"] = "查詢";
$MESS["PERFMON_COMP_QUERIES_TIME"] = "查詢時間";
$MESS["PERFMON_COMP_TITLE"] = "性能監視器：組件";
