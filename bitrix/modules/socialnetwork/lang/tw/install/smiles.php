<?php
$MESS["FICON_BIGGRIN"] = "大笑";
$MESS["FICON_COOL"] = "涼爽的";
$MESS["FICON_CRY"] = "哭";
$MESS["FICON_EEK"] = "驚訝";
$MESS["FICON_EVIL"] = "生氣的";
$MESS["FICON_EXCLAIM"] = "感嘆";
$MESS["FICON_IDEA"] = "主意";
$MESS["FICON_KISS"] = "吻";
$MESS["FICON_NEUTRAL"] = "懷疑論者";
$MESS["FICON_QUESTION"] = "問題";
$MESS["FICON_REDFACE"] = "使困惑";
$MESS["FICON_SAD"] = "傷心";
$MESS["FICON_SMILE"] = "微笑";
$MESS["FICON_WINK"] = "眨眼";
