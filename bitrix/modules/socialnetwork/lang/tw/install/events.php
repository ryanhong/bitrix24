<?php
$MESS["SONET_AGREE_FRIEND_DESC"] = "＃關係_id＃ - 關係ID
＃sender_user_id＃ - 發件人ID
＃sender_user_name＃ - 發件人名字
＃sender_USER_LAST_NAME＃ - 發送者姓氏
＃sender_email_to＃ - 發送者電子郵件地址
＃收件人_USER_ID＃ - 收件人ID
＃coverient_user_name＃ - 收件人名字
＃收件人_USER_LAST_NAME＃ - 收件人姓
＃收件人_USER_EMAIL_TO＃ - 收件人電子郵件地址
＃消息＃ - 消息正文";
$MESS["SONET_AGREE_FRIEND_MESSAGE"] = "來自＃site_name＃的問候！
-------------------------------------------------- --------

您好＃conferient_user_name＃！

＃sender_user_name＃＃sender_user_last_name＃確認您的邀請以加入您的朋友。

這是自動生成的通知。";
$MESS["SONET_AGREE_FRIEND_NAME"] = "確認朋友邀請";
$MESS["SONET_AGREE_FRIEND_SUBJECT"] = "＃site_name＃：朋友邀請的確認";
$MESS["SONET_BAN_FRIEND_DESC"] = "＃關係_id＃ - 關係ID
＃sender_user_id＃ - 發件人ID
＃sender_user_name＃ - 發件人名字
＃sender_USER_LAST_NAME＃ - 發送者姓氏
＃sender_email_to＃ - 發送者電子郵件地址
＃收件人_USER_ID＃ - 收件人ID
＃coverient_user_name＃ - 收件人名字
＃收件人_USER_LAST_NAME＃ - 收件人姓
＃收件人_USER_EMAIL_TO＃ - 收件人電子郵件地址
＃消息＃ - 消息正文";
$MESS["SONET_BAN_FRIEND_MESSAGE"] = "來自＃site_name＃的問候！
-------------------------------------------------- --------

您好＃conferient_user_name＃！

＃sender_user_name＃＃sender_user_last_name＃已將您添加到黑名單中。

這是自動生成的通知。";
$MESS["SONET_BAN_FRIEND_NAME"] = "黑名單";
$MESS["SONET_BAN_FRIEND_SUBJECT"] = "＃site_name＃：黑名單";
$MESS["SONET_INVITE_FRIEND_DESC"] = "＃關係_id＃ - 關係ID
＃sender_user_id＃ - 發件人ID
＃sender_user_name＃ - 發件人名字
＃sender_USER_LAST_NAME＃ - 發送者姓氏
＃sender_email_to＃ - 發送者電子郵件地址
＃收件人_USER_ID＃ - 收件人ID
＃coverient_user_name＃ - 收件人名字
＃收件人_USER_LAST_NAME＃ - 收件人姓
＃收件人_USER_EMAIL_TO＃ - 收件人電子郵件地址
＃消息＃ - 消息正文";
$MESS["SONET_INVITE_FRIEND_MESSAGE"] = "來自＃site_name＃的問候！
-------------------------------------------------- --------

您好＃conferient_user_name＃！

＃sender_user_name＃＃sender_user_last_name＃邀請您加入他們的朋友。

單擊下面的鏈接以響應邀請：

http：//＃server_name ## url＃

訊息:
-------------------------------------------------- --------
#訊息#
-------------------------------------------------- --------

這是自動生成的通知。";
$MESS["SONET_INVITE_FRIEND_NAME"] = "邀請加入朋友";
$MESS["SONET_INVITE_FRIEND_SUBJECT"] = "＃site_name＃：朋友邀請函";
$MESS["SONET_INVITE_GROUP_DESC"] = "＃關係_id＃ - 關係ID
＃group_id＃ - 組ID
＃USER_ID＃ - 用戶ID
＃group_name＃ - 組名稱
＃user_name＃ - 用戶名
＃USER_LAST_NAME＃ - 用戶姓氏
＃USER_EMAIL＃ - 用戶電子郵件地址
＃啟動_user_name＃ - 邀請發件人的名稱
＃啟動_USER_LAST_NAME＃ - 邀請發件人的姓氏
＃URL＃ - 通往用戶消息頁面的路徑頁
＃消息＃ - 消息正文";
$MESS["SONET_INVITE_GROUP_MESSAGE"] = "來自＃site_name＃的問候！
-------------------------------------------------- --------

您好＃user_name＃！

用戶＃啟動_USER_NAME＃＃啟動_USER_LAST_NAME＃邀請您加入＃group_name＃workgroup。

單擊下面的鏈接以響應邀請：

http：//＃server_name ## url＃

訊息:
-------------------------------------------------- --------
#訊息#
-------------------------------------------------- --------

這是自動生成的通知。";
$MESS["SONET_INVITE_GROUP_NAME"] = "加入小組的邀請";
$MESS["SONET_INVITE_GROUP_SUBJECT"] = "＃site_name＃：加入組的邀請";
$MESS["SONET_LOG_NEW_COMMENT_DESC"] = "＃email_to＃ - 消息收件人電子郵件
＃comment_id＃ - 評論ID
＃log_entry_id＃ - 消息ID
＃收件人_id＃ - 收件人ID
＃url_id＃ - 消息查看URL
";
$MESS["SONET_LOG_NEW_COMMENT_NAME"] = "新評論添加了";
$MESS["SONET_LOG_NEW_ENTRY_DESC"] = "＃email_to＃ - 消息收件人電子郵件
＃log_entry_id＃ - 消息ID
＃收件人_id＃ - 收件人ID
＃url_id＃ - 消息查看URL
";
$MESS["SONET_LOG_NEW_ENTRY_NAME"] = "添加了新消息";
$MESS["SONET_NEW_EVENT_DESC"] = "＃entity_id＃ - 事件源ID
＃log_date＃ - 事件已記錄的日期
＃標題＃ - 標題
＃消息＃ - 消息
＃URL＃ -  URL
＃實體＃-事件註冊處置
＃SUBSCRIBER_NAME＃ -收件人的名字
＃SUBSCRIBER_LAST_NAME＃ -收件人的姓氏
＃SISECRIBER_EMAIL＃ -收件人的電子郵件地址
＃SUBSCRIBER_ID＃ -收件人的ID";
$MESS["SONET_NEW_EVENT_GROUP_DESC"] = "＃entity_id＃ - 組ID
＃log_date＃ - 輸入日期
＃標題＃ - 標題
＃消息＃ - 消息
＃url＃ - 地址（url）
＃group_name＃ - 組標題
＃SUBSCRIBER_NAME＃ - 訂閱者的名字
＃SUBSCRIBER_LAST_NAME＃ - 訂戶的姓氏
＃SISECRIBER_EMAIL＃-SISPCRIBER的電子郵件地址
＃SUBSCRIBER_ID＃-subscriber ID";
$MESS["SONET_NEW_EVENT_GROUP_MESSAGE"] = "來自＃site_name＃的問候！
-------------------------------------------------- --------

您好＃subscriber_name＃！

組＃group_name＃中發生以下更改：

＃標題＃

-------------------------------------------------- --------
#訊息#
-------------------------------------------------- --------

您可以通過單擊以下鏈接打開網站：

http：//＃server_name ## url＃

這是自動生成的通知。";
$MESS["SONET_NEW_EVENT_GROUP_NAME"] = "新小組活動";
$MESS["SONET_NEW_EVENT_GROUP_SUBJECT"] = "＃site_name＃：新組事件";
$MESS["SONET_NEW_EVENT_MESSAGE"] = "來自＃site_name＃的問候！
-------------------------------------------------- --------

親愛的用戶＃subscriber_name＃！

自您上次訪問以來，已經發生以下更新：

＃標題＃

-------------------------------------------------- --------
#訊息#
-------------------------------------------------- --------

使用以下鏈接查看事件：

＃URL＃

此消息已自動生成。";
$MESS["SONET_NEW_EVENT_NAME"] = "新事件";
$MESS["SONET_NEW_EVENT_SUBJECT"] = "＃site_name＃：＃entity＃ - ＃entity_type＃中的新事件＃";
$MESS["SONET_NEW_EVENT_USER_DESC"] = "＃entity_id＃ - 組ID
＃log_date＃ - 輸入日期
＃標題＃ - 標題
＃消息＃ - 消息
＃url＃ - 地址（url）
＃user_name＃ - 用戶名
＃SUBSCRIBER_NAME＃ - 訂閱者的名字
＃SUBSCRIBER_LAST_NAME＃ - 訂戶的姓氏
＃SISECRIBER_EMAIL＃-SISPCRIBER的電子郵件地址
＃SUBSCRIBER_ID＃-subscriber ID";
$MESS["SONET_NEW_EVENT_USER_MESSAGE"] = "來自＃site_name＃的問候！
-------------------------------------------------- --------

您好＃subscriber_name＃！

用戶＃user_name＃具有以下新事件：

＃標題＃

-------------------------------------------------- --------
#訊息#
-------------------------------------------------- --------

您可以通過單擊以下鏈接打開網站：

http：//＃server_name ## url＃

這是自動生成的通知。";
$MESS["SONET_NEW_EVENT_USER_NAME"] = "新用戶事件";
$MESS["SONET_NEW_EVENT_USER_SUBJECT"] = "＃site_name＃：新用戶事件";
$MESS["SONET_NEW_MESSAGE_DESC"] = "＃Message_id＃ - 消息ID
＃USER_ID＃ - 用戶ID
＃user_name＃ - 用戶名
＃USER_LAST_NAME＃ - 用戶姓氏
＃sender_id＃ - 消息發送者ID
＃sender_name＃ - 消息發件人名字
＃sender_last_name＃ - 消息發送者姓氏
＃標題＃ - 消息標題
＃消息＃ - 消息正文
＃email_to＃ - 收件人電子郵件地址";
$MESS["SONET_NEW_MESSAGE_MESSAGE"] = "來自＃site_name＃的問候！
-------------------------------------------------- --------

您好＃user_name＃！

您從＃sender_name＃＃sender_last_name＃：

-------------------------------------------------- --------
#訊息#
-------------------------------------------------- --------

鏈接到消息：

http：//＃server_name＃/company/personal/sexssemp/chater/＃sender_id＃/

這是自動生成的通知。
";
$MESS["SONET_NEW_MESSAGE_NAME"] = "您有新消息";
$MESS["SONET_NEW_MESSAGE_SUBJECT"] = "＃site_name＃：您有一條新消息";
$MESS["SONET_REQUEST_GROUP_DESC"] = "＃message_id＃ - 消息ID
＃user_id＃ - 請求收件人ID
＃user_name＃ - 請求收件人名字
＃user_last_name＃ -請求收件人姓氏
＃sender_id＃ - 請求發件人ID
＃sender_name＃ -請求發件人名字
＃sender_last_name＃ -請求發件人姓氏
＃標題＃ - 消息標題
＃消息＃ - 消息正文
＃email_to＃ - 消息收件人的電子郵件";
$MESS["SONET_REQUEST_GROUP_MESSAGE"] = "來自＃site_name＃的消息
-------------------------------------------------- --------

您好＃user_name＃！

-------------------------------------------------- --------
#訊息#
-------------------------------------------------- --------

此消息已自動生成。";
$MESS["SONET_REQUEST_GROUP_NAME"] = "小組會員資格請求";
$MESS["SONET_REQUEST_GROUP_SUBJECT"] = "＃標題＃";
