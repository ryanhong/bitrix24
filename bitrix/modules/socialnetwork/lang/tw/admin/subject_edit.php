<?php
$MESS["SONETE_2FLIST"] = "主題";
$MESS["SONETE_ADDING"] = "創建一個新主題";
$MESS["SONETE_DELETE_SUBJECT"] = "刪除主題";
$MESS["SONETE_DELETE_SUBJECT_CONFIRM"] = "您確定要刪除此主題嗎？如果主題包含組，則不會刪除該主題。";
$MESS["SONETE_ERROR_SAVING"] = "保存主題的錯誤。";
$MESS["SONETE_NAME"] = "主題標題";
$MESS["SONETE_NEW_SUBJECT"] = "新話題";
$MESS["SONETE_NO_PERMS2ADD"] = "創建新主題的許可不足。";
$MESS["SONETE_SITE"] = "主題網站";
$MESS["SONETE_SORT"] = "主題排序";
$MESS["SONETE_TAB_SUBJECT"] = "小組主題";
$MESS["SONETE_TAB_SUBJECT_DESCR"] = "組主題參數";
$MESS["SONETE_UPDATING"] = "編輯主題參數";
