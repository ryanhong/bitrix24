<?php
$MESS["ERROR_ADD_NEW_REC"] = "錯誤創建一個新的笑臉。";
$MESS["ERROR_ADD_REC"] = "錯誤創建笑臉。";
$MESS["ERROR_DELETE"] = "刪除笑臉的錯誤。";
$MESS["ERROR_DEL_SMILE"] = "刪除笑臉的錯誤。";
$MESS["ERROR_EMPTY_NAME1"] = "設置笑臉標題";
$MESS["ERROR_EMPTY_NAME2"] = "為了：";
$MESS["ERROR_EMPTY_NEW_NAME"] = "設置新語言笑臉的標題";
$MESS["ERROR_UPDATE_REC"] = "錯誤更新笑臉。";
$MESS["FSAN_ADD_NEW"] = "新的笑臉";
$MESS["FSAN_ADD_NEW_ALT"] = "創造一個新的笑臉";
$MESS["FS_SUCCESS_DEL"] = "笑臉已成功刪除。";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "檢查：";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "刪除";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "選擇：";
$MESS["PAGES"] = "笑臉";
$MESS["SMILE_DEL"] = "刪除";
$MESS["SMILE_DEL_CONF"] = "您確定要刪除這種笑臉嗎？";
$MESS["SMILE_F_DESCR"] = "描述：";
$MESS["SMILE_F_NAME"] = "標題：";
$MESS["SMILE_ID"] = "ID";
$MESS["SMILE_NAV"] = "笑臉";
$MESS["SMILE_RESET"] = "取消";
$MESS["SMILE_SORT"] = "種類";
$MESS["SMILE_TITLE"] = "社交網絡笑臉";
$MESS["SMILE_TYPE"] = "類型";
$MESS["SMILE_TYPE_ICON"] = "圖示";
$MESS["SMILE_TYPE_SMILE"] = "笑臉";
$MESS["SMILE_UPD"] = "節省";
$MESS["SONET_ACTIONS"] = "動作";
$MESS["SONET_ADD_SMILE"] = "加上笑臉";
$MESS["SONET_DELETE_DESCR"] = "刪除笑臉";
$MESS["SONET_EDIT"] = "編輯";
$MESS["SONET_EDIT_DESCR"] = "編輯笑臉參數";
$MESS["SONET_NAME"] = "標題";
$MESS["SONET_SMILE_ICON"] = "圖像";
$MESS["SONET_TYPING"] = "打字";
