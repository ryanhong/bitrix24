<?php
$MESS["authprov_sg_a"] = "團體所有者";
$MESS["authprov_sg_current"] = "當前小組";
$MESS["authprov_sg_e"] = "組主持人";
$MESS["authprov_sg_k"] = "所有小組成員";
$MESS["authprov_sg_name"] = "社交網絡組";
$MESS["authprov_sg_name_out"] = "社交網絡組";
$MESS["authprov_sg_panel_last"] = "最後的";
$MESS["authprov_sg_panel_my_group"] = "我的團體";
$MESS["authprov_sg_panel_search"] = "搜尋";
$MESS["authprov_sg_panel_search_text"] = "輸入組名稱。";
$MESS["authprov_sg_socnet_group"] = "社交網絡組";
