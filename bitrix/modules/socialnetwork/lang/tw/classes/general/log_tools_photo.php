<?php
$MESS["SONET_IM_NEW_PHOTO"] = "上傳了一張新照片到相冊\“＃title＃\” in \“＃group_name＃\”。";
$MESS["SONET_PHOTOALBUM_IM_COMMENT"] = "在您的專輯中留下評論\“＃permum_title＃\”";
$MESS["SONET_PHOTOPHOTO_LOG_1"] = "＃fure_name＃添加了一個新照片\“＃標題＃\”。";
$MESS["SONET_PHOTO_ADD_COMMENT_SOURCE_ERROR"] = "未能向事件源添加評論。";
$MESS["SONET_PHOTO_DELETE_COMMENT_SOURCE_ERROR"] = "無法將評論刪除到事件源";
$MESS["SONET_PHOTO_IM_COMMENT"] = "在您的照片上發表評論\“＃photo_title＃\”，\“＃permun_title＃\”";
$MESS["SONET_PHOTO_LOG_1"] = "＃fure_name＃添加了一個文件＃標題＃";
$MESS["SONET_PHOTO_LOG_2"] = "照片（＃count＃）";
$MESS["SONET_PHOTO_LOG_GUEST"] = "客人";
$MESS["SONET_PHOTO_LOG_MAIL_TEXT"] = "新照片：＃鏈接＃和其他。";
$MESS["SONET_PHOTO_UPDATE_COMMENT_SOURCE_ERROR"] = "無法將評論更新為事件源";
