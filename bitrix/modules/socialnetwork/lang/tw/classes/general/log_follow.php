<?php
$MESS["SONET_LF_UNFOLLOW_IM_BUTTON_N"] = "不，謝謝";
$MESS["SONET_LF_UNFOLLOW_IM_BUTTON_Y"] = "啟用智能關注模式";
$MESS["SONET_LF_UNFOLLOW_IM_MESSAGE"] = "在活動流中獲取大量消息？啟用智能關注模式僅查看對您重要的事件。只有您是作者的消息，向您致意或提及您將被移至頂部。每當您在消息中添加評論時，就開始關注它。";
$MESS["SONET_LF_UNFOLLOW_IM_MESSAGE2"] = "提要中的帖子太多了？啟用智能關注模式僅查看重要事件。
您創建的帖子被提及或被選為收件人，將在提要中向上移動。在帖子中添加評論以接收有關它的更新。";
