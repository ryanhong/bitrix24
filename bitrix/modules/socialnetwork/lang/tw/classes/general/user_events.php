<?php
$MESS["SONET_GB_EMPTY_USER_ID"] = "未指定用戶。";
$MESS["SONET_GB_ERROR_NO_USER_ID"] = "指定的用戶不正確。";
$MESS["SONET_UE_EMPTY_EVENT_ID"] = "該事件未指定。";
$MESS["SONET_UE_EMPTY_SITE_ID"] = "該網站未指定。";
$MESS["SONET_UE_ERROR_NO_EVENT_ID"] = "指定的事件不正確。";
$MESS["SONET_UE_ERROR_NO_SITE"] = "該站點不正確。";
