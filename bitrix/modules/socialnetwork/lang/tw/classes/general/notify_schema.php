<?php
$MESS["SONET_NS_FRIEND"] = "列出或未列為朋友";
$MESS["SONET_NS_INOUT_GROUP"] = "小組成員更改（用於主持人）";
$MESS["SONET_NS_INVITE_GROUP_BTN"] = "收到邀請或請求加入工作組；從工作組中刪除";
$MESS["SONET_NS_INVITE_GROUP_INFO"] = "工作組成員更改";
$MESS["SONET_NS_INVITE_USER"] = "朋友邀請";
$MESS["SONET_NS_MODERATORS_GROUP"] = "您被分配或未分配為工作組主持人";
$MESS["SONET_NS_OWNER_GROUP"] = "工作組所有者更改";
$MESS["SONET_NS_SONET_GROUP_EVENT"] = "工作組更新";
