<?php
$MESS["BLG_NAME"] = "開始";
$MESS["BLG_SHARE"] = "和...分享：";
$MESS["BLG_SHARE_1"] = "和...分享：";
$MESS["BLG_SHARE_ALL"] = "所有員工";
$MESS["BLG_SHARE_ALL_BUS"] = "全部用戶";
$MESS["BLG_SHARE_HIDDEN_1"] = "隱藏的收件人";
$MESS["BLOG_BPE_DESTINATION_EMPTY"] = "請指定至少一個人。";
$MESS["BLOG_BPE_EXTRANET_ERROR"] = "對話不能向所有人講話，請指定至少一個收件人。";
$MESS["SBPE_EXISTING_POST_PREMODERATION"] = "無法發布，因為選擇了預製的用戶組作為收件人。";
$MESS["SBPE_MULTIPLE_PREMODERATION"] = "由於某些選定的組已預先修改，因此無法發布帖子。";
$MESS["SBPE_MULTIPLE_PREMODERATION2"] = "由於在這些工作組中啟用了預壓，因此無法發布項目：＃groups_list＃";
$MESS["SONET_HELPER_CREATED_BY_ANONYMOUS"] = "未經授權的訪客";
$MESS["SONET_HELPER_NO_PERMISSIONS"] = "拒絕訪問一個或多個消息收件人";
$MESS["SONET_HELPER_PAGE_TITLE_WORKGROUP_TEMPLATE"] = "＃工作組＃：＃標題＃";
$MESS["SONET_HELPER_STEPPER_LIVEFEED"] = "活動流重新索引";
$MESS["SONET_HELPER_STEPPER_LIVEFEED2"] = "重新索引提要職位";
$MESS["SONET_HELPER_VIDEO_CONVERSION_COMPLETED"] = "已連接到您的消息\“＃Post_title＃\”的視頻文件已轉換。現在可以向收件人使用。";
