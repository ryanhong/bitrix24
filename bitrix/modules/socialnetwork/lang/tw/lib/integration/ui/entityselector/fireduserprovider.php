<?php
$MESS["SOCNET_ENTITY_SELECTOR_FIREDUSER_FOOTER_INFO"] = "此視圖僅顯示了您選擇的字段與CRM項目鏈接的被解僱的員工。如果視圖沒有顯示您正在搜索的員工，則意味著他們沒有CRM依賴性。";
$MESS["SOCNET_ENTITY_SELECTOR_FIREDUSER_TAB_TITLE"] = "解僱的人";
