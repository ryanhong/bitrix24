<?php
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_TITLE"] = "工作組：\“＃group_name＃\”";
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_TITLE_PROJECT"] = "項目：\“＃group_name＃\”";
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_UNLINKED"] = "WorkGroup \“＃group_name＃\”不再鏈接到此聊天。";
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_UNLINKED_PROJECT"] = "項目\“＃group_name＃\”不再鏈接到此聊天。";
