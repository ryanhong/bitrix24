<?php
$MESS["SONET_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["SONET_ID"] = "用戶身份";
$MESS["SONET_ITEMS_COUNT"] = "列表中的項目";
$MESS["SONET_PAGE_VAR"] = "頁面變量";
$MESS["SONET_PATH_TO_USER"] = "用戶配置文件路徑模板";
$MESS["SONET_SET_NAVCHAIN"] = "設置麵包屑";
$MESS["SONET_USER_PROPERTY"] = "顯示其他屬性";
$MESS["SONET_USER_VAR"] = "用戶變量";
$MESS["SONET_VARIABLE_ALIASES"] = "可變別名";
