<?php
$MESS["SONET_C30_PRESET_FILTER_ALL"] = "所有事件";
$MESS["SONET_C30_SMART_FOLLOW"] = "啟用智能關注模式";
$MESS["SONET_C30_SMART_FOLLOW_HINT"] = "智能關注模式即將開始。只有您作為作者，收件人或文本中提到的帖子才會移至頂部。您會自動遵循您評論的任何帖子。";
$MESS["SONET_C30_T_FILTER_COMMENTS"] = "也搜索評論";
$MESS["SONET_C30_T_FILTER_CREATED_BY"] = "作者";
$MESS["SONET_C30_T_FILTER_DATE"] = "日期";
$MESS["SONET_C30_T_FILTER_GROUP"] = "團體";
$MESS["SONET_C30_T_FILTER_TITLE"] = "搜尋";
$MESS["SONET_C30_T_RESET"] = "重置";
$MESS["SONET_C30_T_SHOW_HIDDEN"] = "顯示隱藏的類別";
$MESS["SONET_C30_T_SUBMIT"] = "選擇";
