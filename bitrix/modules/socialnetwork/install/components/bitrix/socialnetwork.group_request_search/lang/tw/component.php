<?php
$MESS["SONET_C11_BAD_RELATION"] = "用戶“＃名＃”已被邀請到該組或已成為成員。";
$MESS["SONET_C11_BAD_USER"] = "用戶'＃名稱＃'禁用工作組的邀請";
$MESS["SONET_C11_ERR_SELF"] = "您不能將自己添加到一個小組中。";
$MESS["SONET_C33_CANNOT_USER_ADD"] = "沒有添加電子郵件“＃電子郵件＃”的用戶：";
$MESS["SONET_C33_NOT_EMPLOYEE"] = "沒有邀請使用電子郵件\\'＃電子郵件＃\\'的用戶，因為他們不是註冊用戶或不是員工。";
$MESS["SONET_C33_NO_GROUP"] = "找不到小組。";
$MESS["SONET_C33_NO_GROUP_ID"] = "該組未指定。";
$MESS["SONET_C33_NO_PERMS"] = "您無權邀請用戶加入該組。";
$MESS["SONET_C33_NO_USER1"] = "找不到用戶“＃名＃”。";
$MESS["SONET_C33_NO_USERS"] = "未指定邀請收件人。";
$MESS["SONET_C33_PAGE_TITLE"] = "邀請小組";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
