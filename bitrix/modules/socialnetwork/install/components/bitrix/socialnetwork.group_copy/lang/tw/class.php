<?php
$MESS["SOCNET_GROUP_COPY_DATE_FORMAT_ERROR"] = "不正確的日期字段格式";
$MESS["SOCNET_GROUP_COPY_FEATURE_BLOG"] = "活動流柱";
$MESS["SOCNET_GROUP_COPY_FEATURE_BLOG2"] = "飼料職位";
$MESS["SOCNET_GROUP_COPY_FEATURE_CHECKLISTS"] = "清單";
$MESS["SOCNET_GROUP_COPY_FEATURE_COMMENTS"] = "評論";
$MESS["SOCNET_GROUP_COPY_FEATURE_DEPARTMENTS"] = "部門";
$MESS["SOCNET_GROUP_COPY_FEATURE_ELEMENT"] = "專案";
$MESS["SOCNET_GROUP_COPY_FEATURE_FILES"] = "駕駛";
$MESS["SOCNET_GROUP_COPY_FEATURE_GROUP_LISTS"] = "列表";
$MESS["SOCNET_GROUP_COPY_FEATURE_LANDING_KNOWLEDGE"] = "知識庫";
$MESS["SOCNET_GROUP_COPY_FEATURE_ONLY_FOLDERS"] = "僅文件夾";
$MESS["SOCNET_GROUP_COPY_FEATURE_PHOTO"] = "照片庫";
$MESS["SOCNET_GROUP_COPY_FEATURE_ROBOTS"] = "自動化規則";
$MESS["SOCNET_GROUP_COPY_FEATURE_SECTION"] = "部分";
$MESS["SOCNET_GROUP_COPY_FEATURE_TASKS"] = "任務";
$MESS["SOCNET_GROUP_COPY_FEATURE_USERS"] = "成員";
$MESS["SOCNET_GROUP_COPY_FEATURE_VOTE_RESULT"] = "民意調查結果";
$MESS["SOCNET_GROUP_COPY_FEATURE_WORKFLOW"] = "工作流程";
$MESS["SOCNET_GROUP_COPY_TITLE_BASE"] = "複製";
$MESS["SOCNET_GROUP_COPY_TITLE_BASE_GROUP"] = "複製工作組";
$MESS["SOCNET_GROUP_COPY_TITLE_BASE_PROJECT"] = "複製項目";
$MESS["SOCNET_GROUP_COPY_TITLE_GROUP"] = "團體";
$MESS["SOCNET_GROUP_COPY_TITLE_PROJECT"] = "專案";
