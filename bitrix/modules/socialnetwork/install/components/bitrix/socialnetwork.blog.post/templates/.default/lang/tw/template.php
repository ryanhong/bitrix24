<?php
$MESS["BLOG_ALREADY_READ"] = "我讀過";
$MESS["BLOG_BLOG_BLOG_CATEGORY"] = "標籤：";
$MESS["BLOG_BLOG_BLOG_DELETE"] = "刪除";
$MESS["BLOG_BLOG_BLOG_EDIT"] = "編輯";
$MESS["BLOG_BLOG_BLOG_MORE"] = "閱讀更多...";
$MESS["BLOG_BLOG_BLOG_NO_AVAIBLE_MES"] = "沒有找到對話。";
$MESS["BLOG_COMMENTS"] = "評論";
$MESS["BLOG_COMMENTS_ADD"] = "評論";
$MESS["BLOG_DESTINATION_ALL"] = "給所有員工";
$MESS["BLOG_DESTINATION_ALL_BSM"] = "全部";
$MESS["BLOG_DESTINATION_HIDDEN_0"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_1"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_2"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_3"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_4"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_5"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_6"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_7"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_8"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_HIDDEN_9"] = "和＃num＃隱藏的收件人";
$MESS["BLOG_DESTINATION_MORE_0"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_1"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_2"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_3"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_4"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_5"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_6"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_7"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_8"] = "和＃num＃更多收件人";
$MESS["BLOG_DESTINATION_MORE_9"] = "和＃num＃更多收件人";
$MESS["BLOG_FILES"] = "文件：";
$MESS["BLOG_HREF"] = "打開消息";
$MESS["BLOG_LINK"] = "關聯";
$MESS["BLOG_LINK2"] = "複製鏈接";
$MESS["BLOG_MES_DELETE_POST_CONFIRM"] = "您確定要刪除對話嗎？";
$MESS["BLOG_MES_HIDE"] = "隱藏";
$MESS["BLOG_MES_HIDE_POST_CONFIRM"] = "您確定要隱藏對話嗎？";
$MESS["BLOG_MONTH_01"] = "一月";
$MESS["BLOG_MONTH_02"] = "二月";
$MESS["BLOG_MONTH_03"] = "行進";
$MESS["BLOG_MONTH_04"] = "四月";
$MESS["BLOG_MONTH_05"] = "可能";
$MESS["BLOG_MONTH_06"] = "六月";
$MESS["BLOG_MONTH_07"] = "七月";
$MESS["BLOG_MONTH_08"] = "八月";
$MESS["BLOG_MONTH_09"] = "九月";
$MESS["BLOG_MONTH_10"] = "十月";
$MESS["BLOG_MONTH_11"] = "十一月";
$MESS["BLOG_MONTH_12"] = "十二月";
$MESS["BLOG_PHOTO"] = "照片：";
$MESS["BLOG_PINNED_COMMENTS"] = "評論：";
$MESS["BLOG_POST"] = "對話";
$MESS["BLOG_POST_BUTTON_COPILOT"] = "副駕駛";
$MESS["BLOG_POST_BUTTON_MORE"] = "更多的";
$MESS["BLOG_POST_CREATE_TASK"] = "創建任務";
$MESS["BLOG_POST_CREATE_TASK_BUTTON_TITLE"] = "查看任務";
$MESS["BLOG_POST_CREATE_TASK_ERROR_GET_DATA"] = "無法獲取數據來創建任務。";
$MESS["BLOG_POST_CREATE_TASK_FAILURE_TITLE"] = "錯誤";
$MESS["BLOG_POST_CREATE_TASK_SUCCESS_DESCRIPTION"] = "您現在可以查看和編輯任務。";
$MESS["BLOG_POST_CREATE_TASK_SUCCESS_TITLE"] = "已經創建了任務";
$MESS["BLOG_POST_CREATE_TASK_WAIT"] = "請稍等...";
$MESS["BLOG_POST_FOLLOW_N"] = "跟隨";
$MESS["BLOG_POST_FOLLOW_Y"] = "取消關注";
$MESS["BLOG_POST_LIMITED_VIEW"] = "（一旦視頻轉換後，用戶將看到該帖子）";
$MESS["BLOG_POST_LINK_COPIED"] = "鏈接複製";
$MESS["BLOG_POST_MOD_PUB"] = "發布";
$MESS["BLOG_POST_PINNED_COLLAPSE"] = "坍塌";
$MESS["BLOG_POST_PINNED_EXPAND"] = "擴張";
$MESS["BLOG_POST_VOTE_EXPORT"] = "導出民意調查結果";
$MESS["BLOG_READERS"] = "用戶";
$MESS["BLOG_READ_"] = "標記為已讀";
$MESS["BLOG_READ_F"] = "標記為已讀";
$MESS["BLOG_READ_M"] = "標記為已讀";
$MESS["BLOG_SHARE"] = "添加收件人";
$MESS["BLOG_SHARE_ADD"] = "添加";
$MESS["BLOG_SHARE_CANCEL"] = "取消";
$MESS["BLOG_USERS_ALREADY_READ"] = "閱讀";
$MESS["MPF_DESTINATION"] = "到";
$MESS["MPF_DESTINATION_1"] = "添加人員，小組或部門";
$MESS["MPF_DESTINATION_2"] = "添加更多";
$MESS["MPF_DESTINATION_3"] = "所有員工";
$MESS["MPF_DESTINATION_4"] = "全部用戶";
