<?php
$MESS["BR_BLOG_VAR"] = "博客標識符變量";
$MESS["BR_GROUP_ID"] = "博客組標識符";
$MESS["BR_NUM_POSTS"] = "發出的帖子";
$MESS["BR_PAGE_VAR"] = "頁面變量";
$MESS["BR_PATH_TO_GROUP_POST"] = "工作組博客文章頁面路徑";
$MESS["BR_PATH_TO_POST"] = "博客消息頁的模板";
$MESS["BR_PATH_TO_USER"] = "博客用戶頁面路徑的模板";
$MESS["BR_POST_VAR"] = "博客消息標識符變量";
$MESS["BR_TYPE"] = "RSS格式";
$MESS["BR_USER_VAR"] = "博客用戶標識符變量";
$MESS["B_VARIABLE_ALIASES"] = "可變別名";
