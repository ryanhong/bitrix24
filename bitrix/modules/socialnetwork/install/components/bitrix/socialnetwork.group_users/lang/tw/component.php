<?php
$MESS["SONET_C25_NAME_TEMPLATE_DEFAULT"] = "＃NOBR ##名稱＃#last_name ##/nobr＃";
$MESS["SONET_C25_NAV"] = "成員";
$MESS["SONET_C25_NOT_SELECTED"] = "沒有選擇用戶。";
$MESS["SONET_C25_NO_PERMS"] = "您無權查看此組。";
$MESS["SONET_C25_PAGE_TITLE"] = "小組成員";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_USER_NO_GROUP"] = "找不到小組。";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶。";
