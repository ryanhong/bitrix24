<?php
$MESS["SONET_GROUP_VAR"] = "組變量";
$MESS["SONET_LOG_DATE_DAYS"] = "天數";
$MESS["SONET_LOG_LOG_CNT"] = "最大可見物品";
$MESS["SONET_LOG_SUBSCRIBE_ONLY"] = "包括訂閱";
$MESS["SONET_PAGE_VAR"] = "頁面變量";
$MESS["SONET_PATH_TO_GROUP"] = "小組頁面路徑模板";
$MESS["SONET_PATH_TO_MESSAGES_CHAT"] = "聊天日誌頁面路徑模板";
$MESS["SONET_PATH_TO_MESSAGES_USERS"] = "消息傳遞頁面路徑模板";
$MESS["SONET_PATH_TO_MESSAGES_USERS_MESSAGES"] = "消息日誌頁面路徑模板";
$MESS["SONET_PATH_TO_MESSAGE_FORM"] = "消息帖子形式路徑模板";
$MESS["SONET_PATH_TO_SMILE"] = "笑臉文件夾的路徑（相對於站點根）";
$MESS["SONET_PATH_TO_USER"] = "用戶配置文件路徑模板";
$MESS["SONET_SET_NAVCHAIN"] = "設置麵包屑";
$MESS["SONET_USER_ID"] = "用戶身份";
$MESS["SONET_USER_VAR"] = "用戶變量";
$MESS["SONET_USE_COMMENTS"] = "與事件有關";
$MESS["SONET_VARIABLE_ALIASES"] = "可變別名";
