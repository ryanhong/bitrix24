<?php
$MESS["FL_FORUM_CHAIN"] = "論壇";
$MESS["FL_FORUM_GROUP_CHAIN"] = "討論";
$MESS["FL_FORUM_USER_CHAIN"] = "論壇";
$MESS["IBLOCK_DEFAULT_UF"] = "默認畫廊";
$MESS["SONET_APP_IS_NOT_ACTIVE"] = "找不到申請。";
$MESS["SONET_CONTENT_SEARCH_CHAIN"] = "搜尋";
$MESS["SONET_CREATE_WEBDAV"] = "創建庫";
$MESS["SONET_FILES"] = "文件";
$MESS["SONET_FILES_IS_NOT_ACTIVE"] = "未啟用\“ files \”功能。";
$MESS["SONET_GALLERIES_IS_NOT_ACTIVE"] = "您的照片庫當前不活動。請聯繫管理員。";
$MESS["SONET_GALLERY_IS_NOT_ACTIVE"] = "您的照片庫目前不活動。請聯繫管理員。";
$MESS["SONET_GALLERY_NOT_FOUND"] = "沒有照片。";
$MESS["SONET_GROUP"] = "團體";
$MESS["SONET_GROUP_NOT_EXISTS"] = "該組不存在。";
$MESS["SONET_GROUP_PAGES_TITLE_TEMPLATE"] = "＃group_name＃：＃page_title＃";
$MESS["SONET_GROUP_PREFIX"] = "團體：";
$MESS["SONET_IBLOCK_ID_EMPTY"] = "未指定信息塊。";
$MESS["SONET_IB_MODULE_IS_NOT_INSTALLED"] = "信息塊模塊未安裝。";
$MESS["SONET_LOADING"] = "載入中...";
$MESS["SONET_LOG_TEMPLATE_AUTHOR"] = "
作者：<a href= \"#url# \">＃標題＃</a>。";
$MESS["SONET_LOG_TEMPLATE_AUTHOR_MAIL"] = "
作者：＃標題＃（＃url＃）。";
$MESS["SONET_LOG_TEMPLATE_GUEST"] = "
作者：＃fure_name＃。";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_NAME_TEMPLATE_DEFAULT"] = "＃NOBR ##名稱＃#last_name ##/nobr＃";
$MESS["SONET_PANEL_REINDEX"] = "reindex";
$MESS["SONET_PANEL_REINDEX_TITLE"] = "Reindex社交網絡搜索數據";
$MESS["SONET_PHOTO"] = "照片";
$MESS["SONET_PHOTO_IS_NOT_ACTIVE"] = "未啟用\“ Photos \”功能。";
$MESS["SONET_P_MODULE_IS_NOT_INSTALLED"] = "未安裝照片庫模塊。";
$MESS["SONET_WD_MODULE_IS_NOT_INSTALLED"] = "未安裝文檔庫模塊。";
$MESS["SONET_WEBDAV_NOT_EXISTS"] = "沒有文件。";
$MESS["SONET_WORKGROUPS_FEATURE_DISABLED"] = "工作組功能被禁用。";
