<?php
$MESS["SONET_SGM_T_MODULE_NOT_INSTALLED"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_SGM_T_NOTIFY_HINT_OFF"] = "您將不再在Messenger中收到<br>組更新";
$MESS["SONET_SGM_T_NOTIFY_HINT_ON"] = "您將在Messenger中收到<br>組更新";
$MESS["SONET_SGM_T_NOTIFY_TITLE_OFF"] = "禁用通知";
$MESS["SONET_SGM_T_NOTIFY_TITLE_ON"] = "啟用了通知";
$MESS["SONET_SGM_T_NOT_ATHORIZED"] = "你沒有登錄。";
$MESS["SONET_SGM_T_SESSION_WRONG"] = "您的會議已經過期。請再試一次。";
$MESS["SONET_SGM_T_WAIT"] = "請等待＆hellip;";
