<?php
$MESS["WD_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["WD_EDIT_FILE"] = "編輯";
$MESS["WD_EDIT_FILE_ALT"] = "編輯文檔";
$MESS["WD_EDIT_FILE_ALT2"] = "編輯文檔版本";
$MESS["WD_HISTORY_DOCUMENT_TITLE"] = "＃名稱＃的文檔歷史記錄";
$MESS["WD_HISTORY_VERSION_TITLE"] = "＃名稱＃的版本歷史記錄";
$MESS["WD_ORIGINAL"] = "原來的";
$MESS["WD_VERSIONS"] = "版本";
$MESS["WD_VERSIONS_ALT"] = "打開文檔版本";
