<?php
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_NAME_TEMPLATE_DEFAULT"] = "＃NOBR ##名稱＃#last_name ##/nobr＃";
$MESS["SONET_P_PU_NO_RIGHTS"] = "編輯配置文件的許可不足。";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶。";
$MESS["SONET_P_USER_SEX_F"] = "女性";
$MESS["SONET_P_USER_SEX_M"] = "男性";
$MESS["SONET_P_USER_TITLE"] = "編輯用戶資料";
$MESS["SONET_P_USER_TITLE_VIEW"] = "用戶資料";
