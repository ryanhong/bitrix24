<?php
$MESS["SONET_C11_BAD_RELATION"] = "該用戶已被邀請到該組或已成為成員。";
$MESS["SONET_C11_BAD_USER"] = "該用戶禁用工作組的邀請。";
$MESS["SONET_C11_ERR_SELF"] = "您不能將自己添加到一個小組中。";
$MESS["SONET_C11_NO_MESSAGE"] = "未指定用戶消息。";
$MESS["SONET_C11_NO_PERMS"] = "您無權邀請用戶參加此組。";
$MESS["SONET_C11_TITLE"] = "邀請小組";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_USER_NO_GROUP"] = "找不到小組。";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶。";
