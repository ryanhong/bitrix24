<?php
$MESS["PAGE_VAR_TIP"] = "在此處指定將傳遞社交網絡頁面的變量的名稱。";
$MESS["PATH_TO_USER_TIP"] = "用戶配置文件頁面的路徑。示例：sonet_user.php？page = user＆user_id =＃user_id＃。";
$MESS["SET_TITLE_TIP"] = "如果該選項處於活動狀態，則頁面標題將設置為<nobr> <b> </b> <i> \“用戶名\” </i>。</i>。</nobr>";
$MESS["USER_VAR_TIP"] = "在此處指定社交網絡用戶ID的變量的名稱。";
