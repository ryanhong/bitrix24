<?php
$MESS["SONET_C12_CHECK_ALL"] = "檢查全部 /取消選中";
$MESS["SONET_C12_DO_REJECT"] = "衰退";
$MESS["SONET_C12_DO_SAVE"] = "批准會員資格";
$MESS["SONET_C12_INVITE"] = "邀請小組";
$MESS["SONET_C12_MESSAGE"] = "訊息";
$MESS["SONET_C12_NO_REQUESTS"] = "沒有請求。";
$MESS["SONET_C12_NO_REQUESTS_DESCR"] = "在此處查看和處理用戶請求小組成員資格。";
$MESS["SONET_C12_SENDER"] = "發件人";
$MESS["SONET_C12_SUBTITLE"] = "小組成員資格請求";
