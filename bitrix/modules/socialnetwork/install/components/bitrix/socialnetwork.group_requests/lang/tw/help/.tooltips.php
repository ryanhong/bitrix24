<?php
$MESS["ID_TIP"] = "指定將傳遞用戶ID的代碼。";
$MESS["PAGE_VAR_TIP"] = "在此處指定將傳遞社交網絡頁面的變量的名稱。";
$MESS["PATH_TO_USER_EDIT_TIP"] = "用戶配置文件編輯器頁面的路徑。示例：sonet_user_edit.php？page = user＆user_id =＃user_id＃＆mode =編輯。";
$MESS["PATH_TO_USER_TIP"] = "用戶配置文件頁面的路徑。示例：sonet_user.php？page = user＆user_id =＃user_id＃。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將將頁面標題設置為<i> \“用戶名\” </i> <b>用戶配置文件</b>。";
$MESS["USER_PROPERTY_TIP"] = "在此處選擇將在用戶配置文件中顯示的其他屬性。";
$MESS["USER_VAR_TIP"] = "在此處指定社交網絡用戶ID的變量的名稱。";
