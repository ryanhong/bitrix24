<?php
$MESS["SBPSM_AVATAR_SIZE"] = "郵件的頭像大小（PX）";
$MESS["SBPSM_AVATAR_SIZE_COMMENT"] = "評論的頭像大小（PX）";
$MESS["SBPSM_COMMENTS_COUNT"] = "要顯示的評論數量";
$MESS["SBPSM_EMAIL_TO"] = "收件人電子郵件地址";
$MESS["SBPSM_POST_ID"] = "消息ID";
$MESS["SBPSM_RECIPIENT_ID"] = "收件人ID";
$MESS["SBPSM_URL"] = "消息URL";
