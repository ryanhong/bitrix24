<?php
$MESS["SONET_C28_T_ACTIONS"] = "動作";
$MESS["SONET_C28_T_ALL_MSGS"] = "所有消息";
$MESS["SONET_C28_T_DELETE"] = "刪除";
$MESS["SONET_C28_T_DO_DELETE"] = "刪除";
$MESS["SONET_C28_T_EMPTY"] = "沒有傳出消息。<br>此窗格顯示發送給您的聯繫人的消息。";
$MESS["SONET_C28_T_MESSAGE"] = "訊息";
$MESS["SONET_C28_T_SELECT_ALL"] = "檢查全部 /取消選中";
$MESS["SONET_C28_T_TO_USER"] = "接受者";
