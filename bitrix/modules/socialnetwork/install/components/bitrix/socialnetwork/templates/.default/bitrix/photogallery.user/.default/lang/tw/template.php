<?php
$MESS["P_GALLERIES_MY"] = "我的畫廊";
$MESS["P_GALLERIES_MY_TITLE"] = "查看我的畫廊";
$MESS["P_GALLERY"] = "畫廊";
$MESS["P_GALLERY_MY"] = "我的圖片庫";
$MESS["P_GALLERY_MY_TITLE"] = "編輯我的畫廊";
$MESS["P_GALLERY_TITLE"] = "畫廊";
$MESS["P_LOGIN"] = "登入";
$MESS["P_LOGIN_TITLE"] = "授權";
$MESS["P_PHOTO"] = "相片";
$MESS["P_PHOTO_MY"] = "我的照片";
$MESS["P_PHOTO_MY_TITLE"] = "查看我的照片";
$MESS["P_PHOTO_TITLE"] = "查看畫廊照片";
$MESS["P_UPLOAD"] = "上傳圖片";
