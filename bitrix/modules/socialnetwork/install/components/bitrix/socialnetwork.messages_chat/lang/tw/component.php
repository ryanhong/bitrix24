<?php
$MESS["SONET_C50_NO_PERMS"] = "您無權向該用戶發送消息。";
$MESS["SONET_C50_PAGE_TITLE"] = "消息傳遞";
$MESS["SONET_C50_SELF"] = "您不能向自己發送消息。";
$MESS["SONET_CHAT_GROUP_ACESS"] = "訪問該組是受限制的。";
$MESS["SONET_CHAT_GROUP_NOT_FOUND"] = "找不到組。";
$MESS["SONET_CHAT_GROUP_TITLE"] = "給小組寫一條消息";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶。";
