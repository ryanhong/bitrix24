<?php
$MESS["SN_SPACES_DISCUSSIONS_COMPOSITION_GENERAL_SPACES_NAME"] = "共享空間";
$MESS["SN_SPACES_SCRUM_ACTION_INCLUDE_MODULE_ERROR"] = "不能包括所需的模塊。";
$MESS["SN_SPACES_SCRUM_VIEW_ACTIVE_SPRINT"] = "主動衝刺";
$MESS["SN_SPACES_SCRUM_VIEW_COMPLETED_SPRINT"] = "完成的衝刺";
$MESS["SN_SPACES_SCRUM_VIEW_PLAN"] = "規劃";
