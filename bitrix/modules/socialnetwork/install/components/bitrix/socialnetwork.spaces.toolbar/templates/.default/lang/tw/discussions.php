<?php
$MESS["SN_SPACES_DISCUSSIONS_COMPOSITION"] = "包括在飼料中";
$MESS["SN_SPACES_DISCUSSIONS_COMPOSITION_FILTER_BIZPROC"] = "工作流程";
$MESS["SN_SPACES_DISCUSSIONS_COMPOSITION_FILTER_BLOG"] = "帖子";
$MESS["SN_SPACES_DISCUSSIONS_COMPOSITION_FILTER_CALENDAR"] = "日曆事件";
$MESS["SN_SPACES_DISCUSSIONS_COMPOSITION_FILTER_TASKS"] = "任務";
$MESS["SN_SPACES_DISCUSSIONS_CREATE_TASK"] = "創建一個任務";
$MESS["SN_SPACES_DISCUSSIONS_ORGANIZE_EVENT"] = "創建一個事件";
$MESS["SN_SPACES_DISCUSSIONS_SETTINGS_SMART_TRACKING"] = "智能關注";
$MESS["SN_SPACES_DISCUSSIONS_START_DISCUSSIONS"] = "開始討論";
$MESS["SN_SPACES_DISCUSSIONS_UPLOAD_FILE"] = "上傳文件";
$MESS["SN_SPACES_DISCUSSIONS_UPLOAD_FILE_NOTIFY_MESSAGE"] = "添加了文件。 <a href= \"#path# \ \">查看</a>";
$MESS["SN_SPACES_DISCUSSION_COMPOSITION_TITLE"] = "選擇要在％space_name％的feed中看到的項目。";
