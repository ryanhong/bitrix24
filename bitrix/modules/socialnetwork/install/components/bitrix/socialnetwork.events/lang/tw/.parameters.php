<?php
$MESS["SONET_ID"] = "用戶身份";
$MESS["SONET_MESSAGE_VAR"] = "消息可變";
$MESS["SONET_PAGE_VAR"] = "頁面變量";
$MESS["SONET_PATH_TO_MESSAGES_CHAT"] = "聊天頁面路徑模板";
$MESS["SONET_PATH_TO_MESSAGE_FORM"] = "消息帖子形式路徑模板";
$MESS["SONET_PATH_TO_MESSAGE_FORM_MESS"] = "回复表單路徑模板";
$MESS["SONET_PATH_TO_SMILE"] = "笑臉文件夾的路徑（相對於站點根）";
$MESS["SONET_PATH_TO_USER"] = "用戶配置文件路徑模板";
$MESS["SONET_USER_VAR"] = "用戶變量";
$MESS["SONET_VARIABLE_ALIASES"] = "可變別名";
