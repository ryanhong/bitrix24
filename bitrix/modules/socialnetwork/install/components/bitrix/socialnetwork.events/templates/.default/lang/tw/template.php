<?php
$MESS["SONET_C1_ANSWER"] = "回覆";
$MESS["SONET_C1_BAN"] = "禁止";
$MESS["SONET_C1_CHAT"] = "公開聊天";
$MESS["SONET_C1_CLOSE"] = "關閉";
$MESS["SONET_C1_FR_ADD"] = "接受";
$MESS["SONET_C1_FR_TEXT"] = "想把你列為朋友";
$MESS["SONET_C1_FR_TITLE"] = "友誼邀請！";
$MESS["SONET_C1_GR_ADD"] = "接受";
$MESS["SONET_C1_GR_INV"] = "邀請：";
$MESS["SONET_C1_GR_TITLE"] = "邀請加入集團！";
$MESS["SONET_C1_MS_TITLE"] = "你有一條信息！";
$MESS["SONET_C1_ONLINE"] = "在線的";
$MESS["SONET_C1_REJECT"] = "衰退";
