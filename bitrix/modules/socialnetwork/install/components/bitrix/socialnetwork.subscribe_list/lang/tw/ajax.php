<?php
$MESS["SUBSCRIBE_NAME1_blog"] = "報告";
$MESS["SUBSCRIBE_NAME1_calendar"] = "日曆";
$MESS["SUBSCRIBE_NAME1_files"] = "文件";
$MESS["SUBSCRIBE_NAME1_forum"] = "溝通";
$MESS["SUBSCRIBE_NAME1_photo"] = "照片";
$MESS["SUBSCRIBE_NAME1_tasks"] = "任務";
$MESS["SUBSCRIBE_NAME_blog"] = "部落格";
$MESS["SUBSCRIBE_NAME_calendar"] = "日曆";
$MESS["SUBSCRIBE_NAME_files"] = "文件";
$MESS["SUBSCRIBE_NAME_forum"] = "論壇";
$MESS["SUBSCRIBE_NAME_photo"] = "照片";
$MESS["SUBSCRIBE_NAME_tasks"] = "任務";
$MESS["SUBSCRIBE_TRANSPORT_DIGEST"] = "每日摘要";
$MESS["SUBSCRIBE_TRANSPORT_DIGEST_WEEK"] = "每週摘要";
$MESS["SUBSCRIBE_TRANSPORT_MAIL"] = "電子郵件通知";
$MESS["SUBSCRIBE_TRANSPORT_NONE"] = "不要通知";
$MESS["SUBSCRIBE_TRANSPORT_XMPP"] = "即時消息";
$MESS["SUBSCRIBE_VISIBLE_HIDDEN"] = "隱藏";
$MESS["SUBSCRIBE_VISIBLE_VISIBLE"] = "展示";
