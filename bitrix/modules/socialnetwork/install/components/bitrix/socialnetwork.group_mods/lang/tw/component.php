<?php
$MESS["SONET_C10_NAME_TEMPLATE_DEFAULT"] = "＃NOBR ##名稱＃#last_name ##/nobr＃";
$MESS["SONET_C10_NAV"] = "主持人";
$MESS["SONET_C10_NO_PERMS"] = "您無權查看此組。";
$MESS["SONET_C10_NO_SELECTED"] = "未分配調節器。";
$MESS["SONET_C10_TITLE"] = "組主持人";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_USER_NO_GROUP"] = "找不到小組。";
