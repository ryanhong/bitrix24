<?php
$MESS["F_BVARSFROMFORM"] = "使用表單數據";
$MESS["F_DEFAULT_FID"] = "論壇ID";
$MESS["F_DEFAULT_MESSAGE_TYPE"] = "編輯表單顯示模式（回复，編輯，新主題）";
$MESS["F_DEFAULT_MID"] = "消息ID";
$MESS["F_DEFAULT_PAGE_NAME"] = "呼叫者組件ID";
$MESS["F_DEFAULT_TID"] = "主題ID";
$MESS["F_LIST_TEMPLATE"] = "主題列表頁面";
$MESS["F_MESSAGE_TEMPLATE"] = "消息查看頁面";
$MESS["F_SOCNET_GROUP_ID"] = "組ID";
$MESS["F_USER_ID"] = "用戶身份";
