<?php
$MESS["SONET_C38_TP_NO_PERMS"] = "您無權查看此用戶的配置文件。";
$MESS["SONET_C38_T_BIRTHDAY"] = "生日";
$MESS["SONET_C38_T_HONOURED"] = "榮幸";
$MESS["SONET_C38_T_ONLINE"] = "在線的";
$MESS["SONET_C39_ABSENT"] = "不在辦公室";
$MESS["SONET_C39_CONTACT_TITLE"] = "聯繫信息";
$MESS["SONET_C39_CONTACT_UNAVAIL"] = "聯繫信息不可用。";
$MESS["SONET_C39_EDIT_FEATURES"] = "編輯設置";
$MESS["SONET_C39_EDIT_PROFILE"] = "編輯個人資料";
$MESS["SONET_C39_EDIT_SETTINGS"] = "編輯隱私設置";
$MESS["SONET_C39_FR_ADD"] = "加為好友";
$MESS["SONET_C39_FR_DEL"] = "不朋友";
$MESS["SONET_C39_INV_GROUP"] = "邀請小組";
$MESS["SONET_C39_PERSONAL_TITLE"] = "個人資料";
$MESS["SONET_C39_PERSONAL_UNAVAIL"] = "個人信息不可用。";
$MESS["SONET_C39_SEND_MESSAGE"] = "發信息";
$MESS["SONET_C39_SHOW_MESSAGES"] = "顯示消息日誌";
$MESS["SONET_C39_VIDEO_CALL"] = "視訊通話";
$MESS["SONET_P_USER_BIRTHDAY"] = "生日：";
$MESS["SONET_P_USER_SEX"] = "性別：";
