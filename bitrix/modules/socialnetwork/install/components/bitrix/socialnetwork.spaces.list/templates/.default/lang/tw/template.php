<?php
$MESS["SOCIALNETWORK_SPACES_ITEM_DESC"] = "任務：移動應用的黑暗主題";
$MESS["SOCIALNETWORK_SPACES_ITEM_TITLE"] = "產品設計";
$MESS["SOCIALNETWORK_SPACES_LIST_ACCEPT_INVITATION_BUTTON"] = "加入";
$MESS["SOCIALNETWORK_SPACES_LIST_ADD_SPACE_FORM_NAME_PLACEHOLDER"] = "姓名";
$MESS["SOCIALNETWORK_SPACES_LIST_ADD_SPACE_ITEM_DESCRIPTION"] = "只需單擊幾下";
$MESS["SOCIALNETWORK_SPACES_LIST_ADD_SPACE_ITEM_TITLE"] = "添加空間";
$MESS["SOCIALNETWORK_SPACES_LIST_DECLINE_INVITATION_BUTTON"] = "衰退";
$MESS["SOCIALNETWORK_SPACES_LIST_FILTER_MODE_ALL_DESCRIPTION"] = "瀏覽所有公司的空間，以使自己保持循環。";
$MESS["SOCIALNETWORK_SPACES_LIST_FILTER_MODE_ALL_TITLE"] = "所有空間";
$MESS["SOCIALNETWORK_SPACES_LIST_FILTER_MODE_MY_DESCRIPTION"] = "查看並僅遵循您是成員的空間。";
$MESS["SOCIALNETWORK_SPACES_LIST_FILTER_MODE_MY_TITLE"] = "我的空間";
$MESS["SOCIALNETWORK_SPACES_LIST_FILTER_MODE_OTHER_DESCRIPTION"] = "查看您的同事創建的空間。您可能想加入他們。";
$MESS["SOCIALNETWORK_SPACES_LIST_FILTER_MODE_OTHER_TITLE"] = "其他空間";
$MESS["SOCIALNETWORK_SPACES_LIST_JOIN_SPACE_BUTTON"] = "加入";
$MESS["SOCIALNETWORK_SPACES_LIST_MODE_POPUP_BOTTOM_DESCRIPTION"] = "搜索結果將包括所有匹配空間，而不論此過濾器。";
$MESS["SOCIALNETWORK_SPACES_LIST_MODE_POPUP_NEW_SPACE_BUTTON"] = "新空間";
$MESS["SOCIALNETWORK_SPACES_LIST_NAME_ALREADY_EXISTS_ERROR"] = "這個名字的空間已經存在。";
$MESS["SOCIALNETWORK_SPACES_LIST_RECENT_SEARCH_LIST_TITLE"] = "最近的搜索";
$MESS["SOCIALNETWORK_SPACES_LIST_SEARCH_INPUT_PLACEHOLDER"] = "找到空間";
$MESS["SOCIALNETWORK_SPACES_LIST_SEARCH_LIST_TITLE"] = "搜索結果";
$MESS["SOCIALNETWORK_SPACES_LIST_SPACE_FOLLOW"] = "跟隨";
$MESS["SOCIALNETWORK_SPACES_LIST_SPACE_OPEN"] = "打開";
$MESS["SOCIALNETWORK_SPACES_LIST_SPACE_PIN"] = "別針";
$MESS["SOCIALNETWORK_SPACES_LIST_SPACE_UNFOLLOW"] = "取消關注";
$MESS["SOCIALNETWORK_SPACES_LIST_SPACE_UNPIN"] = "unvin";
$MESS["SOCIALNETWORK_SPACES_LIST_SPACE_VIEW_MODE_CLOSED_DESCRIPTION"] = "每個人都可以看到。根據要求發送加入邀請。";
$MESS["SOCIALNETWORK_SPACES_LIST_SPACE_VIEW_MODE_CLOSED_TITLE"] = "私人空間";
$MESS["SOCIALNETWORK_SPACES_LIST_SPACE_VIEW_MODE_HINT"] = "您可以隨時更改空間隱私模式。";
$MESS["SOCIALNETWORK_SPACES_LIST_SPACE_VIEW_MODE_OPEN_DESCRIPTION"] = "每個人都可以看到。不需要加入邀請。";
$MESS["SOCIALNETWORK_SPACES_LIST_SPACE_VIEW_MODE_OPEN_TITLE"] = "公共場所";
$MESS["SOCIALNETWORK_SPACES_LIST_SPACE_VIEW_MODE_SECRET_DESCRIPTION"] = "僅對成員可見。需要加入邀請。";
$MESS["SOCIALNETWORK_SPACES_LIST_SPACE_VIEW_MODE_SECRET_TITLE"] = "秘密空間";
$MESS["SOCIALNETWORK_SPACES_TIME"] = "9月16日";
$MESS["SOCIALNETWORK_SPACES_TITLE"] = "空間";
