<?php
$MESS["BLOG_BLOG_BLOG_FRIENDS_ONLY"] = "您沒有足夠的權限來查看此消息。";
$MESS["BLOG_BLOG_BLOG_MES_DELED"] = "消息已成功刪除";
$MESS["BLOG_BLOG_BLOG_MES_DEL_ERROR"] = "錯誤刪除消息";
$MESS["BLOG_BLOG_BLOG_MES_DEL_NO_RIGHTS"] = "您沒有足夠的權限來刪除此消息";
$MESS["BLOG_BLOG_BLOG_MES_HIDED"] = "該帖子被隱藏";
$MESS["BLOG_BLOG_BLOG_MES_HIDE_ERROR"] = "隱藏帖子的錯誤";
$MESS["BLOG_BLOG_BLOG_MES_HIDE_NO_RIGHTS"] = "您無權隱藏此帖子。";
$MESS["BLOG_BLOG_BLOG_NO_BLOG"] = "無法找到博客";
$MESS["BLOG_BLOG_GROUP"] = "團體：";
$MESS["BLOG_BLOG_SESSID_WRONG"] = "您的會議已經過期。請再試一次。";
$MESS["BLOG_BLOG_USER"] = "用戶：";
$MESS["BLOG_MODULE_NOT_INSTALL"] = "沒有安裝博客模塊。";
$MESS["BLOG_SONET_MODULE_NOT_AVAIBLE"] = "該用戶不可用博客。";
$MESS["MESSAGE_COUNT"] = "帖子";
