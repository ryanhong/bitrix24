<?php
$MESS["SONET_EVENTS_VAR"] = "事件ID變量名稱";
$MESS["SONET_LOG_DATE_DAYS"] = "天數";
$MESS["SONET_PATH_TO_GROUP"] = "小組頁面路徑模板";
$MESS["SONET_PATH_TO_USER"] = "用戶配置文件路徑模板";
$MESS["SONET_RSS_TTL"] = "TTL（分鐘）";
$MESS["SONET_VARIABLE_ALIASES"] = "可變別名";
