<?php
$MESS["SONET_C8_ERR_CANT_CREATE"] = "您無權創建新組";
$MESS["SONET_C8_ERR_DESCR"] = "沒有指定組描述";
$MESS["SONET_C8_ERR_NAME"] = "小組標題未指定";
$MESS["SONET_C8_ERR_PERMS"] = "未指定邀請函數";
$MESS["SONET_C8_ERR_SECURITY"] = "您無權編輯組參數";
$MESS["SONET_C8_ERR_SPAM_PERMS"] = "未設置向組發送消息的許可";
$MESS["SONET_C8_ERR_SUBJECT"] = "小組主題未指定";
$MESS["SONET_C8_GROUP_CREATE"] = "創建組";
$MESS["SONET_C8_GROUP_EDIT"] = "編輯組";
$MESS["SONET_C8_IP_ALL"] = "全部用戶";
$MESS["SONET_C8_IP_MOD"] = "小組所有者和主持人";
$MESS["SONET_C8_IP_OWNER"] = "團體所有者";
$MESS["SONET_C8_IP_USER"] = "所有成員";
$MESS["SONET_MODULE_NOT_INSTALL"] = "社交網絡模塊未安裝";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶";
