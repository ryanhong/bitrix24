<?php
$MESS["SONET_GROUP_ID"] = "組ID";
$MESS["SONET_GROUP_VAR"] = "組變量";
$MESS["SONET_PAGE_VAR"] = "頁面變量";
$MESS["SONET_PATH_TO_GROUP"] = "小組頁面路徑模板";
$MESS["SONET_PATH_TO_GROUP_BAN"] = "禁止用戶頁面路徑模板";
$MESS["SONET_PATH_TO_GROUP_BLOG"] = "組博客頁面路徑模板";
$MESS["SONET_PATH_TO_GROUP_CALENDAR"] = "組日曆頁面路徑模板";
$MESS["SONET_PATH_TO_GROUP_EDIT"] = "組設置頁面路徑模板";
$MESS["SONET_PATH_TO_GROUP_FILES"] = "組文件頁面路徑模板";
$MESS["SONET_PATH_TO_GROUP_FORUM"] = "小組論壇頁面路徑模板";
$MESS["SONET_PATH_TO_GROUP_MODS"] = "小組中介器頁面路徑模板";
$MESS["SONET_PATH_TO_GROUP_PHOTO"] = "小組照片頁面路徑模板";
$MESS["SONET_PATH_TO_GROUP_REQUESTS"] = "會員申請頁面路徑模板";
$MESS["SONET_PATH_TO_GROUP_REQUEST_SEARCH"] = "邀請到小組頁面路徑模板";
$MESS["SONET_PATH_TO_GROUP_TASKS"] = "小組任務頁網址模板";
$MESS["SONET_PATH_TO_GROUP_USERS"] = "小組成員頁面路徑模板";
