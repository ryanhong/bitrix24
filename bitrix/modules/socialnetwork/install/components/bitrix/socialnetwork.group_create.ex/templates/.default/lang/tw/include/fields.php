<?php
$MESS["SONET_GCE_T_PARAMETERS_TITLE"] = "工作組參數";
$MESS["SONET_GCE_T_PARAMETERS_TITLE_PROJECT"] = "項目參數";
$MESS["SONET_GCE_T_PARAMETERS_TITLE_SCRUM"] = "Scrum Team參數";
$MESS["SONET_GCE_T_SLIDE_FIELDS_SETTINGS_SWITCHER"] = "擴展參數";
$MESS["SONET_GCE_T_SLIDE_FIELDS_TITLE"] = "關於工作組";
$MESS["SONET_GCE_T_SLIDE_FIELDS_TITLE_PROJECT"] = "關於項目";
$MESS["SONET_GCE_T_SLIDE_FIELDS_TITLE_SCRUM"] = "關於Scrum團隊";
