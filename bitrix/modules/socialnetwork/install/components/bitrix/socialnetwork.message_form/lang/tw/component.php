<?php
$MESS["BLOG_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_C26_NO_TEXT"] = "未指定用戶消息。";
$MESS["SONET_C26_PAGE_TITLE"] = "向用戶發送消息";
$MESS["SONET_C26_PERM_MESS"] = "您無權向該用戶發送消息。";
$MESS["SONET_C26_SELF"] = "您不能向自己發送消息。";
$MESS["SONET_P_USER_NO_USER"] = "找不到用戶。";
