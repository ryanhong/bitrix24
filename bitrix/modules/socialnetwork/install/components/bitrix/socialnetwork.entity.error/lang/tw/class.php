<?php
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_DESCRIPTION_SONET_GROUP"] = "請聯繫工作組所有者或您的BitRix24管理員";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_DESCRIPTION_SUPPORT_BOT"] = "只有管​​理員可以將消息發送到Helpdesk聊天。如果您使用Bitrix24有疑問，請聯繫您的主管或瀏覽我們的廣泛<span ID = \“ sonet-helper-link-error \”> library </span>。";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_DESCRIPTION_USER"] = "請聯繫您的BitRix24管理員";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_TITLE_SONET_GROUP"] = "沒有找到或拒絕工作組";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_TITLE_SUPPORT_BOT"] = "此聊天僅適用於管理員。";
$MESS["SOCIALNETWORK_ENTITY_ERROR_COMPONENT_TITLE_USER"] = "沒有找到或拒絕訪問用戶";
$MESS["SOCIALNETWORK_ENTITY_TITLE_SUPPORT_BOT"] = "Helpdesk聊天";
