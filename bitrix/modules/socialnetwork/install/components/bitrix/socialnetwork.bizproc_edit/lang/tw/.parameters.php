<?php
$MESS["SONET_PAGE_VAR"] = "頁面變量";
$MESS["SONET_PATH_TO_BIZPROC"] = "業務過程路徑模板";
$MESS["SONET_SET_NAVCHAIN"] = "設置麵包屑";
$MESS["SONET_TASK_ID"] = "任務ID";
$MESS["SONET_TASK_VAR"] = "任務變量";
$MESS["SONET_VARIABLE_ALIASES"] = "可變別名";
