<?php
$MESS["SONET_C5_NO_PERMS"] = "您無權查看此組的個人資料。";
$MESS["SONET_C5_PAGE_TITLE"] = "關於工作組";
$MESS["SONET_C5_PAGE_TITLE_PROJECT"] = "關於項目";
$MESS["SONET_C5_PAGE_TITLE_SCRUM"] = "關於Scrum團隊";
$MESS["SONET_C5_TITLE"] = "輪廓";
$MESS["SONET_C6_BLOG_T"] = "報告";
$MESS["SONET_C6_FORUM_T"] = "討論";
$MESS["SONET_C6_NAME_TEMPLATE_DEFAULT"] = "＃NOBR ##名稱＃#last_name ##/nobr＃";
$MESS["SONET_C6_TASKS_T"] = "任務";
$MESS["SONET_MODULE_NOT_INSTALL"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_P_USER_NO_GROUP"] = "找不到小組。";
