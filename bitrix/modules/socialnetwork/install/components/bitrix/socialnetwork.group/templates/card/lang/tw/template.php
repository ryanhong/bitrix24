<?php
$MESS["INTASK_T2456_UPDATES"] = "更新";
$MESS["SONET_C6_ACT_BAN"] = "黑名單";
$MESS["SONET_C6_ACT_DELETE"] = "刪除工作組";
$MESS["SONET_C6_ACT_EDIT"] = "編輯工作組";
$MESS["SONET_C6_ACT_EXIT"] = "離開工作組";
$MESS["SONET_C6_ACT_JOIN"] = "加入工作組";
$MESS["SONET_C6_ACT_MOD"] = "編輯";
$MESS["SONET_C6_ACT_MOD1"] = "主持人";
$MESS["SONET_C6_ACT_REQU"] = "邀請工作組";
$MESS["SONET_C6_ACT_SUBSCRIBE"] = "訂閱";
$MESS["SONET_C6_ACT_USER"] = "編輯成員";
$MESS["SONET_C6_ACT_USER1"] = "成員";
$MESS["SONET_C6_ACT_VREQU"] = "查看加入請求";
$MESS["SONET_C6_ACT_VREQU_OUT"] = "工作組邀請";
$MESS["SONET_C6_AL_USERS"] = "所有成員";
$MESS["SONET_C6_BLOG_T"] = "消息";
$MESS["SONET_C6_CARD_ACTION_LINK_EDIT"] = "編輯";
$MESS["SONET_C6_CARD_CHANGE_THEME"] = "改變主題";
$MESS["SONET_C6_CARD_EFFICIENCY_HELPER_CAPTION"] = "它是如何工作的？";
$MESS["SONET_C6_CARD_EFFICIENCY_SECTION_TITLE"] = "表現";
$MESS["SONET_C6_CARD_EFFICIENCY_SECTION_TITLE_PROJECT"] = "項目性能";
$MESS["SONET_C6_CARD_FAVORITES_N"] = "添加到收藏夾";
$MESS["SONET_C6_CARD_FAVORITES_Y"] = "從收藏夾中刪除";
$MESS["SONET_C6_CARD_MEMBERS"] = "成員";
$MESS["SONET_C6_CARD_MEMBERS_SECTION_TITLE"] = "工作組團隊";
$MESS["SONET_C6_CARD_MEMBERS_SECTION_TITLE_PROJECT"] = "項目團隊";
$MESS["SONET_C6_CARD_MEMBERS_SECTION_TITLE_SCRUM"] = "利益相關者";
$MESS["SONET_C6_CARD_MEMBERS_TITLE"] = "工作組成員";
$MESS["SONET_C6_CARD_MEMBERS_TITLE_PROJECT"] = "項目成員";
$MESS["SONET_C6_CARD_MEMBERS_TITLE_SCRUM"] = "Scrum團隊成員";
$MESS["SONET_C6_CARD_MENU_ABOUT"] = "關於工作組";
$MESS["SONET_C6_CARD_MENU_ABOUT_PROJECT"] = "關於項目";
$MESS["SONET_C6_CARD_MENU_ABOUT_SCRUM"] = "關於Scrum";
$MESS["SONET_C6_CARD_MENU_MEMBERS"] = "成員";
$MESS["SONET_C6_CARD_MENU_ROLES"] = "角色和權限";
$MESS["SONET_C6_CARD_MOD"] = "主持人";
$MESS["SONET_C6_CARD_MODERATORS_SECTION_TITLE"] = "工作組主持人";
$MESS["SONET_C6_CARD_MODERATORS_SECTION_TITLE_PROJECT"] = "項目主持人";
$MESS["SONET_C6_CARD_MODERATORS_SECTION_TITLE_SCRUM"] = "開發團隊";
$MESS["SONET_C6_CARD_MOD_PROJECT"] = "項目助理";
$MESS["SONET_C6_CARD_MOD_SCRUM_PROJECT"] = "開發團隊";
$MESS["SONET_C6_CARD_OWNER"] = "所有者";
$MESS["SONET_C6_CARD_OWNER_PROJECT"] = "項目擁有者";
$MESS["SONET_C6_CARD_OWNER_SECTION_TITLE"] = "工作組所有者";
$MESS["SONET_C6_CARD_OWNER_SECTION_TITLE_PROJECT"] = "項目擁有者";
$MESS["SONET_C6_CARD_OWNER_SECTION_TITLE_SCRUM"] = "產品擁有者";
$MESS["SONET_C6_CARD_PROJECT_TERM_SECTION_TITLE"] = "項目日期";
$MESS["SONET_C6_CARD_PROPERTIES_SECTION_TITLE"] = "工作組屬性";
$MESS["SONET_C6_CARD_PROPERTIES_SECTION_TITLE_PROJECT"] = "項目屬性";
$MESS["SONET_C6_CARD_PROPERTIES_SECTION_TITLE_SCRUM"] = "Scrum屬性";
$MESS["SONET_C6_CARD_SCRUMMASTER_TITLE"] = "Scrum Master";
$MESS["SONET_C6_CARD_SCRUM_TASK_RESPONSIBLE_TITLE"] = "默認負責人";
$MESS["SONET_C6_CARD_SCRUM_TASK_RESPONSIBLE_VALUE_A"] = "由...製作";
$MESS["SONET_C6_CARD_SCRUM_TASK_RESPONSIBLE_VALUE_M"] = "Scrum Master";
$MESS["SONET_C6_CARD_SUBJECT"] = "話題";
$MESS["SONET_C6_CARD_WAIT"] = "請等待＆hellip;";
$MESS["SONET_C6_CREATED"] = "創建於";
$MESS["SONET_C6_DESCR"] = "描述";
$MESS["SONET_C6_FORUM_T"] = "論壇";
$MESS["SONET_C6_MODS_TITLE"] = "主持人";
$MESS["SONET_C6_NMEM"] = "成員";
$MESS["SONET_C6_NO_MODS"] = "沒有主持人";
$MESS["SONET_C6_NO_USERS"] = "沒有成員";
$MESS["SONET_C6_PHOTO_T"] = "相片";
$MESS["SONET_C6_PROJECT_DATE_FINISH"] = "結束日期";
$MESS["SONET_C6_PROJECT_DATE_START"] = "開始日期";
$MESS["SONET_C6_TAGS"] = "標籤";
$MESS["SONET_C6_TOPIC"] = "話題";
$MESS["SONET_C6_TYPE"] = "工作組類型";
$MESS["SONET_C6_TYPE_O1"] = "這是一個公共團體。任何人都可以加入。";
$MESS["SONET_C6_TYPE_O2"] = "這是一個私人團體。會員資格需要所有者批准。";
$MESS["SONET_C6_TYPE_V1"] = "任何人都可以看到這個小組。";
$MESS["SONET_C6_TYPE_V2"] = "這是一個私人工作組。僅工作組成員可以看到。";
$MESS["SONET_C6_USERS_T"] = "成員";
$MESS["SONET_C39_ARCHIVE_GROUP"] = "存檔工作組";
$MESS["SONET_C39_SEND_MESSAGE_GROUP"] = "發信息";
$MESS["SONET_C39_SEND_MESSAGE_GROUP_TITLE"] = "向工作組成員發送消息";
