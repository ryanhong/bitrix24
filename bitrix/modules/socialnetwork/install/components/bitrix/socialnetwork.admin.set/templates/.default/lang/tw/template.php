<?php
$MESS["SONET_SAS_T_ADMIN_OFF"] = "在管理員模式下，您可以查看和編輯限制數據。";
$MESS["SONET_SAS_T_ADMIN_ON"] = "您當前處於社交網絡管理員模式，可以查看和編輯限制數據。";
$MESS["SONET_SAS_T_ADMIN_SET"] = "輸入管理員模式";
$MESS["SONET_SAS_T_ADMIN_UNSET"] = "退出管理員模式";
$MESS["SONET_SAS_T_BANNER_CLOSE"] = "關閉";
$MESS["SONET_SAS_T_MODULE_NOT_INSTALLED"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_SAS_T_NOT_ADMIN"] = "當前用戶沒有社交網絡管理員權限。";
$MESS["SONET_SAS_T_NOT_ATHORIZED"] = "你沒有登錄。";
$MESS["SONET_SAS_T_SESSION_WRONG"] = "您的會議已經過期。請再試一次..";
$MESS["SONET_SAS_T_WAIT"] = "請等待＆hellip;";
