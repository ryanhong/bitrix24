<?php
$MESS["SONET_GRE_T_ERROR"] = "錯誤處理請求。";
$MESS["SONET_GUE_T_ACTIONLINK_ADD"] = "添加";
$MESS["SONET_GUE_T_ACTIONLINK_CHANGE"] = "改變";
$MESS["SONET_GUE_T_ACTIONLINK_INVITE"] = "邀請";
$MESS["SONET_GUE_T_ACTIONS_TITLE"] = "動作";
$MESS["SONET_GUE_T_ACTION_ADDTOMODERATORS"] = "設置為主持人";
$MESS["SONET_GUE_T_ACTION_ADDTOMODERATORS_PROJECT"] = "設置為主持人";
$MESS["SONET_GUE_T_ACTION_ADDTOUSERS"] = "邀請用戶";
$MESS["SONET_GUE_T_ACTION_EXCLUDEFROMGROUP"] = "從組中刪除";
$MESS["SONET_GUE_T_ACTION_EXCLUDEFROMGROUP_CONFIRM"] = "您確定要從工作組中刪除這些用戶嗎？";
$MESS["SONET_GUE_T_ACTION_EXCLUDEFROMGROUP_CONFIRM_PROJECT"] = "您確定要從此工作組中刪除這些用戶嗎？";
$MESS["SONET_GUE_T_ACTION_EXCLUDEFROMGROUP_PROJECT"] = "解僱";
$MESS["SONET_GUE_T_ACTION_EXCLUDEFROMMODERATORS"] = "從主持人中排除";
$MESS["SONET_GUE_T_ACTION_EXCLUDEFROMMODERATORS_PROJECT"] = "從主持人中排除";
$MESS["SONET_GUE_T_ACTION_FAILED"] = "操作返回的結果不正確：＃錯誤＃";
$MESS["SONET_GUE_T_ACTION_SETGROUPOWNER"] = "更改所有者";
$MESS["SONET_GUE_T_ACTION_SETGROUPOWNER_CONFIRM"] = "您確定要更改工作組所有者嗎？";
$MESS["SONET_GUE_T_ACTION_SETGROUPOWNER_CONFIRM_PROJECT"] = "您確定要更改項目所有者嗎？";
$MESS["SONET_GUE_T_ACTION_SETGROUPOWNER_PROJECT"] = "更改所有者";
$MESS["SONET_GUE_T_ACTION_UNBANFROMGROUP"] = "Unban";
$MESS["SONET_GUE_T_ACTION_UNCONNECT_DEPT"] = "斷開";
$MESS["SONET_GUE_T_BAN_SUBTITLE"] = "殘疾用戶";
$MESS["SONET_GUE_T_BUTTON_EXCLUDE"] = "消除";
$MESS["SONET_GUE_T_BUTTON_REMOVEMOD"] = "消除";
$MESS["SONET_GUE_T_BUTTON_UNBAN"] = "Unban";
$MESS["SONET_GUE_T_BUTTON_UNCONNECT"] = "斷開";
$MESS["SONET_GUE_T_CANT_EXCLUDE_AUTO_MEMBER"] = "連接的用戶不能從組中刪除。";
$MESS["SONET_GUE_T_CANT_EXCLUDE_AUTO_MEMBER_PROJECT"] = "您無法從項目中刪除已連接的用戶。";
$MESS["SONET_GUE_T_CANT_EXCLUDE_SCRUM_MASTER"] = "Scrum Master不能被解僱。";
$MESS["SONET_GUE_T_DEPARTMENTS_SUBTITLE"] = "部門";
$MESS["SONET_GUE_T_DEPARTMENT_ID_NOT_DEFINED"] = "未選擇部門。";
$MESS["SONET_GUE_T_FIRED2"] = "被解僱";
$MESS["SONET_GUE_T_FIRED2_F"] = "被解僱";
$MESS["SONET_GUE_T_FIRED2_M"] = "被解僱";
$MESS["SONET_GUE_T_GROUP_ID_NOT_DEFINED"] = "找不到工作組。";
$MESS["SONET_GUE_T_MODS_SUBTITLE"] = "主持人";
$MESS["SONET_GUE_T_MODS_SUBTITLE_PROJECT"] = "所有者和主持人";
$MESS["SONET_GUE_T_MODS_SUBTITLE_SCRUM_PROJECT2"] = "Scrum大師和團隊";
$MESS["SONET_GUE_T_MODULE_NOT_INSTALLED"] = "沒有安裝社交網絡模塊。";
$MESS["SONET_GUE_T_NOT_ATHORIZED"] = "你沒有登錄。";
$MESS["SONET_GUE_T_NO_PERMS"] = "您無權管理此工作組。";
$MESS["SONET_GUE_T_NO_PERMS_PROJECT"] = "您無權管理此項目。";
$MESS["SONET_GUE_T_OWNER"] = "所有者";
$MESS["SONET_GUE_T_OWNER_CANT_EXCLUDE_HIMSELF"] = "所有者無法將自己從小組中刪除。";
$MESS["SONET_GUE_T_OWNER_CANT_EXCLUDE_HIMSELF_PROJECT"] = "所有者無法將自己從項目中刪除。";
$MESS["SONET_GUE_T_OWNER_PROJECT"] = "所有者";
$MESS["SONET_GUE_T_OWNER_SCRUM"] = "產品擁有者";
$MESS["SONET_GUE_T_OWNER_SUBTITLE"] = "所有者";
$MESS["SONET_GUE_T_OWNER_SUBTITLE_PROJECT"] = "項目擁有者";
$MESS["SONET_GUE_T_OWNER_SUBTITLE_SCRUM"] = "產品擁有者";
$MESS["SONET_GUE_T_SAME_OWNER"] = "指定的用戶已經是工作組所有者。";
$MESS["SONET_GUE_T_SAME_OWNER_PROJECT"] = "指定的用戶已經是項目所有者。";
$MESS["SONET_GUE_T_SCRUM_MASTER"] = "Scrum Master";
$MESS["SONET_GUE_T_SESSION_WRONG"] = "您的會議已經過期。請再試一次。";
$MESS["SONET_GUE_T_STEPPER_TITLE"] = "同步用戶";
$MESS["SONET_GUE_T_USERS_AUTO_SUBTITLE"] = "連接的用戶";
$MESS["SONET_GUE_T_USERS_AUTO_SUBTITLE_HINT"] = "部門的員工綁定到工作組。這些員工會自動成為工作組成員，並且只有在將其部門從工作組中刪除後才可以離開工作組。";
$MESS["SONET_GUE_T_USERS_AUTO_SUBTITLE_HINT_PROJECT"] = "部門的員工約束了該項目。這些員工會自動成為項目成員，只有在將其部門從項目中刪除後才可以離開項目。";
$MESS["SONET_GUE_T_USERS_SUBTITLE"] = "成員";
$MESS["SONET_GUE_T_USERS_SUBTITLE_PROJECT"] = "參與的人";
$MESS["SONET_GUE_T_USERS_SUBTITLE_SCRUM"] = "所有參與者";
$MESS["SONET_GUE_T_USER_ID_INCORRECT"] = "選擇一個用戶。";
$MESS["SONET_GUE_T_USER_ID_NOT_DEFINED"] = "沒有選擇用戶。";
