<?php
$MESS["SONET_DATE_TIME_FORMAT"] = "日期和時間格式";
$MESS["SONET_EDITABLE_FIELDS"] = "可編輯的屬性";
$MESS["SONET_ID"] = "用戶身份";
$MESS["SONET_NAME_TEMPLATE"] = "名稱格式";
$MESS["SONET_PAGE_VAR"] = "頁面變量";
$MESS["SONET_PATH_TO_SEARCH_EXTERNAL"] = "用戶搜索表格的外部路徑";
$MESS["SONET_PATH_TO_USER"] = "用戶配置文件路徑模板";
$MESS["SONET_PATH_TO_USER_EDIT"] = "用戶配置文件頁面路徑模板";
$MESS["SONET_SET_NAVCHAIN"] = "設置麵包屑";
$MESS["SONET_SHOW_LOGIN"] = "顯示登錄名，如果沒有必需的用戶名字段";
$MESS["SONET_USER_VAR"] = "用戶變量";
$MESS["SONET_VARIABLE_ALIASES"] = "可變別名";
