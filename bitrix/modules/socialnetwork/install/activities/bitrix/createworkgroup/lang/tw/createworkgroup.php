<?php
$MESS["BPCWG_EMPTY_GROUP_NAME"] = "組名稱丟失。";
$MESS["BPCWG_EMPTY_OWNER"] = "小組所有者缺少。";
$MESS["BPCWG_EMPTY_USERS"] = "小組成員未指定。";
$MESS["BPCWG_ERROR_CREATE_GROUP"] = "錯誤創建組。";
$MESS["BPCWG_ERROR_SUBJECT_ID"] = "無法獲得主題列表。";
$MESS["BPCWG_FIELD_REQUIED"] = "需要字段'＃字段＃'。";
