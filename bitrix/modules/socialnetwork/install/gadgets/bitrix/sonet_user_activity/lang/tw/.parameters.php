<?php
$MESS["GD_ACTIVITY_P_EVENT_ID"] = "消息類型";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_ALL"] = "全部";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_BLOG"] = "博客/報告";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_CALENDAR"] = "日曆";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_FILES"] = "文件";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_FORUM"] = "論壇/討論";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_PHOTO"] = "照片";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_SYSTEM"] = "系統";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_SYSTEM_FRIENDS"] = "朋友們";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_SYSTEM_GROUPS"] = "組";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_TASKS"] = "任務";
$MESS["GD_ACTIVITY_P_LOG_CNT"] = "顯示的項目數";
