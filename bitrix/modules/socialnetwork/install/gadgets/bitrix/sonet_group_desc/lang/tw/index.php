<?php
$MESS["GD_SONET_GROUP_DESC_ARCHIVE"] = "檔案集團";
$MESS["GD_SONET_GROUP_DESC_CREATED"] = "創建";
$MESS["GD_SONET_GROUP_DESC_DESCRIPTION"] = "描述";
$MESS["GD_SONET_GROUP_DESC_NAME"] = "團體";
$MESS["GD_SONET_GROUP_DESC_NMEM"] = "成員";
$MESS["GD_SONET_GROUP_DESC_REQUEST_SENT_MESSAGE"] = "您已經發送了加入此工作組的請求。";
$MESS["GD_SONET_GROUP_DESC_REQUEST_SENT_MESSAGE_BY_GROUP"] = "加入此工作組的邀請已經發送給您。您可以通過<a href='#link#'>邀請頁面確認會員資格</a>。";
$MESS["GD_SONET_GROUP_DESC_SUBJECT_NAME"] = "話題";
$MESS["GD_SONET_GROUP_DESC_TYPE"] = "組類型";
$MESS["GD_SONET_GROUP_DESC_TYPE_O1"] = "這是一個公共團體。任何人都可以加入。";
$MESS["GD_SONET_GROUP_DESC_TYPE_O2"] = "這是一個私人團體。用戶會員資格受管理員批准的約束。";
$MESS["GD_SONET_GROUP_DESC_TYPE_V1"] = "任何人都可以看到這個小組。";
$MESS["GD_SONET_GROUP_DESC_TYPE_V2"] = "這個組是看不見的。只有小組成員才能看到它。";
