<?php
$MESS["WS_GADGET"] = "Vista側欄小工具";
$MESS["WS_GADGET_ALT"] = "Vista側欄小工具";
$MESS["WS_GADGET_DESCR"] = "Windows Vista用戶可以下載特殊的Vista側欄小工具，用於顯示網站的摘要統計信息，並將其安裝在側邊欄或桌面上。";
$MESS["WS_GADGET_LINK"] = "下載Vista側欄小工具";
