<?php
$MESS["WEBS_INSTALL_TITLE"] = "安裝Web服務模塊";
$MESS["WEBS_MODULE_DESCRIPTION"] = "提供實施和使用Web服務和SOAP的功能。";
$MESS["WEBS_MODULE_NAME"] = "網頁服務";
$MESS["WEBS_UNINSTALL_TITLE"] = "卸載Web服務模塊";
$MESS["WS_GADGET_DESCR"] = "Windows Vista用戶可以下載特殊的Vista側欄小工具，用於顯示網站的摘要統計信息，並將其安裝在側邊欄或桌面上。";
$MESS["WS_GADGET_LINK"] = "下載Vista側欄小工具";
