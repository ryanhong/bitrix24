<?php
$MESS["COMMENTS_EXTRANET_GROUP_NAME"] = "評論：Extranet";
$MESS["GROUPS_AND_USERS_FILES_COMMENTS_EXTRANET_NAME"] = "對用戶文件的評論：extranet";
$MESS["GROUPS_AND_USERS_PHOTOGALLERY_COMMENTS_EXTRANET_NAME"] = "關於用戶和小組照片庫的評論：Extranet";
$MESS["GROUPS_AND_USERS_TASKS_COMMENTS_EXTRANET_NAME"] = "對用戶和組任務的評論：Extranet";
$MESS["HIDDEN_EXTRANET_GROUP_NAME"] = "隱藏論壇：extranet";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_DESCRIPTION"] = "臨時站點的個人用戶和組的論壇。";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_NAME"] = "用戶和小組論壇：Extranet";
