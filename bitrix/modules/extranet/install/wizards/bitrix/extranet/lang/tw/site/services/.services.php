<?php
$MESS["SERVICE_BLOG"] = "部落格";
$MESS["SERVICE_FILEMAN"] = "網站資源管理器";
$MESS["SERVICE_FILES"] = "Extranet站點文件";
$MESS["SERVICE_FORM"] = "網絡表格";
$MESS["SERVICE_FORUM"] = "論壇";
$MESS["SERVICE_INTRANET"] = "Intranet門戶設置";
$MESS["SERVICE_LEARNING"] = "電子學習";
$MESS["SERVICE_MAIN_SETTINGS"] = "Extranet站點設置";
$MESS["SERVICE_PHOTOGALLERY"] = "照片庫";
$MESS["SERVICE_SEARCH"] = "搜尋";
$MESS["SERVICE_SOCIALNETWORK"] = "社交網絡";
$MESS["SERVICE_STATISTIC"] = "統計數據";
$MESS["SERVICE_USERS"] = "用戶";
$MESS["SERVICE_VOTE"] = "民意調查";
$MESS["SERVICE_WORKFLOW"] = "工作流程";
