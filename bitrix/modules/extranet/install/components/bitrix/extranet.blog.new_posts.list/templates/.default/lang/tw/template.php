<?php
$MESS["BLOG_BLOG_BLOG_CATEGORY"] = "標籤：";
$MESS["BLOG_BLOG_BLOG_CATEGORY_FILTER"] = "<div class = \“ extranet-blog-tag-title \”>帖子標記：<div class = \“ extranet-blog-tag \”>＃類別＃</div> </div> </div> </ div>";
$MESS["BLOG_BLOG_BLOG_COMMENTS"] = "評論：";
$MESS["BLOG_BLOG_BLOG_MORE"] = "更多的...";
$MESS["BLOG_BLOG_BLOG_NO_AVAIBLE_MES"] = "沒有可用的消息";
$MESS["BLOG_BLOG_BLOG_PERMALINK"] = "永久地址";
$MESS["BLOG_BLOG_BLOG_VIEWS"] = "視圖：";
$MESS["BLOG_MES_DELETE_POST_CONFIRM"] = "您確定要刪除這篇文章嗎？";
