<?php
$MESS["EXTRANET_INVITATION_DESC"] = "＃校大＃-總成本
＃USER_ID＃ - 用戶ID
＃電子郵件＃-用戶的電子郵件
＃site_name＃ - 網站名稱
＃server_name＃ - 網站URL（沒有http：//）
";
$MESS["EXTRANET_INVITATION_MESSAGE"] = "來自＃site_name＃的通知
-------------------------------------------------- --------

您已被邀請到該網站。要確認您的註冊並設置密碼，請單擊此處：
http：//＃server_name＃/extranet/enfact/？chacpword =＃chypword＃＆user_id =＃user_id＃＃

＃USER_TEXT＃

此消息已自動生成。
";
$MESS["EXTRANET_INVITATION_NAME"] = "新網站用戶邀請";
$MESS["EXTRANET_INVITATION_SUBJECT"] = "您已被邀請到Bitrix24";
$MESS["EXTRANET_WG_FROM_ARCHIVE_DESC"] = "＃WG_ID＃ - 工作組ID
＃wg_name＃ - 工作組名稱
＃會員_Email＃-Workgroup成員的電子郵件
＃site_name＃ - 網站名稱
";
$MESS["EXTRANET_WG_FROM_ARCHIVE_MESSAGE"] = "＃site_name＃上的工作組'＃wg_name＃'[＃wg_id＃]是未體現的。
-------------------------------------------------- --------------------------- ----------------------- ---------------

該工作組不再僅閱讀，可以修改。

單擊以下鏈接以查看工作組：
http：//＃server_name＃/extranet/workgroups/group/＃wg_id＃/

此消息已自動生成。
";
$MESS["EXTRANET_WG_FROM_ARCHIVE_NAME"] = "工作組是不合理的。";
$MESS["EXTRANET_WG_FROM_ARCHIVE_SUBJECT"] = "＃site_name＃：工作組'＃wg_name＃'是未體現的。";
$MESS["EXTRANET_WG_TO_ARCHIVE_DESC"] = "＃WG_ID＃ - 工作組ID
＃wg_name＃ - 工作組名稱
＃會員_Email＃-Workgroup成員的電子郵件
＃site_name＃ - 網站名稱
";
$MESS["EXTRANET_WG_TO_ARCHIVE_MESSAGE"] = "＃site_name＃上的工作組'＃wg_name＃'[＃wg_id＃]已存檔。
-------------------------------------------------- --------------------------- ----------------------- ---------------

該工作組現在僅閱讀。

單擊以下鏈接以查看工作組：
http：//＃server_name＃/extranet/workgroups/group/＃wg_id＃/

此消息已自動創建。
";
$MESS["EXTRANET_WG_TO_ARCHIVE_NAME"] = "工作組已存檔。";
$MESS["EXTRANET_WG_TO_ARCHIVE_SUBJECT"] = "＃site_name＃：工作組'＃wg_name＃'已存檔。";
