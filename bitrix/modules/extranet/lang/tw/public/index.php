<?php
$MESS["EXTRANET_INDEX_PAGE_TEXT1"] = "Extranet是與合作夥伴，客戶，客戶，承包商，分銷商和其他外部用戶進行安全合作的私人領域，可以授予與公司人員進行溝通的訪問。";
$MESS["EXTRANET_INDEX_PAGE_TITLE"] = "歡迎來到Extranet網站";
