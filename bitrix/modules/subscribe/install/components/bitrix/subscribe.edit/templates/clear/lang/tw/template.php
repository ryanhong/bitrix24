<?php
$MESS["CT_BSE_AUTH_ERR"] = "您必須登錄才能管理訂閱。";
$MESS["CT_BSE_BTN_ADD_SUBSCRIPTION"] = "訂閱";
$MESS["CT_BSE_BTN_CONF"] = "確認訂閱";
$MESS["CT_BSE_BTN_EDIT_SUBSCRIPTION"] = "編輯訂閱";
$MESS["CT_BSE_BTN_SEND"] = "發送確認代碼";
$MESS["CT_BSE_CONFIRMATION"] = "驗證碼";
$MESS["CT_BSE_CONF_NOTE"] = "您沒有確認您的訂閱。輸入確認代碼以確認。";
$MESS["CT_BSE_EMAIL"] = "您的訂閱電子郵件";
$MESS["CT_BSE_EMAIL_LABEL"] = "電子郵件：";
$MESS["CT_BSE_EXIST_NOTE"] = "要取消訂閱，請取消選中類別旁邊的框（ES），然後單擊\“編輯訂閱\”。";
$MESS["CT_BSE_FORMAT_HTML"] = "html";
$MESS["CT_BSE_FORMAT_LABEL"] = "消息格式：";
$MESS["CT_BSE_FORMAT_TEXT"] = "文字";
$MESS["CT_BSE_NEW_NOTE"] = "保存訂閱後，將立即發送訂閱確認鏈接。";
$MESS["CT_BSE_RUBRIC_LABEL"] = "主題：";
$MESS["CT_BSE_SEND_NOTE"] = "如果您已經訂閱並想更改訂閱參數或退訂，請輸入訂閱電子郵件地址。";
$MESS["CT_BSE_SUBSCRIPTION_FORM_TITLE"] = "訂閱";
