<?php
$MESS["CC_BSS_DELETE_ERROR"] = "刪除訂閱時發生錯誤。請聯繫管理員。";
$MESS["CC_BSS_MODULE_NOT_INSTALLED"] = "通訊模塊當前不可用。";
$MESS["CC_BSS_NOT_AUTHORIZED"] = "您沒有權限。請授權管理您的訂閱設置。";
$MESS["CC_BSS_TITLE"] = "訂閱類別";
$MESS["CC_BSS_UPDATE_SUCCESS"] = "訂閱成功改變了。";
