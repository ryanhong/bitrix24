<?php
$MESS["POST_HTML"] = "html";
$MESS["POST_STATUS_DRAFT"] = "草稿";
$MESS["POST_STATUS_ERROR"] = "發送錯誤";
$MESS["POST_STATUS_PART"] = "進行中";
$MESS["POST_STATUS_SENT"] = "發送";
$MESS["POST_STATUS_WAIT"] = "暫停";
$MESS["POST_TEXT"] = "文字";
$MESS["POST_WRONG_AUTO_FROM"] = "在\“ from \”字段中輸入有效的發送時間";
$MESS["POST_WRONG_AUTO_TILL"] = "在\“ till \”字段中輸入有效的發送時間";
$MESS["POST_WRONG_DATE_SENT_FROM"] = "輸入有效發送的日期\“來自\”字段";
$MESS["POST_WRONG_DATE_SENT_TILL"] = "輸入有效發送的日期在\“ till \”字段中";
$MESS["POST_WRONG_TIMESTAMP_FROM"] = "輸入正確的時間戳\“來自\”";
$MESS["POST_WRONG_TIMESTAMP_TILL"] = "輸入正確的時間戳\“ thil \”";
