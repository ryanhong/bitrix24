<?php
$MESS["POST_EMAIL"] = "電子郵件";
$MESS["POST_GROUP_ACTION_DELETE"] = "刪除訂閱";
$MESS["POST_GROUP_ACTION_INACTIVE"] = "停用訂閱";
$MESS["POST_STATUS_ID"] = "地位";
$MESS["POST_STATUS_ID_ERROR"] = "新聞通訊發送失敗。";
$MESS["POST_STATUS_ID_SUCCESS"] = "新聞通訊已成功發送。";
$MESS["POST_SUBSCRIPTION_ID"] = "訂閱";
$MESS["POST_SUBSCR_DELETED"] = "刪除";
$MESS["POST_SUBSCR_INACTIVE"] = "不活動";
$MESS["POST_USER_ID"] = "用戶";
$MESS["post_title"] = "新聞通訊收件人";
$MESS["post_total"] = "總地址：";
