<?php
$MESS["MAIN_ADD"] = "添加";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "檢查：";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "刪除";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "選擇：";
$MESS["MAIN_ALL"] = "（全部）";
$MESS["POST_ADD_TITLE"] = "點擊添加新問題";
$MESS["POST_ADM_BTN_STOP"] = "停止";
$MESS["POST_ADM_POST_NOT_FOUND"] = "沒有找到新聞通訊問題。";
$MESS["POST_ADM_SENDING_NOTE_LINE1"] = "發送時事通訊正在進行中。";
$MESS["POST_ADM_SENDING_NOTE_LINE2"] = "在過程完成之前，請不要離開此頁面。";
$MESS["POST_ADM_WITH_ERRORS"] = "有錯誤";
$MESS["POST_FIND"] = "尋找";
$MESS["POST_FIND_TITLE"] = "輸入搜索查詢";
$MESS["POST_FROM_TILL_DATE_AUTOSEND"] = "自動發送的\“ till \”日期必須大於\“ \”日期";
$MESS["POST_FROM_TILL_DATE_SENT"] = "發送的日期\“來自\”必須大於\ \“ thil \”";
$MESS["POST_FROM_TILL_TIMESTAMP"] = "修改日期\“ till \”必須大於\ \“";
$MESS["POST_F_AUTO_SEND_TIME"] = "計劃發送時間";
$MESS["POST_F_BODY"] = "訊息";
$MESS["POST_F_BODY_TYPE"] = "消息類型";
$MESS["POST_F_DATE_SENT"] = "發送的日期";
$MESS["POST_F_FROM"] = "從";
$MESS["POST_F_ID"] = "ID";
$MESS["POST_F_RUBRIC"] = "新聞通訊類別";
$MESS["POST_F_STATUS"] = "地位";
$MESS["POST_F_SUBJECT"] = "主題";
$MESS["POST_F_TIMESTAMP"] = "修改日期";
$MESS["POST_F_TO"] = "已經發送到";
$MESS["POST_SHOW_LIST"] = "查看地址列表";
$MESS["POST_WRONG_DATE_AUTOSEND_FROM"] = "請在過濾器中輸入有效\“從\”日期進行自動發送";
$MESS["POST_WRONG_DATE_AUTOSEND_TILL"] = "請在過濾器中輸入有效的\“ till \”日期以進行自動發送";
$MESS["POST_WRONG_DATE_SENT_FROM"] = "輸入正確發送的日期\“來自\”";
$MESS["POST_WRONG_DATE_SENT_TILL"] = "輸入正確發送的日期\“ till \ \”";
$MESS["POST_WRONG_TIMESTAMP_FROM"] = "輸入正確的修改日期\“來自\”";
$MESS["POST_WRONG_TIMESTAMP_TILL"] = "輸入正確的修改日期\“ till \”";
$MESS["post_act_del"] = "刪除";
$MESS["post_act_del_conf"] = "此操作無法撤消。反正刪除問題？";
$MESS["post_act_edit"] = "調整";
$MESS["post_act_send"] = "發送";
$MESS["post_body_type"] = "類型";
$MESS["post_conf"] = "發送新聞通訊？";
$MESS["post_del_err"] = "無法刪除新聞通訊。";
$MESS["post_from"] = "從";
$MESS["post_nav"] = "問題";
$MESS["post_report"] = "發送報告";
$MESS["post_save_err"] = "保存問題的錯誤＃";
$MESS["post_send_ok"] = "通訊已成功發送。";
$MESS["post_sent"] = "發送";
$MESS["post_stat"] = "地位";
$MESS["post_subj"] = "主題";
$MESS["post_title"] = "新聞通訊問題";
$MESS["post_to"] = "到";
$MESS["post_updated"] = "修改的";
$MESS["posting_addr_of"] = "從";
$MESS["posting_addr_processed"] = "處理的地址：";
$MESS["posting_agent_submitted"] = "有一個用於發送新聞通訊的代理功能。";
$MESS["posting_continue_act"] = "繼續";
$MESS["posting_continue_button"] = "繼續";
$MESS["posting_continue_conf"] = "您確定要繼續發送新聞通訊嗎？";
$MESS["posting_copy_link"] = "複製";
$MESS["posting_cron_setup"] = "新聞通訊問題計劃自動發送。您為此操作設置了CRON選項。不要忘記配置它。";
$MESS["posting_stop_act"] = "暫停";
$MESS["posting_stop_conf"] = "您確定要暫停發送新聞通訊嗎？";
