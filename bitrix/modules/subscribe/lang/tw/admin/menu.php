<?php
$MESS["mnu_posting"] = "新聞通訊問題";
$MESS["mnu_posting_alt"] = "新聞通訊管理";
$MESS["mnu_rub"] = "新聞通訊類別";
$MESS["mnu_rub_alt"] = "管理新聞通訊類別";
$MESS["mnu_sect"] = "新聞通訊";
$MESS["mnu_sect_title"] = "新聞通訊訂閱和訂戶管理";
$MESS["mnu_subscr"] = "訂戶";
$MESS["mnu_subscr_alt"] = "管理訂戶的電子郵件地址";
$MESS["mnu_subscr_import"] = "導入地址";
$MESS["mnu_subscr_import_alt"] = "從文件或剪貼板導入訂閱地址";
