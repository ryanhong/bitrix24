<?php
$MESS["SUBSCRIBE_CONFIRM_DESC"] = "＃id＃ - 訂閱ID
＃電子郵件＃-訂閱電子郵件
＃juccess_code＃ - 確認代碼
＃subscr_section＃ - 帶有訂閱編輯頁面的部分（在設置中指定）
＃user_name＃-subscriber的名稱（可選）
＃date_subscr＃ - 添加/更改地址的日期
";
$MESS["SUBSCRIBE_CONFIRM_MESSAGE"] = "來自＃site_name＃的信息消息
-------------------------------------------------- -----

你好，

您之所以收到此消息，是因為從＃server_name＃向您的新聞地址提出了訂閱請求。

這是有關您訂閱的詳細信息：

訂閱電子郵件..............＃電子郵件＃
電子郵件的日期添加/編輯....＃date_subscr＃

您的確認代碼：＃enckern_code＃

請單擊此信中提供的鏈接以確認您的訂閱。
http：//#server_name##subscr_section#subscr_edit.php？id =＃id＃＆escresenge_code =＃juccess_code＃＃

或轉到此頁面並手動輸入您的確認代碼：
http：//#server_name##subscr_section#subscr_edit.php？id =＃id＃id＃

在向我們發送確認之前，您將不會收到任何消息。

-------------------------------------------------- --------------------------- ----------------------- ----
請保存此消息，因為它包含授權信息。
使用確認代碼，您可以更改訂閱參數或
退訂。

編輯參數：
http：//#server_name##subscr_section#subscr_edit.php？id =＃id＃＆escresenge_code =＃juccess_code＃＃

退訂：
http：//#server_name##subscr_section#subscr_edit.php？id =＃id＃＆virckent_code =＃juccess_code＃＆action = unsubscribe
-------------------------------------------------- --------------------------- ----------------------- ----

這是自動生成的消息。
";
$MESS["SUBSCRIBE_CONFIRM_NAME"] = "確認訂閱";
$MESS["SUBSCRIBE_CONFIRM_SUBJECT"] = "＃site_name＃：訂閱確認
";
