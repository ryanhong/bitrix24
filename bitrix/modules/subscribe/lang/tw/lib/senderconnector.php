<?php
$MESS["sender_connector_subscriber_active"] = "主動訂閱：";
$MESS["sender_connector_subscriber_all"] = "任何";
$MESS["sender_connector_subscriber_confirmed"] = "訂閱已確認：";
$MESS["sender_connector_subscriber_dateinsert"] = "創建於：";
$MESS["sender_connector_subscriber_from"] = "從";
$MESS["sender_connector_subscriber_n"] = "不";
$MESS["sender_connector_subscriber_name"] = "訂閱模塊 - 訂戶";
$MESS["sender_connector_subscriber_rubric"] = "通訊：";
$MESS["sender_connector_subscriber_to"] = "到";
$MESS["sender_connector_subscriber_y"] = "是的";
