<?php
$MESS["class_post_err_att"] = "在附件保存期間發生錯誤。";
$MESS["class_post_err_auto_time"] = "指定了錯誤的時間格式用於調度。";
$MESS["class_post_err_charset"] = "已指定了無效的電子郵件編碼。可以在模塊的設置中指定有效的編碼選項。";
$MESS["class_post_err_email"] = "“來自”中有一封無效的電子郵件場地。";
$MESS["class_post_err_files_size"] = "附件超過＃max_files_size＃的最大尺寸。";
$MESS["class_post_err_lock"] = "試圖鎖定新聞通訊時的數據庫錯誤：";
$MESS["class_post_err_lock_advice"] = "請聯繫數據庫管理員以獲取運行DBMS_LOCK的權限。";
$MESS["class_post_err_mail"] = "發送新聞通訊時，消息傳遞功能失敗。";
$MESS["class_post_err_notfound"] = "沒有找到新聞通訊。";
$MESS["class_post_err_status"] = "該問題具有無效狀態。";
$MESS["class_post_err_status2"] = "發行狀態的非法更改。";
$MESS["class_post_err_status4"] = "發送錯誤（問題沒有收件人）。";
$MESS["class_post_err_subj"] = "“主題”字段不能為空。";
$MESS["class_post_err_text"] = "新聞通訊機構一定不能為空。";
$MESS["class_post_err_to"] = "“ to”必須填寫字段，以確保直接發送給每個收件人的問題。";
