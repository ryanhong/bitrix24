<?php
$MESS["class_subscr_addr"] = "給定的訂閱地址不正確。";
$MESS["class_subscr_addr2"] = "訂閱地址已經存在。請指定另一個地址。";
$MESS["class_subscr_conf"] = "無效的訂閱確認代碼。更改未保存。";
$MESS["class_subscr_perm"] = "權限不足。";
$MESS["class_subscr_user"] = "用戶不存在。";
