<?php
$MESS["DOCUMENTGENERATOR_INSTALL_DEPENDENCIES_ERROR"] = "該模塊需要php類\\ domdocument和\\ ziparchive。<br />請安裝並激活php Extenstions php-zip和php-xml";
$MESS["DOCUMENTGENERATOR_INSTALL_TITLE"] = "\“文檔生成器\”模塊安裝";
$MESS["DOCUMENTGENERATOR_MODULE_DESCRIPTION"] = "該模塊將根據模板自動創建文檔。";
$MESS["DOCUMENTGENERATOR_MODULE_NAME"] = "文檔生成器";
$MESS["DOCUMENTGENERATOR_UNINSTALL_QUESTION"] = "您確定要刪除模塊嗎？";
$MESS["DOCUMENTGENERATOR_UNINSTALL_TITLE"] = "\“文檔生成器\”模塊卸載";
