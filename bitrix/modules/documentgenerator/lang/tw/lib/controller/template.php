<?php
$MESS["DOCGEN_CONTROLLER_MODULE_INVALID"] = "無法使用模塊＃模塊＃";
$MESS["DOCGEN_CONTROLLER_TEMPLATE_DOWNLOAD_ERROR"] = "無法下載模板文件";
$MESS["DOCGEN_CONTROLLER_TEMPLATE_FILE_PREFIX"] = "模板";
$MESS["DOCGEN_CONTROLLER_TEMPLATE_NOT_FOUND"] = "找不到模板";
