<?php
$MESS["DOCGEN_FEATURE_MESSAGE"] = "您可以在免費計劃上創建最多創建<span class = \“ document-limit-desc-number \”>＃max＃documents </span>。請升級到無限制訪問文檔生成器的商業計劃之一。";
$MESS["DOCGEN_FEATURE_MODULE_ERROR"] = "未安裝\“文檔生成器\”模塊。";
$MESS["DOCGEN_FEATURE_TITLE"] = "僅在選定的商業計劃上可用";
