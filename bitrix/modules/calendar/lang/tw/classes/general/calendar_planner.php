<?php
$MESS["EC_PL_ATTENDEES_LAST"] = "其他與會者";
$MESS["EC_PL_ATTENDEES_SHOW_MORE"] = "更多的與會者";
$MESS["EC_PL_ATTENDEES_TITLE"] = "與會者";
$MESS["EC_PL_GOTO_NOW"] = "到當前日期";
$MESS["EC_PL_LOCKED_TITLE"] = "您無法在當前計劃中選擇空閒時間";
$MESS["EC_PL_PROPOSE"] = "空置的時間";
$MESS["EC_PL_PROPOSE_NO_RESULT"] = "不幸的是，我們找不到在不久的將來的活動時間。";
$MESS["EC_PL_RESOURCE_TITLE"] = "資源";
$MESS["EC_PL_SETTINGS"] = "設定";
$MESS["EC_PL_SETTINGS_SCALE"] = "規模";
$MESS["EC_PL_SETTINGS_SCALE_1DAY"] = "1天";
$MESS["EC_PL_SETTINGS_SCALE_1HOUR"] = "1小時";
$MESS["EC_PL_SETTINGS_SCALE_2HOUR"] = "2小時";
$MESS["EC_PL_SETTINGS_SCALE_15MIN"] = "15分鐘";
$MESS["EC_PL_SETTINGS_SCALE_30MIN"] = "30分鐘";
$MESS["EC_PL_SETTINGS_SCALE_ACCURACY"] = "顯示時間";
$MESS["EC_PL_SETTINGS_SCALE_READONLY_TITLE"] = "標記\“全天\”選項以更改比例";
$MESS["EC_PL_SETTINGS_SCALE_TIME"] = "顯示時間";
$MESS["EC_PL_STATUS_H"] = "組織者";
$MESS["EC_PL_STATUS_N"] = "拒絕";
$MESS["EC_PL_STATUS_Q"] = "出勤率尚未確認";
$MESS["EC_PL_STATUS_TZALL"] = "有不同時區的參與者。";
$MESS["EC_PL_STATUS_Y"] = "確認出席";
$MESS["EC_PL_UNLOCK_FEATURE"] = "現在升級";
