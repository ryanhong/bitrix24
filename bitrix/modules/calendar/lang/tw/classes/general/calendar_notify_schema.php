<?php
$MESS["EC_NS_CHANGE"] = "事件更新";
$MESS["EC_NS_DELETE_LOCATION"] = "刪除會議室";
$MESS["EC_NS_EVENT_COMMENT"] = "事件的新評論";
$MESS["EC_NS_INFO"] = "確認或拒絕活動";
$MESS["EC_NS_INVITE"] = "活動邀請";
$MESS["EC_NS_REMINDER"] = "事件提醒";
