<?php
$MESS["CALENDAR_TIP_TEMPLATE_LINK_COPIED"] = "事件鏈接已復製到剪貼板。";
$MESS["DEL_CON_CALENDARS"] = "刪除日曆";
$MESS["EC_ACCESS_DENIED"] = "拒絕訪問";
$MESS["EC_ACCESS_GROUP_ADMIN"] = "工作組所有者";
$MESS["EC_ACCESS_GROUP_MEMBERS"] = "所有工作組成員";
$MESS["EC_ACCESS_GROUP_MODERATORS"] = "工作組主持人";
$MESS["EC_ACTION_EXPORT"] = "出口";
$MESS["EC_ACTION_EXTERNAL_ADJUST"] = "編輯設置";
$MESS["EC_ACTION_HIDE"] = "禁用和隱藏";
$MESS["EC_ADD"] = "添加";
$MESS["EC_ADD_APPLICATION"] = "添加一個應用程序";
$MESS["EC_ADD_CALDAV"] = "添加外部日曆";
$MESS["EC_ADD_CALDAV_LINK"] = "Caldav服務器地址";
$MESS["EC_ADD_CALDAV_NAME"] = "姓名";
$MESS["EC_ADD_CALDAV_PASS"] = "密碼";
$MESS["EC_ADD_CALDAV_SECTIONS"] = "可用日曆";
$MESS["EC_ADD_CALDAV_USER_NAME"] = "使用者名稱";
$MESS["EC_ADD_CATEGORY"] = "添加類別";
$MESS["EC_ADD_EVENT"] = "添加事件";
$MESS["EC_ADD_LOCATION"] = "添加會議室";
$MESS["EC_ADD_TASK"] = "新任務";
$MESS["EC_ALLOW_INVITE_LABEL"] = "與會者可以邀請其他人";
$MESS["EC_ALL_DAY"] = "一整天";
$MESS["EC_ASK_TZ"] = "選擇您的時區：";
$MESS["EC_ATTENDEES_EDIT"] = "改變";
$MESS["EC_ATTENDEES_LABEL"] = "與會者";
$MESS["EC_ATTENDEES_MORE"] = "＃計數＃更多";
$MESS["EC_ATTENDEES_TOTAL_COUNT"] = "邀請＃count＃";
$MESS["EC_B24_LOCATION_LIMITATION"] = "<span>您可以在<a href= \"/settings/license_all.php \" target= \"_blank \">僅在<a href= \"/settings/license_all.php \"_>中創建並選擇事件位置。現在升級並享受更多有用的功能：
<ol>
<li>工作時間管理</li>
<li>創建和管理會議和活動</li>
<li>讓外部用戶和第三方參與工作組和項目</li>
</ol>
</span>";
$MESS["EC_B24_LOCATION_LIMITATION_TITLE"] = "創建並選擇位置";
$MESS["EC_BUSY_ALERT"] = "在指定的時間內沒有一個或多個參與者，不能邀請。";
$MESS["EC_CALDAV_COLLAPSE"] = "隱藏";
$MESS["EC_CALDAV_CONNECTION_ERROR"] = "caldav服務器返回錯誤\“＃error_str＃\”時，檢查連接\“＃Connection_name＃\”。請檢查連接設置，然後重試。";
$MESS["EC_CALDAV_DEL"] = "刪除";
$MESS["EC_CALDAV_EDIT"] = "編輯";
$MESS["EC_CALDAV_NOTICE"] = "外部日曆會自動同步。";
$MESS["EC_CALDAV_NO_CHANGE"] = " - 不要改變 - ";
$MESS["EC_CALDAV_RESTORE"] = "恢復";
$MESS["EC_CALDAV_SYNC_DATE"] = "同步";
$MESS["EC_CALDAV_SYNC_ERROR"] = "最後一個同步未成功。";
$MESS["EC_CALDAV_SYNC_OK"] = "日曆已成功同步。";
$MESS["EC_CALDAV_TITLE"] = "連接的外部日曆";
$MESS["EC_CALDAV_URL_ERROR"] = "Caldav連接參數不正確";
$MESS["EC_CALENDAR_APPLE_CALENDAR"] = "iCloud日曆";
$MESS["EC_CALENDAR_CANT_DRAG_SHARED_EVENT"] = "無法重新安排涉及外部參與者的會議。";
$MESS["EC_CALENDAR_CONFERENCE"] = "視訊會議";
$MESS["EC_CALENDAR_EMPTY_SEARCH_RESULT_TEXT"] = "嘗試不同的搜索參數或使用不同的關鍵字。";
$MESS["EC_CALENDAR_EMPTY_SEARCH_RESULT_TITLE"] = "您的搜索沒有產生結果";
$MESS["EC_CALENDAR_GOOGLE_CALENDAR"] = "Google日曆";
$MESS["EC_CALENDAR_HOW_DOES_IT_WORK"] = "它是如何工作的？";
$MESS["EC_CALENDAR_NO_EVENTS_TEXT"] = "您可以添加來自第三方日曆的活動，以將所有活動放在一個地方。";
$MESS["EC_CALENDAR_NO_EVENTS_TITLE"] = "沒有事件";
$MESS["EC_CALENDAR_NO_INVITATIONS_TITLE"] = "您沒有任何未決邀請。";
$MESS["EC_CALENDAR_NO_MORE_EVENTS_FUTURE"] = "不再有計劃的事件。";
$MESS["EC_CALENDAR_NO_MORE_EVENTS_PAST"] = "沒有較早的事件。";
$MESS["EC_CALENDAR_OFFICE365_CALENDAR"] = "Office365日曆";
$MESS["EC_CALENDAR_REC_EVENT"] = "經常性事件";
$MESS["EC_CAL_CONNECT_MORE"] = "連接＃計數＃更多";
$MESS["EC_CAL_DAV_CON_WAIT"] = "請在處理之前的請求時等待。";
$MESS["EC_CAL_GOOGLE_HIDE_CONFIRM"] = "您確定要禁用此日曆並隱藏它嗎？";
$MESS["EC_CAL_LAST_SYNC_DATE"] = "最後同步";
$MESS["EC_CAL_REMOVE_GOOGLE_SYNC_CONFIRM"] = "您確定要禁用Google日曆同步並刪除這些日曆嗎？";
$MESS["EC_CAL_SYNC_ANDROID"] = "安卓";
$MESS["EC_CAL_SYNC_CONNECT"] = "連接";
$MESS["EC_CAL_SYNC_DISCONNECT"] = "斷開";
$MESS["EC_CAL_SYNC_EXCHANGE"] = "MS Exchange";
$MESS["EC_CAL_SYNC_GOOGLE"] = "Google日曆";
$MESS["EC_CAL_SYNC_IPHONE"] = "iPhone";
$MESS["EC_CAL_SYNC_MAC"] = "蘋果系統";
$MESS["EC_CAL_SYNC_OFFICE_365"] = "辦公室365";
$MESS["EC_CAL_SYNC_OK"] = "同步";
$MESS["EC_CAL_SYNC_OUTLOOK"] = "MS Outlook";
$MESS["EC_CAL_SYNC_REFRESH"] = "更新";
$MESS["EC_CAL_SYNC_TITLE"] = "同步";
$MESS["EC_CATEGORY_DELETE_CONFIRM"] = "確定要刪除類別嗎？";
$MESS["EC_CATEGORY_EMPTY"] = "沒有會議室。";
$MESS["EC_COLLAPSED_MESSAGE"] = "事件";
$MESS["EC_COLOR"] = "其他顏色";
$MESS["EC_COUNTER_INVITATION"] = "邀請";
$MESS["EC_COUNTER_INVITATION_PLURAL_0"] = "邀請";
$MESS["EC_COUNTER_INVITATION_PLURAL_1"] = "邀請";
$MESS["EC_COUNTER_INVITATION_PLURAL_2"] = "邀請";
$MESS["EC_COUNTER_NEW_COMMENTS_PLURAL_0"] = "帶有新評論";
$MESS["EC_COUNTER_NEW_COMMENTS_PLURAL_1"] = "帶有新評論";
$MESS["EC_COUNTER_NEW_COMMENTS_PLURAL_2"] = "帶有新評論";
$MESS["EC_COUNTER_TOTAL"] = "事件";
$MESS["EC_CREATE"] = "創造";
$MESS["EC_CREATE_CHAT_CONFIRM_QUESTION"] = "您確定要與所有會議參與者建立聊天嗎？";
$MESS["EC_CREATE_CHAT_OK"] = "是的";
$MESS["EC_CREATE_EVENT"] = "創建事件";
$MESS["EC_DATE_FORMAT_1_MAY"] = "縮略詞，";
$MESS["EC_DATE_WEEK_NUMBER"] = "週＃週_NUMBER＃";
$MESS["EC_DAY_LENGTH"] = "＃計數＃天";
$MESS["EC_DECLINE_ALL"] = "所有事件";
$MESS["EC_DECLINE_NEXT"] = "這是所有隨後的事件";
$MESS["EC_DECLINE_ONLY_THIS"] = "只有這個事件";
$MESS["EC_DECLINE_REC_EVENT"] = "您想拒絕參加重複活動嗎？";
$MESS["EC_DEFAULT_ENTRY_NAME"] = "新事件";
$MESS["EC_DELETE_MEETING_CONFIRM"] = "您將要刪除包含其他參與者的活動。您確定要刪除活動嗎？";
$MESS["EC_DELETE_MEETING_GUEST_CONFIRM"] = "刪除活動將自動取消您的參與。您確定要刪除活動嗎？";
$MESS["EC_DESCRIPTION"] = "描述";
$MESS["EC_DESTINATION_ADD_MORE"] = "添加更多";
$MESS["EC_DESTINATION_ADD_USERS"] = "添加人員，小組或部門";
$MESS["EC_EDEV_EXP_WARN"] = "筆記！導出數據檢查失敗。請確保您的訪問權限有效。";
$MESS["EC_ENTRIES_EVENTS"] = "事件";
$MESS["EC_ENTRIES_TASKS"] = "任務";
$MESS["EC_ENTRY_NAME"] = "事件名稱";
$MESS["EC_EVENT"] = "事件";
$MESS["EC_EVENT_BUTTON"] = "事件";
$MESS["EC_EVENT_IS_MINE"] = "我的活動";
$MESS["EC_EVENT_TZ_DEF_HINT"] = "選擇您的時區。您的偏好將被保存。";
$MESS["EC_EVENT_TZ_HINT"] = "如果事件要在不同的時區進行，請在列表中選擇一個。事件開始和結束時間的不同時區是可能的。";
$MESS["EC_EXP_TEXT"] = "導出日曆向第三方應用程序（以ICAL格式）：";
$MESS["EC_FULL_FORM_LABEL"] = "完整表格";
$MESS["EC_HOST"] = "組織者";
$MESS["EC_JS_DEL_MEETING_GUEST_CONFIRM"] = "確定您想拒絕此事件嗎？";
$MESS["EC_JS_EXPORT_TILE"] = "出口格式";
$MESS["EC_JS_ICAL_COPY_ICAL_SYNC_LINK"] = "複製鏈接";
$MESS["EC_JS_ICAL_COPY_ICAL_SYNC_LINK_FAILED"] = "無法將鏈接複製到剪貼板";
$MESS["EC_JS_ICAL_COPY_ICAL_SYNC_LINK_SUCCESS"] = "鏈接複製到剪貼板";
$MESS["EC_JS_ICAL_ERROR_WITH_PATHES"] = "注意力！日曆同步數據不正確。請聯繫您的BitRix24管理員或HelpDesk。";
$MESS["EC_JS_UNTIL_DATE"] = "直到＃日期＃";
$MESS["EC_LEAVE_EVENT_CONFIRM_DESC"] = "所有更改都將丟失";
$MESS["EC_LEAVE_EVENT_CONFIRM_QUESTION"] = "關閉事件編輯表格？";
$MESS["EC_LOCATION"] = "地點";
$MESS["EC_LOCATION_404"] = "沒有數據";
$MESS["EC_LOCATION_CHOOSE"] = "選擇會議室";
$MESS["EC_LOCATION_LABEL"] = "地點";
$MESS["EC_LOCATION_MEETING_ROOM_SET"] = "配置列表";
$MESS["EC_LOCATION_REPEAT_BUSY_POPUP_RETURN_TO_EDIT"] = "不，回到編輯";
$MESS["EC_LOCATION_REPEAT_BUSY_POPUP_SAVE_WITHOUT_ROOM"] = "是的，沒有會議室";
$MESS["EC_LOCATION_REPEAT_BUSY_POPUP_TITLE"] = "無會議室的經常會議";
$MESS["EC_LOCATION_RESERVE_ERROR"] = "該位置在指定的時間不可用。";
$MESS["EC_LOCATION_SETTINGS_MESSAGE_DESCRIPTION"] = "選擇您要允許創建，刪除和編輯會議室的用戶";
$MESS["EC_LOCATION_SETTINGS_MORE_INFO"] = "細節";
$MESS["EC_LOCATION_VIEW_LOCKED"] = "您無法根據當前計劃創建或保留會議室";
$MESS["EC_LOCATION_VIEW_UNLOCK_FEATURE"] = "立即升級您的計劃";
$MESS["EC_MANAGE_CALDAV"] = "配置外部日曆（Caldav）";
$MESS["EC_MANAGE_CALDAV_TITLE"] = "管理外部日曆連接。";
$MESS["EC_MAP_LOCATION_LABEL"] = "指定位置";
$MESS["EC_MEETING_ROOM_ADD"] = "添加";
$MESS["EC_MEETING_ROOM_LIST_TITLE"] = "會議室";
$MESS["EC_MEETING_ROOM_PLACEHOLDER"] = "姓名";
$MESS["EC_MOBILE_HELP_ANDROID"] = "<p>要連接日曆，請在移動設備上按照以下步驟操作。
<ol>
<li>打開Goog​​lePlay並安裝BitRix24應用。</li>
<li>選擇菜單設置>帳戶> \“ bitrix24 \”
如果沒有存在帳戶，請創建一個新的帳戶並選擇Bitrix24。
<ul>
<li>指定您的Bitrix24地址（例如<i> company.bitrix24.com </i>）</li>
<li>輸入您的登錄（電子郵件）</li>
<li>輸入密碼</li>
</ul>
<li>如果您使用的是兩步身份驗證，請使用用戶配置文件中指定的密碼（\“應用程序密碼\”）。</li>
</li>
<li>單擊帳戶並選擇一個日曆和/或聯繫人以同步並將其保存到您的手機中。</li>

<li>單擊同步。</li>
</ol>
您的Bitrix24日曆現在可以在您的移動設備的日曆中提供，所有事件同步。</p>";
$MESS["EC_MOBILE_HELP_IPHONE"] = "<p>設置您的Apple設備以支持Caldav：</p>
<ol>
<li>在您的Apple設備上，打開菜單<b>設置</b>＆gt; <b>帳戶和密碼</b>。</li>
<li>選擇<b>在帳戶列表下添加帳戶</b>。</li>
<li>選擇Caldav作為帳戶類型（<b>其他</b>＆gt; <b> caldav帳戶</b>）。</li>
<li>將此網站地址指定為服務器（<span class = \“ bxec-link \”>＃calendar_link＃</span>）。使用您的登錄和密碼。</li>
<li>如果您使用的是兩步身份驗證，請使用用戶配置文件中指定的密碼：<b>應用程序密碼</b>＆gt; <b>日曆</b>。</li>
<li>要指定端口號，保存帳戶，然後將其打開以再次進行編輯，然後進行<b> More </b>區域。</li>
</ol>
<p>您的日曆將出現在日曆應用中。</p>";
$MESS["EC_MOBILE_HELP_MAC"] = "<p>連接iCal中的日曆：</p>
<ol>
<li>打開Caneldar>設置並點擊帳戶選項卡。</li>
<li>選擇添加帳戶。</li>
<li>選擇Caldav。</li>
<li>將帳戶類型設置為手冊。</li>
<li>將此網站地址指定為服務器（<span class = \“ bxec-link \”>＃calendar_link＃</span>）。使用您的登錄和密碼。</li>
<li>如果您使用的是兩步身份驗證，請使用用戶配置文件中指定的密碼（\“應用程序密碼\”）。</li>
<li>要指定端口號，保存帳戶，然後將其打開以再次進行編輯。</li>
</ol>
<p>您的日曆將出現在iCal應用中。</p>";
$MESS["EC_MOBILE_SYNC_TITLE_ANDROID"] = "Android同步參數";
$MESS["EC_MOBILE_SYNC_TITLE_IPHONE"] = "Apple iPhone/iPad同步參數";
$MESS["EC_MOBILE_SYNC_TITLE_MACOSX"] = "MACOS同步參數";
$MESS["EC_MONTH_SHORT"] = "<small> #month＃</small>＃日期＃";
$MESS["EC_MONTH_WEEK_TITLE"] = "<span> #day_of_week＃</span>";
$MESS["EC_NEW_CONNECTION_NAME"] = "新的Caldav連接";
$MESS["EC_NOTIFY_STATUS_LABEL"] = "通知與會者確認或拒絕邀請";
$MESS["EC_NO_COUNTERS"] = "沒有緊急事件。";
$MESS["EC_NO_EVENTS"] = "沒有事件";
$MESS["EC_REINVITE_LABEL"] = "要求用戶重新確認出席";
$MESS["EC_REMIND"] = "提醒";
$MESS["EC_REMIND1_0"] = "事件開始時";
$MESS["EC_REMIND1_5"] = "活動前5分鐘";
$MESS["EC_REMIND1_10"] = "活動前10分鐘";
$MESS["EC_REMIND1_15"] = "活動前15分鐘";
$MESS["EC_REMIND1_20"] = "活動開始前20分鐘";
$MESS["EC_REMIND1_30"] = "活動前30分鐘";
$MESS["EC_REMIND1_60"] = "活動前1小時";
$MESS["EC_REMIND1_120"] = "活動前2小時";
$MESS["EC_REMIND1_1440"] = "活動前一天";
$MESS["EC_REMIND1_2880"] = "活動前兩天";
$MESS["EC_REMIND1_CUSTOM"] = "風俗";
$MESS["EC_REMIND1_DAY_0"] = "在活動當天（＃時間＃）";
$MESS["EC_REMIND1_DAY_1"] = "活動發生前一天（＃Time＃）";
$MESS["EC_REMIND1_DAY_2"] = "活動前兩天（＃Time＃）";
$MESS["EC_REMIND1_DAY_COUNT"] = "＃count＃day（s）之前";
$MESS["EC_REMIND1_HOUR_COUNT"] = "＃計數＃小時（s）之前";
$MESS["EC_REMIND1_MIN_COUNT"] = "＃count＃minite（s）";
$MESS["EC_REMIND1_NO"] = "沒有任何";
$MESS["EC_REMIND1_SHORT_0"] = "事件開始時";
$MESS["EC_REMIND1_SHORT_5"] = "5分鐘前";
$MESS["EC_REMIND1_SHORT_10"] = "10分鐘前";
$MESS["EC_REMIND1_SHORT_15"] = "15分鐘前";
$MESS["EC_REMIND1_SHORT_20"] = "20分鐘前";
$MESS["EC_REMIND1_SHORT_30"] = "30分鐘前";
$MESS["EC_REMIND1_SHORT_60"] = "1小時前";
$MESS["EC_REMIND1_SHORT_120"] = "2小時前";
$MESS["EC_REMIND1_SHORT_1440"] = "前一天";
$MESS["EC_REMIND1_SHORT_2880"] = "兩天前";
$MESS["EC_REMIND_LABEL"] = "提醒";
$MESS["EC_REPEAT"] = "重複";
$MESS["EC_REQUEST_APP_FAILURE"] = "錯誤加載或運行應用程序<strong> \“＃AppName＃\” </strong>";
$MESS["EC_REQUEST_APP_NONAME_TAB"] = "- 無標題 -";
$MESS["EC_RESERVE"] = "預訂";
$MESS["EC_RESERVE_PERIOD_WARN"] = "經常性活動無法預定會議室。";
$MESS["EC_ROOM_DELETE_CONFIRM"] = "確定您想刪除會議室嗎？";
$MESS["EC_SAVE_ENTRY_CONFIRM"] = "所有未保存的數據將丟失。";
$MESS["EC_SEARCH_RESET_RESULT"] = "重置";
$MESS["EC_SEARCH_RESULT"] = "搜索結果";
$MESS["EC_SEARCH_RESULT_BY_QUERY"] = "搜索結果\“＃query＃\”";
$MESS["EC_SECTION_BUTTON"] = "日曆";
$MESS["EC_SECTION_ROOMS"] = "會議室";
$MESS["EC_SECTION_ROOMS_LIST"] = "會議室";
$MESS["EC_SEC_CONNECT_TO_OUTLOOK"] = "連接到Microsoft Outlook";
$MESS["EC_SEC_DELETE"] = "刪除";
$MESS["EC_SEC_DELETE_CONFIRM"] = "您確定要刪除日曆和所有事件嗎？";
$MESS["EC_SEC_EDIT"] = "編輯";
$MESS["EC_SEC_EDIT_CONFIRM"] = "變化沒有保存。你確定你要繼續嗎？";
$MESS["EC_SEC_GROUP_TASK_DEFAULT"] = "工作組任務";
$MESS["EC_SEC_HIDE"] = "從列表中刪除";
$MESS["EC_SEC_LEAVE_ONE"] = "僅顯示此項目";
$MESS["EC_SEC_LEAVE_ONE_ROOM"] = "僅顯示這個房間";
$MESS["EC_SEC_MY_TASK_DEFAULT"] = "我的任務";
$MESS["EC_SEC_OPEN_LINK"] = "打開日曆";
$MESS["EC_SEC_SLIDER_ACCESS"] = "訪問權限";
$MESS["EC_SEC_SLIDER_ACCESS_ADD"] = "添加";
$MESS["EC_SEC_SLIDER_ADJUST_SYNC"] = "配置同步設置";
$MESS["EC_SEC_SLIDER_CANCEL"] = "取消";
$MESS["EC_SEC_SLIDER_CATEGORY_SELECTOR_PLACEHOLDER"] = "輸入類別名稱";
$MESS["EC_SEC_SLIDER_CATEGORY_SELECTOR_STUB"] = "沒有類別。";
$MESS["EC_SEC_SLIDER_CHANGE"] = "改變";
$MESS["EC_SEC_SLIDER_CLOSE"] = "關閉";
$MESS["EC_SEC_SLIDER_COLOR"] = "顏色";
$MESS["EC_SEC_SLIDER_CREATE_CHAT_LINK"] = "與與會者聊天";
$MESS["EC_SEC_SLIDER_EDIT_ROOM_CATEGORY"] = "編輯類別";
$MESS["EC_SEC_SLIDER_EDIT_SECTION"] = "編輯日曆";
$MESS["EC_SEC_SLIDER_EDIT_SECTION_PERSONAL"] = "個人定制";
$MESS["EC_SEC_SLIDER_EDIT_SECTION_ROOM"] = "編輯會議室";
$MESS["EC_SEC_SLIDER_GROUP_CALENDARS_LIST"] = "工作組日曆";
$MESS["EC_SEC_SLIDER_MY_CALENDARS_LIST"] = "我的日曆";
$MESS["EC_SEC_SLIDER_NEW_CATEGORY"] = "新類別";
$MESS["EC_SEC_SLIDER_NEW_ROOM"] = "新會議室";
$MESS["EC_SEC_SLIDER_NEW_SECTION"] = "新日曆";
$MESS["EC_SEC_SLIDER_NO_SECTIONS"] = "沒有可用的日曆";
$MESS["EC_SEC_SLIDER_POPUP_EXIST_TITLE"] = "可用日曆";
$MESS["EC_SEC_SLIDER_POPUP_MENU_ADD_COMP"] = "公司日曆";
$MESS["EC_SEC_SLIDER_POPUP_MENU_ADD_GROUP"] = "工作組日曆";
$MESS["EC_SEC_SLIDER_POPUP_MENU_ADD_USER"] = "用戶日曆";
$MESS["EC_SEC_SLIDER_POPUP_MENU_NO_ROOMS"] = "沒有會議室";
$MESS["EC_SEC_SLIDER_POPUP_NEW_MENU"] = "創建日曆";
$MESS["EC_SEC_SLIDER_POPUP_NEW_TITLE"] = "新的";
$MESS["EC_SEC_SLIDER_ROOM_CATEGORY"] = "類別";
$MESS["EC_SEC_SLIDER_ROOM_SELECTOR"] = "會議室";
$MESS["EC_SEC_SLIDER_ROOM_SELECTOR_PLACEHOLDER"] = "輸入會議室名稱";
$MESS["EC_SEC_SLIDER_ROOM_SELECTOR_STUB"] = "沒有會議室。";
$MESS["EC_SEC_SLIDER_SAVE"] = "節省";
$MESS["EC_SEC_SLIDER_SECTION_CAPACITY"] = "與會者人數";
$MESS["EC_SEC_SLIDER_SECTION_NECESSITY"] = "尊重會議室的能力";
$MESS["EC_SEC_SLIDER_SECTION_TITLE"] = "姓名";
$MESS["EC_SEC_SLIDER_SELECT_GROUPS"] = "添加工作組和項目";
$MESS["EC_SEC_SLIDER_SELECT_USERS"] = "添加用戶";
$MESS["EC_SEC_SLIDER_SYNC_DISABLED"] = "禁用";
$MESS["EC_SEC_SLIDER_TITLE_COMP_CAL"] = "公司";
$MESS["EC_SEC_SLIDER_TITLE_GROUP_CAL"] = "工作組和項目";
$MESS["EC_SEC_SLIDER_TITLE_LOCATION_CAL"] = "位置";
$MESS["EC_SEC_SLIDER_TITLE_RESOURCE_CAL"] = "資源";
$MESS["EC_SEC_SLIDER_TYPE_ARCHIVE"] = "存檔Google日曆";
$MESS["EC_SEC_SLIDER_TYPE_ARCHIVE_HELPER"] = "先前斷開的Google日曆。這些日曆與您的Google帳戶不同步。";
$MESS["EC_SEC_SLIDER_TYPE_CALDAV"] = "caldav＃連接＃＃日曆";
$MESS["EC_SEC_SLIDER_TYPE_CALENDARS_LIST"] = "公司日曆";
$MESS["EC_SEC_SLIDER_TYPE_DEFAULT"] = "外部日曆";
$MESS["EC_SEC_SLIDER_TYPE_GOOGLE"] = "Google：＃Connection_name＃";
$MESS["EC_SEC_SLIDER_TYPE_GOOGLE_DIS"] = "Google日曆";
$MESS["EC_SEC_SLIDER_TYPE_ICLOUD"] = "iCloud：＃connection_name＃";
$MESS["EC_SEC_SLIDER_TYPE_ICLOUD_DIS"] = "iCloud日曆";
$MESS["EC_SEC_SLIDER_TYPE_LOCATION_LIST"] = "位置日曆";
$MESS["EC_SEC_SLIDER_TYPE_OFFICE365"] = "Office365：＃Connection_Name＃";
$MESS["EC_SEC_SLIDER_TYPE_OFFICE365_DIS"] = "Office365日曆";
$MESS["EC_SEC_SLIDER_TYPE_RESOURCE_LIST"] = "資源日曆";
$MESS["EC_SEC_SLIDER_TYPE_ROOM_LIST"] = "會議室";
$MESS["EC_SEC_SLIDER_TYPE_YANDEX"] = "yandex：＃connection_name＃";
$MESS["EC_SEC_TASK_HIDE"] = "隱藏任務日曆";
$MESS["EC_SEC_USER_TASK_DEFAULT"] = "用戶任務";
$MESS["EC_SET_SLIDER_PESONAL_SETTINGS_TITLE"] = "個人喜好";
$MESS["EC_SET_SLIDER_SETTINGS_TITLE"] = "設定";
$MESS["EC_SHOW_ALL"] = "顯示所有";
$MESS["EC_SIMPLE_FORM_ADD"] = "添加";
$MESS["EC_SIMPLE_FORM_BACK"] = "後退";
$MESS["EC_SOCNET_DESTINATION_3"] = "給所有員工";
$MESS["EC_SOCNET_DESTINATION_4"] = "給所有用戶";
$MESS["EC_START_VIDEOCONFERENCE_CONFIRM_QUESTION"] = "您確定要與所有會議參與者一起開始視頻會議嗎？";
$MESS["EC_START_VIDEOCONFERENCE_OK"] = "是的";
$MESS["EC_TASK"] = "任務";
$MESS["EC_TASK_BUTTON"] = "任務";
$MESS["EC_TILL_TIME"] = "直到＃時間＃";
$MESS["EC_TIME_FROM_PLACEHOLDER"] = "開始時間";
$MESS["EC_TIME_TO_PLACEHOLDER"] = "時間結束";
$MESS["EC_TODAY"] = "今天";
$MESS["EC_VIEW"] = "打開";
$MESS["EC_VIEW_DAY"] = "天";
$MESS["EC_VIEW_DESIDE_BUT_I"] = "或許";
$MESS["EC_VIEW_DESIDE_BUT_N"] = "衰退";
$MESS["EC_VIEW_DESIDE_BUT_OWNER_N"] = "不要參加";
$MESS["EC_VIEW_DESIDE_BUT_Y"] = "出席";
$MESS["EC_VIEW_LIST"] = "日程";
$MESS["EC_VIEW_MODE_SHOW_BY"] = "排序方式";
$MESS["EC_VIEW_MONTH"] = "月";
$MESS["EC_VIEW_STATUS_BUT_H"] = "參加（組織者）";
$MESS["EC_VIEW_STATUS_BUT_I"] = "不確定";
$MESS["EC_VIEW_STATUS_BUT_N"] = "不會關注";
$MESS["EC_VIEW_STATUS_BUT_Y"] = "參加";
$MESS["EC_VIEW_WEEK"] = "星期";
$MESS["EC_VIEW_YEAR"] = "年";
$MESS["EC_WEEK_TITLE"] = "<span class ='calendar-day-of-週'>＃day_of_week＃</span> <span class ='calendar-num-day'>＃date＃date＃</span>";
