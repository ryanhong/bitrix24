<?php
$MESS["EC_EVENT_NOT_FOUND"] = "沒有找到事件，或者您沒有足夠的許可來查看它";
$MESS["EC_LOCATION_BUSY"] = "所選的會議室不能保留在指定的時間內。";
$MESS["EC_LOCATION_BUSY_RECURRENCE"] = "您選擇的會議室在某些時候不可用。請為重複會議的所有出現選擇其他地方。";
$MESS["EC_SECTION_NOT_FOUND"] = "錯誤：找不到日曆。";
