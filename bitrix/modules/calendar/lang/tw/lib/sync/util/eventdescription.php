<?php
$MESS["CALENDAR_EXPORT_EVENT_LOCK"] = "您無法編輯此事件。";
$MESS["CALENDAR_EXPORT_EVENT_MEETING"] = "小組活動只能在Bitrix24內進行編輯，您可以在其中諮詢活動計劃者以獲取參與者的可用性。";
