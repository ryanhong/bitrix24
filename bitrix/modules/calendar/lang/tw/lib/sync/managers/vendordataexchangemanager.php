<?php
$MESS["CALENDAR_IMPORT_BLOCK_ATTENDEE_ACCESSIBILITY"] = "您對事件的更改[url =＃event_url＃]＃event_title＃[/url]在外部日曆中未能保存，因為在選定的時間不可用事件參與者或會議室。請按照鏈接編輯事件：[url =＃event_url＃]＃event_title＃[/url]。";
$MESS["CALENDAR_IMPORT_BLOCK_FROM_ATTENDEE"] = "您對事件的更改[url =＃event_url＃]＃event_title＃[/url]在外部日曆中沒有保存，因為您是此事件的參與者，無法對其進行編輯。";
$MESS["CALENDAR_IMPORT_BLOCK_RESOURCE_BOOKING"] = "您對事件的更改[url =＃event_url＃]＃event_title＃[/url]在外部日曆中沒有保存，因為您無法編輯此事件。";
