<?php
$MESS["CALENDAR_IMPORT_BLOCK_ATTENDEE_ACCESSIBILITY"] = "您對事件的更改[url =＃event_url＃]＃event_title＃[/url]在外部日曆中未能保存，因為在選定的時間不可用事件參與者或會議室。請按照鏈接編輯事件：[url =＃event_url＃]＃event_title＃[/url]。";
$MESS["CALENDAR_IMPORT_BLOCK_FROM_ATTENDEE"] = "您對事件的更改[url =＃event_url＃]＃event_title＃[/url]在外部日曆中沒有保存，因為您沒有創建事件。";
$MESS["CALENDAR_IMPORT_BLOCK_RESOURCE_BOOKING"] = "您對事件的更改[url =＃event_url＃]＃event_title＃[/url]在外部日曆中沒有保存，因此由於權限不足而保存。請聯繫您的BitRix24管理員。";
$MESS["FINISHED_SYNC_NOTIFICATION_GOOGLE"] = "日曆已成功地與Google日曆同步。";
$MESS["FINISHED_SYNC_NOTIFICATION_ICLOUD"] = "日曆已成功地與iCloud日曆同步。";
$MESS["FINISHED_SYNC_NOTIFICATION_OFFICE365"] = "日曆已成功地與Office365日曆同步。";
$MESS["ROLLBACK_SYNC_NOTIFICATION_GOOGLE"] = "錯誤啟用Google日曆同步。";
$MESS["ROLLBACK_SYNC_NOTIFICATION_ICLOUD"] = "錯誤啟用iCloud日曆同步。";
$MESS["ROLLBACK_SYNC_NOTIFICATION_OFFICE365"] = "啟用Office365日曆同步的錯誤。";
