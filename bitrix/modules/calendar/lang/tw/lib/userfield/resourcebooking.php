<?php
$MESS["USER_TYPE_DURATION_REGEXP_DAY"] = "天| d";
$MESS["USER_TYPE_DURATION_REGEXP_HOUR"] = "小時|小時";
$MESS["USER_TYPE_DURATION_X_DAY"] = "＃num＃day（s）";
$MESS["USER_TYPE_DURATION_X_HOUR"] = "＃num＃hr";
$MESS["USER_TYPE_DURATION_X_MIN"] = "＃num＃min";
$MESS["USER_TYPE_LOADING_TIMEZONE_LIST"] = "載入中...";
$MESS["USER_TYPE_RESOURCEBOOKING_DESCRIPTION"] = "預訂資源";
$MESS["USER_TYPE_RESOURCE_ADD"] = "添加資源";
$MESS["USER_TYPE_RESOURCE_ADD_NEW"] = "添新";
$MESS["USER_TYPE_RESOURCE_ADD_SERVICE"] = "添加";
$MESS["USER_TYPE_RESOURCE_ADD_SERVICES"] = "添加服務和持續時間";
$MESS["USER_TYPE_RESOURCE_ADD_USER"] = "添加員工";
$MESS["USER_TYPE_RESOURCE_BOOKED_ERROR"] = "資源或員工在指定的時間不可用。";
$MESS["USER_TYPE_RESOURCE_CHOOSE"] = "選擇";
$MESS["USER_TYPE_RESOURCE_CHOOSE_RESOURCES"] = "資源";
$MESS["USER_TYPE_RESOURCE_CHOOSE_RESOURCES_AND_USERS"] = "資源和用戶";
$MESS["USER_TYPE_RESOURCE_CHOOSE_USERS"] = "用戶";
$MESS["USER_TYPE_RESOURCE_CLOSE"] = "關閉";
$MESS["USER_TYPE_RESOURCE_DATETIME_BLOCK_TITLE"] = "日期和時間";
$MESS["USER_TYPE_RESOURCE_DATE_BLOCK_TITLE"] = "日期";
$MESS["USER_TYPE_RESOURCE_DATE_LABEL"] = "開始日期";
$MESS["USER_TYPE_RESOURCE_DELETING"] = "刪除";
$MESS["USER_TYPE_RESOURCE_DURATION_LABEL"] = "期間";
$MESS["USER_TYPE_RESOURCE_EMPTY"] = "空的";
$MESS["USER_TYPE_RESOURCE_EVENT_TITLE"] = "預訂";
$MESS["USER_TYPE_RESOURCE_FILTER_NAME"] = "展示CRM實體事件";
$MESS["USER_TYPE_RESOURCE_FULL_DAY"] = "僅日期";
$MESS["USER_TYPE_RESOURCE_LIST_PLACEHOLDER"] = "未選中的";
$MESS["USER_TYPE_RESOURCE_NAME"] = "資源名稱";
$MESS["USER_TYPE_RESOURCE_NEW_RES_PLACEHOLDER"] = "新資源";
$MESS["USER_TYPE_RESOURCE_OVERBOOKING"] = "允許超額預訂";
$MESS["USER_TYPE_RESOURCE_RESOURCE_CONTROL_DEFAULT_NAME"] = "資源";
$MESS["USER_TYPE_RESOURCE_SAVE"] = "節省";
$MESS["USER_TYPE_RESOURCE_SELECT"] = "選擇一個資源";
$MESS["USER_TYPE_RESOURCE_SELECT_ALL"] = "選擇所有資源";
$MESS["USER_TYPE_RESOURCE_SELECT_USER"] = "選擇員工";
$MESS["USER_TYPE_RESOURCE_SERVICE_LABEL"] = "服務";
$MESS["USER_TYPE_RESOURCE_SERVICE_PLACEHOLDER"] = "服務名稱";
$MESS["USER_TYPE_RESOURCE_SETTINGS"] = "日曆設置";
$MESS["USER_TYPE_RESOURCE_TIMEZONE"] = "預訂時區：＃時區＃";
$MESS["USER_TYPE_RESOURCE_TIMEZONE_SETTINGS_TITLE"] = "時區";
$MESS["USER_TYPE_RESOURCE_TIME_LABEL"] = "時間";
$MESS["USER_TYPE_RESOURCE_TO_ALL_USERS"] = "全部用戶";
$MESS["USER_TYPE_RESOURCE_USERS_CONTROL_DEFAULT_NAME"] = "僱員";
$MESS["USER_TYPE_RESOURCE_USERS_CONTROL_TITLE"] = "用戶選擇字段名稱";
$MESS["USER_TYPE_RESOURCE_USE_RESOURCES"] = "選擇資源";
$MESS["USER_TYPE_RESOURCE_USE_USERS"] = "選擇用戶";
$MESS["USER_TYPE_RESOURCE_USE_USER_TIMEZONE"] = "尊重用戶時區";
$MESS["WEBF_RES_AUTO_SELECT_RES"] = "可用資源";
$MESS["WEBF_RES_AUTO_SELECT_RES_SHORT"] = "可用資源";
$MESS["WEBF_RES_AUTO_SELECT_USER"] = "選擇可用的員工";
$MESS["WEBF_RES_AUTO_SELECT_USER_SHORT"] = "可用員工";
$MESS["WEBF_RES_BOOKING_BUSY_DAY_WARNING"] = "沒有時間可用";
$MESS["WEBF_RES_BOOKING_PAST_DATE_WARNING"] = "您過去不能預訂約會";
$MESS["WEBF_RES_BOOKING_REQUIRED_WARNING"] = "完成所有必需的字段";
$MESS["WEBF_RES_BOOKING_SETTINGS_HELP"] = "有關此表格的更多詳細信息，請參閱＃start_link＃指令＃end_link＃</a>";
$MESS["WEBF_RES_BOOKING_STATUS_DATE_IS_NOT_AVAILABLE"] = "選定的時間不可用";
$MESS["WEBF_RES_BOOKING_STATUS_LABEL"] = "選定";
$MESS["WEBF_RES_BOOKING_STATUS_NO_TIME_SELECTED"] = "未選擇時間";
$MESS["WEBF_RES_BOOKING_UF_WARNING"] = "錯誤：資源預訂字段尚未正確配置。";
$MESS["WEBF_RES_BOOKING_WARNING"] = "無法完成預訂的信息";
$MESS["WEBF_RES_BUSY_DAY_DATE_FORMAT"] = "L，F J";
$MESS["WEBF_RES_CALENDAR_START_FROM"] = "顯示日曆";
$MESS["WEBF_RES_CALENDAR_START_FROM_FREE"] = "從第一個可用日期開始";
$MESS["WEBF_RES_CALENDAR_START_FROM_FREE_SHORT"] = "第一個可用日期";
$MESS["WEBF_RES_CALENDAR_START_FROM_TODAY"] = "從今天開始";
$MESS["WEBF_RES_CALENDAR_START_FROM_TODAY_SHORT"] = "今天";
$MESS["WEBF_RES_CALENDAR_STYLE"] = "日曆樣式";
$MESS["WEBF_RES_CALENDAR_STYLE_LINE"] = "脫衣日曆";
$MESS["WEBF_RES_CALENDAR_STYLE_POPUP"] = "彈出日曆";
$MESS["WEBF_RES_CLOSE_SETTINGS_POPUP"] = "完成自定義";
$MESS["WEBF_RES_DATE"] = "日期";
$MESS["WEBF_RES_DATE_FORMAT_DATE"] = "j";
$MESS["WEBF_RES_DATE_FORMAT_DATE_LINE"] = "F J，Y";
$MESS["WEBF_RES_DATE_FORMAT_DAY_LINE"] = "l";
$MESS["WEBF_RES_DATE_FORMAT_DAY_OF_THE_WEEK"] = "d";
$MESS["WEBF_RES_DATE_FORMAT_FROM_TO"] = "從＃date_from＃到＃date_to＃";
$MESS["WEBF_RES_DATE_FORMAT_STATUS"] = "L，F J";
$MESS["WEBF_RES_DATE_FORMAT_STATUS_SHORT"] = "L，F J";
$MESS["WEBF_RES_DATE_LABEL"] = "選擇日期";
$MESS["WEBF_RES_DURATION"] = "期間";
$MESS["WEBF_RES_DURATION_LABEL"] = "期間";
$MESS["WEBF_RES_FIELD_NAME"] = "字段名稱";
$MESS["WEBF_RES_FIELD_NAME_IN_FORM"] = "形式的字段名稱";
$MESS["WEBF_RES_FIELD_REQ_FIELD"] = "必填項目";
$MESS["WEBF_RES_FIELD_SHOW_IN_FORM"] = "以形式顯示";
$MESS["WEBF_RES_FROM_LIST"] = "從列表中選擇";
$MESS["WEBF_RES_MULTIPLE"] = "多次選擇";
$MESS["WEBF_RES_MULTIPLE_SHORT"] = "多種的";
$MESS["WEBF_RES_NAME_LABEL"] = "姓名";
$MESS["WEBF_RES_NO_VALUE"] = "未選中的";
$MESS["WEBF_RES_PART_OF_THE_DAY_AFTERNOON"] = "天";
$MESS["WEBF_RES_PART_OF_THE_DAY_EVENING"] = "晚上";
$MESS["WEBF_RES_PART_OF_THE_DAY_MORNING"] = "早晨";
$MESS["WEBF_RES_PART_OF_THE_DAY_NIGHT"] = "夜晚";
$MESS["WEBF_RES_RESOURCES"] = "資源";
$MESS["WEBF_RES_RESOURCES_LABEL"] = "選擇資源";
$MESS["WEBF_RES_RESOURCE_PLURAL_0"] = "資源";
$MESS["WEBF_RES_RESOURCE_PLURAL_1"] = "資源";
$MESS["WEBF_RES_RESOURCE_PLURAL_2"] = "資源";
$MESS["WEBF_RES_SELECT_ALL"] = "全選";
$MESS["WEBF_RES_SELECT_ALL_SERVICES"] = "選擇所有服務";
$MESS["WEBF_RES_SELECT_DEFAULT_EMPTY"] = "未指定";
$MESS["WEBF_RES_SELECT_DEFAULT_FREE_USER"] = "可用員工";
$MESS["WEBF_RES_SELECT_DEFAULT_TITLE"] = "默認列表項目";
$MESS["WEBF_RES_SELECT_DURATION_AUTO"] = "預設:";
$MESS["WEBF_RES_SELECT_DURATION_BY_DEFAULT"] = "預設:";
$MESS["WEBF_RES_SELECT_DURATION_FROM_LIST_SHORT"] = "從列表中選擇";
$MESS["WEBF_RES_SELECT_MANUAL_INPUT"] = "啟用手動輸入";
$MESS["WEBF_RES_SELECT_RES_FROM_LIST_AUTO"] = "自動選擇可用資源";
$MESS["WEBF_RES_SELECT_RES_FROM_LIST_SHORT"] = "從列表中選擇";
$MESS["WEBF_RES_SELECT_USER"] = "選擇用戶";
$MESS["WEBF_RES_SELECT_USER_FROM_LIST"] = "從列表中選擇用戶";
$MESS["WEBF_RES_SELECT_USER_FROM_LIST_AUTO"] = "自動選擇員工";
$MESS["WEBF_RES_SELECT_USER_FROM_LIST_SHORT"] = "從列表中選擇";
$MESS["WEBF_RES_SERVICES"] = "服務";
$MESS["WEBF_RES_SERVICE_LABEL"] = "選擇服務";
$MESS["WEBF_RES_SERVICE_PLURAL_0"] = "服務";
$MESS["WEBF_RES_SERVICE_PLURAL_1"] = "服務";
$MESS["WEBF_RES_SERVICE_PLURAL_2"] = "服務";
$MESS["WEBF_RES_SETTINGS"] = "設定";
$MESS["WEBF_RES_TIME"] = "時間";
$MESS["WEBF_RES_TIME_BOOKING_SIZE"] = "約會期限";
$MESS["WEBF_RES_TIME_FORMAT_FROM_TO"] = "從＃time_from＃到＃time_to＃";
$MESS["WEBF_RES_TIME_LABEL"] = "選擇時間";
$MESS["WEBF_RES_TIME_SHOW_FINISH_TIME"] = "顯示結束時間";
$MESS["WEBF_RES_TIME_SHOW_FREE_ONLY"] = "僅顯示可用";
$MESS["WEBF_RES_TIME_STYLE"] = "時間樣式";
$MESS["WEBF_RES_TIME_STYLE_SELECT"] = "時間選擇器";
$MESS["WEBF_RES_TIME_STYLE_SLOT"] = "時隙";
$MESS["WEBF_RES_USERS"] = "僱員";
$MESS["WEBF_RES_USERS_LABEL"] = "選擇員工";
$MESS["WEBF_RES_USER_PLURAL_0"] = "員工";
$MESS["WEBF_RES_USER_PLURAL_1"] = "僱員";
$MESS["WEBF_RES_USER_PLURAL_2"] = "僱員";
