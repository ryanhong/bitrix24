<?php
$MESS["CALENDAR_ICAL_INVENT_DESC"] = "
＃date_start＃ - 開始時間
＃date_end＃ - 結束時間
＃消息＃ - 短信
";
$MESS["CALENDAR_ICAL_INVENT_NAME"] = "向用戶發送邀請";
$MESS["CALENDAR_INVENT_MESSAGE_BODY"] = "
<p>讓我邀請您參加事件＃名稱＃#ormanizer＃。</p>

<p>它將從＃date_start＃tall＃date_end＃at #location＃。</p>進行

<p>與會者：#attendees_list＃。</p>

<p>描述：#description＃。</p>

<p>文件：#files＃</p>
";
$MESS["CALENDAR_INVITATION_AUTO_GENERATED"] = "

-------------------------------------------------- --------------------------- ----------------------- ----

此消息已自動生成。";
$MESS["CALENDAR_INVITATION_DESC"] = "

＃email_to＃ - 被邀請的人電子郵件
＃標題＃ - 主題
＃消息＃ - 事件的完整描述
";
$MESS["CALENDAR_INVITATION_NAME"] = "邀請";
$MESS["CALENDAR_REPLY_MESSAGE_BODY"] = "
<p>＃名稱＃答复#answer＃</p>
";
$MESS["CALENDAR_SHARING_EVENT_TYPE_DESC"] = "
＃event_name＃ - 事件名稱
＃event_datetime＃ - 事件日期和時間
＃時區＃ - 時區
＃狀態＃-由日曆所有者確認是否
＃所有者_name＃ - 日曆所有者名稱
＃所有者_PHOTO＃ - 日曆所有者照片URL
＃calendar_weekday＃ -活動工作日
＃日曆_DAY＃ - 每月活動日

＃new_event_link＃ - 新事件創建鏈接
＃ICS_FILE＃ - 下載的ICS文件鏈接
＃cancel_link＃ - 事件取消鏈接
＃videoconference_link＃ - 視頻會議鏈接
";
$MESS["CALENDAR_SHARING_EVENT_TYPE_NAME"] = "給來賓用戶查看開放插槽的消息";
