<?php
$MESS["CAL_DEFAULT_TYPE"] = "事件";
$MESS["CAL_GO_CONVERT"] = "轉換事件日曆";
$MESS["CAL_INSTALL_TITLE"] = "事件日曆模塊安裝";
$MESS["CAL_MODULE_DESCRIPTION"] = "事件日曆";
$MESS["CAL_MODULE_NAME"] = "事件日曆";
$MESS["CAL_MODULE_UNINSTALL_ERROR"] = "無法卸載此模塊。";
$MESS["CAL_MODULE_UNINSTALL_ERROR_CALENDARMOBILE"] = "事件日曆模塊是移動日曆模塊所需的。\ r \ nplease首先卸載移動日曆模塊。";
$MESS["CAL_PHP_L439"] = "您使用的是PHP版本＃ver＃，而模塊需要版本5.0.0或更高版本。請更新您的PHP安裝或聯繫技術支持。";
$MESS["CAL_UNINSTALL_TITLE"] = "事件日曆模塊卸載";
