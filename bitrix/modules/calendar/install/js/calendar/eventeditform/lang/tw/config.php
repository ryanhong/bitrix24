<?php
$MESS["EC_CLOSE_CREATE_FORM_CONFIRM_QUESTION"] = "您確定要關閉活動表格嗎？所有的更改都將丟失。";
$MESS["EC_CLOSE_EDIT_FORM_CONFIRM_OK"] = "是的，關閉表格";
$MESS["EC_CLOSE_EDIT_FORM_CONFIRM_QUESTION"] = "您確定要關閉活動表格嗎？所有的更改都將丟失。";
