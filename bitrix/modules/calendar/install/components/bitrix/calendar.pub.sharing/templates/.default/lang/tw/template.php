<?php
$MESS["EC_CALENDAR_PUB_SHARING_BITRIX24_LOGO_POWERED"] = "由<span class = \“＃class＃\”> </span>提供動力";
$MESS["EC_CALENDAR_PUB_SHARING_COOKIES_ACCEPT"] = "全都接受";
$MESS["EC_CALENDAR_PUB_SHARING_COOKIES_DECLINE"] = "不接受";
$MESS["EC_CALENDAR_PUB_SHARING_COOKIES_TEXT"] = "我們使用cookie文件存儲個性化信息。請同意使用它們來享受各種日曆功能。";
$MESS["EC_CALENDAR_PUB_SHARING_COOKIES_TITLE"] = "餅乾";
$MESS["EC_CALENDAR_PUB_SHARING_FOOTER_REPORT"] = "如果您收到了時間插槽的鏈接，但沒想到，請<a class = \"#class# \" href= \"#href# \">提交報告</a>";
$MESS["EC_CALENDAR_PUB_SHARING_FREE_SITES_AND_CRM"] = "  - 免費<＃tag＃class = \“＃class＃\” href = \“＃href＃\”>任務和crm </＃tag＃>>";
$MESS["EC_CALENDAR_PUB_SHARING_TITLE"] = "日曆";
