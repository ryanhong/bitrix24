<?php
$MESS["CC_BLLE_ACCESS_DENIED"] = "未足夠查看和編輯列表的權限。";
$MESS["CC_BLLE_CHAIN_EDIT"] = "網格設置";
$MESS["CC_BLLE_FIELD_NAME_DEFAULT"] = "新列表";
$MESS["CC_BLLE_FIELD_NAME_DEFAULT_PROCESS"] = "新工作流程";
$MESS["CC_BLLE_LISTS_FOR_SONET_GROUP_DISABLED"] = "該組禁用列表。";
$MESS["CC_BLLE_MODULE_NOT_INSTALLED"] = "未安裝通用列表模塊。";
$MESS["CC_BLLE_NAME_FIELD"] = "姓名";
$MESS["CC_BLLE_TITLE_EDIT"] = "網格設置：＃名稱＃";
$MESS["CC_BLLE_TITLE_EDIT_PROCESS"] = "編輯工作流：＃名稱＃";
$MESS["CC_BLLE_TITLE_NEW"] = "新列表";
$MESS["CC_BLLE_TITLE_NEW_PROCESS"] = "新工作流程";
$MESS["CC_BLLE_UNKNOWN_ERROR"] = "未知錯誤。";
$MESS["CC_BLLE_WRONG_IBLOCK"] = "指定的列表不正確。";
$MESS["CC_BLLE_WRONG_IBLOCK_TYPE"] = "不正確的信息塊類型。";
