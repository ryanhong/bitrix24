<?php
$MESS["CP_BLM_IBLOCK_TYPE_ID"] = "信息塊類型";
$MESS["CP_BLM_IS_SEF"] = "啟用S​​EF模式兼容性";
$MESS["CP_BLM_LIST_ID"] = "列出菜單的ID突出顯示";
$MESS["CP_BLM_LIST_URL"] = "列表URL";
$MESS["CP_BLM_SEF_BASE_URL"] = "SEF的文件夾（站點根搭配）";
$MESS["CP_BLM_SEF_LIST_BASE_URL"] = "列出URL基礎（SEF）";
$MESS["CP_BLM_SEF_LIST_URL"] = "列表URL";
