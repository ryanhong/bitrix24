<?php
$MESS["CC_BLS_ACCESS_DENIED"] = "未足夠查看和編輯列表的權限。";
$MESS["CC_BLS_CHAIN_TITLE"] = "部分管理";
$MESS["CC_BLS_LISTS_FOR_SONET_GROUP_DISABLED"] = "該組禁用列表。";
$MESS["CC_BLS_MODULE_NOT_INSTALLED"] = "未安裝通用列表模塊。";
$MESS["CC_BLS_NEW_SECTION_NAME_PROMPT"] = "輸入新部分的名稱";
$MESS["CC_BLS_PAGE_TITLE"] = "＃名稱＃：部分管理";
$MESS["CC_BLS_SECTION_ACTION_MENU_DELETE"] = "刪除";
$MESS["CC_BLS_SECTION_ACTION_MENU_RENAME"] = "改名";
$MESS["CC_BLS_SECTION_DELETE_PROPMT"] = "這將刪除所有小節和子元素。您確定要刪除該部分嗎？";
$MESS["CC_BLS_UNKNOWN_ERROR"] = "未知錯誤。";
$MESS["CC_BLS_WRONG_IBLOCK"] = "指定的列表不正確。";
$MESS["CC_BLS_WRONG_IBLOCK_TYPE"] = "不正確的信息塊類型。";
