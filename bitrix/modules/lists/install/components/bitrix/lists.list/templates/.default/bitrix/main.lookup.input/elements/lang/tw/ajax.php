<?php
$MESS["CT_BMLI_ACCESS_DENIED"] = "未足夠查看和編輯列表的權限。";
$MESS["CT_BMLI_MODULE_NOT_INSTALLED"] = "未安裝通用列表模塊。";
$MESS["CT_BMLI_UNKNOWN_ERROR"] = "未知錯誤。";
$MESS["CT_BMLI_WRONG_IBLOCK"] = "指定的列表不正確。";
$MESS["CT_BMLI_WRONG_IBLOCK_TYPE"] = "不正確的信息塊類型。";
