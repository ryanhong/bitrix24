<?php
$MESS["CC_BLEE_ACCESS_DENIED"] = "未足夠查看和編輯列表的權限。";
$MESS["CC_BLEE_ACCESS_DENIED_STATUS"] = "您沒有足夠的權限來編輯此記錄的當前狀態";
$MESS["CC_BLEE_DELETE_ERROR"] = "刪除時錯誤。";
$MESS["CC_BLEE_ELEMENT_LOCKED"] = "項目已暫時鎖定";
$MESS["CC_BLEE_FIELD_NAME_DEFAULT"] = "姓名";
$MESS["CC_BLEE_IS_CONSTANTS_TUNED"] = "工作流程常數需要配置。";
$MESS["CC_BLEE_IS_CONSTANTS_TUNED_NEW"] = "必須配置工作流參數。";
$MESS["CC_BLEE_LISTS_FOR_SONET_GROUP_DISABLED"] = "該組禁用列表。";
$MESS["CC_BLEE_MODULE_NOT_INSTALLED"] = "未安裝通用列表模塊。";
$MESS["CC_BLEE_SONET_DEL_LOG_TITLE_TEMPLATE"] = "刪除列表項目\“＃標題＃\”";
$MESS["CC_BLEE_SONET_LOG_TITLE_TEMPLATE"] = "添加或更新列表項目\“＃標題＃\”";
$MESS["CC_BLEE_UNKNOWN_ERROR"] = "未知錯誤。";
$MESS["CC_BLEE_UPPER_LEVEL"] = "更高級別";
$MESS["CC_BLEE_VALIDATE_FIELD_ERROR"] = "“＃名稱＃”字段的值不正確";
$MESS["CC_BLEE_WRONG_IBLOCK"] = "指定的列表不正確。";
$MESS["CC_BLEE_WRONG_IBLOCK_TYPE"] = "不正確的信息塊類型。";
