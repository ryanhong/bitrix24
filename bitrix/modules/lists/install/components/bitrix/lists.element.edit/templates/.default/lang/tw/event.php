<?php
$MESS["LISTS_EDIT_EVENT_CANCELED"] = "動作已取消。現在，您被重定向到上一頁。如果當前頁面仍在顯示，請手動關閉它。";
$MESS["LISTS_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "已經創建了項目<a href='#url#'>＃標題＃</a>。現在，您被重定向到上一頁。如果當前頁面仍在顯示，請手動關閉它。";
