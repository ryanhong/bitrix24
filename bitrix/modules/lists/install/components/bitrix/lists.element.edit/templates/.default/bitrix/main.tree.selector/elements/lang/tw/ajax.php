<?php
$MESS["CT_BMTS_ACCESS_DENIED"] = "未足夠查看和編輯列表的權限。";
$MESS["CT_BMTS_CANCEL"] = "取消";
$MESS["CT_BMTS_MODULE_NOT_INSTALLED"] = "未安裝通用列表模塊。";
$MESS["CT_BMTS_SUBMIT"] = "選擇";
$MESS["CT_BMTS_UNKNOWN_ERROR"] = "未知錯誤。";
$MESS["CT_BMTS_WAIT"] = "載入中...";
$MESS["CT_BMTS_WINDOW_CLOSE"] = "關閉";
$MESS["CT_BMTS_WRONG_IBLOCK"] = "指定的列表不正確。";
$MESS["CT_BMTS_WRONG_IBLOCK_TYPE"] = "不正確的信息塊類型。";
