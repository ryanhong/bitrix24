<?php
$MESS["CC_BLF_ACCESS_DENIED"] = "未足夠查看和編輯列表的權限。";
$MESS["CC_BLF_LISTS_FOR_SONET_GROUP_DISABLED"] = "該組禁用列表。";
$MESS["CC_BLF_MODULE_NOT_INSTALLED"] = "未安裝通用列表模塊。";
$MESS["CC_BLF_UNKNOWN_ERROR"] = "未知錯誤。";
$MESS["CC_BLF_WRONG_FILE"] = "文件未找到。";
$MESS["CC_BLF_WRONG_IBLOCK"] = "指定的列表不正確。";
$MESS["CC_BLF_WRONG_IBLOCK_TYPE"] = "不正確的信息塊類型。";
