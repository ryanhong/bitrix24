<?php
$MESS["BPULDA_DOC_TYPE"] = "實體類型";
$MESS["BPULDA_DT_LISTS"] = "列表";
$MESS["BPULDA_DT_LISTS_SOCNET"] = "工作組和項目列表";
$MESS["BPULDA_DT_PROCESSES"] = "過程";
$MESS["BPULDA_ELEMENT_ID"] = "項目ID";
$MESS["BPULDA_ERROR_DT"] = "不正確的實體類型。";
$MESS["BPULDA_ERROR_ELEMENT_ID"] = "項目ID缺少";
$MESS["BPULDA_ERROR_FIELDS"] = "未指定項目字段";
$MESS["BPULDA_FIELD_REQUIED"] = "需要字段'＃字段＃'。";
$MESS["BPULDA_WRONG_TYPE"] = "“＃param＃”參數類型不確定。";
