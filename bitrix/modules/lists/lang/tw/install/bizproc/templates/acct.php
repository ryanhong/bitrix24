<?php
$MESS["LIBTA_AAQD1"] = "您需要批准或拒絕發票

名稱：{=文檔：名稱}
在：{= document：date_create}上創建
創建者：{= document：create_by_printable}
類型：{= document：property_type}
發票編號和日期：{= document：property_num_date}
金額：{= document：property_sum}
預算項目：{=文檔：property_bdt}
文件：{=變量：domain} {= document：property_file}

{=變量：link} {= document：id}/";
$MESS["LIBTA_AAQD2"] = "您需要批准或拒絕發票付款

批准：{= variable：prainver_printable}
創建者：{= document：create_by_printable}
在：{= document：date_create}上創建
標題：{=文檔：名稱}
類型：{= document：property_type}
發票編號和日期：{= document：property_num_date}
金額：{= document：property_sum}
預算項目：{=文檔：property_bdt}
文件：{=變量：domain} {= document：property_file}

{=變量：link} {= document：id}/";
$MESS["LIBTA_AAQN1"] = "批准發票\“ {= document：name} \”";
$MESS["LIBTA_AAQN2"] = "批准發票付款\“ {= document：name} \”";
$MESS["LIBTA_APPROVED"] = "得到正式認可的";
$MESS["LIBTA_APPROVED_N"] = "不批准";
$MESS["LIBTA_APPROVED_R"] = "拒絕";
$MESS["LIBTA_APPROVED_Y"] = "得到正式認可的";
$MESS["LIBTA_BDT"] = "預算項目";
$MESS["LIBTA_BP_TITLE"] = "發票";
$MESS["LIBTA_CREATED_BY"] = "由...製作";
$MESS["LIBTA_DATE_CREATE"] = "創建於";
$MESS["LIBTA_DATE_PAY"] = "付款日期（由簿記員提供）";
$MESS["LIBTA_DOCS"] = "文件副本";
$MESS["LIBTA_DOCS_NO"] = "不";
$MESS["LIBTA_DOCS_YES"] = "是的";
$MESS["LIBTA_FILE"] = "文件（發票副本）";
$MESS["LIBTA_NAME"] = "姓名";
$MESS["LIBTA_NUM_DATE"] = "發票號碼和日期";
$MESS["LIBTA_NUM_PP"] = "付款訂單號（由簿記員提供）";
$MESS["LIBTA_PAID"] = "有薪酬的";
$MESS["LIBTA_PAID_NO"] = "不";
$MESS["LIBTA_PAID_YES"] = "是的";
$MESS["LIBTA_RIA10_DESCR"] = "支付發票

已確認付款：{=變量：paymentapprover_printable}
批准發票：{= variable：apryver_printable}
創建者：{= document：create_by_printable}
在：{= document：date_create}上創建
標題：{=文檔：名稱}
類型：{= document：property_type}
發票編號和日期：{= document：property_num_date}
金額：{= document：property_sum}
預算項目：{=文檔：property_bdt}
文件：{=變量：domain} {= document：property_file}

{=變量：link} {= document：id}/";
$MESS["LIBTA_RIA10_NAME"] = "支付發票\“ {= document：name} \”";
$MESS["LIBTA_RIA10_R1"] = "付款日期";
$MESS["LIBTA_RIA10_R2"] = "付款訂單號";
$MESS["LIBTA_RRA15_DESCR"] = "收集文檔

已確認付款：{=變量：paymentapprover_printable}
批准發票：{= variable：apryver_printable}
創建者：{= document：create_by_printable}
在：{= document：date_create}上創建
標題：{=文檔：名稱}
類型：{= document：property_type}
發票編號和日期：{= document：property_num_date}
金額：{= document：property_sum}
預算項目：{=文檔：property_bdt}
文件：{=變量：domain} {= document：property_file}

{=變量：link} {= document：id}/";
$MESS["LIBTA_RRA15_NAME"] = "在\“ {= document：name} \”上收集文檔";
$MESS["LIBTA_RRA15_SM"] = "收集文件";
$MESS["LIBTA_RRA15_TASKBUTTON"] = "收集的文檔";
$MESS["LIBTA_RRA17_BUTTON"] = "收到的文檔";
$MESS["LIBTA_RRA17_DESCR"] = "我在此確認收到發票文檔的收到。

付款日期：{=文檔：property_date_pay}
付款奧德號號碼：{= document：property_num_pay}
已確認付款：{=變量：paymentapprover_printable}
批准發票：{= variable：apryver_printable}
創建者：{= document：create_by_printable}
在：{= document：date_create}上創建
標題：{=文檔：名稱}
類型：{= document：property_type}
發票編號和日期：{= document：property_num_date}
金額：{= document：property_sum}
預算項目：{=文檔：property_bdt}
文件：{=變量：domain} {= document：property_file}

{=變量：link} {= document：id}/";
$MESS["LIBTA_RRA17_NAME"] = "在\“ {= document：name} \”上確認文檔的收據";
$MESS["LIBTA_SMA_MESSAGE_1"] = "請批准發票
創建者：{= document：create_by_printable}
標題：{=文檔：名稱}
類型：{= document：property_type}
金額：{= document：property_sum}

{=變量：link} {= document：id}/";
$MESS["LIBTA_SMA_MESSAGE_2"] = "我批准發票

在：{= document：date_create}上創建
標題：{=文檔：名稱}
類型：{= document：property_type}

{=變量：link} {= document：id}/";
$MESS["LIBTA_SMA_MESSAGE_3"] = "請批准發票付款

批准：{= variable：prainver_printable}
創建者：{= document：create_by_printable}
標題：{=文檔：名稱}
類型：{= document：property_type}
發票編號和日期：{= document：property_num_date}
金額：{= document：property_sum}

{=變量：link} {= document：id}/

任務：
{=變量：taskslink}";
$MESS["LIBTA_SMA_MESSAGE_4"] = "發票付款已確認

在：{= document：date_create}上創建
標題：{=文檔：名稱}
類型：{= document：property_type}

{=變量：link} {= document：id}/";
$MESS["LIBTA_SMA_MESSAGE_5"] = "請支付發票

已確認付款：{=變量：paymentapprover_printable}
批准發票：{= variable：apryver_printable}
創建者：{= document：create_by_printable}
標題：{=文檔：名稱}
類型：{= document：property_type}
發票編號和日期：{= document：property_num_date}
金額：{= document：property_sum}
預算項目：{=文檔：property_bdt}

{=變量：link} {= document：id}/

任務：
{=變量：taskslink}";
$MESS["LIBTA_SMA_MESSAGE_6"] = "支付發票；需要文檔。

在：{= document：date_create}上創建
標題：{=文檔：名稱}
類型：{= document：property_type}
";
$MESS["LIBTA_SMA_MESSAGE_7"] = "收集的所有發票文件

付款日期：{=文檔：property_date_pay}
付款奧德號號碼：{= document：property_num_pay}
創建者：{= document：create_by_printable}
在：{= document：date_create}上創建
標題：{=文檔：名稱}
類型：{= document：property_type}
發票編號和日期：{= document：property_num_date}
金額：{= document：property_sum}

{=變量：link} {= document：id}/

任務：
{=變量：taskslink}";
$MESS["LIBTA_SMA_MESSAGE_8"] = "收到的文檔。

在：{= document：date_create}上創建
標題：{=文檔：名稱}
類型：{= document：property_type}

{=變量：link} {= document：id}/";
$MESS["LIBTA_SMA_MESSAGE_9"] = "付款未確認

在：{= document：date_create}上創建
標題：{=文檔：名稱}
類型：{= document：property_type}

{=變量：link} {= document：id}/";
$MESS["LIBTA_SMA_MESSAGE_10"] = "發票未批准

在：{= document：date_create}上創建
標題：{=文檔：名稱}
類型：{= document：property_type}

{=變量：link} {= document：id}/";
$MESS["LIBTA_STATE1"] = "被批准";
$MESS["LIBTA_STATE2"] = "批准（{= variable：pricer_printable}）";
$MESS["LIBTA_STATE3"] = "未批准（{= actible：pricer_printable}）";
$MESS["LIBTA_STATE4"] = "付款正在確認";
$MESS["LIBTA_STATE5"] = "付款確認成功";
$MESS["LIBTA_STATE6"] = "付款等待中";
$MESS["LIBTA_STATE7"] = "有薪酬的";
$MESS["LIBTA_STATE8"] = "關閉";
$MESS["LIBTA_STATE9"] = "付款拒絕";
$MESS["LIBTA_SUM"] = "數量";
$MESS["LIBTA_TYPE"] = "類型";
$MESS["LIBTA_TYPE_ADV"] = "廣告";
$MESS["LIBTA_TYPE_C"] = "可報銷費用";
$MESS["LIBTA_TYPE_D"] = "其他";
$MESS["LIBTA_TYPE_EX"] = "表示津貼";
$MESS["LIBTA_T_AAQN1"] = "批准發票付款";
$MESS["LIBTA_T_AAQN2"] = "發票付款確認";
$MESS["LIBTA_T_ASFA1"] = "設置文檔的\“批准\”字段";
$MESS["LIBTA_T_ASFA2"] = "設置文檔的\“批准\”字段";
$MESS["LIBTA_T_ASFA3"] = "設置文檔的\“批准\”字段";
$MESS["LIBTA_T_ASFA4"] = "編輯文檔";
$MESS["LIBTA_T_ASFA5"] = "編輯文檔";
$MESS["LIBTA_T_GUAX1"] = "選擇主管";
$MESS["LIBTA_T_IFELSEA1"] = "到達主管";
$MESS["LIBTA_T_IFELSEA2"] = "批准發票";
$MESS["LIBTA_T_IFELSEBA1"] = "是的";
$MESS["LIBTA_T_IFELSEBA2"] = "不";
$MESS["LIBTA_T_IFELSEBA3"] = "是的";
$MESS["LIBTA_T_IFELSEBA4"] = "不";
$MESS["LIBTA_T_PBP"] = "順序業務流程";
$MESS["LIBTA_T_PDA1"] = "發布文檔";
$MESS["LIBTA_T_RIA10"] = "支付發票";
$MESS["LIBTA_T_RRA15"] = "文件";
$MESS["LIBTA_T_RRA17_NAME"] = "收到的文檔";
$MESS["LIBTA_T_SA0"] = "動作序列";
$MESS["LIBTA_T_SMA_MESSAGE_1"] = "消息：發票批准請求";
$MESS["LIBTA_T_SMA_MESSAGE_2"] = "消息：批准發票";
$MESS["LIBTA_T_SMA_MESSAGE_3"] = "消息：付款確認";
$MESS["LIBTA_T_SMA_MESSAGE_4"] = "消息：已確認付款";
$MESS["LIBTA_T_SMA_MESSAGE_5"] = "消息：發票";
$MESS["LIBTA_T_SMA_MESSAGE_6"] = "消息：發票已支付";
$MESS["LIBTA_T_SMA_MESSAGE_7"] = "消息：收集的發票文檔";
$MESS["LIBTA_T_SMA_MESSAGE_8"] = "消息：收到的文檔";
$MESS["LIBTA_T_SMA_MESSAGE_9"] = "消息：未確認付款";
$MESS["LIBTA_T_SMA_MESSAGE_10"] = "消息：發票未批准";
$MESS["LIBTA_T_SPA1"] = "設置權限：作者";
$MESS["LIBTA_T_SPAX1"] = "設置了：主管的權限";
$MESS["LIBTA_T_SPAX2"] = "設置權限：批准經理";
$MESS["LIBTA_T_SPAX3"] = "設置：付款人的權限";
$MESS["LIBTA_T_SPAX4"] = "設置了：簿記員的權限";
$MESS["LIBTA_T_SPAX5"] = "設置權限：最終";
$MESS["LIBTA_T_SSTA1"] = "狀態：被批准";
$MESS["LIBTA_T_SSTA2"] = "狀態：批准";
$MESS["LIBTA_T_SSTA3"] = "狀態：未批准";
$MESS["LIBTA_T_SSTA4"] = "狀態：付款正在確認";
$MESS["LIBTA_T_SSTA5"] = "狀態：確認付款";
$MESS["LIBTA_T_SSTA6"] = "狀態：付款未決";
$MESS["LIBTA_T_SSTA7"] = "狀態：發票已支付";
$MESS["LIBTA_T_SSTA8"] = "狀態::發票關閉";
$MESS["LIBTA_T_SSTA9"] = "狀態：付款減少";
$MESS["LIBTA_T_SVWA1"] = "設置主管";
$MESS["LIBTA_T_SVWA2"] = "設置主管";
$MESS["LIBTA_T_SVWA3"] = "設置變量";
$MESS["LIBTA_T_WHILEA1"] = "批准週期";
$MESS["LIBTA_T_XMA_MESSAGES_1"] = "消息：發票批准";
$MESS["LIBTA_T_XMA_MESSAGES_2"] = "消息：付款確認";
$MESS["LIBTA_T_XMA_MESSAGES_3"] = "消息：發票";
$MESS["LIBTA_V_APPR"] = "批准的付款";
$MESS["LIBTA_V_APPRU"] = "導師";
$MESS["LIBTA_V_BK"] = "會計部（批准）";
$MESS["LIBTA_V_BKD"] = "會計部（文檔）";
$MESS["LIBTA_V_BKP"] = "會計部（付款）";
$MESS["LIBTA_V_DOMAIN"] = "領域";
$MESS["LIBTA_V_LINK"] = "關聯";
$MESS["LIBTA_V_MAPPR"] = "管理委員會（批准）";
$MESS["LIBTA_V_MNG"] = "管理委員會";
$MESS["LIBTA_V_PDATE"] = "付款日期";
$MESS["LIBTA_V_PNUM"] = "付款訂單號";
$MESS["LIBTA_V_TLINK"] = "鏈接到任務";
$MESS["LIBTA_XMA_MESSAGES_1"] = "BIP：發票批准";
$MESS["LIBTA_XMA_MESSAGES_2"] = "BIP：付款確認";
$MESS["LIBTA_XMA_MESSAGES_3"] = "BIP：發票";
$MESS["LIBTA_XMA_MESSAGET_1"] = "請批准發票

創建者：{= document：create_by_printable}
在：{= document：date_create}上創建
標題：{=文檔：名稱}
類型：{= document：property_type}
發票編號和日期：{= document：property_num_date}
金額：{= document：property_sum}
預算項目：{=文檔：property_bdt}

{=變量：link} {= document：id}/


業務流程任務：
{=變量：taskslink}";
$MESS["LIBTA_XMA_MESSAGET_2"] = "請確認發票付款

批准：{= variable：prainver_printable}
創建者：{= document：create_by_printable}
在：{= document：date_create}上創建
標題：{=文檔：名稱}
類型：{= document：property_type}
發票編號和日期：{= document：property_num_date}
金額：{= document：property_sum}
預算項目：{=文檔：property_bdt}

{=變量：link} {= document：id}/

任務：
{=變量：taskslink}";
$MESS["LIBTA_XMA_MESSAGET_3"] = "請支付發票

已確認付款：{=變量：paymentapprover_printable}
批准發票：{= variable：apryver_printable}
創建者：{= document：create_by_printable}
在：{= document：date_create}上創建
標題：{=文檔：名稱}
類型：{= document：property_type}
發票編號和日期：{= document：property_num_date}
金額：{= document：property_sum}
預算項目：{=文檔：property_bdt}

{=變量：link} {= document：id}/

任務：
{=變量：taskslink}";
