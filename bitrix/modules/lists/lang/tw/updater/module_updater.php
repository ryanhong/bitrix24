<?php
$MESS["ELEMENT_NAME"] = "元素";
$MESS["MU_IBLOCK_DESCRIPTION_CASH"] = "僅填寫幾個字段來發送現金貨幣請求。一旦您的請求被批准或拒絕，您將收到一條通知消息。

現在就試試！";
$MESS["MU_IBLOCK_DESCRIPTION_HOLIDAY"] = "沒有更多的繁文tape節！僅通過填寫幾個字段來發送假期請求。您的請求將發送給您的主管，然後通過完全批准所需的階段。批准您的請求後，您將收到一條通知消息。

現在就試試！";
$MESS["MU_IBLOCK_DESCRIPTION_INBOX"] = "上傳傳入的文件或填寫所需的字段，然後將其發送給管理。該文檔將被分配一個條目號，並且將立即將行政助理通知新文檔。

現在就試試！";
$MESS["MU_IBLOCK_DESCRIPTION_MISSION"] = "選擇商務旅行的開始和結束日期，進入目的地城市，並描述旅行的目的。業務流程將將您的申請發送給您的主管。申請批准後，您將收到一條通知消息。

現在就試試！";
$MESS["MU_IBLOCK_DESCRIPTION_OUTBOX"] = "註冊一個傳出文檔，指定sendign方法和響應時間。

該文檔將被分配一個傳出號碼；秘書將收到新文件的通知，並將其發送給收件人。

現在就試試！";
$MESS["MU_IBLOCK_DESCRIPTION_PAYING"] = "通過僅填寫幾個字段來發送付款請求，或者只附加發票的掃描圖像。它將發送給您的主管，然後通過最終批准所需的所有步驟。付款後，您將獲得確認。

現在就試試！";
$MESS["MU_IBLOCK_ELEMENTS_NAME"] = "元素";
$MESS["MU_IBLOCK_ELEMENT_ADD"] = "添加元素";
$MESS["MU_IBLOCK_ELEMENT_DELETE"] = "刪除元素";
$MESS["MU_IBLOCK_ELEMENT_EDIT"] = "編輯元素";
$MESS["MU_IBLOCK_ELEMENT_NAME"] = "元素";
$MESS["MU_IBLOCK_FIELD_LIST_NO"] = "不";
$MESS["MU_IBLOCK_FIELD_LIST_YES"] = "是的";
$MESS["MU_IBLOCK_FIELD_NAME"] = "姓名";
$MESS["MU_IBLOCK_NAME_CASH"] = "請求現金";
$MESS["MU_IBLOCK_NAME_FIELD"] = "姓名";
$MESS["MU_IBLOCK_NAME_HOLIDAY"] = "休假要求";
$MESS["MU_IBLOCK_NAME_INBOX"] = "註冊傳入文件";
$MESS["MU_IBLOCK_NAME_MISSION"] = "商務旅行申請";
$MESS["MU_IBLOCK_NAME_OUTBOX"] = "外向文件";
$MESS["MU_IBLOCK_NAME_PAYING"] = "應付發票";
$MESS["MU_IBLOCK_SECTIONS_NAME"] = "部分";
$MESS["MU_IBLOCK_SECTION_ADD"] = "添加部分";
$MESS["MU_IBLOCK_SECTION_DELETE"] = "刪除部分";
$MESS["MU_IBLOCK_SECTION_EDIT"] = "編輯部分";
$MESS["MU_IBLOCK_SECTION_NAME"] = "部分";
$MESS["MU_MENU_TITLE_MY_PROCESSES"] = "我的要求";
$MESS["MU_MENU_TITLE_PROCESS"] = "工作流程";
$MESS["NAME"] = "工作流程";
$MESS["SECTION_NAME"] = "部分";
