<?php
$MESS["LISTS_DEL_SOCNET_LOG_GROUP"] = "列表（刪除）";
$MESS["LISTS_DEL_SOCNET_LOG_TITLE"] = "刪除列表項目\“＃標題＃\”";
$MESS["LISTS_DEL_SOCNET_LOG_TITLE_MAIL"] = "刪除的列表項目\“＃標題＃\”在組\“＃entity＃\”中";
$MESS["LISTS_SOCNET_LOG_GROUP"] = "列表";
$MESS["LISTS_SOCNET_LOG_GROUP_SETTINGS"] = "該組列表的所有更改";
$MESS["LISTS_SOCNET_LOG_TITLE"] = "添加或更新列表項目\“＃標題＃\”";
$MESS["LISTS_SOCNET_LOG_TITLE_MAIL"] = "在組\“＃entity＃\”中添加或更新列表項目\“＃標題＃\”";
$MESS["LISTS_SOCNET_TAB"] = "列表";
$MESS["LISTS_STEPPER_PROGRESS_ERROR"] = "這些ID的列表無法複製：";
$MESS["LISTS_STEPPER_PROGRESS_TITLE"] = "複製列表";
