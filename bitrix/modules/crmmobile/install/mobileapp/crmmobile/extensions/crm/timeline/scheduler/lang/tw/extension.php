<?php
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_DEADLINE"] = "最後期限";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_MENU_FULL_TITLE"] = "計劃活動";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_MENU_TITLE"] = "活動";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_PLACEHOLDER"] = "要做的事情";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_REMINDER_BANNER_DESCRIPTION"] = "安排會議或電話，發送發票或創建任何其他活動";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_REMINDER_BANNER_TITLE"] = "計劃下一步";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_REMINDER_DEFAULT_TEXT"] = "聯繫客戶";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_REMINDER_SKIP_DAY"] = "今天";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_REMINDER_SKIP_FOREVER"] = "絕不";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_REMINDER_SKIP_MONTH"] = "這個月";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_REMINDER_SKIP_TITLE2"] = "不要再建議";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_REMINDER_SKIP_TITLE2_COMPANY"] = "不要再建議（對於所有公司）";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_REMINDER_SKIP_TITLE2_CONTACT"] = "不要再建議（所有聯繫人）";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_REMINDER_SKIP_TITLE2_DEAL"] = "不要再建議（對於管道中的所有交易）";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_REMINDER_SKIP_TITLE2_LEAD"] = "不要再建議（所有潛在客戶）";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_REMINDER_SKIP_TITLE2_QUOTE_MSGVER_1"] = "不要再建議（所有估計）";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_REMINDER_SKIP_TITLE2_SMART_INVOICE"] = "不要再建議（所有發票）";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_REMINDER_SKIP_WEEK"] = "本星期";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_REMINDER_TITLE"] = "主動CRM";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_SAVE"] = "節省";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_SAVE_PROGRESS"] = "保存...";
$MESS["M_CRM_TIMELINE_SCHEDULER_ACTIVITY_TITLE"] = "新活動";
$MESS["M_CRM_TIMELINE_SCHEDULER_ATTACHMENTS_BODY"] = "即將推出！";
$MESS["M_CRM_TIMELINE_SCHEDULER_ATTACHMENTS_TITLE"] = "附件";
$MESS["M_CRM_TIMELINE_SCHEDULER_BADGE_NEW_TITLE"] = "新的";
$MESS["M_CRM_TIMELINE_SCHEDULER_CALL_MENU_TITLE"] = "稱呼";
$MESS["M_CRM_TIMELINE_SCHEDULER_CALL_TITLE"] = "新電話";
$MESS["M_CRM_TIMELINE_SCHEDULER_COMMENT_CREATE"] = "發送";
$MESS["M_CRM_TIMELINE_SCHEDULER_COMMENT_CREATE_PROGRESS"] = "發送...";
$MESS["M_CRM_TIMELINE_SCHEDULER_COMMENT_MENU_FULL_TITLE"] = "留下評論";
$MESS["M_CRM_TIMELINE_SCHEDULER_COMMENT_MENU_TITLE"] = "評論";
$MESS["M_CRM_TIMELINE_SCHEDULER_COMMENT_PLACEHOLDER_2"] = "發表評論";
$MESS["M_CRM_TIMELINE_SCHEDULER_COMMENT_TITLE"] = "新評論";
$MESS["M_CRM_TIMELINE_SCHEDULER_DESKTOP_VERSION"] = "Web CRM";
$MESS["M_CRM_TIMELINE_SCHEDULER_DOCUMENT_MENU_FULL_TITLE"] = "創建文檔";
$MESS["M_CRM_TIMELINE_SCHEDULER_DOCUMENT_MENU_TITLE"] = "文件";
$MESS["M_CRM_TIMELINE_SCHEDULER_DOCUMENT_PREVIOUS_NUMBER_BODY"] = "您已經有一個使用此模板創建的文檔。您是否要為此文檔分配一個新號碼或使用已創建的文檔的編號？";
$MESS["M_CRM_TIMELINE_SCHEDULER_DOCUMENT_PREVIOUS_NUMBER_NEW_BUTTON"] = "創建新的";
$MESS["M_CRM_TIMELINE_SCHEDULER_DOCUMENT_PREVIOUS_NUMBER_OLD_BUTTON"] = "使用以前";
$MESS["M_CRM_TIMELINE_SCHEDULER_DOCUMENT_PREVIOUS_NUMBER_TITLE"] = "選擇號碼";
$MESS["M_CRM_TIMELINE_SCHEDULER_DOCUMENT_SHORT_TITLE"] = "文件";
$MESS["M_CRM_TIMELINE_SCHEDULER_DOCUMENT_TITLE"] = "新文檔";
$MESS["M_CRM_TIMELINE_SCHEDULER_GTC_MENU_FULL_TITLE"] = "邀請聊天";
$MESS["M_CRM_TIMELINE_SCHEDULER_GTC_MENU_TITLE"] = "邀請聊天";
$MESS["M_CRM_TIMELINE_SCHEDULER_GTC_TITLE"] = "邀請聊天";
$MESS["M_CRM_TIMELINE_SCHEDULER_MAIL_MENU_DISABLED"] = "電子郵件未連接";
$MESS["M_CRM_TIMELINE_SCHEDULER_MAIL_MENU_FULL_TITLE"] = "發電子郵件";
$MESS["M_CRM_TIMELINE_SCHEDULER_MAIL_MENU_TITLE_MSGVER_1"] = "電子郵件";
$MESS["M_CRM_TIMELINE_SCHEDULER_MAIL_TITLE"] = "新的電子郵件";
$MESS["M_CRM_TIMELINE_SCHEDULER_MEETING_MENU_TITLE"] = "會議";
$MESS["M_CRM_TIMELINE_SCHEDULER_MEETING_TITLE"] = "新會議";
$MESS["M_CRM_TIMELINE_SCHEDULER_MENTION_USER_TITLE"] = "選擇用戶";
$MESS["M_CRM_TIMELINE_SCHEDULER_PAYMENT_MENU_FULL_TITLE"] = "收到付款";
$MESS["M_CRM_TIMELINE_SCHEDULER_PAYMENT_MENU_TITLE"] = "支付";
$MESS["M_CRM_TIMELINE_SCHEDULER_REMINDERS_EMPTY"] = "設置提醒";
$MESS["M_CRM_TIMELINE_SCHEDULER_REMINDERS_TITLE_MSGVER_1"] = "提醒";
$MESS["M_CRM_TIMELINE_SCHEDULER_SHARING_MENU_FULL_TITLE"] = "分享時間插槽";
$MESS["M_CRM_TIMELINE_SCHEDULER_SHARING_MENU_TITLE"] = "老虎機";
$MESS["M_CRM_TIMELINE_SCHEDULER_SHARING_TITLE"] = "時間表會議";
$MESS["M_CRM_TIMELINE_SCHEDULER_SMS_MENU_FULL_TITLE"] = "發簡訊";
$MESS["M_CRM_TIMELINE_SCHEDULER_SMS_MENU_TITLE"] = "簡訊";
$MESS["M_CRM_TIMELINE_SCHEDULER_SMS_TITLE_2"] = "SMS/WhatsApp";
$MESS["M_CRM_TIMELINE_SCHEDULER_TASK_MENU_FULL_TITLE"] = "創建任務";
$MESS["M_CRM_TIMELINE_SCHEDULER_TASK_MENU_TITLE"] = "任務";
$MESS["M_CRM_TIMELINE_SCHEDULER_TASK_TITLE"] = "新任務";
$MESS["M_CRM_TIMELINE_SCHEDULER_TODAY"] = "今天";
$MESS["M_CRM_TIMELINE_SCHEDULER_TOMORROW"] = "明天";
$MESS["M_CRM_TIMELINE_SCHEDULER_VOICE_NOTES_BODY"] = "即將推出！";
$MESS["M_CRM_TIMELINE_SCHEDULER_VOICE_NOTES_TITLE"] = "語音記事";
