<?php
$MESS["M_CRM_TL_PAYMENT_DETAILS_BACK_TO_PAYMENT_METHOD"] = "返回付款方式";
$MESS["M_CRM_TL_PAYMENT_PAY_DEFAULT_ERROR_BUTTON_CONFIRM_TITLE"] = "好的";
$MESS["M_CRM_TL_PAYMENT_PAY_DEFAULT_ERROR_BUTTON_HELP_TITLE"] = "細節";
$MESS["M_CRM_TL_PAYMENT_PAY_DEFAULT_ERROR_MESSAGE"] = "檢查您的互聯網連接，然後重試。如果問題仍然存在，請聯繫您的BitRix24管理員。";
$MESS["M_CRM_TL_PAYMENT_PAY_DEFAULT_ERROR_TITLE"] = "無法完成動作";
$MESS["M_CRM_TL_PAYMENT_PAY_FAILURE_CHOOSE_OTHER_PAYMENT_METHOD"] = "選擇其他付款方式";
$MESS["M_CRM_TL_PAYMENT_PAY_FAILURE_ERROR_TEXT"] = "無法處理付款";
$MESS["M_CRM_TL_PAYMENT_PAY_FAILURE_GET_NEW_QR_GET_NEW_QR"] = "獲取新的QR碼";
$MESS["M_CRM_TL_PAYMENT_PAY_PAYMENT_SYSTEM_CREATION_ERROR_MESSAGE"] = "錯誤創建付款系統";
$MESS["M_CRM_TL_PAYMENT_PAY_PLEASE_WAIT"] = "請稍等";
$MESS["M_CRM_TL_PAYMENT_PAY_PRODUCTS_CNT"] = "產品：＃CNT＃";
$MESS["M_CRM_TL_PAYMENT_PAY_SCAN_QR"] = "要求客戶用手機掃描QR碼";
$MESS["M_CRM_TL_PAYMENT_PAY_SUCCESS_BACK_TO_LIST"] = "返回付款";
$MESS["M_CRM_TL_PAYMENT_PAY_SUCCESS_PAYMENT_PAID"] = "付款##號碼＃已完成";
$MESS["M_CRM_TL_PAYMENT_PAY_VIA_QR_FAST_MONEY_TRANSFER"] = "使用SBP QR碼付款";
$MESS["M_CRM_TL_PAYMENT_PAY_VIA_QR_PAYMENT_LINK"] = "使用鏈接付款";
$MESS["M_CRM_TL_PAYMENT_PAY_VIA_QR_SBERBANK"] = "使用Sberbank QR碼付款";
