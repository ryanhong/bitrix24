<?php
$MESS["FIELDS_PHONE_DUPLICATE_WARNING_EMAIL"] = "在電子郵件中找到＃entity_total_text＃";
$MESS["FIELDS_PHONE_DUPLICATE_WARNING_LAST_NAME"] = "找到＃entity_total_text＃在名稱中";
$MESS["FIELDS_PHONE_DUPLICATE_WARNING_MORE"] = "了解更多";
$MESS["FIELDS_PHONE_DUPLICATE_WARNING_NAME"] = "找到＃entity_total_text＃在名稱中";
$MESS["FIELDS_PHONE_DUPLICATE_WARNING_PHONE"] = "找到＃entity_total_text＃在電話號碼中";
$MESS["FIELDS_PHONE_DUPLICATE_WARNING_SECOND_NAME"] = "找到＃entity_total_text＃在名稱中";
$MESS["FIELDS_PHONE_DUPLICATE_WARNING_TITLE"] = "找到＃entity_total_text＃在標題中";
