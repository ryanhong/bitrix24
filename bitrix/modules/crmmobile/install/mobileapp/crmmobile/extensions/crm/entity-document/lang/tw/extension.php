<?php
$MESS["M_CRM_ENTITY_DOCUMENT_DEFAULT_TITLE"] = "記錄";
$MESS["M_CRM_ENTITY_DOCUMENT_DELIVERY_TITLE"] = "交付## account_number＃，＃日期＃";
$MESS["M_CRM_ENTITY_DOCUMENT_DISCOUNT"] = "折扣";
$MESS["M_CRM_ENTITY_DOCUMENT_PAYMENT_TITLE"] = "付款## account_number＃，＃日期＃";
$MESS["M_CRM_ENTITY_DOCUMENT_RESEND_LINK"] = "重發";
$MESS["M_CRM_ENTITY_DOCUMENT_SEND"] = "發送";
$MESS["M_CRM_ENTITY_DOCUMENT_SUMMARY_DELIVERY_TITLE"] = "交貨價格";
$MESS["M_CRM_ENTITY_DOCUMENT_SUMMARY_PRODUCTS_TITLE"] = "項目總計（＃金額＃）";
$MESS["M_CRM_ENTITY_DOCUMENT_SUMMARY_TITLE"] = "全部的";
$MESS["M_CRM_ENTITY_DOCUMENT_SUMMARY_TOTAL"] = "合計訂單：";
$MESS["M_CRM_ENTITY_DOCUMENT_SUMMARY_TOTAL_DISCOUNT"] = "折扣：";
$MESS["M_CRM_ENTITY_DOCUMENT_TAX"] = "稅";
