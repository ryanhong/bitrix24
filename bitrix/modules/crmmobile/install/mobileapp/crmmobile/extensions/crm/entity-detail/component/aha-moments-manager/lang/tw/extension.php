<?php
$MESS["M_CRM_DETAIL_AHA_GO2CHAT_DESCRIPTION"] = "邀請您的客戶到Messenger繼續對話。您的聊天歷史記錄將在這裡保存。";
$MESS["M_CRM_DETAIL_AHA_GO2CHAT_DESCRIPTION_COMPANY"] = "邀請您的客戶到Messenger繼續對話。您的聊天歷史記錄將保存給該公司。";
$MESS["M_CRM_DETAIL_AHA_GO2CHAT_DESCRIPTION_CONTACT"] = "邀請您的客戶到Messenger繼續對話。您的聊天歷史記錄將保存到此聯繫人中。";
$MESS["M_CRM_DETAIL_AHA_GO2CHAT_DESCRIPTION_DEAL"] = "邀請您的客戶到Messenger繼續對話。您的聊天歷史記錄將保存到這筆交易中。";
$MESS["M_CRM_DETAIL_AHA_GO2CHAT_DESCRIPTION_INVOICE"] = "邀請您的客戶到Messenger繼續對話。您的聊天歷史記錄將保存到此發票中。";
$MESS["M_CRM_DETAIL_AHA_GO2CHAT_DESCRIPTION_LEAD"] = "邀請您的客戶到Messenger繼續對話。您的聊天歷史記錄將保存到這一線索中。";
$MESS["M_CRM_DETAIL_AHA_GO2CHAT_DESCRIPTION_QUOTE_MSGVER_1"] = "邀請您的客戶到Messenger繼續對話。您的聊天歷史記錄將保存到此估計中";
$MESS["M_CRM_DETAIL_AHA_GO2CHAT_TITLE"] = "邀請客戶聊天";
$MESS["M_CRM_DETAIL_AHA_YOOCHECKS_BACKDROP_TITLE"] = "收據註冊未配置";
$MESS["M_CRM_DETAIL_AHA_YOOCHECKS_BUTTON"] = "連接yandex.checkout";
$MESS["M_CRM_DETAIL_AHA_YOOCHECKS_FEATURE_1"] = "54 Hz在線付款";
$MESS["M_CRM_DETAIL_AHA_YOOCHECKS_FEATURE_2"] = "您不需要啟用註冊註冊表";
$MESS["M_CRM_DETAIL_AHA_YOOCHECKS_FEATURE_3"] = "您不必配置您的寄存器";
$MESS["M_CRM_DETAIL_AHA_YOOCHECKS_FEATURE_4"] = "自動將收據發送給客戶和稅務機關";
$MESS["M_CRM_DETAIL_AHA_YOOCHECKS_SUBTEXT"] = "在您的yandex.checkout帳戶和bitrix24中啟用收據";
$MESS["M_CRM_DETAIL_AHA_YOOCHECKS_TITLE"] = "yandex.neckout收據";
