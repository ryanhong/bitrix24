<?php
$MESS["M_CRM_ENTITY_DETAILS_COPY_TEXT"] = "複製項目";
$MESS["M_CRM_ENTITY_DETAILS_COPY_TEXT_COMPANY"] = "複製公司";
$MESS["M_CRM_ENTITY_DETAILS_COPY_TEXT_CONTACT"] = "複製聯繫人";
$MESS["M_CRM_ENTITY_DETAILS_COPY_TEXT_DEAL"] = "複製交易";
$MESS["M_CRM_ENTITY_DETAILS_COPY_TEXT_LEAD"] = "複製線索";
$MESS["M_CRM_ENTITY_DETAILS_COPY_TEXT_QUOTE"] = "複製報價";
$MESS["M_CRM_ENTITY_DETAILS_COPY_TEXT_SMART_INVOICE"] = "複製發票";
$MESS["M_CRM_ENTITY_DETAILS_REPEATED_APPROACH_DEAL"] = "重複查詢";
$MESS["M_CRM_ENTITY_DETAILS_REPEATED_DEAL"] = "重複交易";
$MESS["M_CRM_ENTITY_DETAILS_REPEATED_LEAD"] = "重複引線";
