<?php
$MESS["WIKI_INSTALL_DESCRIPTION"] = "該模塊為您的網站提供Wiki功能。";
$MESS["WIKI_INSTALL_NAME"] = "Wiki";
$MESS["WIKI_INSTALL_TITLE"] = "Wiki模塊安裝";
$MESS["WIKI_PERM_D"] = "拒絕訪問";
$MESS["WIKI_PERM_R"] = "查看頁面";
$MESS["WIKI_PERM_W"] = "創建和編輯頁面";
$MESS["WIKI_PERM_X"] = "在視覺編輯器中創建和編輯頁面";
$MESS["WIKI_PERM_Y"] = "刪除頁面和歷史記錄";
$MESS["WIKI_PERM_Z"] = "設置許可";
$MESS["WIKI_UNINSTALL_TITLE"] = "Wiki模塊卸載";
