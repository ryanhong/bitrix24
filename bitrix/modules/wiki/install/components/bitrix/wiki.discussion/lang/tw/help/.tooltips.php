<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：緩存在緩存設置中設置的時間; <br /> <i> cache < /i>：始終在旁邊指定的時期; <br /> <br /> <i>不緩存</i>：沒有執行緩存。";
$MESS["ELEMENT_NAME_TIP"] = "在此處指定將傳遞元素名稱的變量的名稱。";
$MESS["OPER_VAR_TIP"] = "在此處指定一個變量的名稱，其值用於定義當前操作。";
$MESS["PAGE_VAR_TIP"] = "在此處指定將傳遞頁面名稱的變量的名稱。";
$MESS["PATH_TO_CATEGORIES_TIP"] = "指定頁面的路徑，顯示可用的類別。例如：wiki_cats.php";
$MESS["PATH_TO_DISCUSSION_TIP"] = "指定Wiki討論頁面的路徑。例如：wiki_discussion.php？title =＃wiki_name＃";
$MESS["PATH_TO_HISTORY_DIFF_TIP"] = "指定Wiki頁面版本比較表格的路徑。例如：wiki_history_diff.php？title =＃wiki_name＃";
$MESS["PATH_TO_HISTORY_TIP"] = "指定Wiki頁面更改歷史記錄的路徑。例如：wiki_history.php？title =＃wiki_name＃";
$MESS["PATH_TO_POST_EDIT_TIP"] = "指定Wiki頁面編輯器的路徑。例如：wiki_edit.php？title =＃wiki_name＃";
$MESS["PATH_TO_POST_TIP"] = "指定通往Wiki主頁的路徑。例如：wiki.php？title =＃wiki_name＃。";
$MESS["PATH_TO_SMILE_TIP"] = "笑臉夾路徑（相對於站點根）";
$MESS["PATH_TO_USER_TIP"] = "通往用戶配置文件頁面的路徑。例如：user.php？user_id =＃user_id＃＃";
$MESS["SET_TITLE_TIP"] = "檢查此選項將將頁面標題設置為基於Wiki的標題。";
