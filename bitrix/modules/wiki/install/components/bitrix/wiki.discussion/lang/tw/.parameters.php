<?php
$MESS["CP_BND_ELEMENT_CODE"] = "Wiki頁面代碼";
$MESS["CP_BND_ELEMENT_ID"] = "Wiki頁面ID";
$MESS["CP_BND_ELEMENT_NAME"] = "Wiki頁面標題";
$MESS["CP_BND_SET_STATUS_404"] = "丟失元素或部分時生成404狀態";
$MESS["F_FORUM_ID"] = "討論論壇ID";
$MESS["F_MESSAGES_PER_PAGE"] = "每頁消息的數量";
$MESS["F_PATH_TO_SMILE"] = "笑臉夾路徑（相對於站點根）";
$MESS["F_POST_FIRST_MESSAGE"] = "使用元素文本作為第一篇文章";
$MESS["F_READ_TEMPLATE"] = "主題查看頁面（留空白以使用論壇首選項）";
$MESS["F_SHOW_LINK_TO_FORUM"] = "顯示論壇鏈接";
$MESS["F_USE_CAPTCHA"] = "使用驗證碼";
$MESS["RATING_TYPE"] = "評分按鈕設計";
$MESS["RATING_TYPE_CONFIG"] = "預設";
$MESS["RATING_TYPE_LIKE_GRAPHIC"] = "喜歡（圖像）";
$MESS["RATING_TYPE_LIKE_TEXT"] = "喜歡（文字）";
$MESS["RATING_TYPE_STANDART_GRAPHIC"] = "喜歡/不同（圖像）";
$MESS["RATING_TYPE_STANDART_TEXT"] = "喜歡/不同（文字）";
$MESS["SHOW_RATING"] = "啟用評分";
$MESS["SHOW_RATING_CONFIG"] = "預設";
$MESS["T_IBLOCK_DESC_ADD_SECTIONS_CHAIN"] = "將部分添加到麵包屑中";
$MESS["T_IBLOCK_DESC_INCLUDE_IBLOCK_INTO_CHAIN"] = "將信息塊添加到麵包屑";
$MESS["T_IBLOCK_DESC_LIST_ID"] = "信息塊ID";
$MESS["T_IBLOCK_DESC_LIST_TYPE"] = "信息塊類型（僅用於驗證）";
$MESS["WIKI_OPER_VAR"] = "操作變量名稱";
$MESS["WIKI_PAGE_VAR"] = "頁面變量";
$MESS["WIKI_PATH_TO_CATEGORIES"] = "Wiki類別頁面路徑模板";
$MESS["WIKI_PATH_TO_CATEGORY"] = "Wiki類別描述頁面路徑模板";
$MESS["WIKI_PATH_TO_DISCUSSION"] = "討論頁路徑模板";
$MESS["WIKI_PATH_TO_HISTORY"] = "Wiki頁面更新日誌路徑模板";
$MESS["WIKI_PATH_TO_HISTORY_DIFF"] = "Wiki頁面版本操作路徑模板";
$MESS["WIKI_PATH_TO_PAGES"] = "Wiki頁面列表路徑模板";
$MESS["WIKI_PATH_TO_POST"] = "Wiki頁面路徑模板";
$MESS["WIKI_PATH_TO_POST_EDIT"] = "Wiki頁面編輯器路徑模板";
$MESS["WIKI_PATH_TO_USER"] = "用戶配置文件路徑模板";
$MESS["WIKI_VARIABLE_ALIASES"] = "可變別名";
