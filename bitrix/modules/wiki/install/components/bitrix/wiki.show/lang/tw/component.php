<?php
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "未安裝業務流程模塊。";
$MESS["CATEGORY_NAME"] = "類別";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "信息塊模塊未安裝。";
$MESS["IBLOCK_NOT_ASSIGNED"] = "沒有選擇信息塊。";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "沒有安裝社交網絡模塊。";
$MESS["WIKI_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["WIKI_DEFAULT_PAGE_NAME"] = "主頁";
$MESS["WIKI_DEFAULT_PAGE_TEXT"] = "您的Wiki還沒有主頁；您必須創建一個。<br>
[[％name％|立即創建]] <br> <br>

Wiki是一個允許用戶協作創建和編輯網頁以總結和積累知識的網站。每個用戶都可以根據其訪問權限編輯Wiki。 Wiki頁面是在簡單的視覺編輯器中創建和編輯的。 Wiki引擎維護每個Wiki頁面的版本歷史記錄，允許恢復到任何現有版本。<br> <br>

wikipedia.org是Wiki最著名的例子。<br>
有關Wiki的進一步閱讀：[http://en.wikipedia.org/wiki/wiki en.wikipedia.org/wiki/wiki]";
$MESS["WIKI_MODULE_NOT_INSTALLED"] = "未安裝Wiki模塊。";
$MESS["WIKI_PAGE_TEXT"] = "此Wiki頁面尚未創建。";
$MESS["WIKI_PAGE_TEXT_CREATE"] = "現在創建[[％name％|創建]]。";
$MESS["WIKI_REDIRECT"] = "重定向";
$MESS["WIKI_REDIRECT_FROM"] = "從";
$MESS["WIKI_SOCNET_INITIALIZING_FAILED"] = "社交網絡無法初始化。";
