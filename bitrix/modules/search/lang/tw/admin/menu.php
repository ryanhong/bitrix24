<?php
$MESS["mnu_customrank"] = "搜索結果排序規則";
$MESS["mnu_customrank_alt"] = "搜索結果分類管理";
$MESS["mnu_reindex"] = "重新索引";
$MESS["mnu_reindex_alt"] = "站點內容recondexing";
$MESS["mnu_search"] = "搜尋";
$MESS["mnu_search_title"] = "搜索管理";
$MESS["mnu_sitemap"] = "Google Sitemap";
$MESS["mnu_sitemap_alt"] = "創建Google Sitemap";
$MESS["mnu_stat_phrase_list"] = "搜索結果點擊";
$MESS["mnu_stat_phrase_stat"] = "短語列表";
$MESS["mnu_stat_tags_stat"] = "標籤列表";
$MESS["mnu_statistic"] = "統計數據";
$MESS["mnu_statistic_alt"] = "搜索短語統計";
