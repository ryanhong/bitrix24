<?php
$MESS["SEARCH_SITEMAP_BLOG_NO_COMMENTS"] = "在Google站點地圖中僅包含博客文章，而不是評論";
$MESS["SEARCH_SITEMAP_CONTINUE"] = "繼續創建站點地圖";
$MESS["SEARCH_SITEMAP_CREATE"] = "創建一個站點地圖";
$MESS["SEARCH_SITEMAP_CREATED"] = "創建了Google SiteMap索引文件，並在";
$MESS["SEARCH_SITEMAP_DOC_COUNT"] = "處理的文件";
$MESS["SEARCH_SITEMAP_ERR"] = "創建站點地圖時發生錯誤";
$MESS["SEARCH_SITEMAP_ERR_COUNT"] = "有錯誤";
$MESS["SEARCH_SITEMAP_FORUM_TOPICS_ONLY"] = "在Google站點地圖中僅包含論壇主題，而不是博客文章";
$MESS["SEARCH_SITEMAP_INSTR1"] = "這些步驟描述瞭如何向Google SiteMap服務提交站點地圖";
$MESS["SEARCH_SITEMAP_INSTR2"] = "登錄到";
$MESS["SEARCH_SITEMAP_INSTR3"] = "通過你的";
$MESS["SEARCH_SITEMAP_INSTR4"] = "按照鏈接";
$MESS["SEARCH_SITEMAP_INSTR5"] = "在提供的字段中輸入到站點地點位置的URL，然後單擊“提交URL”按鈕。";
$MESS["SEARCH_SITEMAP_INSTR6"] = "如果您已經提交了站點索引文件，則可以使用Google SiteMap服務重新提交它";
$MESS["SEARCH_SITEMAP_INSTR7"] = "Google帳戶";
$MESS["SEARCH_SITEMAP_INSTR8"] = "添加一個站點地圖";
$MESS["SEARCH_SITEMAP_NOTE"] = "警告！在創建Google SiteMap的過程中，所選站點的根文件夾中匹配\“ SiteMap _*。XML \”的所有文件都將被覆蓋。";
$MESS["SEARCH_SITEMAP_RECORD_LIMIT"] = "一個步驟處理的最大文檔數量：";
$MESS["SEARCH_SITEMAP_SESSION_ERR"] = "在SiteMap創建期間發生了會話驗證錯誤。會議可能已經過期了。請再次創建站點地圖。";
$MESS["SEARCH_SITEMAP_SITE"] = "選擇網站：";
$MESS["SEARCH_SITEMAP_STEP"] = "步驟持續時間：";
$MESS["SEARCH_SITEMAP_STEP_sec"] = "秒";
$MESS["SEARCH_SITEMAP_STOP"] = "停止";
$MESS["SEARCH_SITEMAP_TAB_TITLE"] = "設定";
$MESS["SEARCH_SITEMAP_TITLE"] = "創建Google Sitemap";
$MESS["SEARCH_SITEMAP_USE_HTTPS"] = "使用https";
$MESS["SEARCH_SITEMAP_WARN"] = "注意力！創建站點地圖時發現了不兼容的URL。它們已存儲在單獨的文件中";
$MESS["SEARCH_SITEMAP_WARN1"] = "注意，不兼容的URL超過2048個字符的長度或包含擴展的ASCII字符。";
