<?php
$MESS["SEARCH_ERROR1"] = "未配對的支架";
$MESS["SEARCH_ERROR2"] = "錯誤";
$MESS["SEARCH_ERROR3"] = "空搜索查詢（不包含字母或數字）。";
$MESS["SEARCH_ERROR4"] = "搜索查詢太長。";
$MESS["SEARCH_TERM_AND"] = "和";
$MESS["SEARCH_TERM_NOT_1"] = "不是";
$MESS["SEARCH_TERM_NOT_2"] = "沒有";
$MESS["SEARCH_TERM_OR"] = "或者";
