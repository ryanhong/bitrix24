<?php
$MESS["SEARCH_CP_ALL"] = "（全部）";
$MESS["SEARCH_CP_BLOG"] = "部落格";
$MESS["SEARCH_CP_CRM"] = "CRM";
$MESS["SEARCH_CP_DISK"] = "驅動文件";
$MESS["SEARCH_CP_FORUM"] = "論壇";
$MESS["SEARCH_CP_IBLOCK_TYPE"] = "在類型\“＃type_id＃\”的信息塊中搜索";
$MESS["SEARCH_CP_INTRANET_USERS"] = "用戶";
$MESS["SEARCH_CP_MICROBLOG"] = "微博";
$MESS["SEARCH_CP_NO_LIMIT"] = "搜索所有區域";
$MESS["SEARCH_CP_SOCIALNETWORK_GROUPS"] = "社交網絡組";
$MESS["SEARCH_CP_SOCIALNETWORK_USER"] = "社交網絡用戶";
$MESS["SEARCH_CP_SOCNET"] = "社交網絡（組）";
$MESS["SEARCH_CP_SOCNET_USER"] = "社交網絡（用戶）";
$MESS["SEARCH_CP_STATIC"] = "靜態文件";
$MESS["SEARCH_CP_URL"] = "URL從以下任何路徑開始";
$MESS["SEARCH_CP_WHERE_FILTER"] = "限制搜索區域";
