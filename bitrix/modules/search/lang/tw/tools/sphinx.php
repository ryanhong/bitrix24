<?php
$MESS["SEARCH_SPHINX_CONN_ERROR"] = "連接錯誤（＃errstr＃）。";
$MESS["SEARCH_SPHINX_CONN_ERROR_INDEX_NAME"] = "搜索索引標識符可能僅包含拉丁字母，數字和下劃線。";
$MESS["SEARCH_SPHINX_CONN_EXT_IS_MISSING"] = "未安裝MySQL擴展名。";
$MESS["SEARCH_SPHINX_CONN_INDEX_NOT_FOUND"] = "找不到指定的索引。";
$MESS["SEARCH_SPHINX_CONN_INDEX_WRONG_TYPE"] = "指定的索引應為\“ rt \”。";
$MESS["SEARCH_SPHINX_CONN_NO_INDEX"] = "索引ID未指定。";
$MESS["SEARCH_SPHINX_DESCR_ERROR"] = "錯誤獲取索引描述（＃errstr＃）。";
$MESS["SEARCH_SPHINX_NO_FIELDS"] = "索引中缺少字段（＃field_list＃）。";
