<?php
$MESS["CP_BSTC_FILTER_NAME"] = "額外的過濾器";
$MESS["SEARCH_CHECK_DATES"] = "僅搜索未到期的文檔";
$MESS["SEARCH_CNT"] = "按頻率";
$MESS["SEARCH_NAME"] = "按名字";
$MESS["SEARCH_PAGE_ELEMENTS"] = "標籤數";
$MESS["SEARCH_PERIOD"] = "在（天）內顯示標籤";
$MESS["SEARCH_SORT"] = "等級標籤";
$MESS["SEARCH_TAGS_INHERIT"] = "狹窄的搜索區域";
$MESS["SEARCH_URL_SEARCH"] = "搜索頁面的路徑（相對於站點根）";
