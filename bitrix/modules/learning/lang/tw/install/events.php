<?php
$MESS["NEW_LEARNING_TEXT_ANSWER_DESC"] = "＃id＃ - 結果ID
＃trib_id＃ - 嘗試ID
＃test_name＃ - 測試名稱
＃用戶＃-用戶參加測試
＃日期＃ - 日期和時間
＃Question_Text＃ - 問題
＃anding_text＃ - 答案
＃email_from＃ - 發件人的電子郵件地址
＃email_to＃ - 收件人的電子郵件地址
＃message_title＃ - 電子郵件主題";
$MESS["NEW_LEARNING_TEXT_ANSWER_MESSAGE"] = "來自＃site_name＃的消息
 -------------------------------------------------- --------

課程：＃course_name＃
測試：＃test_name＃

用戶：＃用戶＃
日期：＃日期＃

問題：
-------------------------------------------------- --------
＃Question_Text＃
-------------------------------------------------- --------

回答：
-------------------------------------------------- --------
＃anding_text＃
-------------------------------------------------- --------

要查看和編輯答案，請點擊鏈接：
http：//#server_name#/bitrix/admin/learn_test_result_edit.php？lang = en＆id = en＆id =＃id＃＆troun_id =＃

此消息已自動生成。";
$MESS["NEW_LEARNING_TEXT_ANSWER_NAME"] = "新文字答案";
$MESS["NEW_LEARNING_TEXT_ANSWER_SUBJECT"] = "＃site_name＃：＃course_name＃：＃message_title＃＃";
