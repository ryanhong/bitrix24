<?php
$MESS["LEARNING_ADD_MARK"] = "添加標記";
$MESS["LEARNING_APPROVED"] = "自動結果檢查";
$MESS["LEARNING_ATTEMPT_LIMIT"] = "允許嘗試";
$MESS["LEARNING_ATTEMPT_LIMIT_HINT"] = "（0或空為無限的數字）";
$MESS["LEARNING_COMPLETED_SCORE"] = "通過測試的最低分數";
$MESS["LEARNING_COMPLETED_SCORE2"] = "總分數的％";
$MESS["LEARNING_CURRENT_INDICATION"] = "顯示當前結果";
$MESS["LEARNING_CURRENT_INDICATION_MARK"] = "顯示為標記";
$MESS["LEARNING_CURRENT_INDICATION_PERCENT"] = "顯示為百分比";
$MESS["LEARNING_DESC"] = "描述";
$MESS["LEARNING_DESC_TITLE"] = "測試說明";
$MESS["LEARNING_EDIT_TITLE1"] = "創建新測試";
$MESS["LEARNING_EDIT_TITLE2"] = "編輯測試## ID＃";
$MESS["LEARNING_FINAL_INDICATION"] = "顯示最終結果";
$MESS["LEARNING_FINAL_INDICATION_CORRECT_COUNT"] = "顯示正確的答案計數";
$MESS["LEARNING_FINAL_INDICATION_MARK"] = "顯示最終標記";
$MESS["LEARNING_FINAL_INDICATION_MESSAGE"] = "根據標記顯示信息";
$MESS["LEARNING_FINAL_INDICATION_SCORE"] = "表演收到了分數";
$MESS["LEARNING_INCLUDE_SELF_TEST"] = "包括自我測試問題";
$MESS["LEARNING_INCORRECT_CONTROL"] = "錯誤的答案控制";
$MESS["LEARNING_MARKS"] = "分數";
$MESS["LEARNING_MARKS_TITLE"] = "分數";
$MESS["LEARNING_MARK_EXISTS_ERROR"] = "標記\“ ## mark ## \”的重複記錄。";
$MESS["LEARNING_MAX_MARK_ERROR"] = "未指定最大分數（100％正確答案）";
$MESS["LEARNING_MIN_TIME_BETWEEN_ATTEMPTS"] = "嘗試之間的最小間隔";
$MESS["LEARNING_MIN_TIME_BETWEEN_ATTEMPTS_D"] = "天";
$MESS["LEARNING_MIN_TIME_BETWEEN_ATTEMPTS_H"] = "小時";
$MESS["LEARNING_MIN_TIME_BETWEEN_ATTEMPTS_M"] = "分鐘";
$MESS["LEARNING_NEXT_QUESTION_ON_ERROR"] = "繼續下一個問題";
$MESS["LEARNING_ON_ERROR"] = "關於錯誤的答案";
$MESS["LEARNING_PASSAGE_TYPE"] = "通過測試的類型";
$MESS["LEARNING_PASSAGE_TYPE_0"] = "禁止去下一個問題，而無需回答當前問題。用戶<b>不能修改他們的答案。";
$MESS["LEARNING_PASSAGE_TYPE_1"] = "允許在不回答當前問題的情況下進入下一個問題。用戶<b>不能修改他們的答案。";
$MESS["LEARNING_PASSAGE_TYPE_2"] = "允許在不回答當前問題的情況下進入下一個問題。用戶<b> can </b>修改他們的答案。";
$MESS["LEARNING_PREVIOUS_TEST_ID"] = "拒絕訪問此測試而無需通過測試";
$MESS["LEARNING_PREVIOUS_TEST_SCORE"] = "不少於";
$MESS["LEARNING_PREVIOUS_TEST_SCORE2"] = "總分數的％";
$MESS["LEARNING_PREV_QUESTION_ON_ERROR"] = "繼續這個問題";
$MESS["LEARNING_QUESTIONS_FROM"] = "包括在測試中";
$MESS["LEARNING_QUESTIONS_FROM_ALL"] = "課程的所有問題";
$MESS["LEARNING_QUESTIONS_FROM_ALL_CHAPTER"] = "所有章節問題";
$MESS["LEARNING_QUESTIONS_FROM_ALL_LESSON"] = "所有課程問題";
$MESS["LEARNING_QUESTIONS_FROM_ALL_LESSON_WITH_SUBLESSONS"] = "所有課程的問題 +兒童課程";
$MESS["LEARNING_QUESTIONS_FROM_CHAPTERS"] = "每一章的問題";
$MESS["LEARNING_QUESTIONS_FROM_COURSE"] = "課程問題";
$MESS["LEARNING_QUESTIONS_FROM_LESSONS"] = "每個課程的問題";
$MESS["LEARNING_RANDOM_ANSWERS"] = "洗牌答案";
$MESS["LEARNING_RANDOM_QUESTIONS"] = "洗牌問題";
$MESS["LEARNING_SCORE_EXISTS_ERROR"] = "##得分##％答案存在重複記錄。";
$MESS["LEARNING_SHOW_ERRORS"] = "顯示錯誤";
$MESS["LEARNING_TEST"] = "設定";
$MESS["LEARNING_TEST_MARK"] = "標記";
$MESS["LEARNING_TEST_MARK_DELETE"] = "刪除";
$MESS["LEARNING_TEST_MARK_MESSAGE"] = "訊息";
$MESS["LEARNING_TEST_MARK_SCORE"] = "正確答案";
$MESS["LEARNING_TEST_NO_DEPENDS"] = "不要限制訪問";
$MESS["LEARNING_TEST_SCORE_TILL"] = "到";
$MESS["LEARNING_TEST_TEST"] = "測試";
$MESS["LEARNING_TEST_TITLE"] = "測試設置";
$MESS["LEARNING_TIME_LIMIT"] = "時限";
$MESS["LEARNING_TIME_LIMIT_HINT"] = "分鐘（0或空 - 無限制）";
