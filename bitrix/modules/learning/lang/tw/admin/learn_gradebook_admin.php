<?php
$MESS["LEARNING_ADMIN_APPROVED"] = "通過";
$MESS["LEARNING_ADMIN_ATTEMPTS"] = "嘗試";
$MESS["LEARNING_ADMIN_EXTRA_ATTEMPTS"] = "額外的嘗試";
$MESS["LEARNING_ADMIN_MAX_RESULT"] = "最大限度。分數";
$MESS["LEARNING_ADMIN_RESULT"] = "分數";
$MESS["LEARNING_ADMIN_RESULTS"] = "結果";
$MESS["LEARNING_ADMIN_STUDENT"] = "學生";
$MESS["LEARNING_ADMIN_TEST"] = "測試";
$MESS["LEARNING_ADMIN_TITLE"] = "培訓結果";
$MESS["LEARNING_ERROR"] = "保存錯誤的成績簿";
$MESS["MAIN_ADMIN_LIST_COMPLETED"] = "確認結果";
$MESS["MAIN_ADMIN_LIST_UNCOMPLETED"] = "拒絕結果";
$MESS["SAVE_ERROR"] = "錯誤更新成績簿＃";
