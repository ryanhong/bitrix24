<?php
$MESS["learning_CERTIFICATION"] = "認證";
$MESS["learning_GRADEBOOK"] = "年級";
$MESS["learning_PUBLIC_PROFILE"] = "允許公眾訪問學生的個人資料";
$MESS["learning_RESUME"] = "用戶的簡歷";
$MESS["learning_TAB"] = "學習";
$MESS["learning_TAB_TITLE"] = "學生的個人資料";
$MESS["learning_TRANSCRIPT"] = "成績單＃";
