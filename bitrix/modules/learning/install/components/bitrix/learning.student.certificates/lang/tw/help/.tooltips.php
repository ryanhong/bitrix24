<?php
$MESS["COURSE_DETAIL_TEMPLATE_TIP"] = "課程詳細信息頁面的路徑。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將將頁面標題設置為<b>課程報告</b>。";
$MESS["TESTS_LIST_TEMPLATE_TIP"] = "顯示課程所有測試的頁面的路徑。";
