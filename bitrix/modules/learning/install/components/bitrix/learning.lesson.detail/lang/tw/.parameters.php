<?php
$MESS["LEARNING_CHECK_PERMISSIONS"] = "檢查允許的權限";
$MESS["LEARNING_COURSE_ID"] = "課程ID";
$MESS["LEARNING_DESC_NO"] = "不";
$MESS["LEARNING_DESC_YES"] = "是的";
$MESS["LEARNING_LESSON_ID_NAME"] = "課程ID";
$MESS["LEARNING_PATH_TO_USER_PROFILE"] = "用戶個人資料頁面URL";
$MESS["LEARNING_SELF_TEST_TEMPLATE_NAME"] = "自我測試頁面URL";
