<?php
$MESS["CACHE_TIME_TIP"] = "在此處指定緩存有效期的時間段。";
$MESS["CACHE_TYPE_TIP"] = "<i> auto < /i>：在緩存設置中預定的時間內，緩存; <br /> <i> cache < /i>：始終在下一個字段中指定的時期中始終緩存; <br /> < i>不緩存</i>：沒有執行緩存。";
$MESS["CHECK_PERMISSIONS_TIP"] = "如果要檢查該課程的用戶訪問權限，請在此處選擇\“是\”。";
$MESS["COURSE_ID_TIP"] = "在此處選擇現有課程之一。如果選擇<b> <i>（其他）</i> </b>，則必須在旁邊的字段中指定課程ID。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將將頁面標題設置為課程名稱。";
