<?php
$MESS["LEARNING_COURSES_COURSE_ADD"] = "添加新課程";
$MESS["LEARNING_COURSES_NAV"] = "培訓班";
$MESS["LEARNING_COURSE_LIST"] = "課程清單";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "未安裝電子學習模塊。";
$MESS["LEARNING_PANEL_CONTROL_PANEL"] = "控制面板";
$MESS["LEARNING_PANEL_CONTROL_PANEL_ALT"] = "在控制面板中執行操作";
$MESS["comp_course_list_toolbar_add"] = "添加新課程";
$MESS["comp_course_list_toolbar_add_title"] = "在控制面板中添加新課程";
$MESS["comp_course_list_toolbar_list"] = "課程管理";
$MESS["comp_course_list_toolbar_list_title"] = "控制面板中的課程列表";
