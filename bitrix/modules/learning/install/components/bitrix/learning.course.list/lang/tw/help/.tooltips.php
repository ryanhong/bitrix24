<?php
$MESS["CHECK_PERMISSIONS_TIP"] = "如果要檢查訪問權限，請在此處選擇\“是\”。";
$MESS["COURSES_PER_PAGE_TIP"] = "可以在頁面上顯示的最大課程數量。其他課程將通過BreadCrumb導航鏈接提供。";
$MESS["COURSE_DETAIL_TEMPLATE_TIP"] = "課程詳細信息頁面的路徑。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將頁面標題設置為<b>課程</b>。";
$MESS["SORBY_TIP"] = "指定課程將被分類的字段。";
$MESS["SORORDER_TIP"] = "指定將分類課程的順序。";
