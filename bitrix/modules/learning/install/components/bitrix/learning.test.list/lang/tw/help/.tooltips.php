<?php
$MESS["CHECK_PERMISSIONS_TIP"] = "如果要檢查訪問權限，請在此處選擇\“是\”。";
$MESS["COURSE_ID_TIP"] = "在此處選擇現有課程之一。如果選擇<b> <i>（其他）</i> </b>，則必須在旁邊的字段中指定課程ID。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將將頁面標題設置為<b> tests </b>。";
$MESS["TESTS_PER_PAGE_TIP"] = "可以在頁面上顯示的最大測試數量。其他測試將使用麵包屑導航進行。";
$MESS["TEST_DETAIL_TEMPLATE_TIP"] = "通往主測試頁面的路徑。";
