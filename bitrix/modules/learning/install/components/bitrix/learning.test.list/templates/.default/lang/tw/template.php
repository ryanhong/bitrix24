<?php
$MESS["LEARNING_BTN_CONTINUE"] = "繼續";
$MESS["LEARNING_BTN_START"] = "開始";
$MESS["LEARNING_PASSAGE_FOLLOW_EDIT"] = "允許在不回答當前問題的情況下轉到下一個問題。 <b>允許</b>修改答案。";
$MESS["LEARNING_PASSAGE_FOLLOW_NO_EDIT"] = "允許在不回答當前問題的情況下轉到下一個問題。 <b>不允許</b>修改答案。";
$MESS["LEARNING_PASSAGE_NO_FOLLOW_NO_EDIT"] = "不允許不回答當前問題的情況下下一個問題。 <b>不允許</b>修改答案。";
$MESS["LEARNING_PASSAGE_TYPE"] = "通過測試的類型";
$MESS["LEARNING_PREV_TEST_REQUIRED"] = "要訪問此測試，您必須通過test＃test_link＃至少獲得最大總數的＃test_score＃％。";
$MESS["LEARNING_TEST_ATTEMPT_LIMIT"] = "允許嘗試";
$MESS["LEARNING_TEST_ATTEMPT_UNLIMITED"] = "無限數字";
$MESS["LEARNING_TEST_NAME"] = "測試名稱";
$MESS["LEARNING_TEST_TIME_LIMIT"] = "時限";
$MESS["LEARNING_TEST_TIME_LIMIT_MIN"] = "最小。";
$MESS["LEARNING_TEST_TIME_LIMIT_UNLIMITED"] = "沒有限制";
