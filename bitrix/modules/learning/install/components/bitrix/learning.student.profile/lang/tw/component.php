<?php
$MESS["LEARNING_BAD_MAIL"] = "電子郵件是不正確的";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "未安裝電子學習模塊。";
$MESS["LEARNING_NO_AUTHORIZE"] = "查看此頁面所需的授權。";
$MESS["LEARNING_NO_MAIL"] = "未指定電子郵件";
$MESS["LEARNING_PROFILE_TITLE"] = "學生的個人資料";
