<?php
$MESS["COURSE_ID_TIP"] = "在此處選擇現有課程之一。如果選擇<b> <i>（其他）</i> </b>，則必須在旁邊的字段中指定課程ID。";
$MESS["GRADEBOOK_TEMPLATE_TIP"] = "包含測試結果的頁面的路徑（即成績簿）。";
$MESS["PAGE_NUMBER_VARIABLE_TIP"] = "測試問題ID的變量名稱。";
$MESS["PAGE_WINDOW_TIP"] = "在導航鏈中顯示的問題數量。";
$MESS["SET_TITLE_TIP"] = "檢查此選項將將頁面標題設置為用戶名。";
$MESS["SHOW_TIME_LIMIT_TIP"] = "如果測試強加了時間限制，則啟用此選項將顯示時間計數器。";
$MESS["TEST_ID_TIP"] = "在此處選擇現有測試之一。如果您選擇<b> <i>（其他）</i> </b>，則必須在旁邊的字段中指定測試ID。";
