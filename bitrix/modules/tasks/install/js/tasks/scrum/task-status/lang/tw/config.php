<?php
$MESS["TST_ERROR_POPUP_TITLE"] = "有一個錯誤";
$MESS["TST_PARENT_COMPLETE_CANCEL_CAPTION"] = "取消";
$MESS["TST_PARENT_COMPLETE_MESSAGE"] = "\“ <b> #name＃</b> \”父任務的所有子任務均已完成。<br> <br>您想關閉parent task \“ <b> #name＃</b> \ “還是繼續努力？";
$MESS["TST_PARENT_COMPLETE_NOTIFY"] = "父任務已成功完成。";
$MESS["TST_PARENT_COMPLETE_OK_CAPTION"] = "完全的";
$MESS["TST_PARENT_PROCEED_CAPTION"] = "繼續";
$MESS["TST_PARENT_PROCEED_NOTIFY"] = "您可以繼續在Sprint的看板上處理父母任務。";
$MESS["TST_PARENT_RENEW_CANCEL_CAPTION"] = "取消";
$MESS["TST_PARENT_RENEW_MESSAGE"] = "子任務\“＃sub-name＃\”已恢復。<br> <br>您是否要恢復父級任務\” <b> #name＃</b> \“？";
$MESS["TST_PARENT_RENEW_NOTIFY"] = "父任務已恢復。";
$MESS["TST_PARENT_RENEW_OK_CAPTION"] = "恢復";
