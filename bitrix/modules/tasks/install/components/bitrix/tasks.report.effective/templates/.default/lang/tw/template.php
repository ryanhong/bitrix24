<?php
$MESS["TASKS_ADD_TASK"] = "創建任務";
$MESS["TASKS_CIRCLE_EFFECTIVE_TITLE"] = "效率";
$MESS["TASKS_COMPLETED"] = "任務完成";
$MESS["TASKS_EFFECTIVE_HELP_TEXT"] = "它是如何工作的？";
$MESS["TASKS_EFFECTIVE_HELP_URL"] = "https://helpdesk.bitrix24.com/open/6808471/";
$MESS["TASKS_EFFECTIVE_MORE"] = "細節";
$MESS["TASKS_EFFECTIVE_TITLE_FULL"] = "效率";
$MESS["TASKS_EFFECTIVE_TITLE_SHORT"] = "效率";
$MESS["TASKS_IN_PROGRESS"] = "總共正在進行";
$MESS["TASKS_MORE_LINK_TEXT"] = "細節";
$MESS["TASKS_MY_EFFECTIVE"] = "我的效率";
$MESS["TASKS_MY_EFFECTIVE_BY_DAY"] = "日常效率";
$MESS["TASKS_MY_EFFECTIVE_FROM"] = "從<strong> #date＃</strong>開始收集的統計信息";
$MESS["TASKS_NO_DATA_TEXT"] = "沒有數據";
$MESS["TASKS_PANEL_TEXT_EFFECTIVE_MORE"] = "您有多效率？<br/>
<br/>
BitRix24跟踪分配的所有任務，以便測量其中有多少個時間，沒有異議。<br/>
<br/>
效率指標為您提供了許多好處。首先，它可以幫助您了解個人和小組績效。其次，任何被測量的東西都更容易改進。最後，您可以將KPI基於此指標，並就誰獲得晉升和獎勵做出明智的決定。<br/>
<br/>
了解更多<a href = \"https://helpdesk.bitrix24.com/open/6808471/ \">在這裡</a>
";
$MESS["TASKS_TITLE_GRAPH_KPI"] = "效率， ％";
$MESS["TASKS_VIOLATION"] = "有異議的任務";
