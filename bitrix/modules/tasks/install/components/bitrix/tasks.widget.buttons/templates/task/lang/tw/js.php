<?php
$MESS["TASKS_ADD_SUBTASK"] = "創建子任務";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN"] = "添加到工作日計劃";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN_EX"] = "添加到工作日計劃";
$MESS["TASKS_COPY_TASK"] = "複製";
$MESS["TASKS_COPY_TASK_EX"] = "重複任務";
$MESS["TASKS_DEFER_TASK"] = "推遲";
$MESS["TASKS_DELEGATE_TASK"] = "代表";
$MESS["TASKS_DELETE_CONFIRM"] = "您真的想刪除它嗎？";
$MESS["TASKS_DELETE_TASK"] = "刪除";
$MESS["TASKS_RENEW_TASK"] = "恢復";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "您已經在使用\“ {{title}} \”的時間跟踪器。此任務將被暫停。繼續？";
$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "時間跟踪器現在正在與另一個任務一起使用。";
$MESS["TASKS_UNKNOWN"] = "未知";
