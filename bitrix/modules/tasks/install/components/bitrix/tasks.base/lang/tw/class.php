<?php
$MESS["TASKS_TB_CSRF_ERROR"] = "不幸的是，有一個錯誤。請刷新頁面，然後重試。";
$MESS["TASKS_TB_TASKS_MODULE_NOT_AVAILABLE"] = "在此版本中，任務模塊不可用。";
$MESS["TASKS_TB_TASKS_MODULE_NOT_INSTALLED"] = "任務模塊未安裝。";
$MESS["TASKS_TB_USER_NOT_AUTHORIZED"] = "您無權查看任務";
