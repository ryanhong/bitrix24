<?php
$MESS["ERROR_TASKS_CHANGE_DEADLINE_COUNT_OVER"] = "你不能更改截止日期";
$MESS["ERROR_TASKS_CHANGE_DEADLINE_MAXTIME_OVER"] = "最終可能的截止日期是＃截止日期＃";
$MESS["ERROR_TASKS_CHANGE_DEADLINE_NULL"] = "您無法刪除截止日期";
$MESS["TASKS_SIDEBAR_ACCOMPLICES"] = "參與者";
$MESS["TASKS_SIDEBAR_ADD_ACCOMPLICES"] = "添加參與者";
$MESS["TASKS_SIDEBAR_ADD_AUDITORS"] = "添加觀察者";
$MESS["TASKS_SIDEBAR_AUDITORS"] = "觀察者";
$MESS["TASKS_SIDEBAR_AUTOMATION"] = "自動化";
$MESS["TASKS_SIDEBAR_CHANGE"] = "改變";
$MESS["TASKS_SIDEBAR_CREATED_DATE"] = "創建於";
$MESS["TASKS_SIDEBAR_DEADLINE_NO"] = "沒有任何";
$MESS["TASKS_SIDEBAR_DURATION"] = "期間";
$MESS["TASKS_SIDEBAR_FINISH"] = "結尾";
$MESS["TASKS_SIDEBAR_IN_REPORT"] = "在報告中";
$MESS["TASKS_SIDEBAR_IN_REPORT_NO"] = "不";
$MESS["TASKS_SIDEBAR_IN_REPORT_YES"] = "是的";
$MESS["TASKS_SIDEBAR_REGULAR_TASK"] = "重複的任務";
$MESS["TASKS_SIDEBAR_REMINDER"] = "提醒";
$MESS["TASKS_SIDEBAR_REPEAT"] = "再次發生的";
$MESS["TASKS_SIDEBAR_ROBOTS_1"] = "配置";
$MESS["TASKS_SIDEBAR_STAGE"] = "階段";
$MESS["TASKS_SIDEBAR_START"] = "開始";
$MESS["TASKS_SIDEBAR_START_DATE"] = "自從";
$MESS["TASKS_SIDEBAR_STATUS"] = "地位";
$MESS["TASKS_SIDEBAR_STOP_WATCH_CONFIRM"] = "您確定您不再想成為此任務的觀察者嗎？";
$MESS["TASKS_SIDEBAR_TASK_CREATED_BY_TEMPLATE"] = "任務是使用模板自動創建的";
$MESS["TASKS_SIDEBAR_TASK_OVERDUE"] = "任務逾期！";
$MESS["TASKS_SIDEBAR_TASK_REPEATS"] = "任務重複";
$MESS["TASKS_SIDEBAR_TEMPLATE"] = "模板";
$MESS["TASKS_SIDEBAR_TEMPLATE_NOT_ACCESSIBLE"] = "無法查看重複的任務模板";
$MESS["TASKS_SIDEBAR_TIME_ESTIMATE"] = "估計";
$MESS["TASKS_SIDEBAR_TIME_SPENT_IN_LOGS"] = "期間";
$MESS["TASKS_TASK_EPIC"] = "史詩";
$MESS["TASKS_TTDP_TEMPLATE_COPY_CURRENT_URL"] = "將任務鏈接複製到剪貼板";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_ACCOMPLICES"] = "參與者";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_AUDITORS"] = "觀察者";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_ENTER_AUDITOR"] = "觀察";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_LEAVE_AUDITOR"] = "不要看";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_LEAVE_AUDITOR_CONFIRM"] = "如果您不再是觀察者，則可能無法查看任務。您想刪除觀察者角色嗎？";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_ORIGINATOR"] = "由...製作";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_RESPONSIBLE"] = "負責人";
$MESS["TASK_RESULT_SIDEBAR_HINT"] = "任務狀態摘要是必需的";
$MESS["TASK_RESULT_SIDEBAR_HINT_MSGVER_1"] = "需要任務狀態摘要";
