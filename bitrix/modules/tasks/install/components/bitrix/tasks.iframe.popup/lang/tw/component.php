<?php
$MESS["TASKS_MODULE_NOT_FOUND"] = "任務模塊未安裝。";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "您已經在使用\“ {{title}} \”的時間跟踪器。此任務將被暫停。繼續？";
$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "您已經在使用\“ {{title}} \”的時間跟踪器。此任務將被暫停。繼續？";
