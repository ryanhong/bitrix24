<?php
$MESS["TASKS_RESULT_TUTORIAL_MESSAGE"] = "您可以將任何評論用作任務報告，最終或中級。單擊評論，然後選擇\“標記為任務狀態摘要\”。";
$MESS["TASKS_RESULT_TUTORIAL_TITLE"] = "任務創建者要求您提供任務報告。";
