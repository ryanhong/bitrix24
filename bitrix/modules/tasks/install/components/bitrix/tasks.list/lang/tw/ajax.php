<?php
$MESS["TASKS_DOUBLE_CLICK"] = "雙擊查看";
$MESS["TASKS_GANTT_RESULT_REQUIRED"] = "任務創建者要求您提供任務狀態摘要。<br>對任務發表評論並將其作為摘要。";
$MESS["TASKS_MARK"] = "分數";
$MESS["TASKS_MARK_MSGVER_1"] = "評分";
$MESS["TASKS_MARK_N"] = "消極的";
$MESS["TASKS_MARK_NONE"] = "沒有任何";
$MESS["TASKS_MARK_N_MSGVER_1"] = "消極的";
$MESS["TASKS_MARK_P"] = "積極的";
$MESS["TASKS_MARK_P_MSGVER_1"] = "積極的";
$MESS["TASKS_PRIORITY"] = "優先事項";
$MESS["TASKS_PRIORITY_0"] = "低的";
$MESS["TASKS_PRIORITY_1"] = "普通的";
$MESS["TASKS_PRIORITY_2"] = "高的";
