<?php
$MESS["TASKS_ABOUT_DEADLINE"] = "截止日期之前";
$MESS["TASKS_ACCEPT_TASK"] = "接受任務";
$MESS["TASKS_ADD_BACK_TO_TASKS_LIST"] = "返回任務";
$MESS["TASKS_ADD_SUBTASK_2"] = "創建子任務";
$MESS["TASKS_ADD_TASK"] = "新任務";
$MESS["TASKS_ADD_TASK_SHORT"] = "添加";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN"] = "添加到日常計劃";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN_EX"] = "添加到日常計劃";
$MESS["TASKS_ADD_TEMPLATE_SUBTASK"] = "任務模板（用於新子任務）";
$MESS["TASKS_ADD_TEMPLATE_TASK"] = "從任務模板創建";
$MESS["TASKS_APPLY"] = "申請";
$MESS["TASKS_APPROVE_TASK"] = "接受完成";
$MESS["TASKS_BY_DATE"] = "按日期";
$MESS["TASKS_CANCEL"] = "取消";
$MESS["TASKS_CLOSE_TASK"] = "結束";
$MESS["TASKS_COPY_TASK"] = "複製";
$MESS["TASKS_COPY_TASK_EX"] = "重複任務";
$MESS["TASKS_CREATOR"] = "創造者";
$MESS["TASKS_CTT_SYS_LOG"] = "歷史";
$MESS["TASKS_DATE_COMPLETED"] = "完成於";
$MESS["TASKS_DATE_CREATED"] = "創建於";
$MESS["TASKS_DATE_END"] = "結束日期";
$MESS["TASKS_DATE_START"] = "開始日期";
$MESS["TASKS_DATE_STARTED"] = "開始";
$MESS["TASKS_DEADLINE"] = "最後期限";
$MESS["TASKS_DECLINE_REASON"] = "拒絕的原因";
$MESS["TASKS_DECLINE_TASK"] = "衰退";
$MESS["TASKS_DEFER_TASK"] = "推遲";
$MESS["TASKS_DELEGATE_TASK"] = "代表";
$MESS["TASKS_DELETE_CONFIRM"] = "確認刪除？";
$MESS["TASKS_DELETE_FILE_CONFIRM"] = "刪除文件？";
$MESS["TASKS_DELETE_TASK"] = "刪除";
$MESS["TASKS_DELETE_TASK_CONFIRM"] = "確認刪除？";
$MESS["TASKS_DEPENDENCY_END"] = "結束";
$MESS["TASKS_DEPENDENCY_START"] = "開始";
$MESS["TASKS_DEPENDENCY_TYPE"] = "依賴性";
$MESS["TASKS_DETAIL_CHECKLIST"] = "清單";
$MESS["TASKS_DETAIL_CHECKLIST_ADD"] = "添加";
$MESS["TASKS_DOUBLE_CLICK"] = "雙擊查看";
$MESS["TASKS_DURATION"] = "花了幾個小時";
$MESS["TASKS_ELAPSED_ADD"] = "添加";
$MESS["TASKS_ELAPSED_AUTHOR"] = "由...製作";
$MESS["TASKS_ELAPSED_COMMENT"] = "評論";
$MESS["TASKS_ELAPSED_DATE"] = "日期";
$MESS["TASKS_ELAPSED_H"] = "H";
$MESS["TASKS_ELAPSED_M"] = "m";
$MESS["TASKS_ELAPSED_REMOVE_CONFIRM"] = "您要刪除經過的時間值嗎？";
$MESS["TASKS_ELAPSED_S"] = "s";
$MESS["TASKS_ELAPSED_SOURCE_MANUAL"] = "該記錄被手動添加或編輯";
$MESS["TASKS_ELAPSED_SOURCE_UNDEFINED"] = "未知是否添加或手動編輯此記錄";
$MESS["TASKS_ELAPSED_TIME"] = "所花費的時間";
$MESS["TASKS_ELAPSED_TIME_SHORT"] = "時間流逝";
$MESS["TASKS_EXPORT_EXCEL"] = "導出到Excel";
$MESS["TASKS_EXPORT_OUTLOOK"] = "導出到Outlook";
$MESS["TASKS_FIELD_DEADLINE_AFTER"] = "截止日期";
$MESS["TASKS_FIELD_END_DATE_PLAN_AFTER"] = "結束";
$MESS["TASKS_FIELD_START_DATE_PLAN_AFTER"] = "開始";
$MESS["TASKS_FIELD_TIME_ESTIMATE"] = "時間估計";
$MESS["TASKS_FILES_FROM_COMMENTS"] = "文件";
$MESS["TASKS_FILTER"] = "設置過濾器";
$MESS["TASKS_FILTER_COMMON"] = "簡單的";
$MESS["TASKS_FILTER_EXTENDED"] = "擴展";
$MESS["TASKS_FILTER_STATUSES"] = "狀態";
$MESS["TASKS_GROUP_ADD"] = "添加";
$MESS["TASKS_GROUP_LOADING"] = "請稍等...";
$MESS["TASKS_HOURS_G"] = "小時";
$MESS["TASKS_HOURS_N"] = "小時";
$MESS["TASKS_HOURS_P"] = "小時";
$MESS["TASKS_IMPORTANT_TASK"] = "優先級";
$MESS["TASKS_LOG_ACCOMPLICES"] = "參與者";
$MESS["TASKS_LOG_ADD_IN_REPORT"] = "在報告中";
$MESS["TASKS_LOG_AUDITORS"] = "觀察者";
$MESS["TASKS_LOG_CHECKLIST_ITEM_CHECK"] = "檢查列表項已完成。";
$MESS["TASKS_LOG_CHECKLIST_ITEM_CREATE"] = "添加到清單中的項目";
$MESS["TASKS_LOG_CHECKLIST_ITEM_REMOVE"] = "從清單中刪除的項目";
$MESS["TASKS_LOG_CHECKLIST_ITEM_RENAME"] = "檢查列表項目已重命名。";
$MESS["TASKS_LOG_CHECKLIST_ITEM_UNCHECK"] = "檢查列表項目尚未完成。";
$MESS["TASKS_LOG_COMMENT"] = "評論創建";
$MESS["TASKS_LOG_COMMENT_DEL"] = "評論已刪除";
$MESS["TASKS_LOG_COMMENT_EDIT"] = "評論改變了";
$MESS["TASKS_LOG_CREATED_BY"] = "創造者";
$MESS["TASKS_LOG_DEADLINE"] = "最後期限";
$MESS["TASKS_LOG_DELETED_FILES"] = "文件已刪除";
$MESS["TASKS_LOG_DEPENDS_ON"] = "以前的任務";
$MESS["TASKS_LOG_DESCRIPTION"] = "描述已更新";
$MESS["TASKS_LOG_DURATION_FACT"] = "實際持續時間";
$MESS["TASKS_LOG_DURATION_PLAN"] = "計劃的持續時間";
$MESS["TASKS_LOG_DURATION_PLAN_SECONDS"] = "計劃的持續時間";
$MESS["TASKS_LOG_END_DATE_PLAN"] = "擬議的結束日期";
$MESS["TASKS_LOG_FILES"] = "文件";
$MESS["TASKS_LOG_GROUP_ID"] = "團體";
$MESS["TASKS_LOG_MARK"] = "分數";
$MESS["TASKS_LOG_MARK_MSGVER_1"] = "評分";
$MESS["TASKS_LOG_NEW"] = "創建的任務";
$MESS["TASKS_LOG_NEW_FILES"] = "添加了文件";
$MESS["TASKS_LOG_PARENT_ID"] = "父任務";
$MESS["TASKS_LOG_PRIORITY"] = "優先事項";
$MESS["TASKS_LOG_RESPONSIBLE_ID"] = "負責人";
$MESS["TASKS_LOG_START_DATE_PLAN"] = "擬議的開始日期";
$MESS["TASKS_LOG_STATUS"] = "地位";
$MESS["TASKS_LOG_TAGS"] = "標籤";
$MESS["TASKS_LOG_TIME_SPENT_IN_LOGS"] = "所花費的時間";
$MESS["TASKS_LOG_TITLE"] = "任務名稱";
$MESS["TASKS_LOG_WHAT"] = "更新";
$MESS["TASKS_LOG_WHEN"] = "日期";
$MESS["TASKS_LOG_WHERE"] = "更新處置";
$MESS["TASKS_LOG_WHO"] = "由...製作";
$MESS["TASKS_MAIL_FORWARD"] = "向前的地址";
$MESS["TASKS_MARK"] = "分數";
$MESS["TASKS_MARK_MSGVER_1"] = "評分";
$MESS["TASKS_MARK_N"] = "消極的";
$MESS["TASKS_MARK_NONE"] = "沒有任何";
$MESS["TASKS_MARK_P"] = "積極的";
$MESS["TASKS_NO_RESPONSIBLE"] = "未指定負責人。";
$MESS["TASKS_NO_SUBTASKS"] = "沒有子任務";
$MESS["TASKS_NO_TEMPLATES"] = "沒有可用的模板";
$MESS["TASKS_NO_TITLE"] = "任務名稱未指定。";
$MESS["TASKS_OK"] = "好的";
$MESS["TASKS_PARENT_TASK"] = "父任務";
$MESS["TASKS_PARENT_TEMPLATE"] = "一般模板";
$MESS["TASKS_PAUSE_TASK"] = "暫停";
$MESS["TASKS_QUICK_CANCEL"] = "取消";
$MESS["TASKS_QUICK_DESCRIPTION"] = "描述";
$MESS["TASKS_QUICK_INFO_DETAILS"] = "細節";
$MESS["TASKS_QUICK_INFO_EMPTY_DATE"] = "不";
$MESS["TASKS_QUICK_IN_GROUP"] = "項目中的任務";
$MESS["TASKS_QUICK_SAVE"] = "節省";
$MESS["TASKS_QUICK_TITLE"] = "任務名稱";
$MESS["TASKS_REDO_TASK_MSGVER_1"] = "返回修訂";
$MESS["TASKS_REMIND"] = "提醒";
$MESS["TASKS_REMINDER_OK"] = "好的";
$MESS["TASKS_REMINDER_TITLE"] = "提醒";
$MESS["TASKS_REMIND_BEFORE"] = "在＃num＃day（s）";
$MESS["TASKS_REMIND_VIA_EMAIL"] = "通過電子郵件";
$MESS["TASKS_REMIND_VIA_EMAIL_EX"] = "發送電子郵件";
$MESS["TASKS_REMIND_VIA_JABBER"] = "即時消息";
$MESS["TASKS_REMIND_VIA_JABBER_EX"] = "發送即時消息";
$MESS["TASKS_RENEW_TASK"] = "恢復";
$MESS["TASKS_REPORTS"] = "報告";
$MESS["TASKS_RESPONSIBLE"] = "負責任的";
$MESS["TASKS_SELECT"] = "選擇";
$MESS["TASKS_SIDEBAR_REGULAR_TASK"] = "重複的任務";
$MESS["TASKS_START_TASK"] = "開始";
$MESS["TASKS_STATUS"] = "地位";
$MESS["TASKS_STATUS_1"] = "新的";
$MESS["TASKS_STATUS_2"] = "待辦的";
$MESS["TASKS_STATUS_3"] = "進行中";
$MESS["TASKS_STATUS_4"] = "據說已經完成";
$MESS["TASKS_STATUS_5"] = "完全的";
$MESS["TASKS_STATUS_6"] = "遞延";
$MESS["TASKS_STATUS_7"] = "拒絕";
$MESS["TASKS_STATUS_ACCEPTED"] = "待辦的";
$MESS["TASKS_STATUS_COMPLETED"] = "完全的";
$MESS["TASKS_STATUS_DECLINED"] = "拒絕";
$MESS["TASKS_STATUS_DELAYED"] = "遞延";
$MESS["TASKS_STATUS_IN_PROGRESS"] = "進行中";
$MESS["TASKS_STATUS_NEW"] = "新的";
$MESS["TASKS_STATUS_OVERDUE"] = "逾期";
$MESS["TASKS_STATUS_WAITING"] = "待審核";
$MESS["TASKS_TASK_ADD_TO_FAVORITES"] = "添加到收藏夾";
$MESS["TASKS_TASK_COMMENTS"] = "評論";
$MESS["TASKS_TASK_COMPONENT_TEMPLATE_MAKE_IMPORTANT"] = "優先級";
$MESS["TASKS_TASK_DURATION_DAYS"] = "天";
$MESS["TASKS_TASK_DURATION_DAYS_PLURAL_0"] = "天";
$MESS["TASKS_TASK_DURATION_DAYS_PLURAL_1"] = "天";
$MESS["TASKS_TASK_DURATION_DAYS_PLURAL_2"] = "天";
$MESS["TASKS_TASK_DURATION_HOURS"] = "小時";
$MESS["TASKS_TASK_DURATION_HOURS_PLURAL_0"] = "小時";
$MESS["TASKS_TASK_DURATION_HOURS_PLURAL_1"] = "小時";
$MESS["TASKS_TASK_DURATION_HOURS_PLURAL_2"] = "小時";
$MESS["TASKS_TASK_DURATION_MINUTES"] = "分鐘";
$MESS["TASKS_TASK_DURATION_MINUTES_PLURAL_0"] = "分鐘";
$MESS["TASKS_TASK_DURATION_MINUTES_PLURAL_1"] = "分鐘";
$MESS["TASKS_TASK_DURATION_MINUTES_PLURAL_2"] = "分鐘";
$MESS["TASKS_TASK_DURATION_SECONDS_PLURAL_0"] = "第二";
$MESS["TASKS_TASK_DURATION_SECONDS_PLURAL_1"] = "秒";
$MESS["TASKS_TASK_DURATION_SECONDS_PLURAL_2"] = "秒";
$MESS["TASKS_TASK_FILES"] = "文件";
$MESS["TASKS_TASK_GROUP"] = "此任務在組（項目）中";
$MESS["TASKS_TASK_LINKED_TASKS"] = "依賴任務";
$MESS["TASKS_TASK_LOG"] = "更改日誌";
$MESS["TASKS_TASK_LOG_SHORT"] = "歷史";
$MESS["TASKS_TASK_NO_TAGS"] = "沒有任何";
$MESS["TASKS_TASK_NUM"] = "任務##​​ task_num＃";
$MESS["TASKS_TASK_PREDECESSORS"] = "以前的任務";
$MESS["TASKS_TASK_PREVIOUS_TASKS"] = "以前的任務";
$MESS["TASKS_TASK_SUBTASKS"] = "子任務";
$MESS["TASKS_TASK_TAGS"] = "標籤";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_ADD_SUBTEMPLATE"] = "添加子任務";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_TO_LIST"] = "返回目錄";
$MESS["TASKS_TASK_TITLE"] = "任務名稱和描述";
$MESS["TASKS_TASK_TITLE_LABEL"] = "任務";
$MESS["TASKS_TEMPLATES"] = "模板";
$MESS["TASKS_TEMPLATES_LIST"] = "所有模板";
$MESS["TASKS_TEMPLATE_COPY"] = "複製模板";
$MESS["TASKS_TEMPLATE_CREATE_SUB"] = "添加子模板";
$MESS["TASKS_TEMPLATE_CREATE_TASK"] = "在此模板上創建任務";
$MESS["TASKS_TITLE"] = "任務";
$MESS["TASKS_TTDP_DATES"] = "時期";
$MESS["TASKS_TTDP_PROJECT_TASK_IN"] = "此任務在組（項目）中";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_ACCOMPLICES"] = "參與者";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_AUDITORS"] = "觀察者";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_ORIGINATOR"] = "由...製作";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_RESPONSIBLE"] = "負責人";
$MESS["TASKS_TTV_CANCEL_BUTTON_TEXT"] = "取消";
$MESS["TASKS_TTV_SUB_TITLE"] = "任務的模板## ID＃";
$MESS["TASKS_TTV_TASK_INACCESSIBLE"] = "無法查看任務";
$MESS["TASKS_TTV_TEMPLATE_INACCESSIBLE"] = "無法查看模板";
$MESS["TASKS_TTV_TYPE_FOR_NEW_USER_HINT"] = "此模板用於為每個新的註冊用戶創建一個任務";
$MESS["TASKS_TT_VIEW"] = "查看模板## ID＃";
