<?php
$MESS["TASKS_TUFP_DRAG_N_DROP_OFF"] = "禁用字段拖動";
$MESS["TASKS_TUFP_DRAG_N_DROP_ON"] = "啟用字段拖動";
$MESS["TASKS_TUFP_FIELD_HIDE_CONFIRM"] = "您想隱藏該領域嗎？您可以通過單擊\“顯示字段\”再次顯示出來。";
$MESS["TASKS_TUFP_FIELD_HIDE_DELETE_CONFIRM"] = "您要刪除字段，還是只是隱藏該字段，以免以任務編輯表格顯示？";
$MESS["TASKS_TUFP_FIELD_MANDATORY"] = "必需的";
$MESS["TASKS_TUFP_LICENSE_BODY"] = "
自定義字段可在<a href= \"/settings/license_all.php \" target= \之詞\"_blank \">選定的商業計劃</a>。<br />
<br />
用更多字段（例如\ \“項目預算\”或\“預付費金額\”）為任務供電。它們將適用於所有用戶。";
$MESS["TASKS_TUFP_LICENSE_BODY_V2"] = "將您的自定義字段添加到任務中。他們將向所有用戶使用。<br />
<br />
自定義字段可在<a href= \"/settings/license_all.php \" target= \"_blank \">選定的商業計劃</a>中獲得。";
$MESS["TASKS_TUFP_LICENSE_TITLE"] = "將您的字段添加到任務";
$MESS["TASKS_TUFP_NEW_FIELD_BOOLEAN"] = "新布爾領域";
$MESS["TASKS_TUFP_NEW_FIELD_DATETIME"] = "新日期字段";
$MESS["TASKS_TUFP_NEW_FIELD_DOUBLE"] = "新數字字段";
$MESS["TASKS_TUFP_NEW_FIELD_STRING"] = "新的文本字段";
$MESS["TASKS_TUFP_SAVE_SCHEME_TO_EVERYONE"] = "將字段設置為所有用戶";
$MESS["TASKS_TUFP_SAVE_TO_ALL_CONFIRM"] = "所選的字段配置將分配給所有門戶用戶。繼續？";
