<?php
$MESS["TASKS_TUFE_EMPTY_LABEL"] = "未指定字段名稱";
$MESS["TASKS_TUFE_UF_ADMIN_RESTRICTED"] = "自定義字段只能由管理員訪問";
$MESS["TASKS_TUFE_UF_MANAGING_RESTRICTED"] = "您無法在計劃中管理自定義字段。";
$MESS["TASKS_TUFE_UF_NAME_GENERATION_FAILED"] = "無法創建自定義字段名稱";
$MESS["TASKS_TUFE_UF_NOT_FOUND"] = "找不到自定義字段";
$MESS["TASKS_TUFE_UF_RELATED_FIELDS_CREATING_ERROR"] = "無法創建一些鏈接字段";
$MESS["TASKS_TUFE_UF_RELATED_FIELDS_UPDATING_ERROR"] = "無法更新一些鏈接字段";
$MESS["TASKS_TUFE_UF_UNEXPECTED_ERROR"] = "由於未知錯誤，無法保存字段。";
$MESS["TASKS_TUFE_UF_UNKNOWN_ENTITY_CODE"] = "未知實體代碼";
$MESS["TASKS_TUFE_UF_UNKNOWN_ID"] = "未知自定義字段ID";
$MESS["TASKS_TUFE_UF_UNKNOWN_TYPE"] = "未知自定義字段類型";
$MESS["TASKS_TUFE_UF_USAGE_RESTRICTED"] = "您不能在計劃中使用自定義字段。";
$MESS["TASKS_TUFE_UF_USAGE_RESTRICTED_MANDATORY"] = "無法創建所需的字段。";
