<?php
$MESS["TASKS_CALENDAR_COLLAPSED_ENTRIES_NAME"] = "任務";
$MESS["TASKS_CALENDAR_NEW_TASK"] = "新任務";
$MESS["TASKS_CALENDAR_NOTIFY_CHANGE_DEADLINE"] = "任務截止日期更改為＃日期＃";
$MESS["TASKS_CLOSE_PAGE_CONFIRM"] = "您所做的更改可能會丟失。";
$MESS["TASKS_DELETE_SUCCESS"] = "任務已刪除";
$MESS["TASKS_LIST_TRIAL_EXPIRED_TEXT"] = "免費計劃可讓您在任務之間最多有5個依賴關係。如果您想擁有無限的依賴關係，請升級到高級任務和項目管理。

當前支持四種依賴性類型：
完成（FS）
開始啟動（SS）
完成到完成（FF）
開始完成（SF）

態

高級任務 +高級CRM +高級電話和其他其他功能在選定的商業計劃中提供。";
$MESS["TASKS_LIST_TRIAL_EXPIRED_TITLE_V2"] = "僅在Bitrix24的高級任務中可用";
$MESS["TASKS_TITLE"] = "任務";
$MESS["TASKS_TITLE_GROUP_TASKS"] = "工作組任務";
$MESS["TASKS_TITLE_MY"] = "我的任務";
$MESS["TASKS_TITLE_PROJECT"] = "專案";
