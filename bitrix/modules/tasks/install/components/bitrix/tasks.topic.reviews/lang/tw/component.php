<?php
$MESS["COMM_COMMENT_OK"] = "評論成功保存了";
$MESS["COMM_COMMENT_OK_AND_NOT_APPROVED"] = "該評論已成功添加。主持人批准後將顯示。";
$MESS["F_ERR_FID_EMPTY"] = "評論論壇尚未設置";
$MESS["F_ERR_FID_IS_NOT_EXIST"] = "評論論壇＃論壇＃不存在";
$MESS["F_ERR_FORUM_NO_ACCESS"] = "您無權查看評論。";
$MESS["F_ERR_TID_EMPTY"] = "該任務未指定。";
$MESS["F_ERR_TID_IS_NOT_EXIST"] = "未找到任務＃任務_id＃。";
$MESS["F_NO_MODULE"] = "論壇模塊未安裝";
$MESS["F_NO_MODULE_TASKS"] = "任務模塊未安裝。";
$MESS["NAV_OPINIONS"] = "評論";
