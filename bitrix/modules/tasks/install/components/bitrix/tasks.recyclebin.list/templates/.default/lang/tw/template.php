<?php
$MESS["TASKS_RECYCLEBIN_CONVERT_DATA"] = "現在正在更新任務引擎例程。數據恢復暫時不可用。請稍後再試。";
$MESS["TASKS_RECYCLEBIN_FILE_LIFETIME"] = "回收箱將使已刪除的文件保持30天。";
$MESS["TASKS_RECYCLEBIN_TITLE"] = "回收箱";
