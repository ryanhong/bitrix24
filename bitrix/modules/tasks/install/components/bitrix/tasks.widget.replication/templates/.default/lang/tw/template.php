<?php
$MESS["TASKS_TTDP_REPLICATION_ANY"] = "任何";
$MESS["TASKS_TTDP_REPLICATION_DAILY"] = "天";
$MESS["TASKS_TTDP_REPLICATION_DAY_INTERVAL"] = "一天間隔";
$MESS["TASKS_TTDP_REPLICATION_DAY_OF_MONTH"] = "天";
$MESS["TASKS_TTDP_REPLICATION_MONTHLY"] = "月";
$MESS["TASKS_TTDP_REPLICATION_MONTH_ALT"] = "月";
$MESS["TASKS_TTDP_REPLICATION_MONTH_SHORT"] = "莫。";
$MESS["TASKS_TTDP_REPLICATION_REPEATS"] = "迭代";
$MESS["TASKS_TTDP_REPLICATION_REPEAT_END"] = "重複直到";
$MESS["TASKS_TTDP_REPLICATION_REPEAT_END_C_DATE"] = "結束日期";
$MESS["TASKS_TTDP_REPLICATION_REPEAT_END_C_NONE"] = "沒有結束日期";
$MESS["TASKS_TTDP_REPLICATION_REPEAT_END_C_TIMES"] = "完成之後";
$MESS["TASKS_TTDP_REPLICATION_REPEAT_START"] = "重複出現";
$MESS["TASKS_TTDP_REPLICATION_REPEAT_TYPE"] = "重複任期";
$MESS["TASKS_TTDP_REPLICATION_TASK_CTIME"] = "創建的任務";
$MESS["TASKS_TTDP_REPLICATION_TZ_HINT"] = "任務創建時間顯示在＃TimeZone＃TimeZone中。";
$MESS["TASKS_TTDP_REPLICATION_WEEKLY"] = "星期";
$MESS["TASKS_TTDP_REPLICATION_WEEK_ALT"] = "星期";
$MESS["TASKS_TTDP_REPLICATION_YEARLY"] = "年";
