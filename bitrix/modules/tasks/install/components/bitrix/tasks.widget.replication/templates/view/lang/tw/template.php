<?php
$MESS["TASKS_SIDEBAR_TASK_REPEATS"] = "任務重複";
$MESS["TASKS_SIDEBAR_TEMPLATE_NOT_ACCESSIBLE"] = "無法查看重複的任務模板";
$MESS["TASKS_TWRV_DISABLE_REPLICATION"] = "暫停重複的任務";
$MESS["TASKS_TWRV_ENABLE_REPLICATION"] = "恢復重複的任務";
