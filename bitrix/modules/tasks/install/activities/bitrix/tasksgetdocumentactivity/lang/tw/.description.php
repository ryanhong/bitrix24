<?php
$MESS["TASKS_GLDA_CATEGORY"] = "任務";
$MESS["TASKS_GLDA_DESC"] = "獲得指定任務的現場值，以供進一步使用";
$MESS["TASKS_GLDA_DESC_1"] = "將任務字段值發送到其他自動化規則。";
$MESS["TASKS_GLDA_DESC_1_MSGVER_1"] = "將任務字段值發送到其他自動化規則。這是一個輔助自動化規則。";
$MESS["TASKS_GLDA_NAME"] = "讀取任務字段";
$MESS["TASKS_GLDA_NAME_1"] = "獲取任務信息";
