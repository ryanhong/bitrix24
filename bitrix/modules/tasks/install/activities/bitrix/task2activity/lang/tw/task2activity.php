<?php
$MESS["BPCDA_FIELD_REQUIED"] = "需要字段'＃字段＃'。";
$MESS["BPSA_CHECK_LIST_ITEMS"] = "清單";
$MESS["BPSA_CREATED_BY_ERROR"] = "任務創建者未指定。";
$MESS["BPSA_CYCLING_ERROR_1"] = "沒有創建任務，因為一個或多個活動可能會導致無限的遞歸。";
$MESS["BPSA_TRACK_CLOSED"] = "＃日期＃關閉任務";
$MESS["BPSA_TRACK_DELETED"] = "任務已刪除。";
$MESS["BPSA_TRACK_ERROR"] = "當您創建任務錯誤時。";
$MESS["BPSA_TRACK_OK"] = "使用ID ## Val＃創建任務";
$MESS["BPSA_TRACK_SUBSCR"] = "過程正在等待任務完成";
$MESS["BPSNMA_EMPTY_REQUIRED_PROPERTY"] = "屬性“＃property_name＃”丟失。";
$MESS["BPSNMA_EMPTY_TASKASSIGNEDTO"] = "未指定“負責”值。";
$MESS["BPSNMA_EMPTY_TASKNAME"] = "未指定“任務名稱”值。";
$MESS["BPTA1A_ADD_TO_REPORT_2"] = "在效率報告中包括任務";
$MESS["BPTA1A_ALLOW_TIME_TRACKING"] = "啟用任務的時間跟踪";
$MESS["BPTA1A_CHANGE_DEADLINE"] = "負責人可能會更改截止日期";
$MESS["BPTA1A_CHECK_RESULT"] = "任務需要關閉批准";
$MESS["BPTA1A_CHECK_RESULT_V2"] = "完成完成任務";
$MESS["BPTA1A_FIELD_NAME_AS_CHILD_TASK"] = "作為此任務的子任務創建";
$MESS["BPTA1A_FIELD_NAME_AUTO_LINK_TO_CRM_ENTITY"] = "與當前CRM實體結合";
$MESS["BPTA1A_FIELD_NAME_DEPENDS_ON"] = "依賴任務";
$MESS["BPTA1A_FIELD_NAME_TAGS"] = "標籤";
$MESS["BPTA1A_HOLD_TO_CLOSE"] = "任務運行時暫停過程";
$MESS["BPTA1A_MAKE_SUBTASK"] = "子任務的子任務";
$MESS["BPTA1A_REQUIRE_RESULT"] = "任務狀態摘要是必需的";
$MESS["BPTA1A_TASKACCOMPLICES"] = "參與者";
$MESS["BPTA1A_TASKACTIVEFROM"] = "開始";
$MESS["BPTA1A_TASKACTIVETO"] = "結尾";
$MESS["BPTA1A_TASKASSIGNEDTO"] = "負責任的";
$MESS["BPTA1A_TASKCREATEDBY"] = "創建任務為";
$MESS["BPTA1A_TASKDEADLINE"] = "最後期限";
$MESS["BPTA1A_TASKDETAILTEXT"] = "任務描述";
$MESS["BPTA1A_TASKFORUM"] = "任務評論論壇";
$MESS["BPTA1A_TASKGROUPID"] = "社交網絡組";
$MESS["BPTA1A_TASKNAME"] = "任務名稱";
$MESS["BPTA1A_TASKPRIORITY_V2"] = "優先事項";
$MESS["BPTA1A_TASKPRIORITY_V3"] = "優先級";
$MESS["BPTA1A_TASKTRACKERS"] = "觀察者";
$MESS["BPTA1A_TASK_TASK_PRESENCE_ERROR"] = "具有ID“＃task_id＃”的任務不存在。";
$MESS["BPTA1A_TASK_URL_LABEL"] = "打開任務";
$MESS["BPTA1A_TASK_URL_NAME"] = "任務";
$MESS["BPTA1A_TIME_TRACKING_H"] = "計劃的時間，數小時";
$MESS["BPTA1A_TIME_TRACKING_M"] = "計劃的時間，分鐘";
$MESS["TASK_EMPTY_GROUP"] = "個人任務";
