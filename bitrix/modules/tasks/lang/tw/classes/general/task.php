<?php
$MESS["TASKS_DEADLINE_OUT_OF_PROJECT_RANGE"] = "任務截止日期超出了項目日期範圍";
$MESS["TASKS_MESSAGE_ACCOMPLICES"] = "參與者";
$MESS["TASKS_MESSAGE_AUDITORS"] = "觀察者";
$MESS["TASKS_MESSAGE_DEADLINE"] = "最後期限";
$MESS["TASKS_MESSAGE_DESCRIPTION"] = "描述";
$MESS["TASKS_MESSAGE_NO"] = "不";
$MESS["TASKS_MESSAGE_PRIORITY"] = "優先事項";
$MESS["TASKS_MESSAGE_RESPONSIBLE"] = "負責人";
$MESS["TASKS_MESSAGE_TITLE"] = "姓名";
$MESS["TASKS_NEW_TASK"] = "新任務";
$MESS["TASKS_NEW_TASK_MESSAGE"] = "添加了一項新任務

任務名稱：＃Task_title＃
創建者：＃task_author＃
負責人：＃Task_Responsible＃
＃Task_Extra＃
查看任務：
＃path_to_task＃";
$MESS["TASKS_PLAN_DATE_END_OUT_OF_PROJECT_RANGE"] = "計劃的任務結束日期超出了項目日期範圍";
$MESS["TASKS_PLAN_DATE_START_OUT_OF_PROJECT_RANGE"] = "計劃的任務開始日期超出了項目日期範圍";
