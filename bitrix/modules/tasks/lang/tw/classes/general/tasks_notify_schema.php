<?php
$MESS["TASKS_NS_COMMENT"] = "添加了任務的新評論";
$MESS["TASKS_NS_MANAGE"] = "創建或更新的任務";
$MESS["TASKS_NS_REMINDER"] = "任務提醒";
$MESS["TASKS_NS_TASK_ASSIGNED"] = "分配給您的任務";
$MESS["TASKS_NS_TASK_EXPIRED_SOON"] = "任務幾乎逾期";
