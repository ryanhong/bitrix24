<?php
$MESS["JS_CORE_PL_TASKS"] = "今天的任務";
$MESS["JS_CORE_PL_TASKS_ADD"] = "輸入新任務";
$MESS["JS_CORE_PL_TASKS_CHOOSE"] = "從列表中選擇";
$MESS["JS_CORE_PL_TASKS_CREATE"] = "新任務";
$MESS["JS_CORE_PL_TASKS_FINISH"] = "完成任務並停止時間跟踪器";
$MESS["JS_CORE_PL_TASKS_MENU_REMOVE_FROM_PLAN"] = "從日常計劃中刪除";
$MESS["JS_CORE_PL_TASKS_START_TIMER"] = "開始時間跟踪器";
$MESS["JS_CORE_PL_TASKS_STOP_TIMER"] = "暫停我的計時器";
$MESS["TASKS_MODULE_NOT_FOUND"] = "任務模塊未安裝。";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "您已經在使用\“ {{title}} \”的時間跟踪器。此任務將被暫停。繼續？";
$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "您已經在使用\“ {{title}} \”的時間跟踪器。此任務將被暫停。繼續？";
