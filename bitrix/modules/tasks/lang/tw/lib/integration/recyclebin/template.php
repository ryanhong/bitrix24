<?php
$MESS["TASKS_RECYCLEBIN_TEMPLATE_MOVED_TO_RECYCLEBIN"] = "模板已移至<a href= \"#recyclebin_url# \ \">回收箱</a>。";
$MESS["TASKS_TEMPLATE_RECYCLEBIN_REMOVE_CONFIRM"] = "該模板將被永久刪除。您確定要繼續嗎？";
$MESS["TASKS_TEMPLATE_RECYCLEBIN_REMOVE_MESSAGE"] = "模板已刪除";
$MESS["TASKS_TEMPLATE_RECYCLEBIN_RESTORE_CONFIRM"] = "您是否要恢復所選模板並將其移回列表？";
$MESS["TASKS_TEMPLATE_RECYCLEBIN_RESTORE_MESSAGE"] = "模板已恢復";
