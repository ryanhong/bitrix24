<?php
$MESS["TASKS_RECYCLEBIN_REMOVE_CONFIRM"] = "該任務將被永久刪除。您確定要繼續嗎？";
$MESS["TASKS_RECYCLEBIN_REMOVE_MESSAGE"] = "任務已刪除";
$MESS["TASKS_RECYCLEBIN_RESTORE_CONFIRM"] = "您是否要恢復所選任務並將其移回列表？";
$MESS["TASKS_RECYCLEBIN_RESTORE_MESSAGE"] = "任務已恢復";
$MESS["TASKS_RECYCLEBIN_TASK_MOVED_TO_RECYCLEBIN"] = "任務已移至<a href= \"#recyclebin_url# \ \">回收bin </a>。";
