<?php
$MESS["TASKS_COMMENT_MESSAGE_ADD_F"] = "為任務添加了評論＃task_url_begin ## task_title ## url_end＃。";
$MESS["TASKS_COMMENT_MESSAGE_ADD_M"] = "為任務添加了評論＃task_url_begin ## task_title ## url_end＃。";
$MESS["TASKS_COMMENT_MESSAGE_ADD_N"] = "為任務添加了評論＃task_url_begin ## task_title ## url_end＃。";
$MESS["TASKS_COMMENT_MESSAGE_ADD_PUSH_F"] = "＃user_name＃在任務上評論＃task_title＃";
$MESS["TASKS_COMMENT_MESSAGE_ADD_PUSH_M"] = "＃user_name＃在任務上評論＃task_title＃";
$MESS["TASKS_COMMENT_MESSAGE_ADD_WITH_TEXT"] = " 評論文本是：“＃Task_comment_text＃”";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_F"] = "將註釋更新為＃task_url_begin ## task_title ## url_end＃。";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_M"] = "將註釋更改為＃task_url_begin ## task_title ## url_end＃。";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_N"] = "將註釋更改為＃task_url_begin ## task_title ## url_end＃。";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_WITH_TEXT"] = " 新文本是：“＃task_comment_text＃”";
