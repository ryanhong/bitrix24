<?php
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_0"] = "發展";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_1"] = "源代碼已完成並簽入VCS存儲庫";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_2"] = "源代碼遵循代碼樣式指南";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_3"] = "源代碼測試錯誤；發現錯誤解決了";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_4"] = "源代碼測試了安全缺陷；發現問題解決了";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_5"] = "所需的功能和功能已測試錯誤；發現錯誤解決了";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_6"] = "API測試並發現向後兼容；發現後退兼容性問題已解決";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_7"] = "功能自動測試通過";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_8"] = "源代碼審查（不良編碼實踐的快速視覺檢查）";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_9"] = "產品所有者接受的積壓項目。如果接受演示項目會產生大量時間或精力，則必須在演示之前接受。";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_10"] = "批准的穩定文本消息提交了本地化。";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_NEW_0"] = "完成的定義";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_NEW_1"] = "已驗證任務（測試完成，或任務作者接受的結果）。";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_NEW_2"] = "產品所有者接受的任務。";
$MESS["TASKS_SCRUM_DEFINITION_OF_DONE_NEW_3"] = "準備任務說明或文檔（如果需要）。";
