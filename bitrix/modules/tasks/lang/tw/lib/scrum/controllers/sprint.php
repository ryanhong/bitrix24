<?php
$MESS["TASKS_SCRUM_SPRINT_NAME"] = "sprint％s";
$MESS["TASKS_SCRUM_SPRINT_START_ALREADY_ERROR"] = "您已經有一個跑步衝刺。";
$MESS["TASKS_SCRUM_SPRINT_START_ERROR"] = "錯誤啟動Sprint。";
$MESS["TASKS_SCRUM_SPRINT_START_NOT_TASKS_ERROR"] = "沒有任務就無法運行沖刺。";
$MESS["TSSC_ERROR_ACCESS_DENIED"] = "拒絕訪問。";
$MESS["TSSC_ERROR_COULD_NOT_READ_ACTIVE_SPRINT"] = "無法完成主動衝刺。它可能已經完成。";
$MESS["TSSC_ERROR_COULD_NOT_READ_SPRINT"] = "無法獲取表格的數據。";
$MESS["TSSC_ERROR_INCLUDE_MODULE_ERROR"] = "無法連接所需的模塊。";
