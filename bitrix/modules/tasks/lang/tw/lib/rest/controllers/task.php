<?php
$MESS["TASKS_FAILED_RESULT_REQUIRED"] = "任務創建者要求您提供任務報告。<br>對任務發表評論並將其標記為任務狀態摘要。";
$MESS["TASKS_FAILED_START_TASK_TIMER"] = "無法啟動任務計時器";
$MESS["TASKS_FAILED_STOP_TASK_TIMER"] = "無法停止任務計時器";
$MESS["TASKS_FAILED_WRONG_ORDER_FIELD"] = "不正確的排序字段";
$MESS["TASKS_OTHER_TASK_ON_TIMER"] = "現在，另一個任務（## ID＃）當前處於活動狀態。請在繼續之前停止它。";
