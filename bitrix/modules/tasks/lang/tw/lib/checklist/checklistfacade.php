<?php
$MESS["TASKS_CHECKLIST_FACADE_ACTION_ADD"] = "新增項目";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_MODIFY"] = "編輯項目";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_NOT_ALLOWED"] = "＃action_name＃：動作不可用";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_REMOVE"] = "刪除項目";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_REORDER"] = "移動項目";
$MESS["TASKS_CHECKLIST_FACADE_ACTION_TOGGLE"] = "編輯項目狀態";
$MESS["TASKS_CHECKLIST_FACADE_ATTACHMENT_ADDING_FAILED"] = "錯誤添加附件";
$MESS["TASKS_CHECKLIST_FACADE_CHECKLIST_DELETE_FAILED"] = "錯誤刪除項目";
$MESS["TASKS_CHECKLIST_FACADE_EMPTY_FIELDS"] = "未指定可編輯字段";
$MESS["TASKS_CHECKLIST_FACADE_EMPTY_TITLE"] = "項目名稱丟失";
$MESS["TASKS_CHECKLIST_FACADE_MEMBER_DELETE_FAILED"] = "錯誤刪除參與者";
$MESS["TASKS_CHECKLIST_FACADE_NOT_ALLOWED_FIELD"] = "未知字段通過[＃field_name＃]";
$MESS["TASKS_CHECKLIST_FACADE_NO_LOOPS_AVAILABLE"] = "父項目不可能是本身的子信息";
$MESS["TASKS_CHECKLIST_FACADE_USER_FIELD_DELETE_FAILED"] = "錯誤刪除自定義字段";
$MESS["TASKS_CHECKLIST_FACADE_WRONG_MEMBER_TYPE"] = "未知的用戶類型傳遞[＃類型＃]";
