<?php
$MESS["TASKS_CHECKLIST_TREE_CHILD_ATTACH"] = "無法創建循環引用[＃id＃，＃parent_id＃]";
$MESS["TASKS_CHECKLIST_TREE_EXISTING_NODE_ADDING"] = "無法添加節點，因為它已經存在[＃id＃，＃parent_id＃]";
$MESS["TASKS_CHECKLIST_TREE_ILLEGAL_NODE"] = "不正確的節點ID [＃id＃，＃parent_id＃]";
$MESS["TASKS_CHECKLIST_TREE_NODE_NOT_FOUND"] = "找不到指定的節點[＃id＃]";
$MESS["TASKS_CHECKLIST_TREE_PATH_EXISTS"] = "節點已經鏈接[＃id＃，＃parent_id＃]";
$MESS["TASKS_CHECKLIST_TREE_SELF_ATTACH"] = "無法將節點附加到自身[＃id＃，＃parent_id＃]";
