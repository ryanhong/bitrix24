<?php
$MESS["TASKS_ITEM_TASK_TEMPLATE_BAD_RESPONSIBLE_ERROR"] = "找不到字段中指定的用戶\“負責人\”。";
$MESS["TASKS_ITEM_TASK_TEMPLATE_CANT_SWITCH_TYPE_ERROR"] = "無法更改現有模板的類型";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_BASE_TEMPLATE_ID"] = "一般模板";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_ACCESS"] = "訪問權限";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_CHECKLIST"] = "清單";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_TAG"] = "標籤";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_MULTITASK"] = "如果指定了多個負責人，則無法分配基本模板。";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_REPLICATION"] = "無法分配基本模板，因為啟用了重複執行。";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "無法將基本模板分配給新的用戶模板。";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_MULTITASKING_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "無法為新用戶創建多個負責人模板。";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_REPLICATION_ALLOWED_ERROR_BASE_TEMPLATE_ID"] = "分配基本模板時無法設置重複執行參數。";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_REPLICATION_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "創建新的用戶模板時，無法設置重複執行參數。";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BASE_TEMPLATE_ID"] = "分配基本模板時無法創建新的用戶模板。";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BECAUSE_MULTITASK"] = "無法創建新的用戶模板，因為啟用了重複執行。";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BECAUSE_REPLICATION"] = "無法創建新的用戶模板，因為啟用了重複執行。";
$MESS["TASKS_ITEM_TASK_TEMPLATE_PARENT_ITEM_CONFLICT_ERROR"] = "無法同時設置常規模板和基本任務";
