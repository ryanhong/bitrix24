<?php
$MESS["ERROR_TASKS_GUID_NON_UNIQUE"] = "GUID值必須是唯一的";
$MESS["TASKS_BAD_CREATED_BY"] = "任務創建者未指定。";
$MESS["TASKS_BAD_DURATION"] = "計劃的任務持續時間太長";
$MESS["TASKS_BAD_PARENT_ID"] = "字段中指定的任務“子任務”沒找到。";
$MESS["TASKS_BAD_PLAN_DATES"] = "計劃的結束日期早於開始日期。";
$MESS["TASKS_BAD_RESPONSIBLE_ID"] = "未指定負責人。";
$MESS["TASKS_BAD_RESPONSIBLE_ID_EX"] = "找不到字段中指定的用戶\“負責人\”。";
$MESS["TASKS_BAD_TITLE"] = "任務名稱未指定。";
$MESS["TASKS_DEADLINE_OUT_OF_PROJECT_RANGE"] = "任務截止日期超出了項目日期範圍";
$MESS["TASKS_DEPENDS_ON_SELF"] = "任務不能取決於自己。";
$MESS["TASKS_INCORRECT_STATUS"] = "狀態不正確";
$MESS["TASKS_IS_LINKED_END_DATE_PLAN_REMOVE"] = "由於任務具有依賴關係，因此無法刪除任務的完成時間";
$MESS["TASKS_IS_LINKED_SET_PARENT"] = "無法分配父任務，因為任務取決於當前任務";
$MESS["TASKS_PLAN_DATE_END_OUT_OF_PROJECT_RANGE"] = "計劃的任務結束日期超出了項目日期範圍";
$MESS["TASKS_PLAN_DATE_START_OUT_OF_PROJECT_RANGE"] = "計劃的任務開始日期超出了項目日期範圍";
