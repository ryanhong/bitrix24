<?php
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_0_TEXT"] = "紅色計數器顯示了逾期任務的數量。單擊查看它們。";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_0_TITLE"] = "時間快了！";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_1_TEXT"] = "單擊並移動截止日期。櫃檯將重置。櫃檯是您的小助手，在截止日期臨時時提示您。";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_1_TITLE"] = "移動截止日期";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_2_BUTTON"] = "知道了謝謝";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_2_TEXT"] = "觀看您的櫃檯按計劃按計劃進行。";
$MESS["TASKS_TOUR_GUIDE_EXPIRED_TASKS_DEADLINE_CHANGE_POPUP_2_TITLE"] = "您做得很好，現在沒有櫃檯！";
