<?php
$MESS["TASKS_LIST_CHOOSE_ACTION"] = "選擇動作";
$MESS["TASKS_LIST_GROUP_ACTION_ADD_ACCOMPLICE"] = "添加參與者";
$MESS["TASKS_LIST_GROUP_ACTION_ADD_AUDITOR"] = "添加觀察者";
$MESS["TASKS_LIST_GROUP_ACTION_ADD_FAVORITE"] = "添加到收藏夾";
$MESS["TASKS_LIST_GROUP_ACTION_CHANGE_ORIGINATOR"] = "更改創建者";
$MESS["TASKS_LIST_GROUP_ACTION_CHANGE_RESPONSIBLE"] = "改變負責人";
$MESS["TASKS_LIST_GROUP_ACTION_COMPLETE"] = "完全的";
$MESS["TASKS_LIST_GROUP_ACTION_DELETE_FAVORITE"] = "從收藏夾中刪除";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_AT_DAY"] = "天";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_AT_MONTH"] = "月";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_AT_WEEK"] = "星期";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_LEFT"] = "向後移動截止日期";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_RIGHT"] = "向前移動截止日期";
$MESS["TASKS_LIST_GROUP_ACTION_MUTE"] = "沉默的";
$MESS["TASKS_LIST_GROUP_ACTION_PING"] = "ping";
$MESS["TASKS_LIST_GROUP_ACTION_REMOVE"] = "刪除";
$MESS["TASKS_LIST_GROUP_ACTION_SET_DEADLINE"] = "設定截止日期";
$MESS["TASKS_LIST_GROUP_ACTION_SET_GROUP"] = "設置組（項目）";
$MESS["TASKS_LIST_GROUP_ACTION_SET_TASK_CONTROL_NO"] = "不";
$MESS["TASKS_LIST_GROUP_ACTION_SET_TASK_CONTROL_V2"] = "禁用完成後檢查";
$MESS["TASKS_LIST_GROUP_ACTION_SET_TASK_CONTROL_YES"] = "是的";
$MESS["TASKS_LIST_GROUP_ACTION_UNMUTE"] = "取消靜音";
