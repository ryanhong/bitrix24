<?php
$MESS["TASKS_GRID_TASK_ROW_CONTENT_STATUS_1"] = "新的";
$MESS["TASKS_GRID_TASK_ROW_CONTENT_STATUS_2"] = "待辦的";
$MESS["TASKS_GRID_TASK_ROW_CONTENT_STATUS_3"] = "進行中";
$MESS["TASKS_GRID_TASK_ROW_CONTENT_STATUS_4"] = "待審核";
$MESS["TASKS_GRID_TASK_ROW_CONTENT_STATUS_5"] = "完全的";
$MESS["TASKS_GRID_TASK_ROW_CONTENT_STATUS_6"] = "遞延";
$MESS["TASKS_GRID_TASK_ROW_CONTENT_STATUS_7"] = "拒絕";
