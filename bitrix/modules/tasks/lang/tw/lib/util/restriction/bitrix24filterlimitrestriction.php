<?php
$MESS["TASKS_RESTRICTION_FILTER_LIMIT_TEXT"] = "
<p>我們正在努力提高服務質量和績效，以適應您的業務規模。隨著業務的增長，數據的數量增加：每天創建新任務，交易和其他實體。</p>
<p>您的業務規模越大，搜索您創建的數據就越多。選擇適合您數據以更快搜索的計劃。</p>";
$MESS["TASKS_RESTRICTION_FILTER_LIMIT_TITLE"] = "達到任務限制（＃limit＃）";
