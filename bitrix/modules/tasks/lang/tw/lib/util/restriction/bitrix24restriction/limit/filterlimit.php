<?php
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_NOTIFICATION"] = "你是真正的職業！現在，您在BitRix24中擁有＃Count＃任務。請注意：當其數字超過＃limit＃時，您只能在商業計劃上搜索任務。搜索如此多的任務將需要大量資源，因此只有預設過濾器才能在免費計劃中使用。 ＃helpdesk_link＃";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_NOTIFICATION_HELPDESK_LINK"] = "細節";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_CONTENT"] = "
<p>我們正在努力提高服務質量和績效，以適應您的業務規模。隨著業務的增長，數據的數量增加：每天創建新任務，交易和其他實體。</p>
<p>您的業務規模越大，搜索您創建的數據就越多。選擇適合您數據以更快搜索的計劃。</p>";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_CONTENT_V2"] = "<p>達到了任務搜索限制（＃limit＃）。我們正在努力提高服務質量和績效以適合您的業務規模。隨著業務的增長，數據的數量增加：每天創建新任務，交易和其他實體。</p>
<p>您的業務規模越大，搜索您創建的數據就越多。選擇適合您數據以更快搜索的計劃。</br> #helpdesk_link＃</p>
";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_HELPDESK_LINK"] = "細節";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_TITLE"] = "達到任務限制（＃limit＃）";
$MESS["TASKS_RESTRICTION_B24_RESTRICTION_LIMIT_FILTER_STUB_TITLE_V2"] = "達到了搜索限制";
