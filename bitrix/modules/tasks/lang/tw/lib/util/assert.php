<?php
$MESS["TASKS_ASSERT_ARRAY_EXPECTED"] = "參數＃arg_name＃必須是一個數組。";
$MESS["TASKS_ASSERT_ARRAY_NOT_EMPTY_EXPECTED"] = "參數＃arg_name＃必須是一個非空數數組。";
$MESS["TASKS_ASSERT_ARRAY_OF_INTEGER_NOT_NULL_EXPECTED"] = "參數＃arg_name＃不是一個積極的整數。";
$MESS["TASKS_ASSERT_ARRAY_OF_STRING_NOT_NULL_EXPECTED"] = "參數＃arg_name＃不是一系列非空字符串。";
$MESS["TASKS_ASSERT_EMPTY_ARGUMENT"] = "空的參數傳遞。";
$MESS["TASKS_ASSERT_EMPTY_ENUMERATION"] = "一個空的枚舉傳遞給了檢查方法。";
$MESS["TASKS_ASSERT_INTEGER_EXPECTED"] = "參數＃arg_name＃必須是一個整數。";
$MESS["TASKS_ASSERT_INTEGER_NONNEGATIVE_EXPECTED"] = "參數＃arg_name＃必須是一個非負整數。";
$MESS["TASKS_ASSERT_INTEGER_NOTNULL_EXPECTED"] = "參數＃arg_name＃必須是一個積極的整數。";
$MESS["TASKS_ASSERT_ITEM_NOT_IN_ENUMERATION"] = "參數＃arg_name＃不是枚舉值之一。";
$MESS["TASKS_ASSERT_STRING_NOTNULL_EXPECTED"] = "參數＃arg_name＃必須是一個非空字符串。";
