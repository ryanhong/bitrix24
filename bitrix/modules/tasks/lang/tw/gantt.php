<?php
$MESS["TASKS_GANTT_CHART_TITLE"] = "任務";
$MESS["TASKS_GANTT_CIRCULAR_DEPENDENCY"] = "不允許圓形鏈接";
$MESS["TASKS_GANTT_DATE_END"] = "結束日期";
$MESS["TASKS_GANTT_DATE_START"] = "開始日期";
$MESS["TASKS_GANTT_DEADLINE"] = "最後期限";
$MESS["TASKS_GANTT_DELETE_DEPENDENCY"] = "刪除";
$MESS["TASKS_GANTT_DEPENDENCY_FROM"] = "從";
$MESS["TASKS_GANTT_DEPENDENCY_TO"] = "到";
$MESS["TASKS_GANTT_EMPTY_DATE"] = "沒有任何";
$MESS["TASKS_GANTT_EMPTY_END_DATE"] = "建立鏈接需要結束日期";
$MESS["TASKS_GANTT_END"] = "結尾";
$MESS["TASKS_GANTT_INDENT_TASK"] = "縮進任務";
$MESS["TASKS_GANTT_OUTDENT_TASK"] = "OUTTENT任務";
$MESS["TASKS_GANTT_PERMISSION_ERROR"] = "您無權編輯此任務的日期";
$MESS["TASKS_GANTT_RELATION_ERROR"] = "無法將任務鏈接到其父任務";
$MESS["TASKS_GANTT_START"] = "開始";
