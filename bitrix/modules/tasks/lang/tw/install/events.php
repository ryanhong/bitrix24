<?php
$MESS["TASKS_ADD_COMMENT_NAME"] = "任務添加了新評論";
$MESS["TASKS_ADD_TASK_DESC"] = "＃email_to＃ - 收件人電子郵件
＃Task_ID＃ - 任務ID
＃coverient_id＃ - 分配的用戶ID
＃url_id＃ - 任務URL
";
$MESS["TASKS_ADD_TASK_NAME"] = "添加了新任務";
$MESS["TASKS_TASK_ADD_EMAIL_DESC"] = "＃email_to＃ - 收件人電子郵件
＃Task_ID＃ - 任務ID
＃Task_Title＃ - 任務標題
＃coverient_id＃ - 分配的用戶ID
＃user_id＃ - 用戶ID檢查任務權限
＃URL＃ - 任務URL
＃主題＃ - 消息標題
";
$MESS["TASKS_TASK_ADD_EMAIL_NAME"] = "添加了新任務（電子郵件用戶）";
$MESS["TASKS_TASK_COMMENT_ADD_EMAIL_DESC"] = "＃email_to＃ - 收件人電子郵件
＃Task_ID＃ - 任務ID
＃Task_Title＃ - 任務標題
＃comment_id＃ - 評論ID
＃coverient_id＃ - 分配的用戶ID
＃user_id＃ - 用戶ID檢查任務權限
＃URL＃ - 任務URL
＃主題＃ - 消息標題";
$MESS["TASKS_TASK_COMMENT_ADD_EMAIL_NAME"] = "添加到任務的新評論（電子郵件用戶）";
$MESS["TASKS_TASK_UPDATE_EMAIL_DESC"] = "＃email_to＃ - 收件人電子郵件
＃Task_ID＃ - 任務ID
＃Task_Title＃ - 任務標題
＃Task_previous_fields＃ - 更改之前的任務詳細信息
＃coverient_id＃ - 分配的用戶ID
＃user_id＃ - 用戶ID檢查任務權限
＃URL＃ - 任務URL
＃主題＃ - 消息標題
";
$MESS["TASKS_TASK_UPDATE_EMAIL_NAME"] = "更改了任務（電子郵件用戶）";
$MESS["TASKS_UPDATE_TASK_DESC"] = "＃email_to＃ - 收件人電子郵件
＃Task_ID＃ - 任務ID
＃coverient_id＃ - 分配的用戶ID
＃url_id＃ - 任務URL
";
$MESS["TASKS_UPDATE_TASK_NAME"] = "任務狀態更改";
$MESS["TASK_REMINDER_DESC"] = "＃Task_Title＃ - 任務名稱
＃path_to_task＃ - 任務URL";
$MESS["TASK_REMINDER_MESSAGE"] = "來自＃site_name＃的消息
-------------------------------------------------- --------
單擊下面的鏈接以查看任務：

＃path_to_task＃

此消息已自動生成。";
$MESS["TASK_REMINDER_NAME"] = "任務提醒";
$MESS["TASK_REMINDER_SUBJECT"] = "＃site_name＃：這是\“＃task_title＃\”提醒。";
