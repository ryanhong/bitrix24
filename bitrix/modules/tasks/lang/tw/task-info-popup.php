<?php
$MESS["TASKS_DATE_COMPLETED"] = "完成於";
$MESS["TASKS_DATE_CREATED"] = "創建於";
$MESS["TASKS_DATE_DEADLINE"] = "最後期限";
$MESS["TASKS_DATE_END"] = "結束日期";
$MESS["TASKS_DATE_START"] = "開始日期";
$MESS["TASKS_DATE_STARTED"] = "開始";
$MESS["TASKS_DIRECTOR"] = "創造者";
$MESS["TASKS_FILES"] = "文件";
$MESS["TASKS_PRIORITY_V2"] = "優先級";
$MESS["TASKS_QUICK_INFO_DETAILS"] = "細節";
$MESS["TASKS_QUICK_INFO_EMPTY_DATE"] = "不";
$MESS["TASKS_RESPONSIBLE"] = "負責任的";
$MESS["TASKS_STATUS"] = "地位";
$MESS["TASKS_STATUS_ACCEPTED"] = "待辦的";
$MESS["TASKS_STATUS_COMPLETED"] = "完全的";
$MESS["TASKS_STATUS_DECLINED"] = "拒絕";
$MESS["TASKS_STATUS_DELAYED"] = "遞延";
$MESS["TASKS_STATUS_IN_PROGRESS"] = "進行中";
$MESS["TASKS_STATUS_NEW"] = "新的";
$MESS["TASKS_STATUS_OVERDUE"] = "逾期";
$MESS["TASKS_STATUS_WAITING"] = "待審核";
$MESS["TASKS_TASK_TITLE_LABEL"] = "任務編號";
