<?php
$MESS["ME_ACTION"] = "開始開會";
$MESS["ME_AGENDA"] = "議程";
$MESS["ME_CHANGE"] = "改變";
$MESS["ME_CLOSE"] = "結束會議";
$MESS["ME_COMMENTS"] = "評論";
$MESS["ME_COPY"] = "創建下一個會議";
$MESS["ME_CURRENT_STATE"] = "地位";
$MESS["ME_DATE_START"] = "開始";
$MESS["ME_DESCR_TITLE"] = "會議描述";
$MESS["ME_EDIT_TITLE"] = "編輯";
$MESS["ME_FILES"] = "文件";
$MESS["ME_GROUP"] = "專案";
$MESS["ME_KEEPER"] = "會議記錄秘書";
$MESS["ME_LIST_TITLE"] = "查看會議";
$MESS["ME_MEMBERS"] = "與會者";
$MESS["ME_OWNER"] = "所有者";
$MESS["ME_PLACE"] = "地點";
$MESS["ME_PREPARE"] = "簡歷會議";
$MESS["ME_REFUSED"] = "拒絕";
