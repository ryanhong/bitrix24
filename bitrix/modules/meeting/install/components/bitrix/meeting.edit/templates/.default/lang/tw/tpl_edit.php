<?php
$MESS["ME_AGENDA"] = "議程";
$MESS["ME_AGENDA_EX"] = "上一次會議的話題";
$MESS["ME_CANCEL"] = "取消";
$MESS["ME_CREATE"] = "創建會議";
$MESS["ME_DATE"] = "日期";
$MESS["ME_DESCRIPTION"] = "會議描述";
$MESS["ME_DESCR_TITLE"] = "會議描述";
$MESS["ME_DURATION"] = "期間";
$MESS["ME_DURATION_60"] = "分鐘";
$MESS["ME_DURATION_3600"] = "小時";
$MESS["ME_EDIT_TITLE"] = "編輯";
$MESS["ME_EVENT_NOTIFY"] = "通知與會者確認或拒絕邀請";
$MESS["ME_EVENT_REINVITE"] = "要求用戶重新確認出席";
$MESS["ME_FILES"] = "文件";
$MESS["ME_GROUP"] = "項目會議";
$MESS["ME_KEEPER"] = "行政助理";
$MESS["ME_LIST_TITLE"] = "查看會議";
$MESS["ME_MEMBERS"] = "與會者";
$MESS["ME_MR_FREE"] = "會議室可用。";
$MESS["ME_MR_RESERVED"] = "會議室被佔領。";
$MESS["ME_MR_RESERVED_WARNING"] = "您選擇的會議室被佔用。請選擇另一個地方或時間。";
$MESS["ME_PLACE"] = "地點";
$MESS["ME_PLANNER"] = "計劃者";
$MESS["ME_SAVE"] = "節省";
$MESS["ME_TIME"] = "時間";
$MESS["ME_TITLE"] = "話題";
$MESS["ME_TITLE_DEFAULT"] = "會議主題";
$MESS["ME_VIEW_TITLE"] = "看法";
