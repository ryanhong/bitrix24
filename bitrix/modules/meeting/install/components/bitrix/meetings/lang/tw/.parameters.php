<?php
$MESS["INTL_IBLOCK"] = "會議室信息塊";
$MESS["INTL_IBLOCK_TYPE"] = "會議室信息塊類型";
$MESS["INTL_IBLOCK_TYPE_V"] = "視頻會議室信息塊類型";
$MESS["INTL_IBLOCK_V"] = "視頻會議室信息塊";
$MESS["M_MEETINGS_COUNT"] = "每頁會議";
$MESS["M_NAME_TEMPLATE"] = "名稱顯示模板";
$MESS["M_PARAM_list"] = "會議頁面路徑模板";
$MESS["M_PARAM_meeting"] = "會議查看頁面路徑模板";
$MESS["M_PARAM_meeting_copy"] = "下一個會議創建頁面路徑模板";
$MESS["M_PARAM_meeting_edit"] = "會議編輯頁面路徑模板";
$MESS["M_PARAM_meeting_item"] = "議程主題詳細信息頁面路徑模板";
