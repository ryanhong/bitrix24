<?php
$MESS["BPCGERR_INVALID_ARG"] = "參數“＃param＃”具有無效的值。";
$MESS["BPCGERR_INVALID_ARG1"] = "參數'＃param＃具有無效的值'＃值＃'";
$MESS["BPCGERR_INVALID_TYPE"] = "“＃param＃”參數的類型無效。";
$MESS["BPCGERR_INVALID_TYPE1"] = "“＃param＃”參數必須為'＃value＃'類型。";
$MESS["BPCGERR_NULL_ARG"] = "參數“＃param＃”是未定義的。";
