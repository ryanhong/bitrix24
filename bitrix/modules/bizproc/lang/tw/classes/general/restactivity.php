<?php
$MESS["BPRA_DEBUG_EVENT"] = "由於測試模式事件而跳過了申請響應等待";
$MESS["BPRA_DEFAULT_LOG_MESSAGE"] = "收到申請響應";
$MESS["BPRA_DEFAULT_STATUS_MESSAGE"] = "等待申請響應";
$MESS["BPRA_NOT_FOUND_ERROR"] = "未安裝應用程序活動。";
$MESS["BPRA_PD_CONFIGURE"] = "配置";
$MESS["BPRA_PD_ERROR_EMPTY_PROPERTY"] = "需要此字段：＃名稱＃";
$MESS["BPRA_PD_NO"] = "不";
$MESS["BPRA_PD_SET_STATUS_MESSAGE"] = "設置狀態消息";
$MESS["BPRA_PD_STATUS_MESSAGE"] = "狀態文字";
$MESS["BPRA_PD_TIMEOUT_DURATION"] = "暫停";
$MESS["BPRA_PD_TIMEOUT_DURATION_HINT"] = "是時候等待申請響應了";
$MESS["BPRA_PD_TIMEOUT_LIMIT"] = "最小等待時間";
$MESS["BPRA_PD_TIME_D"] = "天";
$MESS["BPRA_PD_TIME_H"] = "小時";
$MESS["BPRA_PD_TIME_M"] = "分鐘";
$MESS["BPRA_PD_TIME_S"] = "秒";
$MESS["BPRA_PD_USER_ID"] = "運行為";
$MESS["BPRA_PD_USE_SUBSCRIPTION"] = "等待回應";
$MESS["BPRA_PD_YES"] = "是的";
