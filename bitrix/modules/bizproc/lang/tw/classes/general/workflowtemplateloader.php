<?php
$MESS["BPCGWTL_CANT_DELETE"] = "無法刪除業務流程模板，因為使用此模板存在一個或多個活動的業務流程（ES）。";
$MESS["BPCGWTL_EMPTY_TEMPLATE"] = "業務過程模板'＃id＃'是空的。";
$MESS["BPCGWTL_INVALID1"] = "“＃名＃”的值不是整數。";
$MESS["BPCGWTL_INVALID2"] = "“＃名稱＃”的值不是實際數字。";
$MESS["BPCGWTL_INVALID3"] = "“＃名稱＃”的值不正確。";
$MESS["BPCGWTL_INVALID4"] = "“＃名稱＃”的值不評估為“是”或“否”。";
$MESS["BPCGWTL_INVALID5"] = "“＃名＃”中使用的日期格式不遵循格式'＃格式＃'。";
$MESS["BPCGWTL_INVALID6"] = "“用戶”類型的參數“＃名＃”需要安裝社交網絡模塊。";
$MESS["BPCGWTL_INVALID7"] = "“＃名稱＃”參數的類型是未定義的。";
$MESS["BPCGWTL_INVALID8"] = "名字的字段是必需的。";
$MESS["BPCGWTL_INVALID_WF_ID"] = "找不到業務流程模板“＃id＃”。";
$MESS["BPCGWTL_UNKNOWN_ERROR"] = "未知錯誤。";
$MESS["BPCGWTL_WRONG_TEMPLATE"] = "業務流程模板不正確";
$MESS["BPWTL_ERROR_MESSAGE_PREFIX"] = "動作'＃標題＃'：";
