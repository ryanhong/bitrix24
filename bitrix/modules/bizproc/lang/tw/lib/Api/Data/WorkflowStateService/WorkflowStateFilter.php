<?php
$MESS["BIZPROC_API_DATA_WORKFLOW_STATE_FILTER_PRESET_ALL_COMPLETED"] = "完全的";
$MESS["BIZPROC_API_DATA_WORKFLOW_STATE_FILTER_PRESET_HAS_TASK"] = "我是受讓人";
$MESS["BIZPROC_API_DATA_WORKFLOW_STATE_FILTER_PRESET_IN_WORK"] = "進行中";
$MESS["BIZPROC_API_DATA_WORKFLOW_STATE_FILTER_PRESET_STARTED"] = "由我開始";
