<?php
$MESS["BIZPROC_VALIDATOR_ARRAY_KEY"] = "數組'＃名稱＃'指定不正確的鍵'＃val＃'。";
$MESS["BIZPROC_VALIDATOR_ARRAY_VAL"] = "數組'＃名稱＃'包含不正確的值'＃val＃'。";
$MESS["BIZPROC_VALIDATOR_ENUM"] = "字段'＃名稱＃'必須是值之一'＃enum＃'。";
$MESS["BIZPROC_VALIDATOR_IS_ARRAY"] = "字段'＃名稱＃'必須是一個數組。";
$MESS["BIZPROC_VALIDATOR_IS_NUM"] = "字段'＃名稱＃'必須是一個數字。";
$MESS["BIZPROC_VALIDATOR_IS_STRING"] = "字段'＃名稱＃'必須是字符串。";
$MESS["BIZPROC_VALIDATOR_NUM_MAX"] = "字段'＃名稱＃'不得大於＃max＃。";
$MESS["BIZPROC_VALIDATOR_NUM_MIN"] = "字段'＃名稱＃'不得小於＃min＃。";
$MESS["BIZPROC_VALIDATOR_REGEXP"] = "字段'＃名稱＃'不匹配正則表達式'＃模式＃'。";
$MESS["BIZPROC_VALIDATOR_REQUIRE"] = "需要字段'＃名稱＃'。";
$MESS["BIZPROC_VALIDATOR_STRING_MAX"] = "字段'＃名稱＃'不得包含超過＃最大＃字符。";
$MESS["BIZPROC_VALIDATOR_STRING_MIN"] = "字段'＃名稱＃'必須至少包含＃min＃字符。";
