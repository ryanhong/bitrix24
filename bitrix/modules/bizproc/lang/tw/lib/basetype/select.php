<?php
$MESS["BPDT_SELECT_INVALID"] = "所選項目不是列表項目。";
$MESS["BPDT_SELECT_NOT_SET"] = "沒有設置";
$MESS["BPDT_SELECT_OPTIONS1"] = "在新行上指定每個變體。如果變體值和變體名稱不同，則將名稱帶有方括號中的值。例如：[v1]變體1";
$MESS["BPDT_SELECT_OPTIONS2"] = "完成後單擊\“設置\”。";
$MESS["BPDT_SELECT_OPTIONS3"] = "放";
