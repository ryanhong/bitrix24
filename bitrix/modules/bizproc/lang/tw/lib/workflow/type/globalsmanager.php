<?php
$MESS["BIZPROC_LIB_WF_TYPE_GLOBALS_MANAGER_CAN_NOT_UPSERT"] = "無法添加或更新項目。";
$MESS["BIZPROC_LIB_WF_TYPE_GLOBAL_FIELD_VISIBILITY_SHORT_GLOBAL"] = "到處";
$MESS["BIZPROC_WF_TYPE_GLOBALS_MANAGER_COLUMN_CREATED_BY_TITLE"] = "由...製作";
$MESS["BIZPROC_WF_TYPE_GLOBALS_MANAGER_COLUMN_CREATED_DATE_TITLE"] = "創建於";
$MESS["BIZPROC_WF_TYPE_GLOBALS_MANAGER_COLUMN_DESCRIPTION_TITLE"] = "描述";
$MESS["BIZPROC_WF_TYPE_GLOBALS_MANAGER_COLUMN_ID_TITLE"] = "ID";
$MESS["BIZPROC_WF_TYPE_GLOBALS_MANAGER_COLUMN_IS_MULTIPLE_TITLE"] = "多種的";
$MESS["BIZPROC_WF_TYPE_GLOBALS_MANAGER_COLUMN_IS_REQUIRED_TITLE"] = "必需的";
$MESS["BIZPROC_WF_TYPE_GLOBALS_MANAGER_COLUMN_MODIFIED_BY_TITLE"] = "修改";
$MESS["BIZPROC_WF_TYPE_GLOBALS_MANAGER_COLUMN_MODIFIED_DATE_TITLE"] = "修改";
$MESS["BIZPROC_WF_TYPE_GLOBALS_MANAGER_COLUMN_NAME_TITLE"] = "姓名";
$MESS["BIZPROC_WF_TYPE_GLOBALS_MANAGER_COLUMN_PROPERTIES_TEXT_FIELD_LENGTH_ERROR"] = "字段\“＃field_title＃\”太長了。請使其短。";
$MESS["BIZPROC_WF_TYPE_GLOBALS_MANAGER_COLUMN_PROPERTY_OPTIONS_TITLE"] = "選項";
$MESS["BIZPROC_WF_TYPE_GLOBALS_MANAGER_COLUMN_PROPERTY_SETTINGS_TITLE"] = "設定";
$MESS["BIZPROC_WF_TYPE_GLOBALS_MANAGER_COLUMN_PROPERTY_TYPE_TITLE"] = "類型";
$MESS["BIZPROC_WF_TYPE_GLOBALS_MANAGER_COLUMN_PROPERTY_VALUE_TITLE"] = "價值";
$MESS["BIZPROC_WF_TYPE_GLOBALS_MANAGER_COLUMN_VISIBILITY_TITLE"] = "能見度";
