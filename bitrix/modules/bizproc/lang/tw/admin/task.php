<?php
$MESS["BPAT_ACTION_DELEGATE"] = "代表";
$MESS["BPAT_BACK"] = "後退";
$MESS["BPAT_DESCR"] = "任務描述";
$MESS["BPAT_GOTO_DOC"] = "轉到元素";
$MESS["BPAT_NAME"] = "任務名稱";
$MESS["BPAT_NO_STATE"] = "找不到業務流程。";
$MESS["BPAT_NO_TASK"] = "找不到任務。";
$MESS["BPAT_TAB_1"] = "任務";
$MESS["BPAT_TAB_TITLE_1"] = "任務";
$MESS["BPAT_TITLE_1"] = "分配＃ID＃";
$MESS["BPAT_USER"] = "用戶";
$MESS["BPAT_USER_NOT_FOUND"] = "（未找到）";
