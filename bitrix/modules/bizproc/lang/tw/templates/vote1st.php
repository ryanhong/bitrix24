<?php
$MESS["BP_V1ST_APPR"] = "得到正式認可的";
$MESS["BP_V1ST_APPR_S"] = "狀態：批准";
$MESS["BP_V1ST_DESC"] = "在單一受訪者的批准時建議進行。創建一個建議參加投票的人的列表。首次投票時，投票已完成。";
$MESS["BP_V1ST_MAIL2_NA"] = "對{= document：name}進行投票：該文檔已被拒絕。";
$MESS["BP_V1ST_MAIL2_NA_TEXT"] = "在\“ {= document：name} \”上投票已完成。

該文件已被拒絕。
{=批准性1：評論}";
$MESS["BP_V1ST_MAIL_NAME"] = "電子郵件";
$MESS["BP_V1ST_MAIL_SUBJ"] = "對{= document：name}進行投票：該文檔已批准。";
$MESS["BP_V1ST_MAIL_TEXT"] = "在\“ {= document：name} \”上投票已完成。

該文件已獲批准。
{=批准性1：評論}";
$MESS["BP_V1ST_NAME"] = "首次批准";
$MESS["BP_V1ST_PARAM1"] = "投票人";
$MESS["BP_V1ST_PARAM1_DESC"] = "參與批准過程的用戶。";
$MESS["BP_V1ST_S2_1"] = "活動序列";
$MESS["BP_V1ST_SEQ"] = "順序業務流程";
$MESS["BP_V1ST_STAT_NA"] = "拒絕";
$MESS["BP_V1ST_STAT_NA_T"] = "狀態：被拒絕";
$MESS["BP_V1ST_T3"] = "發布文檔";
$MESS["BP_V1ST_TASK_DESC"] = "您必須批准或拒絕文檔\“ {= document：name} \”。

作者：{=文檔：create_by_printable}";
$MESS["BP_V1ST_TASK_NAME"] = "批准文檔：\“ {= document：name} \”";
$MESS["BP_V1ST_TASK_T"] = "請批准或拒絕該文件。";
$MESS["BP_V1ST_TASK_TEXT"] = "您必須批准或拒絕文檔\“ {= document：name} \”。

通過打開鏈接：＃BASE_HREF ## TASK_URL＃

作者：{=文檔：create_by_printable}";
$MESS["BP_V1ST_TNA"] = "該文件已被拒絕。";
$MESS["BP_V1ST_VNAME"] = "響應文件";
