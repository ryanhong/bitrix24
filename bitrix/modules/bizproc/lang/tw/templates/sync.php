<?php
$MESS["BPT_SYNC_DESC"] = "啟用在受控站點上發布信息塊元素。文檔將首先發佈在控制器站點上。";
$MESS["BPT_SYNC_NAME"] = "在受控站點上發布";
$MESS["BPT_SYNC_PUBLISH"] = "發布文檔";
$MESS["BPT_SYNC_SEQ"] = "順序業務流程";
$MESS["BPT_SYNC_SYNC"] = "在受控站點上發布";
