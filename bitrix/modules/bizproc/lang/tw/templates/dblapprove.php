<?php
$MESS["BP_DBLA_APP"] = "得到正式認可的";
$MESS["BP_DBLA_APPROVE"] = "請批准或拒絕該文件。";
$MESS["BP_DBLA_APPROVE2"] = "請批准或拒絕該文件。";
$MESS["BP_DBLA_APPROVE2_TEXT"] = "您必須批准或拒絕文檔\“ {= document：name} \”。

作者：{=文檔：create_by_printable}";
$MESS["BP_DBLA_APPROVE2_TITLE"] = "文件批准：第2階段";
$MESS["BP_DBLA_APPROVE_TEXT"] = "您必須批准或拒絕文檔\“ {= document：name} \”。

作者：{=文檔：create_by_printable}";
$MESS["BP_DBLA_APPROVE_TITLR"] = "文件批准：第1階段";
$MESS["BP_DBLA_APP_S"] = "狀態：批准";
$MESS["BP_DBLA_DESC"] = "在批准之前，建議文件需要初步專家評估時建議。在第一個過程階段，專家證明了文件。如果專家拒絕該文檔，則將後者返回給發起者進行修訂。如果批准該文件，則將以簡單的多數派批准選定的員工最終批准。如果最終投票失敗，則將返回文檔進行修訂，並且批准程序從一開始就開始。";
$MESS["BP_DBLA_M"] = "電子郵件";
$MESS["BP_DBLA_MAIL2_SUBJ"] = "請響應\“ {= document：name} \”";
$MESS["BP_DBLA_MAIL2_TEXT"] = "您必須批准或拒絕文檔\“ {= document：name} \”。

通過打開鏈接：＃BASE_HREF ## TASK_URL＃

作者：{=文檔：create_by_printable}";
$MESS["BP_DBLA_MAIL3_SUBJ"] = "對\“ {= document：name}進行投票：該文檔已被接受。";
$MESS["BP_DBLA_MAIL3_TEXT"] = "在\“ {= document：name} \”上投票已完成。

該文檔被{= apploveactivity2：批准的percent}％接受。

批准：{=批准性2：批准量}
被拒絕：{= apploveactivity2：notappravycount}

{= Apploveactivity2：評論}";
$MESS["BP_DBLA_MAIL4_SUBJ"] = "對{= document：name}進行投票：該文檔已被拒絕。";
$MESS["BP_DBLA_MAIL4_TEXT"] = "批准\“ {= document：name} \”的第一階段已經完成。

該文件被拒絕。

{=批准性1：評論}";
$MESS["BP_DBLA_MAIL_SUBJ"] = "該文件已通過了第1階段";
$MESS["BP_DBLA_MAIL_TEXT"] = "文檔\“ {= document：name} \”已通過了批准的第一階段。

該文件已獲批准。

{=批准性1：評論}";
$MESS["BP_DBLA_NAME"] = "兩階段的批准";
$MESS["BP_DBLA_NAPP"] = "對\“ {= document：name}進行投票：該文檔已被拒絕。";
$MESS["BP_DBLA_NAPP_DRAFT"] = "發送進行修訂";
$MESS["BP_DBLA_NAPP_DRAFT_S"] = "狀態：發送進行修訂";
$MESS["BP_DBLA_NAPP_TEXT"] = "在\“ {= document：name} \”上投票已完成。

該文件被拒絕。

批准：{=批准性2：批准量}
被拒絕：{= apploveactivity2：notappravycount}

{= Apploveactivity2：評論}";
$MESS["BP_DBLA_PARAM1"] = "第1階段投票人員";
$MESS["BP_DBLA_PARAM2"] = "階段2投票人員";
$MESS["BP_DBLA_PUB_TITLE"] = "發布文檔";
$MESS["BP_DBLA_S_1"] = "活動序列";
$MESS["BP_DBLA_T"] = "順序業務流程";
$MESS["BP_DBLA_TASK"] = "批准文檔：\“ {= document：name} \”";
$MESS["BP_DBLA_TASK_DESC"] = "您必須批准或拒絕文檔\“ {= document：name} \”。

通過打開鏈接：＃BASE_HREF ## TASK_URL＃

作者：{=文檔：create_by_printable}";
