<?php
$MESS["BPT_ST_APPROVERS"] = "批准文件的人";
$MESS["BPT_ST_APPROVE_DESC"] = "您必須批准文檔\“ {= document：name} \”：

文檔內容：
{= document：preview_text}

作者：
{=文檔：create_by_printable}";
$MESS["BPT_ST_APPROVE_NAME"] = "批准文檔\“ {= document：name} \”";
$MESS["BPT_ST_APPROVE_TITLE"] = "文件批准";
$MESS["BPT_ST_AUTHOR"] = "作者;";
$MESS["BPT_ST_BP_NAME_1"] = "狀態驅動的業務流程";
$MESS["BPT_ST_CMD_APP"] = "贊同";
$MESS["BPT_ST_CMD_APPR"] = "發送批准";
$MESS["BPT_ST_CMD_PUBLISH"] = "發布";
$MESS["BPT_ST_CREATORS"] = "編輯文件的人";
$MESS["BPT_ST_DESC_1"] = "由多個人批准以狀態驅動的元素。";
$MESS["BPT_ST_F"] = "事件外殼";
$MESS["BPT_ST_INIT_1"] = "初始化狀態";
$MESS["BPT_ST_INS"] = "狀態初始化";
$MESS["BPT_ST_MAIL_TITLE"] = "電子郵件";
$MESS["BPT_ST_NAME_1"] = "批准具有狀態的元素";
$MESS["BPT_ST_PUBDC"] = "文件發布";
$MESS["BPT_ST_SAVEH"] = "保存歷史記錄";
$MESS["BPT_ST_SEQ"] = "活性序列";
$MESS["BPT_ST_SETSTATE_1"] = "設置狀態";
$MESS["BPT_ST_SET_DRAFT"] = "設置草稿狀態";
$MESS["BPT_ST_SET_PUB"] = "設置發布狀態";
$MESS["BPT_ST_ST_DRAFT"] = "草稿";
$MESS["BPT_ST_SUBJECT"] = "批准文檔：\“ {= document：name} \”";
$MESS["BPT_ST_TEXT"] = "您必須批准或拒絕文檔\“ {= document：name} \”。

通過打開鏈接：＃BASE_HREF ## TASK_URL＃

文檔內容：
{= document：lidet_text}

作者：{=文檔：create_by_printable}";
$MESS["BPT_ST_TP"] = "出版";
