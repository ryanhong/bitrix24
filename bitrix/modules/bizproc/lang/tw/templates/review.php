<?php
$MESS["BP_REVW_DESC"] = "此過程是確保每個應該熟悉某些信息的人的理想選擇。用戶確認已閱讀它時可以提供評論。";
$MESS["BP_REVW_MAIL"] = "電子郵件";
$MESS["BP_REVW_MAIL_SUBJ"] = "已讀取文檔\“ {= document：name} \”。";
$MESS["BP_REVW_MAIL_TEXT"] = "已讀取文檔\“ {= document：name} \”。";
$MESS["BP_REVW_PARAM1"] = "讀者列表";
$MESS["BP_REVW_REVIEW"] = "閱讀文檔{= document：name}";
$MESS["BP_REVW_REVIEW_DESC"] = "您應該讀取文檔：\“ {= document：name} \”。

作者：{=文檔：create_by_printable}";
$MESS["BP_REVW_T"] = "順序業務流程";
$MESS["BP_REVW_TASK"] = "請閱讀文檔：\“ {= document：name} \”";
$MESS["BP_REVW_TASK_DESC"] = "您應該讀取文檔：\“ {= document：name} \”。

請打開鏈接：＃BASE_HREF ## task_url＃

作者：{=文檔：create_by_printable}";
$MESS["BP_REVW_TITLE"] = "閱讀文檔";
