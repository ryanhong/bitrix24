<?php
$MESS["BPT_SM_ACT_NAME_1"] = "活動序列";
$MESS["BPT_SM_ACT_TITLE"] = "電子郵件";
$MESS["BPT_SM_APPROVE_DESC"] = "您必須批准或拒絕文檔\“ {= document：name} \”。

作者：{=文檔：create_by_printable}";
$MESS["BPT_SM_APPROVE_NAME"] = "請批准或拒絕該文件。";
$MESS["BPT_SM_APPROVE_TITLE"] = "響應文件";
$MESS["BPT_SM_DESC"] = "建議通過簡單的大多數選票做出決定。您可以分配投票人員並允許他們發表評論。投票完成後，所有相關人員都會收到有關結果的通知。";
$MESS["BPT_SM_MAIL1_SUBJ"] = "對\“ {= document：name}進行投票：文檔已通過。";
$MESS["BPT_SM_MAIL1_TEXT"] = "在\“ {= document：name} \”上投票已完成。

該文檔被{= apploveactivity1：批准的percecent}％接受。

批准：{=批准性1：批准量}
被拒絕：{= apploveactivity1：notappravycount}";
$MESS["BPT_SM_MAIL1_TITLE"] = "該文件已批准";
$MESS["BPT_SM_MAIL2_STATUS"] = "拒絕";
$MESS["BPT_SM_MAIL2_STATUS2"] = "狀態：被拒絕";
$MESS["BPT_SM_MAIL2_SUBJ"] = "對\“ {= document：name}進行投票：該文檔已被拒絕。";
$MESS["BPT_SM_MAIL2_TEXT"] = "在\“ {= document：name} \”上投票已完成。

該文件被拒絕。

批准：{=批准性1：批准量}
被拒絕：{= apploveactivity1：notappravycount}";
$MESS["BPT_SM_MAIL2_TITLE"] = "該文件已被拒絕。";
$MESS["BPT_SM_NAME"] = "簡單批准/投票";
$MESS["BPT_SM_PARAM_DESC"] = "參與投票的用戶。";
$MESS["BPT_SM_PARAM_NAME"] = "投票人";
$MESS["BPT_SM_PUB"] = "發布文檔";
$MESS["BPT_SM_STATUS"] = "得到正式認可的";
$MESS["BPT_SM_STATUS2"] = "狀態：批准";
$MESS["BPT_SM_TASK1_TEXT"] = "您必須批准或拒絕文檔\“ {= document：name} \”。

通過打開鏈接：＃BASE_HREF ## TASK_URL＃

作者：{=文檔：create_by_printable}";
$MESS["BPT_SM_TASK1_TITLE"] = "批准文檔：\“ {= document：name} \”";
$MESS["BPT_SM_TITLE1"] = "順序業務流程";
