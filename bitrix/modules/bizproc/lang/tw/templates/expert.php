<?php
$MESS["BP_EXPR_APP2_DESC"] = "所有指定的人都檢查了文件並表達了他們的意見。

{=批准性1：評論}

您現在需要批准或拒絕該文件。";
$MESS["BP_EXPR_APP2_TEXT"] = "批准文檔：\“ {= document：name} \”";
$MESS["BP_EXPR_APPR1"] = "文檔\“ {= document：name} \”需要您的評論。";
$MESS["BP_EXPR_APPR1_DESC"] = "您的意見必須在文檔上做出決定\“ {= document：name} \”。";
$MESS["BP_EXPR_DESC"] = "建議在批准或拒絕文檔需要專家評論的人時，建議使用。這個過程創建了一群專家，每個專家都表達了他們對文檔的看法。然後，意見轉移給做出最終決定的人。";
$MESS["BP_EXPR_M"] = "電子郵件";
$MESS["BP_EXPR_MAIL2_SUBJ"] = "批准文檔：\“ {= document：name} \”";
$MESS["BP_EXPR_MAIL2_TEXT"] = "所有指定的人都檢查了文件並表達了他們的意見。
您現在需要批准或拒絕該文件。

請打開鏈接：＃BASE_HREF ## task_url＃

{=批准性1：評論}";
$MESS["BP_EXPR_MAIL3_SUBJ"] = "\“ {= document：name} \”的批准：文檔已通過。";
$MESS["BP_EXPR_MAIL3_TEXT"] = "\“ {= document：name} \”的辯論已完成；該文件已獲批准。

{= Apploveactivity2：評論}";
$MESS["BP_EXPR_MAIL4_SUBJ"] = "關於\“ {= document：name} \”的討論：該文檔已被拒絕。";
$MESS["BP_EXPR_MAIL4_TEXT"] = "關於\“ {= document：name} \”的討論已完成；該文件已被拒絕。

{= Apploveactivity2：評論}";
$MESS["BP_EXPR_NA"] = "拒絕";
$MESS["BP_EXPR_NAME"] = "專家意見";
$MESS["BP_EXPR_NA_ST"] = "狀態：被拒絕";
$MESS["BP_EXPR_PARAM1"] = "批准的人";
$MESS["BP_EXPR_PARAM2"] = "專家";
$MESS["BP_EXPR_PARAM2_DESC"] = "一個專家小組，其成員可以對文件表達意見。";
$MESS["BP_EXPR_PUB"] = "發布文檔";
$MESS["BP_EXPR_S"] = "順序業務流程";
$MESS["BP_EXPR_ST3_T"] = "得到正式認可的";
$MESS["BP_EXPR_ST3_TIT"] = "狀態：批准";
$MESS["BP_EXPR_ST_1"] = "活動序列";
$MESS["BP_EXPR_TAPP"] = "批准文件";
$MESS["BP_EXPR_TASK1"] = "文檔\“ {= document：name} \”需要您的評論。";
$MESS["BP_EXPR_TASK1_MAIL"] = "您的意見必須在文檔上做出決定\“ {= document：name} \”。

請打開鏈接：＃BASE_HREF ## task_url＃";
