<?php
$MESS["BIZPROC_BIZPROCDESIGNER_INSTALLED"] = "已安裝業務流程設計器模塊。在繼續之前，請卸載它。";
$MESS["BIZPROC_INSTALL_DESCRIPTION"] = "創建管理和運行業務流程的模塊。";
$MESS["BIZPROC_INSTALL_NAME"] = "業務流程";
$MESS["BIZPROC_INSTALL_TITLE"] = "模塊安裝";
$MESS["BIZPROC_PERM_D"] = "拒絕訪問";
$MESS["BIZPROC_PERM_R"] = "讀";
$MESS["BIZPROC_PERM_W"] = "寫";
