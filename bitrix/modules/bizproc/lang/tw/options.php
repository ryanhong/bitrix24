<?php
$MESS["BIZPROC_EMPLOYEE_COMPATIBLE_MODE"] = "啟用\“綁定到用戶\”類型的兼容模式";
$MESS["BIZPROC_LIMIT_SIMULTANEOUS_PROCESSES"] = "元素的最大同時過程";
$MESS["BIZPROC_LOG_CLEANUP_DAYS"] = "保留業務流程日誌的天數";
$MESS["BIZPROC_LOG_SKIP_TYPES"] = "不要記錄事件";
$MESS["BIZPROC_LOG_SKIP_TYPES_1_1"] = "活動開始";
$MESS["BIZPROC_LOG_SKIP_TYPES_2_1"] = "活動結束";
$MESS["BIZPROC_NAME_TEMPLATE"] = "名稱顯示格式";
$MESS["BIZPROC_OPTIONS_NAME_IN_SITE_FORMAT"] = "網站格式";
$MESS["BIZPROC_OPT_LOCKED_WI_PATH"] = "懸掛工作流程列表路徑";
$MESS["BIZPROC_OPT_TIME_LIMIT"] = "最小響應超時";
$MESS["BIZPROC_OPT_TIME_LIMIT_D"] = "天";
$MESS["BIZPROC_OPT_TIME_LIMIT_H"] = "小時";
$MESS["BIZPROC_OPT_TIME_LIMIT_M"] = "分鐘";
$MESS["BIZPROC_OPT_TIME_LIMIT_S"] = "秒";
$MESS["BIZPROC_OPT_USE_GZIP_COMPRESSION"] = "壓縮存檔數據";
$MESS["BIZPROC_OPT_USE_GZIP_COMPRESSION_EMPTY"] = "預設";
$MESS["BIZPROC_OPT_USE_GZIP_COMPRESSION_N"] = "不";
$MESS["BIZPROC_OPT_USE_GZIP_COMPRESSION_Y"] = "是的";
$MESS["BIZPROC_TAB_SET"] = "設定";
$MESS["BIZPROC_TAB_SET_ALT"] = "模塊設置";
