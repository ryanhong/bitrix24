<?php
$MESS["BPT1_BTF_P_APP"] = "贊同";
$MESS["BPT1_BTF_P_APPS"] = "[x]未經批准
[y]批准
[n]被拒絕";
$MESS["BPT1_BT_AA11_DESCR"] = "員工：{= template：targetuser_printable}
休假開始：{= template：date_start}
離開結束：{= template：date_end}";
$MESS["BPT1_BT_AA11_NAME"] = "批准{= template：targetuser_printable}從{= template：date_start}通過{= template：date_end}離開離開";
$MESS["BPT1_BT_AA11_STATUS_MESSAGE"] = "被高級管理層批准";
$MESS["BPT1_BT_AA11_TITLE"] = "批准休假";
$MESS["BPT1_BT_CYCLE"] = "批准週期";
$MESS["BPT1_BT_IEBA1_V1"] = "需要進一步批准";
$MESS["BPT1_BT_IEBA2_V2"] = "不需要進一步批准";
$MESS["BPT1_BT_IEBA15_V1"] = "休假已獲得批准。";
$MESS["BPT1_BT_IEBA15_V2"] = "休假尚未獲得批准。";
$MESS["BPT1_BT_IF11_N"] = "狀態";
$MESS["BPT1_BT_PARAM_BOOK"] = "會計部門";
$MESS["BPT1_BT_PARAM_BOSS"] = "被允許批准休假的人";
$MESS["BPT1_BT_PARAM_OP_ADMIN"] = "員工允許管理流程";
$MESS["BPT1_BT_PARAM_OP_CREATE"] = "員工允許創建新的流程";
$MESS["BPT1_BT_PARAM_OP_READ"] = "員工允許查看所有流程";
$MESS["BPT1_BT_P_TARGET"] = "員工";
$MESS["BPT1_BT_RA17_DESCR"] = "假期已獲得高級管理層的批准。必須註冊相應的訂單；假日薪水必須支付。

員工：{= template：targetuser_printable}
休假開始：{= template：date_start}
離開結束：{= template：date_end}";
$MESS["BPT1_BT_RA17_NAME"] = "註冊{= template：targetuser_printable}的離開，{= template：date_start}  -  {= template：date_end}";
$MESS["BPT1_BT_RA17_STATUS_MESSAGE"] = "會計部門處理";
$MESS["BPT1_BT_RA17_TBM"] = "休假已獲得批准和註冊。";
$MESS["BPT1_BT_RA17_TITLE"] = "會計部門處理";
$MESS["BPT1_BT_RIA11_DESCR"] = "請假已被批准。它需要進一步批准嗎？

員工：{= template：targetuser_printable}
休假開始：{= template：date_start}
離開結束：{= template：date_end}";
$MESS["BPT1_BT_RIA11_NAME"] = "{= template：targetuser_printable}從{= template：date_start}通過{= template：date_end}的離開需要進一步批准嗎？";
$MESS["BPT1_BT_RIA11_P1"] = "需要進一步批准";
$MESS["BPT1_BT_RIA11_P2"] = "被批准";
$MESS["BPT1_BT_RIA11_TITLE"] = "額外批准";
$MESS["BPT1_BT_SA1_TITLE_1"] = "活動序列";
$MESS["BPT1_BT_SFA1_TITLE"] = "保存離開參數";
$MESS["BPT1_BT_SFA12_TITLE"] = "編輯文檔";
$MESS["BPT1_BT_SFTA12_ST"] = "假期已獲得高級管理層的批准。";
$MESS["BPT1_BT_SFTA12_T"] = "設置狀態文本";
$MESS["BPT1_BT_SNMA16_TEXT"] = "您的休假已獲得高級管理層的批准。";
$MESS["BPT1_BT_SNMA16_TITLE"] = "社交網絡消息";
$MESS["BPT1_BT_SNMA18_TEXT"] = "您的休假尚未獲得批准。 {= A94751_67978_49922_99999：註釋}";
$MESS["BPT1_BT_SNMA18_TITLE"] = "社交網絡消息";
$MESS["BPT1_BT_SSTA14_ST"] = "拒絕";
$MESS["BPT1_BT_SSTA14_T"] = "設置狀態文本";
$MESS["BPT1_BT_SSTA18_ST"] = "請假已註冊。";
$MESS["BPT1_BT_SSTA18_T"] = "設置狀態文本";
$MESS["BPT1_BT_STA1_STATE_TITLE"] = "贊同";
$MESS["BPT1_BT_STA1_TITLE"] = "設置狀態文本";
$MESS["BPT1_BT_SWA"] = "順序業務流程";
$MESS["BPT1_BT_T_DATE_END"] = "離開結束";
$MESS["BPT1_BT_T_DATE_START"] = "休假開始";
$MESS["BPT1_TTITLE"] = "離開";
$MESS["BPT_BT_AA7_DESCR"] = "年假";
$MESS["BPT_BT_AA7_FSTATE"] = "作品";
$MESS["BPT_BT_AA7_NAME"] = "離開";
$MESS["BPT_BT_AA7_STATE"] = "離開";
$MESS["BPT_BT_AA7_TITLE"] = "缺席圖表";
