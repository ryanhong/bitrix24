<?php
$MESS["BPT_BT_AA1_DESCR"] = "商務旅行需要批准{=模板：targetuser_printable}

宣教國家：{=模板：國家 /地區}
宣教城市：{=模板：城市}
日期：{= template：date_start}  -  {= template：date_end}
計劃支出：{=模板：支出}

目的：
{=模板：目的}";
$MESS["BPT_BT_AA1_NAME"] = "批准商務旅行{= template：targetuser_printable}，{= template：country}  -  {= template：city}";
$MESS["BPT_BT_AA1_STATUS_MESSAGE"] = "正在等待批准";
$MESS["BPT_BT_AA1_TITLE"] = "商務旅行批准";
$MESS["BPT_BT_AA7_DESCR"] = "目的：{= template：perim}";
$MESS["BPT_BT_AA7_FSTATE"] = "作品";
$MESS["BPT_BT_AA7_NAME"] = "商務旅行";
$MESS["BPT_BT_AA7_STATE"] = "商務旅行";
$MESS["BPT_BT_AA7_TITLE"] = "缺席圖表";
$MESS["BPT_BT_DF_CITY"] = "目的地城市";
$MESS["BPT_BT_DF_COUNTRY"] = "目的地國家";
$MESS["BPT_BT_DF_DATE_END_REAL"] = "實際完成";
$MESS["BPT_BT_DF_EXP_REAL"] = "花費";
$MESS["BPT_BT_DF_TICKETS"] = "附加的文件";
$MESS["BPT_BT_PARAM_BOOK"] = "會計部門";
$MESS["BPT_BT_PARAM_BOSS"] = "員工批准商務旅行";
$MESS["BPT_BT_PARAM_FORM1"] = "旅行費用摘要表";
$MESS["BPT_BT_PARAM_FORM2"] = "商務旅行分配表格";
$MESS["BPT_BT_PARAM_OP_ADMIN"] = "員工允許管理流程";
$MESS["BPT_BT_PARAM_OP_CREATE"] = "員工允許創建新的流程";
$MESS["BPT_BT_PARAM_OP_READ"] = "員工允許查看所有流程";
$MESS["BPT_BT_P_TARGET"] = "員工";
$MESS["BPT_BT_RA1_DESCR"] = "這次商務旅行已獲得批准，必須通過會計註冊。

員工：{= template：targetuser_printable}

目的地國家：{=模板：country}
目的地城市：{=模板：城市}
日期：{= template：date_start}  -  {= template：date_end}
計劃支出：{=模板：支出}

目的：
{=模板：目的}

附加的文件：
{=模板：tickets_printable}";
$MESS["BPT_BT_RA1_NAME"] = "註冊{=模板：targetuser_printable}的商務旅行，{= template：country}  -  {= template：city}";
$MESS["BPT_BT_RA1_STATUS_MESSAGE"] = "通過會計註冊";
$MESS["BPT_BT_RA1_TBM"] = "商務旅行註冊";
$MESS["BPT_BT_RA1_TITLE"] = "通過會計註冊";
$MESS["BPT_BT_RA2_DESCR"] = "在您離開商務旅行之前：

1. [url = {=變量：parameterform1}]下載[/url]旅行費用表格。

該表格應在旅行結束時填寫並授予會計部門。

2.請記住，您的商務旅行期間發生的任何付款都必須通過相應的賬單或收據確認。";
$MESS["BPT_BT_RA2_NAME"] = "商務旅行文檔";
$MESS["BPT_BT_RA2_STATUS_MESSAGE"] = "指示";
$MESS["BPT_BT_RA2_TBM"] = "熟悉";
$MESS["BPT_BT_RA2_TITLE"] = "熟人/批准";
$MESS["BPT_BT_RA2_TITLE1"] = "商業旅程文檔";
$MESS["BPT_BT_RA3_DESCR"] = "{= temaplate：targetuser_printable}的旅行報告

計劃日期：{= template：date_start}  -  {= template：date_end}
實際旅行日：{= varible：date_end_real}

花費：
{=變量：ebependitures_real}

目的：
{=模板：目的}

旅行後報告：
{=變量：報告}";
$MESS["BPT_BT_RA3_NAME"] = "閱讀摘要報告：{=模板：targetuser_printable}";
$MESS["BPT_BT_RA3_STATUS_MESSAGE"] = "管理報告";
$MESS["BPT_BT_RA3_TBM"] = "熟悉";
$MESS["BPT_BT_RA4_DESCR"] = "以下商務旅行的旅行後報告已批准，必須通過會計註冊。

{= temaplate：targetuser_printable}的旅行報告

實際旅行日：{= template：date_start}  -  {= varible：date_end_real}

總費用：
{=變量：ebependitures_real}

總結報告：
{=變量：報告}";
$MESS["BPT_BT_RA4_NAME"] = "摘要報告{= template：targetuser_printable}";
$MESS["BPT_BT_RA4_STATUS_MESSAGE"] = "會計部門註冊";
$MESS["BPT_BT_RA4_TITLE"] = "會計部門註冊";
$MESS["BPT_BT_RA4_TMB"] = "掛號的";
$MESS["BPT_BT_RIA1_DATE_END_REAL"] = "結束日期";
$MESS["BPT_BT_RIA1_DESCR"] = "旅行後報告。提供原始收據的費用報告。在下面提供的空間中編寫摘要，並在您的完整報告中包含一個鏈接（如果適用）。";
$MESS["BPT_BT_RIA1_EXP_REAL"] = "花費";
$MESS["BPT_BT_RIA1_NAME"] = "摘要報告（將在出差旅行時完成）";
$MESS["BPT_BT_RIA1_REPORT"] = "總結報告";
$MESS["BPT_BT_RIA1_TITLE"] = "總結報告";
$MESS["BPT_BT_SA1_TITLE_1"] = "活動序列";
$MESS["BPT_BT_SA3_TITLE_1"] = "活動序列";
$MESS["BPT_BT_SFA1_NAME"] = "商務旅行{= template：targetuser_printable}，{= template：country}  -  {= template：city}";
$MESS["BPT_BT_SFA1_TITLE"] = "保存旅行參數";
$MESS["BPT_BT_SFA2_TITLE"] = "保存報告";
$MESS["BPT_BT_SNMA1_TEXT"] = "您的商務旅行已獲批准。";
$MESS["BPT_BT_SNMA1_TITLE"] = "社交網絡消息";
$MESS["BPT_BT_SNMA2_TEXT"] = "管理層尚未確認您的商務旅行。";
$MESS["BPT_BT_SNMA2_TITLE"] = "社交網絡消息";
$MESS["BPT_BT_SSTA2_STATE_TITLE"] = "註冊商務旅行";
$MESS["BPT_BT_SSTA2_TITLE"] = "設置狀態：註冊";
$MESS["BPT_BT_SSTA3_STATE_TITLE"] = "總結報告";
$MESS["BPT_BT_SSTA3_TITLE"] = "設置狀態文本";
$MESS["BPT_BT_SSTA4_STATE_TITLE"] = "完全的";
$MESS["BPT_BT_SSTA4_TITLE"] = "設置狀態文本";
$MESS["BPT_BT_SSTA5_STATE_TITLE"] = "被管理層拒絕";
$MESS["BPT_BT_SSTA5_TITLE"] = "設置狀態：被拒絕";
$MESS["BPT_BT_STA1_STATE_TITLE"] = "專案";
$MESS["BPT_BT_STA1_TITLE"] = "設置狀態文本";
$MESS["BPT_BT_SWA"] = "順序業務流程";
$MESS["BPT_BT_T_CITY"] = "目的地城市";
$MESS["BPT_BT_T_COUNTRY"] = "目的地國家";
$MESS["BPT_BT_T_COUNTRY_DEF"] = "美國";
$MESS["BPT_BT_T_DATE_END"] = "計劃結束日期";
$MESS["BPT_BT_T_DATE_START"] = "開始日期";
$MESS["BPT_BT_T_EXP"] = "計劃的費用";
$MESS["BPT_BT_T_PURPOSE"] = "目的";
$MESS["BPT_BT_T_TICKETS"] = "附加的文件";
$MESS["BPT_TTITLE"] = "商務旅行";
