<?php
$MESS["BIZPROC_JS_WOW_MOMENT_CRM_CHECK_AUTOMATION_TEXT"] = "轉到“自動化”選項卡以概述您的自動化規則和触發器";
$MESS["BIZPROC_JS_WOW_MOMENT_CRM_CHECK_AUTOMATION_TITLE"] = "自動化概述";
$MESS["BIZPROC_JS_WOW_MOMENT_CRM_HOW_CHECK_ROBOT_TEXT"] = "要檢查行動中的自動化規則，請使用配置的自動化規則將任何項目移至舞台上。然後，打開此項目以查看結果";
$MESS["BIZPROC_JS_WOW_MOMENT_CRM_HOW_CHECK_ROBOT_TITLE"] = "如何檢查自動化規則配置？";
$MESS["BIZPROC_JS_WOW_MOMENT_CRM_HOW_CHECK_TRIGGER_TEXT"] = "要檢查您的觸發器，請在任何項目中執行與觸發名稱相匹配的測試活動。例如，用數據填寫表單。然後，檢查項目是否已更改並打開項目以查看結果";
$MESS["BIZPROC_JS_WOW_MOMENT_CRM_HOW_CHECK_TRIGGER_TITLE"] = "如何檢查觸發配置？";
$MESS["BIZPROC_JS_WOW_MOMENT_CRM_SUCCESS_AUTOMATION_TEXT"] = "請注意，成功處理的自動化規則和触發器由綠色背景突出顯示。
<br/>在下面的鏈接文章中閱讀有關其他狀態的更多信息";
$MESS["BIZPROC_JS_WOW_MOMENT_CRM_SUCCESS_AUTOMATION_TITLE"] = "自動化規則和触發器的狀態";
