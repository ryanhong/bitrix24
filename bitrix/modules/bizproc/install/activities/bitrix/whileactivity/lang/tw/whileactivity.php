<?php
$MESS["BPWA_CONDITION_NOT_SET"] = "未指定的循環條件";
$MESS["BPWA_CYCLE_LIMIT"] = "循環迭代最大超過";
$MESS["BPWA_INVALID_CONDITION_TYPE"] = "找不到條件類型。";
$MESS["BPWA_NO_CONDITION"] = "條件丟失了。";
