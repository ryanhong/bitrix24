<?php
$MESS["BPFC_PD_ADD"] = "添加條件";
$MESS["BPFC_PD_AND"] = "和";
$MESS["BPFC_PD_CALENDAR"] = "日曆";
$MESS["BPFC_PD_CONDITION"] = "狀態";
$MESS["BPFC_PD_DELETE"] = "刪除";
$MESS["BPFC_PD_FIELD"] = "屬性或字段";
$MESS["BPFC_PD_NO"] = "不";
$MESS["BPFC_PD_OR"] = "或者";
$MESS["BPFC_PD_PARAMS"] = "參數";
$MESS["BPFC_PD_VARS"] = "變量";
$MESS["BPFC_PD_YES"] = "是的";
