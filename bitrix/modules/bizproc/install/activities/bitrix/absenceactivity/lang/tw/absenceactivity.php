<?php
$MESS["BPAA2_NO_PERMS"] = "您沒有寫入缺席圖表的書面許可。";
$MESS["BPSNMA_EMPTY_ABSENCEFROM"] = "缺少“開始日期”屬性。";
$MESS["BPSNMA_EMPTY_ABSENCENAME"] = "缺少“事件名稱”屬性。";
$MESS["BPSNMA_EMPTY_ABSENCETO"] = "缺少“結束日期”屬性。";
$MESS["BPSNMA_EMPTY_ABSENCEUSER"] = "缺少“用戶”屬性。";
