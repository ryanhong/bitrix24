<?php
$MESS["BPSWFA_PD_ACCESS_DENIED_1"] = "只有帳戶管理員才能訪問活動屬性。";
$MESS["BPSWFA_PD_DOCUMENT_ID"] = "元素ID";
$MESS["BPSWFA_PD_DOCUMENT_TYPE"] = "元素類型";
$MESS["BPSWFA_PD_ENTITY"] = "實體";
$MESS["BPSWFA_PD_TEMPLATE"] = "模板";
$MESS["BPSWFA_PD_USE_SUBSCRIPTION"] = "等待工作流程完成";
