<?php
$MESS["BPGUIA_ERROR_1"] = "目標員工未指定。";
$MESS["BPGUIA_ERROR_2"] = "目標員工不正確（權限不足）";
$MESS["BPGUIA_ERROR_USER_NOT_FOUND"] = "員工ID =＃ID＃沒有找到";
$MESS["BPGUIA_IS_ABSENT"] = "辭職（根據缺席時間表）";
$MESS["BPGUIA_TARGET_USER_NAME"] = "員工";
$MESS["BPGUIA_TIMEMAN_STATUS"] = "工作日狀況";
