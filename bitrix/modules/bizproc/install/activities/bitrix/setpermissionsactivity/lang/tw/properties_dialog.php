<?php
$MESS["BPSA_PD_PERM"] = "\“＃op＃\”權限已授予";
$MESS["BPSA_PD_PERM_CLEAR"] = "清除";
$MESS["BPSA_PD_PERM_CURRENT_LABEL"] = "當前的元素前提";
$MESS["BPSA_PD_PERM_HOLD"] = "維持";
$MESS["BPSA_PD_PERM_REWRITE"] = "覆蓋現有的權限";
$MESS["BPSA_PD_PERM_SCOPE_DOCUMENT"] = "所有元素權限";
$MESS["BPSA_PD_PERM_SCOPE_LABEL"] = "清理和覆蓋的範圍";
$MESS["BPSA_PD_PERM_SCOPE_WORFLOW"] = "當前業務流程設置的許可";
