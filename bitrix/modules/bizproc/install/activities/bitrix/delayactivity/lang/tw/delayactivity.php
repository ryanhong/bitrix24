<?php
$MESS["BPDA_DEBUG_EVENT"] = "由於測試模式事件而暫停跳過";
$MESS["BPDA_EMPTY_PROP"] = "未指定“週期”值。";
$MESS["BPDA_SUBSCRIBE_ERROR_MSGVER_1"] = "系統錯誤暫停工作流程。";
$MESS["BPDA_TRACK1"] = "推遲到＃期間＃";
$MESS["BPDA_TRACK2"] = "未指定延期期。";
$MESS["BPDA_TRACK3"] = "暫停跳過。配置的暫停日期是在當前日期之前。";
$MESS["BPDA_TRACK4"] = "推遲＃ersig1＃，直到＃ersig2＃";
