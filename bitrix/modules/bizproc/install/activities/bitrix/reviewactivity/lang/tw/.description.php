<?php
$MESS["BPAA_DESCR_CM"] = "評論";
$MESS["BPAR_DESCR_DESCR"] = "閱讀元素並發表評論";
$MESS["BPAR_DESCR_LR"] = "上一次閱讀";
$MESS["BPAR_DESCR_LR_COMMENT"] = "最後讀者的評論";
$MESS["BPAR_DESCR_NAME"] = "讀取元素";
$MESS["BPAR_DESCR_RC"] = "人讀";
$MESS["BPAR_DESCR_TA1"] = "自動檢查";
$MESS["BPAR_DESCR_TASKS"] = "任務";
$MESS["BPAR_DESCR_TC"] = "閱讀的人";
