<?php
$MESS["BPABS_EMPTY_DOC_ID"] = "沒有指定要創建業務流程的元素ID。";
$MESS["BPABS_EMPTY_DOC_TYPE"] = "需要元素類型。";
$MESS["BPABS_EMPTY_ENTITY"] = "沒有指定要創建業務流程的實體。";
$MESS["BPADH_NO_PERMS"] = "拒絕訪問。";
$MESS["BPADH_TITLE"] = "業務流程";
$MESS["BPATT_NO_MODULE_ID"] = "需要模塊ID。";
