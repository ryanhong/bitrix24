<?php
$MESS["BPWC_WTC_EMPTY_IBLOCK"] = "未指定信息塊ID。";
$MESS["BPWC_WTC_EMPTY_IBLOCK_TYPE"] = "未指定信息塊類型。";
$MESS["BPWC_WTC_ERROR"] = "錯誤";
$MESS["BPWC_WTC_PAGE_NAV_CHAIN"] = "任務名稱＃'";
$MESS["BPWC_WTC_PAGE_TITLE"] = "任務名稱＃'";
$MESS["BPWC_WTC_PERMS_ERROR"] = "會議已經過期。請打開<a href='#url#'>摘要頁面</a>，然後重試。";
$MESS["BPWC_WTC_WRONG_IBLOCK"] = "找不到組件設置中指定的信息塊。";
$MESS["BPWC_WTC_WRONG_IBLOCK_TYPE"] = "找不到組件參數中指定的信息塊類型。";
$MESS["BPWC_WTC_WRONG_TASK"] = "找不到任務。";
