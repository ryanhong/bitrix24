<?php
$MESS["BPATL_COMMENTS"] = "評論";
$MESS["BPATL_DOC_HISTORY"] = "業務流程日誌";
$MESS["BPATL_SUCCESS"] = "作業完成。";
$MESS["BPATL_TASK_TITLE_1"] = "分配描述";
$MESS["BPATL_USER_STATUS_NO"] = "你拒絕了元素";
$MESS["BPATL_USER_STATUS_OK"] = "你讀了元素";
$MESS["BPATL_USER_STATUS_YES"] = "您批准了元素";
$MESS["BPAT_COMMENT"] = "評論";
$MESS["BPAT_GOTO_DOC"] = "轉到元素";
