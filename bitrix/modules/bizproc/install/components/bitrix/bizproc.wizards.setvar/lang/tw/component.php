<?php
$MESS["BPWC_WVC_EMPTY_IBLOCK"] = "未指定信息塊ID。";
$MESS["BPWC_WVC_EMPTY_IBLOCK_TYPE"] = "未指定信息塊類型。";
$MESS["BPWC_WVC_ERROR"] = "錯誤";
$MESS["BPWC_WVC_PAGE_NAV_CHAIN"] = "業務流程變量";
$MESS["BPWC_WVC_PAGE_TITLE"] = "＃名稱＃：業務流程變量";
$MESS["BPWC_WVC_WRONG_IBLOCK"] = "找不到信息塊。";
$MESS["BPWC_WVC_WRONG_IBLOCK_TYPE"] = "找不到組件參數中指定的信息塊類型。";
$MESS["BPWC_WVC_WRONG_TMPL"] = "丟失了業務流程模板。";
