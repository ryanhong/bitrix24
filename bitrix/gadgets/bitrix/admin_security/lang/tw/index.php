<?php
$MESS["GD_SECURITY_EVENT_COUNT"] = "入侵嘗試<br>阻止了";
$MESS["GD_SECURITY_EVENT_COUNT_EMPTY"] = "未檢測到入侵嘗試";
$MESS["GD_SECURITY_FILTER_OFF_DESC"] = "主動過濾器被禁用。";
$MESS["GD_SECURITY_FILTER_ON_DESC"] = "有關更多詳細信息，請＃StartLink＃查看設置＃EndLink＃";
$MESS["GD_SECURITY_LEVEL"] = "您可以在<a href= \"security_panel.php?lang=#language_id# \ \">安全面板</a>上檢查模塊的當前安全級別。";
$MESS["GD_SECURITY_MODULE"] = "沒有安裝主動保護模塊。";
$MESS["GD_SECURITY_MODULE_INSTALL"] = "安裝";
$MESS["GD_SECURITY_SCANNER_RUN"] = "跑步";
$MESS["GD_SECURITY_SCANNER_TITLE"] = "安全<br>掃描儀";
$MESS["GD_SECURITY_SCANNER_VIEW"] = "細節";
$MESS["GD_SECURITY_UPDATE_NEEDED"] = "安全模塊必須更新為版本＃min_version＃";
