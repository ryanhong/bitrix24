<?php
$MESS["GD_RSS_READER_P_CACHE_TIME"] = "緩存時間，秒（0-不要緩存）";
$MESS["GD_RSS_READER_P_CNT"] = "新聞數（0-顯示全部）";
$MESS["GD_RSS_READER_P_IS_HTML"] = "\“ Description \”元素包含HTML";
$MESS["GD_RSS_READER_P_RSSS"] = "預定義的RSS提要（每條從新線開始）";
$MESS["GD_RSS_READER_P_RSS_LINK"] = "鏈接到RSS feed";
$MESS["GD_RSS_READER_P_SHOW_DETAIL"] = "顯示詳細頁面鏈接";
