<?php
$MESS["GD_SHARED_DOCS_P_CNT"] = "最大可見文檔";
$MESS["GD_SHARED_DOCS_P_DATA"] = "展示日期";
$MESS["GD_SHARED_DOCS_P_PIC"] = "顯示圖像";
$MESS["GD_SHARED_DOCS_P_PREV"] = "顯示描述";
$MESS["GD_SHARED_DOCS_P_URL"] = "公司文檔頁面URL";
$MESS["T_COMMON_DISK_LIST_STORAGE_ID"] = "共享文檔存儲";
