<?php
$MESS["GD_PRD_AV_PRICE"] = "平均價格";
$MESS["GD_PRD_NAME"] = "產品";
$MESS["GD_PRD_NO_DATA"] = "沒有數據。";
$MESS["GD_PRD_PRICE"] = "價格";
$MESS["GD_PRD_QUANTITY"] = "數量";
$MESS["GD_PRD_SUM"] = "全部的";
$MESS["GD_PRD_TAB_1"] = "最購買的";
$MESS["GD_PRD_TAB_2"] = "最受關注";
$MESS["GD_PRD_VIEWED"] = "視圖";
