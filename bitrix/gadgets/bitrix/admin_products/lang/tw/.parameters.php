<?php
$MESS["GD_PRD_P_LIMIT"] = "最大限度。專案";
$MESS["GD_PRD_P_MONTH"] = "月";
$MESS["GD_PRD_P_PERIOD"] = "時期";
$MESS["GD_PRD_P_QUATER"] = "四分之一";
$MESS["GD_PRD_P_SITE_ID"] = "網站";
$MESS["GD_PRD_P_SITE_ID_ALL"] = "全部";
$MESS["GD_PRD_P_WEEK"] = "星期";
$MESS["GD_PRD_P_YEAR"] = "年";
