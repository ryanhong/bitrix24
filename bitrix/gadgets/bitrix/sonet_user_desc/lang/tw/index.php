<?php
$MESS["GD_SONET_USER_DESC_CONTACT_TITLE"] = "聯繫信息";
$MESS["GD_SONET_USER_DESC_CONTACT_UNAVAIL"] = "聯繫信息不可用。";
$MESS["GD_SONET_USER_DESC_CONTACT_UNSET"] = "沒有提供聯繫信息。";
$MESS["GD_SONET_USER_DESC_EMPLOYEES"] = "負責：";
$MESS["GD_SONET_USER_DESC_EMPLOYEES_NUM"] = "僱員";
$MESS["GD_SONET_USER_DESC_FORWARD_TO"] = "將消息轉發給您的Bitrix24";
$MESS["GD_SONET_USER_DESC_FORWARD_TO_BLOG_POST"] = "為活動流添加帖子";
$MESS["GD_SONET_USER_DESC_FORWARD_TO_SHOW"] = "展示";
$MESS["GD_SONET_USER_DESC_FORWARD_TO_TASK"] = "創建任務";
$MESS["GD_SONET_USER_DESC_MANAGER"] = "下屬：";
$MESS["GD_SONET_USER_DESC_OTP_ACTIVATE"] = "使能夠";
$MESS["GD_SONET_USER_DESC_OTP_ACTIVE"] = "在";
$MESS["GD_SONET_USER_DESC_OTP_AUTH"] = "兩步身份驗證";
$MESS["GD_SONET_USER_DESC_OTP_CHANGE_PHONE"] = "設置新電話";
$MESS["GD_SONET_USER_DESC_OTP_CODES"] = "恢復代碼";
$MESS["GD_SONET_USER_DESC_OTP_CODES_SHOW"] = "看法";
$MESS["GD_SONET_USER_DESC_OTP_DEACTIVATE"] = "禁用";
$MESS["GD_SONET_USER_DESC_OTP_LEFT_DAYS"] = "（將在＃num＃中啟用）";
$MESS["GD_SONET_USER_DESC_OTP_NOT_ACTIVE"] = "離開";
$MESS["GD_SONET_USER_DESC_OTP_NO_DAYS"] = "總是";
$MESS["GD_SONET_USER_DESC_OTP_PASSWORDS"] = "應用程序密碼";
$MESS["GD_SONET_USER_DESC_OTP_PASSWORDS_SETUP"] = "設定";
$MESS["GD_SONET_USER_DESC_OTP_PROROGUE"] = "延遲";
$MESS["GD_SONET_USER_DESC_PERSONAL_TITLE"] = "個人資料";
$MESS["GD_SONET_USER_DESC_PERSONAL_UNAVAIL"] = "個人信息不可用。";
$MESS["GD_SONET_USER_DESC_SECURITY_TITLE"] = "安全";
$MESS["GD_SONET_USER_DESC_SYNCHRONIZE"] = "同步";
$MESS["GD_SONET_USER_DESC_SYNCHRONIZE_SETUP"] = "設定";
$MESS["GD_SONET_USER_DESC_VOTE"] = "表決";
