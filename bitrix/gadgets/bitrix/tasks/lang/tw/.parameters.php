<?php
$MESS["GD_TASKS_P_ORDER_BY"] = "安排";
$MESS["GD_TASKS_P_ORDER_BY_D1"] = "到期日";
$MESS["GD_TASKS_P_ORDER_BY_D2"] = "創建日期";
$MESS["GD_TASKS_P_ORDER_BY_D3"] = "優先事項";
$MESS["GD_TASKS_P_PATH_TO_TASK"] = "任務頁網址";
$MESS["GD_TASKS_P_PATH_TO_TASK_NEW"] = "新任務頁網址";
$MESS["GD_TASKS_P_TYPE"] = "顯示任務";
$MESS["GD_TASKS_P_TYPE_U"] = "由我創建";
$MESS["GD_TASKS_P_TYPE_Z"] = "分配給我";
